.PHONY: build
up:
	go install && one serve --debug --testing --dbName=one

dbup: 
	docker run -d -p 26258:26257 cockroachdb/cockroach start-single-node --insecure

test:
	./deploy/test.sh

build:
	echo "Compilando frontend"
	cd pocketbiz && npm run build
	echo "Compilando statik"
	statik --src=./pocketbiz/dist
	echo "Compilando programa go"
	(CGO_ENABLED=0 go build -ldflags "-s -w -X bitbucket.org/marcos19/one/cmd.BuildDate=`date -u +.%Y-%m-%dT%H:%M:%S`Z -X bitbucket.org/marcos19/one/cmd.LastCommit=`git log --pretty="%H" | head -n 1` -X bitbucket.org/marcos19/one/cmd.LastCommitDate=`git log --pretty="%aI" | head -n 1`")

upload:
	scp -i /home/marcos/.ssh/gcloud ./one marcos@crosslogic.com.ar:~/one/ 


test_all:
	gotest ./pkg/afip/wsaa/core/wsaa_test.go -tags=wsaa
	gotest ./pkg/afip/wsinsc/get_ticket_test.go -tags=wsaa
	gotest ./... -tags=ws

coverHTML:
	echo "Realizando coverage de package ${pkg}..."
	gotest ${pkg} -coverprofile=/tmp/cover.out 
	go tool cover -html=/tmp/cover.out -o /tmp/cover.html

coverPKG:
	go test	${pkg} -cover -json | tparse -all

