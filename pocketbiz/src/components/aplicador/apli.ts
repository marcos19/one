import {
  Aplicacion,
  Partida,
  DisponiblesResponse,
  Config
} from "@/interfaces/aplicador";

export class Aplicador {
  originales: DisponiblesResponse[];
  pendientes: DisponiblesResponse[];
  tildadas: Map<string, number>;
  cerradas: Partida[];
  totalSeleccionado: number;
  private id: number;

  constructor() {
    this.originales = [];
    this.cerradas = [];
    this.pendientes = [];
    this.tildadas = new Map<string, number>();
    this.totalSeleccionado = 0;
    this.id = 0 as number;
  }
  // Carga las partidas libres que llegan del servidor
  // Si ya había tildadas o cerradas, no se pierden
  load(pp: DisponiblesResponse[]) {
    for (let v of pp) {
      v.MontoAplicado = 0;
    }
    this.originales = pp;
    this.pendientes = pp;
    this.recalcularSaldos();
  }

  tildarParcial(p: DisponiblesResponse, monto: number) {
    // Si tenía aplicaciones parciales, determino que no se pase
    if (p.Saldo < 0 && monto < p.Saldo) {
      throw Error("La aplicación no puede superar el monto disponible");
    }
    if (p.Saldo > 0 && monto > p.Saldo) {
      throw Error("La aplicación no puede superar el monto disponible");
    }

    this.tildadas.set(this.key(p), monto);
    p.MontoAplicado = monto;

    // Si tenía aplicaciones parciales, determino que no se pase
    if (p.Saldo) {
      this.totalSeleccionado += monto;
    }
  }
  // Cuando se marca una partida
  tildar(p: DisponiblesResponse, monto: number) {
    let anterior = this.tildadas.get(this.key(p));

    // Estaba tildada?
    if (anterior) {
      if (monto == 0 || monto == anterior) {
        // Quito el item
        this.destildar(p);
        return;
      } else {
        // Si se puso un monto distinto => actualizo
        this.tildadas.set(this.key(p), monto);
      }
    }

    // Estaba cerrada?
    let cerr = this.cerradas.filter(x => {
      return this.key(x) == this.key(p);
    });
    let montoCerrado = 0;
    for (let v of cerr) {
      montoCerrado += v.Aplicacion;
    }

    // Si tenía aplicaciones parciales, determino que no se pase
    if (p.Saldo < 0) {
      monto = Math.max(p.Saldo - montoCerrado, monto);
      this.totalSeleccionado += monto;
    }
    if (p.Saldo > 0) {
      monto = Math.min(monto, p.Saldo - montoCerrado);
      this.totalSeleccionado += monto;
    }

    p.MontoAplicado = monto;
    this.tildadas.set(this.key(p), monto);
  }
  tildarTodas() {
    this.tildadas.clear();
    this.totalSeleccionado = 0;
    for (let v of this.pendientes) {
      v.MontoAplicado = v.Saldo;
      this.tildadas.set(this.key(v), v.MontoAplicado);
      this.totalSeleccionado += v.MontoAplicado;
    }
  }
  destildar(p: DisponiblesResponse) {
    this.tildadas.delete(this.key(p));
    let found = this.pendientes.find(x => this.key(x) == this.key(p));
    if (!found) {
      throw Error("no se encontró partida para destildar");
    }
    if (found.Saldo) {
      this.totalSeleccionado -= found.MontoAplicado;
    }
    found.MontoAplicado = 0;
  }
  destildarTodas() {
    console.log("destildando todas...");
    for (let v of this.pendientes) {
      v.MontoAplicado = 0;
    }
    console.log("Tildadas tenía", this.tildadas.size);
    this.tildadas.clear();
    console.log("Tildadas despues de borrar", this.tildadas.size);
    this.totalSeleccionado = 0;
  }
  // Cuando se tienen varias partidas seleccionadas, esta función
  // quita las partidas de las disponibles y genera un nuevo grupo.
  cerrarGrupo() {
    // Da cero?
    let sum = 0;
    for (let v of this.tildadas.values()) {
      console.log(v);
      sum += Math.round(v * 100);
    }
    if (sum != 0) {
      throw Error("El grupo no cierra. Diferencia: " + sum / 100);
    }

    let grupoID = this.nuevoGrupoID();
    for (let v of this.originales) {
      let key = this.key(v);
      let estaba = this.tildadas.get(key);
      if (estaba == undefined) {
        continue;
      }

      console.log("Agregando partida a cerradas");
      let p: Partida = {
        OpID: v.OpID,
        PartidasID: v.PartidasID,
        FechaContable: v.FechaContable,
        FechaVto: v.FechaVto,
        CompNombre: v.CompNombre,
        NCompStr: v.NCompStr,
        MontoOriginal: v.MontoOriginal,
        Saldo: v.Saldo,
        Grupo: grupoID,
        Aplicacion: estaba,
        Detalle: v.Detalle,
        DetallePartida: v.DetallePartida
      };
      this.cerradas.push(p);
    }
    this.totalSeleccionado = 0;

    let idsCerradas: string[] = [];
    for (let v of this.cerradas) {
      idsCerradas.push(this.key(v));
    }
    // Busco las originales que no estén cerradas
    this.pendientes = this.originales.filter(x => {
      if (idsCerradas.includes(this.key(x))) {
        return false;
      }
      x.MontoAplicado = 0;
      return true;
    });

    this.tildadas.clear();
    this.recalcularSaldos();
  }
  liberarGrupo(grupoID: number) {
    // Determino los IDS que quedan cerrados luego de liberar
    // este grupo.
    let idsCerradas: string[] = [];
    for (let v of this.cerradas) {
      if (v.Grupo != grupoID) {
        idsCerradas.push(this.key(v));
      }
    }
    console.log("ids cerradas", idsCerradas);

    // Busco las originales que no estén cerradas
    this.pendientes = this.originales.filter(x => {
      if (idsCerradas.includes(this.key(x))) {
        return false;
      }
      console.log("Original queda abierta", x.FechaVto);
      x.MontoAplicado = 0;
      return true;
    });

    console.log("Cerradas son grupo distinto a", grupoID);
    this.cerradas = this.cerradas.filter(x => x.Grupo != grupoID);
    this.destildarTodas();
  }
  // Borra todo
  clear() {
    this.cerradas = [];
    this.originales = [];
    this.tildadas = new Map<string, number>();
    this.recalcularSaldos();
  }
  recalcularSaldos() {
    let sum = 0;
    let totalSeleccionado = 0;
    for (let v of this.pendientes) {
      sum += v.Saldo;
      v.Acumulado = sum;

      if (v.MontoAplicado) {
        totalSeleccionado += v.MontoAplicado;
      }
    }
    this.totalSeleccionado = totalSeleccionado;
  }
  key(v: DisponiblesResponse | Partida): string {
    let key = [v.OpID, v.FechaVto, ...v.PartidasID].join("|");
    // console.log(key);
    return key;
  }
  nuevoGrupoID(): number {
    this.id++;
    return this.id;
  }
}
