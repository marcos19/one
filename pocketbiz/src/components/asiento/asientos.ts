export interface Partida {
  ID: string;
  FechaFinanciera: string;
  Cuenta: string;
  Deposito: string;
  Caja: string;
  Centro: string;
  Producto: string;
  Sucursal: string;
  Persona: string;
  SKU: string;
  Unicidad: string;
  UnicidadTipo: string;
  ConceptoIVA: number | null;
  AlicuotaIVA: number | null;
  PartidaAplicada: string | null;
  Cantidad: number;
  Monto: number;
  UM: string;
  Detalle: string;
  Moneda: string | null;
  MontoMonedaOriginal: number | null;
  TC: number | null;
}
