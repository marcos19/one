export class Config {
  ID: string | null;
  Empresa: string | null;
  Nombre:string;
  Persona: string | null;
  Sucursal: string | null;
  Descripcion: string;
  CSVConfig: CSVConfig;
	Imputaciones:  Imputacion[];
	CompGastos: string | null;
	CompMovs: string |null;
	CuentaContableGastos: string |null;
	CuentaContableMovs: string |null;
  CreatedAt?: string;

  constructor() {
	this.ID = null
    this.Empresa = null;
	this.Nombre = "";
    this.Persona = null;
    this.Sucursal = null;
    this.Descripcion= "";
    this.CSVConfig = new CSVConfig();
	this.Imputaciones = [];
	this.CompGastos = null;
	this.CompMovs= null;
	this.CuentaContableGastos = null;
	this.CuentaContableMovs = null;
  }

  filtraPorCodigo(): boolean {
	if (this.CSVConfig.Columnas["Código"]) {
		return true
	}
	return false
   }
}

export function NewConfig(json: any): Config {
	const c = new Config()
	Object.assign(c, json)
	return c
}
class CSVConfig {
  OmitirRenglones: number;
  Comma: string;
  FechaLayout: string;
  SeparadorDecimal: string;
  SeparadorMiles: string;
  Columnas: Columnas;
  constructor() {
    this.OmitirRenglones = 0;
    this.Comma = ";";
    this.FechaLayout = "02/01/2006";
	this.SeparadorDecimal = "."
	this.SeparadorMiles = ""
    this.Columnas = new Columnas();
  }
}

class Columnas {
  Fecha: number | null;
  Código: number | null;
  Detalle: number | null;
  "Número Referencia": number | null;
  Monto: number | null;
  Débito: number | null;
  Crédito: number | null;
  Saldo: number | null;

  constructor() {
    this.Fecha = null;
    this.Código = null;
    this.Detalle = null;
    this["Número Referencia"] = null;
	this.Monto = null;
    this.Débito = null;
    this.Crédito = null;
    this.Saldo = null;
  }
}

export class Imputacion {
	ID: string | null ; 
	Codigo: string |null;
	Contiene: string;
	ContieneAnterior: string;
	Detalle: string;
	Cuenta: string | null;
	Tipo: string |null;
	IVAConcepto: string |null;
	IVAAlicuota: string |null;

	constructor() {
		this.ID =null
		this.Codigo = null
		this.Contiene = "" 
		this.ContieneAnterior = "" 
		this.Detalle = ""
		this.Cuenta = null
		this.Tipo = null
		this.IVAConcepto = null;
		this.IVAAlicuota = null;
	}
}

// // Event bus para resbancario config.
// type Events = { 
// 	imputacionAgregada: Imputacion,
// 	imputacionModificada: { index: number,imp: Imputacion},
// 	imputacionEliminada: number
// }

// export type Bus = {
// 	$on<E extends keyof Events>(
// 		event: E,
// 		f: (args: Events[E]) => void
// 	): void
// 	$emit<E extends keyof Events>(
// 		event: E,
// 		f: (args: Events[E]) => void
// 	): void
// }

