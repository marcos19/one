export interface SaldoStock {
	SKU: string;
	Codigo: string;
	Nombre: string;
	UM: string;
	Cantidad: number;
	PPP: number;
	Monto: number;
}
