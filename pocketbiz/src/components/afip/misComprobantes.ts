
export interface Recibido{
	Fecha :        string;
	Tipo : number;
	PuntoDeVenta :number;
	NumeroDesde :number;
	NumeroHasta :number;
	CAE :string;
	TipoDocEmisor :string;
	NumeroEmisor : number;
	Denominacion : string;
	TipoDeCambio : number;
	Moneda :       string;
	Neto: number;
	NoGravado: number;
	Exento: number;
	IVA: number;
	Total: number;
}

export interface Propio {
	OpID :     string;
	Fecha :     string;
	CUIT : number;
	TipoComp :  number;
	PuntoVenta :number;
	NComp :number;
	Denominacion :string;
	Neto :  number;
	Exento : number;
	NoGravado : number;
	IVA :         number;
	Total :    number ;
}

export interface Comparacion {
	FaltanContabilizar: Recibido[];
	FaltanEnAFIP: Propio[];
	Propios: Propio[];
	DeAFIP: Recibido[];
	Periodo: string;
}
