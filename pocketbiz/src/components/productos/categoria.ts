export interface Categoria {
	ID: null | string; 	
	Nombre: string;
	Imputable: boolean;
	PadreID: string | null;
	Facetas: string[];
	Mascara: string;
}
