export interface CategoriaConCantidad {
  ID: string;
  Nombre: string;
  Cantidad: number;
}

export interface ListaDePrecio {
  ID: string;
  Nombre: string;
  Moneda: string;
  Porcentaje: number;
}

export interface Producto {
  ID: string | null;
  CodigoAlfanumerico: string;
  Nombre: string;
  NombreImpresion: string;
  Categoria: string | null;
  Descripcion: string;
  EsUnCombo: boolean;
  Inventariable: boolean;
  Cuantificable: boolean;
  TipoUM: string;
  UM: string;
  DisponibleParaVenta: boolean;
  PrecioFinal: boolean;
  AlicuotaIVA: string;
  AlicuotaCeroVenta: boolean;
  Envase: string;
  RelacionesUMFija: boolean;
  RelacionesUM: any[];
  Facetas: any;
  AttsGenerales: Att[];
  Activo: boolean;
  TieneVariantes: boolean;
  Apertura: string[];

  TrabajaConUnicidades: boolean;
  ClaseUnicidad: string | null;
}

export interface Att {
  Key: string;
  Tipo: string;
  String: string | null;
  Number: number | null;
  Bool: boolean | null;
}
export interface Categoria {
  ID: string | null;
  Nombre: string;
  Imputable: boolean;
  PadreID: string;
  FullPath: string;
  Familia: string;
  Facetas: Faceta[];
}

export interface Faceta {
  Nombre: string;
  Tipo: string;
}

export interface SKU {
  ID: string | null;
  Producto: string | null;
  Codigo: string;
  Nombre: string;
  NombreImpresion: string;
  BarCode: string;
  Categoria: string;
  Inventariable: boolean;
  Cuantificable: boolean;
  TipoUM: string;
  UM: string | null;
  RelacionesUMFija: boolean;
  RelacionesUM: any[];
  Atts: any;
}
// Es que devuelve cuando busco productos para una factura.
export interface ProductoView {
  ID: string;
  CodigoAlfanumerico: string;
  Nombre: string;
  Precio: number;
  CantidadStock: number;
  UM: string;
  Categoria: string;
}

export interface ProductoSKUyPrecio {
  Producto: Producto;
  SKUs: Array<SKU>;
  Precios: Array<Precio>;
}

/* Los precios que llegan del backend */
export interface Precio {
  SKU: string;
  Lista: string;
  PrecioNeto: number;
  PrecioFinal: number;
  PrecioFecha: string;
}
