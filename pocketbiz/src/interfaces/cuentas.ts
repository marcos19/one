export interface Cuenta {
  ID: string;
  Codigo: string;
  Nombre: string;
  AperturaPersona: boolean;
  AperturaPersonaRestrictiva: boolean;
  AperturaProducto: boolean;
  AperturaCaja: boolean;
  AperturaDeposito: boolean;
  AperturaContrato: boolean;
  AperturaUnicidad: boolean;
  TipoUnicidad: string | null;
  AperturaCentro: boolean;
  CentrosPermitidos: string[];
  CentrosGrupos: string[];
  TipoCuenta: string;
  AjustaPorInflacion: boolean;
  Monetaria: boolean;
  EsSistemica: boolean;
  Funcion: string;
  RubroBsUso: string | null;
  MonedaExtranjera: boolean;
  Alias: string;
  UsaAplicaciones: boolean;
  MedioDePago: boolean;
  PadreID: string | null;
  Orden: number;
  Descripcion: string;
  ConceptoIvaCompras: string;
  CreatedAt: string | null;
  UpdatedAt: string | null;
  ConceptoProducto: string | null;
}

export interface Grupo {
  ID: string | null;
  Nombre: string;
  Tipo: string | null;
  Detalle: string;
  Cuentas: string[];
}

export function traerHijas(cc: Cuenta[], id: string): Cuenta[] {
  let out = [];
  let hijas = cc.filter(x => x.PadreID == id);
  for (let v of hijas) {
    if (v.TipoCuenta != "Agrupadora") {
      out.push(v);
    } else {
      let nuevas: Cuenta[] = traerHijas(cc, v.ID as string);
      out.push(...nuevas);
    }
  }
  return out;
}
