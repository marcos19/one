export interface Mayor {
  SaldoInicio: number;
  Partidas: Partida[];
  SaldoFinal: number;
}

export interface Partida {
  ID: string;
  OpID: string;
  emp: string;
  fch: string;
  vto: string;
  cmp: string;
  n: string;
  detc: string;
  per: string;
  cta: string;
  mto: number;
  ac: number;
  orig: number;
  tc: number;
  mon: number;
  q: number;
  det: string;
}
