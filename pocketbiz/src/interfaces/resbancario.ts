export interface PartidaImputada {
  Item: number;
  Fecha: string;
  Codigo: string;
  Detalle: string;
  NumeroRef: string;
  Debito: number;
  Credito: number;
  Saldo: number;

  Tipo: string;
  Cuenta: string;
  IVAConcepto: string;
  IVAAlicuota: string;
  Comprobante: string;
}

export interface Resumen {
  ID: string | null;
  Empresa: string | null;
  Config: string | null;
  Desde: string | null;
  Hasta: string | null;
  Persona: string | null;
  PersonaNombre: string;
  Sucursal: string | null;
  SucursalNombre: string;
  Partidas: PartidaImputada[];
  Imputaciones: Imputacion[];
  CreatedAt: string | null;
}

export interface Imputacion {
  Codigo: string;
  Deltalle: string;
  Cuenta: string;
  Tipo: string;
  IVAConcepto: string;
  IVAAlicuota: string;
}

export interface Config {
  ID: string;
  Empresa: string;
  Nombre: string;
  Persona: string;
  Sucursal: string;
  Descripcion: string;
  CSVConfig: CSVConfig;
  Imputaciones: Imputacion[];
  CompGastos: string;
  CompMovs: string;
  CuentaContableGastos: string;
  CuentaContableMovs: string;
  CreatedAt: string;
}

export interface CSVConfig {
  OmitirRenglones: number;
  Comma: string;
  LazyQuotes: boolean;
  Columnas: any;
  FechaLayout: string;
}

export interface Grupo {
  Cuenta: string;
  Partidas: PartidaImputada[];
}
