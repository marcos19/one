export interface Usuario {
  ID: string;
  Email: string;
  Nombre: string;
  Apellido: string;
  DNI: number;
  Estado: string;
  Administrador: boolean;
  Inactivo: boolean;
  UltimaActualizacionContraseña: string;
  CreatedAt: string;
}
