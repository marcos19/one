export interface Domicilio {
  DomCalle: string;
  DomNumero: string;
  DomPiso: string;
  DomDpto: string;
  CiudadID: number;
  CiudadCP: number;
  CiudadNombre: string;
  ProvinciaID: number;
  ProvinciaNombre: string;
  Tipo: string;
}

export interface Provincia {
  ID: number;
  Nombre: string;
}

export interface Ciudad {
  ID: number;
  Nombre: string;
  CP: number;
  CPAlfa: string;
  Provincia: number;
  ProvinciaNombre: string;
}
