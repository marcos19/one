export interface Comp {
  ID: string;
  Empresa: string | null;
  Tipo: string | null;
  Nombre: string;
  Detalle: string;
  PuntoDeVenta: number | null;
  PuntoDeVentaObligatorio: string | null;
  Emisor: string | null;
  TipoEmision: string | null;
  TipoNumeracion: string | null;
  TipoAnulacion: string | null;
  UltimoNumeroUsado: number;
  CalculaIVA: boolean;
  DiscriminaIVA: boolean;
  ImprimeDatosCajero: boolean;
  Letra: string | null;
  CompraOVenta: string | null;
  VaAlLibroDeIVA: boolean;
  LibroIVA: string | null;
  ComprobanteFijo: boolean;
  OriginalesAceptados: string[];

  FechaDesde: string | null;
  FechaHasta: string | null;

  ProductosNombreImpresion: boolean;
  ProductosOcultarCodigo: boolean;
  ProductosOcultarCantidades: boolean;
  MargenDer: number;
  MargenIzq: number;
  MargenSup: number;
  MargenInf: number;
  AlturaCabecera: number;
  AlturaPersona: number;
  AlturaPie: number;
  ImprimeLogo: boolean | null;
  ImprimeBordes: boolean | null;
  ImprimeNombreUsuario: boolean | null;
  ImprimeTS: boolean | null;
}

export interface CompLookup {
  ID: string;
  Empresa: string;
  Nombre: string;
  PuntoDeVenta: number;
}
