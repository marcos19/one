export interface Empresa {
  ID: string;
  Nombre: string;
  CUIT: bigint | null;
  IVA: string | null;
  Domicilio: string;
  CierreEjercicio: number | null;
  FechaDesde: string | null;
  FechaHasta: string | null;
  ImpresionNombre: string;
  ImpresionNombreFantasia: string;
  ImpresionRenglonesLeft: string;
  ImpresionRenglonesRight: string;
  PieMail: string;
  MailHost: string;
  MailPort: number | null;
  MailSenderAlias: string;
  MailUserName: string;
  MailPassword: string;
  MailTemplate: string;
  CreatedAt: string | null;
  UpdatedAt: string | null;
}

export interface EmpresaLookup {
  ID: string;
  Nombre: string;
  IVA: string;
}
