export interface BalanceReq {
  Empresa: string;
  Desde: string;
  Hasta: string;
}

export interface Partida {
  Cuenta: string;
  CuentaNombre: string;
  CuentaTipo: string;
  Codigo: string;
  PadreID: string;
  Centro: string;
  Monto: number;
  Cantidad: number;
  children: Partida[] | null;
  nivel: number;
  Orden: number;
}

// Es el el query que lee Mayor.vue
export interface MayorFiltro {
  empresa: string;
  desde: string;
  hasta: string;
  cuenta: string;
}
