export interface Config {
	ID: null | string;
	Empresa: null | string;
	Nombre: string;
	Detalle: string;
	Tipo: string | null;
	CuentaRefundicion: string | null;	
	CreatedAt?: string;
	UpdatedAt?: string;
}

export interface Esquema {
	ID: null | string;
	Empresa: string | null;
	Nombre: string;
	Detalle: string;
	Config: string | null;
	Comp: string | null;
	CreatedAt: string | null;
	UpdatedAt: string | null;
}

export interface Minuta {
	ID: string | null;
	Empresa: string | null;
	Config: string | null;
	Esquema: string | null;
	Comp: string | null;
	Fecha: string | null;
	PuntoDeVenta: number | null;
	NComp: number | null;
	Detalle: string;
	Partidas: Partida[];
}

export interface Partida {
	ID: string;
	Cuenta: string | null;
	Centro: string | null;
	Caja: string | null;
	Moneda: string | null;
	TC: number | null;
	MontoMonedaOriginal: number | null;
	Deposito: string | null;
	Persona: string | null;
	Sucursal: string | null;
	Vencimiento: string | null;
	Debe: number | null;
	Haber: number | null;
	Detalle: string;
}
