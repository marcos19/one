export interface Aplicacion {
  ID: string | null;
  Fecha: string | null;
  Empresa: string | null;
  Config: string | null;
  Persona: string | null;
  Sucursal: string | null;
  Partidas: Partida[];
}

export interface Partida {
  OpID: string;
  PartidasID: string[];
  FechaContable: string;
  FechaVto: string;
  CompNombre: string;
  NCompStr: string;
  MontoOriginal: string;
  Saldo: number;

  Grupo: number;
  Aplicacion: number;
  Detalle: string;
  DetallePartida: string;
}

export interface DisponiblesResponse {
  OpID: string;
  PartidasID: string[];
  FechaContable: string;
  FechaVto: string;
  CompNombre: string;
  NCompStr: string;
  MontoOriginal: string;
  Saldo: number;
  Detalle: string;
  DetallePartida: string;

  MontoAplicado: number;

  // Totalizo la suma de saldos
  Acumulado: number;
}

export interface Config {
  ID: string;
  Nombre: string;
  Empresa: string;
  Comp: string;
  Cuentas: string[];
  GruposCuentas: string[];
  SugerirFecha: boolean;
  AgruparPartidas: boolean;
  PorPersona: boolean;
  PorFechaVto: boolean;
}
