export interface Caja {
  ID: string;
  Nombre: string;
}

export interface ValorEnCaja {
  CuentaID: string;
  CuentaNombre: string;
  SaldoInicio: number;
  Movimientos: number;
  SaldoFinal: number;
}

