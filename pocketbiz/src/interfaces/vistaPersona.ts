export interface Saldo {
  Cuenta: string;
  Centro: string;
  Monto: number;
  Anticuacion: number;
  AnticuacionPromedio: number;
}

export interface MovimientosResponse {
  SaldoInicioFecha: string | null;
  SaldoInicio: number;
  Movimientos: Movimiento[];
  SaldoFinal: number;
  SaldoFinalFecha: string | null;
}
export interface Movimiento {
  ID: string;
  Fecha: string;
  FechaVto: string;
  CompNombre: string;
  NComp: string;
  Cuenta: string;
  Centro: string | null;
  Monto: number;
  Saldo: number;
  Detalle: string | null;
  CreatedAt: string;
}


// Partida de aplicación pendiente
export interface Partida {
  ID: string;
  FechaContable: string;
  FechaFinanciera: string;
  TipoComp: string;
  NComp: string;
  Cuenta: string;
  Centro: string | null;
  MontoOriginal: number;
  MontoPendiente: number;
  Dias: number;
  Saldo: number;
}

export interface Ventas {
	ID: string;
	Nombre: string;
	UM: string;
	Cantidad: number;
	Monto: number;
}
