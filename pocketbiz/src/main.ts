import App from "./App.vue";
import Vue from "vue";
import Buefy from "buefy";
import store from "./store/store.js";

// Vue.use(Vuelidate);
Vue.component("tabla", () => import("./components/widgets/Tabla.vue"));

Vue.component("multiselect", () =>
  import("@vueform/multiselect/dist/multiselect.vue2.js")
);
import "@vueform/multiselect/themes/default.css";

// Vue.use(Buefy, {});
Vue.use(Buefy);

// Filtros
import {
  enPesos,
  enPorcentaje,
  enteros,
  enFecha,
  enFechaHora,
  enHora,
  debe,
  haber,
  siNo
} from "@/modules/util/format";

Vue.filter("enPesos", enPesos);
Vue.filter("enPorcentaje", enPorcentaje);
Vue.filter("enteros", enteros);
Vue.filter("enFecha", enFecha);
Vue.filter("enFechaHora", enFechaHora);
Vue.filter("enHora", enHora);
Vue.filter("debe", debe);
Vue.filter("haber", haber);
Vue.filter("siNo", siNo);

import "@/modules/autoaplicador/index";
import "@/modules/bloqueosPorCuenta/index";
import "@/modules/apertura/index";
import "@/modules/cajas/index";
import "@/modules/bsuso/index";
import "@/modules/centros/index";
import "@/modules/cierre/index";
import "@/modules/comitentes/index";
import "@/modules/condiciones/index";
import "@/modules/depositos/index";
import "@/modules/inventarios/index";
import "@/modules/inflacion/index";
import "@/modules/lookups/index";
import "@/modules/monedas/index";
import "@/modules/oplog/index";
import "@/modules/personas/index";
import "@/modules/rendiciones/index";
import "@/modules/refundicion/index";
import "@/modules/unicidades/index";
import "@/modules/unicidadesdef/index";
import router from "./router";

new Vue({
  el: "#app",
  render: h => h(App),
  router,
  store
});
