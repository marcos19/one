import Login from "./components/auth/Login.vue";
import SeleccionarComitente from "./components/auth/SeleccionarComitente.vue";
import CerrarSesion from "./components/auth/CerrarSesion.vue";
import RegistracionConfirmar from "./components/auth/RegistracionConfirmar.vue";
import CambiarContraseña from "./components/auth/CambiarContraseña.vue";

import Layout from "./components/layout/Layout.vue";
import Home from "./components/layout/Home.vue";

// ABMs

import AFIPCertificados from "./components/afip/Certificados.vue";
import AFIPCertificado from "./components/afip/Certificado.vue";
import AFIPTickets from "./components/afip/Tickets.vue";
import AFIPMisComprobantes from "./components/afip/MisComprobantes.vue";

import Aplicador from "./components/aplicador/Aplicador.vue";
import AplicadorConfig from "./components/aplicador/AplicadorConfig.vue";
import AplicadorConfigs from "./components/aplicador/AplicadorConfigs.vue";

import Cache from "./components/cache/Cache.vue";

import Cuentas from "./components/cuentas/Cuentas.vue";
import Cuenta from "./components/cuentas/Cuenta.vue";
import CuentasGrupos from "./components/cuentas/Grupos.vue";
import CuentasGrupo from "./components/cuentas/Grupo.vue";

import Conceptos from "./components/conceptos/Conceptos.vue";
import Concepto from "./components/conceptos/Concepto.vue";

import Comprobantes from "./components/comprobantes/Comprobantes.vue";
import Comprobante from "./components/comprobantes/Comprobante.vue";
import ComprobantesAAutorizar from "./components/afip/ComprobantesAAutorizar.vue";

import GruposDeUsuarios from "./components/grupos/GruposDeUsuarios.vue";
import GrupoDeUsuarios from "./components/grupos/GrupoDeUsuarios.vue";

import Empresas from "./components/empresas/Empresas.vue";
import Empresa from "./components/empresas/Empresa.vue";

import Esquemas from "./modules/fact_esquemas/pages/Esquemas.vue";
import Esquema from "./modules/fact_esquemas/pages/Esquema.vue";

import ListasDePrecios from "./components/precios/ListasDePrecios.vue";
import ListaDePrecio from "./components/precios/ListaDePrecios.vue";

import MinutaConfigs from "./components/minuta/MinutaConfigs.vue";
import MinutaEsquemas from "./components/minuta/MinutaEsquemas.vue";
import MinutaConfig from "./components/minuta/MinutaConfig.vue";
import MinutaEsquema from "./components/minuta/MinutaEsquema.vue";

import MostrarComprobante from "./components/op/MostrarComprobante.vue";

import Precios from "./components/precios/Precios.vue";

import Productos from "./components/productos/Productos.vue";
import Producto from "./components/productos/Producto.vue";

import ProductosCategorias from "./components/productos/ProductosCategorias.vue";
import ProductosCategoria from "./components/productos/ProductosCategoria.vue";

import ReconstruccionSaldos from "./components/mantenimiento/ReconstruccionSaldos.vue";

import ResumenesBancarios from "./components/resbancario/Resumenes.vue";
import ResumenBancario from "./components/resbancario/Resumen.vue";
import ResumenConfig from "./components/resbancario/Config.vue";
import ResumenesConfigs from "./components/resbancario/Configs.vue";

import Usuarios from "./components/usuarios/Usuarios.vue";
import Usuario from "./components/usuarios/Usuario.vue";

import UsuariosGrupos from "./modules/usuarios/pages/Grupos.vue";
import UsuarioGrupo from "./modules/usuarios/pages/Grupo.vue";

// Operacionales
import AjusteInflacion from "./components/inflacion/AjusteInflacion.vue";
import Factura from "./modules/fact/pages/Factura.vue";
import ModificarDatos from "./modules/ops/pages/ModificarDatos.vue";

import Historial from "./components/op/Historial.vue";
import Asiento from "./components/asiento/Asiento.vue";
import Mayor from "./components/mayor/Mayor.vue";
import LibroDiario from "./components/libro_diario/LibroDiario.vue";
import LibroIVA from "./components/libro_iva/LibroIVA.vue";
import Balance from "./components/balance/Balance.vue";

import SaldosStock from "./components/inventarios/SaldosStock.vue";
import MayorProductos from "./components/inventarios/MayorProductos.vue";

import Minuta from "./components/minuta/Minuta.vue";
import Op from "./components/op/Op.vue";

import FactConfigs from "./modules/fact_config/pages/Configs.vue";
import FactConfig from "./modules/fact_config/pages/Config.vue";

import LevantarExcel from "./components/finanzas/LevantarExcel.vue";

import Test from "./components/widgets/Test.vue";

export const routes = [
  {
    path: "/",
    component: Layout,
    children: [
      {
        path: "/",
        component: Home,
        name: "home",
      },
      {
        path: "/afip_certificados",
        component: AFIPCertificados,
        name: "afipCertificados",
      },
      {
        path: "/afip_certificado",
        component: AFIPCertificado,
        name: "afipCertificado",
      },
      {
        path: "/mis_comprobantes",
        component: AFIPMisComprobantes,
        name: "misComprobantes",
      },
      {
        path: "/afip_tickets",
        component: AFIPTickets,
        name: "afipTickets",
      },
      {
        path: "/aplicador",
        component: Aplicador,
        name: "aplicador",
      },
      {
        path: "/aplicador_config",
        component: AplicadorConfig,
        name: "aplicadorConfig",
      },
      {
        path: "/aplicador_configs",
        component: AplicadorConfigs,
        name: "aplicadorConfigs",
      },
      {
        path: "/cuentas",
        component: Cuentas,
        name: "cuentas",
      },
      {
        path: "/cuenta",
        component: Cuenta,
        name: "cuenta",
      },
      {
        path: "/cuentas_grupos",
        component: CuentasGrupos,
        name: "cuentasGrupos",
      },
      {
        path: "/cuentas_grupo",
        component: CuentasGrupo,
        name: "cuentasGrupo",
      },
      {
        path: "/conceptos",
        component: Conceptos,
        name: "conceptos",
      },
      {
        path: "/concepto",
        component: Concepto,
        name: "concepto",
      },
      {
        path: "/conceptos/nuevo",
        component: Concepto,
        name: "conceptoNuevo",
      },
      {
        path: "/mayor",
        component: Mayor,
        name: "mayor",
      },
      {
        path: "/empresas",
        component: Empresas,
        name: "empresas",
      },
      {
        path: "/empresa",
        component: Empresa,
        name: "empresa",
      },
      {
        path: "/esquemas",
        component: Esquemas,
        name: "esquemas",
      },
      {
        path: "/esquema",
        component: Esquema,
        name: "esquema",
      },
      {
        path: "/listas_de_precios",
        component: ListasDePrecios,
        name: "listasDePrecios",
      },
      {
        path: "/lista_de_precio",
        component: ListaDePrecio,
        name: "listaDePrecio",
      },
      {
        path: "/ajuste_inflacion",
        component: AjusteInflacion,
        name: "ajusteInflacion",
      },
      {
        path: "/fact",
        component: Factura,
        name: "fact",
      },
      {
        path: "/modificar_datos",
        component: ModificarDatos,
        name: "modificarDatos",
      },
      {
        path: "/recibo",
        component: Factura,
        name: "recibo",
      },
      {
        path: "/grupos_de_usuarios",
        component: GruposDeUsuarios,
        name: "gruposDeUsuarios",
      },
      {
        path: "/grupo_de_usuarios",
        component: GrupoDeUsuarios,
        name: "grupoDeUsuarios",
      },
      {
        path: "/minuta",
        component: Minuta,
        name: "minuta",
      },
      {
        path: "/cache",
        component: Cache,
        name: "cache",
      },
      {
        path: "/comprobantes",
        component: Comprobantes,
        name: "comprobantes",
      },
      {
        path: "/comprobante",
        component: Comprobante,
        name: "comprobante",
      },
      {
        path: "/comprobantes_a_autorizar",
        component: ComprobantesAAutorizar,
        name: "comprobantesAAutorizar",
      },
      {
        path: "/historial",
        component: Historial,
        name: "historial",
      },
      {
        path: "/balance",
        component: Balance,
        name: "balance",
      },
      {
        path: "/asiento",
        component: Asiento,
        name: "asiento",
      },
      {
        path: "/libro_diario",
        component: LibroDiario,
        name: "libroDiario",
      },
      {
        path: "/libro_iva",
        component: LibroIVA,
        name: "libroIVA",
      },
      {
        path: "/op",
        component: Op,
        name: "op",
      },
      {
        path: "/minutas_config",
        component: MinutaConfigs,
        name: "minutaConfigs",
      },
      {
        path: "/minuta_config",
        component: MinutaConfig,
        name: "minutaConfig",
      },
      {
        path: "/minutas_esquema",
        component: MinutaEsquemas,
        name: "minutaEsquemas",
      },
      {
        path: "/minuta_esquema",
        component: MinutaEsquema,
        name: "minutaEsquema",
      },
      {
        path: "/mostrar_comprobante",
        component: MostrarComprobante,
        name: "mostrarComprobante",
      },
      {
        path: "/precios/",
        component: Precios,
        name: "precios",
      },
      {
        path: "/inventario/saldos_stock",
        component: SaldosStock,
        name: "saldosStock",
      },
      {
        path: "/inventario/mayor_productos",
        component: MayorProductos,
        name: "mayorProductos",
      },
      {
        path: "/productos",
        component: Productos,
        name: "productos",
      },
      {
        path: "/producto",
        component: Producto,
        name: "producto",
      },
      // {
      //   path: "/productos_buscador",
      //   component: ProductosBuscador,
      //   name: "productosBuscador"
      // },
      {
        path: "/productos_categorias",
        component: ProductosCategorias,
        name: "productosCategorias",
      },
      {
        path: "/productos_categoria",
        component: ProductosCategoria,
        name: "productosCategoria",
      },
      {
        path: "/reconstruccion_saldos",
        component: ReconstruccionSaldos,
        name: "reconstruccionSaldos",
      },
      {
        path: "/resbancario",
        component: ResumenBancario,
        name: "resbancario",
      },
      {
        path: "/resumenes_bancarios",
        component: ResumenesBancarios,
        name: "resumenesBancarios",
      },
      {
        path: "/resumen_config",
        component: ResumenConfig,
        name: "resumenConfig",
      },
      {
        path: "/resumenes_configs",
        component: ResumenesConfigs,
        name: "resumenesConfigs",
      },
      {
        path: "/fact_configs",
        component: FactConfigs,
        name: "factConfigs",
      },
      {
        path: "/fact_config",
        component: FactConfig,
        name: "factConfig",
      },
      {
        path: "/usuarios",
        component: Usuarios,
        name: "usuarios",
      },
      {
        path: "/usuario",
        component: Usuario,
        name: "usuario",
      },
      {
        path: "/usuarios_grupos",
        component: UsuariosGrupos,
        name: "usuariosGrupos",
      },
      {
        path: "/usuario_grupo",
        component: UsuarioGrupo,
        name: "usuarioGrupo",
      },
      {
        path: "/finanzas/subirMovimientos",
        component: LevantarExcel,
        name: "levantarExcel",
      },
    ],
  },
  {
    path: "/login",
    component: Login,
    name: "login",
  },
  {
    path: "/seleccionar_comitente",
    component: SeleccionarComitente,
    name: "seleccionarComitente",
  },
  {
    path: "/cerrar_sesion",
    component: CerrarSesion,
    name: "cerrarSesion",
  },
  {
    path: "/registracion_confirmar",
    component: RegistracionConfirmar,
    name: "registracionConfirmar",
  },
  {
    path: "/cambiar_pass",
    component: CambiarContraseña,
    name: "cambiarContraseña",
  },
];
