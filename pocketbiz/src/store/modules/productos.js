import axios from "axios";
import eventBus from "@/eventBus.ts";

const state = {
  familias: [],
  categorias: [],
  ums: []
};

const getters = {
  familiaPorID: state => id => {
    return state.familias.find(item => item.ID == id);
  },
  categoriaPorID: state => id => {
    // console.log("buscando ID ", id, "en", state.categorias);
    return state.categorias.find(item => item.ID == id);
  },
  categoriasImputables: state => {
    let imputables = state.categorias.filter(item => item.Imputable);
    imputables.sort((a, b) => {
      return new Intl.Collator("ar").compare(a.FullPath, b.FullPath);
    });
    return imputables;
  },
  umPorID: state => id => {
    return state.ums[id];
  },
  // Se ingresa "Peso" y devuelve Kg, Tonelada, Gramo
  umPorTipo: state => tipo => {
	  const out = []
	  for (const v of Object.values(state.ums)) {
		  if (v.Tipo != tipo) {
			  continue
		  }
			out.push(v)
	  }
	  return out
  }
};

const mutations = {
  fijarFamilias(state, payload) {
    state.array = payload;
  },
  fijarCategorias(state, payload) {
    state.categorias = payload;
  },
  fijarUms(state, payload) {
    state.ums = payload;
  }
};

const actions = {
  pedirAlBackend({ commit }) {
    // Familias
    // axios
    //   .post("/backend/productos_familias/many", {})
    //   .then(res => {
    //     commit("fijarFamilias", res.data, { root: false });
    //   })
    //   .catch(err => {
    //     eventBus.$emit("error", err);
    //   });

    // Categorias
    axios
      .post("/backend/categorias/many", {})
      .then(res => {
        commit("fijarCategorias", res.data, { root: false });
      })
      .catch(err => {
        eventBus.$emit("error", err);
      });

    // Ums
      axios
        .post("/backend/productos/ums", {})
        .then(res => {
          commit("fijarUms", res.data, { root: false });
        })
        .catch(err => {
          eventBus.$emit("error", err);
        });
  }
};
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
