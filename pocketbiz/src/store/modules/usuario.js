import axios from "axios";
import eventBus from "@/eventBus.ts";

const state = {
  Usuario: null,
};

const getters = {
  logged(state) {
    if (!state.Usuario) {
      return false;
    }
    return true;
  },
  nombre(state) {
    if (!state.Usuario) {
      return "Usuario no loggeado";
    }
    return state.Usuario.Nombre + " " + state.Usuario.Apellido;
  },
};

const mutations = {
  setUser(state, usuario) {
    state.Usuario = usuario;
  },
  logout(state) {
    state.Usuario = null;
    state.Empresa = null;
    state.Empresas = [];
  },
};
const actions = {
  // login fija el usuario que está loggeado.
  // Además pide las empresas  disponibles.
  login(context, user) {
    context.commit("setUser", user);
  },
  logout(context) {
    // Le aviso al backend que quiero que mate el token
    axios
      .post("/backend/sesiones/cerrar_sesion")
      .catch((err) => {
        eventBus.$emit("error", err);
      })
      .then(() => {
        // Borro token del browser
        document.cookie =
          "token=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;";
        localStorage.removeItem("token");
        sessionStorage.removeItem("token");
      });

    // Borro datos del usuario en Vue
    context.commit("logout");
  },
  pedirDatosUsuario(context) {
    axios
      .post("/backend/sesiones/datos_usuario")
      .then((res) => {
        context.dispatch("login", res.data);
      })
      .catch((err) => {
        console.log("error pidiendo datos de usuario redireccionadno a login");
        // Si vuelve un error seguramente es por no autorizado
        // ahi redirige al login
        eventBus.$emit("error", err);
      });
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
