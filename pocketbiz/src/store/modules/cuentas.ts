import axios from "axios";
import eventBus from "@/eventBus";
import { Cuenta, traerHijas } from "@/interfaces/cuentas";
const state = {
  array: []
} as State;

interface State {
  array: Cuenta[];
}

const getters = {
  porID: (state: State) => (id: string) => {
    let found = state.array.find(item => item.ID == id);
    if (!found) {
      console.error("no se pudo encontrar cuenta ", id);
      return null;
    }
    return Object.assign({}, found);
  },
  // porIDs devuelve un array con las cuentas contables de los IDs
  // ingresados.
  porIDs: (state: State) => (ids: string[]): Cuenta[] => {
    let ctas = [] as Cuenta[];
    for (let v of ids) {
      let found = state.array.find(item => item.ID == v);
      if (found) {
        ctas.push(found);
      } else {
        let err = Error("No se pudo encontrar la cuenta " + v);
        console.error(err);
        eventBus.$emit("error", err);
      }
    }
    let out = [] as Cuenta[];
    for (let v of ctas) {
      out.push(Object.assign({}, v));
    }
    return out.sort((a: Cuenta, b: Cuenta) => {
      return new Intl.Collator("ar").compare(a.Nombre, b.Nombre);
    });
  },

  // todas devuelve todas las cuentas que sean del tipo ingresado.
  // Si es null no filtra nada.
  todas: (state: State) => (tipo: string): Cuenta[] => {
    let ctas: Cuenta[] = [];
    if (tipo) {
      ctas = state.array.filter(item => {
        if (item.TipoCuenta == tipo) {
          return true;
        }
      });
    } else {
      ctas = state.array;
    }
    let out = [];
    for (let v of ctas) {
      out.push(Object.assign({}, v));
    }
    out = out.sort((a, b) => {
      return new Intl.Collator("ar").compare(a.Nombre, b.Nombre);
    });
    return out;
  },

  imputables: (state: State): Cuenta[] => {
    let arr = state.array.filter(item => {
      if (item.TipoCuenta == "Resultado" || item.TipoCuenta == "Patrimonial" || item.TipoCuenta == "Orden") {
        return true;
      }
    });

    arr.sort((a, b) => {
      return new Intl.Collator("ar").compare(a.Nombre, b.Nombre);
    });
    let out = [];
    for (let v of arr) {
      out.push(Object.assign({}, v));
    }
    return out;
  },

  // Si elijo una cuenta no imputable me devuelve las imputables que están debajo
  imputablesDebajo: (state: State) => (id: string): Cuenta[] => {
    let cta = state.array.find(x => x.ID == id);
    if (!cta) {
      return [];
    }
    if (cta.TipoCuenta != "Agrupadora") {
      return [cta];
    }
    // Se trata de una cuenta agrupadora, devuelvo todas las hijas
    let hijas = traerHijas(state.array, id);

    return hijas;
  },

  valores: (state: State) => {
    let ctas = state.array.filter(item => {
      if (item.MedioDePago == true) {
        return true;
      }
    });
    let out = [];
    for (let v of ctas) {
      out.push(Object.assign({}, v));
    }
    return out;
  },
  conAperturaRestrictivaPorPersona: (state: State) => {
    let arr = state.array.filter(item => {
      if (item.AperturaPersona) {
        return true;
      }
    });
    arr.sort((a, b) => {
      return new Intl.Collator("ar").compare(a.Nombre, b.Nombre);
    });
    let out = [];
    for (let v of arr) {
      out.push(Object.assign({}, v));
    }
    return out;
  },
  porFuncion: (state:State) => (funcion: Funcion) => {
	const out = state.array.filter(x => x.Funcion == funcion)
    out.sort((a, b) => {
      return new Intl.Collator("ar").compare(a.Nombre, b.Nombre);
    });
	return out
  },
  pendientesStock: (state:State) => {
	const pp = ["100", "105", "106", "107", "200", "202", "203", "207"]		
	const m = new Map<string, string>()
	const out: string[] = []
	for (let v of state.array)  {
		if (!v.ConceptoProducto) {
			continue
		}
		if (pp.includes(v.ConceptoProducto)) {
			m.set(v.ConceptoProducto, v.ID as string)
		}
	}
	for (let v of pp) {
		const id = m.get(v)
		if (!id) {
			continue
		}
		out.push(id)
	}
	return out
  },

};

type Funcion = 'Cuenta bancaria' 
| "Recpam"

const mutations = {
  fijarCuentas(state: State, payload: Cuenta[]) {
    state.array = payload;
  }
};

const actions = {
  pedirAlBackend({ commit }: any) {
    axios
      .get("/backend/cuentas", {})
      .then(res => {
        commit("fijarCuentas", res.data, { root: false });
      })
      .catch(err => {
        eventBus.$emit("error", err);
      });
  }
};
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
