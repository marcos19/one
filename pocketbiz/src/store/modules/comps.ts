import axios from "axios";
import eventBus from "@/eventBus";

// Estado inicial
const state = {
  afip: null,
};

interface State {
  afip: ComprobanteAFIP[];
}
interface ComprobanteAFIP {
  ID: string;
  Nombre: string;
}

const getters = {
  // Devuelve todos los comprobantes
  afip(state: State): ComprobanteAFIP[] {
    let out: ComprobanteAFIP[] = [];
    if (!state.afip) {
      return out;
    }
    for (let v of state.afip) {
      out.push(Object.assign({}, v));
    }
    return out;
  },
  // Devuelve todos los comprobantes
  porID: (state: State) => (id: string) => {
    if (!state.afip) {
      return null;
    }
    return state.afip.find(x => x.ID == id);
  }
};

const mutations = {
  fijarCompsAFIP(state: State, payload: ComprobanteAFIP[]) {
    state.afip = payload;
  }
};

const actions = {
  pedirAlBackend({ commit }: any) {
    axios
      .post("/backend/afip/comps")
      .then(res => {
        commit("fijarCompsAFIP", res.data, { root: false });
      })
      .catch(err => {
        eventBus.$emit("error", err);
      });
  }
};
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
