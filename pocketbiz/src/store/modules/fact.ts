import axios from "axios";
import eventBus from "@/eventBus";

const state = {
  // Cada una de las transacciones creadas por el usuario
  configs: [],

  // Factura venta (directa), Factura venta (anticipada), etc
  tipos: [],

  // Estas variables están espejadas de fact/grupos.go
  grupos: [],

  //
  lastUpdate: null
} as FactState;

import { Config, Tipo, GrupoConfig } from "@/modules/fact_config/interfaces";
interface FactState {
  configs: Config[];
  tipos: Tipo[];
  grupos: GrupoConfig[];
}

const getters = {
  porID: (state: FactState) => (id: string) => {
    let config = state.configs.find(item => item.ID == id);
    if (!config) {
      console.error("no se pudo encontrar la config ID ", id);
    }
    return config;
  },

  // Devuelve las config para una empresa
  porEmpresa: (state: FactState) => (id: string) => {
    return state.configs.filter(item => item.Empresa == id);
  },

  tipos: (state: FactState): Tipo[] => {
    return state.tipos;
  },
  tipo: (state: FactState) => (id: string): Tipo | undefined => {
    return state.tipos.find(x => x.ID == id);
  },

  grupos: (state: FactState): GrupoConfig[] => {
    return state.grupos;
  },

  // Devuelve el grupo por ID
  grupo: (state: FactState) => (id: string) => {
    let v = state.grupos.find(item => item.ID == id);
    if (!v) {
      eventBus.$emit("error", new Error("No se pudo encontrar el grupo " + id));
    }
    return v;
  },
  // Se le ingresa el ID de la transacción y devuelve el
  // tipo.
  transaccionTipo: (state: FactState) => (id: string): Tipo => {
    let t = state.configs.find(item => item.ID == id);
    if (!t) {
      console.error("no se encotró transacción ID", id);
      return {} as Tipo;
    }
    let tipo = state.tipos.find(item => item.ID == (t as Config).TipoOp);
    if (!tipo) {
      console.error("no se encontró tipo", t.TipoOp);
      return {} as Tipo;
    }
    return tipo;
  }
};

const mutations = {
  fijarConfigs(state: FactState, payload: Config[]) {
    state.configs = payload;
  },
  fijarTipos(state: FactState, payload: Tipo[]) {
    state.tipos = payload;
  },
  fijarGrupos(state: FactState, payload: GrupoConfig[]) {
    state.grupos = payload;
  }
};

const actions = {
  pedirAlBackend({ commit }: any) {
    // Pido las configuraciones
    axios
      .post("/backend/fact_configs/many", {})
      .then(res => {
        commit("fijarConfigs", res.data, { root: false });
      })
      .catch(err => {
        eventBus.$emit("error", err);
      });

    // Pido los grupos
    axios
      .post("/backend/fact_configs/grupos")
      .then(res => {
        commit("fijarGrupos", res.data, { root: false });
      })
      .catch(err => {
        eventBus.$emit("error", err);
      });
  }
};
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
