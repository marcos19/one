import axios from "axios";
import eventBus from "@/eventBus";
import { EmpresaLookup } from "@/interfaces/empresas";

const state = {
  seleccionada: null as string | null,

  // Son todas las empresas que "podría" ver el usuario
  delComitente: [] as EmpresaLookup[]
} as State;

interface State {
  seleccionada: string | null;
  delComitente: EmpresaLookup[];
}

const getters = {
  // Devuelve el objeto Empresa de la empresa seleccionada
  seleccionada(state: State) {
    return state.delComitente.find(item => {
      return item.ID == state.seleccionada;
    });
  },

  porID: (state: State) => (id: string) => {
    let v = state.delComitente.find(item => item.ID == id);
    if (!v) {
      console.error("no se pudo encontrar la empresa ID", id);
    }
    return v;
  },
  // Por ahora devuelve todas, después debería traer
  // las empresas que están tildadas como "Habilitadas"
  permitidas: (state: State) => {
    return state.delComitente;
  }
};

const mutations = {
  // Fija una empresa como predeterminada
  seleccionar(state: State, empresaID: string) {
    state.seleccionada = empresaID;
  },
  // Fija las empresas del array al state.array
  empresas(state: State, array: EmpresaLookup[]) {
    state.delComitente = array;
  },

  // Fija las empresas permitidias
  permitidas(state: State, array: EmpresaLookup[]) {
    state.delComitente = array;
  }
};

const actions = {
  // Pide las empresas disponibles al backend
  get(context: any) {
    // Pido las empresas disponibles
    axios
      .get("/backend/empresas")
      .then(res => {
        context.commit("empresas", res.data);
      })
      .catch(err => {
        eventBus.$emit("error", err);
      });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
