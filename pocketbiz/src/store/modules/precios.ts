import { ListaDePrecio } from "@/components/productos/productos";
import axios from "axios";
import eventBus from "@/eventBus";

const state = {
  listas: []
};

interface PreciosState {
  listas: ListaDePrecio[];
}

const getters = {
  listaPorID: (state: PreciosState) => (id: string) => {
    return state.listas.find(item => item.ID == id);
  },

  listasPorID: (state: PreciosState) => (ids: string[]) => {
    let found = state.listas.filter(item => {
      return ids.includes(item.ID);
    });
    if (!found) {
      return [];
    }
    return found;
  },

  ids: (state: PreciosState) => (): string[] => {
    let out = [];
    for (let v of state.listas) {
      out.push(v.ID);
    }
    return out;
  },
  // Devuelve todas menos la del ID ingresado
  listasOtras: (state: PreciosState) => (id: string) => {
    return state.listas.filter(item => item.ID != id);
  },

  // Calcula como queda el precio de la listaPropID, cuando
  // el precio de la lista base pasa a ser nuevoPrecio
  coeficienteAjuste: (state: PreciosState) => (listaPropID: string) => {
    let listaProp = state.listas.find(item => item.ID == listaPropID);
    if (!listaProp) {
      eventBus.$emit(
        "error",
        new Error("No se encontró la lista proporcional ID" + listaPropID)
      );
      return;
    }
    // let listaBase = state.listas.filter(item => item.ID != listaProp.ListaBase);
    let coeficiente = 1 + listaProp.Porcentaje / 100;
    // Cuando agregue el tope la lógia entra acá
    return coeficiente;
  }
};

const mutations = {
  fijarListas(state: PreciosState, payload: ListaDePrecio[]) {
    state.listas = payload;
  }
};

const actions = {
  pedirListas({ commit }: any) {
    // Categorias
    axios
      .post("/backend/listasprecios/para_store", {})
      .then(res => {
        commit("fijarListas", res.data, { root: false });
      })
      .catch(err => {
        eventBus.$emit("error", err);
      });
  }
};
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
