import axios from "axios";
import eventBus from "@/eventBus.ts";

const state = {
  // Cada una de las transacciones creadas por el usuario
  configs: [],
  esquemas: [],
  //
  lastUpdate: null
};

const getters = {
  configPorID: state => id => {
    let config = state.configs.find(item => item.ID == id);
    if (!config) {
      console.error("no se pudo encontrar la config ID ", id);
    }
    return config;
  },

  // Devuelve las config para una empresa
  configPorEmpresa: state => id => {
    return state.configs.filter(item => item.Empresa == id);
  },

  // Devuelve las config para una empresa
  esquemaPorID: state => id => {
    return state.esquemas.find(item => item.ID == id);
  },

  // Devuelve los esquemas de un determinado config
  esquemasPorConfig: state => id => {
    return state.esquemas.filter(item => item.Config == id);
  }
};

const mutations = {
  fijarConfigs(state, payload) {
    state.configs = payload;
  },
  fijarEsquemas(state, payload) {
    state.esquemas = payload;
  }
};

const actions = {
  pedirAlBackend({ commit }) {
    // Pido las configuraciones
    axios
      .post("/backend/minutas_config/many", {})
      .then(res => {
        commit("fijarConfigs", res.data, { root: false });
      })
      .catch(err => {
        eventBus.$emit("error", err);
      });
    // Pido los esquemas
    axios
      .post("/backend/minutas_esquema/many", {})
      .then(res => {
        commit("fijarEsquemas", res.data, { root: false });
      })
      .catch(err => {
        eventBus.$emit("error", err);
      });
  }
};
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
