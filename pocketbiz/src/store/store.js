import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";

import comps from "./modules/comps";
//import condiciones from "./modules/condiciones/store";
import comitentes from "@/modules/comitentes/store";
import cuentas from "./modules/cuentas";
import empresas from "./modules/empresas";
import fact from "./modules/fact";
import iva from "../modules/afip/store";
import minuta from "./modules/minuta";
// import personas from "./modules/personas";
import productos from "./modules/productos";
import precios from "./modules/precios";
import unicidadesdef from "../modules/unicidadesdef/store";
import usuario from "./modules/usuario";
import eventBus from "@/eventBus";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    comps,
    comitentes,
    cuentas,
    empresas,
    fact,
    iva,
    minuta,
    precios,
    // personas,
    productos,
    unicidadesdef,
    usuario,
  },
  state: {
    procesosPendientes: 0,
  },
  strict: process.env.NODE_ENV !== "production",
  mutations: {
    // A medida que se terminan de cargar las solicitudas al store
    // voy disminuyendo el contador.
    requestEnded(state) {
      state.procesosPendientes -= 1;
    },
    // Cuando inicio la carga de todos los datos, pongo este contador.
    iniciarProceso(state, payload) {
      state.procesosPendientes = payload;
    },
  },
  actions: {
    actualizarTodo(context) {
      let promesas = [
        context.dispatch("cajas/pedirAlBackend", null, { root: true }),
        context.dispatch("centros/pedirAlBackend", null, { root: true }),
        context.dispatch("comps/pedirAlBackend", null, { root: true }),
        context.dispatch("comitentes/pedirAlBackend", null, { root: true }),
        context.dispatch("condiciones/pedirAlBackend", null, { root: true }),
        context.dispatch("cuentas/pedirAlBackend", null, { root: true }),
        context.dispatch("depositos/pedirAlBackend", null, { root: true }),
        context.dispatch("empresas/get", null, { root: true }),
        context.dispatch("fact/pedirAlBackend", null, { root: true }),
        context.dispatch("iva/pedirAlBackend", null, { root: true }),
        context.dispatch("minuta/pedirAlBackend", null, { root: true }),
        context.dispatch("monedas/pedirAlBackend", null, { root: true }),
        context.dispatch("unicidadesdef/pedirAlBackend", null, { root: true }),
        context.dispatch("precios/pedirListas", null, { root: true }),
        context.dispatch("productos/pedirAlBackend", null, { root: true }),
      ];
      context.commit("iniciarProceso", promesas.length);
      Promise.all(promesas).catch((err) => {
        eventBus.$emit("error", err);
      });
    },

    cambiaronCajas(context) {
      context.dispatch("cajas/pedirAlBackend");
    },

    cambiaronCategoriasProductos(context) {
      context.dispatch("productos/pedirAlBackend");
    },

    cambiaronCentros(context) {
      context.dispatch("centros/pedirAlBackend");
    },

    cambiaronComps(context) {
      context.dispatch("comps/pedirAlBackend");
    },

    cambiaronCondiciones(context) {
      context.dispatch("condiciones/pedirAlBackend");
    },

    cambiaronFactConfigs(context) {
      context.dispatch("fact/pedirAlBackend");
    },

    cambiaronCuentas(context) {
      context.dispatch("cuentas/pedirAlBackend");
    },

    cambiaronEmpresas(context) {
      context.dispatch("empresas/get");
    },

    cambiaronEsquemas(context) {
      // No los estoy trabajando con el cache
    },

    cambiaronListasDePrecios(context) {
      context.dispatch("precios/pedirListas");
    },

    cambiaronProductos(context) {
      context.dispatch("productos/pedirAlBackend");
    },

    cambiaronUnicidades(context) {
      context.dispatch("unicidadesdef/pedirAlBackend");
    },
  },
  plugins: [
    createPersistedState({
      storage: window.localStorage,
    }),
  ],
});

export default store;
