import Centros from "@/modules/centros/pages/Centros.vue";
import Centro from "@/modules/centros/pages/Centro.vue";
import Grupos from "@/modules/centros/pages/Grupos.vue";
import Grupo from "@/modules/centros/pages/Grupo.vue";

import Layout from "@/components/layout/Layout.vue";
import router from "@/router";

router.addRoute("centros", {
  path: "/centros",
  component: Layout,
  children: [
    {
      path: "centros",
      component: Centros,
      name: "centros"
    },
    {
      path: "centro",
      component: Centro,
      name: "centro"
    },
    {
      path: "centros_grupos",
      component: Grupos,
      name: "centrosGrupos"
    },
    {
      path: "centros_grupo",
      component: Grupo,
      name: "centrosGrupo"
    }
  ]
});
