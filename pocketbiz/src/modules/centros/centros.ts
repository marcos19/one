export interface Centro {
  ID: string;
  Nombre: string;
  EsFamiliaDeProductos: boolean;
}

export interface Grupo {
  ID: string | null;
  Nombre: string;
  Detalle: string;
  Centros: string[];
}
