import axios from "axios";
import eventBus from "@/eventBus";
import { Centro, Grupo } from "@/modules/centros/centros";
import store from "@/store/store";

const state = {
  array: [] as Centro[],
  grupos: [] as Grupo[],
} as State;

interface State {
  array: Centro[];
  grupos: Grupo[];
}

const getters = {
  porID:
    (state: State) =>
    (id: string): Centro | null => {
      let centro = state.array.find((item) => item.ID == id);
      if (!centro) {
        return null;
      }
      return centro;
    },
  grupos: (state: State): Grupo[] => {
    return state.grupos;
  },
  familiasDeProductos: (state: State) => {
    return state.array.filter((item) => item.EsFamiliaDeProductos);
  },
  // Si ids == null => trae todos
  many: (state: State) => (ids: string[] | null, grupos?: string[]) => {
    console.log("Buscando centros IDS", ids);
    let set = new Set<String>();

    // Agrego IDs
    if (ids) {
      for (let v of ids) {
        set.add(v);
      }
    } else {
      return state.array;
    }

    // Agrego los centros de los grupos
    if (grupos) {
      for (let v of grupos) {
        let gr = state.grupos.find((x) => x.ID == v);
        if (!gr) {
          console.error("no se encontró grupo ID", v);
          continue;
        }
        for (let id of gr.Centros) {
          set.add(id);
        }
      }
    }

    // Traigo los centros
    let out: Centro[] = [];
    for (let v of set.values()) {
      console.log("buscando", v);
      let found = state.array.find((item) => item.ID == v);
      if (!found) {
        eventBus.$emit("error", Error("No se pudo encontrar el centro " + v));
        continue;
      }
      out.push(found);
    }
    return out.sort((a: Centro, b: Centro) => {
      return new Intl.Collator("ar").compare(a.Nombre, b.Nombre);
    });
  },
  todos: (state: State) => {
    return state.array;
  },
};

const mutations = {
  fijarCentros(state: State, payload: any) {
    state.array = payload.sort((a: Centro, b: Centro) => {
      return new Intl.Collator("ar").compare(a.Nombre, b.Nombre);
    });
  },
  fijarGrupos(state: State, payload: any) {
    state.grupos = payload;
  },
};

const actions = {
  pedirAlBackend({ commit }: any) {
    axios
      .get("/backend/centros")
      .then((res) => {
        commit("fijarCentros", res.data, { root: false });
      })
      .catch((err) => {
        eventBus.$emit("error", err);
      });
    axios
      .post("/backend/centrosgrupos/many", {})
      .then((res) => {
        commit("fijarGrupos", res.data, { root: false });
      })
      .catch((err) => {
        eventBus.$emit("error", err);
      });
  },
};

store.registerModule("centros", {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
});
