import { Permiso } from "@/modules/fact_config/permiso";

export interface Config {
  ID: string | null;
  Empresa: string;
  Nombre: string;
  TipoOp: string | null;
  Grupo: string;
  Detalle: string;
  SugiereFechaActual: boolean;
  CondicionPago: string;
  PermiteProductos: boolean;
  PermiteDetalleProductos: boolean;
  ProductosValorizados: boolean;
  PermitePrecioCero: boolean;
  TotalizaDesdePartidasFact: boolean;
  ProductosSe: string;
  ProductosSeAplicanACuentas: string[];
  MuestraDepositos: boolean;
  PreciosDesde: string;
  UsaListaDePrecios: boolean;
  PermiteModificarPrecios: boolean;
  TrabajaConPrecioFinal: boolean;
  PermiteProductosPorCuentaDeTerceros: boolean;
  PermiteConceptos: boolean;
  PermitePartidasDirectas: boolean;
  DetalleObligatorioEnPartidas: boolean;
  CopiaPersonaDeCabecera: boolean;
  PermiteValores: boolean;
  BuscadorPersonasExtendido: boolean;
  PermiteAplicar: boolean;
  EmiteRecibo: boolean;
  CuentasAnticipo: string[];
  GrupoCuentasAplicarRecibo: string | null;
  UnicidadesQueSeCrean: string[];
  EspecificarUnicidades: boolean;
  DiferenciaMaxRedondeo: number;

  SignoRenglonProducto: number;
  SignoRenglonValor: number;
  NoHaceAsiento: boolean;
  HaceAsientoEstandar: boolean;
  HaceAsientoCosto: boolean;
  CuentaRenglonGeneraAplicacion: boolean;
  CuentaPatrimonialGeneraAplicacion: boolean;

  EsNotaDeCredito: boolean;

  Imputaciones: ImputacionContable[];

  GeneraReingreso: boolean;
  ReingresoObligatorio: boolean;
  ReingresoEmpresa: string | null;
  ReingresoCuentaIVA: string | null;
  ReingresoCuentaReingreso: string | null;
  ReingresoCuentaPatrimonial: string | null;

  GeneraReingresoPago: boolean;
  ReingresoPagoObligatorio: boolean;
  ReingresoPagoCuentasPermitidas: string[];

  PermiteImputacionDiferida: boolean;

  MostrarSaldo: boolean;
  GrupoCuentasSaldo: string | null;

  CategoriasIgnoradasEnFactura: string[];
  Inactiva: boolean;

  Permisos: Permiso[];
}

export interface ImputacionContable {
  Categoria: string; // ID

  CuentaRenglon: string;
  CuentaPatrimonial: string;
  IVA: string;
  Diferimiento: string;

  ProductosSolicitadosARecibir: string;
  ProductosSolicitados: string;
  ProductosSolicitadosCancelados: string;
  ProductosSolicitadosRecibidos: string;
  ProductosSolicitadosFacturados: string;
  FacturasARecibir: string;
  ProductosCompradosARecibir: string;
  NotasDeCreditoARecibir: string;
  Productos: string;
  Proveedores: string;

  PedidosDeClientesAEntregar: string;
  PedidosDeClientes: string;
  FacturasAEmitir: string;
  ProductosFacturadosAEntregar: string;
  PedidosDeClientesEntregados: string;
  PedidosDeClientesCancelados: string;
  PedidosDeClientesFacturados: string;
  Clientes: string;
  Ventas: string;

  CuentaSaldoUnicidad: string;
  CuentasSaldosUnicidad: string;
}

export interface GrupoConfig {
  ID: string;
  NombreSingular: string;
  Genero: string;
}

// Tipo es la configuración genérica que tiene cada comprobante.
// Contienen la información para saber cómo es el asiento que tienen
// que contabilizar.
export interface Tipo {
  ID: string;
  Nombre: string;
  Grupo: GrupoConfig;
  CuentaRenglon: string;
  CuentaPatrimonial: string;
  SignoRenglon: number;
  HaceAsientoCosto: string;
  CuentaBsCambio: string;
  CuentaRenglonGeneraAplicacion: boolean;
  CuentaPatrimonialGeneraAplicacion: boolean;
}

