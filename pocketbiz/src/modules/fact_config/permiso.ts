export interface Permiso {
  ID: string;
  Empresa?: string;
  Config: string;
  Esquema: string | null;
  Usuario: string | null;
  GrupoUsuario: string | null;
  Create: boolean;
  Read: boolean;
  Update: boolean;
  Delete: boolean;
  // DiasFuturo: number;
  // DiasPasado: number;

  // Lo lleno desde el frontend, es para mostrar
  GrupoUsuarioNombre: string;
}
