import { startOfToday } from "date-fns";

// Devuelve la fecha en la hora CERO para el local time.
export function newDate(str: string | Date): Date {
  const dt = new Date(str);
  const adj = new Date(dt.valueOf() + dt.getTimezoneOffset() * 60 * 1000);
  return adj;
}

export function hoy(): Date {
  return newDate(startOfToday());
}
