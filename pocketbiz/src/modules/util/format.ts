/**
 * Devuelve un número con formato 325.151,19
 */
export function enPesos(value: number): string {
  if (isNaN(value)) {
    return "NaN";
  }
  if (!value) {
    return "-";
  }
  return formatterEnPesos.format(value);
}

var formatterEnPesos = new Intl.NumberFormat("es-AR", {
  style: "decimal",
  currency: "ARS",
  minimumFractionDigits: 2,
  maximumFractionDigits: 2,
});

export function enPorcentaje(value: number) {
  if (isNaN(value)) {
    return "";
  }
  return porcentaje.format(value * 100) + "%";
}

// Filtro porcentaje
var porcentaje = new Intl.NumberFormat("es-AR", {
  style: "decimal",
  currency: "ARS",
  // minimumFractionDigits: 4
});
var formatterEnteros = new Intl.NumberFormat("es-AR", {
  style: "decimal",
  currency: "ARS",
  minimumFractionDigits: 0,
});

export function enteros(value: number) {
  if (value == 0) {
    return "-";
  }
  if (!value) {
    return "";
  }
  try {
    let decimalCount = 0;
    let thousands = ".";
    let decimal = ",";
    const negativeSign = value < 0 ? "-" : "";

    let valStr = Math.abs(Number(value) || 0).toFixed(decimalCount);
    let valInt = parseInt(valStr);
    valStr = valInt.toString();
    let j = valStr.length > 3 ? valStr.length % 3 : 0;

    return (
      negativeSign +
      (j ? valStr.substr(0, j) + thousands : "") +
      valStr.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) +
      (decimalCount
        ? decimal +
          Math.abs(value - valInt)
            .toFixed(decimalCount)
            .slice(2)
        : "")
    );
  } catch (e) {
    console.log(e);
  }
}

import { newDate } from "@/modules/util/fechas";
import { format } from "date-fns";
export function enFecha(value: string | null) {
  if (value == null) {
    return "-";
  }
  try {
    let d = newDate(value);
    return format(d, "dd/MM/yyyy");
  } catch {
    return "N/D";
  }
}

// Devuelve 10/02/2020 03:34 en el timezone del usuario
export function enFechaHora(value: string) {
  if (!value) {
    return "-";
  }
  try {
    const mom = new Date(value);
    return format(mom, "dd/MM/yyyy HH:mm");
  } catch {
    return "N/D";
  }
}

export function enHora(value: string) {
  if (!value) {
    return "-";
  }
  try {
    let mom = new Date(value);
    return format(mom, "HH:mm");
  } catch {
    return "N/D";
  }
}

export function debe(value: number) {
  if (value > 0) {
    return enPesos(value);
  } else {
    return "-";
  }
}

export function haber(value: number) {
  if (value < 0) {
    return enPesos(-value);
  } else {
    return "-";
  }
}

export function siNo(value: boolean) {
  if (value == true) {
    return "Sí";
  } else {
    return "No";
  }
}
