import { Permiso } from "@/modules/fact_config/permiso"

// Esquema representa un conjunto de transacción, comprobantes y permisos.
export interface Esquema {
  ID: string | null;
  Empresa: string | null;
  Config: string | null;
  Grupo: string;
  Comprobantes: any;
  Nombre: string;
  Detalle: string;

  CAE: string;
  Cajas: string[];
  Valores: string[];
  PartidasDirectasConfig: PartidaDirectaConfig[];
  Depositos: string[];
  Condiciones: string[];

  EnviarFacturasPorMail: boolean;
  EnviarRecibosPorMail: boolean;

  Permisos: Permiso[];
}

export interface PartidaDirectaConfig {
  Cuenta: string | null;
  Conceptos: string[];
  Alicuotas: string[];
  ContraCuenta: string | null;
}
