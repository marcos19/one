/**
 Fija la route con la query ingresada, sin que esto tenga efecto sobre Vue
*/
export function updateQuery(newQuery: any, vueInstance: any) {
  let q = Object.keys(newQuery)
    .map(key => {
      let newVal = newQuery[key];
      if (newVal == null || newVal == undefined) {
        return;
      }
      return encodeURIComponent(key) + "=" + encodeURIComponent(newVal);
    })
    .join("&");
  //   console.log("Path era", this.$route.path);
  //   console.log("FullPath era", this.$route.fullPath);
  let newurl =
    window.location.protocol +
    "//" +
    window.location.host +
    "/#" +
    vueInstance.$route.path +
    "?" +
    q;
  window.history.replaceState({ path: newurl }, "", newurl);
}
