
import Cierre from "./pages/Cierre.vue";

import Layout from "@/components/layout/Layout.vue";
import router from "@/router";

router.addRoute("cierre", {
  path: "/cierre",
  component: Layout,
  children: [
    {
      path: "/cierre",
      component: Cierre,
      name: "asientoCierre"
    }
  ]
});
