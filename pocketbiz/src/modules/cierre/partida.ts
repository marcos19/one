
export interface Partida {
	Item: number;
	Cuenta: string;
	CuentaNombre: string;
	Centro: string;
	CentroNombre: string;
	Monto: number;
}
