export interface Caja {
  ID: string;
  Nombre: string;
  AceptaSaldoNegativo: boolean;
  OtrasCuentas: string[];
}

import axios from "axios";
import eventBus from "@/eventBus";
import store from "@/store/store";

const state = {
  cc: [],
  lookup: []
};

interface State {
  cc: Caja[];
  lookup: Caja[];
}

const getters = {
  porID: (state: State) => (id: string) => {
    return state.cc.find(item => item.ID == id);
  },
  porIDs: (state: State) => (ids: string[]) => {
    return state.cc.filter(item => ids.includes(item.ID));
  },
  // Devuelve el objeto Empresa de la empresa seleccionada
  todas: (state: State) => {
    return state.cc;
  },
  // Devuelve el objeto Empresa de la empresa seleccionada
  permitidas: (state: State) => {
    return state.lookup;
  }
};

const mutations = {
  fijarCajas(state: State, payload: Caja[]) {
    state.cc = payload;
  },
  fijarCajasPermitidas(state: State, payload: Caja[]) {
    state.lookup = payload;
  }
};

const actions = {
  pedirAlBackend({ commit }: any) {
    axios
      .get("/backend/cajas")
      .then(res => {
        commit("fijarCajas", res.data, { root: false });
      })
      .catch(err => {
        eventBus.$emit("error", err);
      });
    axios
      .get("/backend/cajas?lookup")
      .then(res => {
        commit("fijarCajasPermitidas", res.data, { root: false });
      })
      .catch(err => {
        eventBus.$emit("error", err);
      });
  }
};

store.registerModule("cajas", {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
});
