import Cajas from "@/modules/cajas/pages/Cajas.vue";
import Caja from "@/modules/cajas/pages/Caja.vue";
import ResumenDeCaja from "@/modules/cajas/pages/ResumenDeCaja.vue";

import Layout from "@/components/layout/Layout.vue";
import router from "@/router";

router.addRoute("cajas", {
  path: "/cajas",
  component: Layout,
  children: [
    {
      path: "cajas",
      component: Cajas,
      name: "cajas"
    },
    {
      path: "caja",
      component: Caja,
      name: "caja"
    },
    {
      path: "resumen_de_caja",
      component: ResumenDeCaja,
      name: "resumenDeCaja"
    }
  ]
});
