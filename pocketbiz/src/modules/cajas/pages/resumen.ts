export interface Response {
  // La key es la moneda, cada renglón es una cuenta contable
  Saldos: Record<string, Resumen[]>;

  // La key es la moneda, cada renglón es una cuenta contable
  Totales: Record<string, Resumen>;
  Movs: Mov[];
}

export interface Resumen {
  Cuenta: string;
  Inicio: number;
  Entradas: number;
  Salidas: number;
  Final: number;
  InicioOrig: number;
  EntradasOrig: number;
  SalidasOrig: number;
  FinalOrig: number;

  // Campo lo pega el frontend
  Nombre: string;
}

export interface Mov {
  ID: string;
  Fecha: string;
  TS: string;
  Cuenta: string;
  Moneda: string;
  Monto: number;
  MontoOrig: number;
  Saldo: number;
  SaldoOrig: number;
  Comp: string | null;
  NComp: string | null;
  Detalle: string | null;
  Persona: string | null;
}

