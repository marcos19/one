export interface Config {
	ID: string | null;
	Empresa: string | null;
	Comp: string | null;
	CuentaRecpam: string | null;
	Indice: string;
	Nombre: string;
	CreatedAt: string | null;
	UpdatedAt: string | null;
}
