export interface Tran {
	ID: string;
	Empresa: string;
	Config: string;
	Desde: string;
	Hasta: string;
	Ops: Op[];
	CreatedAt: string;
	Usuario: string;
}

export interface Op {
	ID: string;
	Fecha: string;
	Nombre: string;
	NCompStr: string;
	Detalle: string;
	CantidadPartidas: string;
}
