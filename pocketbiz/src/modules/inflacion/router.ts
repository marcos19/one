import GenerarAjuste from "./pages/GenerarAjusteInflacion.vue";
import Configs from "./pages/Configs.vue";
import Config from "./pages/Config.vue";
import ListAjustes from "./pages/ListAjustes.vue";
import Ajuste from "./pages/Ajuste.vue";

import Layout from "@/components/layout/Layout.vue";
import router from "@/router";

router.addRoute("inflacion", {
  path: "/inflacion",
  component: Layout,
  children: [
    {
      path: "ajustes",
      component: ListAjustes,
      name: "inflacionAjustes"
    },
    {
      path: "ajuste",
      component: Ajuste,
      name: "inflacion"
    },
    {
      path: "generar_ajuste",
      component: GenerarAjuste,
      name: "generarAjusteInflacion"
    },
    {
      path: "configs",
      component: Configs,
      name: "inflacionConfigs"
    },
    {
      path: "config",
      component: Config,
      name: "inflacionConfig"
    }
  ]
});
