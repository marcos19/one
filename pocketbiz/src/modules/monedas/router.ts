import Moneda from "./pages/Moneda.vue";
import Monedas from "./pages/Monedas.vue";

import Layout from "@/components/layout/Layout.vue";
import router from "@/router";

router.addRoute("monedas", {
  path: "/monedas",
  component: Layout,
  children: [
    {
      path: "monedas",
      component: Monedas,
      name: "monedas"
    },
    {
      path: "moneda",
      component: Moneda,
      name: "moneda"
    }
  ]
});
