import { Moneda } from "./monedas";
import store from "@/store/store";
import axios from "axios";
import eventBus from "@/eventBus";
interface State {
  monedas: Moneda[];
}

const state = {
  monedas: [] as Moneda[]
};

const getters = {
  disponibles: (state: State): Moneda[] => {
    return state.monedas;
  },
  porID: (state: State) => (id: string) => {
    let found = state.monedas.find(x => x.ID == id);
    if (found) {
      return found;
    }
    return null;
  }
};

const mutations = {
  fijarMonedas(state: State, payload: Moneda[]) {
    state.monedas = payload;
  }
};

const actions = {
  pedirAlBackend({ commit }: any) {
    axios
      .post("/backend/monedas/many")
      .then(res => commit("fijarMonedas", res.data, { root: false }))
      .catch(err => {
        eventBus.$emit("error", err);
      });
  }
};

store.registerModule("monedas", {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
});
