export interface Moneda {
  ID: string | null;
  Codigo: string;
  Nombre: string;
  NombrePlural: string;
  Simbolo: string;
}
