
import axios from "axios";
import eventBus from "@/eventBus";
import { Deposito } from "./deposito";
import store from "@/store/store";

const state = {
  array: [] as Deposito[],
} as State;

interface State {
  array: Deposito[];
}

const getters = {
  porID: (state: State) => (id: string): Deposito | null => {
    let centro = state.array.find(item => item.ID == id);
    if (!centro) {
      return null;
    }
    return centro;
  },
  // Si ids == null => trae todos
  porIDs: (state: State) => (ids: string[]) => {

	if (!ids) {
		return state.array	
	}

    // Traigo los centros
    let out: Deposito[] = state.array.filter(item =>  ids.includes(item.ID));
	console.log("depositos encontrados", out)
    return out.sort((a: Deposito, b: Deposito) => {
      return new Intl.Collator("ar").compare(a.Nombre, b.Nombre);
    });
  },
  todos: (state: State) => {
    return state.array;
  }
};

const mutations = {
  fijarDepositos(state: State, payload: any) {
    state.array = payload.sort((a: Deposito, b: Deposito) => {
      return new Intl.Collator("ar").compare(a.Nombre, b.Nombre);
    });
  },
};

const actions = {
  pedirAlBackend({ commit }: any) {
    axios
      .post("/backend/depositos/todos", {})
      .then(res => {
        commit("fijarDepositos", res.data, { root: false });
      })
      .catch(err => {
        eventBus.$emit("error", err);
      });
  }
};

store.registerModule("depositos", {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
});
