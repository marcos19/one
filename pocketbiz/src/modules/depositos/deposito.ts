export interface Deposito {
	ID: string;
	Nombre: string;
	DisponibleParaVenta: boolean;
	AceptaStockNegativo: boolean;
	ValidoDesde: boolean;
	ValidoHasta: boolean;
	CreatedAt: string | null;
	UpdatedAt: string | null;
}
