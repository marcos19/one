
import Deposito from "./pages/Deposito.vue";
import Depositos from "./pages/Depositos.vue";

import Layout from "@/components/layout/Layout.vue";
import router from "@/router";

router.addRoute("depositos", {
  path: "/depositos",
  component: Layout,
  children: [
    {
      path: "/depositos",
      component: Depositos,
      name: "depositos"
    },
    {
      path: "/deposito",
      component: Deposito,
      name: "deposito"
    }
  ]
});
