export interface Op {
  ID: string | null;
  Empresa: string;
  Fecha: string;
  FechaContable: string | null;
  Programa: string;
  TranID: string;
  TranDef: string;
  CompItem: number | null;
  CompID: string;
  CompNombre: string;
  PuntoDeVenta: number | null;
  NComp: number | null;
  NCompStr: string;
  Persona: string | null;
  Sucursal: string | null;
  PersonaNombre: string;
  TotalComp: number;
  Detalle: string;
  TipoMoneda: string;
  TipoCambio: number;
  TipoCotizacion: string;
  Impresion: any;
  FechaOriginal: string | null;
  TotalesConceptos: TotalConcepto[];
  AFIPComprobanteID: string | null;
  PartidasContables: [];
  CAE: string | null;
  CreatedAt: string | null;
}

export interface TotalConcepto {
	Nombre: string;
	Monto: number;
}
