export interface Comitente {
  ID: string;
  Nombre: string;
  CuentaCorriente: string;
}
