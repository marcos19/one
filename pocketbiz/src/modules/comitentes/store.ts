import axios from "axios";
import eventBus from "@/eventBus";

import { Comitente } from "./comitente";

const state = {
  enUso: null as Comitente | null,
};

interface State {
  enUso: Comitente | null;
}

const mutations = {
  // Fija una empresa como predeterminada
  fijar(state: State, comitente: Comitente) {
    state.enUso = comitente;
  },
};

const getters = {
  enUso: (state: State): Comitente | null => {
    return state.enUso;
  },
};

const actions = {
  // Pide las empresas disponibles al backend
  pedirAlBackend(context: any) {
    // Pido las empresas disponibles
    axios.get("/backend/comitentes/en_uso").then((res) => {
      context.commit("fijar", res.data);
    });
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
