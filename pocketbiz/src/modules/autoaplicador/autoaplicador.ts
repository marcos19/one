export interface Partida {
	PersonaID: string;
	PersonaNombre: string;
	Cantidad: number;
	tildado: boolean;
}
