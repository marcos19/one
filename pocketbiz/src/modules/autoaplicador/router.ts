import Autoaplicador from "@/modules/autoaplicador/pages/Autoaplicador.vue";
import VerAplicacion from "@/modules/autoaplicador/pages/VerAplicacion.vue";
import Layout from "@/components/layout/Layout.vue";
import router from "@/router";

router.addRoute("autoaplicador", {
  path: "/autoaplicador",
  component: Layout,
  children: [
    {
      path: "autoaplicador",
      component: Autoaplicador,
      name: "autoaplicador"
    },
    {
      path: "ver_aplicacion",
      component: VerAplicacion,
      name: "verAplicacion"
    },
  ]
});
