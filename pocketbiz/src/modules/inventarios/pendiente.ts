// export interface Pendiente {
// 	ID: string;
// 	Alfa: string;
// 	Nombre: string;
// 	Rec: string;
// }

export interface PendientePorSKU {
	// OpID: string;
	// PartidasID: string[];
	// FechaContable: string;
	// FechaVto: string;
	// CompNombre: string;
	// NCompStr: string;
	// CantidadOriginal: number;
	// CantidadPendiente: number;
	// MontoPendiente: number;
	// Detalle: string | null;
	// DetallePartida: string |null
	// CantidadAcumulada: number
	SKU: string;
	Alfa: string;
	UM: string;
	Envase: string;
	Nombre: string;
	Vals: Record<string, number>;
}
