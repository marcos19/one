import PendientesStock from "./pages/PendientesStock.vue";
import PendienteStock from "./pages/PendienteStock.vue";

import Layout from "@/components/layout/Layout.vue";
import router from "@/router";

router.addRoute("inventarios", {
  path: "/inventarios",
  component: Layout,
  children: [
    {
      path: "pendientes",
      component: PendientesStock,
      name: "pendientesStock"
    },
    {
      path: "pendiente",
      component: PendienteStock,
      name: "pendienteStock"
    },
  ]
});
