import {
	Alicuota,
	Concepto,
	Identificacion,
	CondicionFiscalFacturacion,
} from "./afip";
import axios from "axios";
import eventBus from "@/eventBus";

interface IVAstate {
	alicuotas: Alicuota[];
	identificaciones: Identificacion[];
	condicionesFacturacion: CondicionFiscalFacturacion[];
	conceptos: Concepto[];
}

const state = {
	// Las alícuotas con sus códigos, porcentajes y nombre
	// Datos están recplicados en iva.go
	alicuotas: [] as Alicuota[],

	// ID y nombre. Ej: 80- CUIT
	identificaciones: [] as Identificacion[],

	// ID y nombre. Ej: 1- No Inscripto
	condicionesFacturacion: [] as CondicionFiscalFacturacion[],

	conceptos: [] as Concepto[],
} as IVAstate;

// Condicion facturación

const getters = {
	identificacionPorID: (state: IVAstate) => (id: number) => {
		return state.identificaciones.find((item) => item.ID == id);
	},
	condicionIVAPorID: (state: IVAstate) => (id: string) => {
		return state.condicionesFacturacion.find((item) => item.ID == id);
	},
	condicionesIVAPorIDs: (state: IVAstate) => (ids: string[]) => {
		let cc = state.condicionesFacturacion.filter((item) =>
			ids.includes(item.ID)
		);
		if (!cc) {
			console.log("buscando condiciones de facturación:", cc);
			return [];
		}
		return cc;
	},
	/**
	 * Devuelve todos los conceptos
	 */
	conceptos: (state: IVAstate) => {
		return state.conceptos;
	},
	/**
	 * Devuelve concepto con el ID ingresado
	 */
	conceptoPorID: (state: IVAstate) => (id: string) => {
		return state.conceptos.find((x) => x.ID == id);
	},
	/**
	 * Devuelve conceptos con los id ingreados
	 */
	conceptosPorID: (state: IVAstate) => (ids: string[]) => {
		if (!ids) {
			return [];
		}
		return state.conceptos.filter((x) => ids.includes(x.ID));
	},

	/**
	 * Devuelve todas las alícuotas de IVA
	 */
	alicuotas: (state: IVAstate) => {
		return state.alicuotas;
	},
	/**
	 * Devuelve alícuotas de IVA con el ID ingresado
	 */
	alicuotaPorID:
		(state: IVAstate) =>
		(id: string): Alicuota | undefined => {
			return state.alicuotas.find((x) => x.ID == id);
		},
	/**
	 * Devuelve alícuotas de IVA con los ID ingresados
	 */
	alicuotasPorID:
		(state: IVAstate) =>
		(ids: string[]): Alicuota[] => {
			return state.alicuotas.filter((x) => ids.includes(x.ID));
		},

	alicuotasPorConcepto:
		(state: IVAstate) =>
		(concepto: string): Alicuota[] => {
			console.log("concepto era", concepto);
			if (["1", "2"].includes(concepto)) {
				// Base imponibe o IVA
				return state.alicuotas.filter((x) => x.ID > "2");
			} else if (concepto == "6") {
				// Exento
				return state.alicuotas.filter((x) => x.ID == "2");
			} else if (concepto == "5") {
				// Exento
				return state.alicuotas.filter((x) => x.ID == "1");
			} else {
				//
				return state.alicuotas.filter((x) => x.ID <= "2");
			}
		},
};

const mutations = {
	fijarAlicuotas(state: IVAstate, payload: Alicuota[]) {
		state.alicuotas = payload;
	},
	fijarIdentificaciones(state: IVAstate, payload: Identificacion[]) {
		state.identificaciones = payload;
	},
	fijarCondicionesIVA(
		state: IVAstate,
		payload: CondicionFiscalFacturacion[]
	) {
		state.condicionesFacturacion = payload;
	},
	fijarConceptosIVA(state: IVAstate, payload: Concepto[]) {
		state.conceptos = payload;
	},
};

const actions = {
	pedirAlBackend({ commit }: any) {
		// Alicuotas: No gravado, exento, 0%, 2,5%, 5%, 10,5%, 21%, 27%
		axios
			.post("/backend/afip/alicuotas")
			.then((res) => {
				commit("fijarAlicuotas", res.data, { root: false });
			})
			.catch((err) => {
				eventBus.$emit("error", err);
			});

		// Identificaciones: CUIT, CUIL, CDI, DNI, No identificado
		axios
			.post("/backend/afip/identificaciones")
			.then((res) => {
				commit("fijarIdentificaciones", res.data, { root: false });
			})
			.catch((err) => {
				eventBus.$emit("error", err);
			});

		// Condiciones: CF, RI, MT, EX, NI
		axios
			.post("/backend/afip/condiciones_fiscales_facturacion")
			.then((res) => {
				commit("fijarCondicionesIVA", res.data, { root: false });
			})
			.catch((err) => {
				eventBus.$emit("error", err);
			});

		// Conceptos: Base imponible, IVA, No gravado, Exento, Percepciones, Tributos
		axios
			.post("/backend/afip/conceptos_iva")
			.then((res) => {
				commit("fijarConceptosIVA", res.data, { root: false });
			})
			.catch((err) => {
				eventBus.$emit("error", err);
			});
	},
};
export default {
	namespaced: true,
	state,
	getters,
	mutations,
	actions,
};
