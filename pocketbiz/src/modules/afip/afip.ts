import axios from "axios";

/**
 * CUIT, CUIL, CDI, DNI, No identificado
 */
export interface Identificacion {
  ID: number;
  Nombre: string;
}

/**
 * Base imponible, IVA, No gravado, Exento, Percepciones, Tributos
 */
export interface Concepto {
  ID: string;
  Nombre: string;
}

/**
 *  No gravado, exento, 0%, 2,5%, 5%, 10,5%, 21%, 27%
 */
export interface Alicuota {
  ID: string;
  Nombre: string;
  Porcentaje: number;
}

/**
 *  Son las que se seleccionan al realizar una factura.
 */
export interface CondicionFiscalFacturacion {
  ID: string;
  Nombre: string;
  NombrePlural: string;
}

export function solicitarCAE(opID: string): Promise<any> {
  return new Promise((resolve, reject) => {
    if (!opID) {
      reject("no se ingresó ID de operación a autorizar");
      return;
    }
    const req = async () => {
      try {
        console.log("llamando a POST");
        const resp = await axios.post("/backend/afip/solicitar_cae", {
          OpID: opID,
        });
        resolve(resp);
      } catch (err) {
        reject(err);
      }
    };
    req();
  });
}

interface Cruce {
  puede: boolean;
  msg: string;
}

export enum Emisor {
  LaEmpresa = "1",
  UnTercero = "2",
}
export interface CruceReq {
  letra: string;
  condicionPersona: string;
  condicionEmpresa: string;
  emisor: Emisor;
}

/**
 *
 * @param letra Letra de la factura (A,B,C)
 * @param condPersona al IVA de la persona ("2": "RI")
 * @param condEmpresa frente al IVA de la empresa.
 */
export function cruzarFacturaCondicion(cr: CruceReq): Cruce {
  let condEmpresa = cr.condicionEmpresa;

  switch (condEmpresa) {
    case "Responsable inscripto":
      condEmpresa = "2";
      break;
    case "No inscripto":
      condEmpresa = "1";
  }
  switch (cr.emisor) {
    case Emisor.LaEmpresa:
      return emiteEmpresa(cr.letra, cr.condicionPersona, condEmpresa);
    case Emisor.UnTercero:
      return emiteUnTercero(cr.letra, cr.condicionPersona, condEmpresa);
  }

  return { puede: true, msg: "" };
}

function emiteEmpresa(
  letra: string | null,
  condPersona: string,
  condEmpresa: string
): Cruce {
  // Si la empresa es no inscripta, que no haga comprobantes fiscales
  if (condEmpresa == "1") {
    if (letra && ["A", "B", "C"].includes(letra)) {
      return {
        puede: false,
        msg: "La empresa está configurada como No inscripta. No puede realizar comprobantes fiscales",
      };
    }
    return { puede: true, msg: "" };
  }

  if (condEmpresa == "2") {
    return emiteEmpresaRI(letra, condPersona);
  } else {
    return emiteEmpresaMToExenta(letra, condPersona);
  }
}

function emiteUnTercero(
  letra: string | null,
  condPersona: string,
  condEmpresa: string
): Cruce {
  if (condPersona == "2") {
    return emiteTerceroRI(letra, condEmpresa);
  } else {
    return emiteTerceroMToExento(letra);
  }
}

// Es un comprobante que emite la propia empresa, la cual es responsable inscripta.
function emiteTerceroRI(letra: string | null, condEmpresa: string): Cruce {
  if (letra == "C") {
    // Letra C => No puede ser C
    return {
      puede: false,
      msg: "Un responsable inscripto no puede emitir facturas C",
    };
  }

  // Si la empresa es no inscripta, no hago controles
  if (condEmpresa == "1") {
    return { puede: true, msg: "" };
  }

  const RI = "2";
  if (condEmpresa == RI) {
    switch (letra) {
      case "A":
        // Letra A => OK
        return { puede: true, msg: "" };
      case "B":
        // Letra B => No debería, pero puede ser
        return {
          puede: true,
          msg: "Un responsable inscripto no debería haber emitido factura B a otro responsable inscripto... por favor corrobore",
        };
      default:
        // no controlo
        return { puede: true, msg: "" };
    }
  } else {
    // Si la persona es MT o Exento
    switch (letra) {
      case "A":
        return {
          puede: false,
          msg: "El comprobante debería ser B",
        };

      case "B":
        return { puede: true, msg: "" };

      default:
        // no controlo
        return { puede: true, msg: "" };
    }
  }
}

function emiteTerceroMToExento(letra: string | null): Cruce {
  if (letra == "A" || letra == "B") {
    return {
      puede: false,
      msg: `La persona no está como responsable inscripta, por lo tanto no puede emitir ${letra}`,
    };
  }
  return { puede: true, msg: "" };
}

// Es un comprobante que emite la propia empresa, la cual es responsable inscripta.
function emiteEmpresaRI(letra: string | null, condPersona: string): Cruce {
  if (letra == "C") {
    // Letra C => No puede ser C
    return {
      puede: false,
      msg: "Un responsable inscripto no puede emitir facturas C",
    };
  }

  const RI = "2";
  if (condPersona == RI) {
    switch (letra) {
      case "A":
        // Letra A => OK
        return { puede: true, msg: "" };
      case "B":
        // Letra B => No debería, pero puede ser
        return {
          puede: true,
          msg: "Un responsable inscripto no debería haber emitido factura B... corrobore",
        };
      default:
        // no controlo
        return { puede: true, msg: "" };
    }
  } else {
    // Si la persona es MT o Exento
    switch (letra) {
      case "A":
        return {
          puede: false,
          msg: `Para ser A, la persona debe ser responsable inscripta, no puede ser ${nombreCond(
            condPersona
          )}`,
        };

      case "B":
        // Letra C => No puede ser C
        return { puede: true, msg: "" };

      default:
        // no controlo
        return { puede: true, msg: "" };
    }
  }
}

function emiteEmpresaMToExenta(
  letra: string | null,
  condPersona: string
): Cruce {
  // Controlo
  if (letra == "A" || letra == "B") {
    return {
      puede: false,
      msg: `No puede emitir facturas ${letra} a una persona ${nombreCond(
        condPersona
      )}`,
    };
  }

  return { puede: true, msg: "" };
}

// Devuelve el nombre de la condición en base a su ID.
// Son los valores definidos en condiciones_fiscales.go
function nombreCond(cond: string): string {
  switch (cond) {
    case "1":
      return "No Inscripto";
    case "2":
      return "Responsable Inscripto";
    case "4":
      return "Exento";
    case "5":
      return "Consumidor Final";
    case "6":
      return "Monotributista";
    case "8":
      return "Proveedor del Exterior";
    case "9":
      return "Cliente del Exterior";
    case "10":
      return "IVA Liberado";
    case "13":
      return "Monotributo social";
    case "15":
      return "IVA no alcanzado";
  }
  return "N/D";
}
