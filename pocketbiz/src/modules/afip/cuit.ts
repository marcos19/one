//* Devuelve el CUIT con los guiones
export function conGuiones(str: string): string {
  if (!str) {
    return "";
  }
  if (validarCUIT(str) == false) {
    return `CUIT INVÁLIDO: ${str}`;
  }
  let primer = str.substring(0, 2);
  let dni = str.substr(2, 8);
  let digit = str.substr(10, 11);
  return [primer, dni, digit].join("-");
}

export function sinGuiones(str: string): string {
  if (!str) {
    return "";
  }
  str = str.replace("-", "");
  str = str.replace("-", "");
  return str;
}

//* Corrobora el CUIT utilizando el algoritmo de AFIP
export function validarCUIT(cuit: string): boolean {
  cuit = cuit.replace("-", "");
  cuit = cuit.replace("-", "");
  if (cuit.length != 11) {
    return false;
  }
  let partesCuit: number[] = [];
  for (let v of cuit) {
    let digito = parseInt(v);
    if (Number.isNaN(digito)) {
      return false;
    } else {
      partesCuit.push(digito);
    }
  }
  let digits: number[] = [5, 4, 3, 2, 7, 6, 5, 4, 3, 2, 1];

  let suma = 0;
  for (let i = 0; i < 11; i++) {
    suma += partesCuit[i] * digits[i];
  }

  if (suma % 11 == 0) {
    return true;
  }
  return false;
}
