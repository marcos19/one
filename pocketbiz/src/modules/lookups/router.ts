import Permisos from "@/modules/lookups/pages/Permisos.vue";
import Permiso from "@/modules/lookups/pages/Permiso.vue";

import Layout from "@/components/layout/Layout.vue";
import router from "@/router";

router.addRoute("permisos", {
  path: "/permisos",
  component: Layout,
  children: [
    {
      path: "permisos",
      component: Permisos,
      name: "permisos"
    },
    {
      path: "permiso",
      component: Permiso,
      name: "permiso"
    }
  ]
});
