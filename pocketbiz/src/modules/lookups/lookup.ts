export interface Lookup {
  ID: string | null;
  Usuario: string | null;
  GrupoUsuarios: string | null;
  Para: string;
  Int: string | null;
  String: string | null;
  Detalle: string;
}

export interface LookupMany {
  ID: string | null;
  Usuario: string | null;
  GrupoUsuarios: string | null;
  Para: string;
  Int: string | null;
  String: string | null;
  Detalle: string;

  NombreGrupo?: string;
  NombreUsuario?: string ;
  ApellidoUsuario?: string ;
}
