import { Unicidad } from "@/modules/unicidades/unicidades";
import { CuentaAsociada, Sucursal } from "@/modules/personas/personas";
import { Op, TotalConcepto } from "@/modules/ops/interfaces";

export interface Fact {
  ID: string | null;
  Empresa: string;
  Config: string | null;
  Esquema: string | null;
  Fecha: string | null;
  Persona: Persona | null;
  CondicionPago: string | null;
  Detalle: string;
  Productos: Producto[];
  PartidasDirectas: PartidaDirecta[];
  Valores: Valor[];
  Aplicaciones: AplicacionPartida[];
  AplicacionesEstaFactura: AplicacionPartida[];
  AnticipoCuenta: string | null;
  AnticipoCentro: string | null;
  AnticipoMonto: number | null;
  AplicacionesProductos: AplicacionProducto[];
  Deposito: string | null;
  // Caja: string;
  ReingresoMonto: number | null;
  ReingresoPagoCuenta: string | null;
  ReingresoPagoPersona: string | null;
  ReingresoPagoSucursal: string | null;
  ReingresoPagoCaja: string | null;
  TotalesConceptos: TotalConcepto[];
  Vencimientos: Vencimiento[];
  VencimientosPago: Vencimiento[];
  VencimientosManuales?: boolean;

  ImputacionDesde: string | null;

  Usuario: string;
  CreatedAt: string | null;

  Factura: Op | null;
  Recibo: Op | null;
  Anulacion: Op | null;
  Reingreso: Op | null;
  Diferimientos: Op[];
}

export interface AplicacionProducto {
  PartidaAplicada: string; // ID partida aplicada
  SKU: string;
  Cuenta: string;
  Codigo: string;
  UM: string;
  Envase: string;
  Nombre: string;
  CantidadDisponible: number;
  MontoDisponible: number;
  Cantidad: number;
  Monto: number;
  Fecha: string;
  CompNombre: string;
  NCompStr: string;

  Precio: number;
}

export interface Aplicaciones {
  aplicaciones: AplicacionPartida[];
  aplicacionesEstaFactura: AplicacionPartida[];
  anticipoCuenta: string | null;
  anticipoCentro: string | null;
  anticipoMonto: number;
}

export interface AplicacionPartida {
  ID: string | null;
  FechaContable: string;
  FechaFinanciera: string;
  TipoComp: string;
  NComp: string;
  Detalle: string | null;
  Cuenta: string;
  Centro: string;
  MontoOriginal: number | null;
  MontoPendiente: number;
  Dias: number;

  // Este es un número ABSOLUTO. El signo depende del signo de MontoOriginal
  MontoAplicado: number;
  // MontoAplicado con el sigo que va
  Aplicacion: number;
}

// Son los códigos definidos por AFIP frente a IVA
export interface CondicionIVA {
  ID: bigint;
  Nombre: string;
}

// Renglon de producto de fact
export interface Producto {
  Producto: string;
  SKU: string;
  Codigo: string;
  ProductoNombre: string;
  ProductoImpresion: string;
  ProductoDetalle: string;
  Unicidades: any[];
  UnicidadesNuevas: Unicidad[];
  UM: string;
  UMNombre: string;
  Cantidad: number;
  PrecioUnitario: number;
  PorcentajeRecargo: number;
  PrecioUnitarioFinal: number;
  AlicuotaIVA: string;
  MontoTotal: number;
  ListaPrecios: string | null;
}
export interface PartidaDirecta {
  CuentaID: string | null;
  CuentaNombre: string | null;
  Centro: string | null;
  Persona: string | null;
  Sucursal: string | null;
  Concepto: string | null;
  AlicuotaIVA: string | null;
  Monto: number | null;
  CuentaPatrimonial: string | null;
  CentroPatrimonial: string | null;
  Detalle: string;
}

// Es la DTO que se graba en Fact
export interface Persona {
  ID: string;
  Nombre: string;
  TipoIdentificacionFiscal: string;
  NIdentificacionFiscal: string;
  Domicilio: string;
  CondicionFiscal: string;
  Sucursal: string | null;
  NombreSucursal: string | null;
}

//
export interface PersonaLookup {
  ID: string;
  Nombre: string;
  Tipo: string;
  CondicionIVA: string;
  CondicionMT: string;
  UsaSucursales: boolean;
  Sucursales: Array<Sucursal>;
  CuentasAsociadas: Array<CuentaAsociada>;
}

// Valor es la partida que va en PartidaValores de fact
export interface Valor {
  Cuenta: string;
  Caja: string | null;
  Persona: string | null;
  Sucursal: string | null;
  Centro: string | null;
  UnicidadNueva: Unicidad | null;

  TipoUnicidad: string | null;
  UnicidadRef: string | null;

  Detalle: string;
  Alias: string;
  Monto: number | null;
  Vencimiento: string;

  Moneda: string | null;
  TC: number | null;
  MontoMonedaOriginal: number | null;
}

export interface Vencimiento {
  id: string;
  Fecha: string;
  Dias: number;
  Monto: number;
}

export interface ReingresoDTO {
  // Si es false significa que NO HAY reingreso.
  // Es distinto a que sea TRUE y haya MONTO.
  HayMonto: boolean;
  Monto: number;
  Cuenta: string | null;
  Persona: string | null;
  Sucursal: string | null;
  Caja: string | null;
}
