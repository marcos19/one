import { Cuenta, traerHijas } from "@/interfaces/cuentas";
import { PartidaDirectaConfig } from "@/modules/fact_esquemas/interfaces";

//* Devuelve las cuentas permitidas por la config
export function cuentasDisponibles(
  cc: Cuenta[],
  pp: PartidaDirectaConfig[]
): Cuenta[] {
  const out: Cuenta[] = [];
  for (const v of pp) {
    if (!cc) {
      return [];
    }
    let hijas = traerHijas(cc, v.Cuenta as string);
    if (!hijas.length) {
      let mismaCuenta = cc.find((x) => x.ID == v.Cuenta);
      if (!mismaCuenta) {
        continue;
      }
      hijas = [mismaCuenta];
    }

    for (let hija of hijas) {
      const estaba = out.find((x) => x.ID == hija.ID);
      if (!estaba) {
        out.push(hija);
      }
    }
  }

  out.sort((a, b) => {
    return new Intl.Collator("ar").compare(a.Nombre, b.Nombre);
  });
  return out;
}

//*
// Una config define que cuentas puede utilizar en los renglones.
// Además, para cada cuenta (o grupo) se define los conceptos y alícuotas.
// Es necesario que cada vez que se
export function conceptosDisponibles(
  cta: string,
  cuentas: Cuenta[],
  permisos: PartidaDirectaConfig[]
): string[] {
  const out = new Set<string>();

  // Aplano los grupos
  for (const v of permisos) {
    const found = cuentas.find((x) => x.ID == v.Cuenta);
    if (!found) {
      continue;
    }

    // Busco las que están debajo
    let debajo = traerHijas(cuentas, found.ID as string);
    if (!debajo.length) {
      debajo = [found];
    }

    const ctaFound = debajo.find((x) => x.ID == cta);
    if (!ctaFound) {
      continue;
    }

    // La cuenta que se estaba buscando se econtró como hija
    // de la definida en el permiso
    for (const conc of v.Conceptos) {
      out.add(conc);
    }
  }

  return Array.from(out);
}
//*
// Una config define que cuentas puede utilizar en los renglones.
// Además, para cada cuenta (o grupo) se define los conceptos y alícuotas.
// Es necesario que cada vez que se
export function alicuotasDisponibles(
  cta: string,
  concepto: string,
  cuentas: Cuenta[],
  permisos: PartidaDirectaConfig[]
): string[] {
  const out = new Set<string>();

  // Aplano los grupos
  for (const v of permisos) {
    const found = cuentas.find((x) => x.ID == v.Cuenta);
    if (!found) {
      continue;
    }

    // Busco las que están debajo
    let debajo = traerHijas(cuentas, found.ID as string);
    if (!debajo.length) {
      debajo = [found];
    }

    const ctaFound = debajo.find((x) => x.ID == cta);
    if (!ctaFound) {
      continue;
    }

    // La cuenta que se estaba buscando se econtró como hija
    // de la definida en el permiso
    for (const conc of v.Conceptos) {
      if (conc != concepto) {
        continue;
      }
      for (const alic of v.Alicuotas) {
        out.add(alic);
      }
    }
  }

  return Array.from(out);
}

//*
// Una config define que cuentas puede utilizar en los renglones.
// Además, para cada cuenta (o grupo) se define los conceptos y alícuotas.
// Es necesario que cada vez que se
export function patrimonialesDisponibles(
  cta: string,
  cuentas: Cuenta[],
  permisos: PartidaDirectaConfig[]
): string[] {
  const out = new Set<string>();

  // Aplano los grupos
  for (const v of permisos) {
    const found = cuentas.find((x) => x.ID == v.Cuenta);
    if (!found) {
      continue;
    }

    // Busco las que están debajo
    let debajo = traerHijas(cuentas, found.ID as string);
    if (!debajo.length) {
      debajo = [found];
    }

    const ctaFound = debajo.find((x) => x.ID == cta);
    if (!ctaFound) {
      continue;
    }

    // La cuenta que se estaba buscando se econtró como hija
    // de la definida en el permiso
    if (!v.ContraCuenta) {
      continue;
    }
    out.add(v.ContraCuenta);
  }

  return Array.from(out);
}
