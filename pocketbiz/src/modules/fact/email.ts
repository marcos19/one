export interface EmailReq {
	From: string;
	Subject: string;
	To: string[];
	Body: string;
	AttachmentsNames: string[];
}
