
import Apertura from "./pages/Apertura.vue";

import Layout from "@/components/layout/Layout.vue";
import router from "@/router";

router.addRoute("apertura", {
  path: "/apertura",
  component: Layout,
  children: [
    {
      path: "/apertura",
      component: Apertura,
      name: "asientoApertura"
    }
  ]
});
