export interface Condicion {
  ID: string;
  Nombre: string;
  Modalidad: string;
  CantidadCuotas: number;
  Cuotas: Cuota[];
  Modificable: boolean;

  ValidoDesde: boolean;
  ValidoHasta: boolean;
  CreatedAt: string | null;
  UpdatedAt: string | null;
}

interface Cuota {
  Numero: number;
  Dias: number;
  FechaFija: string | null;
  Porcentaje: number;
}
