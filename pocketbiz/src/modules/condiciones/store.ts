import axios from "axios";
import eventBus from "@/eventBus";
import { Condicion } from "./condicion";
import store from "@/store/store";
import { newDate } from "@/modules/util/fechas";
import { addDays, format } from "date-fns";
import { Vencimiento } from "@/modules/fact/interfaces";

const state = {
  array: [] as Condicion[],
} as State;

interface State {
  array: Condicion[];
}

const getters = {
  porID:
    (state: State) =>
    (id: string): Condicion | null => {
      let found = state.array.find((item) => item.ID == id);
      if (!found) {
        return null;
      }
      return found;
    },

  // Si ids == null => trae todos
  porIDs: (state: State) => (ids: string[]) => {
    if (!ids) {
      return state.array;
    }

    let out: Condicion[] = state.array.filter((item) => ids.includes(item.ID));
    return out.sort((a: Condicion, b: Condicion) => {
      return new Intl.Collator("ar").compare(a.Nombre, b.Nombre);
    });
  },
  todas: (state: State) => {
    return state.array;
  },
  // Crea el listado de vencimientos
  vencimientos:
    (state: State) =>
    (condicionID: string, fecha: string, monto: number): Vencimiento[] => {
      // Busco la condición
      console.log("generando vencimientos", condicionID, fecha, monto);
      let cond = state.array.find((item) => item.ID == condicionID);
      if (!cond) {
        eventBus.$emit(
          "error",
          Error("No se pudo encontrar la condición de pago " + condicionID)
        );
        return [];
      }
      let vtos = [] as Vencimiento[];
      let sum = 0;
      for (let v of cond.Cuotas) {
        let vto = {
          Monto: Math.round((v.Porcentaje / 100) * monto * 100) / 100,
        } as Vencimiento;
        sum += vto.Monto;
        // Le pongo los días y calculo la fecha
        if (cond.Modalidad == "Días") {
          vto.Dias = v.Dias;
          let fechaMoment = newDate(fecha);
          // console.log("Sumando", vto.Dias, " a ", fechaMoment.toString());
          vto.Fecha = format(
            addDays(fechaMoment, Number(vto.Dias)),
            "yyyy-MM-dd"
          );
        }
        vtos.push(vto);
      }

      // Si no me da exacto sumo la diferencia a la última cuota
      let falta = monto - sum;
      vtos[vtos.length - 1].Monto += falta;
      return vtos;
    },
};

const mutations = {
  fijarCondiciones(state: State, payload: any) {
    state.array = payload.sort((a: Condicion, b: Condicion) => {
      return new Intl.Collator("ar").compare(a.Nombre, b.Nombre);
    });
  },
};

const actions = {
  pedirAlBackend({ commit }: any) {
    axios
      .post("/backend/condiciones/many", {})
      .then((res) => {
        commit("fijarCondiciones", res.data, { root: false });
      })
      .catch((err) => {
        eventBus.$emit("error", err);
      });
  },
};

store.registerModule("condiciones", {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
});
