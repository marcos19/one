
import Condicion from "./pages/Condicion.vue";
import Condiciones from "./pages/Condiciones.vue";

import Layout from "@/components/layout/Layout.vue";
import router from "@/router";

router.addRoute("condiciones", {
  path: "/condiciones",
  component: Layout,
  children: [
    {
      path: "/condiciones",
      component: Condiciones,
      name: "condiciones"
    },
    {
      path: "/condicion",
      component: Condicion,
      name: "condicion"
    }
  ]
});
