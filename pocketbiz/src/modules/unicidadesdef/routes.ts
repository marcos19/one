import UnicidadesDef from "./pages/UnicidadesDef.vue";
import UnicidadDef from "./pages/UnicidadDef.vue";

import Layout from "@/components/layout/Layout.vue";
import router from "@/router";

router.addRoute("unicidadesdef", {
  path: "/unicidadesdef",
  component: Layout,
  children: [
    {
      path: "/unicidades_def",
      component: UnicidadesDef,
      name: "unicidadesDef"
    },
    {
      path: "/unicidad_def",
      component: UnicidadDef,
      name: "unicidadDef"
    }
  ]
});
