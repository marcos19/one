import { UnicidadDef } from "./def";
import axios from "axios";
import eventBus from "@/eventBus";

export interface State {
  array: UnicidadDef[];
}
const state = {
  array: []
} as State;

const getters = {
  todas: (state: State) => () => {
	return state.array
  },
  porID: (state: State) => (id: string) => {
    return state.array.find(item => item.ID == id);
  },
  deUso: (state: State) => {
    return state.array.filter(item => item.Tipo == "Uso");
  },
  deReferencia: (state: State) => {
    return state.array.filter(item => item.Tipo == "Referencial");
  }
};

const mutations = {
  fijarUnicidades(state: State, payload: UnicidadDef[]) {
    state.array = payload;
  }
};

const actions = {
  pedirAlBackend({ commit }: any) {
    axios
      .post("/backend/unicidadesdef/para_store", {})
      .then(res => {
        commit("fijarUnicidades", res.data, { root: false });
      })
      .catch(err => {
        eventBus.$emit("error", err);
      });
  }
};
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
};
