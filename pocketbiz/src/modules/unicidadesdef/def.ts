export interface UnicidadDef {
  ID: string;
  Predefinida: string;
  Nombre: string;
  NombreSingular: string;
  Genero: string;
  Tipo: string;
  AttsIndexados: Att[];
  Atts: Att[];
  Funcion: string;
}

export interface Att {
  Nombre: string;
  Type: string;
  Obligatorio: boolean;
  IncluirEnDetalle: number;
  IncluirKeyEnDetalle: boolean;
  Funcion: string;
}
