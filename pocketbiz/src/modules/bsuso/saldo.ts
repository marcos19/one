export interface Saldo {
	Unicidad: string;
	Detalle: string;
	FechaAlta: string;
	FechaInicioAmort: string | null;
	CuentaValorOrigen: string;
	ValorOrigen: number;
	VidaUtil: number;
	CuentaAmorAcum: string;
	ValorAmortAcum: number;
	VidaUtilAcum: number;
}

export interface Avance {
	UM: string;
	Cantidad: number;
}
