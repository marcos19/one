import Saldos from "./pages/Saldos.vue"
import Amortizar from "./pages/Amortizar.vue"
import AjustarInflacion from "./pages/AjustarInflacion.vue"

import Layout from "@/components/layout/Layout.vue";
import router from "@/router";

router.addRoute("bsuso", {
  path: "/bsuso",
  component: Layout,
  children: [
    {
      path: "saldos",
      component: Saldos,
      name: "bsUsoSaldos"
    },
    {
      path: "amortizar",
      component: Amortizar,
      name: "amortizar"
    },
    {
      path: "ajustar_inflacion",
      component: AjustarInflacion,
      name: "bsUsoAjustarInflacion"
    },
  ]
});
