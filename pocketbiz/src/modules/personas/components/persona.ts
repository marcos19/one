export interface Persona {
	ID: string;
	Nombre: string;
	NombreFantasia: string;
	DNI: string;
	CUIT: string;
	Calle: string;
	Numero: string;
	Piso: string;
	Depto: string;
	CiudadNombre: string;
	ProvinciaNombre: string;
	Telefonos: Telefono[];
	Activo: boolean;
	UsaSucursales: boolean;
	CuentasAsociadas: boolean;
}
