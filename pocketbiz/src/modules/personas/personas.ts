export interface Persona {
  ID: string | null;
  Nombre: string;
  NombreFantasia: string;
  TipoCUIT: string | null;
  CUIT: string | null;
  TipoDNI: string | null;
  DNI: string | null;
  ProvinciaID: number;
  ProvinciaNombre: string;
  CiudadID: number;
  CiudadCP: number;
  CiudadNombre: string;
  Calle: string;
  Numero: string;
  Piso: string;
  Depto: string;
  Tipo: string;
  CondicionIVA: string | null;
  CondicionGcias: string | null;
  CondicionMT: string;
  UsaSucursales: boolean;
  Sucursales: Array<Sucursal>;
  CuentasAsociadas: Array<CuentaAsociada>;
  Telefonos: Telefono[];
  Emails: Email[];
  Domicilios: Domicilio[];
  Activo: boolean;
  MotivoInactividad: string;
  SaldoContable: number | null | "";
  SaldoFinanciero: number | null | "";
}

export interface Domicilio {
  ID: string;
  Tipo: string;
  DomCalle: string;
  DomNumero: string;
  DomPiso: string;
  DomDpto: string;
  CiudadID: number;
  CiudadCP: number;
  CiudadNombre: string;
  ProvinciaID: number;
  ProvinciaNombre: string;
}

// En base a los datos que tiene registrados la persona, crea
// un string que es el que se pega en la factura
export function domicilioString(persona: Persona) {
  let calle = persona.Calle;
  let numero = persona.Numero;
  if (!numero) {
    numero = "SN";
  }
  let piso = persona.Piso;
  if (piso != "") {
    piso = " " + piso;
  }

  let dpto = persona.Depto;
  if (dpto != "") {
    piso = " " + piso;
  }

  return (
    calle +
    " " +
    numero +
    piso +
    dpto +
    " - " +
    persona.CiudadNombre +
    " (" +
    persona.CiudadCP +
    ") " +
    persona.ProvinciaNombre
  );
}

export interface Sucursal {
  ID: string | null;
  PersonaID: string | null;
  Nombre: string;
  NSucursal: number | null;
  CuentasAsociadas: Array<CuentaAsociada>;
  Telefonos: Telefono[];
  Emails: Email[];
  Domicilios: Domicilio[];
  ProvinciaID: number;
  ProvinciaNombre: string;
  CiudadID: number;
  CiudadCP: number;
  CiudadNombre: string;
  Calle: string;
  Numero: string;
  Piso: string;
  Depto: string;
  Tipo: string;
  Activo: boolean;
  MotivoInactividad: string;
  Empresa: string | null;
}

export interface CuentaAsociada {
  Cuenta: string;
  Persona: string;
  Sucursal: string | null;
}

export interface Email {
  Descripcion: string;
  Email: string;
}

export interface Telefono {
  Tipo: string;
  Area: number;
  Numero: number;
  NumeroCompleto: string;
  Detalle: string;
}

export interface Saldo {
  ID: string;
  Nombre: string;
  SaldoContable: number;
  SaldoFinanciero: number;
  Provincia: string;
  Ciudad: string;
  Domicilio: string;
}

export interface ICuenta {
  ID: string | null;
  AperturaPersonaRestrictiva: boolean;
}

export interface IPersona {
  CuentasAsociadas: CuentaAsociada[];
}

export interface ICuentaAsociada {
  Cuenta: string;
}

export interface ISucursal {
  ID: string | null;
  Empresa: string | null;
  CuentasAsociadas: ICuentaAsociada[];
}

//*
// Al hacer una operación, cuando se selecciona una persona es necesario determinar
// cuales sucursales tienen habilitadas las cuentas. Ej: BANCO NACION tiene
// como sucursales CTA 123/1 y Tarjeta Agronacion. Si elijo un pago con tarjeta
// no quiero que me sugiera CTA 123/1.
// Esta función hace ese cruce.
export function cruzarCuentasYSucursales(
  emp: string,
  p: IPersona,
  ss: ISucursal[],
  cc: ICuenta[]
): ISucursal[] {
  if (!p) {
    return [];
  }
  if (!ss) {
    return [];
  }
  if (!ss.length) {
    return [];
  }
  if (!cc) {
    return [];
  }

  // Si la sucursal tiene definida alguna empresa
  let out: ISucursal[] = ss.filter((x) => !x.Empresa || x.Empresa == emp);

  // Si alguna cuenta es abierta => devuelvo todas las sucursales
  for (let v of cc) {
    if (!v.AperturaPersonaRestrictiva) {
      return out;
    }
  }

  // Son todas cuentas restrictivas. La general la tiene? => Todas
  let idsCtas: string[] = cc.map((x) => x.ID as string);
  let idsPers: string[] = p.CuentasAsociadas.map((x) => x.Cuenta);
  let intersect = idsPers.filter((x) => idsCtas.includes(x));
  if (intersect.length) {
    return out;
  }

  // Muestro las sucursales que las tengan solamente
  let nuevasOut: ISucursal[] = [];
  for (let v of out) {
    let deSucursal = v.CuentasAsociadas.map((x) => x.Cuenta);
    let intersect = idsCtas.filter((x) => deSucursal.includes(x));
    if (intersect.length) {
      nuevasOut.push(v);
    }
  }
  return nuevasOut;
}
