import axios from "axios";
import eventBus from "@/eventBus";

const state = {};

const mutations = {
  fijarAlicuotas(state, payload) {
    state.array = payload;
  }
};

const actions = {
  pedirAlBackend({ commit }) {
    axios
      .post("/backend/afip/alicuotas")
      .then(res => {
        commit("fijarAlicuotas", res.data, { root: false });
      })
      .catch(err => {
        eventBus.$emit("error", err);
      });
  }
};
export default {
  namespaced: true,
  state,
  mutations,
  actions
};
