import Personas from "@/modules/personas/pages/Personas.vue";
import Persona from "@/modules/personas/pages/Persona.vue";
import Sucursal from "@/modules/personas/pages/Sucursal.vue";
import VistaPersona from "@/modules/personas/pages/VistaPersona.vue";
import Saldos from "@/modules/personas/pages/Saldos.vue";

import Layout from "@/components/layout/Layout.vue";
import router from "@/router";

router.addRoute("personas", {
  path: "/personas",
  component: Layout,
  children: [
    {
      path: "/personas",
      component: Personas,
      name: "personas"
    },
    {
      path: "/persona",
      component: Persona,
      name: "persona"
    },
    {
      path: "/personas/nueva",
      component: Persona,
      name: "personaNueva"
    },
    {
      path: "/sucursal",
      component: Sucursal,
      name: "sucursal"
    },
    {
      path: "/vista_persona",
      component: VistaPersona,
      name: "vistaPersona"
    },
    {
      path: "/saldos_personas",
      component: Saldos,
      name: "saldosPersonas"
    }
  ]
});
