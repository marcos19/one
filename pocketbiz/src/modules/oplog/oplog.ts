export interface Oplog {
  OpID: string;
  CompID: string;
  PuntoDeVenta: string;
  NComp: string;
  Persona: string;
  TS: string;
  Usuario: string;
  Operacion: string;
  Detalle: string;
}
interface TipoOperacion {
  ID: string;
  Nombre: string;
}
interface State {
  tiposOperacion: TipoOperacion[];
}

let tipos = [
  {
    ID: "1",
    Nombre: "Creación"
  },
  {
    ID: "2",
    Nombre: "Lectura"
  },
  {
    ID: "3",
    Nombre: "Modificación"
  },
  {
    ID: "4",
    Nombre: "Eliminación"
  },
  {
    ID: "5",
    Nombre: "Solicitud CAE"
  },
  {
    ID: "6",
    Nombre: "Envío mail"
  }
];

const state = {
  tiposOperacion: [] as TipoOperacion[]
};
const mutations = {
  fijarTipos(state: State, payload: TipoOperacion[]) {
    state.tiposOperacion = payload;
  }
};
const getters = {
  porID: (state: State) => (id: string): TipoOperacion | null => {
    let found = state.tiposOperacion.find(x => x.ID == id);
    if (!found) {
      return null;
    }
    return found;
  }
};

import store from "@/store/store";
store.registerModule("oplog", {
  namespaced: true,
  state,
  mutations,
  getters
});

store.commit("oplog/fijarTipos", tipos);
