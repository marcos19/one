import PorComprobante from "./components/PorComprobante.vue";
import router from "@/router";
import Layout from "@/components/layout/Layout.vue";

router.addRoute("oplog", {
  path: "/oplog",
  component: Layout,
  children: [
    {
      path: "por_comprobante",
      name: "porComprobante",
      component: PorComprobante
    }
  ]
});
