import { UnicidadDef, Att } from "@/modules/unicidadesdef/def";
import { enFecha, enPesos } from "@/modules/util/format";

export interface Unicidad {
  ID: string;
  Definicion: string;
  // "Fecha diferida": string;
  // Monto: number;
  Atts: Record<string, any>;
  AttsIndexados: Record<string, any>;
  CreatedAt: string | null;
}

export interface UnicidadConSaldo {
  ID: string;
  Definicion: string;
  // "Fecha diferida": string;
  Saldo: number;
  Atts: any;
  AttsIndexados: any;
  Seleccionado: boolean;
}

// En base a la definición, arma un string con los atributos que tiene
// seleccionados
export function unicidadString(unicidad: Unicidad| UnicidadConSaldo, def: UnicidadDef): string {
  // Pego unicidades
  if (!unicidad) {
    return "";
  }

  // Determino los campos que van
  let campos = [] as Att[];
  if (def.AttsIndexados) {
    campos.push(...def.AttsIndexados);
  }
  if (def.Atts) {
    campos.push(...def.Atts);
  }

  let van = campos.filter(item => {
    return item.IncluirEnDetalle > 0;
  });

  // Los ordeno
  van.sort((a, b) => {
    return a.IncluirEnDetalle - b.IncluirEnDetalle;
  });

  // Armo el string
  let vals: string[] = [];
  for (let att of van) {
    let key = "";
    if (att.IncluirKeyEnDetalle) {
      key = att.Nombre;
    }
    if (unicidad.AttsIndexados) {
      let val = unicidad.AttsIndexados[att.Nombre];
      if (!val) {
        // Era un atributo sin indexar
        if (unicidad.Atts) {
          val = unicidad.Atts[att.Nombre];
        }
      }
      if (att.Type == "fecha") {
        val = enFecha(val);
      } else if (att.Type == "d2") {
        val = enPesos(val);
      }
      if (key) {
        val = [key, val].join(": ");
      }
      if (val) {
        vals.push(val);
      }
    }
  }
  return vals.join(" | ");
}
