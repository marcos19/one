// import SaldosUnicidades from "./pages/SaldosUnicidades.vue";
import Unicidades from "./pages/Unicidades.vue";
// import Unicidad from "./components/Unicidad.vue";

import Layout from "@/components/layout/Layout.vue";
import router from "@/router";

router.addRoute("unicidades", {
  path: "/unicidades",
  component: Layout,
  children: [
    {
      path: "/unicidades",
      component: Unicidades,
      name: "unicidades"
    }
    // {
    //   path: "/unicidad",
    //   component: Unicidad,
    //   name: "unicidad"
    // }
  ]
});
