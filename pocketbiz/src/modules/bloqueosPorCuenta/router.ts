import Bloqueos from "./pages/Bloqueos.vue";
import Bloqueo from "./pages/Bloqueo.vue";
import router from "@/router";
import Layout from "@/components/layout/Layout.vue";

router.addRoute("opsbloqcta", {
  path: "/opsbloqcta",
  component: Layout,
  children: [
    {
      path: "bloqueos",
      name: "bloqueosPorCuenta",
      component: Bloqueos
    },

    {
      path: "bloqueo",
      name: "bloqueoPorCuenta",
      component: Bloqueo
    }
  ]
});
