export interface Bloqueo {
  Empresa: string | null;
  Cuenta: string | null;
  Desde: string;
  Hasta: string;
}
