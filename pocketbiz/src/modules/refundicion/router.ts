
import Refundicion from "./pages/Refundicion.vue";

import Layout from "@/components/layout/Layout.vue";
import router from "@/router";

router.addRoute("refundicion", {
  path: "/refundicion",
  component: Layout,
  children: [
    {
      path: "/asiento_refundicion",
      component: Refundicion,
      name: "asientoRefundicion"
    }
  ]
});
