import Rendicion from "./pages/Rendicion.vue";
import Configs from "./pages/Configs.vue";
import Config from "./pages/Config.vue";

import Layout from "@/components/layout/Layout.vue";
import router from "@/router";

router.addRoute("rendiciones", {
  path: "/rendiciones",
  component: Layout,
  children: [
    {
      path: "rendicion_de_caja",
      component: Rendicion,
      name: "rendicionCaja"
    },
    {
      path: "configs",
      component: Configs,
      name: "rendicionesConfigs"
    },
    {
      path: "config",
      component: Config,
      name: "rendicionConfig"
    }
  ]
});
