import { Op } from "@/modules/ops/interfaces";

export interface Rendicion {
  ID: string | null;
  Empresa: string;
  Config: string;
  Fecha: string;
  Detalle: string;
  DetalleManual: string;
  Valores: Partida[];
  CajaDesde: string;
  CajaHacia: string;
  Monto: number;
  Usuario: string;
  Op: Op | null;
}
export interface Saldo {
  Cuenta: string;
  Monto: number;
  Cantidad: number;
}

export interface Partida {
  Cuenta: string;
  Alias: string;
  Detalle: string;
  TipoUnicidad: string | null;
  UnicidadRef: string | null;
  Monto: number;
  Vencimento: string;
}

export class Config {
  ID: string | null;
  Empresa: string | null;
  Nombre: string;
  Comp: string | null;
  CajasDesde: string[];
  CajasHacia: string[];

  constructor() {
    this.ID = null;
    this.Empresa = null;
    this.Nombre = "";
    this.Comp = null;
    this.CajasDesde = [];
    this.CajasHacia = [];
  }
}
