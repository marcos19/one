module.exports = {
  productionSourceMap: false,
  lintOnSave: false,
  devServer: {
    overlay: {
      warnings: false,
      errors: false,
    },
    proxy: {
      "/backend/": {
        target: "http://localhost:9040",
      },
      "/backend/ws": {
        target: "ws://localhost:9040",
      },
    },
  },
  // css: {
  //   loaderOptions: {
  //     sass: {
  //       additionalData: `@import "@/assets/variables.scss"`
  //     }
  //   }
  // }
};
