import BuscadorProductos from "@/components/widgets/BuscadorProductos.vue";
import { createLocalVue, mount } from "@vue/test-utils";
// import store from "@/store/store.js";
// import Vuex from "vuex";

// localVue.use(Vuex);
import Buefy from "buefy";
const localVue = createLocalVue();
localVue.use(Buefy);

describe("Buscador de productos", () => {
  jest.mock("axios", () => ({
    post: () =>
      Promise.resolve([
        {
          ID: "1",
          Nombre: "Indumentaria",
          Cantidad: 8
        },
        {
          ID: "2",
          Nombre: "Electrodomésticos",
          Cantidad: 10
        }
      ])
  }));

  test("había categorías de productos", async () => {
    const wrapper = mount(BuscadorProductos, { localVue });
    wrapper.vm.$nextTick(() => {
      let ul = wrapper.findComponent({ ref: "cats" });
      console.log("UL ENcontrado", wrapper.html());
    });
  });
});
