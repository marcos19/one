import {
  ICuenta,
  IPersona,
  ISucursal,
  cruzarCuentasYSucursales
} from "@/modules/personas/personas";

describe("Cruzar personas, sucursales y cuentas", () => {
  const e1 = "CrossLogic SA";
  const e2 = "Testing";

  // Cuentas
  const ctaCte: ICuenta = {
    ID: "Cta Cte ARS",
    AperturaPersonaRestrictiva: true
  };
  const tarjetas: ICuenta = {
    ID: "Tarjetas",
    AperturaPersonaRestrictiva: true
  };
  const clientes: ICuenta = {
    ID: "Clientes",
    AperturaPersonaRestrictiva: false
  };


  // Persona
  const p: IPersona = {
    CuentasAsociadas: [
      // {
      //   Cuenta: "Cta Cte ARS"
      // }
    ]
  };

  // Sucursales
  const s1: ISucursal = {
    ID: "CTA 11/2",
    Empresa: "CrossLogic SA",
    CuentasAsociadas: [{ Cuenta: "Cta Cte ARS" }]
  };
  const s2: ISucursal = {
    ID: "AgroNación",
    Empresa: "CrossLogic SA",
    CuentasAsociadas: [{ Cuenta: "Tarjetas" }]
  };

  const ss = [s1, s2];

 it( "Sucursal-Empresa fija: Deposito bancario", () => {// Depósito bancario
    let intersect = cruzarCuentasYSucursales(e1, p, ss, [ctaCte]);
    expect(intersect).toHaveLength(1);
  })

  it("Sucursal-Empresa fija: Pago con tarjeta", () => {// Pago con tarjeta
    let intersect = cruzarCuentasYSucursales(e1, p, ss, [tarjetas]);
    expect(intersect).toHaveLength(1);
  })

 it("Sucursal-Empresa fija: Venta al banco", () =>  {// Venta al banco
    let intersect = cruzarCuentasYSucursales(e1, p, ss, [clientes]);
    expect(intersect).toHaveLength(2);
  })

 it( "Sucursal-Empresa fija: Deposito bancario (otra empresa)", () => {
    let intersect = cruzarCuentasYSucursales(e2, p, ss, [ctaCte]);
    expect(intersect).toHaveLength(0);
  })

  it("Sucursal-Empresa fija: Pago con tarjeta (otra empresa)", () => {
    let intersect = cruzarCuentasYSucursales(e2, p, ss, [tarjetas]);
    expect(intersect).toHaveLength(0);
  })
 it("Sucursal-Empresa fija: Venta al banco (otra empresa)", () =>  {
    let intersect = cruzarCuentasYSucursales(e2, p, ss, [clientes]);
    expect(intersect).toHaveLength(2);
  })
});
