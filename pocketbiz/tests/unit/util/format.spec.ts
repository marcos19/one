import { enPesos, enFecha, enFechaHora, enHora } from "@/modules/util/format";
import { newDate } from "@/modules/util/fechas";

describe("Formatear dinero", () => {
  test("otro type", () => {
    expect(enPesos(NaN)).toBe("NaN");
  });

  test("cero", () => {
    expect(enPesos(0)).toBe("-");
  });

  test("centavos", () => {
    expect(enPesos(0.32)).toBe("0,32");
  });

  test("centavos negativo", () => {
    expect(enPesos(-0.32)).toBe("-0,32");
  });

  test("1 dígito", () => {
    expect(enPesos(1)).toBe("1,00");
  });

  test("1 dígito negativo", () => {
    expect(enPesos(-1)).toBe("-1,00");
  });

  test("2 dígitos", () => {
    expect(enPesos(-12)).toBe("-12,00");
  });

  test("3 dígitos", () => {
    expect(enPesos(-123)).toBe("-123,00");
  });

  test("4 dígitos", () => {
    expect(enPesos(-1234)).toBe("-1.234,00");
  });
});

describe("Formatear fechas", () => {
  test("fecha común", () => {
    expect(enFecha("2021-08-21")).toBe("21/08/2021");
  });
  test("fecha vacía", () => {
    expect(enFecha("0000-00-00")).toBe("N/D");
  });
  test("fecha errónea", () => {
    expect(enFecha("2021-13-05")).toBe("N/D");
  });
});

describe("Formatear horas", () => {
  test("fecha y hora", () => {
    expect(enFechaHora("2021-10-19T10:41:50.672075Z")).toBe("19/10/2021 07:41");
  });
});

describe("Crear fecha", () => {
  test("en local time", () => {
    const dt = newDate("2021-06-02");
    const expected = new Date("2021-06-02T03:00:00.000Z");
    const notExpected = new Date("2021-06-01T21:00:00.000Z");
    expect(dt).toEqual(expected);
    expect(dt).not.toEqual(notExpected);
  });
});
