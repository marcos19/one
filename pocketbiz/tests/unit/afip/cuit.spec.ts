import { conGuiones, validarCUIT } from "@/modules/afip/cuit";

test("agrega guiones al CUIT", () => {
  expect(conGuiones("20328896479")).toBe("20-32889647-9");
  expect(conGuiones("")).toBe("");
  expect(conGuiones("20328896478")).toContain("CUIT INVÁLIDO");
});

test("Validación de CUIT", () => {
  expect(validarCUIT("20328896479")).toBe(true);
  expect(validarCUIT("20-32889647-9")).toBe(true);
  expect(validarCUIT("20-32889647-8")).toBe(false);
  expect(validarCUIT("20328896478")).toBe(false);
  expect(validarCUIT("2032889647")).toBe(false);
});
