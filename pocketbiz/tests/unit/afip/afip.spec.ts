import {
  solicitarCAE,
  cruzarFacturaCondicion,
  Emisor,
} from "@/modules/afip/afip";

describe("Solicitar CAE", () => {
  test("Sin ID", () => {
    expect(solicitarCAE("")).rejects.toContain("no se ingres");
  });

  //test("Con ID", () => {
  //return solicitarCAE("asdasd").then((data) => {
  //// expect(data).toContain("falopa");
  //});
  //});
});

test("Comprobantes y condiciones", () => {
  const RI = "2";
  const MT = "mt";
  const LA_EMPRESA = Emisor.LaEmpresa;
  const UN_TERCERO = Emisor.UnTercero;
  const A = "A";
  const B = "B";
  const C = "C";
  const ResponsableInscripto = "Responsable inscripto";
  const NoInscripto = "No inscripto";

  {
    // Facturas de venta
    let res = cruzarFacturaCondicion({
      emisor: LA_EMPRESA,
      condicionEmpresa: ResponsableInscripto,
      condicionPersona: RI,
      letra: A,
    });
    expect(res.puede).toBe(true);
    expect(res.msg).toBe("");

    res = cruzarFacturaCondicion({
      emisor: LA_EMPRESA,
      condicionEmpresa: ResponsableInscripto,
      condicionPersona: RI,
      letra: B,
    });
    expect(res.puede).toBe(true);
    expect(res.msg).not.toBe("");

    res = cruzarFacturaCondicion({
      emisor: LA_EMPRESA,
      condicionEmpresa: ResponsableInscripto,
      condicionPersona: RI,
      letra: C,
    });
    expect(res.puede).toBe(false);
    expect(res.msg).not.toBe("");

    res = cruzarFacturaCondicion({
      emisor: LA_EMPRESA,
      condicionEmpresa: ResponsableInscripto,
      condicionPersona: MT,
      letra: A,
    });
    expect(res.puede).toBe(false);
    expect(res.msg).not.toBe("");

    res = cruzarFacturaCondicion({
      emisor: LA_EMPRESA,
      condicionEmpresa: ResponsableInscripto,
      condicionPersona: MT,
      letra: B,
    });
    expect(res.puede).toBe(true);
    expect(res.msg).toBe("");

    res = cruzarFacturaCondicion({
      emisor: LA_EMPRESA,
      condicionEmpresa: ResponsableInscripto,
      condicionPersona: MT,
      letra: C,
    });
    expect(res.puede).toBe(false);
    expect(res.msg).not.toBe("");
  }

  {
    // Facturas de compra
    let res = cruzarFacturaCondicion({
      emisor: UN_TERCERO,
      condicionPersona: RI,
      condicionEmpresa: ResponsableInscripto,
      letra: A,
    });
    expect(res.puede).toBe(true);
    expect(res.msg).toBe("");

    res = cruzarFacturaCondicion({
      emisor: UN_TERCERO,
      condicionPersona: RI,
      condicionEmpresa: ResponsableInscripto,
      letra: B,
    });
    expect(res.puede).toBe(true);
    expect(res.msg).not.toBe("");

    res = cruzarFacturaCondicion({
      emisor: UN_TERCERO,
      condicionPersona: RI,
      condicionEmpresa: ResponsableInscripto,
      letra: C,
    });
    expect(res.puede).toBe(false);
    expect(res.msg).not.toBe("");

    res = cruzarFacturaCondicion({
      emisor: UN_TERCERO,
      condicionPersona: MT,
      condicionEmpresa: ResponsableInscripto,
      letra: A,
    });
    expect(res.puede).toBe(false);
    expect(res.msg).not.toBe("");

    res = cruzarFacturaCondicion({
      emisor: UN_TERCERO,
      condicionPersona: MT,
      condicionEmpresa: ResponsableInscripto,
      letra: B,
    });
    expect(res.puede).toBe(false);
    expect(res.msg).not.toBe("");

    res = cruzarFacturaCondicion({
      emisor: UN_TERCERO,
      condicionPersona: MT,
      condicionEmpresa: ResponsableInscripto,
      letra: C,
    });
    expect(res.puede).toBe(true);
    expect(res.msg).toBe("");
  }

  {
    // Empresa no insripta
    let res = cruzarFacturaCondicion({
      emisor: UN_TERCERO,
      condicionPersona: RI,
      condicionEmpresa: NoInscripto,
      letra: A,
    });
    expect(res.puede).toBe(true);
    expect(res.msg).toBe("");
	
    res = cruzarFacturaCondicion({
      emisor: UN_TERCERO,
      condicionPersona: MT,
      condicionEmpresa: NoInscripto,
      letra: C,
    });
    expect(res.puede).toBe(true);
    expect(res.msg).toBe("");

	// La empresa no inscripta no puede emitir comprobante fiscal
	res = cruzarFacturaCondicion({
		emisor: LA_EMPRESA,
		condicionPersona: RI,
		condicionEmpresa: NoInscripto,
		letra: A,
	});
	console.log(res)
	expect(res.puede).toBe(false);
	expect(res.msg).not.toBe("");

	// La empresa no inscripta no puede emitir comprobante fiscal
	res = cruzarFacturaCondicion({
		emisor: LA_EMPRESA,
		condicionPersona: RI,
		condicionEmpresa: NoInscripto,
		letra: B,
	});
	expect(res.puede).toBe(false);
	expect(res.msg).not.toBe("");

	// La empresa no inscripta no puede emitir comprobante fiscal
    res = cruzarFacturaCondicion({
      emisor: LA_EMPRESA,
      condicionPersona: RI,
      condicionEmpresa: NoInscripto,
      letra: C,
    });
    expect(res.puede).toBe(false);
    expect(res.msg).not.toBe("");
	
  }
});
