import { unicidadString } from "@/modules/unicidades/unicidades";
import { Unicidad } from "@/modules/unicidades/unicidades";
import { UnicidadDef, Att } from "@/modules/unicidadesdef/def";

test("Unicidad string", () => {
  let def: UnicidadDef = {
    ID: "a",
    Predefinida: "",
    Nombre: "Cheques",
    NombreSingular: "Cheque",
    Genero: "Masculino",
    Tipo: "uso",
    Funcion: "",
    AttsIndexados: [
      {
        Nombre: "Número",
        Type: "int",
        Obligatorio: true,
        IncluirEnDetalle: 1,
        IncluirKeyEnDetalle: true,
        Funcion: ""
      }
    ],
    Atts: [
      {
        Nombre: "Fecha diferida",
        Type: "fecha",
        Obligatorio: true,
        IncluirEnDetalle: 2,
        IncluirKeyEnDetalle: true,
        Funcion: "fecha_diferida"
      },
      {
        Nombre: "Fecha emisión",
        Type: "fecha",
        Obligatorio: true,
        IncluirEnDetalle: 0,
        IncluirKeyEnDetalle: false,
        Funcion: ""
      }
    ]
  };
  let u: Unicidad = {
    ID: "1",
    Definicion: "a",
    Atts: {
      "Fecha emisión": "2021-09-30",
      "Fecha diferida": "2021-12-30"
    },
    AttsIndexados: {
      Número: 934858
    },
	CreatedAt: null,
  };
  expect(unicidadString(u, def)).toBe(
    "Número: 934858 | Fecha diferida: 30/12/2021"
  );
});
