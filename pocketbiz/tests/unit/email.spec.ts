import { createLocalVue, mount } from "@vue/test-utils";
import Email from "@/modules/fact/components/Email.vue"; //"@/components/fact/Email.vue";
import Buefy from "buefy";
// import Vuex from "vuex";

describe("Email modal form", () => {
  test("renders a greeting", () => {
    let localVue = createLocalVue();
    // {
    //   propsData: {
    //     empresa: "1";
    //   }
    // }
    localVue.use(Buefy);
    // localVue.use(Vuex);
    const wrapper = mount(Email, {
      localVue,
      propsData: {
        empresa: "1"
      }
    });
    expect(wrapper.find("button").text()).toBe("Enviar");
  });
});

// console.log(wrapper.html());

// "eslint-plugin-vue": "^6.2.2",
