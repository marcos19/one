package unicidadesdef

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/unicidadesdef"
	"github.com/cockroachdb/errors"
)

type Server struct {
	h *unicidadesdef.Handler
}

func NewServer(h *unicidadesdef.Handler) (s *Server) {

	return &Server{h}

}
func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string

	// Saco lo que viene después
	head, r.URL.Path = shiftPath(r.URL.Path)
	head = strings.TrimLeft(head, "/")

	// Leo usuario
	_, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Leo comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch head {
	case "para_store":
		s.HandleDefinicionesParaStore(comitente)(w, r)
		return
	}

	switch head {

	case "many":
		s.ReadMany(comitente)(w, r)

	case "one":
		s.ReadOne(comitente)(w, r)

	case "create":
		s.Create(comitente)(w, r)

	case "update":
		s.Update(comitente)(w, r)

	default:
		deferror.Frontend(deferror.NotFound(head), w, r)
	}
}

// HandleCreateDef inserta una nueva definición de unicidad
func (s *Server) Create(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo el body
		req := unicidadesdef.Def{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Inserto
		req.Comitente = comitente
		err = s.h.Create(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "creando definición de unicidad"), w, r)
			return
		}
	}
}

func (s *Server) ReadMany(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Busco
		dd, err := s.h.ReadMany(r.Context(), comitente)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando definiciones de unicidades"), w, r)
			return
		}

		err = json.NewEncoder(w).Encode(&dd)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// HandleDefinicionesParaStore devuelve las definiciones para el store del frontend
func (s *Server) HandleDefinicionesParaStore(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		out, err := s.h.HandleDefinicionesParaStore(r.Context(), comitente)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// HandleOne devuelve el listado de todas las unicidades
func (s *Server) ReadOne(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo el body
		req := unicidadesdef.ReadOneReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.h.ReadOne(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando definición de unicidad"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// HandleUpdate modifica una definición de unicidad
func (s *Server) Update(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo el body
		req := unicidadesdef.Def{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Modifico
		req.Comitente = comitente
		err = s.h.Update(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "modificando definición unicidad"), w, r)
			return
		}
	}
}

func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
