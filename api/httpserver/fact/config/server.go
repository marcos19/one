package fact

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/fact/config"
	"github.com/cockroachdb/errors"
)

type Server struct {
	h *config.Handler
}

func NewServer(h *config.Handler) *Server {
	return &Server{h}
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	user, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Leo comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch head {

	case "many":
		s.ReadMany(comitente)(w, r)

	case "lookup":
		s.Lookup(comitente, user)(w, r)

	case "one":
		s.ReadOne(comitente)(w, r)

	case "create":
		s.Create(comitente)(w, r)

	case "update":
		s.Update(comitente)(w, r)

	case "delete":
		s.Delete(comitente)(w, r)

	case "duplicate":
		s.Duplicate(comitente)(w, r)

	case "grupos":
		s.Grupos()(w, r)

	default:
		deferror.Frontend(deferror.NotFound(head), w, r)
	}
}

func (s *Server) ReadMany(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo parámetros
		req := config.ReadManyReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		cc, err := s.h.ReadMany(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando configs"), w, r)
			return
		}

		//  Codifico JSON
		err = json.NewEncoder(w).Encode(&cc)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) Lookup(comitente int, user string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo parámetros
		req := config.ReadManyReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		cc, err := s.h.Lookup(r.Context(), req, user)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando configs"), w, r)
			return
		}

		//  Codifico JSON
		err = json.NewEncoder(w).Encode(&cc)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) Create(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo body
		req := config.Config{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Inserto
		req.Comitente = comitente
		err = s.h.Create(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "creando config"), w, r)
			return
		}
	}
}
func (s *Server) Update(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo body
		req := config.Config{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Persisto
		req.Comitente = comitente
		err = s.h.Update(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "actualizando config"), w, r)
			return
		}

	}
}

func (s *Server) ReadOne(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := config.ReadOneReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.h.ReadOne(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando config por ID"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) Delete(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo body
		req := config.ReadOneReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Borro
		req.Comitente = comitente
		err = s.h.Delete(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "borrando config"), w, r)
			return
		}
	}
}

func (s *Server) Duplicate(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := config.DupReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		err = s.h.Duplicar(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}
func (s *Server) Grupos() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		out := config.GruposConfig

		// Devuelvo JSON
		err := json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// // Tipos devuelve el listado de tipos (factura directa, remito, etc)
// func (s *Server) Tipos() http.HandlerFunc {

// 	return func(w http.ResponseWriter, r *http.Request) {
// 		tt := config.Tipos()

// 		err := json.NewEncoder(w).Encode(&tt)
// 		if err != nil {
// 			deferror.Frontend(err, w, r)
// 			return
// 		}
// 	}
// }

// ShiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
