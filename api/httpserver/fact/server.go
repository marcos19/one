package fact

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/fact"
	"bitbucket.org/marcos19/one/pkg/fact/handler"
	"bitbucket.org/marcos19/one/pkg/fact/permisos"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/factinter"
	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/rs/zerolog/log"
)

// Handler da acceso al a todos los routes de personas.
type Server struct {
	h            *handler.Handler
	hInteractive *factinter.Handler
	conn         *pgxpool.Pool
}

// NewHandler inicializa un cache y devuelve el puntero a este.
func NewServer(h *handler.Handler, hi *factinter.Handler, conn *pgxpool.Pool) *Server {

	s := &Server{}
	s.h = h
	s.hInteractive = hi
	s.conn = conn

	//
	s.h.Bus.On(fact.EventMailSending{}, func(e interface{}) {
	})
	s.h.Bus.On(fact.EventMailSent{}, func(e interface{}) {
		log.Info().Msgf("mail enviado OK")
	})
	s.h.Bus.On(fact.EventMailFailed{}, func(e interface{}) {
		log.Error().Msgf("error al enviar mail: %v", e)
	})

	return s
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	user, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Determino el comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch head {

	case "one":
		s.ReadOne(comitente, user)(w, r)

	case "create":
		s.Create(comitente, user)(w, r)

	case "delete":
		s.Delete(comitente, user)(w, r)

	case "calcular":
		s.ProcesarOperacion(comitente, user)(w, r)

	case "pdf":
		s.PDF(comitente)(w, r)

	case "anular":
		s.Anular(comitente, user)(w, r)

	case "comprobante_preview":
		s.ComprobantePreview(comitente, user)(w, r)

	case "comprobante_preview_confirmar":
		s.ComprobantePreviewConfirmar(comitente, user)(w, r)

	case "comprobante_enviar":
		s.ComprobanteEnviar(comitente, user)(w, r)

	case "diferir_imputacion":
		s.DiferirImputacion(comitente, user)(w, r)

	case "opprod_cuentas":
		s.OpprodCuentas()(w, r)

	case "opprod_conceptos":
		s.OpprodConceptos()(w, r)

	default:
		httperr.NotFound(w, r)
	}
}

func (s *Server) ReadOne(comitente int, user string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo body
		req := fact.ReadOneReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		req.Comitente = comitente

		// Busco fact
		out, err := s.h.ReadOne(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Tiene permisos?
		permisoReq := permisos.PuedeReq{
			Comitente: comitente,
			Usuario:   user,
			Config:    out.Config,
			Esquema:   out.Esquema,
			CRUD:      permisos.Read,
		}
		puede, err := permisos.Puede(r.Context(), s.conn, permisoReq)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}
		if !puede {
			httperr.Forbidden(w, "No está autorizado para ver esta operación")
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}
func (s *Server) Create(comitente int, user string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo body
		f := types.Fact{}
		err := json.NewDecoder(r.Body).Decode(&f)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}
		f.Comitente = comitente

		// Tiene permisos?
		permisoReq := permisos.PuedeReq{
			Comitente: comitente,
			Usuario:   user,
			Config:    f.Config,
			Esquema:   f.Esquema,
			CRUD:      permisos.Create,
		}
		puede, err := permisos.Puede(r.Context(), s.conn, permisoReq)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}
		if !puede {
			httperr.Forbidden(w, "No está autorizado para ver crear esta operación")
			return
		}

		// Inserto
		args := handler.CreateArgs{Usuario: user}
		err = s.hInteractive.NuevaFact(r.Context(), &f, args)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo fact recién creada
		err = json.NewEncoder(w).Encode(&f)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) Delete(comitente int, user string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		log.Debug().Msg("Entando a server delete")
		// Leo body
		req := handler.DeleteReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}
		req.Comitente = comitente

		// Busco operación original
		o, err := s.h.ReadOne(r.Context(), fact.ReadOneReq{Comitente: req.Comitente, ID: req.ID})
		if err != nil {
			httperr.InternalServerError(w, errors.Wrap(err, "buscando operación orginal"))
			return
		}

		// Tiene permisos?
		permisoReq := permisos.PuedeReq{
			Comitente: comitente,
			Usuario:   user,
			Config:    o.Config,
			Esquema:   o.Esquema,
			CRUD:      permisos.Delete,
		}
		puede, err := permisos.Puede(r.Context(), s.conn, permisoReq)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}
		if !puede {
			httperr.Forbidden(w, "No está autorizado para borrar esta operación")
			return
		}

		// Borro
		req.Usuario = user
		err = s.h.Delete(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, errors.Wrap(err, "borrando"))
			return
		}
	}
}

func (s *Server) ProcesarOperacion(comitente int, usuario string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo body
		f := types.Fact{}
		err := json.NewDecoder(r.Body).Decode(&f)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Completo datos de transacción
		f.Comitente = comitente

		// Proceso
		err = s.h.Procesar(r.Context(), &f, handler.CreateArgs{Usuario: usuario})
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo la fact
		err = json.NewEncoder(w).Encode(&f)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

// HandlePDF devuelve el comprobante
func (s *Server) PDF(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo body
		req := fact.PDFReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		req.Comitente = comitente

		// Junto los datos para el PDF
		outputer, err := s.h.PDF(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, errors.Wrap(err, "armando datos para PDF"))
			return
		}

		// Exporto
		w.Header().Set("Content-Type", "application/pdf")
		w.Header().Set("Content-Disposition", "attachment; filename=factura.pdf")
		err = outputer.Output(w)
		if err != nil {
			httperr.InternalServerError(w, errors.Wrap(err, "outputing"))
		}
	}
}

func (s *Server) Anular(comitente int, usuario string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		// Leo body
		req := handler.AnularReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Inserto
		req.Comitente = comitente
		req.Usuario = usuario

		out, err := s.h.Anular(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo fact recién creada
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) ComprobantePreview(comitente int, usuario string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := []uuid.UUID{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Genero
		out, err := s.h.PreviewMail(r.Context(), comitente, req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

// Confirma el envío una vez modificado el mail
func (s *Server) ComprobantePreviewConfirmar(comitente int, usuario string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := handler.EnviarReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Genero
		req.Comitente = comitente
		err = s.h.PreviewMailConfirmar(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}
	}
}

// Envío automático de comprobantes
func (s *Server) ComprobanteEnviar(comitente int, usuario string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := []uuid.UUID{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Genero
		err = s.h.EnviarMail(r.Context(), comitente, req)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) DiferirImputacion(comitente int, usuario string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := handler.DiferirReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Genero
		req.Comitente = comitente
		req.Usuario = usuario
		err = s.h.Diferir(r.Context(), req)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}

	}
}

func (s *Server) OpprodConceptos() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		out := s.h.OpprodConceptos()

		err := json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}
	}
}

func (s *Server) OpprodCuentas() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		out := s.h.OpprodCuentas()

		// Devuelvo JSON
		err := json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

// ShiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
