package permisos

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/fact/permisos"
)

type Server struct {
	h *permisos.Handler
}

func NewServer(h *permisos.Handler) *Server {
	return &Server{h}
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	user, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Leo comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch head {

	// case "many":
	// 	s.ReadMany(comitente)(w, r)

	// case "one":
	// 	s.ReadOne(comitente)(w, r)

	case "create":
		s.Create(comitente, user)(w, r)

	case "update":
		s.Update(comitente, user)(w, r)

	case "delete":
		s.Delete(comitente, user)(w, r)

	case "por_config":
		s.PorConfig(comitente)(w, r)

	case "por_esquema":
		s.PorEsquema(comitente)(w, r)

	default:
		deferror.Frontend(deferror.NotFound(head), w, r)
	}
}

func (s *Server) ReadMany(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// // Leo parámetros
		// req := config.ReadManyReq{}
		// err := json.NewDecoder(r.Body).Decode(&req)
		// if err != nil {
		// 	deferror.Frontend(deferror.BadRequest(err), w, r)
		// 	return
		// }

		// // Busco
		// req.Comitente = comitente
		// cc, err := s.h.ReadMany(r.Context(), req)
		// if err != nil {
		// 	deferror.Frontend(errors.Wrap(err, "buscando configs"), w, r)
		// 	return
		// }

		// //  Codifico JSON
		// err = json.NewEncoder(w).Encode(&cc)
		// if err != nil {
		// 	deferror.Frontend(err, w, r)
		// 	return
		// }
	}
}

func (s *Server) Create(comitente int, user string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo body
		req := permisos.Permiso{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Inserto
		req.Comitente = comitente
		err = s.h.Create(r.Context(), req, user)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) Update(comitente int, user string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo body
		req := permisos.Permiso{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Persisto
		req.Comitente = comitente
		err = s.h.Update(r.Context(), req, user)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

	}
}

func (s *Server) PorConfig(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo body
		req := permisos.PorConfigReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Inserto
		req.Comitente = comitente
		out, err := s.h.PorConfig(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) PorEsquema(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo body
		req := permisos.PorEsquemaReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Inserto
		req.Comitente = comitente
		out, err := s.h.PorEsquema(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) ReadOne(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// // Leo request
		// req := config.ReadOneReq{}
		// err := json.NewDecoder(r.Body).Decode(&req)
		// if err != nil {
		// 	deferror.Frontend(deferror.BadRequest(err), w, r)
		// 	return
		// }

		// // Busco
		// req.Comitente = comitente
		// out, err := s.h.ReadOne(r.Context(), req)
		// if err != nil {
		// 	deferror.Frontend(errors.Wrap(err, "buscando config por ID"), w, r)
		// 	return
		// }

		// // Devuelvo JSON
		// err = json.NewEncoder(w).Encode(&out)
		// if err != nil {
		// 	deferror.Frontend(errors.Wrap(err, "actualizando config"), w, r)
		// 	return
		// }
	}
}

func (s *Server) Delete(comitente int, user string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo body
		req := permisos.Permiso{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Borro
		req.Comitente = comitente
		err = s.h.Delete(r.Context(), req, user)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// ShiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
