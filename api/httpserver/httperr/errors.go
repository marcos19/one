package httperr

import (
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
)

type Response struct {
	Tipo string
	Msg  string
}

func BadRequest(w http.ResponseWriter, err error) {

	w.WriteHeader(http.StatusBadRequest)
	w.Write([]byte(err.Error()))
}

func UnprocessableEntity(w http.ResponseWriter, err error) {

	resp := Response{
		Tipo: "Validación",
		Msg:  err.Error(),
	}
	w.WriteHeader(http.StatusUnprocessableEntity)
	err2 := json.NewEncoder(w).Encode(&resp)
	if err2 != nil {
		Encoding(w, err2)
	}
}

// Unauthorized (401) indica que la petición (request)
// no ha sido ejecutada porque carece de credenciales válidas
// de autenticación para el recurso solicitado.
// Cuando el frontend recibe esta respuesta redirige a login.
func Unauthorized(w http.ResponseWriter) {
	w.WriteHeader(http.StatusUnauthorized)
}

// No debería pasar. Significa que un usuario no
func ComitenteIndefinido(w http.ResponseWriter) {
	log.Error().Msgf("COMITENTE INDEFINIDO")
	w.WriteHeader(http.StatusUnauthorized)
	fmt.Fprintf(w, "Comitente indefinido")
}

// Forbidden (403) no se soluciona con una re-autenticación.
// El acceso está permanentemente prohibido y ligado a la
// lógica de la aplicación, como el no tener los permisos
// necesarios para acceder al recurso.
func Forbidden(w http.ResponseWriter, msg string) {
	w.WriteHeader(http.StatusForbidden)
	w.Write([]byte(msg))
}

func NotFound(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
	msg := "No se encontró la dirección"
	u, _ := contexto.GetPath(r.Context())
	if u != "" {
		msg += ": " + u
	}
	fmt.Fprintln(w, msg)
}

func Encoding(w http.ResponseWriter, err error) {
	http.Error(w, errors.Wrap(err, "encoding output").Error(), http.StatusInternalServerError)
}

func InternalServerError(w http.ResponseWriter, err error) {
	w.WriteHeader(http.StatusInternalServerError)
	fmt.Fprintf(w, err.Error())
}

func NotImplemented(w http.ResponseWriter, r *http.Request) {
	http.Error(w, fmt.Sprintf("método %v no implementado", r.Method), http.StatusNotImplemented)
}
