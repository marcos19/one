package lookups

// import (
// 	"strings"
// 	"testing"

// 	"bitbucket.org/marcos19/one/pkg/migrations"
// 	"github.com/cockroachdb/errors"
// 	"github.com/stretchr/testify/assert"
// )

// const dbName = "test_permisos"

// func TestComprobantes(t *testing.T) {
// 	// Conecto a base
// 	db, err := migrations.TestSQLDB(dbName)
// 	if err != nil {
// 		t.Fatal(errors.Wrap(err, "conectando a la base de datos"))
// 	}

// 	_, err = db.Exec(`
// 		DELETE FROM permisos where para = 'comprobantes'
// 		;`)
// 	assert.Nil(t, err)

// 	_, err = db.Exec(`
// 		INSERT INTO permisos (id, para, empresa, programa, usuario_grupo, comprobante_out)
// 		VALUES
// 			(1, 'comprobantes', 1, 'minuta', 20, 1564),
// 			(2, 'comprobantes', 1, 'minuta', 20, 8654)
// 			;
// 		`)
// 	assert.Nil(t, err)

// 	_, err = db.Exec(`DELETE FROM grupos WHERE id = 20;`)
// 	assert.Nil(t, err)

// 	_, err = db.Exec(`
// 		INSERT INTO grupos
// 		(id, de, nombre, strings) VALUES
// 		(20, 'usuarios', 'cajeros', ARRAY['marcos', 'jose']);
// 		`)
// 	assert.Nil(t, err)
// 	// Pruebo
// 	body := strings.NewReader(`{"empresa":"1","programa": "minuta"}`)
// 	ids, err := Comprobantes(db, body, "marcos")
// 	assert.Nil(t, err)
// 	assert.Len(t, ids, 2)

// }

// func TestCentrosEmisores(t *testing.T) {
// 	// Conecto a base
// 	db, err := migrations.TestSQLDB(dbName)
// 	if err != nil {
// 		t.Fatal(errors.Wrap(err, "conectando a la base de datos"))
// 	}

// 	_, err = db.Exec(`
// 		DELETE FROM permisos where para = 'emisores'
// 		;`)
// 	assert.Nil(t, err)

// 	_, err = db.Exec(`
// 		INSERT INTO permisos (id, para, empresa, programa, usuario_grupo, emisor_out)
// 		VALUES
// 			(11, 'emisores', 1, 'minuta', 30, 120),
// 			(12, 'emisores', 1, 'minuta', 30, 121),
// 			(13, 'emisores', 1, 'minuta', 30, 122)
// 			;
// 		`)
// 	assert.Nil(t, err)

// 	_, err = db.Exec(`DELETE FROM grupos WHERE id = 30;`)
// 	assert.Nil(t, err)

// 	_, err = db.Exec(`
// 		INSERT INTO grupos
// 		(id, de, nombre, strings) VALUES
// 		(30, 'usuarios', 'cajeros', ARRAY['marcos', 'jose']);
// 		`)
// 	assert.Nil(t, err)

// 	// Pruebo
// 	body := strings.NewReader(`{"empresa":"1","programa": "minuta"}`)
// 	ids, err := CentrosEmisores(db, body, "marcos")
// 	assert.Nil(t, err)
// 	assert.Len(t, ids, 3)

// }
// func TestEmpresas(t *testing.T) {
// 	db, err := migrations.TestSQLDB(dbName)
// 	if err != nil {
// 		t.Fatal(errors.Wrap(err, "conectando a la base de datos"))
// 	}

// 	_, err = db.Exec(`
// 		DELETE FROM permisos where para = 'empresas'
// 		;`)
// 	assert.Nil(t, err)

// 	_, err = db.Exec(`
// 		INSERT INTO permisos (id, para, programa, usuario_grupo, empresa_out)
// 		VALUES
// 			(22, 'empresas', 'minuta', 1, 8),
// 			(24, 'empresas', 'minuta', 2, 8),
// 			(25, 'empresas', 'minuta', 3, 7)
// 			;
// 		`)
// 	assert.Nil(t, err)

// 	_, err = db.Exec(`
// 		DELETE FROM grupos WHERE id < 10
// 		;`)
// 	assert.Nil(t, err)

// 	_, err = db.Exec(`
// 		INSERT INTO grupos
// 		(id, de, nombre, strings) VALUES
// 		(1, 'usuarios', 'cajeros', ARRAY['marcos', 'jose']),
// 		(2, 'usuarios', 'repositores', ARRAY['antonio', 'pedro']),
// 		(3, 'usuarios', 'todos', ARRAY['marcos', 'jose', 'antonio', 'pedro', 'auditor'])
// 		;
// 		`)
// 	assert.Nil(t, err)

// 	// Usario que está en dos grupos
// 	body := strings.NewReader(`{"programa": "minuta"}`)
// 	ids, err := Empresas(db, body, "marcos")
// 	assert.Nil(t, err)
// 	assert.Len(t, ids, 2)

// 	// Usuario que está en un grupo
// 	body = strings.NewReader(`{"programa": "minuta"}`)
// 	ids, err = Empresas(db, body, "auditor")
// 	assert.Nil(t, err)
// 	assert.Len(t, ids, 1)

// 	// Usuario que no está en ningún grupo
// 	body = strings.NewReader(`{"programa": "minuta"}`)
// 	ids, err = Empresas(db, body, "no existe")
// 	assert.Nil(t, err)
// 	assert.Len(t, ids, 0)
// }
