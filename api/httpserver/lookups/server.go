package lookups

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	permisos "bitbucket.org/marcos19/one/pkg/permisos/lookups"
	"github.com/gofrs/uuid"
)

// Handler es el encargado de recibir los http requests y emitir los responses.
type Server struct {
	Handler *permisos.Handler
}

// NewHandler inicializa un cache y devuelve el puntero a este.
func NewServer(handler *permisos.Handler) (s *Server) {
	s = &Server{}
	s.Handler = handler
	return
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	_, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Determino el comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch head {

	case "one":
		s.LookupOne(comitente)(w, r)

	case "many":
		s.LookupMany(comitente)(w, r)

	case "insertar":
		s.LookupInsertar(comitente)(w, r)

	case "modificar":
		s.LookupModificar(comitente)(w, r)

	default:
		httperr.NotFound(w, r)
		return
	}
}

// ManyLookup determina si se puede realizar la operación crud solicitada.
func (s *Server) LookupOne(comitente int) http.HandlerFunc {
	type req struct {
		ID uuid.UUID
	}
	return func(w http.ResponseWriter, r *http.Request) {

		req := req{}

		// Leo request
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Busco
		response, err := s.Handler.LookupOne(r.Context(), req.ID, comitente)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&response)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

// ManyLookup determina si se puede realizar la operación crud solicitada.
func (s *Server) LookupMany(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		req := permisos.LookupManyReq{}

		// Leo request
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		req.Comitente = comitente

		// Busco
		response, err := s.Handler.LookupMany(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&response)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

// InsertarLookup persiste un nuevo permiso.
func (s *Server) LookupInsertar(comitente int) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		req := permisos.DeLookups{}

		// Leo request
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		req.Comitente = comitente

		// Inserto
		err = s.Handler.Insertar(r.Context(), &req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}
	}
}

// ModificarLookup modifica un permiso existente.
func (s *Server) LookupModificar(comitente int) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		req := permisos.DeLookups{}

		// Leo request
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		req.Comitente = comitente

		// Busco
		err = s.Handler.Modificar(r.Context(), &req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
