package productos

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/productos/categorias"
	"github.com/cockroachdb/errors"
)

type Server struct {
	h *categorias.Handler
}

func NewServer(h *categorias.Handler) *Server {
	return &Server{h}
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	_, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Leo comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch head {

	case "create":
		s.Create(comitente)(w, r)

	case "one":
		s.ReadOne(comitente)(w, r)

	case "many":
		s.ReadMany(comitente)(w, r)

	case "categorias_con_cantidad":
		s.ReadCategoriasConCantidad(comitente)(w, r)

	case "update":
		s.Update(comitente)(w, r)

	case "delete":
		s.Delete(comitente)(w, r)

	default:
		httperr.NotFound(w, r)
	}
}

// HandleInsertarCategoria devuelve las categorías solicitadas
func (s *Server) Create(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := categorias.Categoria{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Creo
		req.Comitente = comitente
		err = s.h.Create(r.Context(), &req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		err = json.NewEncoder(w).Encode(&req)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) CategoriasParaStore(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Busco
		cc, err := s.h.CategoriasParaStore(r.Context(), comitente)
		if err != nil {
			httperr.InternalServerError(w, err)
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&cc)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "codificando JSON"), w, r)
			return
		}
	}
}

// HandleCategoriasConCantidad devuelve el listado de categorias con la cantidad de productos que tiene cada una.
func (s *Server) ReadCategoriasConCantidad(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		// Busco
		cc, err := s.h.ReadCategoriasConCantidad(r.Context(), comitente)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando categorías con cantidad"), w, r)
			return
		}

		// Corroboro que no haya ninguna con ese nombre
		err = json.NewEncoder(w).Encode(&cc)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "codificando JSON"), w, r)
			return
		}
	}
}

func (s *Server) ReadMany(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := categorias.ReadManyReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		cc, err := s.h.ReadMany(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando categorías"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&cc)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "codificando JSON"), w, r)
			return
		}
	}
}

// HandleCategoria devuelve las categorías solicitadas
func (s *Server) ReadOne(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		// Leo request
		req := categorias.ReadOneReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		c, err := s.h.ReadOne(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando categoría"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&c)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "codificando JSON"), w, r)
			return
		}
	}
}

// HandleModificarCategoria cambia los datos de una categoría de producto.
func (s *Server) Update(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := categorias.Categoria{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Modifico
		req.Comitente = comitente
		err = s.h.Update(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "modificando categoría"), w, r)
			return
		}
	}
}

// Delete cambia los datos de una categoría de producto.
func (s *Server) Delete(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := categorias.DeleteReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Borro
		req.Comitente = comitente
		err = s.h.Delete(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "borrando categoría"), w, r)
			return
		}
	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
