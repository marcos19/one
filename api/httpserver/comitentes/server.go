package comitentes

import (
	"encoding/json"
	"fmt"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/comitentes"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/crosslogic/niler"
	"github.com/pkg/errors"
)

type Server struct {
	reader comitentes.ReaderOne
}

func NewServer(r comitentes.ReaderOne) (s *Server, err error) {
	if niler.IsNil(r) {
		return s, errors.Errorf("Handler no puede ser nil")
	}
	s = &Server{r}
	return
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	_, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Leo comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch r.Method {

	case http.MethodGet:
		switch head {
		case "en_uso":
			s.ReadEnUso(comitente)(w, r)
		default:
			httperr.NotFound(w, r)
			return
		}

	default:
		http.Error(w, fmt.Sprintf(`método "%v" no permitido`, r.Method), http.StatusNotImplemented)
		return
	}
}

func (s *Server) ReadEnUso(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Busco
		out, err := s.reader.ReadOne(r.Context(), comitente)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando comitente"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
		}
	}
}

func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
