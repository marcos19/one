package comitentes

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/pkg/comitentes"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestContextData(t *testing.T) {

	t.Run("missing user", func(t *testing.T) {
		s := Server{readerMock{}}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusUnauthorized, w.Result().StatusCode)
	})

	t.Run("missing comitente", func(t *testing.T) {
		s := Server{readerMock{}}
		w := httptest.NewRecorder()

		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		// Pego usuario
		nuevoCtx := contexto.SetUser(r.Context(), "user")
		r = r.WithContext(nuevoCtx)

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusUnauthorized, w.Result().StatusCode)
	})

	t.Run("fails reader", func(t *testing.T) {
		s := Server{readerMock{err: true}}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "/en_uso/", nil)
		require.Nil(t, err)
		// Pego usuario
		nuevoCtx := contexto.SetUser(r.Context(), "user")
		// Pego comitente
		nuevoCtx = contexto.SetComitente(nuevoCtx, 1)
		r = r.WithContext(nuevoCtx)

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode)
	})

	t.Run("should work", func(t *testing.T) {
		s := Server{readerMock{}}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "/en_uso/", nil)
		require.Nil(t, err)
		// Pego usuario
		nuevoCtx := contexto.SetUser(r.Context(), "user")
		// Pego comitente
		nuevoCtx = contexto.SetComitente(nuevoCtx, 1)
		r = r.WithContext(nuevoCtx)

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode)
	})

}

type readerMock struct {
	err bool
}

func (r readerMock) ReadOne(context.Context, int) (out comitentes.Comitente, err error) {
	if r.err {
		return out, errors.Errorf("err mock")
	}

	return
}
