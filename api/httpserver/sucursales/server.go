package sucursales

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/sucursales"
	"github.com/cockroachdb/errors"
)

type Server struct {
	h *sucursales.Handler
}

func NewServer(h *sucursales.Handler) (s *Server, err error) {
	s = &Server{}
	if h == nil {
		return s, errors.New("sucursal handler era nil")
	}
	s.h = h

	return
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string

	// Saco lo que viene después
	head, r.URL.Path = shiftPath(r.URL.Path)
	head = strings.TrimLeft(head, "/")

	// Determino el usuario que estoy usando
	_, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}
	// Determino el comitente que estoy usando
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch head {
	case "create":
		s.Create(comitente)(w, r)

	case "read_many":
		s.ReadMany(comitente)(w, r)

	case "read_one":
		s.ReadOne(comitente)(w, r)

	case "update":
		s.Update(comitente)(w, r)

	case "delete":
		s.Delete(comitente)(w, r)

	default:
		httperr.NotFound(w, r)
		return
	}
}

func (s *Server) Create(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := sucursales.Sucursal{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Inserto
		req.Comitente = comitente
		err = s.h.Create(r.Context(), &req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

	}
}
func (s *Server) ReadMany(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := sucursales.ReadManyReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Busco sucursales
		req.Comitente = comitente
		ss, err := s.h.ReadMany(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&ss)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) ReadOne(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := sucursales.ReadOneReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.h.ReadOne(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) Update(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		// Leo request
		req := sucursales.Sucursal{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Modifico
		req.Comitente = comitente
		err = s.h.Update(r.Context(), &req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}
	}
}

func (s *Server) Delete(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo ID
		req := sucursales.DeleteReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Borro
		req.Comitente = comitente
		err = s.h.Delete(req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

	}
}

// ShiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
