package comps

import (
	"encoding/json"
	"net/http"
	"net/url"
	"path"
	"strconv"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/niler"
)

type Server struct {
	readerOne         comps.ReaderOne
	readerMany        comps.ReaderMany
	lookerUp          comps.LookerUp
	creater           comps.Creater
	updater           comps.Updater
	deleter           comps.Deleter
	withAuthorization Auther
}

func NewServer(h Handler, auther Auther) (s *Server, err error) {
	if niler.IsNil(h) {
		return s, errors.New("Handler no puede ser nil")
	}
	if niler.IsNil(auther) {
		return s, errors.New("CrudsHandler no puede ser nil")
	}

	return &Server{
		readerOne:         h,
		readerMany:        h,
		creater:           h,
		updater:           h,
		deleter:           h,
		lookerUp:          h,
		withAuthorization: auther,
	}, nil
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	_, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Determino el comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	var then http.HandlerFunc

	switch r.Method {

	case http.MethodGet:
		switch head {

		case "":
			then = s.readMany(comitente, r.URL.Query())

		case "categorias":
			then = s.categorias()

		case "lookups":
			then = s.lookup(comitente)

		default:
			then = s.readOne(comitente, head)

		}
	case http.MethodPost:
		then = s.create(comitente)

	case http.MethodPut:
		then = s.update(comitente)

	case http.MethodDelete:
		then = s.delete(comitente, head)

	default:
		http.Error(w, "method not implemented", http.StatusNotImplemented)
		return
	}

	s.withAuthorization(then)(w, r)

}

func (s *Server) create(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := comps.Comp{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Creo
		req.Comitente = comitente
		id, err := s.creater.Create(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		out := struct {
			ID int `json:",string"`
		}{id}
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
		}
	}
}

func (s *Server) readOne(comitente int, idStr string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		if idStr == "" {
			http.Error(w, "no se ingresó ID", http.StatusBadRequest)
			return
		}

		id, err := strconv.Atoi(idStr)
		if err != nil {
			http.Error(w, "el id ingresado es inválido", http.StatusBadRequest)
			return
		}

		// Leo request
		req := comps.ReadOneReq{}
		req.Comitente = comitente
		req.ID = id

		out, err := s.readerOne.ReadOne(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando comprobante"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) readMany(comitente int, query url.Values) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		var err error

		// Leo params
		req, msg, code := parseReadManyQuery(query)
		if code != 0 {
			http.Error(w, msg, code)
		}
		req.Comitente = comitente

		out, err := s.readerMany.ReadMany(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando comprobantes"), w, r)
			return
		}
		if out == nil {
			out = []comps.Comp{}
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func parseReadManyQuery(query url.Values) (out comps.ReadManyReq, msg string, code int) {

	var err error
	for k, v := range query {
		switch strings.ToLower(k) {
		case "empresa":
			if len(v) > 1 {
				return out, "sólo se acepta una empresa", http.StatusUnprocessableEntity
			}
			if v[0] == "" {
				return out, "se puso filtro de empresa pero no se seleccionó ninguna", http.StatusUnprocessableEntity
			}
			out.Empresa, err = strconv.Atoi(v[0])
			if err != nil {
				return out, "ID de empresa inválido", http.StatusBadRequest
			}

		case "ids":
			for _, idStr := range v {
				id, err := strconv.Atoi(idStr)
				if err != nil {
					return out, "ID de comprobante inválido", http.StatusBadRequest
				}
				out.IDs = append(out.IDs, id)
			}

		case "incluirinactivos":
			if len(v) > 1 {
				return out, "IncluirInactivos no puede tener más de un valor", http.StatusBadRequest
			}
			if strings.ToLower(v[0]) == "false" {
				out.IncluirInactivos = false
			} else {
				out.IncluirInactivos = true
			}
		case "tipo":
			if len(v) > 1 {
				return out, "tipo no puede tener más de un valor", http.StatusBadRequest
			}
			out.Tipo = v[0]
		}
	}
	return
}

func (s *Server) update(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := comps.Comp{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Modifico
		req.Comitente = comitente
		err = s.updater.Update(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "modificando comprobante"), w, r)
			return
		}
	}
}

func (s *Server) delete(comitente int, idStr string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		if idStr == "" {
			http.Error(w, "no se ingresó ID", http.StatusBadRequest)
			return
		}

		id, err := strconv.Atoi(idStr)
		if err != nil {
			http.Error(w, "el id ingresado es inválido", http.StatusBadRequest)
			return
		}
		// Leo request
		req := comps.ReadOneReq{
			Comitente: comitente,
			ID:        id,
		}

		// Borro
		req.Comitente = comitente
		err = s.deleter.Delete(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "borrando comprobante"), w, r)
			return
		}
	}
}

func (s *Server) lookup(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Busco
		out, err := s.lookerUp.Lookup(r.Context(), comitente)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando comps lookup"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) categorias() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Busco
		out := comps.Categorias()

		// Devuelvo JSON
		err := json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
