package comps

import (
	"net/http"

	"bitbucket.org/marcos19/one/pkg/comps"
)

type Handler interface {
	comps.ReaderOne
	comps.ReaderMany
	comps.Creater
	comps.Updater
	comps.Deleter
	comps.LookerUp
}

// Si pasa los controles devuelve 'then'.
// Si no los pasa devuelve una handlerfunc que responde Forbidden
type Auther func(then http.HandlerFunc) http.HandlerFunc
