package comps

import (
	"context"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestContextData(t *testing.T) {

	t.Run("missing user", func(t *testing.T) {
		s := Server{
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusUnauthorized, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("missing comitente", func(t *testing.T) {
		s := Server{
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()

		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		// Pego usuario
		nuevoCtx := contexto.SetUser(r.Context(), "user")
		r = r.WithContext(nuevoCtx)

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusUnauthorized, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("resource forbidden", func(t *testing.T) {
		s := Server{
			readerMany:        readerManyMock{err: false},
			withAuthorization: authForbiddenMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		// Pego usuario
		nuevoCtx := contexto.SetUser(r.Context(), "user")
		// Pego comitente
		nuevoCtx = contexto.SetComitente(nuevoCtx, 1)
		r = r.WithContext(nuevoCtx)

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusForbidden, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("should work", func(t *testing.T) {
		s := Server{
			readerMany:        readerManyMock{err: false},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		// Pego usuario
		nuevoCtx := contexto.SetUser(r.Context(), "user")
		// Pego comitente
		nuevoCtx = contexto.SetComitente(nuevoCtx, 1)
		r = r.WithContext(nuevoCtx)

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
}

type readerManyMock struct {
	err bool
}

func (r readerManyMock) ReadMany(context.Context, comps.ReadManyReq) ([]comps.Comp, error) {
	if r.err {
		return nil, errors.Errorf("test err")
	}
	return nil, nil
}

func TestReadOne(t *testing.T) {

	t.Run("id not numeric", func(t *testing.T) {
		s := Server{
			readerOne:         &readOneMock{},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "asd15", nil)
		r = r.WithContext(prepareCtx())
		require.Nil(t, err)
		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
	t.Run("internal", func(t *testing.T) {
		s := Server{
			readerOne:         &readOneMock{true},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "4543534534534534534534534", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
	t.Run("should work", func(t *testing.T) {
		s := Server{
			readerOne:         &readOneMock{},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "34", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())
		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
}

type readOneMock struct {
	err bool
}

func (r readOneMock) ReadOne(context.Context, comps.ReadOneReq) (comps.Comp, error) {
	if r.err {
		return comps.Comp{}, errors.Errorf("test err")
	}
	return comps.Comp{}, nil
}

func TestReadMany(t *testing.T) {

	t.Run("business layer error", func(t *testing.T) {
		s := Server{
			readerMany:        &readManyMock{err: true},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
	t.Run("nil return", func(t *testing.T) {
		s := Server{
			readerMany:        &readManyMock{nil: true},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		body := readBody(w.Result().Body)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", body)
		expected := "[]\n"
		assert.Equal(t, expected, body, "expected: %v, returned: %v", expected, body)
	})

	t.Run("empty slice", func(t *testing.T) {
		s := Server{
			readerMany:        &readManyMock{},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		body := readBody(w.Result().Body)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", body)
		expected := "[]\n"
		assert.Equal(t, expected, body, "expected: %v, returned: %v", expected, body)
	})

	t.Run("with values", func(t *testing.T) {
		s := Server{
			readerMany:        &readManyMock{withValues: true},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		body := readBody(w.Result().Body)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", body)
		assert.True(t, len(body) > 10, "short body: %v", body)
	})
}

type readManyMock struct {
	err        bool
	withValues bool
	nil        bool
}

func (r readManyMock) ReadMany(context.Context, comps.ReadManyReq) (out []comps.Comp, err error) {

	if r.err {
		return nil, errors.Errorf("testing err")
	}
	if r.nil {
		return nil, nil
	}
	if !r.withValues {
		out = []comps.Comp{}
		return
	}
	out = append(out, comps.Comp{ID: 1, Nombre: "Cuenta name"})
	return

}

func TestCreate(t *testing.T) {

	t.Run("empty body", func(t *testing.T) {
		s := Server{
			creater:           &createMock{},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader("")
		r, err := http.NewRequest(http.MethodPost, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("malformed json", func(t *testing.T) {
		s := Server{
			creater:           &createMock{},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{: ""}`,
		)
		r, err := http.NewRequest(http.MethodPost, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("business layer error", func(t *testing.T) {
		s := Server{
			creater:           &createMock{err: true},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{"Nombre": "Testing name"}`,
		)
		r, err := http.NewRequest(http.MethodPost, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("should work", func(t *testing.T) {
		s := Server{
			creater:           &createMock{err: false},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{"Nombre": "Testing name"}`,
		)
		r, err := http.NewRequest(http.MethodPost, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		body := readBody(w.Result().Body)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", body)
		assert.Equal(t, `{"ID":"343"}`+"\n", body)
	})
}

type createMock struct {
	err bool
}

func (r *createMock) Create(context.Context, comps.Comp) (int, error) {
	if r.err {
		return 0, errors.Errorf("business layer error")
	}
	return 343, nil
}

func TestDelete(t *testing.T) {

	t.Run("no id", func(t *testing.T) {
		s := Server{
			deleter:           &deleteMock{},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodDelete, "", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("invalid id", func(t *testing.T) {
		s := Server{
			deleter:           &deleteMock{},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodDelete, "invalidNumber", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("business layer error", func(t *testing.T) {
		s := Server{
			deleter:           &deleteMock{err: true},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodDelete, "453", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("should work", func(t *testing.T) {
		s := Server{
			deleter:           &deleteMock{err: false},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodDelete, "453", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
}

type deleteMock struct {
	err bool
}

func (r *deleteMock) Delete(context.Context, comps.ReadOneReq) error {
	if r.err {
		return errors.Errorf("business layer error")
	}
	return nil
}

func TestUpdate(t *testing.T) {

	t.Run("empty body", func(t *testing.T) {
		s := Server{
			updater:           &updateMock{},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader("")
		r, err := http.NewRequest(http.MethodPut, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("malformed json", func(t *testing.T) {
		s := Server{
			updater:           &updateMock{},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{: ""}`,
		)
		r, err := http.NewRequest(http.MethodPut, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("business layer error", func(t *testing.T) {
		s := Server{
			updater:           &updateMock{err: true},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{"Nombre": "Testing name"}`,
		)
		r, err := http.NewRequest(http.MethodPut, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("should work", func(t *testing.T) {
		s := Server{
			updater:           &updateMock{err: false},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{"Nombre": "Testing name"}`,
		)
		r, err := http.NewRequest(http.MethodPut, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		body := readBody(w.Result().Body)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", body)
		assert.Equal(t, "", body)
	})
}

type updateMock struct {
	err bool
}

func (r *updateMock) Update(context.Context, comps.Comp) error {
	if r.err {
		return errors.Errorf("business layer error")
	}
	return nil
}

func TestLookups(t *testing.T) {

	s := Server{
		lookerUp:          lookerUpMock{},
		withAuthorization: authMock,
	}
	w := httptest.NewRecorder()
	r, err := http.NewRequest(http.MethodGet, "lookups", strings.NewReader(""))
	r = r.WithContext(prepareCtx())
	require.Nil(t, err)
	s.ServeHTTP(w, r)
	assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", readBody(w.Result().Body))
}

type lookerUpMock struct {
	null  bool
	empty bool
}

func (m lookerUpMock) Lookup(context.Context, int) (out []comps.Lookup, err error) {
	if m.null {
		return nil, nil
	}
	if m.empty {
		return []comps.Lookup{}, nil
	}
	out = []comps.Lookup{
		{ID: 1, Nombre: "Nombre1"},
		{ID: 2, Nombre: "Nombre2"},
	}
	return
}

func TestParseReadManyQuery(t *testing.T) {
	completa := func() (out url.Values) {
		out = url.Values{}
		out["Empresa"] = []string{"1"}
		out["IDs"] = []string{"1", "2"}
		out["IncluirInactivos"] = []string{"true"}
		out["Tipo"] = []string{"Facturas"}
		return
	}

	t.Run("complete valid", func(t *testing.T) {
		out, msg, code := parseReadManyQuery(completa())
		assert.Equal(t, 0, code)
		assert.Equal(t, "", msg)
		assert.Equal(t, 1, out.Empresa)
		assert.Equal(t, tipos.GrupoInts{1, 2}, out.IDs)
		assert.Equal(t, true, out.IncluirInactivos)
		assert.Equal(t, "Facturas", out.Tipo)
	})
	t.Run("missing empresa", func(t *testing.T) {
		v := completa()
		delete(v, "Empresa")
		out, msg, code := parseReadManyQuery(v)
		assert.Equal(t, 0, code)
		assert.Equal(t, "", msg)
		assert.Equal(t, 0, out.Empresa)
		assert.Equal(t, tipos.GrupoInts{1, 2}, out.IDs)
		assert.Equal(t, true, out.IncluirInactivos)
		assert.Equal(t, "Facturas", out.Tipo)
	})
	t.Run("missing IDS", func(t *testing.T) {
		v := completa()
		delete(v, "IDs")
		out, msg, code := parseReadManyQuery(v)
		assert.Equal(t, 0, code)
		assert.Equal(t, "", msg)
		assert.Equal(t, 1, out.Empresa)
		var n tipos.GrupoInts
		assert.Equal(t, n, out.IDs)
		assert.Equal(t, true, out.IncluirInactivos)
		assert.Equal(t, "Facturas", out.Tipo)
	})
	t.Run("missing incluir inactivos", func(t *testing.T) {
		v := completa()
		delete(v, "IncluirInactivos")
		out, msg, code := parseReadManyQuery(v)
		assert.Equal(t, 0, code)
		assert.Equal(t, "", msg)
		assert.Equal(t, 1, out.Empresa)
		assert.Equal(t, tipos.GrupoInts{1, 2}, out.IDs)
		assert.Equal(t, false, out.IncluirInactivos)
		assert.Equal(t, "Facturas", out.Tipo)
	})
	t.Run("missing tipo", func(t *testing.T) {
		v := completa()
		delete(v, "Tipo")
		out, msg, code := parseReadManyQuery(v)
		assert.Equal(t, 0, code)
		assert.Equal(t, "", msg)
		assert.Equal(t, 1, out.Empresa)
		assert.Equal(t, tipos.GrupoInts{1, 2}, out.IDs)
		assert.Equal(t, true, out.IncluirInactivos)
		assert.Equal(t, "", out.Tipo)
	})

}

func authMock(in http.HandlerFunc) http.HandlerFunc {
	return in
}
func authForbiddenMock(in http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "forbidden", http.StatusForbidden)
	}
}

func readBody(r io.ReadCloser) string {
	by, err := io.ReadAll(r)
	if err != nil {
		return "error leyendo body"
	}
	return string(by)
}

// Sets user and comitente
func prepareCtx() context.Context {
	// Pego usuario
	ctx := contexto.SetUser(context.Background(), "user")
	// Pego comitente
	ctx = contexto.SetComitente(ctx, 1)
	return ctx
}
