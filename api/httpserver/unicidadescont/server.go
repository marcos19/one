package unicidadescont

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	handler "bitbucket.org/marcos19/one/pkg/unicidadescont/handler"
	"bitbucket.org/marcos19/one/pkg/unicidadescont/parafacturar"
	"bitbucket.org/marcos19/one/pkg/unicidadescont/porcantidad"
	"bitbucket.org/marcos19/one/pkg/unicidadescont/pormonto"
	"bitbucket.org/marcos19/one/pkg/unicidadescont/porop"
)

type Server struct {
	h *handler.Handler
}

func NewServer(h *handler.Handler) (s *Server) {
	s = &Server{
		h: h,
	}
	return
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string

	// Saco lo que viene después
	head, r.URL.Path = shiftPath(r.URL.Path)
	head = strings.TrimLeft(head, "/")

	// Leo usuario
	_, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Leo comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch head {

	case "apertura_por_monto":
		s.AperturaPorMonto(comitente)(w, r)

	case "apertura_por_cantidad":
		s.AperturaPorCantidad(comitente)(w, r)

	case "apertura_por_cantidad_factura":
		s.AperturaPorCantidadFactura(comitente)(w, r)

	case "ops_por_unicidad":
		s.OpsPorUnicidad(comitente)(w, r)

	default:
		httperr.NotFound(w, r)
	}
}

// OpsPorUnicidad devuelve el listado de comprobantes que tocan
// esta unicidad.
func (s *Server) OpsPorUnicidad(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := porop.Req{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.h.OpsPorUnicidad(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) AperturaPorCantidad(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := porcantidad.Req{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.h.AperturaPorCantidad(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}
		if out == nil {
			out = []porcantidad.Resp{}
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) AperturaPorCantidadFactura(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := parafacturar.Req{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.h.AperturaPorCantidadFactura(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}
		if out == nil {
			out = []porcantidad.Resp{}
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) AperturaPorMonto(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := pormonto.Req{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.h.AperturaPorMonto(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}
		if out == nil {
			out = []pormonto.Resp{}
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
