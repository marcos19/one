package oplog

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/oplog"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Server struct {
	conn *pgxpool.Pool
}

func NewServer(conn *pgxpool.Pool) *Server {
	return &Server{conn}
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	_, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Leo comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch head {

	case "por_comprobante":
		s.PorComprobante(comitente)(w, r)

	default:
		httperr.NotFound(w, r)
	}

}

func (s *Server) PorComprobante(comitente int) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := oplog.PorComprobanteReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := oplog.PorComprobante(r.Context(), s.conn, req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
