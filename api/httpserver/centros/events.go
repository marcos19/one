package centros

import (
	"fmt"

	"bitbucket.org/marcos19/one/pkg/centros"
	"bitbucket.org/marcos19/one/pkg/socket"
	"github.com/alash3al/go-pubsub"
)

func (s *Server) handleEvents(c <-chan *pubsub.Message) {
	for {
		msg, ok := <-c
		if ok {
			out := socket.Msg{
				Recurso: Programa,
			}
			comitente := 0

			switch e := msg.GetPayload().(type) {

			case centros.EvCentroCreado:
				out.Evento = socket.Created
				out.Usuario = e.Usuario
				out.Mensaje = fmt.Sprintf("%v creó centro '%v'", e.Usuario, e.Centro.Nombre)
				out.Comitente = e.Centro.Comitente
				comitente = out.Comitente

			case centros.EvCentroModificado:
				out.Evento = socket.Updated
				out.Usuario = e.Usuario
				out.Mensaje = fmt.Sprintf("%v modificó centro '%v'", e.Usuario, e.Centro.Nombre)
				out.Comitente = e.Centro.Comitente
				comitente = out.Comitente

			case centros.EvCentroEliminado:
				out.Evento = socket.Updated
				out.Usuario = e.Usuario
				out.Mensaje = fmt.Sprintf("%v borró centro '%v'", e.Usuario, e.Centro.Nombre)
				out.Comitente = e.Centro.Comitente
				comitente = out.Comitente
			}

			go s.socket.BroadcastMsgAComitente(out, comitente)
		}
	}
}
