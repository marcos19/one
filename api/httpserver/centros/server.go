package centros

import (
	"encoding/json"
	"net/http"
	"path"
	"strconv"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/centros"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/alash3al/go-pubsub"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/niler"
)

type Server struct {
	readerOne     centros.ReaderOne
	readerMany    centros.ReaderMany
	creater       centros.Creater
	updater       centros.Updater
	deleter       centros.Deleter
	eventListener <-chan *pubsub.Message
	socket        Socket
}
type Socket interface {
	BroadcastMsgAComitente(msg interface{}, comitente int)
}

const Programa = "centros"

func NewServer(h Handler, soc Socket) (s *Server, err error) {

	if niler.IsNil(h) {
		return s, errors.Errorf("Handler no puede ser nil")
	}
	s = &Server{
		readerOne:  h,
		readerMany: h,
		creater:    h,
		updater:    h,
		deleter:    h,
		socket:     soc,
	}
	s.eventListener, err = h.EventChan()
	if err != nil {
		return s, errors.Wrap(err, "creando event chan")
	}
	go s.handleEvents(s.eventListener)

	return
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	usuario, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Leo comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}
	switch r.Method {

	case http.MethodGet:

		if head == "" {
			s.readMany(comitente)(w, r)
			return
		}

		s.readOne(comitente, head)(w, r)

	case http.MethodPut:
		s.update(comitente, usuario)(w, r)

	case http.MethodPost:
		s.create(comitente, usuario)(w, r)

	case http.MethodDelete:
		s.delete(comitente, usuario, head)(w, r)

	case "many":

	case "one":

	case "nuevo":

	case "modificar":

	case "eliminar":

	default:
		http.Error(w, "método no disponible", http.StatusMethodNotAllowed)

	}
}

func (s *Server) readMany(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Busco
		out, err := s.readerMany.ReadMany(r.Context(), centros.ReadManyReq{Comitente: comitente})
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando centros de imputación"), w, r)
			return
		}

		// Devuelvo JSON
		if out == nil {
			out = []centros.Centro{}
		}
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) readOne(comitente int, head string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		if head == "" {
			http.Error(w, "no se ingresó ID", http.StatusBadRequest)
			return
		}
		// Debería ser el número de ID
		id, err := strconv.Atoi(head)
		if err != nil {
			http.Error(w, "ID inválido", http.StatusBadRequest)
			return
		}

		// Leo request
		req := centros.ReadOneReq{}
		req.Comitente = comitente
		req.ID = id

		out, err := s.readerOne.ReadOne(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando centro"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) create(comitente int, usuario string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := centros.Centro{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Creo
		req.Comitente = comitente
		id, err := s.creater.Create(r.Context(), req, usuario)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "creando centro"), w, r)
			return
		}

		out := struct {
			ID int `json:",string"`
		}{ID: id}
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) update(comitente int, usuario string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := centros.Centro{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}
		req.Comitente = comitente

		// Modifico
		err = s.updater.Update(r.Context(), req, usuario)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "modificando comitente"), w, r)
			return
		}
	}
}
func (s *Server) delete(comitente int, usuario string, head string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		if head == "" {
			http.Error(w, "no se ingresó ID", http.StatusBadRequest)
			return
		}
		// Debería ser el número de ID
		id, err := strconv.Atoi(head)
		if err != nil {
			http.Error(w, "ID inválido", http.StatusBadRequest)
			return
		}

		// Leo request
		req := centros.ReadOneReq{}
		req.Comitente = comitente
		req.ID = id

		// Borro
		err = s.deleter.Delete(r.Context(), req, usuario)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "borrando centro"), w, r)
			return
		}
	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
