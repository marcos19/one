package centros

import "bitbucket.org/marcos19/one/pkg/centros"

type Handler interface {
	centros.Creater
	centros.ReaderOne
	centros.ReaderMany
	centros.Updater
	centros.Deleter
	centros.Emitter
}
