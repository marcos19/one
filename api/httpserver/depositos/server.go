package depositos

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/depositos"
)

type Server struct {
	h *depositos.Handler
}

func NewServer(h *depositos.Handler) (s *Server) {
	s = &Server{}
	s.h = h
	return s
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	// Leo usuario
	_, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Leo usuario
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	switch head {

	case "nuevo":
		s.Create(comitente)(w, r)

	case "todos":
		s.ReadMany(comitente)(w, r)

	case "one":
		s.ReadOne(comitente)(w, r)

	case "modificar":
		s.Update(comitente)(w, r)

	default:
		httperr.NotFound(w, r)
	}
}

func (s *Server) ReadMany(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Busco
		dd, err := s.h.ReadMany(r.Context(), comitente)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Codifico
		err = json.NewEncoder(w).Encode(&dd)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) ReadOne(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo ID
		req := depositos.ReadOneReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.h.ReadOne(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Codifico
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}
	}
}

func (s *Server) Create(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo ID
		req := depositos.Deposito{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Inserto
		req.Comitente = comitente
		err = s.h.Create(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}
	}
}

// HandleModificar persiste las modificaciones a un depósito
func (s *Server) Update(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo ID
		req := depositos.Deposito{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		req.Comitente = comitente

		// Modifico
		err = s.h.Update(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}
	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
