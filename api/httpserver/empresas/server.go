package empresas

import (
	"encoding/json"
	"fmt"
	"net/http"
	"path"
	"strconv"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/niler"
)

type Server struct {
	// h           Handler
	readerOne   empresas.ReaderOne
	readerMany  empresas.ReaderMany
	creater     empresas.Creater
	updater     empresas.Updater
	deleter     empresas.Deleter
	readerLogo  empresas.ReaderLogo
	logoUpdater empresas.UpdaterLogo
	auther      Auther
}

func NewServer(h Handler, auther Auther) (s *Server, err error) {
	if niler.IsNil(h) {
		return nil, errors.Errorf("Handler no puede ser nil")
	}
	if niler.IsNil(auther) {
		return nil, errors.Errorf("Auther no puede ser nil")
	}

	s = &Server{}

	// Empresas handler
	if h == nil {
		return s, errors.New("empresas handler era nil")
	}

	s.readerOne = h
	s.readerMany = h
	s.creater = h
	s.updater = h
	s.deleter = h
	s.readerLogo = h
	s.logoUpdater = h
	s.auther = auther

	return
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Determino el id de usuario
	_, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Determino el comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	var then http.HandlerFunc

	switch head {

	case "logo":

		head, r.URL.Path = shiftPath(r.URL.Path)
		switch r.Method {

		case http.MethodGet:
			then = s.readLogo(comitente, head)

		case http.MethodPut:
			then = s.updateLogo(comitente)

		default:
			httperr.NotImplemented(w, r)
		}

	default:

		switch r.Method {

		case http.MethodGet:
			switch head {
			case "":
				then = s.ReadMany(comitente)

			default:
				then = s.readOne(comitente, head)
			}

		case http.MethodPost:
			then = s.create(comitente)

		case http.MethodPut:
			then = s.update(comitente)

		case http.MethodDelete:
			then = s.delete(comitente, head)

		default:
			http.Error(w, fmt.Sprintf(`método "%v" no implementado`, r.Method), http.StatusNotImplemented)
		}
	}

	s.auther(then)(w, r)
}

func (s *Server) create(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := empresas.Empresa{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Modifico
		req.Comitente = comitente
		out, err := s.creater.Create(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&struct {
			ID int `json:",string"`
		}{out})
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) readOne(comitente int, id string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Valido
		id, err := strconv.Atoi(id)
		if err != nil {
			httperr.BadRequest(w, errors.Errorf(`ID de empresa "%v" no es válido`, id))
			return
		}

		// Ok
		req := empresas.ReadOneReq{}
		req.Comitente = comitente
		req.ID = id

		// Busco
		out, err := s.readerOne.ReadOne(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

// Devuelve todas las empresas del comitente
func (s *Server) ReadMany(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Busco
		out, err := s.readerMany.ReadMany(r.Context(), empresas.ReadManyReq{Comitente: comitente})
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		if out == nil {
			out = []empresas.ReadManyResp{}
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) update(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := empresas.Empresa{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Modifico
		req.Comitente = comitente
		err = s.updater.Update(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}
	}
}

func (s *Server) delete(comitente int, ids string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		id, err := strconv.Atoi(ids)
		if err != nil {
			httperr.BadRequest(w, errors.Errorf(`empresa "%v" no es válida`, id))
			return
		}

		// Leo request
		req := empresas.ReadOneReq{}
		req.Comitente = comitente
		req.ID = id

		err = s.deleter.Delete(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}
	}
}

func (s *Server) updateLogo(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := empresas.UpdateLogoReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Modifico
		req.Comitente = comitente
		err = s.logoUpdater.UpdateLogo(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}
	}
}

func (s *Server) readLogo(comitente int, id string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		idInt, err := strconv.Atoi(id)
		if err != nil {
			httperr.BadRequest(w, errors.Errorf(`id de logo incorrecto: "%v"`, id))
			return
		}

		// Leo request
		req := empresas.ReadLogoReq{}
		req.Comitente = comitente
		req.Empresa = idInt

		out, err := s.readerLogo.ReadLogo(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo img
		w.Header().Set("Content-Type", "image/png")
		w.Write(out)
	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
