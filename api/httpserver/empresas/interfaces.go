package empresas

import (
	"net/http"

	"bitbucket.org/marcos19/one/pkg/empresas"
)

type Handler interface {
	empresas.Creater
	empresas.ReaderOne
	empresas.ReaderMany
	empresas.Updater
	empresas.Deleter

	empresas.ReaderLogo
	empresas.UpdaterLogo
}

// Si pasa los controles devuelve 'then'.
// Si no los pasa devuelve una handlerfunc que responde Forbidden
type Auther func(then http.HandlerFunc) http.HandlerFunc
