package empresas

import (
	"context"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestContextData(t *testing.T) {

	t.Run("missing user", func(t *testing.T) {
		s := Server{
			auther: authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "empresas/1", nil)
		require.Nil(t, err)

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusUnauthorized, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("missing comitente", func(t *testing.T) {
		s := Server{auther: authMock}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "empresas/1", nil)
		require.Nil(t, err)
		// Pego usuario
		nuevoCtx := contexto.SetUser(r.Context(), "user")
		r = r.WithContext(nuevoCtx)

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusUnauthorized, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("should work", func(t *testing.T) {
		s := Server{auther: authMock}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "empresas/1", nil)
		require.Nil(t, err)
		// Pego usuario
		nuevoCtx := contexto.SetUser(r.Context(), "user")
		// Pego comitente
		nuevoCtx = contexto.SetComitente(nuevoCtx, 1)
		r = r.WithContext(nuevoCtx)

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

}

func TestReadOne(t *testing.T) {

	t.Run("id not numeric", func(t *testing.T) {
		s := Server{
			readerOne: &readOneMock{},
			auther:    authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "asd15", nil)
		r = r.WithContext(prepareCtx())
		require.Nil(t, err)
		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
	t.Run("internal", func(t *testing.T) {
		s := Server{
			readerOne: &readOneMock{true},
			auther:    authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "4543534534534534534534534", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
	t.Run("should work", func(t *testing.T) {
		s := Server{
			readerOne: &readOneMock{},
			auther:    authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "34", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())
		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

}

type readOneMock struct {
	err bool
}

func (r *readOneMock) ReadOne(context.Context, empresas.ReadOneReq) (empresas.Empresa, error) {
	if r.err == false {
		return empresas.Empresa{}, nil
	}
	return empresas.Empresa{}, errors.Errorf("testing error")
}

func TestReadMany(t *testing.T) {

	t.Run("business layer error", func(t *testing.T) {
		s := Server{
			readerMany: &readManyMock{err: true},
			auther:     authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
	t.Run("nil return", func(t *testing.T) {
		s := Server{
			readerMany: &readManyMock{nil: true},
			auther:     authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		body := readBody(w.Result().Body)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", body)
		expected := "[]\n"
		assert.Equal(t, expected, body, "expected: %v, returned: %v", expected, body)
	})

	t.Run("empty slice", func(t *testing.T) {
		s := Server{
			readerMany: &readManyMock{},
			auther:     authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		body := readBody(w.Result().Body)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", body)
		expected := "[]\n"
		assert.Equal(t, expected, body, "expected: %v, returned: %v", expected, body)
	})

	t.Run("with values", func(t *testing.T) {
		s := Server{
			readerMany: &readManyMock{withValues: true},
			auther:     authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		body := readBody(w.Result().Body)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", body)
		assert.True(t, len(body) > 10, "short body: %v", body)
	})
}

type readManyMock struct {
	err        bool
	withValues bool
	nil        bool
}

func (r *readManyMock) ReadMany(context.Context, empresas.ReadManyReq) (out []empresas.ReadManyResp, err error) {
	if r.err {
		return nil, errors.Errorf("testing err")
	}
	if r.nil {
		return nil, nil
	}
	if !r.withValues {
		out = []empresas.ReadManyResp{}
		return
	}
	out = append(out, empresas.ReadManyResp{ID: 1, Nombre: "Empresa name"})
	return
}

func TestCreate(t *testing.T) {

	t.Run("empty body", func(t *testing.T) {
		s := Server{
			creater: &createMock{},
			auther:  authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader("")
		r, err := http.NewRequest(http.MethodPost, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("malformed json", func(t *testing.T) {
		s := Server{
			creater: &createMock{},
			auther:  authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{: ""}`,
		)
		r, err := http.NewRequest(http.MethodPost, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("business layer error", func(t *testing.T) {
		s := Server{
			creater: &createMock{err: true},
			auther:  authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{"Nombre": "Testing name"}`,
		)
		r, err := http.NewRequest(http.MethodPost, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("should work", func(t *testing.T) {
		s := Server{
			creater: &createMock{err: false},
			auther:  authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{"Nombre": "Testing name"}`,
		)
		r, err := http.NewRequest(http.MethodPost, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		body := readBody(w.Result().Body)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", body)
		assert.Equal(t, `{"ID":"343"}`+"\n", body)
	})
}

type createMock struct {
	err bool
}

func (r *createMock) Create(context.Context, empresas.Empresa) (int, error) {
	if r.err {
		return 0, errors.Errorf("business layer error")
	}
	return 343, nil
}

func TestDelete(t *testing.T) {

	t.Run("no id", func(t *testing.T) {
		s := Server{
			deleter: &deleteMock{},
			auther:  authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodDelete, "", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("invalid id", func(t *testing.T) {
		s := Server{
			deleter: &deleteMock{},
			auther:  authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodDelete, "invalidNumber", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("business layer error", func(t *testing.T) {
		s := Server{
			deleter: &deleteMock{err: true},
			auther:  authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodDelete, "453", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("should work", func(t *testing.T) {
		s := Server{
			deleter: &deleteMock{err: false},
			auther:  authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodDelete, "453", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
}

type deleteMock struct {
	err bool
}

func (r *deleteMock) Delete(context.Context, empresas.ReadOneReq) error {
	if r.err {
		return errors.Errorf("business layer error")
	}
	return nil
}

func TestUpdate(t *testing.T) {

	t.Run("empty body", func(t *testing.T) {
		s := Server{
			updater: &updateMock{},
			auther:  authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader("")
		r, err := http.NewRequest(http.MethodPut, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("malformed json", func(t *testing.T) {
		s := Server{
			updater: &updateMock{},
			auther:  authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{: ""}`,
		)
		r, err := http.NewRequest(http.MethodPut, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("business layer error", func(t *testing.T) {
		s := Server{
			updater: &updateMock{err: true},
			auther:  authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{"Nombre": "Testing name"}`,
		)
		r, err := http.NewRequest(http.MethodPut, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("should work", func(t *testing.T) {
		s := Server{
			updater: &updateMock{err: false},
			auther:  authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{"Nombre": "Testing name"}`,
		)
		r, err := http.NewRequest(http.MethodPut, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		body := readBody(w.Result().Body)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", body)
		assert.Equal(t, "", body)
	})
}

type updateMock struct {
	err bool
}

func (r *updateMock) Update(context.Context, empresas.Empresa) error {
	if r.err {
		return errors.Errorf("business layer error")
	}
	return nil
}

func TestLogoRead(t *testing.T) {

	t.Run("empty id", func(t *testing.T) {
		s := Server{
			readerLogo: &readerLogoMock{},
			auther:     authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "logo", nil)
		r = r.WithContext(prepareCtx())
		require.Nil(t, err)
		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
	t.Run("id not numeric", func(t *testing.T) {
		s := Server{
			readerLogo: &readerLogoMock{},
			auther:     authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "logo/notValid", nil)
		r = r.WithContext(prepareCtx())
		require.Nil(t, err)
		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
	t.Run("business layer error", func(t *testing.T) {
		s := Server{
			readerLogo: &readerLogoMock{true},
			auther:     authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "logo/324", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("should work", func(t *testing.T) {
		s := Server{
			readerLogo: &readerLogoMock{},
			auther:     authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "logo/324", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())
		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

}

type readerLogoMock struct {
	err bool
}

func (r *readerLogoMock) ReadLogo(context.Context, empresas.ReadLogoReq) ([]byte, error) {
	if r.err {
		return nil, errors.Errorf("testing error")
	}
	return []byte("this is an image"), nil
}

func TestLogoUpdate(t *testing.T) {

	t.Run("empty body", func(t *testing.T) {
		s := Server{
			logoUpdater: &updaterLogoMock{},
			auther:      authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodPut, "logo", strings.NewReader(""))
		r = r.WithContext(prepareCtx())
		require.Nil(t, err)
		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
	t.Run("different types", func(t *testing.T) {
		s := Server{
			logoUpdater: &updaterLogoMock{},
			auther:      authMock,
		}
		w := httptest.NewRecorder()
		body := strings.NewReader(`{"Empresa":1}`)
		r, err := http.NewRequest(http.MethodPut, "logo", body)
		r = r.WithContext(prepareCtx())
		require.Nil(t, err)
		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
	t.Run("business layer error", func(t *testing.T) {
		s := Server{
			logoUpdater: &updaterLogoMock{true},
			auther:      authMock,
		}
		w := httptest.NewRecorder()
		body := strings.NewReader(`{"Empresa":"1"}`)
		r, err := http.NewRequest(http.MethodPut, "logo", body)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("should work", func(t *testing.T) {
		s := Server{
			logoUpdater: &updaterLogoMock{},
			auther:      authMock,
		}
		w := httptest.NewRecorder()
		body := strings.NewReader(`{"Empresa":"1", "Imagen": "b64msg"}`)
		r, err := http.NewRequest(http.MethodPut, "logo", body)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())
		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

}

type updaterLogoMock struct {
	err bool
}

func (r *updaterLogoMock) UpdateLogo(context.Context, empresas.UpdateLogoReq) error {
	if r.err {
		return errors.Errorf("testing error")
	}
	return nil
}

func authMock(then http.HandlerFunc) http.HandlerFunc {
	return then
}

func readBody(r io.ReadCloser) string {
	by, err := io.ReadAll(r)
	if err != nil {
		return "error leyendo body"
	}
	return string(by)
}

// Sets user and comitente
func prepareCtx() context.Context {
	// Pego usuario
	ctx := contexto.SetUser(context.Background(), "user")
	// Pego comitente
	ctx = contexto.SetComitente(ctx, 1)
	return ctx
}
