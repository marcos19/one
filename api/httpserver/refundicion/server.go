package refundicion

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/refundicion"
	uuid "github.com/gofrs/uuid"
)

type Server struct {
	h *refundicion.Handler
}

func NewServer(h *refundicion.Handler) (s *Server) {
	s = &Server{}
	s.h = h
	return s
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	user, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Leo comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch head {

	case "vista_previa":
		s.VistaPrevia(comitente, user)(w, r)

	case "confirmar":
		s.Confirmar(comitente, user)(w, r)

	default:
		deferror.Frontend(deferror.NotFound(head), w, r)
	}
}
func (s *Server) VistaPrevia(comitente int, usuario string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := refundicion.VistaPreviaReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Creo
		req.Comitente = comitente
		req.Usuario = usuario
		out, err := s.h.VistaPrevia(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Devuelvo ID
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) Confirmar(comitente int, usuario string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := refundicion.ConfirmarReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Creo
		req.Comitente = comitente
		req.Usuario = usuario
		out, err := s.h.Confirmar(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Devuelvo ID
		err = json.NewEncoder(w).Encode(&struct{ ID uuid.UUID }{ID: out})
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
