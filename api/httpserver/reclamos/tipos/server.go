package tipos

import (
	"encoding/json"
	"fmt"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/reclamos/tipos"
)

type Server struct {
	h *tipos.Handler
}

func NewServer(h *tipos.Handler) (out *Server, err error) {
	if h == nil {
		return out, fmt.Errorf("missing handler")
	}
	out = &Server{
		h: h,
	}
	return
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	// Leo usuario
	_, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Leo comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch r.Method {

	case http.MethodGet:
		s.readMany(comitente)(w, r)

	//case http.MethodPost:
	//s.create(comitente)(w, r)

	default:
		http.Error(w, fmt.Sprintf(`método "%v" no permitido`, r.Method), http.StatusNotImplemented)
		return
	}
}

func (s *Server) readMany(comitente int) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		out, err := s.h.ReadMany(r.Context(), comitente)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}
		if out == nil {
			out = []tipos.Tipo{}
		}

		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

//func (s *Server) create(comitente int) http.HandlerFunc {
//return func(w http.ResponseWriter, r *http.Request) {

//// Read request
//req := reclamos.Reclamo{}
//err := json.NewDecoder(r.Body).Decode(&req)
//if err != nil {
//httperr.BadRequest(w, err)
//return
//}

//// Create
//err = s.h.Create(r.Context(), &req)
//if err != nil {
//httperr.BadRequest(w, err)
//return
//}
//}
//}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
