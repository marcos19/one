package reclamos

import (
	"encoding/json"
	"fmt"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	tiposHTTP "bitbucket.org/marcos19/one/api/httpserver/reclamos/tipos"
	"bitbucket.org/marcos19/one/pkg/reclamos"
	"bitbucket.org/marcos19/one/pkg/reclamos/tipos"
	"github.com/gofrs/uuid"
)

type Server struct {
	h     *reclamos.Handler
	tipoH *tiposHTTP.Server
}

func NewServer(h *reclamos.Handler, tipoH *tipos.Handler) (out *Server, err error) {
	if h == nil {
		return out, fmt.Errorf("missing handler")
	}

	tipoServer, err := tiposHTTP.NewServer(tipoH)
	if err != nil {
		return out, fmt.Errorf("creating reclamos_tipos handler")
	}

	out = &Server{
		h:     h,
		tipoH: tipoServer,
	}
	return
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	// Leo usuario
	usuario, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Leo comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	head, _ := shiftPath(r.URL.Path)
	if head == "tipos" {
		s.tipoH.ServeHTTP(w, r)
		return
	}

	switch r.Method {

	case http.MethodGet:
		s.readMany(comitente)(w, r)

	case http.MethodPost:
		s.create(comitente, usuario)(w, r)

	default:
		http.Error(w, fmt.Sprintf(`método "%v" no permitido`, r.Method), http.StatusNotImplemented)
		return
	}
}

func (s *Server) readMany(comitente int) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		// Read request query
		persona := r.URL.Query().Get("persona")
		if len(persona) == 0 {
			httperr.BadRequest(w, fmt.Errorf("no se ingresó persona"))
			return
		}
		u, err := uuid.FromString(persona)
		if err != nil {
			httperr.BadRequest(w, fmt.Errorf("leyendo persona query: %w", err))
			return
		}

		req := reclamos.ReadManyReq{
			Comitente: comitente,
			Persona:   u,
		}

		out, err := s.h.ReadMany(r.Context(), req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}
		if out == nil {
			out = []reclamos.ReadManyResp{}
		}

		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) create(comitente int, usuario string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		// Read request
		req := reclamos.Reclamo{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}
		req.Comitente = comitente
		req.Usuario = usuario

		// Create
		err = s.h.Create(r.Context(), &req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}
	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
