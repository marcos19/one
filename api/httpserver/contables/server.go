package contables

import (
	"encoding/csv"
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/contables/aplifin"
	aplifinhandler "bitbucket.org/marcos19/one/pkg/contables/aplifin/handler"
	"bitbucket.org/marcos19/one/pkg/contables/balance"
	"bitbucket.org/marcos19/one/pkg/contables/librodiario"
	"bitbucket.org/marcos19/one/pkg/contables/mayor"
	"bitbucket.org/marcos19/one/pkg/contables/mayorproductos"
	"bitbucket.org/marcos19/one/pkg/contables/resumencaja"
	"bitbucket.org/marcos19/one/pkg/contables/saldospersonas"
	"bitbucket.org/marcos19/one/pkg/contables/saldosproductos"
	"bitbucket.org/marcos19/one/pkg/contables/vistapersona"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/niler"
)

// Server es el encargado de recibir los http requests y emitir los responses.
type Server struct {
	aplifin         *aplifinhandler.Handler
	balance         *balance.Handler
	libroDiario     *librodiario.Handler
	mayor           *mayor.Handler
	mayorProductos  *mayorproductos.Handler
	resumenCaja     *resumencaja.Handler
	saldosPersonas  *saldospersonas.Handler
	saldosProductos *saldosproductos.Handler
	vistaPersona    *vistapersona.Handler
}

// NewHandler incializa un Handler.
func NewServer(
	aplifin *aplifinhandler.Handler,
	balance *balance.Handler,
	libroDiario *librodiario.Handler,
	mayor *mayor.Handler,
	mayorProductos *mayorproductos.Handler,
	resumenCaja *resumencaja.Handler,
	saldosPersonas *saldospersonas.Handler,
	saldosProductos *saldosproductos.Handler,
	vistaPersona *vistapersona.Handler,

) (h *Server, err error) {
	if niler.IsNil(aplifin) {
		return h, errors.Errorf("aplifin no puede ser nil")
	}
	if niler.IsNil(balance) {
		return h, errors.Errorf("balance no puede ser nil")
	}
	if niler.IsNil(libroDiario) {
		return h, errors.Errorf("libroDiario no puede ser nil")
	}
	if niler.IsNil(mayor) {
		return h, errors.Errorf("mayor no puede ser nil")
	}
	if niler.IsNil(mayorProductos) {
		return h, errors.Errorf("mayorProductos no puede ser nil")
	}
	if niler.IsNil(resumenCaja) {
		return h, errors.Errorf("resumenCaja no puede ser nil")
	}
	if niler.IsNil(saldosPersonas) {
		return h, errors.Errorf("saldosPersonas no puede ser nil")
	}
	if niler.IsNil(saldosProductos) {
		return h, errors.Errorf("saldosProductoos no puede ser nil")
	}
	if niler.IsNil(vistaPersona) {
		return h, errors.Errorf("vistaPersona no puede ser nil")
	}
	h = &Server{
		aplifin:         aplifin,
		balance:         balance,
		libroDiario:     libroDiario,
		mayor:           mayor,
		resumenCaja:     resumenCaja,
		saldosPersonas:  saldosPersonas,
		saldosProductos: saldosProductos,
		vistaPersona:    vistaPersona,
		mayorProductos:  mayorProductos,
	}
	return
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	_, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Leo comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch head {
	// /backend/contables
	case "asiento":

	case "balance":
		s.BalanceDesdeSaldos(comitente)(w, r)

	case "balance_csv":
		s.BalanceCSV(comitente)(w, r)

	case "balance_xlsx":
		s.BalanceXLSX(comitente)(w, r)

	case "mayor":
		s.Mayor(comitente)(w, r)

	case "libro_diario":
		s.LibroDiario(comitente)(w, r)

	case "saldos_personas":
		s.SaldosPersonas(comitente)(w, r)

	case "saldos_personas_excel":
		s.SaldoPersonaExcel(comitente)(w, r)

	case "saldos_personas_siap":
		s.SaldosPersonasSIAP(comitente)(w, r)

	case "saldo_persona":
		s.SaldoPersona(comitente)(w, r)

	case "saldos_productos":
		s.SaldosProductos(comitente)(w, r)

	case "mayor_productos":
		s.MayorProductos(comitente)(w, r)

	case "partidas_disponibles":
		s.PartidasDisponibles(comitente)(w, r)

	case "vista_persona_cuenta_corriente":
		s.VistaPersonaCuentaCorriente(comitente)(w, r)

	case "vista_persona_movimientos":
		s.VistaPersonaMovimientos(comitente)(w, r)

	case "vista_persona_movimientos_pdf":
		s.VistaPersonaMovimientosPDF(comitente)(w, r)

	case "vista_persona_ventas":
		s.VistaPersonaVentas(comitente)(w, r)

	case "resumen_de_caja":
		s.ResumenDeCaja(comitente)(w, r)

	//case "resumen_otras_cuentas":
	//s.ResumenOtrasCuentas(comitente)(w, r)

	default:
		deferror.Frontend(deferror.NotFound(""), w, r)
	}
}

// BalanceCSV devuelve el balance desde la tabla de saldos
func (s *Server) BalanceCSV(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		req := balance.BalanceReq{}

		// Leo request
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Tiro balance
		req.Comitente = comitente
		out, err := s.balance.BalanceCSV(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Pongo headers
		w.Header().Set("Content-Type", "text/csv")
		w.Header().Set("Content-Disposition", "attachment; filename=balance.csv")

		// Codifico CSV
		err = csv.NewWriter(w).WriteAll(out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// BalanceXLSX devuelve el balance en Excel.
func (s *Server) BalanceXLSX(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		req := balance.BalanceReq{}

		// Leo request
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Pongo headers
		w.Header().Set("Content-Type", "application/octet-stream")
		w.Header().Set("Content-Disposition", "attachment; filename=balance.xlsx")
		w.Header().Set("Content-Transfer-Encoding", "binary")

		// Tiro balance
		req.Comitente = comitente
		err = s.balance.BalanceXLSX(r.Context(), w, req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// HandleBalanceDesdeSaldos devuelve el balance desde la tabla de saldos
func (s *Server) BalanceDesdeSaldos(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := balance.BalanceReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Tiro balance
		req.Comitente = comitente
		bce, err := s.balance.TirarBalance(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&bce)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) Mayor(comitente int) http.HandlerFunc {

	type req struct {
		mayor.Filtro
		Output string
	}

	return func(w http.ResponseWriter, r *http.Request) {

		req := req{}

		// Leo request
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Tiro mayor
		req.Comitente = comitente
		out, err := s.mayor.Mayorizar(r.Context(), req.Filtro)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "tirando libro mayor"), w, r)
			return
		}

		switch req.Output {
		case "", "json":
			// JSON
			err = json.NewEncoder(w).Encode(&out)
			if err != nil {
				deferror.Frontend(err, w, r)
				return
			}

		case "csv":

			enCSV := out.CSV()

			// Codifico CSV
			w.Header().Set("Content-Type", "text/csv")
			w.Header().Set("Content-Disposition", "attachment; filename=mayor.csv")

			err = csv.NewWriter(w).WriteAll(enCSV)
			if err != nil {
				deferror.Frontend(err, w, r)
				return
			}

		case "excel":
			w.Header().Set("Content-Type", "application/octet-stream")
			w.Header().Set("Content-Disposition", "attachment; filename=mayor.xlsx")
			w.Header().Set("Content-Transfer-Encoding", "binary")
			err = out.Excel(w)
			if err != nil {
				deferror.Frontend(err, w, r)
				return
			}

		default:
			if err != nil {
				deferror.Frontend(deferror.BadRequest(errors.Errorf("salida '%v' no implementada", req.Output)), w, r)
				return
			}
		}
	}
}

func (s *Server) LibroDiario(comitente int) http.HandlerFunc {

	type req struct {
		librodiario.LibroDiarioReq
		Salida string
	}
	return func(w http.ResponseWriter, r *http.Request) {

		req := req{}

		// Leo request
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Tiro libro
		req.Comitente = comitente
		out, err := s.libroDiario.GenerarLibroDiario(r.Context(), req.LibroDiarioReq)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "generando libro diario"), w, r)
			return
		}

		switch req.Salida {

		case "JSON":
			err = json.NewEncoder(w).Encode(&out)
			if err != nil {
				deferror.Frontend(errors.Wrap(err, "codificando JSON"), w, r)
				return
			}

		case "PDF":
			w.Header().Set("Content-Type", "application/pdf")
			w.Header().Set("Content-Disposition", "attachment; filename=diario.pdf")

			err = out.PDF(w)
			if err != nil {
				deferror.Frontend(errors.Wrap(err, "generando PDF de libro diario"), w, r)
				return
			}

		default:
			deferror.Frontend(errors.New("salida "+req.Salida+" no implementada"), w, r)
			return
		}

	}
}

func (s *Server) ResumenDeCaja(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		req := resumencaja.Req{}

		// Leo request
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Tiro listado
		req.Comitente = comitente
		out, err := s.resumenCaja.Informe(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "tirando resumen de caja"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

//func (s *Server) ResumenOtrasCuentas(comitente int) http.HandlerFunc {

//return func(w http.ResponseWriter, r *http.Request) {

//req := resumencaja.ResumenCajaReq{}

//// Leo request
//err := json.NewDecoder(r.Body).Decode(&req)
//if err != nil {
//deferror.Frontend(err, w, r)
//return
//}

//// Tiro listado
//req.Comitente = comitente
//out, err := s.resumenCaja.TirarOtrasCuentas(r.Context(), req)
//if err != nil {
//deferror.Frontend(errors.Wrap(err, "tirando otras caja"), w, r)
//return
//}

//if out.Movimientos == nil {
//out.Movimientos = []resumencaja.MovimientoDeCaja{}
//}
//if out.Valores == nil {
//out.Valores = []resumencaja.ValorEnCaja{}
//}
//// Devuelvo JSON
//err = json.NewEncoder(w).Encode(&out)
//if err != nil {
//deferror.Frontend(err, w, r)
//return
//}
//}
//}

// HandleSaldosPersonas devuelve los saldos financieros y contables de las personas.
func (s *Server) SaldosPersonas(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		req := saldospersonas.SaldosPersonasReq{}

		// Leo request
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Tiro listado
		req.Comitente = comitente
		ss, err := s.saldosPersonas.SaldosPersonas(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "tirando saldos de personas"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&ss)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) SaldosPersonasSIAP(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		req := saldospersonas.SiapReq{}

		// Leo request
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Tiro listado
		req.Comitente = comitente
		ss, err := s.saldosPersonas.ConSaldoInicioSIAP(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Devuelvo STRING
		_, err = w.Write([]byte(ss))
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// SaldoPersona devuelve los saldos financieros y contables de
// las persona. Se usa al facturar. No tiene en cuenta empresa.
func (s *Server) SaldoPersona(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		req := saldospersonas.PersonaReq{}

		// Leo request
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Tiro listado
		req.Comitente = comitente
		ss, err := s.saldosPersonas.Persona(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "tirando saldo de persona"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&ss)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// SaldoPersonaExcel devuelve un Excel con los saldos financieros y contables de las personas.
func (s *Server) SaldoPersonaExcel(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		req := saldospersonas.SaldosPersonasReq{}

		// Leo request
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Tiro listado
		req.Comitente = comitente
		out, err := s.saldosPersonas.ExportarExcel(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "tirando saldo de persona"), w, r)
			return
		}

		w.Header().Set("Content-Type", "application/octet-stream")
		w.Header().Set("Content-Disposition", "attachment; filename=mayor.xlsx")
		w.Header().Set("Content-Transfer-Encoding", "binary")
		err = out.Write(w)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "encoding response"), w, r)
			return
		}
	}
}
func (s *Server) VistaPersonaCuentaCorriente(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := vistapersona.CuentaCorrienteReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Tiro listado
		req.Comitente = comitente
		vista, err := s.vistaPersona.CuentaCorriente(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "tirando listado VistaPersonaCuentaCorriente"), w, r)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&vista)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) VistaPersonaMovimientos(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := vistapersona.MovimientosReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Tiro vista
		req.Comitente = comitente
		vista, err := s.vistaPersona.Movimientos(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&vista)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

	}
}

func (s *Server) VistaPersonaMovimientosPDF(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := vistapersona.MovimientosReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Tiro vista
		req.Comitente = comitente
		outputer, err := s.vistaPersona.MovimientosPDF(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Exporto
		w.Header().Set("Content-Type", "application/pdf")
		w.Header().Set("Content-Disposition", "attachment; filename=factura.pdf")
		err = outputer.Output(w)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "generando PDF"), w, r)
		}
	}
}

func (s *Server) VistaPersonaVentas(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := vistapersona.VentasReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Tiro vista
		req.Comitente = comitente
		out, err := s.vistaPersona.Ventas(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// HandleSaldosProductos devuelve el listado de productos en stock
func (s *Server) SaldosProductos(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo JSON
		req := saldosproductos.SaldosProductosReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.saldosProductos.SaldosProductos(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando saldos"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) MayorProductos(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo JSON
		req := mayorproductos.Req{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.mayorProductos.Mayorizar(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando saldos"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// HandlePartidasDisponibles busca dentro de los saldos cuales son las partidas
// que están disponibles para ser aplicadas.
func (s *Server) PartidasDisponibles(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := aplifin.PartidasDisponiblesReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		pp, err := s.aplifin.PartidasDisponibles(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "trayendo partidas disponibles"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&pp)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
