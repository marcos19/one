package resbancario

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/resbancario"
)

// Handler da acceso al a todos los routes de personas.
type Server struct {
	h *resbancario.Handler
}

// NewHandler inicializa un cache y devuelve el puntero a este.
func NewServer(h *resbancario.Handler) (s *Server) {
	return &Server{h}
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	_, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Determino el comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch head {
	case "create":
		s.Create(comitente)(w, r)

	case "one":
		s.ReadOne(comitente)(w, r)

	case "many":
		s.ReadMany(comitente)(w, r)

		// Queda bloqueando transacción
	// case "update":
	// 	s.Update(comitente)(w, r)

	case "delete":
		s.Delete(comitente)(w, r)

	case "bancos":
		s.HandleBancos(comitente)(w, r)

	case "leer_csv":
		s.LeerCSV(comitente)(w, r)

	case "fijar_movimiento":
		s.FijarMovimiento(comitente)(w, r)

	default:
		httperr.NotFound(w, r)
		return
	}
}

func (s *Server) Create(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := resbancario.CreateReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Inserto
		req.Comitente = comitente
		res, err := s.h.Create(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&res)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

// HandleMany devuelve on resumen bancario por su ID
func (s *Server) ReadMany(comitente int) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		// Leo JSON
		req := resbancario.ReadManyReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.h.ReadMany(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

// HandleOne devuelve on resumen bancario por su ID
func (s *Server) ReadOne(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo JSON
		req := resbancario.ReadOneReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Busco el resumen
		req.Comitente = comitente
		out, err := s.h.ReadOne(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) Update(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := resbancario.CreateReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Inserto
		req.Comitente = comitente
		res, err := s.h.Update(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&res)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) Delete(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := resbancario.DeleteReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Borro
		req.Comitente = comitente
		err = s.h.Delete(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

	}
}

// FijarMovimiento fija en la configuración la imputación contable
// para el codigo de movimiento y devuelve todo el resumen completo.
func (s *Server) FijarMovimiento(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := resbancario.FijarMovReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Fijo
		req.Comitente = comitente
		pp, err := s.h.FijarMov(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo las partidas con la nueva imputación corregida
		err = json.NewEncoder(w).Encode(&pp)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

// LeerCSV lee el archivo CSV y devuelve las partidas imputadas y ops
func (s *Server) LeerCSV(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		req := resbancario.LeerCSVReq{}

		// Leo request
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Leo CSV
		req.Comitente = comitente
		out, err := s.h.LeerCSV(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

// HandleBancos devuelve aquellas personas que tienen asociada la
// la cuenta contable definida como cuenta bancaria.
func (s *Server) HandleBancos(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Busco bancos
		out, err := s.h.Bancos(r.Context(), comitente)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

// ShiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
