package productos

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/productos"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/um"
	"github.com/gofrs/uuid"
)

// Handler es el encargado de recibir los http requests y emitir los responses.
type Server struct {
	h *productos.Handler
}

// NewHandler inicializa un cache y devuelve el puntero a este.
func NewServer(handler *productos.Handler) (s *Server, err error) {
	if handler == nil {
		return s, errors.New("ProductosHandler no puede ser null")
	}
	s = &Server{handler}
	return
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	_, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Leo comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch head {

	// Productos

	case "insertar":
		s.Create(comitente)(w, r)

	case "one":
		s.ReadOne(comitente)(w, r)

	case "many":
		s.ReadMany(comitente)(w, r)

	case "modificar":
		s.Update(comitente)(w, r)

	case "eliminar":
		s.Delete(comitente)(w, r)

	// SKUs

	case "insertar_sku":
		s.CreateSKU(comitente)(w, r)

	case "skus":
		s.ReadManySKU(comitente)(w, r)

	case "modificar_sku":
		s.UpdateSKU(comitente)(w, r)

	case "eliminar_sku":
		s.DeleteSKU(comitente)(w, r)

	// Imagenes

	// case "grabar_imagen_sku":
	// 	s.CreateImgSKU(comitente)(w, r)

	// case "grabar_imagen_producto":
	// 	s.CreateImg(comitente)(w, r)

	// case "read_one_img":
	// 	s.ReadOneImg()(w, r)

	case "ums":
		s.UMs()(w, r)

	default:
		// Error
		httperr.NotFound(w, r)
	}
}

func (s *Server) Create(comitente int) http.HandlerFunc {

	// Devuelvo el ID para ser usado
	type response struct {
		ID uuid.UUID
	}

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := productos.CreateReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Creo
		req.Comitente = comitente
		id, err := s.h.Create(r.Context(), &req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo ID
		response := response{ID: id}
		err = json.NewEncoder(w).Encode(&response)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

// HandleBuscarPorID devuelve el producto solicitado.
func (s *Server) ReadOne(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		req := productos.ReadOneReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		req.Comitente = comitente
		// Busco el producto
		c, err := s.h.ReadOne(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		err = json.NewEncoder(w).Encode(&c)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) ReadMany(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := productos.ReadManyReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Busco
		req.Comitente = comitente
		cc, err := s.h.ReadMany(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&cc)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

// HandleModificar
func (s *Server) Update(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := productos.Producto{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Modifico
		req.Comitente = comitente
		err = s.h.Update(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
		}
	}
}

// HandleEliminar elimina el producto
func (s *Server) Delete(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := productos.DeleteReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Borro
		req.Comitente = comitente
		err = s.h.Delete(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}
	}
}

func (s *Server) UMs() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		cc := um.Medidas()

		// Devuelvo JSON
		err := json.NewEncoder(w).Encode(&cc)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
