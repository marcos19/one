package productos

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/productos"
	"github.com/cockroachdb/errors"
)

func (s *Server) CreateSKU(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		c := productos.SKU{}
		err := json.NewDecoder(r.Body).Decode(&c)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Inserto
		req := productos.CreateSKUReq{
			SKU:       c,
			Comitente: comitente,
		}
		vv, err := s.h.CreateSKU(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&vv)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

// HandleSKUs devuelve todos los SKUS ue hay para el producto.
func (s *Server) ReadManySKU(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := productos.ReadManySKUReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Busco
		req.Comitente = comitente
		cc, err := s.h.ReadManySKU(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&cc)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

// HandleModificarSKU modifica un SKU y los devuelve todos.
func (s *Server) UpdateSKU(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := productos.SKU{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Modifico
		err = s.h.UpdateSKU(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo los SKU modificados
		reqRead := productos.ReadManySKUReq{
			ID:        req.Producto,
			Comitente: comitente,
		}
		vv, err := s.h.ReadManySKU(r.Context(), reqRead)
		if err != nil {
			httperr.InternalServerError(w, errors.Wrap(err, "buscando variantes modificadas"))
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&vv)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

// HandleEliminarSKU modifica un SKU y los devuelve todos.
func (s *Server) DeleteSKU(comitente int) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := productos.DeleteSKUReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Borro
		req.Comitente = comitente
		err = s.h.DeleteSKU(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo los SKU que quedan
		reqRead := productos.ReadManySKUReq{
			ID:        req.Producto,
			Comitente: comitente,
		}
		vv, err := s.h.ReadManySKU(r.Context(), reqRead)
		if err != nil {
			httperr.InternalServerError(w, errors.Wrap(err, "buscando skus"))
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&vv)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}
