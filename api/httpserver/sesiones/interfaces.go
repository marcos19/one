package sesiones

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/sesiones"
	"bitbucket.org/marcos19/one/pkg/usuarios"
)

type Tokener interface {
	Creater
	Toucher
	Invalidater
	ComitenteSetter
}

type Creater interface {
	NewToken(userID string) (nuevoToken string, err error)
}
type Toucher interface {
	TouchTime(token string) (nuevoToken string, f sesiones.Fields, err error)
}
type Invalidater interface {
	InvalidateToken(token string) (nuevoToken string, err error)
}
type ComitenteSetter interface {
	SetComitente(token string, comitente int) (nuevoToken string, err error)
}

// Consulta si no los datos ingresados permiten que el usuario
// se haga de un token para ingresar.
type Sessioner interface {
	Login(context.Context, usuarios.LoginReq) error
}

type LoginReq struct {
	UserID    string
	Pass      string
	Comitente int `json:",string"`
}

type LoginResponse struct {
	ID       string
	Nombre   string
	Apellido string
}
