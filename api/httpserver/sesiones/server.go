// Package sesiones maneja las sesiones como package, no es un web server
package sesiones

import (
	"encoding/json"
	"net/http"
	"path"
	"strconv"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/sesiones"
	"bitbucket.org/marcos19/one/pkg/usuarios"
	"github.com/crosslogic/niler"
	"github.com/pkg/errors"
)

type Server struct {
	sessioner       Sessioner
	creater         Creater
	toucher         Toucher
	invalidater     Invalidater
	comitenteSetter ComitenteSetter
	usuariosReader  usuarios.ReaderOne
}

func NewServer(ses Sessioner, tok Tokener, usu usuarios.ReaderOne) (s *Server, err error) {
	if niler.IsNil(ses) {
		return s, errors.New("Sessioner no puede ser nil")
	}
	if niler.IsNil(tok) {
		return s, errors.New("Tokener no puede ser nil")
	}
	if niler.IsNil(usu) {
		return s, errors.New("usuarios.ReaderOne no puede ser nil")
	}
	s = &Server{
		creater:         tok,
		toucher:         tok,
		invalidater:     tok,
		comitenteSetter: tok,
		sessioner:       ses,
		usuariosReader:  usu,
	}
	return
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	user, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}
	switch r.Method {
	case http.MethodPost:
		switch head {
		case "comitente":
			id, _ := shiftPath(r.URL.Path)
			s.SetComitente(user, id)(w, r)
		}
	default:
		httperr.NotFound(w, r)
	}
}

// Login devuelve una HandlerFunc que corrobora usuario y contraseña y si pasa
// le pega una cookie.
func (s *Server) Login(w http.ResponseWriter, r *http.Request) {

	// Leo request
	req := usuarios.LoginReq{}
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		deferror.Frontend(deferror.BadRequest(err), w, r)
		return
	}

	// Está ok para loggear?
	err = s.sessioner.Login(r.Context(), req)
	if err != nil {
		switch t := err.(type) {

		case usuarios.ErrContraseñaIncorrecta:
			http.Error(w, t.Frontend(), http.StatusUnauthorized)
			return

		case usuarios.ErrCorrespondeCambiar:
			http.Error(w, t.Frontend(), http.StatusUnauthorized)
			return

		case usuarios.ErrUsuarioNoConfirmado:
			http.Error(w, t.Frontend(), http.StatusUnauthorized)
			return

		case sesiones.ErrMuchosComitentes:
			// ok

		default:
			deferror.Frontend(err, w, r)
		}
		return
	}

	// Genero token
	token, err := s.creater.NewToken(req.UserID)
	if err != nil {
		http.Error(w, errors.Wrap(err, "creando token").Error(), http.StatusInternalServerError)
		return
	}

	// Determino comitentes disponibles
	u, err := s.usuariosReader.ReadOne(r.Context(), req.UserID)
	if err != nil {
		deferror.Frontend(err, w, r)
		return
	}
	if len(u.Comitentes) == 0 {
		deferror.Frontend(errors.Errorf("el usuario no tiene asignado ningún comitente"), w, r)
		return
	}
	muchosComitentes := false
	if len(u.Comitentes) > 1 {
		muchosComitentes = true
	}

	if !muchosComitentes { // Fijo comitente
		token, err = s.comitenteSetter.SetComitente(token, u.Comitentes[0])
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}

	// Pego token a response (aunque no tenga comitente)
	setToken(w, token)
	w.Header().Set("Content-Type", "application/json")

	if muchosComitentes {
		http.Error(w, sesiones.ErrMuchosComitentes{}.Frontend(), http.StatusUnauthorized)
		return
	}

	out := LoginResponse{}
	out.Apellido = u.Apellido
	out.Nombre = u.Nombre
	out.ID = u.ID

	// Devuelvo datos del usuario para que los use el frontend
	err = json.NewEncoder(w).Encode(&out)
	if err != nil {
		deferror.Frontend(err, w, r)
		return
	}

}

func (s *Server) SetComitente(user string, comitente string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		if comitente == "" {
			http.Error(w, "no se ingresó ID de comitente", http.StatusBadRequest)
			return
		}
		idInt, err := strconv.Atoi(comitente)
		if err != nil {
			http.Error(w, "el ID de comitente debe ser numérico", http.StatusBadRequest)
			return
		}

		// Tiene el usuario asignado este comitente?
		u, err := s.usuariosReader.ReadOne(r.Context(), user)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
		ok := false
		for _, v := range u.Comitentes {
			if v == idInt {
				ok = true
				break
			}
		}
		if !ok {
			http.Error(w, "el usuario no tiene asignado el comitente seleccionado", http.StatusBadRequest)
			return
		}

		// Estamos ok
		token, err := extraerToken(r)

		token, err = s.comitenteSetter.SetComitente(token, idInt)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		setToken(w, token)

		out := LoginResponse{}
		out.Apellido = u.Apellido
		out.Nombre = u.Nombre
		out.ID = u.ID

		// Devuelvo datos del usuario para que los use el frontend
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// Login devuelve una HandlerFunc que corrobora usuario y contraseña y si pasa
// le pega una cookie.
func (s *Server) Logout(w http.ResponseWriter, r *http.Request) {

	// Extraigo token
	cookie, err := r.Cookie("token")
	if err != nil {
		return
	}

	// Invalido
	token, err := s.invalidater.InvalidateToken(cookie.Value)
	if err != nil {
		return
	}

	// Lo pego en la response
	setToken(w, token)

}

// ParsearToken intenta leer los campos del token y los guarda en context.
// Si da un error, o no los encuentra no pasa nada, es una situación común
// recibir un request sin token.
func (h *Server) Touch(w http.ResponseWriter, r *http.Request) (err error) {

	// Extraigo el string del request
	token, err := extraerToken(r)
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Parseo el string que vino en el request
	token, f, err := h.toucher.TouchTime(token)
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Grabo los datos del token en el context
	ctx := contexto.SetUser(r.Context(), f.UserID)
	*r = *r.WithContext(ctx)

	if f.Comitente != 0 {
		ctx = contexto.SetComitente(r.Context(), int(f.Comitente))
		*r = *r.WithContext(ctx)
	}
	// Pego a response
	setToken(w, token)

	// if f.Comitente == 0 {
	// 	http.Error(w, sesiones.ErrMuchosComitentes{}.Frontend(), http.StatusUnauthorized)
	// 	return
	// }

	return
}

// Extrae el token que está en la request (el string)
func extraerToken(r *http.Request) (token string, err error) {

	c, err := r.Cookie("token")
	if err != nil {
		return token, errors.Wrap(err, "buscando cookie")
	}
	return c.Value, nil
}

// setToken agrega la cookie a la response.
func setToken(w http.ResponseWriter, token string) {

	cookie := http.Cookie{}
	cookie.Name = "token"
	cookie.Path = "/"
	cookie.Value = token
	cookie.SameSite = http.SameSiteStrictMode
	http.SetCookie(w, &cookie)
	return
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
