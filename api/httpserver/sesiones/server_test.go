package sesiones

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"bitbucket.org/marcos19/one/pkg/sesiones"
	"bitbucket.org/marcos19/one/pkg/usuarios"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestLogin(t *testing.T) {
	t.Run("should work", func(t *testing.T) {
		s := Server{
			sessioner:       sessionMock{},
			creater:         createrMock{},
			usuariosReader:  usuarioMock{comitentes: 1},
			comitenteSetter: comitenteMock{},
		}

		// Llega desde el frontend
		req := LoginReq{
			UserID: "testID",
			Pass:   "testPass",
		}

		w := httptest.NewRecorder()
		by, err := json.Marshal(req)
		require.Nil(t, err)
		buf := bytes.NewReader(by)
		r, err := http.NewRequest(http.MethodPost, "", buf)
		require.Nil(t, err)

		s.Login(w, r)
		body, err := io.ReadAll(w.Result().Body)
		require.Nil(t, err)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "Body: %v", string(body))

		resp := LoginResponse{}
		err = json.Unmarshal(body, &resp)
		require.Nil(t, err)
		assert.Equal(t, "userID", resp.ID)
		assert.Equal(t, "userNombre", resp.Nombre)
		assert.Equal(t, "userApellido", resp.Apellido)
	})

	t.Run("missing body", func(t *testing.T) {
		s := Server{
			sessioner:       sessionMock{},
			creater:         createrMock{},
			usuariosReader:  usuarioMock{comitentes: 1},
			comitenteSetter: comitenteMock{},
		}

		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodPost, "", strings.NewReader(""))
		require.Nil(t, err)

		s.Login(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode)
	})

	ee := []usuarios.FrontendErr{
		usuarios.ErrContraseñaIncorrecta{},
		usuarios.ErrUsuarioNoConfirmado{},
		usuarios.ErrCorrespondeCambiar{},
	}
	for _, v := range ee {
		t.Run(v.Error(), func(t *testing.T) {
			s := Server{
				sessioner:       sessionMock{err: v},
				creater:         createrMock{},
				usuariosReader:  usuarioMock{comitentes: 1},
				comitenteSetter: comitenteMock{},
			}

			// Llega desde el frontend
			req := LoginReq{
				UserID: "testID",
				Pass:   "testPass",
			}

			w := httptest.NewRecorder()
			by, err := json.Marshal(req)
			require.Nil(t, err)
			buf := bytes.NewReader(by)
			r, err := http.NewRequest(http.MethodPost, "", buf)
			require.Nil(t, err)

			s.Login(w, r)
			assert.Equal(t, http.StatusUnauthorized, w.Result().StatusCode)
			body, err := io.ReadAll(w.Result().Body)
			require.Nil(t, err)
			assert.Equal(t, v.Frontend()+"\n", string(body))
		})
	}

	t.Run("fails token generation", func(t *testing.T) {
		s := Server{
			sessioner:       sessionMock{},
			creater:         createrMock{err: true},
			usuariosReader:  usuarioMock{comitentes: 1},
			comitenteSetter: comitenteMock{},
		}

		// Llega desde el frontend
		req := LoginReq{
			UserID: "testID",
			Pass:   "testPass",
		}

		w := httptest.NewRecorder()
		by, err := json.Marshal(req)
		require.Nil(t, err)
		buf := bytes.NewReader(by)
		r, err := http.NewRequest(http.MethodPost, "", buf)
		require.Nil(t, err)

		s.Login(w, r)
		body, err := io.ReadAll(w.Result().Body)
		require.Nil(t, err)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "Body: %v", string(body))
	})

	t.Run("sin comitente asignado", func(t *testing.T) {
		s := Server{
			sessioner:       sessionMock{},
			creater:         createrMock{},
			usuariosReader:  usuarioMock{comitentes: 0},
			comitenteSetter: comitenteMock{},
		}

		// Llega desde el frontend
		req := LoginReq{
			UserID: "testID",
			Pass:   "testPass",
		}

		w := httptest.NewRecorder()
		by, err := json.Marshal(req)
		require.Nil(t, err)
		buf := bytes.NewReader(by)
		r, err := http.NewRequest(http.MethodPost, "", buf)
		require.Nil(t, err)

		s.Login(w, r)
		body, err := io.ReadAll(w.Result().Body)
		require.Nil(t, err)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "Body: %v", string(body))
	})

	t.Run("muchos comitentes", func(t *testing.T) {
		s := Server{
			sessioner:       sessionMock{},
			creater:         createrMock{},
			usuariosReader:  usuarioMock{comitentes: 2},
			comitenteSetter: comitenteMock{},
		}

		// Llega desde el frontend
		req := LoginReq{
			UserID: "testID",
			Pass:   "testPass",
		}

		w := httptest.NewRecorder()
		by, err := json.Marshal(req)
		require.Nil(t, err)
		buf := bytes.NewReader(by)
		r, err := http.NewRequest(http.MethodPost, "", buf)
		require.Nil(t, err)

		s.Login(w, r)
		body, err := io.ReadAll(w.Result().Body)
		require.Nil(t, err)
		assert.Equal(t, http.StatusUnauthorized, w.Result().StatusCode, "Body: %v", string(body))
		assert.Equal(t, sesiones.ErrMuchosComitentes{}.Frontend()+"\n", string(body), "Body: %v", string(body))
	})

	t.Run("error fijando comitente", func(t *testing.T) {
		s := Server{
			sessioner:       sessionMock{},
			creater:         createrMock{},
			usuariosReader:  usuarioMock{comitentes: 1},
			comitenteSetter: comitenteMock{err: true},
		}

		// Llega desde el frontend
		req := LoginReq{
			UserID: "testID",
			Pass:   "testPass",
		}

		w := httptest.NewRecorder()
		by, err := json.Marshal(req)
		require.Nil(t, err)
		buf := bytes.NewReader(by)
		r, err := http.NewRequest(http.MethodPost, "", buf)
		require.Nil(t, err)

		s.Login(w, r)
		body, err := io.ReadAll(w.Result().Body)
		require.Nil(t, err)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "Body: %v", string(body))
	})
}

type sessionMock struct {
	err error
}

func (s sessionMock) Login(context.Context, usuarios.LoginReq) error {
	return s.err
}

type createrMock struct {
	err bool
}

func (s createrMock) NewToken(id string) (token string, err error) {
	if s.err {
		return "", errors.Errorf("createrMock error")
	}
	return
}

type usuarioMock struct {
	err        bool
	comitentes int
}

func (u usuarioMock) ReadOne(context.Context, string) (out usuarios.Usuario, err error) {
	if u.err {
		return out, errors.Errorf("buscando usuario")
	}
	out.ID = "userID"
	out.Nombre = "userNombre"
	out.Apellido = "userApellido"
	for i := 0; i < u.comitentes; i++ {
		out.Comitentes = append(out.Comitentes, i)
	}

	return
}

type comitenteMock struct {
	err bool
}

func (c comitenteMock) SetComitente(string, int) (string, error) {
	if c.err {
		return "", errors.Errorf("comitenteMock err")
	}
	return "tokenstringencrypted", nil
}
