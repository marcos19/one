package autoaplicador

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/autoaplicador"
	"bitbucket.org/marcos19/one/pkg/deferror"
)

type Server struct {
	h *autoaplicador.Handler
}

func NewServer(h *autoaplicador.Handler) *Server {
	return &Server{h}
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Determino el id de usuario
	_, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Determino el comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch head {
	case "buscar":
		s.Buscar(comitente)(w, r)

	case "confirmar":
		s.Confirmar(comitente)(w, r)

	case "delete":
		s.Delete(comitente)(w, r)

	default:
		deferror.Frontend(deferror.NotFound("head"), w, r)
		return
	}
}

func (s *Server) Buscar(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := autoaplicador.BuscarReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.h.Buscar(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) Confirmar(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := autoaplicador.ConfirmarReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		err = s.h.Confirmar(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) Delete(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := autoaplicador.DeleteReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		err = s.h.Delete(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
