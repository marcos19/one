package listasprecios

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/listasprecios"
)

type Server struct {
	h *listasprecios.Handler
}

func NewServer(h *listasprecios.Handler) *Server {
	return &Server{h}
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	_, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Leo comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch head {

	case "create":
		s.Create(comitente)(w, r)

	case "one":
		s.ReadOne(comitente)(w, r)

	case "many":
		s.ReadMany(comitente)(w, r)

	case "update":
		s.Update(comitente)(w, r)

	case "para_store":
		s.ParaStore(comitente)(w, r)

	default:
		httperr.NotFound(w, r)
	}
}

func (s *Server) Create(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := listasprecios.Lista{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Creo
		req.Comitente = comitente
		err = s.h.Create(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}
	}
}

func (s *Server) Update(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := listasprecios.Lista{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Modifico
		req.Comitente = comitente
		err = s.h.Update(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
		}
	}
}

func (s *Server) ReadOne(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := listasprecios.ReadOneReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.h.ReadOne(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}
func (s *Server) ReadMany(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Busco
		req := listasprecios.ReadManyReq{Comitente: comitente}
		out, err := s.h.ReadMany(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		req.Comitente = comitente
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) ParaStore(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Busco
		req := listasprecios.ReadManyReq{Comitente: comitente}
		out, err := s.h.ReadMany(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
