package bsuso

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/bsuso"
	"bitbucket.org/marcos19/one/pkg/deferror"
)

type Server struct {
	h *bsuso.Handler
}

func NewServer(h *bsuso.Handler) *Server {
	return &Server{h: h}
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Determino el id de usuario
	user, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Determino el comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch head {

	case "rubros":
		s.Rubros(comitente)(w, r)

	case "saldos":
		s.Saldos(user, comitente)(w, r)

	case "previsualizar":
		s.PrevisualizarAmortizaciones(user, comitente)(w, r)

	case "confirmar_amortizaciones":
		s.ConfirmarAmortizaciones(user, comitente)(w, r)

	case "previsualizar_ajuste_inflacion":
		s.PrevisualizarAjustePorInflacion(user, comitente)(w, r)

	case "confirmar_ajuste_inflacion":
		s.ConfirmarAjustePorInflacion(user, comitente)(w, r)

	default:
		deferror.Frontend(deferror.NotFound("head"), w, r)
		return
	}
}

func (s *Server) Rubros(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Genero
		out, err := s.h.Rubros(r.Context(), comitente)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// HandlePrevisualizar genera las ops y las devuelve para que las vea el usuario.
func (s *Server) Saldos(user string, comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := bsuso.TraerSaldosReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Genero
		req.Comitente = comitente
		out, err := s.h.TraerSaldos(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// HandlePrevisualizar genera las ops y las devuelve para que las vea el usuario.
func (s *Server) PrevisualizarAmortizaciones(user string, comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := bsuso.AmortizarReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Genero
		req.Comitente = comitente
		req.Usuario = user
		out, err := s.h.PrevisualizarAmortizaciones(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// HandlePrevisualizar genera las ops y las devuelve para que las vea el usuario.
func (s *Server) ConfirmarAmortizaciones(user string, comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := bsuso.AmortizarReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Genero
		req.Comitente = comitente
		req.Usuario = user
		id, err := s.h.ConfirmarAmortizaciones(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&id)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// HandlePrevisualizar genera las ops y las devuelve para que las vea el usuario.
func (s *Server) PrevisualizarAjustePorInflacion(user string, comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := bsuso.AjustarInflacionReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Genero
		req.Comitente = comitente
		req.Usuario = user
		out, err := s.h.PrevisualizarAjustePorInflacion(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) ConfirmarAjustePorInflacion(user string, comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := bsuso.AjustarInflacionReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Genero
		req.Comitente = comitente
		req.Usuario = user
		out, err := s.h.ConfirmarAjustePorInflacion(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
