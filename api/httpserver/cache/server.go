package cache

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"github.com/dgraph-io/ristretto"
)

type Server struct {
	cache *ristretto.Cache
}

func NewServer(cache *ristretto.Cache) (s *Server) {
	s = &Server{}
	s.cache = cache
	return s
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	_, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	switch head {

	case "metrics":
		s.Metrics()(w, r)

	default:
		httperr.NotFound(w, r)
	}
}

type Metric struct {
	Hits         uint64 `json:",string"`
	Misses       uint64 `json:",string"`
	KeysAdded    uint64 `json:",string"`
	KeysUpdated  uint64 `json:",string"`
	KeysEvicted  uint64 `json:",string"`
	CostAdded    uint64 `json:",string"`
	CostEvicted  uint64 `json:",string"`
	SetsDropped  uint64 `json:",string"`
	SetsRejected uint64 `json:",string"`
	GetsDropped  uint64 `json:",string"`
	GetsKept     uint64 `json:",string"`
}

func (s *Server) Metrics() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		m := s.cache.Metrics
		out := Metric{
			Hits:         m.Hits(),
			Misses:       m.Misses(),
			KeysAdded:    m.KeysAdded(),
			KeysUpdated:  m.KeysUpdated(),
			KeysEvicted:  m.KeysEvicted(),
			CostAdded:    m.CostAdded(),
			CostEvicted:  m.CostEvicted(),
			SetsDropped:  m.SetsDropped(),
			SetsRejected: m.SetsRejected(),
			GetsDropped:  m.GetsDropped(),
			GetsKept:     m.GetsKept(),
		}

		// Devuelvo JSON
		err := json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
