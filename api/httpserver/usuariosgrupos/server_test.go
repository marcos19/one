package usuariosgrupos

import (
	"context"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/pkg/usuariosgrupos"
	"github.com/cockroachdb/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestContextData(t *testing.T) {

	t.Run("missing user", func(t *testing.T) {
		s := Server{}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusUnauthorized, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("missing comitente", func(t *testing.T) {
		s := Server{}
		w := httptest.NewRecorder()

		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		// Pego usuario
		nuevoCtx := contexto.SetUser(r.Context(), "user")
		r = r.WithContext(nuevoCtx)

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusUnauthorized, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("should work", func(t *testing.T) {
		s := Server{
			readerMany: readManyMock{},
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		// Pego usuario
		nuevoCtx := contexto.SetUser(r.Context(), "user")
		// Pego comitente
		nuevoCtx = contexto.SetComitente(nuevoCtx, 1)
		r = r.WithContext(nuevoCtx)
		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
}

type readManyMock struct {
	err        bool
	withValues bool
	nil        bool
}

func (r readManyMock) ReadMany(context.Context, usuariosgrupos.ReadManyReq) (out []usuariosgrupos.Grupo, err error) {

	if r.err {
		return nil, errors.Errorf("testing err")
	}
	if r.nil {
		return nil, nil
	}
	if !r.withValues {
		out = []usuariosgrupos.Grupo{}
		return
	}
	out = append(out, usuariosgrupos.Grupo{ID: 1, Nombre: "Cuenta name"})
	return

}

func TestReadOne(t *testing.T) {

	t.Run("id not numeric", func(t *testing.T) {
		s := Server{
			readerOne: &readOneMock{},
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "asd15", nil)
		r = r.WithContext(prepareCtx())
		require.Nil(t, err)
		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
	t.Run("business layer error", func(t *testing.T) {
		s := Server{
			readerOne: &readOneMock{err: true},
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "54", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
	t.Run("should work", func(t *testing.T) {
		s := Server{
			readerOne: &readOneMock{},
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "34", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())
		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
}

type readOneMock struct {
	err bool
}

func (r readOneMock) ReadOne(context.Context, usuariosgrupos.ReadOneReq) (usuariosgrupos.Grupo, error) {
	if r.err {
		return usuariosgrupos.Grupo{}, errors.Errorf("test err")
	}
	return usuariosgrupos.Grupo{}, nil
}

func TestReadMany(t *testing.T) {

	t.Run("business layer error", func(t *testing.T) {
		s := Server{
			readerMany: &readManyMock{err: true},
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
	t.Run("nil return", func(t *testing.T) {
		s := Server{
			readerMany: &readManyMock{nil: true},
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		body := readBody(w.Result().Body)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", body)
		expected := "[]\n"
		assert.Equal(t, expected, body, "expected: %v, returned: %v", expected, body)
	})

	t.Run("empty slice", func(t *testing.T) {
		s := Server{
			readerMany: &readManyMock{},
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		body := readBody(w.Result().Body)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", body)
		expected := "[]\n"
		assert.Equal(t, expected, body, "expected: %v, returned: %v", expected, body)
	})

	t.Run("with values", func(t *testing.T) {
		s := Server{
			readerMany: &readManyMock{withValues: true},
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		body := readBody(w.Result().Body)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", body)
		assert.True(t, len(body) > 10, "short body: %v", body)
	})
}

func TestCreate(t *testing.T) {

	t.Run("empty body", func(t *testing.T) {
		s := Server{
			creater: &createMock{},
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader("")
		r, err := http.NewRequest(http.MethodPost, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("malformed json", func(t *testing.T) {
		s := Server{
			creater: &createMock{},
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{: ""}`,
		)
		r, err := http.NewRequest(http.MethodPost, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("business layer error", func(t *testing.T) {
		s := Server{
			creater: &createMock{err: true},
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{"Nombre": "Testing name"}`,
		)
		r, err := http.NewRequest(http.MethodPost, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("should work", func(t *testing.T) {
		s := Server{
			creater: &createMock{err: false},
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{"Nombre": "Testing name"}`,
		)
		r, err := http.NewRequest(http.MethodPost, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		body := readBody(w.Result().Body)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", body)
		assert.Equal(t, `{"ID":"343"}`+"\n", body)
	})
}

type createMock struct {
	err bool
}

func (r *createMock) Create(context.Context, usuariosgrupos.Grupo) (int, error) {
	if r.err {
		return 0, errors.Errorf("business layer error")
	}
	return 343, nil
}

func TestDelete(t *testing.T) {

	t.Run("no id", func(t *testing.T) {
		s := Server{
			deleter: &deleteMock{},
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodDelete, "", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("invalid id", func(t *testing.T) {
		s := Server{
			deleter: &deleteMock{},
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodDelete, "invalidNumber", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("business layer error", func(t *testing.T) {
		s := Server{
			deleter: &deleteMock{err: true},
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodDelete, "453", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("should work", func(t *testing.T) {
		s := Server{
			deleter: &deleteMock{err: false},
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodDelete, "453", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
}

type deleteMock struct {
	err bool
}

func (r *deleteMock) Delete(context.Context, usuariosgrupos.ReadOneReq) error {
	if r.err {
		return errors.Errorf("business layer error")
	}
	return nil
}

func TestUpdate(t *testing.T) {

	t.Run("empty body", func(t *testing.T) {
		s := Server{
			updater: &updateMock{},
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader("")
		r, err := http.NewRequest(http.MethodPut, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("malformed json", func(t *testing.T) {
		s := Server{
			updater: &updateMock{},
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{: ""}`,
		)
		r, err := http.NewRequest(http.MethodPut, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("business layer error", func(t *testing.T) {
		s := Server{
			updater: &updateMock{err: true},
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{"Nombre": "Testing name"}`,
		)
		r, err := http.NewRequest(http.MethodPut, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("should work", func(t *testing.T) {
		s := Server{
			updater: &updateMock{err: false},
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{"Nombre": "Testing name"}`,
		)
		r, err := http.NewRequest(http.MethodPut, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		body := readBody(w.Result().Body)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", body)
		assert.Equal(t, "", body)
	})
}

type updateMock struct {
	err bool
}

func (r *updateMock) Update(context.Context, usuariosgrupos.Grupo) error {
	if r.err {
		return errors.Errorf("business layer error")
	}
	return nil
}

func readBody(r io.ReadCloser) string {
	by, err := io.ReadAll(r)
	if err != nil {
		return "error leyendo body"
	}
	return string(by)
}

// Sets user and comitente
func prepareCtx() context.Context {
	// Pego usuario
	ctx := contexto.SetUser(context.Background(), "user")
	// Pego comitente
	ctx = contexto.SetComitente(ctx, 1)
	return ctx
}
