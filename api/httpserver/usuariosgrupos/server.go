package usuariosgrupos

import (
	"encoding/json"
	"net/http"
	"path"
	"strconv"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/usuariosgrupos"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/niler"
)

type Server struct {
	readerOne  usuariosgrupos.ReaderOne
	readerMany usuariosgrupos.ReaderMany
	creater    usuariosgrupos.Creater
	updater    usuariosgrupos.Updater
	deleter    usuariosgrupos.Deleter
}

func NewServer(h Handler) (s *Server, err error) {
	if niler.IsNil(h) {
		return s, errors.Errorf("Handler no puede ser nil")
	}
	return &Server{
		readerOne:  h,
		readerMany: h,
		creater:    h,
		updater:    h,
		deleter:    h,
	}, nil
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	_, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Leo comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch r.Method {

	case http.MethodGet:
		switch head {
		case "":
			s.readMany(comitente)(w, r)
		default:
			s.readOne(comitente, head)(w, r)
		}

	case http.MethodPost:
		s.create(comitente)(w, r)

	case http.MethodPut:
		s.update(comitente)(w, r)

	case http.MethodDelete:
		s.delete(comitente, head)(w, r)

	default:
		httperr.NotImplemented(w, r)
	}
}

func (s *Server) create(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Parseo
		req := usuariosgrupos.Grupo{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Inserto
		req.Comitente = comitente
		id, err := s.creater.Create(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		out := struct {
			ID int `json:",string"`
		}{id}
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
		}
	}
}

func (s *Server) readOne(comitente int, id string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo ID
		if id == "" {
			httperr.UnprocessableEntity(w, errors.Errorf("No se ingresó ID"))
			return
		}
		idInt, err := strconv.Atoi(id)
		if err != nil {
			httperr.BadRequest(w, errors.Errorf("ID debe ser numérico"))
			return
		}

		req := usuariosgrupos.ReadOneReq{
			Comitente: comitente,
			ID:        idInt,
		}

		// Busco
		req.Comitente = comitente
		out, err := s.readerOne.ReadOne(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) readMany(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		req := usuariosgrupos.ReadManyReq{}

		// Busco
		req.Comitente = comitente
		out, err := s.readerMany.ReadMany(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, errors.Wrap(err, "buscando grupos de centros contables"))
			return
		}

		if out == nil {
			out = []usuariosgrupos.Grupo{}
		}
		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) update(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := usuariosgrupos.Grupo{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			http.Error(w, "decoding JSON", http.StatusBadRequest)
			return
		}

		// Modifico
		err = s.updater.Update(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, errors.Wrap(err, "modificando cuenta contable"))
			return
		}

	}
}

func (s *Server) delete(comitente int, id string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request

		// Leo ID
		if id == "" {
			httperr.BadRequest(w, errors.Errorf("no se ingresó ID"))
			return
		}
		idInt, err := strconv.Atoi(id)
		if err != nil {
			httperr.BadRequest(w, errors.Errorf("ID debe ser numérico"))
			return
		}

		req := usuariosgrupos.ReadOneReq{
			Comitente: comitente,
			ID:        idInt,
		}
		// Borro
		err = s.deleter.Delete(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, errors.Wrap(err, "modificando cuenta contable"))
			return
		}

	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
