package usuariosgrupos

import "bitbucket.org/marcos19/one/pkg/usuariosgrupos"

type Handler interface {
	usuariosgrupos.ReaderOne
	usuariosgrupos.ReaderMany
	usuariosgrupos.Creater
	usuariosgrupos.Updater
	usuariosgrupos.Deleter
}
