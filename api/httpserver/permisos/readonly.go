package permisos

import (
	"net/http"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"github.com/pkg/errors"
)

// Todos pueden leer, pero solo pueden modificar aquellos
// que son administradores/
type ReadOnly struct {
	adminer Adminer
}

func NewReadOnly(a Adminer) *ReadOnly {
	return &ReadOnly{a}
}

func (a *ReadOnly) Can(then http.HandlerFunc) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo usuario
		u, err := contexto.GetUser(r.Context())
		if err != nil {
			httperr.Unauthorized(w)
			return
		}

		if r.Method == http.MethodGet {
			then(w, r)
			return
		}

		isAdmin, err := a.adminer.IsAdmin(r.Context(), u)
		if err != nil {
			httperr.InternalServerError(w, errors.Wrap(err, "determinando permisos de usuario"))
			return
		}

		if !isAdmin {
			httperr.Forbidden(w, "no tiene permisos para realizar la acción solicitada")
			return
		}

		then(w, r)
	}
}
