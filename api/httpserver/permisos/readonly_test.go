package permisos

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
)

func TestReadOnlyCan(t *testing.T) {

	t.Run("GET unauthenticated", func(t *testing.T) {
		r, _ := http.NewRequest(http.MethodGet, "", nil)
		ro := ReadOnly{adminer: adminerMock{isAdmin: false}}

		out := ro.Can(okey)

		w := httptest.NewRecorder()
		out(w, r)
		assert.Equal(t, http.StatusUnauthorized, w.Code)
	})

	t.Run("GET default user", func(t *testing.T) {
		r, _ := http.NewRequest(http.MethodGet, "", nil)
		ro := ReadOnly{adminer: adminerMock{isAdmin: false}}
		ctx := contexto.SetUser(r.Context(), "testing user")
		r = r.WithContext(ctx)
		out := ro.Can(okey)

		w := httptest.NewRecorder()
		out(w, r)
		assert.Equal(t, http.StatusNoContent, w.Code)
	})
	t.Run("GET admin user", func(t *testing.T) {
		r, _ := http.NewRequest(http.MethodGet, "", nil)
		ro := ReadOnly{adminer: adminerMock{isAdmin: true}}
		ctx := contexto.SetUser(r.Context(), "testing user")
		r = r.WithContext(ctx)

		out := ro.Can(okey)

		w := httptest.NewRecorder()
		out(w, r)
		assert.Equal(t, http.StatusNoContent, w.Code)
	})

	mm := []string{
		http.MethodPost,
		http.MethodPut,
		http.MethodPatch,
		http.MethodDelete,
	}
	for _, v := range mm {
		t.Run("POST fails to check admin rights", func(t *testing.T) {
			r, _ := http.NewRequest(http.MethodPost, "", nil)
			ro := ReadOnly{adminer: adminerMock{err: true}}
			ctx := contexto.SetUser(r.Context(), "testing user")
			r = r.WithContext(ctx)

			out := ro.Can(okey)

			w := httptest.NewRecorder()
			out(w, r)
			assert.Equal(t, http.StatusInternalServerError, w.Code)
		})
		t.Run(fmt.Sprintf("%v user is admin", v), func(t *testing.T) {
			r, _ := http.NewRequest(http.MethodPost, "", nil)
			ro := ReadOnly{adminer: adminerMock{isAdmin: true}}
			ctx := contexto.SetUser(r.Context(), "testing user")
			r = r.WithContext(ctx)

			out := ro.Can(okey)

			w := httptest.NewRecorder()
			out(w, r)
			assert.Equal(t, http.StatusNoContent, w.Code)
		})
		t.Run(fmt.Sprintf("%v user is not admin", v), func(t *testing.T) {
			r, _ := http.NewRequest(http.MethodPost, "", nil)
			ro := ReadOnly{adminer: adminerMock{isAdmin: false}}
			ctx := contexto.SetUser(r.Context(), "testing user")
			r = r.WithContext(ctx)

			out := ro.Can(okey)

			w := httptest.NewRecorder()
			out(w, r)
			assert.Equal(t, http.StatusForbidden, w.Code)
		})
	}
}

type adminerMock struct {
	isAdmin bool
	err     bool
}

func (a adminerMock) IsAdmin(context.Context, string) (bool, error) {
	if a.err {
		return false, errors.Errorf("err test")
	}
	return a.isAdmin, nil
}

func forbidden(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "", http.StatusForbidden)
}

func okey(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNoContent)
	w.Write([]byte(okeyMsg))
}

const okeyMsg = "HanlderFunc 'then' ejecutada"
