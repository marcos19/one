package permisos

import "context"

type Adminer interface {
	IsAdmin(context.Context, string) (bool, error)
}
