package afip

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/afip"
	"bitbucket.org/marcos19/one/pkg/afip/cert"
	"bitbucket.org/marcos19/one/pkg/afip/miscomprobantes"
	"bitbucket.org/marcos19/one/pkg/afip/wsaa"
	"bitbucket.org/marcos19/one/pkg/afip/wsfev1"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/iva"
	"github.com/cockroachdb/errors"
	"github.com/rs/zerolog/log"
)

type Server struct {
	h *afip.Handler

	// Factura electrónica handler
	feHandler *wsfev1.Handler

	// WS de autenticación handler
	aaHandler *wsaa.Store

	// Certificados handler
	certHandler *cert.Handler
}

func NewServer(h *afip.Handler, fe *wsfev1.Handler, aa *wsaa.Store, cc *cert.Handler) (s *Server, err error) {
	if h == nil {
		return s, errors.New("AFIPHandler no puede ser nil")
	}
	if fe == nil {
		return s, errors.New("wsfev1.Handler no puede ser nil")
	}
	if aa == nil {
		return s, errors.New("wsaa.Handler no puede ser nil")
	}
	if cc == nil {
		return s, errors.New("cert.Handler no puede ser nil")
	}
	s = &Server{h, fe, aa, cc}
	return
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	_, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Leo comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch head {

	case "alicuotas_venta":

	case "alicuotas":
		s.Alicuotas()(w, r)

	case "conceptos_iva":
		s.Conceptos()(w, r)

	case "identificaciones":
		s.Identificaciones()(w, r)

	case "condiciones_fiscales_facturacion":
		s.CondicionesFiscalesFacturacion()(w, r)

	case "comps":
		s.Comps()(w, r)

	case "comprobantes_a_autorizar":
		s.ComprobantesAAutorizar(comitente)(w, r)

	case "comprobantes_a_autorizar_general":
		s.ComprobantesAAutorizarGeneral(comitente)(w, r)

	case "libro_iva":
		s.LibroIVA(comitente)(w, r)

	case "libro_iva_digital":
		s.LibroIVADigital(comitente)(w, r)

	case "libro_iva_digital_excel":
		s.LibroIVADigitalExcel(comitente)(w, r)

	case "mis_comprobantes":
		s.MisComprobantes(comitente)(w, r)

	case "comparar_iva_compras":
		s.CompararIVACompras(comitente)(w, r)

	case "solicitar_cae":
		s.SolicitarCAE(comitente)(w, r)

	case "consultar_cae":
		s.ConsultarCAE(comitente)(w, r)

	case "regrabar_cae":
		s.RegrabarCAE(comitente)(w, r)

	case "comp_ultimo_autorizado":
		s.CompUltimoAutorizado(comitente)(w, r)

	case "constancia_inscripcion":
		s.ConstanciaInscripcion(comitente)(w, r)

	case "tickets":
		s.Tickets(comitente)(w, r)

	case "solicitar_ticket_acceso":
		s.SolicitarTicketAcceso(comitente)(w, r)

	case "certificados":
		s.Certificados(comitente)(w, r)

	case "certificado":
		s.Certificado(comitente)(w, r)

	case "subir_certificado":
		s.SubirCertificado(comitente)(w, r)

	case "delete_certificado":
		s.DeleteCertificado(comitente)(w, r)

	case "solicitar_csr":
		s.SolicitarCSR(comitente)(w, r)

	default:
		// Error
		deferror.Frontend(deferror.NotFound(head), w, r)
	}
}

func (s *Server) SolicitarCAE(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := wsfev1.SolicitarCAEReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Solicito CAE
		req.Comitente = comitente
		out, err := s.feHandler.SolicitarCAE(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) ConsultarCAE(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := wsfev1.SolicitarCAEReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Solicito CAE
		req.Comitente = comitente
		out, err := s.feHandler.ConsultarCAE(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "consultando CAE"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) RegrabarCAE(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := wsfev1.SolicitarCAEReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Solicito CAE
		req.Comitente = comitente
		err = s.feHandler.RegrabarCAE(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "regrabando CAE"), w, r)
			return
		}

	}
}

func (s *Server) CompUltimoAutorizado(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := wsfev1.CompUltimoAutorizadoReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Solicito CAE
		req.Comitente = comitente
		out, err := s.feHandler.CompUltimoAutorizado(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "consultando CAE"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// Devuelve un listado de todos los tickets del comitente
func (s *Server) Tickets(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Busco
		out, err := s.aaHandler.Tickets(comitente)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando tickets"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// Desde el frontend exigo solicitar un ticket, devuelvo todos los tickets.
func (s *Server) SolicitarTicketAcceso(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := struct {
			Empresa  int `json:",string"`
			Servicio string
		}{}

		type Ticket struct {
			EmpresaID     int `json:",string"`
			EmpresaNombre string
			CUIT          int `json:",string"`
			Servicio      string
			Expiracion    time.Time
		}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		_, err = s.aaHandler.GetTicket(r.Context(), comitente, req.Empresa, req.Servicio)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "solicitando ticket"), w, r)
			return
		}

		// Pido tickets
		creds, err := s.aaHandler.Tickets(comitente)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		out := []Ticket{}
		for _, v := range creds {
			t := Ticket{}
			t.CUIT = v.CUIT
			t.Expiracion = v.ExpirationTime
			t.Servicio = v.Servicio
			out = append(out, t)
		}
		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// Devuelve un listado de todos los certificados del comitente
func (s *Server) Certificados(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Busco
		out, err := s.certHandler.Certificados(r.Context(), comitente)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) Certificado(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := cert.CertificadoReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.certHandler.Certificado(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) ConstanciaInscripcion(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		req := afip.ConstanciaInscripcionReq{}

		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco la persona
		req.Comitente = comitente
		persona, err := s.h.ConstanciaInscripcion(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&persona)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) SubirCertificado(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := cert.CertAFIP{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Guardo
		req.Comitente = comitente
		err = s.certHandler.SubirCertificado(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) DeleteCertificado(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := cert.DeleteCertificadoReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Guardo
		req.Comitente = comitente
		err = s.certHandler.DeleteCertificado(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// Devuelve un listado de todos los certificados del comitente
func (s *Server) SolicitarCSR(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := cert.GenerarCSRReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)

		}

		// Genero
		req.Comitente = comitente
		out, err := s.certHandler.GenerarCSR(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
		log.Info().Msg(out)

		// Devuelvo bytes
		w.Header().Set("Content-Type", "application/octet-stream")
		w.Header().Set("Content-Disposition", "attachment; filename=request.csr")
		// w.Header().Set("Content-Transfer-Encoding", "binary")
		w.Write([]byte(out))
	}
}

func (s *Server) LibroIVA(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		type reqL struct {
			afip.LibroIVAReq
			Salida string
		}
		// Leo request
		req := reqL{}

		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		req.Comitente = comitente

		// Genero
		switch req.Salida {
		case "pdf":
			out, err := s.h.LibroIVAPDF(r.Context(), req.LibroIVAReq)
			if err != nil {
				deferror.Frontend(errors.Wrap(err, "generando libro IVA"), w, r)
				return
			}
			err = out.Output(w)
			if err != nil {
				deferror.Frontend(errors.Wrap(err, "exportando"), w, r)
				return
			}
			return

		case "xlsx":
			out, err := s.h.LibroIVAExcel(r.Context(), req.LibroIVAReq)
			if err != nil {
				deferror.Frontend(errors.Wrap(err, "generando libro IVA"), w, r)
				return
			}
			err = out.Write(w)
			if err != nil {
				deferror.Frontend(errors.Wrap(err, "exportando"), w, r)
				return
			}

		default:
			deferror.Frontend(deferror.Validationf("formato de salida '%v' no implementada", req.Salida), w, r)
			return
		}

	}
}

func (s *Server) LibroIVADigital(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := afip.LibroIVADigitalReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Genero
		req.Comitente = comitente
		out, err := s.h.LibroIVADigital(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "generando libro IVA"), w, r)
			return
		}
		// Devuelvo csv
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "encoding JSON"), w, r)
			return
		}
	}
}

func (s *Server) LibroIVADigitalExcel(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := afip.LibroIVADigitalReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Genero
		req.Comitente = comitente
		out, err := s.h.LibroIVADigitalExcel(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "generando libro IVA"), w, r)
			return
		}

		w.Header().Set("Content-Type", "application/octet-stream")
		w.Header().Set("Content-Disposition", "attachment; filename=mayor.xlsx")
		w.Header().Set("Content-Transfer-Encoding", "binary")
		err = out.Write(w)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "encoding response"), w, r)
			return
		}
	}
}

func (s *Server) MisComprobantes(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := miscomprobantes.CompararReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Genero
		req.Comitente = comitente
		out, err := s.h.MisComprobantes(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Devuelvo
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "encoding JSON"), w, r)
			return
		}
	}
}

func (s *Server) CompararIVACompras(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := miscomprobantes.CompararReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Genero
		req.Comitente = comitente
		out, err := s.h.CompararIVACompras(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Devuelvo
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "encoding JSON"), w, r)
			return
		}
	}
}

// // HandleLibroIVA devuelve el libro de IVA en el formato especificado
// func (s *Server) HandleLibroIVA(comitente int) http.HandlerFunc {

// 	type req struct {
// 		Empresa int `json:",string"`
// 		Desde   fecha.Fecha
// 		Hasta   fecha.Fecha
// 		Tipo    string
// 		Version string
// 		Salida  string // [json, pdf, excel]
// 	}

// 	return func(w http.ResponseWriter, r *http.Request) {

// 		// Leo request
// 		req := req{}
// 		err := json.NewDecoder(r.Body).Decode(&req)
// 		if err != nil {
// 			deferror.Frontend(deferror.BadRequest(err), w, r)
// 			return
// 		}

// 		// Genero
// 		libro, err := s.h.LibroIVADigital(libroiva.LibroIVAParams{
// 			Comitente: comitente,
// 			Empresa:   req.Empresa,
// 			Desde:     req.Desde,
// 			Hasta:     req.Hasta,
// 			Tipo:      req.Tipo,
// 		})

// 		if err != nil {
// 			deferror.Frontend(errors.Wrap(err, "generando libro IVA Ventas"), w, r)
// 			return
// 		}

// 		// Devuelvo salida
// 		switch req.Salida {
// 		case "JSON":
// 			err = json.NewEncoder(w).Encode(&libro)
// 			if err != nil {
// 				deferror.Frontend(err, w, r)
// 				return
// 			}

// 		case "PDF":
// 			w.Header().Set("Content-Type", "application/pdf")
// 			w.Header().Set("Content-Disposition", "attachment; filename=diario.pdf")

// 			// TODO: separar PDF de generación
// 			// err = libro.PDF(w, h.db)
// 			// if err != nil {
// 			// 	deferror.Frontend(err, w, r)
// 			// 	return
// 			// }

// 		default:
// 			deferror.Frontend(errors.Errorf("salida '%v' no implementada", req.Salida), w, r)
// 			return
// 		}
// 	}
// }

// HandleComps devuelve el listado de comprobantes definidos por AFIP
func (s *Server) Comps() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Busco
		out, err := s.h.Comps(r.Context())
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando comprobantes AFIP"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "codificando JSON"), w, r)
			return
		}
	}
}

func (s *Server) ComprobantesAAutorizar(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := wsfev1.CompsPendientesAutorizarReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.feHandler.CompsPendientesAutorizar(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando comprobantes AFIP"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "codificando JSON"), w, r)
			return
		}
	}
}

func (s *Server) ComprobantesAAutorizarGeneral(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Busco
		out, err := s.feHandler.CompsPendientesAutorizarGeneral(r.Context(), comitente)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando comprobantes AFIP"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "codificando JSON"), w, r)
			return
		}
	}
}

// HandleCondicionesFiscalesFacturacion devuelve el listado de alícuotas habilitados
func (s *Server) CondicionesFiscalesFacturacion() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		out := afip.CondicionesFacturacion()
		err := json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "codificando JSON"), w, r)
			return
		}
	}
}

// HandleAlicuotas devuelve el listado de alícuotas habilitados
func (s *Server) Alicuotas() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		aa := iva.Alicuotas()
		err := json.NewEncoder(w).Encode(&aa)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "codificando JSON"), w, r)
			return
		}
	}
}

// HandleConceptos devuelve el listado de conceptos definidos
// por AFIP (Base imponible, IVA, Percepcion, etc)
func (s *Server) Conceptos() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		cc := iva.Conceptos()
		err := json.NewEncoder(w).Encode(&cc)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "codificando JSON"), w, r)
			return
		}
	}
}

func (s *Server) Identificaciones() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		// Busco
		out, err := s.h.Identificaciones(r.Context())
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando identificaciones AFIP"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "codificando JSON"), w, r)
			return
		}

	}
}

func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
