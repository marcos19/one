package usuarios

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/usuarios"
	"github.com/crosslogic/niler"
	"github.com/pkg/errors"
)

type Server struct {
	readerOne         usuarios.ReaderOne
	readerMany        usuarios.ReaderMany
	creater           usuarios.Creater
	updater           usuarios.Updater
	patcher           usuarios.Patcher
	comitentesGetter  usuarios.ComitentesGetter
	withAuthorization Auther
}

func NewServer(h Handler, a Auther) (s *Server, err error) {
	if niler.IsNil(h) {
		return s, errors.New("Handler no puede ser nil")
	}
	if niler.IsNil(a) {
		return s, errors.New("Auther no puede ser nil")
	}

	s = &Server{
		readerOne:         h,
		readerMany:        h,
		creater:           h,
		updater:           h,
		patcher:           h,
		comitentesGetter:  h,
		withAuthorization: a,
	}
	return
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Cambio de contraseña
	if head == "password" && r.Method == http.MethodPatch {
		s.cambiarPassword()(w, r)
		return
	}

	// Leo usuario
	user, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	if head == "comitentes" {
		s.comitentesDisponibles(user)(w, r)
		return
	}

	// Leo comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	var then http.HandlerFunc

	switch r.Method {

	case http.MethodPost:
		then = s.create(comitente, user)

	case http.MethodGet:

		switch head {
		case "":
			then = s.readMany(comitente)

		default:
			then = s.readOne(comitente, head)
		}

	case http.MethodPut:
		then = s.update(comitente, user)

	default:
		http.Error(w, "método no permitido", http.StatusNotImplemented)
		return
	}

	s.withAuthorization(then)(w, r)
}

// Esta es la estrucutra que va y viene del frontend
type UsuarioDTO struct {
	ID            string
	Email         *string
	Nombre        string
	Apellido      string
	DNI           int
	Estado        string
	Administrador bool
	Inactivo      bool
	PieMail       string

	UltimaActualizacionContraseña time.Time
	CreatedAt                     *time.Time
}

func (s *Server) create(comitente int, user string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := UsuarioDTO{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// ¿Es admin?
		solic, err := s.readerOne.ReadOne(r.Context(), user)
		if err != nil {
			http.Error(w, "buscando usuario", http.StatusInternalServerError)
			return
		}
		if !solic.Administrador {
			http.Error(w, "no está autorizado a crear usuarios", http.StatusForbidden)
			return
		}

		u := usuarios.Usuario{
			ID:            req.ID,
			Email:         req.Email,
			Nombre:        req.Nombre,
			Apellido:      req.Apellido,
			DNI:           req.DNI,
			Estado:        usuarios.EstadoConfirmado,
			Administrador: req.Administrador,
			Inactivo:      false,
			PieMail:       req.PieMail,
			Comitentes:    []int{comitente},
		}

		// Inserto
		err = s.creater.Create(r.Context(), u)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "creando usuario"), w, r)
			return
		}
	}
}

func (s *Server) readMany(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Busco
		uu, err := s.readerMany.ReadMany(r.Context(), usuarios.ReadManyReq{
			Comitente: comitente,
		})
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando usuarios"), w, r)
			return
		}

		out := []UsuarioDTO{}
		for _, v := range uu {
			out = append(out, toDTO(v))
		}

		// Devuelvo los datos del usuario
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) readOne(comitente int, id string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		u, err := s.readerOne.ReadOne(r.Context(), id)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando usuario"), w, r)
			return
		}

		dto := toDTO(u)

		// Devuelvo los datos del usuario
		err = json.NewEncoder(w).Encode(&dto)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func toDTO(u usuarios.Usuario) UsuarioDTO {
	return UsuarioDTO{
		ID:                            u.ID,
		Nombre:                        u.Nombre,
		Email:                         u.Email,
		Apellido:                      u.Apellido,
		DNI:                           u.DNI,
		Estado:                        u.Estado,
		Administrador:                 u.Administrador,
		Inactivo:                      u.Administrador,
		PieMail:                       u.PieMail,
		UltimaActualizacionContraseña: u.UltimaActualizacionContraseña,
		CreatedAt:                     u.CreatedAt,
	}
}

func fromDTO(u UsuarioDTO) usuarios.Usuario {
	return usuarios.Usuario{
		ID:            u.ID,
		Nombre:        u.Nombre,
		Email:         u.Email,
		Apellido:      u.Apellido,
		DNI:           u.DNI,
		Estado:        u.Estado,
		Administrador: u.Administrador,
		Inactivo:      u.Administrador,
		PieMail:       u.PieMail,
	}
}

func (s *Server) update(comitente int, user string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := UsuarioDTO{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// ¿Es admin?
		solic, err := s.readerOne.ReadOne(r.Context(), user)
		if err != nil {
			http.Error(w, "buscando usuario", http.StatusInternalServerError)
			return
		}
		if !solic.Administrador {
			http.Error(w, "no está autorizado a modificar usuarios", http.StatusForbidden)
			return
		}

		// Modifico
		err = s.updater.Update(r.Context(), fromDTO(req))
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "modificando usuario"), w, r)
			return
		}
	}
}

// CambiarContraseña se llama desde la página /blanquear o luego de caducada.
// Se usa esta opción cuando el usuario decide voluntariamente cambiar de clave,
// o cuando se vence por el plazo.
func (s *Server) cambiarPassword() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := usuarios.CambiarPasswordReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		err = s.patcher.CambiarPassword(r.Context(), req)
		if err != nil {
			switch t := err.(type) {
			case usuarios.ErrContraseñaIncorrecta:
				httperr.InternalServerError(w, errors.New(t.Frontend()))
				return

			default:
				httperr.InternalServerError(w, err)
				return
			}
		}
	}
}

func (s *Server) comitentesDisponibles(userID string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Valido
		if userID == "" {
			http.Error(w, "no se ingresó ID de usuario", http.StatusBadRequest)
			return
		}

		// Busco
		out, err := s.comitentesGetter.ComitentesDisponibles(r.Context(), userID)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando comitentes disponibles"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
