package usuarios

import (
	"net/http"

	"bitbucket.org/marcos19/one/pkg/usuarios"
)

type Handler interface {
	usuarios.Creater
	usuarios.Updater
	usuarios.ReaderOne
	usuarios.ReaderMany
	usuarios.Patcher
	usuarios.ComitentesGetter
}

// Si pasa los controles devuelve 'then'.
// Si no los pasa devuelve una handlerfunc que responde Forbidden
type Auther func(http.HandlerFunc) http.HandlerFunc
