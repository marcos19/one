package usuarios

import (
	"context"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/pkg/usuarios"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestContextData(t *testing.T) {

	t.Run("missing user", func(t *testing.T) {
		s := Server{withAuthorization: authMock}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusUnauthorized, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("missing comitente", func(t *testing.T) {
		s := Server{}
		w := httptest.NewRecorder()

		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		// Pego usuario
		nuevoCtx := contexto.SetUser(r.Context(), "user")
		r = r.WithContext(nuevoCtx)

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusUnauthorized, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("should work", func(t *testing.T) {
		s := Server{
			readerMany:        readManyMock{},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		// Pego usuario
		nuevoCtx := contexto.SetUser(r.Context(), "user")
		// Pego comitente
		nuevoCtx = contexto.SetComitente(nuevoCtx, 1)
		r = r.WithContext(nuevoCtx)
		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
}

type readManyMock struct {
	err        bool
	withValues bool
	nil        bool
}

func (r readManyMock) ReadMany(context.Context, usuarios.ReadManyReq) (out []usuarios.Usuario, err error) {

	if r.err {
		return nil, errors.Errorf("testing err")
	}
	if r.nil {
		return nil, nil
	}
	if !r.withValues {
		out = []usuarios.Usuario{}
		return
	}
	out = append(out, usuarios.Usuario{ID: "TestID", Nombre: "TestName"})
	return

}

func TestReadOne(t *testing.T) {

	t.Run("business layer error", func(t *testing.T) {
		s := Server{
			readerOne:         &readOneMock{err: true},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "54", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
	t.Run("should work", func(t *testing.T) {
		s := Server{
			readerOne:         &readOneMock{},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "34", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())
		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
}

type readOneMock struct {
	err   bool
	admin bool
}

func (r readOneMock) ReadOne(context.Context, string) (usuarios.Usuario, error) {
	if r.err {
		return usuarios.Usuario{}, errors.Errorf("test err")
	}
	return usuarios.Usuario{Administrador: r.admin}, nil
}

func TestComitentes(t *testing.T) {

	t.Run("business layer error", func(t *testing.T) {
		s := Server{
			comitentesGetter: comitentesMock{err: true},
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "comitentes/marcos", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
	t.Run("should work", func(t *testing.T) {
		s := Server{
			comitentesGetter: comitentesMock{},
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "comitentes/marcos", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())
		s.ServeHTTP(w, r)
		body := readBody(w.Result().Body)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", body)
		assert.Equal(t,
			`[{"ID":"1","Nombre":"nombre1"},{"ID":"2","Nombre":"nombre2"}]`+"\n",
			body,
		)
	})
}

type comitentesMock struct {
	err bool
	nil bool
}

func (m comitentesMock) ComitentesDisponibles(context.Context, string) (out []usuarios.ComitentesDisponiblesResp, err error) {

	if m.err {
		return out, errors.Errorf("test error")
	}
	if m.nil {
		return nil, nil
	}
	out = []usuarios.ComitentesDisponiblesResp{
		{
			ID:     1,
			Nombre: "nombre1",
		},
		{
			ID:     2,
			Nombre: "nombre2",
		},
	}

	return
}

func TestReadMany(t *testing.T) {

	t.Run("business layer error", func(t *testing.T) {
		s := Server{
			readerMany:        &readManyMock{err: true},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
	t.Run("nil return", func(t *testing.T) {
		s := Server{
			readerMany:        &readManyMock{nil: true},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		body := readBody(w.Result().Body)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", body)
		expected := "[]\n"
		assert.Equal(t, expected, body, "expected: %v, returned: %v", expected, body)
	})

	t.Run("empty slice", func(t *testing.T) {
		s := Server{
			readerMany:        &readManyMock{},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		body := readBody(w.Result().Body)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", body)
		expected := "[]\n"
		assert.Equal(t, expected, body, "expected: %v, returned: %v", expected, body)
	})

	t.Run("with values", func(t *testing.T) {
		s := Server{
			readerMany:        &readManyMock{withValues: true},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		r, err := http.NewRequest(http.MethodGet, "", nil)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		body := readBody(w.Result().Body)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", body)
		assert.True(t, len(body) > 10, "short body: %v", body)
	})
}

func TestCreate(t *testing.T) {

	t.Run("empty body", func(t *testing.T) {
		s := Server{
			creater:           &createMock{},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader("")
		r, err := http.NewRequest(http.MethodPost, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("malformed json", func(t *testing.T) {
		s := Server{
			creater:           &createMock{},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{: ""}`,
		)
		r, err := http.NewRequest(http.MethodPost, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("user is not admin", func(t *testing.T) {
		s := Server{
			creater:           &createMock{err: true},
			readerOne:         &readOneMock{admin: false},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{"Nombre": "Testing name"}`,
		)
		r, err := http.NewRequest(http.MethodPost, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusForbidden, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})
	t.Run("business layer error", func(t *testing.T) {
		s := Server{
			creater:           &createMock{err: true},
			readerOne:         &readOneMock{admin: true},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{"Nombre": "Testing name"}`,
		)
		r, err := http.NewRequest(http.MethodPost, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("should work", func(t *testing.T) {
		s := Server{
			creater:           &createMock{err: false},
			readerOne:         &readOneMock{admin: true},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{"Nombre": "Testing name"}`,
		)
		r, err := http.NewRequest(http.MethodPost, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		body := readBody(w.Result().Body)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", body)
	})
}

type createMock struct {
	err bool
}

func (r *createMock) Create(context.Context, usuarios.Usuario) error {
	if r.err {
		return errors.Errorf("business layer error")
	}
	return nil
}

func TestUpdate(t *testing.T) {

	t.Run("empty body", func(t *testing.T) {
		s := Server{
			updater:           &updateMock{},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader("")
		r, err := http.NewRequest(http.MethodPut, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("malformed json", func(t *testing.T) {
		s := Server{
			updater:           &updateMock{},
			readerOne:         &readOneMock{},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{: ""}`,
		)
		r, err := http.NewRequest(http.MethodPut, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusBadRequest, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("business layer error", func(t *testing.T) {
		s := Server{
			updater:           &updateMock{err: true},
			readerOne:         &readOneMock{admin: true},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{"Nombre": "Testing name"}`,
		)
		r, err := http.NewRequest(http.MethodPut, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("should work", func(t *testing.T) {
		s := Server{
			updater:           &updateMock{err: false},
			readerOne:         &readOneMock{admin: true},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{"Nombre": "Testing name"}`,
		)
		r, err := http.NewRequest(http.MethodPut, "", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		body := readBody(w.Result().Body)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode, "%v", body)
		assert.Equal(t, "", body)
	})
}

type updateMock struct {
	err bool
}

func (r *updateMock) Update(context.Context, usuarios.Usuario) error {
	if r.err {
		return errors.Errorf("business layer error")
	}
	return nil
}

func TestCambiarPassword(t *testing.T) {

	t.Run("business layer error", func(t *testing.T) {
		s := Server{
			patcher:           patcherMock{err: true},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{"ID": "marcos","Anterior":"123","Nueva":"234"}`,
		)
		r, err := http.NewRequest(http.MethodPatch, "password", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusInternalServerError, w.Result().StatusCode, "%v", readBody(w.Result().Body))
	})

	t.Run("should work", func(t *testing.T) {
		s := Server{
			patcher:           patcherMock{err: false},
			withAuthorization: authMock,
		}
		w := httptest.NewRecorder()
		reqBody := strings.NewReader(
			`{"ID": "marcos","Anterior":"123","Nueva":"234"}`,
		)
		r, err := http.NewRequest(http.MethodPatch, "password", reqBody)
		require.Nil(t, err)
		r = r.WithContext(prepareCtx())

		s.ServeHTTP(w, r)
		assert.Equal(t, http.StatusOK, w.Result().StatusCode)
	})

}

type patcherMock struct {
	err bool
}

func (p patcherMock) CambiarPassword(context.Context, usuarios.CambiarPasswordReq) error {
	if p.err {
		return errors.Errorf("test err")
	}
	return nil
}

func authMock(in http.HandlerFunc) http.HandlerFunc {
	return in
}

func readBody(r io.ReadCloser) string {
	by, err := io.ReadAll(r)
	if err != nil {
		return "error leyendo body"
	}
	return string(by)
}

// Sets user and comitente
func prepareCtx() context.Context {
	// Pego usuario
	ctx := contexto.SetUser(context.Background(), "user")
	// Pego comitente
	ctx = contexto.SetComitente(ctx, 1)
	return ctx
}
