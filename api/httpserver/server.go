package httpserver

import (
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/pkg/log"

	afipHTTP "bitbucket.org/marcos19/one/api/httpserver/afip"
	aperturaHTTP "bitbucket.org/marcos19/one/api/httpserver/apertura"
	aplicadorHTTP "bitbucket.org/marcos19/one/api/httpserver/aplicador"
	aplicadorConfigHTTP "bitbucket.org/marcos19/one/api/httpserver/aplicador/config"
	apliprodHTTP "bitbucket.org/marcos19/one/api/httpserver/apliprod"
	autoaplicadorHTTP "bitbucket.org/marcos19/one/api/httpserver/autoaplicador"
	bcraHTTP "bitbucket.org/marcos19/one/api/httpserver/bcra"
	"bitbucket.org/marcos19/one/api/httpserver/bsuso"
	cacheHTTP "bitbucket.org/marcos19/one/api/httpserver/cache"
	cajasHTTP "bitbucket.org/marcos19/one/api/httpserver/cajas"
	categoriasHTTP "bitbucket.org/marcos19/one/api/httpserver/categorias"
	centrosHTTP "bitbucket.org/marcos19/one/api/httpserver/centros"
	centrosgruposHTTP "bitbucket.org/marcos19/one/api/httpserver/centrosgrupos"
	cierreHTTP "bitbucket.org/marcos19/one/api/httpserver/cierre"
	"bitbucket.org/marcos19/one/api/httpserver/comitentes"
	compsHTTP "bitbucket.org/marcos19/one/api/httpserver/comps"
	condicionesHTTP "bitbucket.org/marcos19/one/api/httpserver/condiciones"
	contablesHTTP "bitbucket.org/marcos19/one/api/httpserver/contables"
	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	cuentasHTTP "bitbucket.org/marcos19/one/api/httpserver/cuentas"
	cuentasgruposHTTP "bitbucket.org/marcos19/one/api/httpserver/cuentasgrupos"
	depositosHTTP "bitbucket.org/marcos19/one/api/httpserver/depositos"
	empresasHTTP "bitbucket.org/marcos19/one/api/httpserver/empresas"
	factHTTP "bitbucket.org/marcos19/one/api/httpserver/fact"
	factConfigHTTP "bitbucket.org/marcos19/one/api/httpserver/fact/config"
	factEsquemasHTTP "bitbucket.org/marcos19/one/api/httpserver/fact/esquemas"
	factPermisosHTTP "bitbucket.org/marcos19/one/api/httpserver/fact/permisos"
	"bitbucket.org/marcos19/one/api/httpserver/geo"
	gruposHTTP "bitbucket.org/marcos19/one/api/httpserver/grupos"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	inflacionHTTP "bitbucket.org/marcos19/one/api/httpserver/inflacion"
	listaspreciosHTTP "bitbucket.org/marcos19/one/api/httpserver/listasprecios"
	"bitbucket.org/marcos19/one/api/httpserver/lookups"
	minutasHTTP "bitbucket.org/marcos19/one/api/httpserver/minutas"
	minutasConfigHTTP "bitbucket.org/marcos19/one/api/httpserver/minutas/configs"
	minutasEsquemasHTTP "bitbucket.org/marcos19/one/api/httpserver/minutas/esquemas"
	monedasHTTP "bitbucket.org/marcos19/one/api/httpserver/monedas"
	oplogHTTP "bitbucket.org/marcos19/one/api/httpserver/oplog"
	opsHTTP "bitbucket.org/marcos19/one/api/httpserver/ops"
	opsbloqctaHTTP "bitbucket.org/marcos19/one/api/httpserver/opsbloqcta"
	"bitbucket.org/marcos19/one/api/httpserver/permisos"
	personasHTTP "bitbucket.org/marcos19/one/api/httpserver/personas"
	preciosHTTP "bitbucket.org/marcos19/one/api/httpserver/precios"
	productosHTTP "bitbucket.org/marcos19/one/api/httpserver/productos"
	sucursalesHTTP "bitbucket.org/marcos19/one/api/httpserver/sucursales"
	"bitbucket.org/marcos19/one/api/httpserver/usuarios"
	"bitbucket.org/marcos19/one/api/httpserver/usuariosgrupos"

	reclamosHTTP "bitbucket.org/marcos19/one/api/httpserver/reclamos"
	refundicionHTTP "bitbucket.org/marcos19/one/api/httpserver/refundicion"
	rendHTTP "bitbucket.org/marcos19/one/api/httpserver/rendiciones"
	rendConfigHTTP "bitbucket.org/marcos19/one/api/httpserver/rendiciones/configs"
	resbancarioHTTP "bitbucket.org/marcos19/one/api/httpserver/resbancario"
	resbancarioConfigHTTP "bitbucket.org/marcos19/one/api/httpserver/resbancario/config"
	sesionesHTTP "bitbucket.org/marcos19/one/api/httpserver/sesiones"
	socketHTTP "bitbucket.org/marcos19/one/api/httpserver/socket"
	unicidadesHTTP "bitbucket.org/marcos19/one/api/httpserver/unicidades"
	unicidadescontHTTP "bitbucket.org/marcos19/one/api/httpserver/unicidadescont"
	unicidadesDefHTTP "bitbucket.org/marcos19/one/api/httpserver/unicidadesdef"
	"bitbucket.org/marcos19/one/pkg/allhandlers"
	"bitbucket.org/marcos19/one/pkg/programas"
	"bitbucket.org/marcos19/one/pkg/socket"

	"github.com/cockroachdb/errors"
	"github.com/dgraph-io/ristretto"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Server es la estructura que agrupa todos los routers.
type Server struct {
	// Config Config

	// Handlers
	afipServer            *afipHTTP.Server
	aperturaServer        *aperturaHTTP.Server
	aplicadorServer       *aplicadorHTTP.Server
	aplicadorConfigServer *aplicadorConfigHTTP.Server
	autoaplicadorServer   *autoaplicadorHTTP.Server
	bcraServer            *bcraHTTP.Server
	bsusoServer           *bsuso.Server
	cajasServer           *cajasHTTP.Server
	cacheServer           *cacheHTTP.Server
	centrosServer         *centrosHTTP.Server
	categoriasServer      *categoriasHTTP.Server
	centrosGruposServer   *centrosgruposHTTP.Server
	cierreServer          *cierreHTTP.Server
	compsServer           *compsHTTP.Server
	comitentesServer      *comitentes.Server
	condicionesServer     *condicionesHTTP.Server
	contablesServer       *contablesHTTP.Server
	cuentasServer         *cuentasHTTP.Server
	cuentasgruposServer   *cuentasgruposHTTP.Server
	depositosServer       *depositosHTTP.Server
	empresasServer        *empresasHTTP.Server
	factServer            *factHTTP.Server
	factConfigsServer     *factConfigHTTP.Server
	factEsquemasServer    *factEsquemasHTTP.Server
	factPermisos          *factPermisosHTTP.Server
	geoServer             *geo.Server
	gruposServer          *gruposHTTP.Server
	inflacionServer       *inflacionHTTP.Server
	apliprodServer        *apliprodHTTP.Server
	listaspreciosServer   *listaspreciosHTTP.Server
	minutasServer         *minutasHTTP.Server
	minutasConfigServer   *minutasConfigHTTP.Server
	minutasEsquemasServer *minutasEsquemasHTTP.Server
	monedasServer         *monedasHTTP.Server
	opsServer             *opsHTTP.Server
	oplogServer           *oplogHTTP.Server
	opsbloqctaServer      *opsbloqctaHTTP.Server

	lookupsServer *lookups.Server

	personasServer          *personasHTTP.Server
	sucursalesServer        *sucursalesHTTP.Server
	preciosServer           *preciosHTTP.Server
	productosServer         *productosHTTP.Server
	programasHandler        *programas.Handler
	refundicionServer       *refundicionHTTP.Server
	reclamosServer          *reclamosHTTP.Server
	rendServer              *rendHTTP.Server
	rendConfigServer        *rendConfigHTTP.Server
	resBancarioServer       *resbancarioHTTP.Server
	resBancarioConfigServer *resbancarioConfigHTTP.Server
	sesionesServer          *sesionesHTTP.Server
	unicidadesServer        *unicidadesHTTP.Server
	unicidadesDefServer     *unicidadesDefHTTP.Server
	unicidadescontServer    *unicidadescontHTTP.Server
	usuariosServer          *usuarios.Server
	usuariosGruposServer    *usuariosgrupos.Server

	socketServer    *socketHTTP.Server
	frontEndHandler http.Handler
}

type Args struct {
	Conn *pgxpool.Pool
	// MailDialer *mail.Dialer

	// la clave que uso para codificar los JWT
	// SecretKey []byte

	// OmitirControlSesiones bool

	// Cuando se envían los mails, lo que aparece en el campo from.
	// Debería ser del tipo Sweet <no_responder@sweet.com.ar>
	// MailFrom string
	WebSocket *socket.Handler
	Cache     *ristretto.Cache
	Frontend  http.Handler
}

// New devuelve una nueva instancia de un servidor
func New(h *allhandlers.Handler, a Args) (s *Server, err error) {
	s = &Server{}
	s.frontEndHandler = a.Frontend
	// s.socketServer = socketHTTP.NewServer(h.Ssocket)

	// Para POST, PUT o DELETE es necesario que el usuario esté
	// tildado como administrador. Sino, solo pueden leer.
	readOnly := permisos.NewReadOnly(h.Usuarios)

	// Usuarios
	s.usuariosServer, err = usuarios.NewServer(h.Usuarios, readOnly.Can)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando usuarios server")
	}

	s.condicionesServer = condicionesHTTP.NewServer(h.Condiciones)

	// AFIP
	s.afipServer, err = afipHTTP.NewServer(h.AFIP, h.AFIPwsfe, h.AFIPwsaa, h.AFIPcert)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando AFIP server")
	}

	// Aplicador
	s.aplicadorServer, err = aplicadorHTTP.NewServer(h.Aplicador)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando Aplicador server")
	}

	// Aplicador config
	s.aplicadorConfigServer, err = aplicadorConfigHTTP.NewServer(h.AplicadorConfig)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando AplicadorConfig server")
	}

	s.autoaplicadorServer = autoaplicadorHTTP.NewServer(h.Autoaplicador)

	s.bsusoServer = bsuso.NewServer(h.Bsuso)

	s.bcraServer = bcraHTTP.NewServer(h.BCRA)

	// Cache
	s.cacheServer = cacheHTTP.NewServer(a.Cache)

	// Cajas
	s.cajasServer, err = cajasHTTP.NewServer(h.Cajas, h.WebSocket, readOnly.Can)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando cajas server")
	}

	// Centros
	s.centrosServer, err = centrosHTTP.NewServer(h.Centros, h.WebSocket)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando centros server")
	}

	// Centros grupos
	s.centrosGruposServer = centrosgruposHTTP.NewServer(h.CentrosGrupos)

	{ // Comps
		s.compsServer, err = compsHTTP.NewServer(h.Comps, readOnly.Can)
		if err != nil {
			return nil, errors.Wrap(err, "inciando CompsServer")
		}
	}

	// Categorías productos
	s.categoriasServer = categoriasHTTP.NewServer(h.Categorias)

	{ // Cuentas contables
		s.cuentasServer, err = cuentasHTTP.NewServer(h.Cuentas, readOnly.Can)
		if err != nil {
			return nil, errors.Wrap(err, "iniciando cuentas contables")
		}
	}

	// Cuentas grupos
	s.comitentesServer, err = comitentes.NewServer(h.Comitentes)
	if err != nil {
		return nil, errors.Wrap(err, "inciando comitentes server")
	}

	// Cuentas grupos
	s.cuentasgruposServer = cuentasgruposHTTP.NewServer(h.CuentasGrupos)

	// Depositos
	s.depositosServer = depositosHTTP.NewServer(h.Depositos)

	{ // Empresas
		s.empresasServer, err = empresasHTTP.NewServer(h.Empresas, readOnly.Can)
		if err != nil {
			return nil, errors.Wrap(err, "iniciando empresas server")
		}
	}

	// Fact config
	s.factConfigsServer = factConfigHTTP.NewServer(h.FactConfig)

	// Fact esquemas
	s.factEsquemasServer = factEsquemasHTTP.NewServer(h.FactEsquema)

	// Fact
	s.factServer = factHTTP.NewServer(h.Fact, h.FactInter, a.Conn)

	// Fact permisos
	s.factPermisos = factPermisosHTTP.NewServer(h.FactPermisos)

	// Geo
	s.geoServer = geo.NewServer(h.Geo)

	// Grupos
	s.gruposServer = gruposHTTP.NewServer(h.Grupos)

	// Inflación
	s.inflacionServer = inflacionHTTP.NewServer(h.Inflacion, h.InflacionConfig)

	// Inflación
	s.apliprodServer = apliprodHTTP.NewServer(h.Apliprod)

	// Listas precios
	s.listaspreciosServer = listaspreciosHTTP.NewServer(h.ListasPrecio)

	// Lookups permisos
	s.lookupsServer = lookups.NewServer(h.Lookups)

	// Minuta
	s.minutasServer = minutasHTTP.NewServer(h.Minuta)

	// Minuta config
	s.minutasConfigServer, err = minutasConfigHTTP.NewServer(h.MinutaConfig)
	if err != nil {
		return nil, errors.Wrap(err, "inciando minutas config server")
	}

	// Minuta esquemas
	s.minutasEsquemasServer, err = minutasEsquemasHTTP.NewServer(h.MinutaEsquema)
	if err != nil {
		return nil, errors.Wrap(err, "inciando minutas esquema server")
	}

	// Monedas
	s.monedasServer, err = monedasHTTP.NewServer(h.Monedas, h.WebSocket)
	if err != nil {
		return nil, errors.Wrap(err, "inciando monedas server")
	}

	s.opsbloqctaServer, err = opsbloqctaHTTP.NewServer(h.OpsBloqCta, h.WebSocket)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando OpsBloqCta server")
	}

	// Ops
	s.opsServer = opsHTTP.NewServer(h.Ops)

	//
	s.oplogServer = oplogHTTP.NewServer(a.Conn)

	// Personas
	s.personasServer, err = personasHTTP.NewServer(h.Personas, h.WebSocket)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando PersonasServer")
	}

	// Sucursales
	s.sucursalesServer, err = sucursalesHTTP.NewServer(h.Sucursales)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando sucursales server")
	}

	// Precios
	s.preciosServer = preciosHTTP.NewServer(h.Precios)

	// Productos
	s.productosServer, err = productosHTTP.NewServer(h.Productos)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando ProductosHandler")
	}

	// Reclamos
	s.reclamosServer, err = reclamosHTTP.NewServer(h.Reclamos, h.ReclamosTipos)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando ProductosHandler")
	}

	// Asiento de refundicion
	s.refundicionServer = refundicionHTTP.NewServer(h.Refundicion)
	s.cierreServer = cierreHTTP.NewServer(h.Cierre)
	s.aperturaServer = aperturaHTTP.NewServer(h.Apertura)

	// Rendicion
	s.rendServer = rendHTTP.NewServer(h.Rendicion)
	s.rendConfigServer = rendConfigHTTP.NewServer(h.RendicionConfig)

	// Resumen bancario configs
	s.resBancarioConfigServer = resbancarioConfigHTTP.NewServer(h.ResbancarioConfig)

	// Resumen bancario
	s.resBancarioServer = resbancarioHTTP.NewServer(h.Resbancario)

	// Sesiones
	s.sesionesServer, err = sesionesHTTP.NewServer(h.Usuarios, h.Sesiones, h.Usuarios)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando SesionesServer")
	}

	// Unicidades
	s.unicidadesServer = unicidadesHTTP.NewServer(h.Unicidades)

	// Unicidades def
	s.unicidadesDefServer = unicidadesDefHTTP.NewServer(h.Unicidadesdef)

	// Unicidades cont
	s.unicidadescontServer = unicidadescontHTTP.NewServer(h.Unicidadescont)

	// Usuarios
	s.usuariosGruposServer, err = usuariosgrupos.NewServer(h.UsuariosGrupos)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando usuariosgrupos Server")
	}

	// Socket server
	s.socketServer = socketHTTP.NewServer(a.WebSocket)

	// Contables
	s.contablesServer, err = contablesHTTP.NewServer(
		h.Aplifin,
		h.Balance,
		h.LibroDiario,
		h.Mayor,
		h.MayorProductos,
		h.ResumenCaja,
		h.SaldosPersonas,
		h.SaldosProductos,
		h.VistaPersona,
	)
	if err != nil {
		return nil, errors.Wrap(err, "inciando Contables")
	}

	return
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	// Saco el "backend"
	primerPalabra, _ := shiftPath(r.URL.Path)
	if primerPalabra != "backend" {
		log.Info(r.Context(), "sirviendo frontend...")
		s.frontEndHandler.ServeHTTP(w, r)
		return
	}

	log.SetURL(r)

	// Pego el path al context
	ctx := contexto.SetPath(r.Context(), r.URL.Path)
	r = r.WithContext(ctx)

	var head string

	// Saco el "backend"
	head, r.URL.Path = shiftPath(r.URL.Path)
	if head != "backend" {
		return
	}
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Sin autenticación
	switch head {
	case "login":
		s.sesionesServer.Login(w, r)
		return
	}

	// Leo los datos del token y los pego al Context
	// (userID, comitente, expiration)
	if head == "usuarios" && r.Method == http.MethodPatch {
		// para cuando estoy cambiando contraseña de un usuario
		// nuevo que no está loggeado
	} else {
		err := s.sesionesServer.Touch(w, r)
		if err != nil {
			return
		}
	}

	w.Header().Set("Content-Type", "application/json")

	switch head {

	case "logout":
		s.sesionesServer.Logout(w, r)

	case "afip":
		s.afipServer.ServeHTTP(w, r)

	case "apertura":
		s.aperturaServer.ServeHTTP(w, r)

	case "aplicador":
		s.aplicadorServer.ServeHTTP(w, r)

	case "aplicador_config":
		s.aplicadorConfigServer.ServeHTTP(w, r)

	case "apliprod":
		s.apliprodServer.ServeHTTP(w, r)

	case "autoaplicador":
		s.autoaplicadorServer.ServeHTTP(w, r)

	case "bcra":
		s.bcraServer.ServeHTTP(w, r)

	case "bsuso":
		s.bsusoServer.ServeHTTP(w, r)

	case "cache":
		s.cacheServer.ServeHTTP(w, r)

	case "cajas":
		s.cajasServer.ServeHTTP(w, r)

	case "categorias":
		s.categoriasServer.ServeHTTP(w, r)

	case "centros":
		s.centrosServer.ServeHTTP(w, r) // Centros de imputación

	case "centrosgrupos":
		s.centrosGruposServer.ServeHTTP(w, r)

	case "cierre":
		s.cierreServer.ServeHTTP(w, r)

	case "comitentes":
		s.comitentesServer.ServeHTTP(w, r)

	case "comprobantes":
		s.compsServer.ServeHTTP(w, r) // Configuración de los comprobantes

	case "condiciones":
		s.condicionesServer.ServeHTTP(w, r)

	case "contables":
		s.contablesServer.ServeHTTP(w, r)

	case "cuentas":
		s.cuentasServer.ServeHTTP(w, r)

	case "cuentasgrupos":
		s.cuentasgruposServer.ServeHTTP(w, r)

	case "depositos":
		s.depositosServer.ServeHTTP(w, r)

	case "empresas":
		s.empresasServer.ServeHTTP(w, r)

	case "fact":
		s.factServer.ServeHTTP(w, r)

	case "fact_configs":
		s.factConfigsServer.ServeHTTP(w, r)

	case "fact_esquemas":
		s.factEsquemasServer.ServeHTTP(w, r)

	case "fact_permisos":
		s.factPermisos.ServeHTTP(w, r)

	case "geo":
		s.geoServer.ServeHTTP(w, r)

	case "grupos":
		s.gruposServer.ServeHTTP(w, r)

	case "inflacion":
		s.inflacionServer.ServeHTTP(w, r)

	case "listasprecios":
		s.listaspreciosServer.ServeHTTP(w, r)

	case "lookups":
		s.lookupsServer.ServeHTTP(w, r)

	case "minuta":
		s.minutasServer.ServeHTTP(w, r)

	case "minutas_config":
		s.minutasConfigServer.ServeHTTP(w, r)

	case "minutas_esquema":
		s.minutasEsquemasServer.ServeHTTP(w, r)

	case "monedas":
		s.monedasServer.ServeHTTP(w, r)

	case "ops":
		s.opsServer.ServeHTTP(w, r)

	case "oplog":
		s.oplogServer.ServeHTTP(w, r)

	case "opsbloqcta":
		s.opsbloqctaServer.ServeHTTP(w, r)

	case "precios":
		s.preciosServer.ServeHTTP(w, r)

	case "programas":
		s.programasHandler.ServeHTTP(w, r)

	case "personas":
		s.personasServer.ServeHTTP(w, r)

	case "productos":
		s.productosServer.ServeHTTP(w, r)

	case "refundicion":
		s.refundicionServer.ServeHTTP(w, r)

	case "reclamos":
		s.reclamosServer.ServeHTTP(w, r)

	case "rendiciones":
		s.rendServer.ServeHTTP(w, r)

	case "rendiciones_config":
		s.rendConfigServer.ServeHTTP(w, r)

	case "resbancario":
		s.resBancarioServer.ServeHTTP(w, r)

	case "resbancario_config":
		s.resBancarioConfigServer.ServeHTTP(w, r)

	case "sesiones":
		s.sesionesServer.ServeHTTP(w, r)

	case "sucursales":
		s.sucursalesServer.ServeHTTP(w, r)

	case "usuarios":
		s.usuariosServer.ServeHTTP(w, r)

	case "unicidades":
		s.unicidadesServer.ServeHTTP(w, r)

	case "unicidadesdef":
		s.unicidadesDefServer.ServeHTTP(w, r)

	case "unicidadescont":
		s.unicidadescontServer.ServeHTTP(w, r)

	case "usuariosgrupos":
		s.usuariosGruposServer.ServeHTTP(w, r)

	case "websocket":
		s.socketServer.ServeHTTP(w, r)

	default:
		// Error
		httperr.NotFound(w, r)
	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
