package personas

import (
	"encoding/json"
	"fmt"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/personas"
	"bitbucket.org/marcos19/one/pkg/personas/handler"
	"bitbucket.org/marcos19/one/pkg/socket"
	"github.com/alash3al/go-pubsub"
	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
)

const Programa = "personas"

type Server struct {
	h      *handler.Handler
	socket Socket
}
type Socket interface {
	BroadcastMsgAUsuario(msg interface{}, usuario string)
}

func NewServer(h *handler.Handler, soc Socket) (s *Server, err error) {
	if h == nil {
		return s, errors.New("PersonasHandler no puede ser nil")
	}
	// ev, err := h.EventChan()
	if err != nil {
		return s, errors.New("Iniciando event chan")
	}
	s = &Server{h, soc}

	// go s.handleEvents(ev)
	return
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string

	// Saco lo que viene después
	head, r.URL.Path = shiftPath(r.URL.Path)
	head = strings.TrimLeft(head, "/")

	// Determino el usuario que estoy usando
	usuario, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}
	// Determino el comitente que estoy usando
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch head {
	case "many":
		s.ReadMany(comitente)(w, r)

	case "persona":
		s.ReadOne(comitente)(w, r)

	case "alta":
		s.Create(comitente, usuario)(w, r)

	case "modificar":
		s.Update(comitente, usuario)(w, r)

	case "eliminar":
		s.Delete(comitente, usuario)(w, r)

	default:
		httperr.NotFound(w, r)
		return
	}
}

func (s *Server) Create(comitente int, usuario string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := personas.Persona{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Creo
		req.Comitente = comitente
		id, err := s.h.Create(r.Context(), &req, usuario)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo ID
		err = json.NewEncoder(w).Encode(&struct{ ID uuid.UUID }{id})
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) ReadMany(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := personas.ReadManyReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.h.ReadMany(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) ReadOne(comitente int) http.HandlerFunc {

	req := struct{ ID uuid.UUID }{}
	return func(w http.ResponseWriter, r *http.Request) {

		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Busco la persona
		persona, err := s.h.ReadOne(r.Context(), personas.ReadOneReq{ID: req.ID, Comitente: comitente})
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&persona)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) Update(comitente int, usuario string) http.HandlerFunc {

	req := personas.Persona{}
	return func(w http.ResponseWriter, r *http.Request) {

		// Leo JSON
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Modifico
		req.Comitente = comitente
		err = s.h.Update(r.Context(), req, usuario)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

	}
}

func (s *Server) Delete(comitente int, usuario string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := personas.ReadOneReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Borro
		req.Comitente = comitente
		err = s.h.Delete(r.Context(), req, usuario)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}
	}
}

func (s *Server) handleEvents(c <-chan *pubsub.Message) {
	for {
		msg, ok := <-c
		if ok {
			out := socket.Msg{
				Recurso: Programa,
			}

			switch e := msg.GetPayload().(type) {

			case personas.EvPersonaCreada:
				out.Evento = socket.Created
				out.Usuario = e.Usuario
				out.Mensaje = fmt.Sprintf("%v creó persona '%v'", e.Usuario, e.Persona.Nombre)
				out.Comitente = e.Persona.Comitente

			case personas.EvPersonaModificada:
				out.Evento = socket.Updated
				out.Usuario = e.Usuario
				out.Mensaje = fmt.Sprintf("%v modificó persona '%v'", e.Usuario, e.Persona.Nombre)
				out.Comitente = e.Persona.Comitente

			case personas.EvPersonaEliminada:
				out.Evento = socket.Updated
				out.Usuario = e.Usuario
				out.Mensaje = fmt.Sprintf("%v borró persona '%v'", e.Usuario, e.Persona.Nombre)
				out.Comitente = e.Persona.Comitente
			}

			go s.socket.BroadcastMsgAUsuario(out, out.Usuario)
		}
	}
}

// ShiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
