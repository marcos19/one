package inflacion

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/inflacion"
	"bitbucket.org/marcos19/one/pkg/inflacion/config"
	"github.com/cockroachdb/errors"
)

type Server struct {
	h   *inflacion.Handler
	cfg *config.Handler
}

func NewServer(h *inflacion.Handler, cfg *config.Handler) *Server {
	return &Server{h: h, cfg: cfg}
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Determino el id de usuario
	user, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Determino el comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch head {
	case "previsualizar":
		s.Previsualizar(user, comitente)(w, r)

	case "confirmar":
		s.Confirmar(user, comitente)(w, r)

	case "read_many":
		s.ReadMany(user, comitente)(w, r)

	case "read_one":
		s.ReadOne(user, comitente)(w, r)

	case "delete":
		s.Delete(user, comitente)(w, r)

	case "configs_read_many":
		s.ConfigsReadMany(comitente)(w, r)

	case "configs_read_one":
		s.ConfigsReadOne(comitente)(w, r)

	case "configs_create":
		s.ConfigsCreate(comitente)(w, r)

	case "configs_update":
		s.ConfigsUpdate(comitente)(w, r)

	case "configs_delete":
		s.ConfigsDelete(comitente)(w, r)

	default:
		deferror.Frontend(deferror.NotFound("head"), w, r)
		return
	}
}

// HandlePrevisualizar genera las ops y las devuelve para que las vea el usuario.
func (s *Server) Previsualizar(user string, comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := inflacion.Req{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Genero
		req.Comitente = comitente
		req.Usuario = user
		out, err := s.h.Previsualizar(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "previsualizando ajustes por inflación"), w, r)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// HandleConfirmar genera las ops con los ajustes por inflación y los persiste.
func (s *Server) Confirmar(user string, comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := inflacion.Req{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Grabo
		req.Comitente = comitente
		req.Usuario = user
		out, err := s.h.Confirmar(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "ajustando por inflación"), w, r)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// Lista los ajustes ya realizados
func (s *Server) ReadMany(user string, comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := inflacion.ReadManyReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Grabo
		req.Comitente = comitente
		out, err := s.h.ReadMany(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// Busca un ajuste histórico
func (s *Server) ReadOne(user string, comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := inflacion.ReadOneReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Grabo
		req.Comitente = comitente
		out, err := s.h.ReadOne(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// Busca un ajuste histórico
func (s *Server) Delete(user string, comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := inflacion.ReadOneReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Grabo
		req.Comitente = comitente
		err = s.h.Delete(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) ConfigsReadMany(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := config.ReadManyReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.cfg.ReadMany(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) ConfigsReadOne(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := config.ReadOneReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.cfg.ReadOne(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) ConfigsCreate(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := config.Config{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		err = s.cfg.Create(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) ConfigsDelete(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := config.ReadOneReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		err = s.cfg.Delete(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) ConfigsUpdate(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := config.Config{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		err = s.cfg.Update(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
