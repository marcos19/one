// Package cajas es la API para acceder al backend de cajas.
//
// Los métodos aceptados son:
//   - GET
//   - POST
//   - PUT
//   - DELETE
package cajas

import (
	"encoding/json"
	"fmt"
	"net/http"
	"path"
	"strconv"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/cajas"
	"bitbucket.org/marcos19/one/pkg/permisos/lookups"
	"github.com/alash3al/go-pubsub"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/niler"
)

const Programa = "cajas"

type Server struct {
	readerOne         cajas.ReaderOne
	readerMany        cajas.ReaderMany
	creater           cajas.Creater
	updater           cajas.Updater
	deleter           cajas.Deleter
	lookerUp          cajas.LookerUp
	emitter           cajas.Emitter
	socket            Socket
	eventListener     <-chan *pubsub.Message
	withAuthorization Auther
}

type Socket interface {
	BroadcastMsgAComitente(msg interface{}, comitente int)
}

func NewServer(h Handler, soc Socket, a Auther) (s *Server, err error) {
	if niler.IsNil(h) {
		return s, errors.Errorf("Handler no puede ser nil")
	}
	if niler.IsNil(soc) {
		return s, errors.Errorf("Socket no puede ser nil")
	}
	if niler.IsNil(a) {
		return s, errors.Errorf("Auther no puede ser nil")
	}
	s = &Server{
		readerOne:         h,
		readerMany:        h,
		creater:           h,
		updater:           h,
		deleter:           h,
		lookerUp:          h,
		emitter:           h,
		withAuthorization: a,
	}

	s.socket = soc
	s.eventListener, err = s.emitter.EventChan()
	if err != nil {
		return s, errors.Wrap(err, "solicitando event listener")
	}
	go s.handleEvents(s.eventListener)

	return
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	usuario, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Leo comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	var then http.HandlerFunc

	switch r.Method {

	case http.MethodGet:

		// Una caja?
		if head != "" {
			then = s.readOne(comitente, head)
			break
		}

		// Lookup?
		vv := r.URL.Query()
		_, ok := vv["lookups"]
		if ok {
			then = s.lookup(comitente, usuario)
			break
		}

		// Devuelvo todas
		then = s.readMany(comitente)

	case http.MethodPost:
		then = s.create(comitente, usuario)

	case http.MethodPut:
		then = s.update(comitente, usuario)

	case http.MethodDelete:
		then = s.delete(comitente, head, usuario)

	default:
		http.Error(w, fmt.Sprintf("método %v no implementado", r.Method), http.StatusNotImplemented)
	}

	s.withAuthorization(then)(w, r)
}

func (s *Server) create(comitente int, usuario string) http.HandlerFunc {

	type response struct {
		ID int `json:",string"`
	}

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := cajas.Caja{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Inserto
		req.Comitente = comitente
		id, err := s.creater.Create(r.Context(), req, usuario)
		if err != nil {
			httperr.InternalServerError(w, err)
		}

		// Devuelvo ID
		res := response{id}
		err = json.NewEncoder(w).Encode(&res)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) update(comitente int, usuario string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := cajas.Caja{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Modifico
		req.Comitente = comitente
		err = s.updater.Update(r.Context(), req, usuario)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

	}
}

func (s *Server) readOne(comitente int, id string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Valido
		if id == "" {
			httperr.BadRequest(w, errors.Errorf("no se ingresó ID de caja"))
			return
		}
		idInt, err := strconv.Atoi(id)
		if err != nil {
			httperr.BadRequest(w, errors.Errorf("el ID debe ser numérico"))
			return
		}

		// Leo request
		req := cajas.ReadOneReq{
			Comitente: comitente,
			ID:        idInt,
		}

		// Busco
		out, err := s.readerOne.ReadOne(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) readMany(comitente int) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		req := cajas.ReadManyReq{Comitente: comitente}

		// Busco
		out, err := s.readerMany.ReadMany(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		if out == nil {
			out = []cajas.Caja{}
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) lookup(comitente int, usuario string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		// Busco
		out, err := s.lookerUp.Lookup(r.Context(), comitente, lookups.Programa(""), lookups.Usuario(usuario))
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) delete(comitente int, id string, usuario string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		if id == "" {
			httperr.BadRequest(w, errors.Errorf("no se ingresó ID"))
			return
		}

		// Leo request
		req := cajas.ReadOneReq{}
		req.Comitente = comitente
		idInt, err := strconv.Atoi(id)
		if err != nil {
			httperr.BadRequest(w, errors.Errorf("ID debe ser numérico"))
			return
		}
		req.ID = idInt

		err = s.deleter.Delete(r.Context(), req, usuario)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}

	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
