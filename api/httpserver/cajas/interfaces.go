package cajas

import (
	"net/http"

	"bitbucket.org/marcos19/one/pkg/cajas"
)

type Handler interface {
	cajas.ReaderOne
	cajas.ReaderMany
	cajas.Creater
	cajas.Updater
	cajas.Deleter
	cajas.Emitter
	cajas.LookerUp
}

// Si pasa los controles devuelve 'then'.
// Si no los pasa devuelve una handlerfunc que responde Forbidden
type Auther func(then http.HandlerFunc) http.HandlerFunc
