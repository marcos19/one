package cajas

import (
	"fmt"

	"bitbucket.org/marcos19/one/pkg/cajas"
	"bitbucket.org/marcos19/one/pkg/socket"
	"github.com/alash3al/go-pubsub"
)

func (s *Server) handleEvents(c <-chan *pubsub.Message) {
	for {
		msg, ok := <-c
		if ok {
			out := socket.Msg{
				Recurso: Programa,
			}
			comitente := 0

			switch e := msg.GetPayload().(type) {

			case cajas.EvCajaCreada:
				out.Evento = socket.Created
				out.Usuario = e.Usuario
				out.Mensaje = fmt.Sprintf("%v creó caja '%v'", e.Usuario, e.Caja.Nombre)
				out.Comitente = e.Caja.Comitente
				comitente = out.Comitente

			case cajas.EvCajaModificada:
				out.Evento = socket.Updated
				out.Usuario = e.Usuario
				out.Mensaje = fmt.Sprintf("%v modificó caja '%v'", e.Usuario, e.Caja.Nombre)
				out.Comitente = e.Caja.Comitente
				comitente = out.Comitente

			case cajas.EvCajaEliminada:
				out.Evento = socket.Updated
				out.Usuario = e.Usuario
				out.Mensaje = fmt.Sprintf("%v borró caja '%v'", e.Usuario, e.Caja.Nombre)
				out.Comitente = e.Caja.Comitente
				comitente = out.Comitente
			}

			go s.socket.BroadcastMsgAComitente(out, comitente)
		}
	}
}
