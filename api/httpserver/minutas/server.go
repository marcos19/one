package minutas

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/minuta"
)

type Server struct {
	h *minuta.Handler
}

func NewServer(h *minuta.Handler) *Server {
	return &Server{h}
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	usuario, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Leo comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch head {

	case "create":
		s.Create(comitente, usuario)(w, r)

	case "":
		s.ReadOne(comitente)(w, r)

	case "update":
		s.Update(comitente, usuario)(w, r)

	case "delete":
		s.Delete(comitente, usuario)(w, r)

	default:
		deferror.Frontend(deferror.NotFound(head), w, r)
	}

}

// HandleBuscarPorID devuelve la minuta solicitada por ID.
// Los listados los saco desde el general.
func (s Server) ReadOne(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := minuta.ReadOneReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.h.ReadOne(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando minuta"), w, r)
			return
		}

		// Codifico
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// HandleCreate crea una nueva minuta.
// Los listados los saco desde el general.
func (s *Server) Create(comitente int, usuario string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := minuta.Minuta{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Inserto
		req.Comitente = comitente
		req.Usuario = new(string)
		*req.Usuario = usuario
		id, err := s.h.Create(r.Context(), &req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "insertando minuta"), w, r)
			return
		}

		// Devuelvo JSON
		resp := struct{ ID uuid.UUID }{id}
		err = json.NewEncoder(w).Encode(&resp)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "codificando JSON"), w, r)
			return
		}
	}
}

func (s *Server) Update(comitente int, usuario string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := minuta.Minuta{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Inserto
		req.Comitente = comitente
		req.Usuario = new(string)
		*req.Usuario = usuario
		err = s.h.Update(r.Context(), req, usuario)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "modificando minuta"), w, r)
			return
		}
	}
}
func (s Server) Delete(comitente int, usuario string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := minuta.DeleteReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		req.Usuario = usuario
		err = s.h.Delete(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "eliminando minuta"), w, r)
			return
		}
	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
