package configs

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/minuta/configs"
	"github.com/cockroachdb/errors"
)

type Server struct {
	h *configs.Handler
}

func NewServer(h *configs.Handler) (s *Server, err error) {
	if h == nil {
		return s, errors.New("ConfigHandler no puede ser nil")
	}
	s = &Server{h}
	return
}

// ServeHTTP
func (h *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	_, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Leo comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch head {

	case "create":
		h.Create(comitente)(w, r)

	case "one":
		h.ReadOne(comitente)(w, r)

	case "many":
		h.ReadMany(comitente)(w, r)

	case "update":
		h.Update(comitente)(w, r)

	case "delete":
		h.Delete(comitente)(w, r)

	default:
		deferror.Frontend(deferror.NotFound(head), w, r)
	}

}

// Create crea una nueva configuración de minuta.
func (s *Server) Create(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo params
		req := configs.Config{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Inserto
		req.Comitente = comitente
		err = s.h.Create(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "creando minuta config"), w, r)
			return
		}
	}
}

// HandleConfig devuelve el listado de configs de minutas
func (s *Server) ReadOne(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo params
		req := configs.ReadOneReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.h.ReadOne(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando minuta config"), w, r)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// ReadMaby devuelve el listado de configs de minutas
func (s *Server) ReadMany(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo params
		req := configs.ReadManyReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.h.ReadMany(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando minutas configs en base de datos"), w, r)
			return
		}

		// Codifico JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// Update modifica una minuta existente.
func (s *Server) Update(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo params
		req := configs.Config{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Modifico
		req.Comitente = comitente
		err = s.h.Update(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "modificando minuta config"), w, r)
		}

	}
}

// Update modifica una minuta existente.
func (s *Server) Delete(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo params
		req := configs.ReadOneReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Modifico
		req.Comitente = comitente
		err = s.h.Delete(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "eliminando minuta config"), w, r)
		}
	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
