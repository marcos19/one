package bcra

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"github.com/cockroachdb/errors"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/bcra"
	"bitbucket.org/marcos19/one/pkg/deferror"
)

type Server struct {
	h *bcra.Handler
}

func NewServer(h *bcra.Handler) *Server {
	return &Server{h}
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	_, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	switch head {

	case "bancos":
		s.ReadBancos()(w, r)
	default:
		// Error
		deferror.Frontend(deferror.NotFound(head), w, r)
	}
}

func (s *Server) ReadBancos() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		// Busco
		out, err := s.h.Bancos(r.Context())
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando bancos definidos por BCRA"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "codificando JSON"), w, r)
			return
		}
	}
}
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
