package cuentas

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"path"
	"strconv"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/niler"
)

type Server struct {
	readerOne         cuentas.ReaderOne
	readerMany        cuentas.ReaderMany
	creater           cuentas.Creater
	updater           cuentas.Updater
	deleter           cuentas.Deleter
	funcionesGetter   cuentas.FuncionesGetter
	withAuthorization Auther
}

func NewServer(h Handler, auther Auther) (s *Server, err error) {
	if niler.IsNil(h) {
		return s, errors.Errorf("Handler no puede ser nil")
	}
	if niler.IsNil(auther) {
		return s, errors.Errorf("Auther no puede ser nil")
	}
	s = &Server{}
	s.readerOne = h
	s.readerMany = h
	s.creater = h
	s.updater = h
	s.deleter = h
	s.funcionesGetter = h
	s.withAuthorization = auther
	return
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	_, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Leo comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	var then http.HandlerFunc
	switch r.Method {

	case http.MethodGet:

		switch head {
		case "":
			vv := r.URL.Query()
			then = s.readMany(comitente, vv)

		case "funciones":
			then = s.funcionesContables()

		default:
			then = s.readOne(comitente, head)
		}

	case http.MethodPost:
		then = s.create(comitente)

	case http.MethodPut:
		then = s.update(comitente)

	case http.MethodDelete:
		then = s.delete(comitente, head)

	default:
		http.Error(w, fmt.Sprintf(`método "%v" no permitido`, r.Method), http.StatusNotImplemented)
		return
	}

	s.withAuthorization(then)(w, r)
}

func (s *Server) create(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Parseo
		req := cuentas.Cuenta{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Inserto
		req.Comitente = comitente
		id, err := s.creater.Create(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "insertando cuenta contable"), w, r)
			return
		}

		// Devuelvo JSON
		out := struct {
			ID int `json:",string"`
		}{id}
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
		}

	}
}

func (s *Server) readOne(comitente int, id string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		if id == "" {
			http.Error(w, fmt.Sprintf("ID no ingresado"), http.StatusBadRequest)
			return
		}
		idInt, err := strconv.Atoi(id)
		if err != nil {
			http.Error(w, fmt.Sprintf("ID inválido"), http.StatusBadRequest)
			return
		}
		req := cuentas.ReadOneReq{
			Comitente: comitente,
			ID:        idInt,
		}

		// Busco
		req.Comitente = comitente
		out, err := s.readerOne.ReadOne(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando cuenta contable"), w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
		}
	}
}

func (s *Server) readMany(comitente int, vv url.Values) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := cuentas.ReadManyReq{}
		for k, v := range vv {
			if len(v) > 1 {
				deferror.Frontend(
					deferror.BadRequest(errors.Errorf("sólo se acepta un valor para campo '%v'", k)),
					w,
					r,
				)
				return
			}
			if len(v) == 0 {
				continue
			}

			switch strings.ToLower(k) {
			case "tipo":
				req.Tipo = v[0]
			case "funcion":
				req.Funcion = cuentas.Funcion(v[0])
			case "nombre":
				req.Nombre = v[0]
			}
		}

		// Busco
		req.Comitente = comitente
		out, err := s.readerMany.ReadMany(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "buscando cuentas contables"), w, r)
			return
		}

		if out == nil {
			out = []cuentas.Cuenta{}
		}
		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) update(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := cuentas.Cuenta{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Modifico
		err = s.updater.Update(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "modificando cuenta contable"), w, r)
		}

	}
}

func (s *Server) delete(comitente int, id string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		if id == "" {
			http.Error(w, "no se ingresó ID", http.StatusBadRequest)
			return
		}
		idInt, err := strconv.Atoi(id)
		if err != nil {
			http.Error(w, "ID debe ser numérico", http.StatusBadRequest)
			return
		}

		// Leo request
		req := cuentas.DeleteReq{
			Comitente: comitente,
			ID:        idInt,
		}

		// Borro
		req.Comitente = comitente
		err = s.deleter.Delete(r.Context(), req)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "modificando cuenta contable"), w, r)
		}

	}
}

func (s *Server) funcionesContables() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		out := s.funcionesGetter.FuncionesContables()

		err := json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
