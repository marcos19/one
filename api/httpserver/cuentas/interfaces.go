package cuentas

import (
	"net/http"

	"bitbucket.org/marcos19/one/pkg/cuentas"
)

type Handler interface {
	cuentas.ReaderOne
	cuentas.ReaderMany
	cuentas.Creater
	cuentas.Updater
	cuentas.Deleter
	cuentas.FuncionesGetter
}

// Si pasa los controles devuelve 'then'.
// Si no los pasa devuelve una handlerfunc que responde Forbidden
type Auther func(then http.HandlerFunc) http.HandlerFunc
