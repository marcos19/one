package contexto

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPathContext(t *testing.T) {

	path := "www.crosslogic.com.ar/dummy"
	r, err := http.NewRequest(http.MethodPost, path, nil)
	assert.Nil(t, err)

	ctx := SetPath(r.Context(), path)
	assert.NotNil(t, ctx)
	r = r.WithContext(ctx)

	leido, err := GetPath(r.Context())
	assert.Nil(t, err)
	assert.Equal(t, path, leido)

}
