package contexto

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUsuarioContext(t *testing.T) {

	r, err := http.NewRequest(http.MethodPost, "www.crosslogic.com.ar/dummy", nil)
	assert.Nil(t, err)

	usuario := "marcos"
	ctx := SetUser(r.Context(), usuario)
	assert.NotNil(t, ctx)
	r = r.WithContext(ctx)

	userLeido, err := GetUser(r.Context())
	assert.Nil(t, err)
	assert.Equal(t, usuario, userLeido)

}
