package contexto

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestComitenteContext(t *testing.T) {

	r, err := http.NewRequest(http.MethodPost, "www.crosslogic.com.ar/dummy", nil)
	assert.Nil(t, err)

	comitente := 12
	ctx := SetComitente(r.Context(), comitente)
	assert.NotNil(t, ctx)
	r = r.WithContext(ctx)

	leido, err := GetComitente(r.Context())
	assert.Nil(t, err)
	assert.Equal(t, comitente, leido)

}
