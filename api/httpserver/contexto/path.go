package contexto

import (
	"context"

	"github.com/cockroachdb/errors"
)

const pathKey = pathContextKey("Path")

type pathContextKey string

// SetPath pega el path original del request
func SetPath(in context.Context, e string) (out context.Context) {
	out = context.WithValue(in, pathKey, pathContextKey(e))
	return
}

// GetPath lee el path original del request
func GetPath(ctx context.Context) (id string, err error) {
	e, ok := ctx.Value(pathKey).(pathContextKey)
	if !ok {
		return "", errors.Errorf("no se pudo determinar el path")
	}

	return string(e), nil
}
