package contexto

import (
	"context"

	"github.com/cockroachdb/errors"
)

const bodyKey = bodyContextKey("Body")

type bodyContextKey string

// SetBody pega el body original del request
func SetBody(in context.Context, e string) (out context.Context) {
	out = context.WithValue(in, bodyKey, bodyContextKey(e))
	return
}

// GetBody lee el body original del request
func GetBody(ctx context.Context) (id string, err error) {
	e, ok := ctx.Value(bodyKey).(bodyContextKey)
	if !ok {
		return "", errors.Errorf("no se pudo determinar el body")
	}

	return string(e), nil
}
