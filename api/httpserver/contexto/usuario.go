package contexto

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/sesiones"
)

// El package context me exige que defina un tipo especial.
type usuarioKeyType string

const userKey = usuarioKeyType("UserID")

// SetUser devuelve un nuevo contexto con el usuario pegado.
func SetUser(in context.Context, user string) context.Context {
	return context.WithValue(in, userKey, user)
}

// GetUser lee un usuario del contexto del request
func GetUser(ctx context.Context) (user string, err error) {

	value := ctx.Value(userKey)
	if value == nil {
		return user, sesiones.ErrNotAuthenticated{}
	}

	user, ok := value.(string)
	if !ok {
		return user, sesiones.ErrNotAuthenticated{}
	}
	return
}
