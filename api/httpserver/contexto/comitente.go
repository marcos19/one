package contexto

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/deferror"
)

const comitenteKey = comitenteContextKey("Comitente")

type comitenteContextKey string

// SetComitente pega un comitente al contexto del request
func SetComitente(in context.Context, e int) (out context.Context) {
	out = context.WithValue(in, comitenteKey, e)
	return
}

// GetComitente lee el comiente en uso del request
func GetComitente(ctx context.Context) (id int, err error) {
	e, ok := ctx.Value(comitenteKey).(int)
	if !ok {
		return 0, deferror.ComitenteIndefinido()
	}

	return int(e), nil
}
