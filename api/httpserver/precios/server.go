package precios

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/precios"
)

type Server struct {
	h *precios.Handler
}

func NewServer(h *precios.Handler) *Server {
	return &Server{h}
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	_, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Leo comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch head {

	// case "disponibles":
	// 	h.HandleDisponibles(user, comitente)(w, r)

	case "many":
		s.ReadMany(comitente)(w, r)

	case "por_sku":
		s.PreciosPorSKU(comitente)(w, r)

	case "producto_skus_y_precios":
		s.ProductoSkusYPrecios(comitente)(w, r)

	case "update_por_sku":
		s.UpdatePorSku(comitente)(w, r)

	default:
		httperr.NotFound(w, r)
	}
}

func (s *Server) UpdatePorSku(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := precios.UpdatePorSkuReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Modifico
		req.Comitente = comitente
		err = s.h.UpdatePorSku(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

	}
}

// ProductoSkusYPrecios devuelve todos los datos al seleccionar
func (s *Server) ProductoSkusYPrecios(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := precios.ProductoSkusYPreciosReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.h.ProductoSkusYPrecios(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) PreciosPorSKU(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := precios.PreciosPorSKUReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.h.PreciosPorSKU(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) ReadMany(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := precios.ReadManyReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.h.ReadMany(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
