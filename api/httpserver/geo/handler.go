package geo

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/geo"
)

type Server struct {
	h *geo.Handler
}

func NewServer(h *geo.Handler) (s *Server) {
	return &Server{h}
}

func (h *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	switch head {

	case "ciudades":
		h.CiudadReadMany()(w, r)

	case "ciudad":
		h.CiudadReadOne()(w, r)

	case "provincias":
		// Devuelvo todas
		h.ProvinciasReadMany()(w, r)

	default:
		deferror.Frontend(deferror.NotFound(head), w, r)
	}
}

func (s *Server) CiudadReadMany() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := geo.ReadManyReq{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		out, err := s.h.CiudadReadMany(r.Context(), req)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) ProvinciasReadMany() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Busco
		out, err := s.h.ProvinciasMany(r.Context())
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

func (s *Server) CiudadReadOne() http.HandlerFunc {

	type req struct {
		ID int
	}
	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := req{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			deferror.Frontend(deferror.BadRequest(err), w, r)
			return
		}

		// Busco
		out, err := s.h.CiudadReadOne(r.Context(), req.ID)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
