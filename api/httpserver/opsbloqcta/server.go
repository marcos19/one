package opsbloqcta

import (
	"encoding/json"
	"fmt"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/api/httpserver/contexto"
	"bitbucket.org/marcos19/one/api/httpserver/httperr"
	"bitbucket.org/marcos19/one/pkg/opsbloqcta"
	"bitbucket.org/marcos19/one/pkg/socket"
	"github.com/alash3al/go-pubsub"
	"github.com/cockroachdb/errors"
)

const Programa = "opsbloqcta"

type Server struct {
	h             *opsbloqcta.Handler
	socket        Socket
	eventListener <-chan *pubsub.Message
}

type Socket interface {
	BroadcastMsgAComitente(msg interface{}, comitente int)
}

func NewServer(h *opsbloqcta.Handler, soc Socket) (s *Server, err error) {
	s = &Server{}
	s.h = h
	s.socket = soc
	s.eventListener, err = s.h.EventChan()
	if err != nil {
		return s, errors.Wrap(err, "solicitando event listener")
	}

	go s.handleEvents(s.eventListener)

	return
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	var head string
	head, r.URL.Path = shiftPath(r.URL.Path)

	// Leo usuario
	usuario, err := contexto.GetUser(r.Context())
	if err != nil {
		httperr.Unauthorized(w)
		return
	}

	// Leo comitente
	comitente, err := contexto.GetComitente(r.Context())
	if err != nil {
		httperr.ComitenteIndefinido(w)
		return
	}

	switch head {

	case "create":
		s.Create(comitente, usuario)(w, r)

	case "update":
		s.Update(comitente, usuario)(w, r)

	case "delete":
		s.Delete(comitente, usuario)(w, r)

	case "one":
		s.ReadOne(comitente)(w, r)

	case "many":
		s.ReadMany(comitente)(w, r)

	default:
		httperr.NotFound(w, r)
	}
}

func (s *Server) Create(comitente int, usuario string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := opsbloqcta.Bloqueo{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Inserto
		req.Comitente = comitente
		err = s.h.Create(r.Context(), req, usuario)
		if err != nil {
			httperr.InternalServerError(w, err)
		}
	}
}

func (s *Server) Update(comitente int, usuario string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := opsbloqcta.Bloqueo{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Modifico
		req.Comitente = comitente
		err = s.h.Update(r.Context(), req, usuario)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}
	}
}

func (s *Server) ReadOne(comitente int) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := opsbloqcta.Req{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.h.ReadOne(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) ReadMany(comitente int) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := opsbloqcta.Req{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Busco
		req.Comitente = comitente
		out, err := s.h.ReadMany(r.Context(), req)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}

		// Devuelvo JSON
		err = json.NewEncoder(w).Encode(&out)
		if err != nil {
			httperr.Encoding(w, err)
			return
		}
	}
}

func (s *Server) Delete(comitente int, usuario string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Leo request
		req := opsbloqcta.Req{}
		err := json.NewDecoder(r.Body).Decode(&req)
		if err != nil {
			httperr.BadRequest(w, err)
			return
		}

		// Modifico
		req.Comitente = comitente
		err = s.h.Delete(r.Context(), req, usuario)
		if err != nil {
			httperr.InternalServerError(w, err)
			return
		}
	}
}

// shiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}

func (s *Server) handleEvents(c <-chan *pubsub.Message) {
	for {
		msg, ok := <-c
		if ok {
			out := socket.Msg{
				Recurso: Programa,
			}
			comitente := 0

			switch e := msg.GetPayload().(type) {

			case opsbloqcta.EvBloqueoCreado:
				out.Evento = socket.Created
				out.Usuario = e.Usuario
				out.Mensaje = fmt.Sprintf("%v creó bloqueo de cuenta '%v'", e.Usuario, e.Bloqueo.Cuenta)
				out.Comitente = e.Bloqueo.Comitente
				comitente = out.Comitente

			case opsbloqcta.EvBloqueoModificado:
				out.Evento = socket.Updated
				out.Usuario = e.Usuario
				out.Mensaje = fmt.Sprintf("%v modificó bloqueo de cuenta '%v'", e.Usuario, e.Bloqueo.Cuenta)
				out.Comitente = e.Bloqueo.Comitente
				comitente = out.Comitente

			case opsbloqcta.EvBloqueoBorrado:
				out.Evento = socket.Updated
				out.Usuario = e.Usuario
				out.Mensaje = fmt.Sprintf("%v borró bloqueo de cuenta '%v'", e.Usuario, e.Bloqueo.Cuenta)
				out.Comitente = e.Bloqueo.Comitente
				comitente = out.Comitente
			}

			go s.socket.BroadcastMsgAComitente(out, comitente)
		}
	}
}
