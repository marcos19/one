# One

![](assets/logo.png)

One is an Enterprise Resource Planning software (ERP) built with the objective of
providing data granularity and agregation necesary for business descisions.

## Features

- Interated accounting
- Billing (electronic billing for Argentina using SOAP)
- Cash management.
- Inventory management.
- Multi tenant.
- Multi user.
- Multi company.
- Multi currency.
- Integration with bank information.

## Architecture

Both frontend and backend are packed into a binary using:

```bash
make build
```

For testing and debugging purposes, to spin up CockroachDB database:

```bash
make dbup
```

Run frontend and backend tests:

```bash
make test
```

For production database SSL should be used:

```bash
nohup ./one serve \
--ssl=true \
--certRoot=/path/to/certs/ca.crt \
--cert=/path/to/certs/client.root.crt \
--certKey=/path/to/certs/client.root.key
```
