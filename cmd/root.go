// Copyright © 2019 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	cfgFile  string
	dbHost   string
	dbName   string
	dbPort   int
	dbUser   string
	dbPass   string
	dbSSL    bool
	certRoot string
	cert     string
	certKey  string
	port     int
	debug    bool

	// Si es true, usa entorno Testing.
	testing bool

	Version        string
	BuildDate      string
	LastCommit     string
	LastCommitDate string
)

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "one",
	Short: "A brief description of your application",
	Long: `A longer description that spans multiple lines and likely contains
examples and usage of using your application. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports Persistent Flags, which, if defined here,
	// will be global for your application.

	RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.one.yaml)")
	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	RootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

	// DB
	RootCmd.PersistentFlags().StringVar(&dbHost, "dbHost", "localhost", "host de la base de datos")
	RootCmd.PersistentFlags().IntVar(&dbPort, "dbPort", 26257, "puerto en el que escucha la base de datos")
	RootCmd.PersistentFlags().StringVar(&dbUser, "dbUser", "root", "nombre de usuario de la base de datos")
	RootCmd.PersistentFlags().StringVar(&dbPass, "dbPass", "", "password de la base de datos")
	RootCmd.PersistentFlags().StringVar(&dbName, "dbName", "one", "nombre de la base de datos")
	RootCmd.PersistentFlags().BoolVar(&dbSSL, "ssl", false, "¿usa certificados SSL?")
	RootCmd.PersistentFlags().StringVar(&cert, "cert", "", "ubicación del archivo ca.crt")
	RootCmd.PersistentFlags().StringVar(&certRoot, "certRoot", "", "ubicación del archivo client.*.crt")
	RootCmd.PersistentFlags().StringVar(&certKey, "certKey", "", "ubicación del archivo client.*.key")
	RootCmd.PersistentFlags().BoolVar(&debug, "debug", false, "Si es true => LogLevel = Trace")
	RootCmd.PersistentFlags().BoolVar(&testing, "testing", false, "Si es true => usa webservices de AFIP de testing")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" { // enable ability to specify config file via flag
		viper.SetConfigFile(cfgFile)
	}

	viper.SetConfigName(".one")  // name of config file (without extension)
	viper.AddConfigPath("$HOME") // adding home directory as first search path
	viper.AutomaticEnv()         // read in environment variables that match

}
