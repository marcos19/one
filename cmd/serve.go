// Copyright © 2019 Marcos Bortoluss <marcosbortoluss@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"bytes"
	"context"
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
	"runtime"
	"time"

	"bitbucket.org/marcos19/one/api/httpserver"
	"bitbucket.org/marcos19/one/pkg/allhandlers"
	"bitbucket.org/marcos19/one/pkg/database"
	"bitbucket.org/marcos19/one/pkg/socket"
	"github.com/cockroachdb/errors"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/rakyll/statik/fs"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
)

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Inicia el servidor",
	Long:  `Sirve el backend y el frontend de todos los programas`,
	RunE: func(cmd *cobra.Command, args []string) (err error) {

		// Conecto a la base de datos
		if debug {
			log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
			zerolog.SetGlobalLevel(zerolog.DebugLevel)
		} else {
			zerolog.SetGlobalLevel(zerolog.InfoLevel)
		}
		log.Info().
			Str("go_version", runtime.Version()).
			Str("version", Version).
			Str("build_date", BuildDate).
			Str("last_commit", LastCommit).
			Str("last_commit_date", LastCommitDate).
			Bool("testing", testing).
			Bool("debug", debug).
			Str("certRoot", certRoot).
			Str("cert", cert).
			Str("certKey", certKey).
			Msgf("iniciando serve command...")
		log.Info().Str("db", dbName).Msgf("conectando a base de datos")

		cn := database.ConnectionString{
			Host:     dbHost,
			DB:       dbName,
			User:     dbUser,
			Pass:     dbPass,
			Port:     dbPort,
			SSL:      dbSSL,
			CertRoot: certRoot,
			Cert:     cert,
			Key:      certKey,
		}

		var conn *pgxpool.Pool
		for {
			cfg, err := pgxpool.ParseConfig(cn.String())
			if err != nil {
				return errors.Wrap(err, "inciando pgxpool config")
			}

			cfg.ConnConfig.Logger = &logAdapter{log.Logger}
			if debug {
				cfg.ConnConfig.LogLevel = pgx.LogLevelDebug
			} else {
				cfg.ConnConfig.LogLevel = pgx.LogLevelWarn
			}
			conn, err = pgxpool.ConnectConfig(context.Background(), cfg)
			if err == nil {
				break
			}

			log.Warn().Msgf("No se pudo conectar, reintentando en 3 segundos: %v", err.Error())
			time.Sleep(time.Second * 3)
		}
		log.Info().Str("db", dbName).Msg("conexión establecida con PGX")

		// Creo el MailServer
		// mailDialer := mail.NewDialer(mailHost, mailPort, mailUser, mailPass)
		// log.Info().Str("mailHost", mailHost).
		// 	Str("mailUser", mailUser).Msg("")
		// mailDialer.TLSConfig = &tls.Config{InsecureSkipVerify: true}

		// WebSocket
		webSocket := socket.NewHandler()

		entorno := allhandlers.EntornoProduccion
		if testing {
			entorno = allhandlers.EntornoTesting
		}

		// Creo el Handlers
		handlerArgs := allhandlers.HandlerArgs{
			Conn: conn,
			// MailDialer: mailDialer,
			Entorno:   entorno,
			SecretKey: []byte("Llave secreta posta"),
			MailFrom:  "Sweet <no_responder@sweet.com.ar>",
			EventBus:  webSocket,
		}
		h, err := allhandlers.New(handlerArgs)
		if err != nil {
			return errors.Wrap(err, "iniciando handlers")
		}

		// Vue.JS
		statikFS, err := fs.New()
		if err != nil {
			log.Fatal().Msg(err.Error())
		}
		staticHandler := http.FileServer(statikFS)
		// http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// 	staticHandler.ServeHTTP(w, r)
		// })

		// Creo server
		s, err := httpserver.New(h, httpserver.Args{
			WebSocket: webSocket,
			Cache:     h.Cache,
			Frontend:  staticHandler,
			Conn:      conn,
		})
		if err != nil {
			return errors.Wrap(err, "inciando server")
		}

		log.Info().Int("puerto", port).Msgf("sirviendo HTTP server")
		err = http.ListenAndServe(fmt.Sprint(":", port), withLog(s))
		if err != nil {
			panic(err)
		}
		return err
	},
}

type logAdapter struct {
	logger zerolog.Logger
}

func (l *logAdapter) Log(ctx context.Context, level pgx.LogLevel, msg string, data map[string]interface{}) {
	switch level {
	case pgx.LogLevelTrace:
		if debug {
			l.logger.Trace().Fields(data).Msg(msg)
		}
	case pgx.LogLevelDebug:
		if debug {
			l.logger.Debug().Fields(data).Msg(msg)
		}
	case pgx.LogLevelInfo:
		l.logger.Debug().Fields(data).Msg(msg)
	case pgx.LogLevelWarn:
		l.logger.Warn().Fields(data).Msg(msg)
	case pgx.LogLevelError:
		l.logger.Error().Fields(data).Msg(msg)
	default:
		l.logger.Error().Interface("nivel no encontrado", level).Fields(data).Msg(msg)
	}

}

func withLog(next http.Handler) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		bodyBytes, _ := io.ReadAll(r.Body)
		r.Body.Close() //  must close
		r.Body = io.NopCloser(bytes.NewBuffer(bodyBytes))
		bodyStr := string(bodyBytes)
		if len(bodyStr) > 200 {
			bodyStr = fmt.Sprintf("%v\nBody cortado. Tenía %v bytes", bodyStr[:200], len(bodyStr))
		}
		log.Debug().
			Str("method", r.Method).
			Str("path", r.URL.Path).
			Str("body", bodyStr)

		next.ServeHTTP(w, r)
	})
}

var webHost, mailHost, mailUser, mailPass, senderAlias string
var mailPort int

func init() {
	RootCmd.AddCommand(serveCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// serveCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// serveCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

	serveCmd.PersistentFlags().IntVar(&port, "port", 9040, "puerto en el que escucha el backend")
	// serveCmd.PersistentFlags().BoolVar(&insecure, "insecure", true, "No pide loggear, usa el usuario test")

	// Mail
	flag.StringVar(&webHost, "webHost", "sweet.crosslogic.com.ar", "host en el que escucha el webserver. Interesa para los links que pego en los mails")
	flag.StringVar(&mailHost, "mailHost", "mail.rrtvtecnica.com", "Host al que conecta el servidor de mails")
	flag.IntVar(&mailPort, "mailPort", 587, "Puerto en el que escuchará el servidor web")
	flag.StringVar(&mailUser, "mailUser", "contacto@rrtvtecnica.com", "Usuario que envía el mail")
	flag.StringVar(&mailPass, "mailPass", "1RRTVcontacto", "Contraseña del usuario de mail")
	flag.StringVar(&senderAlias, "sender", "noresponder@sweet.com.ar", "Nombre que aparecerá en el mail")
}
