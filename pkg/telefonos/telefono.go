package telefonos

// Telefono corresponde a un registro JSON de teléfono que tendrá una persona
// dentro del campo "telefonos"
type Telefono struct {
	Tipo           string `json:",omitempty"`
	Area           string `json:",omitempty"`
	Numero         string `json:",omitempty"`
	NumeroCompleto string `json:",omitempty"`
	Detalle        string `json:",omitempty"`
}
