package domicilios

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConcatenaciónDeDomicilio(t *testing.T) {
	// Domicilio Vacío
	ub := Domicilio{}
	rtdo := ub.String()
	if rtdo != "No disponible" {
		t.Error("Domicilio vacío mal concatenado.")
	}

	// Domicilio lleno
	{
		ubLlena := Domicilio{}
		ubLlena.DomCalle = "GRAL ROCA"
		ubLlena.DomNumero = "657"
		ubLlena.CiudadCP = 2520
		ubLlena.CiudadNombre = "LAS ROSAS"
		ubLlena.ProvinciaNombre = "Santa Fe"

		out := ubLlena.String()
		expected := "GRAL ROCA 657 - LAS ROSAS (2520) - Santa Fe"
		assert.Equal(t, expected, out)
	}

	{
		ubLlena := Domicilio{}
		ubLlena.DomCalle = "GRAL ROCA"
		ubLlena.DomNumero = "657"
		ubLlena.CiudadCP = 2520
		ubLlena.CiudadNombre = "LAS ROSAS"

		out := ubLlena.String()
		expected := "GRAL ROCA 657 - LAS ROSAS (2520)"
		assert.Equal(t, expected, out)
	}
}
