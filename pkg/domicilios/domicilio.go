package domicilios

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/cockroachdb/errors"
)

// Domicilio represanta un punto geográfico localizable independiente
// del contexto en que se lo utilice (persona, deposito, etc)
type Domicilio struct {
	DomCalle        string
	DomNumero       string
	DomPiso         string
	DomDpto         string
	CiudadID        int
	CiudadCP        int
	CiudadNombre    string
	ProvinciaID     int
	ProvinciaNombre string
	Tipo            string
	//PosX            // Coordenadas`json:"dom_calle"`
	//PosY            // Coordenadas
}

// CiudadString devuelve el nombre completo de la ciudad con CP y Provincia
func (u Domicilio) CiudadString() string {
	return fmt.Sprint(
		u.CiudadNombre,
		" (", u.CiudadCP, ") ",
		u.ProvinciaNombre,
	)
}

// Devuelve un string formateado así: "GRAL. ROCA 657 - Las Rosas (2520) - Santa Fe".
func (ub Domicilio) String() string {
	NoDisponible := Domicilio{}
	if ub == NoDisponible {
		return "No disponible"
	}
	partes := []string{ub.DomCalle, ub.DomNumero}
	if ub.DomPiso != "" {
		partes = append(partes, fmt.Sprintf("Piso: %v", ub.DomPiso))
	}
	if ub.DomDpto != "" {
		partes = append(partes, fmt.Sprintf("Dpto: %v", ub.DomDpto))
	}
	dom := strings.Join(partes, " ")

	ciudad := []string{}
	if ub.CiudadNombre != "" {
		ciudad = append(ciudad, ub.CiudadNombre)
	}
	if ub.CiudadCP != 0 {
		ciudad = append(ciudad, fmt.Sprintf("(%v)", ub.CiudadCP))
	}
	ciudadStr := strings.Join(ciudad, " ")

	completo := []string{dom, ciudadStr}
	if ub.ProvinciaNombre != "" {
		completo = append(completo, ub.ProvinciaNombre)
	}

	return strings.Join(completo, " - ")
}

// DireccionString devuelve concantenados Calle, Número, Piso y Depto.
func (ub Domicilio) DireccionString() string {

	NoDisponible := Domicilio{}
	if ub == NoDisponible {
		return "No disponible"
	}

	numero := ""
	if ub.DomNumero != "" {
		numero = " " + ub.DomNumero
	}

	piso := ""
	if ub.DomPiso != "" {
		piso = " " + ub.DomPiso
	}

	depto := ""
	if ub.DomDpto != "" {
		depto = " " + ub.DomDpto
	}

	return ub.DomCalle + numero + " " + piso + depto
}

// Devuelve el domicilio por defecto.
func DomicilioPorTipo(p []Domicilio, tipo string) Domicilio {
	for _, v := range p {
		if v.Tipo == tipo {
			return v
		}
	}
	Zero := Domicilio{}
	return Zero
}

type Domicilios []Domicilio

// Value cumple con la interface SQL
func (p Domicilios) Value() (driver.Value, error) {
	return json.Marshal(p)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (p *Domicilios) Scan(src interface{}) error {
	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New("Type assertion error .([]byte)")
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, p)
	if err != nil {
		return errors.Wrap(err, "Unmarshalling")
	}

	return nil
}
