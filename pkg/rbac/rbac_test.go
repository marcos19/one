// Usuarios
// Grupos de usuarios
// Roles
// Usuarios o grupos de usuarios tienen roles
package rbac

import (
	"fmt"
	"testing"

	"github.com/mikespook/gorbac"
)

func TestRBAC(t *testing.T) {
	var err error
	rbac := gorbac.New()

	// Los roles los crearía el usuario
	tesorero := gorbac.NewStdRole("tesorero")
	cajero := gorbac.NewStdRole("cajero")

	// Roles estarían hardcoded
	// pGen := gorbac.NewStdPermission("productos")
	p1 := gorbac.NewStdPermission("productos-crear")
	// p2 := gorbac.NewStdPermission("productos-modificar")
	// p3 := gorbac.NewStdPermission("cambiar precios")

	cajero.Assign(p1)

	rbac.Add(tesorero)
	if err != nil {
		t.Fatal(err)
	}
	rbac.Add(cajero)
	if err != nil {
		t.Fatal(err)
	}

	err = rbac.SetParent("tesorero", "cajero")
	if err != nil {
		t.Fatal(err)
	}

	puedeFacturar := rbac.IsGranted("cajero", p1, nil)
	if puedeFacturar {
		fmt.Println("cajero puede", p1.ID())
	} else {
		fmt.Println("cajero no puede", p1.ID())
	}

	{
		puedeFacturar := rbac.IsGranted("tesorero", p1, nil)
		if puedeFacturar {
			fmt.Println("tesorero puede", p1.ID())
		} else {
			fmt.Println("tesorero no puede", p1.ID())
		}
	}
	// cajero.Assign(pGen)
	// puedeDarPermisos := rbac.IsGranted("cajero", p1, nil)
	// if puedeDarPermisos {
	// 	fmt.Println("cajero puede", p1.ID())
	// } else {
	// 	fmt.Println("cajero no puede", p1.ID())
	// }
}
