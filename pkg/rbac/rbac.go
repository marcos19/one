package rbac

import (
	"sync"

	"github.com/mikespook/gorbac"
)

type Handler struct {
	permisos map[int]*gorbac.RBAC
	mu       *sync.RWMutex
}

func NewHandler() *Handler {
	return &Handler{
		permisos: map[int]*gorbac.RBAC{},
		mu:       &sync.RWMutex{},
	}
}

func (h *Handler) AgregarUsuario(comitente int) {
	// h.mu.RLock()
	// perm := h.permisos[comitente]

	// perm.SetParent(id string, parent string)
	// perm.Add(r gorbac.Role)
}
