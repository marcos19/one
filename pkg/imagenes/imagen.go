package imagenes

import (
	"bytes"
	"encoding/base64"
	"image"
	"io"
	"strings"

	"github.com/cockroachdb/errors"
	"github.com/disintegration/imaging"
)

// ComprimirFromBase64 achica la imagen si supera los máximos.
// Los dos parámetros de máximo son para una imagen horizontal.
// Si la imagen es vertical, se invierten.
func ComprimirFromBase64(in string, maxSize int) (out []byte, err error) {
	// Leo body
	b64data := in[strings.IndexByte(in, ',')+1:]
	datos, err := base64.StdEncoding.DecodeString(b64data)
	if err != nil {
		return datos, errors.Wrap(err, "leyendo base64")
	}

	return Comprimir(datos, maxSize)
}

// Comprimir achica la imagen si supera los máximos.
// Los dos parámetros de máximo son para una imagen horizontal.
// Si la imagen es vertical, se invierten.
func Comprimir(in []byte, maxSize int) (out []byte, err error) {

	// Determino alto de la imagen
	img, _, err := image.DecodeConfig(bytes.NewReader(in))
	if err != nil {
		return nil, errors.Wrap(err, "determinando el tamaño de la imagen")
	}

	var res = &image.NRGBA{}

	// Imagen vertical
	// El límite vertical es el mayor
	if img.Height > img.Width {

		if img.Height < maxSize {
			// Esta dentro de los límites, no hago nada
			return in, nil
		}
		i, err := imaging.Decode(bytes.NewReader(in))
		if err != nil {
			return nil, errors.Wrap(err, "decoding image")
		}
		res = imaging.Resize(i, 0, maxSize, imaging.Lanczos)

	}

	// Imagen horizontal
	// El límite horizontal es el mayor
	if img.Height <= img.Width {

		if img.Width < maxSize {
			// Esta dentro de los límites, no hago nada
			return in, nil
		}
		i, err := imaging.Decode(bytes.NewReader(in))
		if err != nil {
			return nil, errors.Wrap(err, "decoding image")
		}
		res = imaging.Resize(i, maxSize, 0, imaging.Lanczos)

	}

	buf := bytes.NewBuffer(out)
	err = imaging.Encode(buf, res, imaging.JPEG)
	if err != nil {
		return nil, errors.Wrap(err, "transformando a bytes imagen reducida")
	}

	out, err = io.ReadAll(buf)
	if err != nil {
		return nil, errors.Wrap(err, "leyendo buffer")
	}

	return
}
