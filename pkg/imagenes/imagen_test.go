package imagenes

import (
	"bytes"
	"image"
	"io"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestComprimirHorizontal(t *testing.T) {
	file, err := os.Open("./testdata/horizontal.jpg")
	assert.Nil(t, err)

	// Leo datos imagen
	by, err := io.ReadAll(file)
	assert.Nil(t, err)

	config, _, err := image.DecodeConfig(bytes.NewReader(by))
	assert.Nil(t, err)
	assert.Equal(t, config.Height, 240)
	assert.Equal(t, config.Width, 360)

	{
		// Alto máximo: 241
		out, err := Comprimir(by, 361)
		assert.Nil(t, err)
		// Debería devolverme lo mismo.
		assert.Equal(t, by, out)
	}
	{
		out, err := Comprimir(by, 359)
		assert.Nil(t, err)
		// Debería devolverme una imagen más chica (menos bytes), pero no ocurre
		// assert.True(t, len(out) < len(by), "tamaño sin comprimir: %v, tamaño comprimido %v", len(by), len(out))

		red, _, err := image.DecodeConfig(bytes.NewReader(out))
		assert.Nil(t, err)
		assert.Equal(t, 239, red.Height)
		assert.Equal(t, 359, red.Width)
	}
}

func TestComprimirVertical(t *testing.T) {
	file, err := os.Open("./testdata/vertical.jpg")
	assert.Nil(t, err)

	// Leo datos imagen
	by, err := io.ReadAll(file)
	assert.Nil(t, err)

	config, _, err := image.DecodeConfig(bytes.NewReader(by))
	assert.Nil(t, err)
	assert.Equal(t, config.Width, 240)
	assert.Equal(t, config.Height, 360)

	{
		// Alto máximo: 241
		out, err := Comprimir(by, 361)
		assert.Nil(t, err)
		// Debería devolverme lo mismo.
		assert.Equal(t, by, out)
	}
	{
		out, err := Comprimir(by, 359)
		assert.Nil(t, err)

		// assert.True(t, len(out) < len(by), "tamaño sin comprimir: %v, tamaño comprimido %v", len(by), len(out))
		// Deberían ser menos bytes, pero no lo son

		// Debería devolverme una imagen más chica.
		red, _, err := image.DecodeConfig(bytes.NewReader(out))
		assert.Nil(t, err)
		assert.Equal(t, 359, red.Height)
		assert.Equal(t, 239, red.Width)

	}
}
func TestGrande(t *testing.T) {
	file, err := os.Open("./testdata/grande.jpg")
	assert.Nil(t, err)

	// Leo datos imagen
	by, err := io.ReadAll(file)
	assert.Nil(t, err)

	config, _, err := image.DecodeConfig(bytes.NewReader(by))
	assert.Nil(t, err)
	assert.Equal(t, config.Width, 3888)
	assert.Equal(t, config.Height, 2592)

	{
		// Alto máximo: 241
		out, err := Comprimir(by, 4000)
		assert.Nil(t, err)
		// Debería devolverme lo mismo.
		assert.Equal(t, by, out)
		assert.True(t, len(out) > 0)

	}
	{
		out, err := Comprimir(by, 1280)
		assert.Nil(t, err)
		// Debería devolverme una imagen más chica.
		assert.True(t, len(out) < len(by))
		assert.True(t, len(out) > 0)

	}
}
