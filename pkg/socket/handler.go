package socket

import (
	"net/http"
	"sync"

	"github.com/gorilla/websocket"
	"github.com/rs/zerolog/log"
)

// Handler
type Handler struct {
	upgrader websocket.Upgrader

	mutex *sync.RWMutex
	pool  map[key]struct{}
}

type key struct {
	Comitente int
	Usuario   string
	Socket    *websocket.Conn
}

// Msg es el formato que se usa para comunicar entre el backend
// y el frontend
type Msg struct {
	Comitente int                 `json:"-"`
	Evento    string              // Creó
	Recurso   string              // Caja
	Usuario   string              // marcos
	Formato   NotificacionFormato // is-success | is-danger
	Mensaje   interface{}
}

const (
	Created = "created"
	Updated = "updated"
	Deleted = "deleted"
)

// NewHandler inicializa un cache y devuelve el puntero a este.
func NewHandler() (h *Handler) {

	h = &Handler{}

	h.upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
	h.pool = map[key]struct{}{}
	h.mutex = &sync.RWMutex{}

	return
}

type NotificacionFormato string

const (
	NotificacionFormatoOk    = NotificacionFormato("is-success")
	NotificacionFormatoError = NotificacionFormato("is-danger")
)

func (h *Handler) AbrirConexion(comitente int, usuario string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		log.Debug().
			Int("comitente", comitente).
			Str("pkg", "socket").
			Msgf("abriendo nueva conexión")
		conn, err := h.upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Error().
				Int("comitente", comitente).
				Str("pkg", "socket").
				Msgf("upgrading")
		}

		h.mutex.Lock()
		h.pool[key{comitente, usuario, conn}] = struct{}{}
		h.mutex.Unlock()

		log.Debug().Str("remote address", conn.RemoteAddr().String()).Msgf("Abierto websocket")

		for {
			tipo, msg, err := conn.ReadMessage()
			if err != nil {
				log.Err(err).
					Int("comitente", comitente).
					Str("pkg", "socket").
					Msg("msg in")
				return
			}
			log.Debug().
				Int("comitente", comitente).
				Str("pkg", "socket").
				Int("tipo", tipo).
				Str("msg", string(msg)).Msg("msg in")
		}
	}
}

// Esta función transmite el mensaje a todos los sockets de un comitente.
func (h *Handler) BroadcastMsgAComitente(msg interface{}, comitente int) {
	log.Debug().Msgf("Broadcasting a %v sockets", len(h.pool))
	aQuitar := []key{}

	// Locks
	wg := &sync.WaitGroup{}
	h.mutex.RLock()
	wg.Add(len(h.pool))
	total := len(h.pool)

	// Envío mensajes a cada comitente, si se produce un error al enviar
	// doy de baja el socket
	for poolKey := range h.pool {
		if poolKey.Comitente != comitente {
			continue
		}
		go func(k key) {
			err := k.Socket.WriteJSON(msg)
			if err != nil {
				aQuitar = append(aQuitar, k)
			}
			wg.Add(-1)
		}(poolKey)
	}

	log.Debug().Msgf("Esperando que se termine el broadcasting... Había %v", total)
	wg.Wait()
	h.mutex.RUnlock()

	// Estos estbaan con error, los borro de la lista
	log.Debug().Msgf("Se van a borrar %v conexiones", len(aQuitar))
	h.mutex.Lock()
	for _, v := range aQuitar {
		delete(h.pool, v)
		log.Debug().Msgf("Eliminado websocket %v", v)
	}
	h.mutex.Unlock()
}

// Esta función transmite el mensaje a todos los sockets de un usuario.
func (h *Handler) BroadcastMsgAUsuario(msg interface{}, usuario string) {
	log.Debug().Msgf("Broadcasting a %v sockets", len(h.pool))
	aQuitar := []key{}

	// Locks
	wg := &sync.WaitGroup{}
	h.mutex.RLock()
	wg.Add(len(h.pool))
	total := len(h.pool)

	// Envío mensajes a cada comitente, si se produce un error al enviar
	// doy de baja el socket
	for poolKey := range h.pool {
		if poolKey.Usuario != usuario {
			continue
		}
		go func(k key) {
			h.mutex.Lock()
			err := k.Socket.WriteJSON(msg)
			h.mutex.Unlock()
			if err != nil {
				aQuitar = append(aQuitar, k)
			}
			wg.Add(-1)
		}(poolKey)
	}

	log.Debug().Msgf("Esperando que se termine el broadcasting... Había %v", total)
	wg.Wait()
	h.mutex.RUnlock()

	// Estos estbaan con error, los borro de la lista
	log.Debug().Msgf("Se van a borrar %v conexiones", len(aQuitar))
	h.mutex.Lock()
	for _, v := range aQuitar {
		delete(h.pool, v)
		log.Debug().Msgf("Eliminado websocket %v", v)
	}
	h.mutex.Unlock()
}
