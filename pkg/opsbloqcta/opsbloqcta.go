package opsbloqcta

import (
	"time"

	"github.com/crosslogic/fecha"
)

// No se podrá registra ningún asiento que toque esta cuenta,
// si la fecha no está dentro del desde hasta
type Bloqueo struct {
	Comitente int
	Empresa   int `json:",string"`
	Cuenta    int `json:",string"`
	Desde     *fecha.Fecha
	Hasta     *fecha.Fecha
	UpdatedAt *time.Time
}

func (b Bloqueo) TableName() string {
	return "ops_bloqueo_cta"
}

/*
CREATE TABLE IF NOT EXISTS ops_bloqueo_cta (
	comitente
		INT,
	empresa
		INT,
	cuenta
		INT,
	desde
		DATE,
	hasta
		DATE,
	PRIMARY KEY (comitente, empresa, cuenta)
	);
*/
