package opsbloqcta

type EvBloqueoCreado struct {
	Usuario string
	Bloqueo Bloqueo
}

type EvBloqueoModificado struct {
	Usuario string
	Bloqueo Bloqueo
}

type EvBloqueoBorrado struct {
	Usuario string
	Bloqueo Req
}
