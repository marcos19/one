package opsbloqcta

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/alash3al/go-pubsub"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/fecha"
	"github.com/crosslogic/niler"
	"github.com/dgraph-io/ristretto"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Handler struct {
	conn    *pgxpool.Pool
	cache   *ristretto.Cache
	cuentas cuentas.ReaderOne
	bus     *pubsub.Broker
}

// NewHandler inicializa un handler.
func NewHandler(conn *pgxpool.Pool, cuentas cuentas.ReaderOne, cache *ristretto.Cache) (h *Handler, err error) {

	if conn == nil {
		return h, errors.New("DB no puede ser nil")
	}
	if niler.IsNil(cuentas) {
		return h, errors.New("cuentas.ReaderOne no puede ser nil")
	}
	h = &Handler{
		conn:    conn,
		cache:   cache,
		cuentas: cuentas,
		bus:     pubsub.NewBroker(),
	}
	return
}

// Devuelve un channel en el que se publican los eventos
func (h *Handler) EventChan() (out <-chan *pubsub.Message, err error) {
	// Creo suscritptor
	sub, err := h.bus.Attach()
	if err != nil {
		return nil, errors.Wrap(err, "creando suscriptor")
	}

	// Lo suscribo
	h.bus.Subscribe(sub, "")

	out = sub.GetMessages()
	return
}

func (h *Handler) Create(ctx context.Context, req Bloqueo, usuario string) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se definió comitente")
	}
	if usuario == "" {
		return deferror.Validation("no se definió usuario")
	}
	if req.Empresa == 0 {
		return deferror.Validation("no se definió empresa")
	}
	if req.Cuenta == 0 {
		return deferror.Validation("no se ingresó cuenta")
	}

	// Persisto
	err = create(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "persistiendo registro")
	}

	// Disparo evento
	h.bus.Broadcast(EvBloqueoCreado{Usuario: usuario, Bloqueo: req}, "")

	return
}

func (h *Handler) ReadOne(ctx context.Context, req Req) (out Bloqueo, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se definió comitente")
	}
	if req.Empresa == 0 {
		return out, deferror.Validation("no se definió empresa")
	}
	if req.Cuenta == 0 {
		return out, deferror.Validation("no se ingresaron cuentas")
	}

	// Busco
	out, err = readOne(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando registro")
	}

	return
}

func (h *Handler) ReadMany(ctx context.Context, req Req) (out []Bloqueo, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se definió comitente")
	}

	// Busco
	out, err = readMany(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando registros")
	}

	return
}

func (h *Handler) Update(ctx context.Context, req Bloqueo, usuario string) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se definió comitente")
	}
	if req.Empresa == 0 {
		return deferror.Validation("no se definió empresa")
	}
	if req.Cuenta == 0 {
		return deferror.Validation("no se ingresó cuenta")
	}
	err = update(ctx, h.conn, req)
	if err != nil {
		return errors.Wrap(err, "modificando bloqueo")
	}
	// Disparo evento
	h.bus.Broadcast(EvBloqueoModificado{Usuario: usuario, Bloqueo: req}, "")

	return
}

func (h *Handler) Delete(ctx context.Context, req Req, usuario string) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se definió comitente")
	}
	if req.Empresa == 0 {
		return deferror.Validation("no se definió empresa")
	}
	if req.Cuenta == 0 {
		return deferror.Validation("no se ingresó cuenta")
	}

	// Persisto
	err = delete(ctx, h.conn, req)
	// Disparo evento
	h.bus.Broadcast(EvBloqueoBorrado{Usuario: usuario, Bloqueo: req}, "")

	return
}

func (h *Handler) Puede(ctx context.Context, fch fecha.Fecha, comitente int, empresa int, ctas tipos.GrupoInts) (err error) {

	// Valido
	if comitente == 0 {
		return deferror.Validation("no se definió comitente")
	}
	if empresa == 0 {
		return deferror.Validation("no se definió empresa")
	}
	if len(ctas) == 0 {
		return deferror.Validation("no se ingresaron cuentas")
	}

	// Busco
	found, err := readMany(ctx, h.conn, Req{Comitente: comitente, Empresa: empresa, Cuentas: ctas})
	if err != nil {
		return errors.Wrap(err, "buscando bloqueos de cuentas contables")
	}

	for _, v := range found {
		if v.Desde != nil {
			if fch < *v.Desde {
				cta, err := h.cuentas.ReadOne(ctx, cuentas.ReadOneReq{Comitente: comitente, ID: v.Cuenta})
				if err != nil {
					return errors.Wrap(err, "buscando nombre de cuenta")
				}
				return errors.Errorf("no se puede usar la cuenta '%v' con fecha anterior a %v", cta.Nombre, v.Desde)
			}
		}
		if v.Hasta != nil {
			if fch > *v.Hasta {
				cta, err := h.cuentas.ReadOne(ctx, cuentas.ReadOneReq{Comitente: comitente, ID: v.Cuenta})
				if err != nil {
					return errors.Wrap(err, "buscando nombre de cuenta")
				}
				return errors.Errorf("no se puede usar la cuenta '%v' con fecha posterior a %v", cta.Nombre, v.Hasta)
			}
		}
	}
	return
}
