package opsbloqcta

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	"github.com/jackc/pgx/v4/pgxpool"
)

func create(ctx context.Context, conn *pgxpool.Pool, req Bloqueo) (err error) {

	query := `INSERT INTO ops_bloqueo_cta (comitente, empresa, cuenta, desde, hasta, updated_at)
	VALUES ($1,$2,$3,$4,$5,$6)`

	_, err = conn.Exec(ctx, query, req.Comitente, req.Empresa, req.Cuenta, req.Desde, req.Hasta, time.Now())
	if err != nil {
		return deferror.DB(err, "inserting")
	}
	return
}

type Req struct {
	Comitente int
	Empresa   int `json:",string"`
	Cuenta    int `json:",string"`
	Cuentas   tipos.GrupoInts
}

func readOne(ctx context.Context, conn *pgxpool.Pool, req Req) (out Bloqueo, err error) {
	query := `
		SELECT comitente, empresa, cuenta, desde, hasta, updated_at 
		FROM ops_bloqueo_cta 
		WHERE comitente=$1 AND empresa=$2 AND cuenta=$3`

	err = conn.QueryRow(ctx, query, req.Comitente, req.Empresa, req.Cuenta).Scan(
		&out.Comitente, &out.Empresa, &out.Cuenta, &out.Desde, &out.Hasta, &out.UpdatedAt)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}

	return
}

func readMany(ctx context.Context, conn *pgxpool.Pool, req Req) (out []Bloqueo, err error) {
	out = []Bloqueo{}

	ww := []string{"comitente=$1"}
	pp := []interface{}{req.Comitente}

	if req.Empresa != 0 {
		pp = append(pp, req.Empresa)
		ww = append(ww, fmt.Sprintf("empresa=$%v", len(pp)))
	}
	if req.Cuenta != 0 {
		pp = append(pp, req.Cuenta)
		ww = append(ww, fmt.Sprintf("cuenta=$%v", len(pp)))
	}
	if len(req.Cuentas) > 0 {
		ww = append(ww, fmt.Sprintf("cuenta IN %v", req.Cuentas.ParaClausulaIn()))
	}

	where := "WHERE " + strings.Join(ww, " AND ")
	query := fmt.Sprintf(
		`SELECT comitente, empresa, cuenta, desde, hasta, updated_at FROM ops_bloqueo_cta %v`,
		where)

	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()
	for rows.Next() {
		v := Bloqueo{}
		err = rows.Scan(&v.Comitente, &v.Empresa, &v.Cuenta, &v.Desde, &v.Hasta, &v.UpdatedAt)
		if err != nil {
			return out, deferror.DB(err, "escaneando")
		}
		out = append(out, v)
	}
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}

	return
}

func update(ctx context.Context, conn *pgxpool.Pool, req Bloqueo) (err error) {
	query := `
	UPDATE ops_bloqueo_cta 
	SET desde=$4, hasta=$5, updated_at=$6
	WHERE comitente=$1 AND empresa=$2 AND cuenta=$3`

	_, err = conn.Exec(ctx, query, req.Comitente, req.Empresa, req.Cuenta, req.Desde, req.Hasta, time.Now())
	if err != nil {
		return deferror.DB(err, "updating")
	}
	return
}

func delete(ctx context.Context, conn *pgxpool.Pool, req Req) (err error) {

	res, err := conn.Exec(ctx, "DELETE FROM ops_bloqueo_cta WHERE comitente=$1 AND empresa=$2 AND cuenta=$3",
		req.Comitente, req.Empresa, req.Cuenta)
	if err != nil {
		return deferror.DB(err, "deleting")
	}
	if res.RowsAffected() == 0 {
		return errors.New("no se borró ningún registro")
	}
	return
}
