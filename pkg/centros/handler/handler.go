package handler

import (
	"context"
	"fmt"

	"bitbucket.org/marcos19/one/pkg/centros"
	"bitbucket.org/marcos19/one/pkg/centros/internal/db"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/alash3al/go-pubsub"
	"github.com/cockroachdb/errors"
	"github.com/dgraph-io/ristretto"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler da acceso al a todos los routes de personas.
type Handler struct {
	conn  *pgxpool.Pool
	cache *ristretto.Cache
	bus   *pubsub.Broker
}

// NewHandler inicializa un cache y devuelve el puntero a este.
func NewHandler(conn *pgxpool.Pool) (h *Handler) {

	h = &Handler{}
	h.conn = conn
	h.bus = pubsub.NewBroker()
	return
}

type HandlerArgs struct {
	Conn  *pgxpool.Pool
	Cache *ristretto.Cache
}

func (h *Handler) ReadMany(ctx context.Context, req centros.ReadManyReq) (out []centros.Centro, err error) {

	out, err = db.ReadMany(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando centros")
	}

	return
}

func (h *Handler) ReadOne(ctx context.Context, req centros.ReadOneReq) (out centros.Centro, err error) {

	out, err = db.ReadOne(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando centro")
	}

	return
}

func (h *Handler) Create(ctx context.Context, req centros.Centro, usuario string) (id int, err error) {

	// Valido
	if req.Nombre == "" {
		return id, deferror.Validation("No se ingresó nombre")
	}

	// Persisto
	id, err = db.Create(ctx, h.conn, req)
	if err != nil {
		return id, deferror.DB(err, "persistiendo centro")
	}
	h.bus.Broadcast(centros.EvCentroCreado{Usuario: usuario, Centro: req}, "")
	return
}

func (h *Handler) Update(ctx context.Context, req centros.Centro, usuario string) (err error) {

	// Valido
	if req.Nombre == "" {
		deferror.Validation("Debe ingresar un nombre para el centro")
	}

	// Persisto
	err = db.Update(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "modificando centro")
	}

	h.bus.Broadcast(centros.EvCentroModificado{Usuario: usuario, Centro: req}, "")

	return
}

func (h *Handler) Delete(ctx context.Context, req centros.ReadOneReq, usuario string) (err error) {

	// Valido
	if req.ID == 0 {
		return deferror.Validation("no se ingresó ID del centro")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó comitente")
	}
	if usuario == "" {
		return deferror.Validation("no se ingresó usuario")
	}
	// anterior
	anterior, err := h.ReadOne(ctx, req)
	if err != nil {
		return errors.Wrap(err, "trayendo centro a borrar")
	}
	// Invalido cache
	h.cache.Del(hashInt(centros.ReadOneReq{Comitente: req.Comitente, ID: req.ID}))

	// Borro
	err = db.Delete(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "borrando centro")
	}

	h.bus.Broadcast(centros.EvCentroEliminado{Usuario: usuario, Centro: anterior}, "")
	return
}

// Devuelve un channel en el que se publican los eventos
func (h *Handler) EventChan() (out <-chan *pubsub.Message, err error) {
	// Creo suscritptor
	sub, err := h.bus.Attach()
	if err != nil {
		return nil, errors.Wrap(err, "creando suscriptor")
	}

	// Lo suscribo
	h.bus.Subscribe(sub, "")

	out = sub.GetMessages()
	return
}

func hashInt(req centros.ReadOneReq) string {
	return fmt.Sprintf("%v/%v/%v", "Caja", req.Comitente, req.ID)
}
