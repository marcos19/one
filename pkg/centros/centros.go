// Package centros define los centros de imputación
package centros

import (
	"time"

	"github.com/crosslogic/fecha"
)

// Centro representa un centro de imputación
type Centro struct {
	ID                   int `json:",string"`
	Comitente            int
	Nombre               string
	EsFamiliaDeProductos bool
	ValidoDesde          *fecha.Fecha
	ValidoHasta          *fecha.Fecha
	CreatedAt            *time.Time
	UpdatedAt            *time.Time
}
