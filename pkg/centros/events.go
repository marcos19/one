package centros

type EvCentroCreado struct {
	Usuario string
	Centro  Centro
}

type EvCentroModificado struct {
	Usuario string
	Centro  Centro
}

type EvCentroEliminado struct {
	Usuario string
	Centro  Centro
}
