package db

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/centros"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/jackc/pgx/v4/pgxpool"
)

func ReadOne(ctx context.Context, conn *pgxpool.Pool, req centros.ReadOneReq) (out centros.Centro, err error) {

	// Valido
	if req.ID == 0 {
		return out, deferror.Validation("no se ingresó ID de centro a buscar")
	}
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}

	query := `SELECT id, comitente, nombre, valido_desde, valido_hasta, es_familia_de_productos, created_at, updated_at
	FROM centros
	WHERE comitente=$1 and id=$2`

	err = conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(&out.ID, &out.Comitente,
		&out.Nombre, &out.ValidoDesde, &out.ValidoHasta, &out.EsFamiliaDeProductos,
		&out.CreatedAt, &out.UpdatedAt,
	)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}

	return
}

func ReadMany(ctx context.Context, conn *pgxpool.Pool, req centros.ReadManyReq) (out []centros.Centro, err error) {
	out = []centros.Centro{}

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}
	ww := []string{"comitente=$1"}
	pp := []interface{}{req.Comitente}

	if req.SoloActivos {
		pp = append(pp, time.Now())
		ww = append(ww, fmt.Sprintf(
			"(valido_desde IS NULL OR valido_desde < $%v) AND (valido_hasta IS NULL OR valido_hasta > $%v)",
			len(pp), len(pp)),
		)
	}
	where := "WHERE " + strings.Join(ww, " AND ")
	query := fmt.Sprintf(`SELECT 
		id, comitente, nombre, valido_desde, valido_hasta, 
		es_familia_de_productos, created_at, updated_at
	FROM centros
	%v`, where)

	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}
	defer rows.Close()

	for rows.Next() {
		v := centros.Centro{}
		err = rows.Scan(&v.ID, &v.Comitente,
			&v.Nombre, &v.ValidoDesde, &v.ValidoHasta, &v.EsFamiliaDeProductos,
			&v.CreatedAt, &v.UpdatedAt,
		)
		out = append(out, v)
	}

	return
}

func Create(ctx context.Context, conn *pgxpool.Pool, req centros.Centro) (id int, err error) {

	if req.Comitente == 0 {
		return id, errors.Errorf("no se definió ID de comitente")
	}
	query := `INSERT INTO centros (comitente, nombre, valido_desde, valido_hasta, es_familia_de_productos,
	created_at) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`

	err = conn.QueryRow(ctx, query, req.Comitente, req.Nombre, req.ValidoDesde,
		req.ValidoHasta, req.EsFamiliaDeProductos, time.Now(),
	).Scan(&id)
	if err != nil {
		return id, deferror.DB(err, "inserting")
	}

	return

}

func Update(ctx context.Context, conn *pgxpool.Pool, req centros.Centro) (err error) {

	if req.Comitente == 0 {
		return errors.Errorf("no se definió ID de comitente")
	}
	if req.ID == 0 {
		return errors.Errorf("no se definió ID")
	}

	query := `UPDATE centros SET nombre=$3, valido_desde=$4, valido_hasta=$5, 
	es_familia_de_productos=$6,	updated_at=$7
	WHERE comitente=$1 AND id=$2`

	_, err = conn.Exec(ctx, query, req.Comitente, req.ID, req.Nombre, req.ValidoDesde,
		req.ValidoHasta, req.EsFamiliaDeProductos, time.Now(),
	)
	if err != nil {
		return deferror.DB(err, "updating")
	}

	return
}

func Delete(ctx context.Context, conn *pgxpool.Pool, req centros.ReadOneReq) (err error) {

	if req.Comitente == 0 {
		return errors.Errorf("no se definió ID de comitente")
	}
	if req.ID == 0 {
		return errors.Errorf("no se definió ID")
	}

	{ // Está usado en la contabilidad?
		count := 0
		query := `SELECT COUNT(centro) FROM partidas WHERE comitente=$1 AND centro=$2`
		err = conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(&count)
		if err != nil {
			return deferror.DB(err, "determinando si el centro estaba usado")
		}
		if count > 0 {
			return deferror.Validationf("No se puede borrar el centro porque fue usado %v en la contabilidad. Puede desactivarlo.", count)
		}
	}

	query := `DELETE FROM centros WHERE comitente=$1 AND id=$2`
	res, err := conn.Exec(ctx, query, req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "deleting")
	}
	if res.RowsAffected() == 0 {
		return deferror.DB(err, "no se borró ningún centro")
	}

	return
}
