package dbtest

import (
	"context"
	"testing"

	"bitbucket.org/marcos19/one/pkg/centros"
	"bitbucket.org/marcos19/one/pkg/centros/internal/db"
	"bitbucket.org/marcos19/one/pkg/database"
	"github.com/crosslogic/fecha"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDB(t *testing.T) {
	ctx := context.Background()

	cn := database.TestDBConnectionString()

	cfg, err := pgxpool.ParseConfig(cn)
	require.Nil(t, err)
	conn, err := pgxpool.ConnectConfig(context.Background(), cfg)
	require.Nil(t, err)

	// Preparo base de datos
	err = Prepare(ctx, conn)
	require.Nil(t, err)

	id := 0
	t.Run("CREATE", func(t *testing.T) {
		id, err = db.Create(ctx, conn, centroValido())
		require.Nil(t, err)
		assert.NotEqual(t, 0, id)
	})

	t.Run("READ", func(t *testing.T) {
		expected := centroValido()
		expected.ID = id
		e, err := db.ReadOne(ctx, conn, centros.ReadOneReq{
			Comitente: 1,
			ID:        id,
		})
		require.Nil(t, err)
		assert.NotEqual(t, 0, e.ID)
		assert.NotNil(t, 0, e.CreatedAt)
		assert.Nil(t, e.UpdatedAt)
		e.CreatedAt = nil
		e.ID = id
		assert.Equal(t, expected, e)
	})

	t.Run("UPDATE", func(t *testing.T) {
		expected := centroValidoModificado()
		expected.ID = id
		err = db.Update(ctx, conn, expected)
		require.Nil(t, err)

		leido, err := db.ReadOne(ctx, conn, centros.ReadOneReq{
			Comitente: 1,
			ID:        id,
		})
		require.Nil(t, err)
		assert.NotNil(t, leido.UpdatedAt)
		assert.NotNil(t, leido.CreatedAt)
		leido.CreatedAt = nil
		leido.UpdatedAt = nil
		assert.Equal(t, expected, leido)
	})
	t.Run("DELETE sin partida", func(t *testing.T) {
		err := db.Delete(ctx, conn, centros.ReadOneReq{
			Comitente: 1,
			ID:        id,
		})
		require.Nil(t, err)

		_, err = db.ReadOne(ctx, conn, centros.ReadOneReq{
			Comitente: 1,
			ID:        id,
		})
		require.NotNil(t, err)
	})

	t.Run("DELETE con partida", func(t *testing.T) {

		// Creo nueva
		idConMov, err := db.Create(ctx, conn, centroValido())
		require.Nil(t, err)
		assert.NotEqual(t, 0, id)

		// Borro partidas anteriores
		_, err = conn.Exec(ctx, "DELETE FROM partidas;")
		require.Nil(t, err)

		// Le agrego movimiento
		_, err = conn.Exec(ctx, "INSERT INTO partidas (id, comitente, centro) VALUES (1,1,$1)", idConMov)
		require.Nil(t, err)

		// No debe dejar borrar
		err = db.Delete(ctx, conn, centros.ReadOneReq{
			Comitente: 1,
			ID:        idConMov,
		})
		require.NotNil(t, err)

		_, err = db.ReadOne(ctx, conn, centros.ReadOneReq{
			Comitente: 1,
			ID:        idConMov,
		})

		// Tiene que seguir estando
		require.Nil(t, err)

	})
}

func centroValido() centros.Centro {
	desde := fecha.Fecha(20220101)
	hasta := fecha.Fecha(20230101)
	return centros.Centro{
		Nombre:               "Nombre",
		Comitente:            1,
		EsFamiliaDeProductos: true,
		ValidoDesde:          &desde,
		ValidoHasta:          &hasta,
	}
}

func centroValidoModificado() centros.Centro {
	desde := fecha.Fecha(20250101)
	hasta := fecha.Fecha(20260101)
	return centros.Centro{
		Nombre:               "Nombre2",
		Comitente:            1,
		EsFamiliaDeProductos: false,
		ValidoDesde:          &desde,
		ValidoHasta:          &hasta,
	}
}
