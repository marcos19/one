package centros

import (
	"context"

	"github.com/alash3al/go-pubsub"
)

type ReaderOne interface {
	ReadOne(context.Context, ReadOneReq) (Centro, error)
}
type ReaderMany interface {
	ReadMany(context.Context, ReadManyReq) ([]Centro, error)
}
type Creater interface {
	Create(context.Context, Centro, string) (int, error)
}
type Updater interface {
	Update(context.Context, Centro, string) error
}
type Deleter interface {
	Delete(context.Context, ReadOneReq, string) error
}
type Emitter interface {
	EventChan() (<-chan *pubsub.Message, error)
}

type ReadOneReq struct {
	ID        int `json:",string"`
	Comitente int
}
type ReadManyReq struct {
	Comitente   int
	SoloActivos bool
}
