package listasprecios

import (
	"time"

	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
)

type Lista struct {
	ID          int `json:",string"`
	Comitente   int `json:"-"`
	Nombre      string
	Moneda      string
	Tipo        string // Proporcional a otra
	ListaBase   int    `json:",string"`
	Porcentaje  dec.D4 // El porcentaje de recargo o descuento que se le aplica a la lista original
	ValidaDesde *fecha.Fecha
	ValidaHasta *fecha.Fecha
	CreatedAt   *time.Time
	UpdatedAt   *time.Time
}

func (l *Lista) TableName() string {
	return "listas_precios"
}

const (
	TipoListaDirecta      = "Directa"
	TipoListaProporcional = "Proporcional"
)
