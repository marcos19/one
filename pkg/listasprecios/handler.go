package listasprecios

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler es el encargado de recibir los http requests y emitir los responses.
type Handler struct {
	conn *pgxpool.Pool
}

// NewHandler inicializa un cache y devuelve el puntero a este.
func NewHandler(conn *pgxpool.Pool) (h *Handler) {

	h = &Handler{
		conn: conn,
	}
	return
}

func (h *Handler) Create(ctx context.Context, req Lista) (err error) {

	// Valido
	if req.Nombre == "" {
		return deferror.Validation("No se determinó nombre para la lista de precio")
	}
	if req.Tipo == TipoListaProporcional && req.ListaBase == 0 {
		return deferror.Validation("Si se define la lista como Proporcional, debe determinar cual es la lista base sobre la que trabajará")
	}

	// Inserto
	err = create(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "insertando lista de precio")
	}

	return
}

func (h *Handler) Update(ctx context.Context, req Lista) (err error) {

	// Valido
	if req.Nombre == "" {
		return deferror.Validation("No se determinó nombre para la lista de precio")
	}
	if req.Tipo == TipoListaProporcional && req.ListaBase == 0 {
		return deferror.Validation("Si se define la lista como Proporcional, debe determinar cual es la lista base sobre la que trabajará")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}

	// Modifico
	err = update(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "modificando lista de precio")
	}

	return
}

func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (out Lista, err error) {

	// Valido
	if req.ID == 0 {
		return out, deferror.Validation("no se ingresó ID de lista de precio")
	}
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}

	out, err = readOne(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando por ID")
	}

	return
}

func (h *Handler) ReadMany(ctx context.Context, req ReadManyReq) (out []Lista, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó el ID de comitente")
	}

	// Busco
	out, err = readMany(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando listas")
	}

	return
}
