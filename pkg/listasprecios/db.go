package listasprecios

import (
	"context"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/jackc/pgx/v4/pgxpool"
)

func create(ctx context.Context, conn *pgxpool.Pool, req Lista) (err error) {
	query := `INSERT INTO listas_precios (comitente, nombre, moneda, tipo, lista_base, porcentaje, valida_desde, valida_hasta, created_at)
	VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9)`

	_, err = conn.Exec(ctx, query, req.Comitente, req.Nombre, req.Moneda, req.Tipo, req.ListaBase, req.Porcentaje,
		req.ValidaDesde, req.ValidaHasta, time.Now())
	if err != nil {
		return deferror.DB(err, "inserting")
	}
	return
}

type ReadOneReq struct {
	ID        int `json:",string"`
	Comitente int
}

func readOne(ctx context.Context, conn *pgxpool.Pool, req ReadOneReq) (out Lista, err error) {
	query := `SELECT id, comitente, nombre, moneda, tipo, lista_base, porcentaje, 
		valida_desde, valida_hasta, created_at, updated_at 
		FROM listas_precios
	WHERE comitente=$1 and id=$2`

	err = conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(&out.ID, &out.Comitente,
		&out.Nombre, &out.Moneda, &out.Tipo, &out.ListaBase, &out.Porcentaje,
		&out.ValidaDesde, &out.ValidaHasta, &out.CreatedAt, &out.UpdatedAt)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}
	return
}

type ReadManyReq struct {
	Comitente int
}

func readMany(ctx context.Context, conn *pgxpool.Pool, req ReadManyReq) (out []Lista, err error) {
	out = []Lista{}

	query := `SELECT id, comitente, nombre, moneda, tipo, lista_base, porcentaje, 
		valida_desde, valida_hasta, created_at, updated_at 
		FROM listas_precios
	WHERE comitente=$1`

	rows, err := conn.Query(ctx, query, req.Comitente)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		v := Lista{}
		err = rows.Scan(&v.ID, &v.Comitente,
			&v.Nombre, &v.Moneda, &v.Tipo, &v.ListaBase, &v.Porcentaje,
			&v.ValidaDesde, &v.ValidaHasta, &v.CreatedAt, &v.UpdatedAt)
		if err != nil {
			return out, deferror.DB(err, "querying/scanning")
		}
		out = append(out, v)
	}
	return
}

func update(ctx context.Context, conn *pgxpool.Pool, req Lista) (err error) {
	query := `UPDATE listas_precios SET nombre=$3, moneda=$4, tipo=$5, lista_base=$6, 
	porcentaje=$7, valida_desde=$8, valida_hasta=$9, updated_at=$10
	WHERE comitente=$1 AND id=$2`

	_, err = conn.Exec(ctx, query, req.Comitente, req.ID, req.Nombre, req.Moneda,
		req.Tipo, req.ListaBase, req.Porcentaje, req.ValidaDesde, req.ValidaHasta, time.Now())
	if err != nil {
		return deferror.DB(err, "updating")
	}
	return
}
