package dbtest

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/migrations/ddl"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

func Prepare(ctx context.Context, conn *pgxpool.Pool) error {

	{ // Borro base de datos
		q := `DROP DATABASE IF EXISTS test_cajas;`
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "borro base de datos")
		}
	}
	{ // Creo base de datos
		q := `CREATE DATABASE test_cajas; USE test_cajas;`
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando base de datos")
		}
	}

	{ // Tablas adicionales
		q := `CREATE TABLE IF NOT EXISTS comitentes (ID int PRIMARY KEY);`
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla comitentes")
		}

		_, err = conn.Exec(ctx, "UPSERT INTO comitentes (id) VALUES (1);")
		if err != nil {
			return errors.Wrap(err, "insertando comitente")
		}
	}

	{ // Creo table de cajas
		q := ddl.Cajas().SQL
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla cajas")
		}
	}
	{
		// Partidas (la necesito para borrar
		q := `CREATE TABLE IF NOT EXISTS partidas (ID int PRIMARY KEY, comitente int references comitentes, caja int NOT NULL)`
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla partidas")
		}
	}

	return nil
}
