package db

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/marcos19/one/pkg/cajas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Insert persiste una ubicación en la base de datos
func Create(ctx context.Context, conn *pgxpool.Pool, v cajas.Caja) (id int, err error) {
	if v.Comitente == 0 {
		return id, errors.Errorf("no se ingresó ID de comitente")
	}
	query := `
		INSERT INTO cajas (comitente, nombre, acepta_saldo_negativo, otras_cuentas, created_at) 
		VALUES ($1,$2,$3,$4,now()) RETURNING id`
	err = conn.QueryRow(ctx, query,
		v.Comitente, v.Nombre, v.AceptaSaldoNegativo, v.OtrasCuentas).Scan(&id)
	if err != nil {
		return id, deferror.DB(err, "inserting")
	}
	return
}

func ReadMany(ctx context.Context, conn *pgxpool.Pool, req cajas.ReadManyReq) (out []cajas.Caja, err error) {
	out = []cajas.Caja{}

	if req.Comitente == 0 {
		return out, errors.New("no se ingresó ID de comitente")
	}
	ww := []string{}
	pp := []interface{}{}

	// Comitente
	pp = append(pp, req.Comitente)
	ww = append(ww, fmt.Sprintf("comitente = $%v", len(pp)))

	// IDs
	if len(req.IDs) > 0 {
		ww = append(ww, fmt.Sprintf("id IN %v", req.IDs.ParaClausulaIn()))
	}

	where := fmt.Sprintf("WHERE %v", strings.Join(ww, " AND "))
	query := fmt.Sprintf(`SELECT id, comitente, nombre, acepta_saldo_negativo, otras_cuentas, created_at, updated_at FROM cajas %v`, where)

	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "quering")
	}
	defer rows.Close()

	for rows.Next() {
		v := cajas.Caja{}
		err = rows.Scan(&v.ID, &v.Comitente, &v.Nombre, &v.AceptaSaldoNegativo, &v.OtrasCuentas, &v.CreatedAt, &v.UpdatedAt)
		out = append(out, v)
	}
	return
}

func ReadOne(ctx context.Context, conn *pgxpool.Pool, req cajas.ReadOneReq) (out cajas.Caja, err error) {
	if req.Comitente == 0 {
		return out, errors.New("no se ingresó ID de comitente")
	}
	if req.ID == 0 {
		return out, errors.New("no se ingresó ID de caja")
	}
	query := `
		SELECT id, comitente, nombre, acepta_saldo_negativo, otras_cuentas, created_at, updated_at 
		FROM cajas
		WHERE comitente=$1 AND id=$2`

	err = conn.
		QueryRow(ctx, query, req.Comitente, req.ID).
		Scan(&out.ID, &out.Comitente, &out.Nombre, &out.AceptaSaldoNegativo, &out.OtrasCuentas, &out.CreatedAt, &out.UpdatedAt)
	if err != nil {
		return out, deferror.DB(err, "quering/scanning")
	}

	return
}

func Delete(ctx context.Context, conn *pgxpool.Pool, req cajas.ReadOneReq) (err error) {

	// Valido
	if req.ID == 0 {
		return deferror.Validation("no se ingresó ID de caja")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó comitente")
	}

	// Está usada?
	count := 0
	err = conn.QueryRow(ctx, "SELECT COUNT(id) FROM partidas WHERE caja = $1", req.ID).Scan(&count)
	if err != nil {
		return errors.Wrap(err, "determinando si la caja estaba usada")
	}
	if count != 0 {
		return deferror.Validationf("No se puede borrar caja porque está usada %v veces en la contabilidad. Puede inactivarla.", count)
	}

	// Borro
	res, err := conn.Exec(ctx, "DELETE FROM cajas WHERE comitente=$1 AND id=$2", req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "borrando caja")
	}

	affected := res.RowsAffected()
	if affected == 0 {
		return errors.New("no se borró ninguna caja")
	}
	return

}

func Update(ctx context.Context, conn *pgxpool.Pool, req cajas.Caja) (err error) {
	if req.ID == 0 {
		return errors.New("no se ingresó ID de caja")
	}
	if req.Comitente == 0 {
		return errors.New("no se ingresó ID de comitente")
	}

	query := `
UPDATE cajas
SET	nombre=$3, acepta_saldo_negativo=$4, otras_cuentas=$5, updated_at=now()
WHERE comitente=$1 AND id=$2`

	res, err := conn.Exec(ctx, query, req.Comitente, req.ID, req.Nombre, req.AceptaSaldoNegativo, req.OtrasCuentas)
	if err != nil {
		return deferror.DB(err, "inserting")
	}
	if res.RowsAffected() == 0 {
		return errors.New("no row updated")
	}
	return

}
