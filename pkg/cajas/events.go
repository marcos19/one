package cajas

type EvCajaCreada struct {
	Usuario string
	Caja    Caja
}

type EvCajaModificada struct {
	Usuario string
	Caja    Caja
}

type EvCajaEliminada struct {
	Usuario string
	Caja    Caja
}
