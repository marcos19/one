// Package cajas está pensado para definir distintas "Cajas" (caja 1, tesorería, etc)
package cajas

import (
	"time"

	"bitbucket.org/marcos19/one/pkg/tipos"
)

// Caja representa una caja dentro de la empresa.
type Caja struct {
	ID                  int `json:",string"`
	Comitente           int `json:",string"`
	Nombre              string
	AceptaSaldoNegativo bool
	OtrasCuentas        tipos.GrupoInts
	CreatedAt           *time.Time
	UpdatedAt           *time.Time
}

// TableName es el nombre de la tabla en la base de datos.
func (u Caja) TableName() string {
	return "cajas"
}
