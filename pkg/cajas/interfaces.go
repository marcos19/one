package cajas

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/permisos/lookups"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/alash3al/go-pubsub"
)

type ReaderOne interface {
	ReadOne(context.Context, ReadOneReq) (Caja, error)
}
type ReaderMany interface {
	ReadMany(context.Context, ReadManyReq) ([]Caja, error)
}
type Creater interface {
	Create(ctx context.Context, c Caja, user string) (id int, err error)
}
type Updater interface {
	Update(ctx context.Context, c Caja, user string) (err error)
}
type Deleter interface {
	Delete(ctx context.Context, r ReadOneReq, user string) (err error)
}
type Emitter interface {
	EventChan() (<-chan *pubsub.Message, error)
}
type LookerUp interface {
	Lookup(ctx context.Context, comitente int, p lookups.Programa, u lookups.Usuario) ([]Caja, error)
}

type ReadManyReq struct {
	Comitente int
	IDs       tipos.GrupoInts
}
type ReadOneReq struct {
	ID        int `json:",string"`
	Comitente int
}
