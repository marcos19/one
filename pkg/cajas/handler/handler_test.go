package handler

import (
	"fmt"
	"testing"
	"time"

	"bitbucket.org/marcos19/one/pkg/cajas"
	pubsub "github.com/alash3al/go-pubsub"
	"github.com/davecgh/go-spew/spew"
)

func TestChannel(t *testing.T) {

	// bus := make(chan interface{})
	// // msg := ""

	// go func() {
	// 	msg := <-bus
	// 	fmt.Println(msg, "entró en segundo channel PRIMER channel")
	// }()

	// go func() {
	// 	// case bus <- msg:
	// 	msg := <-bus
	// 	fmt.Println(msg, "entró en segundo channel segundo channel")
	// }()

	// time.Sleep(1 * time.Second)
	// bus <- "Mensaje falopa"
	// time.Sleep(1 * time.Second)
	// bus <- "mensaje 1"

	// broker := pubsub.NewBroker()

	// // Creo suscriptor
	// sub, err := broker.Attach()
	// if err != nil {
	// 	t.Fatal(err)
	// }
	// broker.Subscribe(sub, "noticias")

	// // Creo suscriptor
	// sub2, err := broker.Attach()
	// if err != nil {
	// 	t.Fatal(err)
	// }
	// broker.Subscribe(sub2, "noticias")

	// sub.AddTopic("noticias")
	// sub2.AddTopic("noticias")

	// // broker.Broadcast("este es un payload", "c1")

	// ch1 := sub.GetMessages()
	// chAlter := sub.GetMessages()
	// ch2 := sub2.GetMessages()

	// go send(broker)
	// go recieve(sub.GetID(), ch1)
	// go recieve(sub.GetID(), chAlter)
	// go recieve(sub2.GetID(), ch2)

	// time.Sleep(10 * time.Second)

	h := Handler{
		bus: pubsub.NewBroker(),
	}
	ch, err := h.EventChan()
	if err != nil {
		t.Fatal(err)
	}
	go recieve(ch)

	h.bus.Broadcast(cajas.EvCajaCreada{}, "")
	time.Sleep(1 * time.Second)
	h.bus.Broadcast(cajas.EvCajaCreada{}, "")
	time.Sleep(1 * time.Second)
	h.bus.Broadcast(cajas.EvCajaModificada{}, "")
	time.Sleep(1 * time.Second)
	h.bus.Broadcast(cajas.EvCajaEliminada{}, "")
	time.Sleep(1 * time.Second)
}

// func send(b *pubsub.Broker) {
// 	count := 0
// 	for {
// 		count++
// 		msg := fmt.Sprintf("Mensaje %v", count)
// 		fmt.Println("Enviando", msg)
// 		b.Broadcast(msg, "noticias")
// 		time.Sleep(time.Duration(rand.Float64()*4) * time.Second)
// 	}
// }

func recieve(ch <-chan *pubsub.Message) {
	// fmt.Printf("Subscriber %v, receiving...\n")
	count := 0
	for {
		count++
		msg, ok := <-ch
		if ok {
			fmt.Printf("Message recieved: %s\n", spew.Sdump(msg))
		}
		// fmt.Println("Entró msg", count)
		// time.Sleep(time.Duration(rand.Float64()) * 4 * time.Second)
	}
}
