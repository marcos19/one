package handler

import (
	"context"
	"fmt"

	"bitbucket.org/marcos19/one/pkg/cajas"
	"bitbucket.org/marcos19/one/pkg/cajas/internal/db"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/permisos/lookups"
	"github.com/alash3al/go-pubsub"
	"github.com/cockroachdb/errors"
	"github.com/dgraph-io/ristretto"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler es el encargado de recibir los http requests y emitir los responses.
type Handler struct {
	conn    *pgxpool.Pool
	cache   *ristretto.Cache
	bus     *pubsub.Broker
	lookups *lookups.Handler
}

// NewHandler inicializa un handler.
func NewHandler(conn *pgxpool.Pool, cache *ristretto.Cache, l *lookups.Handler) (h *Handler, err error) {

	if conn == nil {
		return h, errors.New("Conn no puede ser nil")
	}
	h = &Handler{
		conn:    conn,
		cache:   cache,
		bus:     pubsub.NewBroker(),
		lookups: l,
	}
	return
}

// Devuelve un channel en el que se publican los eventos
func (h *Handler) EventChan() (out <-chan *pubsub.Message, err error) {
	// Creo suscritptor
	sub, err := h.bus.Attach()
	if err != nil {
		return nil, errors.Wrap(err, "creando suscriptor")
	}

	// Lo suscribo
	h.bus.Subscribe(sub, "")

	out = sub.GetMessages()
	return
}

// HandleNuevaCaja inserta una nueva caja.
func (h *Handler) Create(ctx context.Context, req cajas.Caja, usuario string) (id int, err error) {

	// Valido
	if req.Nombre == "" {
		return id, deferror.Validation("Debe ingresar un nombre para la caja")
	}

	// Persisto
	id, err = db.Create(ctx, h.conn, req)
	if err != nil {
		return id, deferror.DB(err, "persistiendo caja")
	}

	// Disparo evento
	h.bus.Broadcast(cajas.EvCajaCreada{Usuario: usuario, Caja: req}, "")

	return
}

// HandleOne modifica una nueva existente.
func (h *Handler) ReadOne(ctx context.Context, req cajas.ReadOneReq) (out cajas.Caja, err error) {

	// Busco en cache
	val, estaba := h.cache.Get(hashInt(req))
	if estaba {
		out, ok := val.(cajas.Caja)
		if ok && out.Comitente == req.Comitente {
			return out, nil
		}
	}

	// No estaba en cache
	out, err = db.ReadOne(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando caja por ID")
	}

	// Agrego a cache
	h.cache.Set(hashInt(req), out, 0)

	return
}

// HandleMany modifica una nueva existente.
func (h *Handler) ReadMany(ctx context.Context, req cajas.ReadManyReq) (out []cajas.Caja, err error) {

	out, err = db.ReadMany(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando cajas")
	}

	return
}

// Lookup trae las cajas permitidas para el usuario
func (h *Handler) Lookup(ctx context.Context, comitente int, programa lookups.Programa, usuario lookups.Usuario) (out []cajas.Caja, err error) {

	// Busco IDs permitidos
	idsPermitidos, err := h.lookups.Cajas(ctx, comitente, programa, usuario)
	if err != nil {
		return nil, errors.Wrap(err, "determinando cajas permitidas")
	}
	if len(idsPermitidos) == 0 {
		return
	}
	// Hay algún cero? => Devuelvo todas las cajas
	for _, v := range idsPermitidos {
		if v == 0 {
			idsPermitidos = []int{}
			break
		}
	}

	req := cajas.ReadManyReq{Comitente: comitente, IDs: idsPermitidos}

	// Busco las cajas
	out, err = db.ReadMany(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando cajas permitidas para el usuario")
	}

	return
}

func (h *Handler) Update(ctx context.Context, req cajas.Caja, usuario string) (err error) {

	// Valido
	if req.Nombre == "" {
		return deferror.Validation("Debe ingresar un nombre para la caja")
	}
	if req.Comitente == 0 {
		return deferror.ComitenteIndefinido()
	}

	// Invalido cache
	h.cache.Del(hashInt(cajas.ReadOneReq{Comitente: req.Comitente, ID: req.ID}))

	// Modifico
	err = db.Update(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "persistiendo modificación")
	}

	h.bus.Broadcast(cajas.EvCajaModificada{Usuario: usuario, Caja: req}, "")

	return
}

func (h *Handler) Delete(ctx context.Context, req cajas.ReadOneReq, usuario string) (err error) {

	// Busco anterior
	anterior, err := h.ReadOne(ctx, req)
	if err != nil {
		return errors.Wrap(err, "buscando caja a borrar")
	}

	// Invalido cache
	h.cache.Del(hashInt(cajas.ReadOneReq{Comitente: req.Comitente, ID: req.ID}))

	// Borro
	err = db.Delete(ctx, h.conn, req)
	if err != nil {
		return errors.Wrap(err, "borrando caja")
	}

	h.bus.Broadcast(cajas.EvCajaEliminada{Usuario: usuario, Caja: anterior}, "")
	return
}

func hashInt(req cajas.ReadOneReq) string {
	return fmt.Sprintf("%v/%v/%v", "Caja", req.Comitente, req.ID)
}
