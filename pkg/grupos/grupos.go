// Package grupos define un conjunto de items.
package grupos

import (
	"bitbucket.org/marcos19/one/pkg/tipos"
)

// Grupo guarda los datos de un grupo de entidades, que pueden tener int o uuid como key.
type Grupo struct {
	ID        int `json:",string"`
	Comitente int
	De        string
	Nombre    string
	Ints      tipos.GrupoInts
	Uuids     []string
	Strings   []string
}

// TableName devuelve el nombre de la tabla de grupos.
func (g Grupo) TableName() string {
	return "grupos"
}
