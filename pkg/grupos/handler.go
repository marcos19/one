package grupos

import (
	"context"
	"errors"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Handler struct {
	conn *pgxpool.Pool
}

// NewHandler incializa un Handler.
func NewHandler(conn *pgxpool.Pool) (h *Handler) {
	h = &Handler{}
	h.conn = conn
	return
}

type GruposReq struct {
	GrupoDe   string
	Comitente int
}

type GrupoResponse struct {
	ID     int `json:",string"`
	Nombre string
}

// GruposNombres devuelve un listado con los grupos existentes para el rubro solicitado
// (grupos de usuarios, grupos de empresas, etc.)
func (h *Handler) GruposNombres(ctx context.Context, req GruposReq) (out []GrupoResponse, err error) {

	query := `
		SELECT id, nombre
		FROM grupos
		WHERE de = $1
		ORDER BY nombre
		;`

	rows, err := h.conn.Query(ctx, query, req.GrupoDe)
	if err != nil {
		return out, deferror.DB(err, "buscando grupos")
	}
	defer rows.Close()

	for rows.Next() {
		gr := GrupoResponse{}
		err = rows.Scan(&gr.ID, &gr.Nombre)
		if err != nil {
			return out, deferror.DB(err, "escaneando rows")
		}
		out = append(out, gr)
	}

	return
}

type ReadOneReq struct {
	ID        int `json:",string"`
	Comitente int
}

// HandleGrupoByID devuelve el grupo en base al ID ingresado.
func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (out Grupo, err error) {

	query := `SELECT id, de, nombre, ints, uuids, strings FROM grupos WHERE id=$1`
	err = h.conn.QueryRow(ctx, query, req.ID).Scan(&out.ID, &out.De, &out.Nombre, &out.Ints, &out.Uuids, &out.Strings)
	if err != nil {
		return out, deferror.DB(err, "buscando grupo por ID")
	}
	return
}

// HandleModificarGrupo persiste un grupo.
func (h *Handler) Update(ctx context.Context, req Grupo) (err error) {

	// Valido
	if req.Nombre == "" {
		return deferror.Validation("Debe ingresar un nombre para el grupo")
	}

	// Valido tipo de grupo
	switch req.De {
	case "Usuarios", "Cuentas contables", "Empresas":
	default:
		return deferror.Validation("El tipo de grupo " + req.De + " no está definido")
	}

	query := "UPDATE grupos SET de=$1, nombre=$2, ints=$3, uuids=$4, strings=$5 WHERE id=$6"
	// Estamos ok persisto
	res, err := h.conn.Exec(ctx, query, req.De, req.Nombre, req.Ints, req.Uuids, req.Strings, req.ID)
	if err != nil {
		return deferror.DB(err, "updating")
	}
	if res.RowsAffected() == 0 {
		return errors.New("no se modificó ningún grupo")
	}

	return
}
