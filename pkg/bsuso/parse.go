package bsuso

import (
	"fmt"
	"io"
	"strconv"

	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"github.com/xuri/excelize/v2"
)

type BienImportado struct {
	ID                 uuid.UUID
	Empresa            int
	FechaAlta          fecha.Fecha
	FechaInicioAmort   fecha.Fecha
	Detalle            string
	Proveedor          string
	Rubro              int
	RubroNombre        string
	Cuenta             int `json:",string"`
	CuentaAmortAcum    int `json:",string"`
	UMDepreciacion     string
	VidaUtil           dec.D4
	VidaUtilAmortizada dec.D4
	Valor              dec.D2
	AmortAcum          dec.D2
	Centro             int
}

func Parse(r io.Reader, sheet string) (out []BienImportado, err error) {

	f, err := excelize.OpenReader(r)
	if err != nil {
		return out, errors.Wrap(err, "opening reader")
	}
	rows, err := f.GetRows(sheet)
	if err != nil {
		return out, errors.Wrap(err, "reading rows")
	}

	for i, v := range rows {
		if i == 0 {
			continue
		}
		if len(v) == 0 {
			break
		}
		b, err := parseRow(f, sheet, v, i)
		if err != nil {
			return out, errors.Wrapf(err, "en renglón %v", i+1)
		}
		out = append(out, b)
	}
	return
}

func parseRow(f *excelize.File, sheet string, row []string, idx int) (out BienImportado, err error) {

	// ID
	if row[0] == "" {
		out.ID, err = uuid.NewV4()
		if err != nil {
			return out, errors.Wrap(err, "creating ID")
		}
	} else {
		out.ID, err = uuid.FromString(row[0])
		if err != nil {
			return out, errors.Wrap(err, "parsing ID")
		}
	}

	// Empresa
	out.Empresa, err = strconv.Atoi(row[1])
	if err != nil {
		return out, errors.Wrapf(err, "parsing empresa '%v'", row[1])
	}

	// Fecha alta
	out.FechaAlta, err = parseDate(f, sheet, idx, 2)
	if err != nil {
		return out, errors.Wrapf(err, "parsing fecha alta %v", row[2])
	}

	// Fecha inicio amortización pongo la misma
	out.FechaInicioAmort = out.FechaAlta

	out.Proveedor = row[3]
	out.Detalle = row[4]

	// Rubro
	out.Rubro, err = strconv.Atoi(row[5])
	if err != nil {
		return out, errors.Wrapf(err, "parsing rubro %v", row[5])
	}

	// Cuenta valor origen
	out.Cuenta, err = strconv.Atoi(row[6])
	if err != nil {
		return out, errors.Wrapf(err, "parsing cuenta '%v'", row[6])
	}

	// Cuenta amort acum
	if row[7] != "" {
		out.CuentaAmortAcum, err = strconv.Atoi(row[7])
		if err != nil {
			return out, errors.Wrapf(err, "parsing cuenta '%v'", row[7])
		}
	}

	out.RubroNombre = row[8]
	out.UMDepreciacion = row[9]

	{ // Vida util
		fl, err := strconv.ParseFloat(row[10], 64)
		if err != nil {
			return out, errors.Wrapf(err, "parsing vida util '%v'", row[10])
		}
		out.VidaUtil = dec.NewD4(fl)
	}

	{ // Años amortizados
		fl, err := strconv.ParseFloat(row[11], 64)
		if err != nil {
			return out, errors.Wrapf(err, "parsing vida amortizada '%v'", row[11])
		}
		out.VidaUtilAmortizada = dec.NewD4(fl)
	}

	{ // Valor
		fl, err := strconv.ParseFloat(row[12], 64)
		if err != nil {
			return out, errors.Wrapf(err, "parsing valor '%v'", row[12])
		}
		out.Valor = dec.NewD2(fl)
	}

	{ // Amort acum
		fl, err := strconv.ParseFloat(row[13], 64)
		if err != nil {
			return out, errors.Wrapf(err, "parsing amortización'%v'", row[13])
		}
		out.AmortAcum = dec.NewD2(fl)
	}
	return

}

func parseDate(f *excelize.File, sheet string, row, col int) (out fecha.Fecha, err error) {

	colName, err := excelize.ColumnNumberToName(col + 1)
	if err != nil {
		return out, errors.Wrapf(err, "transformando letra columna %v", col+1)
	}
	cell := fmt.Sprintf("%v%v", colName, row+1)

	estiloInt, err := f.NewStyle(&excelize.Style{NumFmt: 2}) // numero
	if err != nil {
		return out, errors.Wrap(err, "creando estilo int")
	}

	err = f.SetCellStyle(sheet, cell, cell, estiloInt)
	if err != nil {
		return out, errors.Wrap(err, "fijando estilo fecha a primer fila")
	}

	enStr, err := f.GetCellValue(sheet, cell)
	if err != nil {
		return out, errors.Wrapf(err, "leyendo valor %v", fmt.Sprintf("%v%v", colName, 1))
	}

	fl, err := strconv.ParseFloat(enStr, 64)
	if err != nil {
		return out, err
	}
	t, err := excelize.ExcelDateToTime(fl, false)
	if err != nil {
		return out, errors.Wrap(err, "ExcelDateToTime()")
	}
	out = fecha.NewFechaFromTime(t)
	return
}
