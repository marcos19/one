package bsuso

import (
	"context"
	"fmt"

	"bitbucket.org/marcos19/one/pkg/inflacion"
	"bitbucket.org/marcos19/one/pkg/minuta"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/davecgh/go-spew/spew"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
)

type mov struct {
	fecha    fecha.Fecha
	cuenta   int
	detalle  string
	monto    dec.D2
	um       string
	unicidad uuid.UUID
}

func generarPartidasAjusteInflacion(
	desde fecha.Mes,
	hasta fecha.Mes,
	ss []Saldo,
	movs []mov,
	recpam int,
	indices map[fecha.Mes]float64,
	uu map[uuid.UUID]unic,
) (out []minuta.Partida, err error) {

	log.Debug().Msgf("Total saldos: %v\nTotal movs: %v", len(ss), len(movs))
	if recpam == 0 {
		return out, errors.Errorf("no se ingresó cuenta de resultado para ajustar por inflación")
	}

	// Ajusto saldos de inicio
	coefInic, ok := indices[desde]
	if !ok {
		spew.Dump(indices)
		return out, errors.Errorf("no había índice de inflación para mes inicio %v", desde)
	}
	coefCierre, ok := indices[hasta]
	if !ok {
		spew.Dump(indices)
		return out, errors.Errorf("no había índice de inflación para mes hasta %v", hasta)
	}

	sum := dec.D2(0)

	for _, v := range ss {

		{ // Sobre el valor de origen
			// Patrimonial
			p := minuta.Partida{}
			p.UM = v.UM
			p.Cuenta = v.CuentaValorOrigen
			p.Unicidad = new(uuid.UUID)
			*p.Unicidad = v.Unicidad
			o := dec.NewD2(v.ValorOrigen.Float() * makeCoef(coefInic, coefCierre))
			if o > 0 {
				p.Debe = o
			} else {
				p.Haber = -o
			}
			p.Detalle = uu[v.Unicidad].Detalle
			p.Detalle += fmt.Sprintf(" (%v%%)", dec.NewD2(makeCoef(coefInic, coefCierre)*100).String())
			sum += o
			out = append(out, p)
		}

		if v.ValorAmortAcum != 0 { // Sobre amort acumulada
			// Patrimonial
			p := minuta.Partida{}
			p.UM = v.UM
			p.Cuenta = v.CuentaAmorAcum
			p.Unicidad = new(uuid.UUID)
			*p.Unicidad = v.Unicidad
			o := dec.NewD2(v.ValorAmortAcum.Float() * makeCoef(coefInic, coefCierre))
			if o > 0 {
				p.Debe = o
			} else {
				p.Haber = -o
			}
			p.Detalle = uu[v.Unicidad].Detalle
			p.Detalle += fmt.Sprintf(" (%v%%)", dec.NewD2(makeCoef(coefInic, coefCierre)*100).String())
			sum += o

			out = append(out, p)
		}
	}

	for _, v := range movs {

		inicio, ok := indices[v.fecha.PeriodoMes()]
		if !ok {
			return out, errors.Wrapf(err, "no había índice de inflación para mes %v", desde)
		}
		{ // Sobre el valor de origen
			// Patrimonial
			p := minuta.Partida{}
			p.UM = v.um
			p.Detalle = uu[v.unicidad].Detalle
			p.Cuenta = v.cuenta
			p.Unicidad = new(uuid.UUID)
			*p.Unicidad = v.unicidad
			o := dec.NewD2(v.monto.Float() * makeCoef(inicio, coefCierre))
			if o > 0 {
				p.Debe = o
			} else {
				p.Haber = -o
			}
			p.Detalle += fmt.Sprintf(" (%v%%)", dec.NewD2(makeCoef(inicio, coefCierre)*100).String())
			sum += o
			out = append(out, p)
		}

		// No debería haber amort acum. porque se dio de alta en el ejercicio,
		// y este proceso lo corro antes de amortizar.
	}
	// Resultado
	sum *= -1
	p := minuta.Partida{}
	p.Cuenta = recpam
	if sum > 0 {
		p.Debe = sum
	} else {
		p.Haber = -sum
	}
	p.Detalle = "Ajuste por inflación bienes de uso"
	out = append(out, p)

	filtrado := []minuta.Partida{}
	for _, v := range out {
		if v.Debe == 0 && v.Haber == 0 && v.Cantidad == 0 {
			continue
		}
		filtrado = append(filtrado, v)
	}
	return filtrado, nil
}

func makeCoef(inicio, ultimo float64) float64 {
	return (ultimo / inicio) - 1
}

func traerIndices(ctx context.Context, conn *pgxpool.Pool, comitente int, indice string, desde, hasta fecha.Mes) (out map[fecha.Mes]float64, err error) {
	if desde.PosteriorOIgual(hasta) {
		return out, errors.Errorf("fecha desde debe ser posterior a fecha hasta")
	}
	out = map[fecha.Mes]float64{}

	f := desde.SumarMeses(-1)
	for {
		f = f.SumarMeses(1)
		req := inflacion.CoeficienteReq{
			Comitente: comitente,
			Mes:       f,
			Indice:    indice,
		}
		coef, err := inflacion.Coeficiente(ctx, conn, req)
		if err != nil {
			return out, errors.Wrapf(err, "obteniendo coefiente '%v' de fecha %v", indice, f)
		}
		out[f] = coef
		if f == hasta {
			break
		}
	}
	return
}
