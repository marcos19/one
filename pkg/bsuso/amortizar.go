package bsuso

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/minuta"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

func calcularAmortizaciones(ctx context.Context, conn *pgxpool.Pool, ch cuentas.ReaderOne, funcs cuentas.PorFuncionGetter, req AmortizarReq) (out minuta.Minuta, err error) {

	// Traigo saldos
	s := TraerSaldosReq{
		Comitente: req.Comitente,
		Empresa:   req.Empresa,
		Fecha:     req.Fecha,
	}
	ss, err := TraerSaldos(ctx, conn, s, ch, funcs)
	if err != nil {
		return out, errors.Wrap(err, "trayendo saldos de bienes de uso")
	}

	// Genero minuta
	out, err = generarMinuta(req.compReq)
	if err != nil {
		return out, errors.Wrap(err, "generando minuta")
	}

	// Calculo amortizaciones
	out.Partidas, err = generarPartidasAmortizacion(ss, req.Avances, req.CtaRtdo)
	if err != nil {
		return out, errors.Wrap(err, "generando partidas")
	}
	return
}

type compReq struct {
	Comitente int
	Empresa   int `json:",string"`
	Fecha     fecha.Fecha
	Config    int `json:",string"`
	Esquema   int `json:",string"`
	Usuario   string
}

// Genera minuta (sin partidas)
func generarMinuta(req compReq) (out minuta.Minuta, err error) {
	// Valido
	if req.Comitente == 0 {
		return out, deferror.ComitenteIndefinido()
	}
	if req.Empresa == 0 {
		return out, deferror.EmpresaIndefinida()
	}
	if req.Fecha == 0 {
		return out, deferror.Validationf("no se ingresó fecha")
	}
	if req.Usuario == "" {
		return out, errors.Errorf("no se ingresó usuario")
	}
	if req.Config == 0 {
		return out, errors.Errorf("no se ingresó config de minuta")
	}
	if req.Esquema == 0 {
		return out, errors.Errorf("no se ingresó esquema de minuta")
	}

	// Genero
	if out.ID, err = uuid.NewV1(); err != nil {
		return out, err
	}

	out.Comitente = req.Comitente
	out.Empresa = req.Empresa
	out.Detalle = "Amortización bienes de uso"
	out.Fecha = req.Fecha
	out.Config = req.Config
	out.Esquema = req.Esquema
	out.Usuario = new(string)
	*out.Usuario = req.Usuario

	return
}

// ctasRtdo dice que cuenta de resultado utilizar para cada cuenta de valor original.
// Si el map tiene key = 0, para todas las partidas usa la misma cuenta de resultado.
func generarPartidasAmortizacion(ss []Saldo, avances map[string]dec.D4, ctaRtdo int) (out []minuta.Partida, err error) {
	if ctaRtdo == 0 {
		return out, errors.Errorf("no se ingresó cuenta de resultado")
	}

	sums := map[int]dec.D2{}

	for _, v := range ss {
		qua, val, err := calcularAmortizacion(v, avances)
		if err != nil {
			return out, errors.Wrap(err, "calculando amortización")
		}
		if qua == 0 && val == 0 {
			continue
		}

		// Partida patrimonial
		p := minuta.Partida{}
		p.Cuenta = v.CuentaAmorAcum
		if val > 0 {
			p.Haber = val
		} else {
			p.Debe = -val
		}
		p.Cantidad = -qua
		p.UM = v.UM
		p.Unicidad = new(uuid.UUID)
		*p.Unicidad = v.Unicidad
		p.Detalle = v.Detalle

		out = append(out, p)

		sums[ctaRtdo] += val
	}

	for k, v := range sums {
		// Partida resultado
		p := minuta.Partida{}
		p.Cuenta = k
		if v > 0 {
			p.Debe = v
		} else {
			p.Haber = -v
		}

		out = append(out, p)
	}

	return
}

// En avances se ingresa algo como {"Años": 1}
func calcularAmortizacion(s Saldo, avances map[string]dec.D4) (q dec.D4, v dec.D2, err error) {

	// Se amortiza?
	añosPendientes := s.VidaUtil + s.VidaUtilAcum
	if añosPendientes <= 0 {
		return 0, 0, nil
	}
	if s.UM == "" {
		return q, v, errors.Errorf("el bien %v %v no tiene definida unidad de medida de amortización",
			s.Detalle, s.Unicidad)
	}

	// Monto amortizable
	montoPendiente := s.ValorOrigen + s.ValorAmortAcum
	if montoPendiente == 0 {
		return 0, 0, nil
	}
	var ok bool
	q, ok = avances[s.UM]
	if !ok {
		return q, v, errors.Errorf("el bien %v %v tiene definido como unidad de medida de amortización %v, pero la misma no fue ingresada para calcular amortización",
			s.Detalle, s.Unicidad, s.UM)
	}
	if q == 0 {
		// Se pidió que esta unidad de medida no avance
		return
	}

	v = dec.NewD2(montoPendiente.Float() / añosPendientes.Float() * q.Float())

	return
}
