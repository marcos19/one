package bsuso

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/minuta"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

type Handler struct {
	conn                 *pgxpool.Pool
	cuentasHandler       cuentas.ReaderOne
	cuentasFuncionGetter cuentas.PorFuncionGetter
	minutaHandler        *minuta.Handler
}

func NewHandler(conn *pgxpool.Pool, ctas cuentas.ReaderOne, funcs cuentas.PorFuncionGetter, m *minuta.Handler) *Handler {
	return &Handler{conn, ctas, funcs, m}
}

func (h *Handler) TraerSaldos(ctx context.Context, req TraerSaldosReq) (out []Saldo, err error) {

	out, err = TraerSaldos(ctx, h.conn, req, h.cuentasHandler, h.cuentasFuncionGetter)
	if err != nil {
		return out, errors.Wrap(err, "trayendo saldos de bienes de uso")
	}
	return
}

type AmortizarReq struct {
	compReq
	Avances map[string]dec.D4
	// CtasRtdo map[int]int
	CtaRtdo   int `json:",string"`
	CtaRecpam int `json:",string"`
}

func (h *Handler) PrevisualizarAmortizaciones(ctx context.Context, req AmortizarReq) (out minuta.Minuta, err error) {

	out, err = calcularAmortizaciones(ctx, h.conn, h.cuentasHandler, h.cuentasFuncionGetter, req)
	if err != nil {
		return out, errors.Wrap(err, "calculando amortizaciones")
	}
	return
}

func (h *Handler) ConfirmarAmortizaciones(ctx context.Context, req AmortizarReq) (id uuid.UUID, err error) {

	m, err := calcularAmortizaciones(ctx, h.conn, h.cuentasHandler, h.cuentasFuncionGetter, req)
	if err != nil {
		return id, errors.Wrap(err, "calculando amortizaciones")
	}

	// Creo op
	id, err = h.minutaHandler.Create(ctx, &m)
	if err != nil {
		return id, errors.Wrap(err, "registrando amortizaciones")
	}

	return
}

type AjustarInflacionReq struct {
	Comitente int
	Empresa   int `json:",string"`
	Config    int `json:",string"`
	Esquema   int `json:",string"`
	Usuario   string
	CtaRecpam int `json:",string"`
	Desde     fecha.Mes
	Hasta     fecha.Mes
	Indice    string
}

func (h *Handler) ConfirmarAjustePorInflacion(ctx context.Context, req AjustarInflacionReq) (id uuid.UUID, err error) {
	aj, err := h.PrevisualizarAjustePorInflacion(ctx, req)
	if err != nil {
		return id, errors.Wrap(err, "calculando")
	}
	id, err = h.minutaHandler.Create(ctx, &aj)
	if err != nil {
		return id, errors.Wrap(err, "insertando minuta")
	}

	return
}
func (h *Handler) PrevisualizarAjustePorInflacion(ctx context.Context, req AjustarInflacionReq) (out minuta.Minuta, err error) {

	if req.Comitente == 0 {
		return out, deferror.ComitenteIndefinido()
	}
	if req.Empresa == 0 {
		return out, deferror.EmpresaIndefinida()
	}
	if !req.Desde.Valid() {
		return out, errors.Errorf("fecha inicio inválida")
	}
	if !req.Hasta.Valid() {
		return out, errors.Errorf("fecha hasta inválida")
	}

	ids := map[uuid.UUID]struct{}{}

	// Traigo saldos de inicio
	reqSS := TraerSaldosReq{
		Comitente: req.Comitente,
		Empresa:   req.Empresa,
		Fecha:     req.Desde.UltimoDia(),
	}
	ss, err := TraerSaldos(ctx, h.conn, reqSS, h.cuentasHandler, h.cuentasFuncionGetter)
	if err != nil {
		return out, errors.Wrapf(err, "trayendo saldos de inicio (al %v)", reqSS.Fecha)
	}
	for _, v := range ss {
		ids[v.Unicidad] = struct{}{}
	}

	// Traigo IDs de cuentas contables valor origen
	ctas, err := h.cuentasFuncionGetter.PorFuncionContable(ctx, req.Comitente, cuentas.BsUsoValorOrigen)
	if err != nil {
		return out, errors.Wrap(err, "trayendo cuentas valor origen")
	}
	if len(ctas) == 0 {
		return out, errors.Errorf("no hay ninguna cuenta contable definida con función '%v'", cuentas.BsUsoValorOrigen)
	}
	// Traigo IDs de cuentas contables amort acum
	ctasAcum, err := h.cuentasFuncionGetter.PorFuncionContable(ctx, req.Comitente, cuentas.BsUsoAmortAcum)
	if err != nil {
		return out, errors.Wrap(err, "trayendo cuentas valor origen")
	}
	if len(ctas) == 0 {
		return out, errors.Errorf("no hay ninguna cuenta contable definida con función '%v'", cuentas.BsUsoAmortAcum)
	}
	cc := []int{}
	for _, v := range ctas {
		cc = append(cc, v.ID)
	}
	for _, v := range ctasAcum {
		cc = append(cc, v.ID)
	}

	// Traigo movimientos del ejercicio
	reqMovs := movsReq{
		Comitente: req.Comitente,
		Empresa:   req.Empresa,
		Desde:     req.Desde.SumarMeses(1).PrimerDia(),
		Hasta:     req.Hasta.UltimoDia(),
		Cuentas:   cc,
	}
	movs, err := queryMovs(ctx, h.conn, reqMovs)
	if err != nil {
		return out, errors.Wrapf(err, "buscando movimientos de bienes de uso entre %v y %v", req.Desde, req.Hasta)
	}
	for _, v := range movs {
		ids[v.unicidad] = struct{}{}
	}

	// Traigo ínidices
	indices, err := traerIndices(ctx, h.conn, req.Comitente, req.Indice, req.Desde, req.Hasta)
	if err != nil {
		return out, errors.Wrap(err, "buscando índices")
	}

	// Traigo datos de las unicidades
	idsSlice := []uuid.UUID{}
	for k := range ids {
		idsSlice = append(idsSlice, k)
	}
	uu, err := queryUnicidades(ctx, h.conn, idsSlice, req.Comitente)
	if err != nil {
		return out, errors.Wrap(err, "buscando unicidades")
	}

	// Genero el comprobante
	compReq := compReq{
		Comitente: req.Comitente,
		Empresa:   req.Empresa,
		Fecha:     req.Hasta.UltimoDia(),
		Config:    req.Config,
		Esquema:   req.Esquema,
		Usuario:   req.Usuario,
	}
	out, err = generarMinuta(compReq)
	if err != nil {
		return out, errors.Wrap(err, "generando minuta")
	}
	out.Detalle = "Ajuste por inflación bienes de uso"

	// Genero partidas
	out.Partidas, err = generarPartidasAjusteInflacion(
		req.Desde,
		req.Hasta,
		ss,
		movs,
		req.CtaRecpam,
		indices,
		uu,
	)
	if err != nil {
		return out, errors.Wrap(err, "calculando amortizaciones")
	}

	return
}

func (h *Handler) Rubros(ctx context.Context, comitente int) (out []Rubro, err error) {
	out, err = Rubros(ctx, h.conn, comitente)
	if err != nil {
		return out, errors.Wrap(err, "buscando rubros de bienes de uso")
	}
	return
}
