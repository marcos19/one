package bsuso

import (
	"os"
	"testing"

	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestParse(t *testing.T) {
	f, err := os.Open("testdata/bsuso.xlsx")
	require.Nil(t, err)

	bb, err := Parse(f, "Hoja1")
	require.Nil(t, err)

	b := bb[151]
	require.Len(t, bb, 179)
	assert.Equal(t, uuid.FromStringOrNil("54b5ce1a-802d-4475-98d7-9777fa78cd10"), b.ID)
	assert.Equal(t, 1, b.Empresa)
	assert.Equal(t, fecha.Fecha(20190318), b.FechaAlta)
	assert.Equal(t, "", b.Proveedor)
	assert.Equal(t, "4 Cámaras IP Movil C/Infr IC09W", b.Detalle)
	assert.Equal(t, 1, b.Rubro)
	assert.Equal(t, "Máquinas y equipos", b.RubroNombre)
	assert.Equal(t, "Años", b.UMDepreciacion)
	assert.Equal(t, dec.NewD4(5), b.VidaUtil)
	assert.Equal(t, dec.NewD4(-3), b.VidaUtilAmortizada)
	assert.Equal(t, dec.NewD2(47895.09), b.Valor)
	assert.Equal(t, dec.NewD2(-28737.06), b.AmortAcum)

	assert.NotEqual(t, uuid.Nil, bb[178].ID)
}
