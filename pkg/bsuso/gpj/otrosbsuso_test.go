package gpj

import (
	"testing"

	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/stretchr/testify/assert"
)

func TestOtrosBsUso(t *testing.T) {

	o := OtroBien{
		ExterorizadoLey26476: true,
		AmortAceleradaLey:    4,
		Descripcion:          "Un bien de la puta madre",
		Amort:                dec.NewD2(500.35),
		AmortAcelerada:       dec.NewD2(700.99),
		ValuacionGcias:       dec.NewD2(8333.33),
		FechaIngreso:         fecha.NewMesMust(2022, 2),
	}

	str := o.generarRenglon()
	assert.Len(t, str, 118)
}

//101/02/2022Un bien de la puta madre                                   400000000500,3500000000700,9900000008333,33
