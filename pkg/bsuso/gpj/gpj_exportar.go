package gpj

import (
	"fmt"

	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/pkg/errors"
)

func llenarTexto(str string, largo int) string {

	// Si el texto es más largo, lo recorto
	if len(str) > largo {
		return str[:largo]
	}

	// Si el más corto, relleno
	cantidadAgregar := largo - len(str)
	relleno := ""
	for i := 1; i <= cantidadAgregar; i++ {
		relleno += " "
	}
	return str + relleno
}

func llenarBool(val bool) string {
	if val {
		return "1"
	}
	return "0"
}

func llenarMes(m fecha.Mes) string {
	if !m.Valid() {
		panic(errors.Errorf("la fecha %v no es válida", m))
	}
	return m.PrimerDia().String()
}

func llenarMesYYYYMM(m fecha.Mes) string {
	if !m.Valid() {
		panic(errors.Errorf("la fecha %v no es válida", m))
	}
	return m.PrimerDia().Time().Format("200601")
}
func llenarD2(v dec.D2, largo int) string {
	return v.ExportarParaCSV(2, "", ",", largo, "0", true)
}

func llenarInt(m int, largo int) string {
	str := fmt.Sprint(m)

	// Si el texto es más largo, lo recorto
	if len(str) > largo {
		panic(errors.Errorf("texto '%v' era más largo de lo permitido (%v caracteres)", m, largo))
	}

	// Si el más corto, relleno
	cantidadAgregar := largo - len(str)
	relleno := ""
	for i := 1; i <= cantidadAgregar; i++ {
		relleno += " "
	}
	return relleno + str
}
func recortarRellenar(textoAModificar string, largo int, llenarConCaracter string, alineadoDerecha bool) (rtdo string) {
	chars := []rune(textoAModificar)

	// Si el texto es más largo, lo recorto
	if len(chars) > largo {
		return string(chars[:largo])
	}

	llenarCon := []rune(llenarConCaracter)

	// Si el más corto, relleno
	cantidadAgregar := largo - len(chars)
	relleno := []rune{}
	for i := 1; i <= cantidadAgregar; i++ {
		relleno = append(relleno, llenarCon...)
	}

	if alineadoDerecha {
		return string(append(relleno, chars...))
	}
	if !alineadoDerecha {
		return string(append(chars, relleno...))
	}
	return
}

// El largo lo mide en bytes, no en caracteres. á ocupa dos bytes
func recortarRellenarLargoBytes(texto string, largo int, llenarCon string, alineadoDerecha bool) (rtdo string) {

	// Si el texto es más largo, lo recorto
	if len(texto) > largo {
		return texto[:largo]
	}

	// Si el más corto, relleno
	cantidadAgregar := largo - len(texto)
	relleno := ""
	for i := 1; i <= cantidadAgregar; i++ {
		relleno += llenarCon
	}

	if alineadoDerecha {
		return relleno + texto
	}
	if !alineadoDerecha {
		return texto + relleno
	}
	return
}
