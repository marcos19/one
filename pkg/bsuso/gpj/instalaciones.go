package gpj

import (
	"strings"

	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
)

type Instalacion struct {
	ExterorizadoLey26476 bool
	FechaIngreso         fecha.Mes
	Descripcion          string
	AmortAceleradaLey    int
	Amort                dec.D2
	AmortAcelerada       dec.D2
	ValuacionGcias       dec.D2
}

func (o Instalacion) generarRenglon() string {

	rr := []string{
		llenarBool(o.ExterorizadoLey26476),
		llenarMes(o.FechaIngreso),
		llenarTexto(o.Descripcion, 71-12+1),
		llenarInt(o.AmortAceleradaLey, 73-72+1),
		llenarD2(o.Amort, 88-74+1),
		llenarD2(o.AmortAcelerada, 103-89+1),
		llenarD2(o.ValuacionGcias, 118-104+1),
	}

	return strings.Join(rr, "")
}
