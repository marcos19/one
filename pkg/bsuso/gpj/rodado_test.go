package gpj

import (
	"fmt"
	"testing"

	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/stretchr/testify/assert"
)

func TestVehiculo(t *testing.T) {

	o := Vehiculo{
		ExterorizadoLey26476: true,
		FechaIngreso:         fecha.NewMesMust(2022, 2),
		AmortAceleradaLey:    4,
		Patente:              "AB156SA",
		Amort:                dec.NewD2(500.35),
		AmortAcelerada:       dec.NewD2(700.99),
		ValuacionGcias:       dec.NewD2(8333.33),
		Fabrica:              98,
		PorcParticipacion:    dec.NewD2(33.33),
		Marca:                "Ford",
		Modelo:               "Falcon",
		AñoFabricacion:       1976,
	}
	str := o.generarRenglon()
	fmt.Println(str)
	assert.Len(t, str, 172)
}

//
//101/02/2022AB156SA        033,33 4000000000500,35000000000700,99000000008333,33Ford                                    Falcon                                             981976
