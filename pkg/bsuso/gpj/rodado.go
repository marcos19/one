package gpj

import (
	"strings"

	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
)

type Vehiculo struct {
	ExterorizadoLey26476 bool
	FechaIngreso         fecha.Mes
	Patente              string
	PorcParticipacion    dec.D2
	AmortAceleradaLey    int
	Amort                dec.D2
	AmortAcelerada       dec.D2
	ValuacionGcias       dec.D2
	Marca                string
	Modelo               string
	Fabrica              int
	AñoFabricacion       int
}

func (o Vehiculo) generarRenglon() string {

	rr := []string{
		llenarBool(o.ExterorizadoLey26476),
		llenarMesYYYYMM(o.FechaIngreso),
		llenarTexto(o.Patente, 22-8+1),
		llenarD2(o.PorcParticipacion, 28-23+1),
		llenarInt(o.AmortAceleradaLey, 30-29+1),
		llenarD2(o.Amort, 45-31+1),
		llenarD2(o.AmortAcelerada, 60-46+1),
		llenarD2(o.ValuacionGcias, 75-61+1),
		llenarTexto(o.Marca, 115-76+1),
		llenarTexto(o.Modelo, 165-116+1),
		llenarInt(o.Fabrica, 168-166+1),
		llenarInt(o.AñoFabricacion, 172-169+1),
	}

	return strings.Join(rr, "")
}
