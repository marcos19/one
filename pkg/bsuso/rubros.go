package bsuso

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

type Rubro struct {
	Comitente int
	ID        int
	Nombre    string
}

func Rubros(ctx context.Context, conn *pgxpool.Pool, comitente int) (out []Rubro, err error) {
	q := `SELECT id, nombre FROM rubros_bsuso WHERE comitente = $1 ORDER BY nombre;`

	rows, err := conn.Query(ctx, q, comitente)
	if err != nil {
		return out, deferror.DB(err, "buscando rubros bs de uso")
	}
	defer rows.Close()

	for rows.Next() {
		r := Rubro{}
		err = rows.Scan(&r.ID, &r.Nombre)
		if err != nil {
			return out, errors.Wrapf(err, "scanning")
		}
		out = append(out, r)
	}

	return
}
