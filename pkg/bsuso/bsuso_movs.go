package bsuso

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

type movsReq struct {
	Comitente int
	Empresa   int
	Desde     fecha.Fecha
	Hasta     fecha.Fecha
	Cuentas   tipos.GrupoInts
}

// Devuelve los movimientos del ejercicio para las cuentas ingresadas
func queryMovs(ctx context.Context, conn *pgxpool.Pool, req movsReq) (out []mov, err error) {
	// Valido
	if req.Comitente == 0 {
		return out, deferror.ComitenteIndefinido()
	}
	if req.Desde == 0 {
		return out, deferror.Validation("No se ingresó fecha desde")
	}
	if req.Hasta == 0 {
		return out, deferror.Validation("No se ingresó fecha hasta")
	}
	if req.Empresa == 0 {
		return out, deferror.EmpresaIndefinida()
	}
	if len(req.Cuentas) == 0 {
		return out, errors.Errorf("no se ingresaron cuentas contables a consultar")
	}

	// Todo ok, busco
	ww := []string{
		"comitente=$1",
		fmt.Sprintf("fecha_contable >= '%v'", req.Desde.JSONString()),
		fmt.Sprintf("fecha_contable <= '%v'", req.Hasta.JSONString()),
		fmt.Sprintf("cuenta IN %v", req.Cuentas.ParaClausulaIn()),
		"unicidad IS NOT NULL",
		"empresa=$2",
	}
	pp := []any{req.Comitente, req.Empresa}

	query := fmt.Sprintf(`SELECT fecha_contable, unicidad, cuenta, SUM(monto), um
		FROM partidas
		WHERE %v
		GROUP BY fecha_contable, unicidad, cuenta, um
		HAVING SUM(monto) <> 0
	;`, strings.Join(ww, " AND "))

	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		s := mov{}
		monto := new(int)
		um := new(string)
		err = rows.Scan(&s.fecha, &s.unicidad, &s.cuenta, &monto, &um)
		if err != nil {
			return out, errors.Wrap(err, "scanning")
		}
		if monto != nil {
			s.monto = dec.D2(float64(*monto))
		}
		if um != nil {
			s.um = *um
		}
		out = append(out, s)
	}

	return
}
