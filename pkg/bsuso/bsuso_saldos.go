package bsuso

import (
	"context"
	"fmt"
	"sort"
	"strings"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/davecgh/go-spew/spew"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

type TraerSaldosReq struct {
	Comitente int
	Empresa   int `json:",string"`
	Fecha     fecha.Fecha
}

func TraerSaldos(ctx context.Context, conn *pgxpool.Pool, req TraerSaldosReq, ctasH cuentas.ReaderOne, funcs cuentas.PorFuncionGetter) (out []Saldo, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.ComitenteIndefinido()
	}
	if req.Fecha == 0 {
		return out, deferror.Validation("no se ingresó fecha de análisis")
	}
	if !req.Fecha.IsValid() {
		return out, deferror.Validationf("fecha '%v' inválida", int(req.Fecha))
	}

	// Traigo IDs de cuentas contables valor origen
	voIDs, err := funcs.PorFuncionContable(ctx, req.Comitente, cuentas.BsUsoValorOrigen)
	if err != nil {
		return out, errors.Wrap(err, "trayendo cuentas valor origen")
	}
	if len(voIDs) == 0 {
		return out, errors.Errorf("no hay ninguna cuenta contable definida con función '%v'", cuentas.BsUsoValorOrigen)
	}

	// Traigo IDs de cuentas contables amort acum
	acumIDs, err := funcs.PorFuncionContable(ctx, req.Comitente, cuentas.BsUsoAmortAcum)
	if err != nil {
		return out, errors.Wrap(err, "trayendo cuentas amortización acumulada")
	}
	if len(acumIDs) == 0 {
		return out, errors.Errorf("no hay ninguna cuenta contable definida con función '%v'", cuentas.BsUsoValorOrigen)
	}

	cc := []int{}
	for _, v := range voIDs {
		if v.RubroBsUso == nil {
			return out, errors.Errorf("cuenta '%v' no define a que rubro pertence", v.Nombre)
		}
		cc = append(cc, v.ID)
	}
	for _, v := range acumIDs {
		if v.RubroBsUso == nil {
			return out, errors.Errorf("cuenta '%v' no define a que rubro pertence", v.Nombre)
		}
		cc = append(cc, v.ID)
	}

	// Traigo saldos
	ss, err := querySaldos(ctx, conn, saldoReq{
		Comitente: req.Comitente,
		Empresa:   req.Empresa,
		Fecha:     req.Fecha,
		Cuentas:   cc,
	})
	spew.Dump("SALDOS ENCONTRADOS", ss)
	if err != nil {
		return out, errors.Wrap(err, "trayendo saldos")
	}

	ids := []uuid.UUID{}
	for k := range ss {
		ids = append(ids, k)
	}

	// Traigo unicidades
	uu, err := queryUnicidades(ctx, conn, ids, req.Comitente)
	if err != nil {
		return out, errors.Wrap(err, "quering unicidades")
	}

	// Junto saldos y unicidades
	return juntarUnicidadesYSaldos(uu, ss, voIDs, acumIDs)

}

func juntarUnicidadesYSaldos(uu map[uuid.UUID]unic, ss map[uuid.UUID][]saldo, cuentasVO, cuentasAmortAcum []cuentas.Cuenta) (out []Saldo, err error) {

	ctaRubro := map[int]int{}
	for _, v := range cuentasVO {
		ctaRubro[v.ID] = *v.RubroBsUso
	}
	for _, v := range cuentasAmortAcum {
		ctaRubro[v.ID] = *v.RubroBsUso
	}
	for k, u := range uu {
		saldos := ss[k]
		item := Saldo{unic: u}
		item.Unicidad = k
		for _, s := range saldos {
			switch {

			case es(s.cuenta, cuentasVO):
				item.CuentaValorOrigen = s.cuenta
				item.ValorOrigen += s.monto
				item.VidaUtil += s.cantidad
				item.UM = s.um

			case es(s.cuenta, cuentasAmortAcum):
				item.CuentaAmorAcum = s.cuenta
				item.ValorAmortAcum += s.monto
				item.VidaUtilAcum += s.cantidad

			default:
				return out, errors.Errorf("cuenta %v no entró como VO ni Acumulada", s.cuenta)
			}
		}
		out = append(out, item)
	}

	for i := range out {
		// Si es la primera vez, no sabe a cual va
		if out[i].CuentaAmorAcum == 0 {
			for _, v := range cuentasAmortAcum {
				if *v.RubroBsUso == out[i].Rubro {
					out[i].CuentaAmorAcum = v.ID
					break
				}
			}
		}
		// if ctaRubro[s.cuenta] != item.Rubro {
		// 	return out, errors.Errorf("cuenta %v tiene definido rubro %v, pero hay movimientos de %v que está en rubro %v",
		// 		s.cuenta, ctaRubro[s.cuenta], item.Detalle, item.Rubro,
		// 	)
		// }
	}

	sort.Slice(out, func(i, j int) bool {
		if out[i].FechaAlta == out[j].FechaAlta {
			return out[i].Detalle < out[j].Detalle
		}
		return out[i].FechaAlta < out[j].FechaAlta
	})
	return
}

// corrobora que el ID de la cuenta pertenezca al grupo
func es(id int, ii []cuentas.Cuenta) bool {
	for _, v := range ii {
		if v.ID == id {
			return true
		}
	}
	return false
}

type Saldo struct {
	unic

	CuentaValorOrigen int `json:",string"`
	ValorOrigen       dec.D2
	VidaUtil          dec.D4

	CuentaAmorAcum int `json:",string"`
	ValorAmortAcum dec.D2
	VidaUtilAcum   dec.D4
}

type unic struct {
	Unicidad         uuid.UUID
	Detalle          string
	FechaAlta        fecha.Fecha
	FechaInicioAmort *fecha.Fecha
	UM               string
	Rubro            int
}

type saldo struct {
	cuenta   int
	monto    dec.D2
	um       string
	cantidad dec.D4
}

type saldoReq struct {
	Comitente int
	Empresa   int
	Fecha     fecha.Fecha
	Cuentas   tipos.GrupoInts
}

// Devuelve los saldos para las unicidades ingresadas
func querySaldos(ctx context.Context, conn *pgxpool.Pool, req saldoReq) (out map[uuid.UUID][]saldo, err error) {
	// Valido
	if req.Comitente == 0 {
		return out, deferror.ComitenteIndefinido()
	}
	if req.Empresa == 0 {
		return out, deferror.EmpresaIndefinida()
	}
	if req.Fecha == 0 {
		return out, deferror.Validation("No se ingresó fecha")
	}
	if len(req.Cuentas) == 0 {
		return out, errors.Errorf("no se ingresaron cuentas contables a consultar")
	}

	out = map[uuid.UUID][]saldo{}

	spew.Dump(req)

	// Todo ok, busco
	ww := []string{
		"comitente=$1",
		fmt.Sprintf("fecha_contable <= '%v'", req.Fecha.JSONString()),
		fmt.Sprintf("cuenta IN %v", req.Cuentas.ParaClausulaIn()),
		"unicidad IS NOT NULL",
		"empresa=$2",
	}
	pp := []any{req.Comitente, req.Empresa}

	query := fmt.Sprintf(`SELECT unicidad, cuenta, SUM(monto), um, SUM(cantidad)
		FROM partidas
		WHERE %v
		GROUP BY unicidad, cuenta, um
		HAVING SUM(monto) <> 0
	;`, strings.Join(ww, " AND "))
	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		s := saldo{}
		id := uuid.UUID{}
		monto := new(int)
		cantidad := new(int)
		um := new(string)
		err = rows.Scan(&id, &s.cuenta, &monto, &um, &cantidad)
		if err != nil {
			return out, errors.Wrap(err, "scanning")
		}
		if monto != nil {
			s.monto = dec.D2(float64(*monto))
		}
		if cantidad != nil {
			s.cantidad = dec.D4(float64(*cantidad))
		}
		if um != nil {
			s.um = *um
		}
		anterior := out[id]
		anterior = append(anterior, s)
		out[id] = anterior
	}

	return
}

const (
	fchInicioAmort = "Fecha inicio amortización"
	fchAlta        = "Fecha alta"
	rubro          = "Rubro"
)

// Busca las unicidades con los ids ingresados
func queryUnicidades(ctx context.Context, conn *pgxpool.Pool, ids []uuid.UUID, comitente int) (out map[uuid.UUID]unic, err error) {
	out = map[uuid.UUID]unic{}

	query := fmt.Sprintf(`
		SELECT id, atts_indexados, atts FROM unicidades WHERE comitente=$1 AND id IN (%v);`, arrayUUID(ids))
	rows, err := conn.Query(ctx, query, comitente)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		id := uuid.UUID{}
		atts := tipos.JSON{}
		attsIndexados := tipos.JSON{}
		err = rows.Scan(&id, &attsIndexados, &atts)
		if err != nil {
			return out, deferror.DB(err, "scanning")
		}

		u := unic{Unicidad: id}

		// Pego nombre
		det, ok := atts["Detalle"]
		if !ok {
			det, ok = attsIndexados["Detalle"]
			if !ok {
				return out, errors.Errorf("unicidad ID %v no tenía campo 'Detalle'", id)
			}
		}
		u.Detalle = det.(string)

		{ // Rubro
			fld := rubro
			fchAny, ok := atts[fld]
			if !ok {
				return out, errors.Errorf("unicidad ID %v no tenía campo '%v'", id, fld)
			}
			u.Rubro, ok = fchAny.(int)
			if !ok {
				fl, ok := fchAny.(float64)
				if !ok {
					return out, errors.Errorf("unicidad ID %v tenía campo '%v' inválido", id, fld)
				}
				u.Rubro = int(fl)
			}
		}

		{ // Fecha de alta
			fld := fchAlta
			fchAny, ok := atts[fld]
			if !ok {
				return out, errors.Errorf("unicidad ID %v no tenía campo '%v'", id, fld)
			}
			fchStr, ok := fchAny.(string)
			if !ok {
				return out, errors.Errorf("unicidad ID %v tenía campo '%v' inválido", id, fld)
			}
			u.FechaAlta, err = fecha.NewFecha(fchStr)
			if err != nil {
				return out, errors.Wrapf(err, "parseando campo '%v' en unicidad ID %v", fld, id)
			}
		}

		{ // Fecha de inicio amortización
			fld := fchInicioAmort
			fchAny, ok := atts[fld]
			if !ok {
				return out, errors.Errorf("unicidad ID %v no tenía campo '%v'", id, fld)
			}
			fchStr, ok := fchAny.(string)
			if !ok {
				spew.Dump(atts)
				return out, errors.Errorf("unicidad ID %v tenía campo '%v' inválido", id, fld)
			}
			if fchStr != "" {
				u.FechaInicioAmort = new(fecha.Fecha)
				*u.FechaInicioAmort, err = fecha.NewFecha(fchStr)
				if err != nil {
					return out, errors.Wrapf(err, "parseando campo '%v' en unicidad ID %v", fld, id)
				}
			}
		}
		out[id] = u
	}

	return
}

func arrayUUID(ids []uuid.UUID) string {
	idsStr := []string{}
	for _, v := range ids {
		idsStr = append(idsStr, fmt.Sprintf("'%v'", v.String()))
	}
	return strings.Join(idsStr, ", ")
}
