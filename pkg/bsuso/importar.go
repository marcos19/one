package bsuso

import (
	"time"

	"bitbucket.org/marcos19/one/pkg/minuta"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"bitbucket.org/marcos19/one/pkg/unicidades"
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
)

func CrearUnicidades(bb []BienImportado, comitente int, def int) (out []unicidades.Unicidad, err error) {
	for _, b := range bb {
		u := crearUnicidad(b, comitente, def)
		out = append(out, u)
	}
	return
}

func crearUnicidad(b BienImportado, comitente int, def int) (u unicidades.Unicidad) {

	u.ID = b.ID
	u.Comitente = comitente
	u.Definicion = def
	u.AttsIndexados = tipos.JSON{
		"Detalle": b.Detalle,
	}
	u.Atts = tipos.JSON{
		"Fecha alta":                b.FechaAlta,
		"Fecha inicio amortización": b.FechaInicioAmort,
		"UM":                        b.UMDepreciacion,
		"Vida útil":                 b.VidaUtil,
		"Rubro":                     b.Rubro,
	}
	if b.Proveedor != "" {
		u.Atts["Proveedor"] = b.Proveedor
	}
	u.CreatedAt = new(time.Time)
	*u.CreatedAt = time.Now()

	return
}

// Crea las partidas de la minuta en base a los bienes importados.
// Las partidas son agregadas a la minuta.
func CrearPartidas(bb []BienImportado, cuentaCierre int) (out []minuta.Partida, err error) {

	sum := dec.D2(0)
	for i, v := range bb {

		// Valor origen
		val := partidaValor(v)
		sum += val.Debe - val.Haber
		out = append(out, val)

		// Amort acum
		if v.CuentaAmortAcum == 0 && v.AmortAcum == 0 {
			continue
		}
		if v.CuentaAmortAcum == 0 && v.AmortAcum != 0 {
			return out, errors.Errorf("no se ingresó cuenta amort acum renglón %v", i)
		}
		ac := partidaAmortAcum(v)
		sum += ac.Debe - ac.Haber
		out = append(out, ac)
	}

	cierre := partidaCierre(-sum, cuentaCierre)
	out = append(out, cierre)
	return
}

func partidaValor(b BienImportado) (out minuta.Partida) {
	out.Unicidad = new(uuid.UUID)
	*out.Unicidad = b.ID
	out.Cuenta = b.Cuenta
	if b.Valor > 0 {
		out.Debe = b.Valor
	} else {
		out.Haber = -b.Valor
	}
	out.Detalle = b.Detalle

	if b.Centro != 0 {
		out.Centro = new(int)
		*out.Centro = b.Centro
	}

	out.Cantidad = b.VidaUtil
	out.UM = b.UMDepreciacion

	return
}

func partidaAmortAcum(b BienImportado) (out minuta.Partida) {
	out.Unicidad = new(uuid.UUID)
	*out.Unicidad = b.ID
	out.Cuenta = b.CuentaAmortAcum
	if b.AmortAcum > 0 {
		out.Debe = b.AmortAcum
	} else {
		out.Haber = -b.AmortAcum
	}
	out.Detalle = b.Detalle

	if b.Centro != 0 {
		out.Centro = new(int)
		*out.Centro = b.Centro
	}

	out.Cantidad = b.VidaUtilAmortizada
	out.UM = b.UMDepreciacion

	return
}

func partidaCierre(montoCierre dec.D2, cuentaCierre int) (out minuta.Partida) {
	out.Cuenta = cuentaCierre
	if montoCierre > 0 {
		out.Debe = montoCierre
	} else {
		out.Haber = -montoCierre
	}

	return
}
