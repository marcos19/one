package cuentasgrupos

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/jackc/pgx/v4/pgxpool"
)

func readOne(ctx context.Context, conn *pgxpool.Pool, req ReadOneReq) (out Grupo, err error) {
	query := `SELECT id, comitente, nombre, detalle, tipo, cuentas, created_at, updated_at
	FROM cuentas_grupos
	WHERE comitente=$1 AND id=$2`

	err = conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(
		&out.ID, &out.Comitente, &out.Nombre, &out.Detalle, &out.Tipo, &out.Cuentas, &out.CreatedAt, &out.UpdatedAt)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}
	return
}

func readMany(ctx context.Context, conn *pgxpool.Pool, req ReadManyReq) (out []Grupo, err error) {
	out = []Grupo{}

	if req.Comitente == 0 {
		return out, deferror.ComitenteIndefinido()
	}

	ww := []string{"comitente = $1"}
	pp := []interface{}{req.Comitente}
	if req.Tipo != "" {
		ww = append(ww, "tipo = $2")
		pp = append(pp, req.Tipo)
	}
	where := fmt.Sprintf("WHERE %v", strings.Join(ww, " AND "))
	query := fmt.Sprintf(`SELECT id, comitente, nombre, detalle, tipo, cuentas, created_at, updated_at
	FROM cuentas_grupos
	%v
	ORDER BY nombre`, where)

	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		v := Grupo{}
		err = rows.Scan(
			&v.ID, &v.Comitente, &v.Nombre, &v.Detalle, &v.Tipo, &v.Cuentas, &v.CreatedAt, &v.UpdatedAt)
		if err != nil {
			return out, deferror.DB(err, "querying/scanning")
		}
		out = append(out, v)
	}
	return
}

func create(ctx context.Context, conn *pgxpool.Pool, req Grupo) (err error) {
	query := `INSERT INTO cuentas_grupos (comitente, nombre, detalle, tipo, cuentas, created_at) 
	VALUES ($1,$2,$3,$4,$5,$6)`

	_, err = conn.Exec(ctx, query, req.Comitente, req.Nombre, req.Detalle, req.Tipo, req.Cuentas, time.Now())
	if err != nil {
		return deferror.DB(err, "inserting")
	}
	return
}

func update(ctx context.Context, conn *pgxpool.Pool, req Grupo) (err error) {
	query := `UPDATE cuentas_grupos 
		SET nombre=$3, detalle=$4, tipo=$5, cuentas=$6, updated_at=$7
		WHERE comitente=$1 AND id=$2`

	_, err = conn.Exec(ctx, query, req.Comitente, req.ID, req.Nombre, req.Detalle, req.Tipo, req.Cuentas, time.Now())
	if err != nil {
		return deferror.DB(err, "updating")
	}
	return
}

func delete(ctx context.Context, conn *pgxpool.Pool, req ReadOneReq) (err error) {

	res, err := conn.Exec(ctx, "DELETE FROM cuentas_grupos WHERE comitente=$1 AND id=$2", req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "borrando")
	}
	if res.RowsAffected() == 0 {
		return errors.New("no se borró ningún registro")
	}
	return
}
