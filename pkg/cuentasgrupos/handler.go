package cuentasgrupos

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler es el encargado de recibir los http requests y emitir los responses.
type Handler struct {
	conn *pgxpool.Pool
}

type Getter interface {
	Cuentas(ctx context.Context, comitente int, gr tipos.GrupoInts) (tipos.GrupoInts, error)
}

type HandlerConfig struct {
	Conn *pgxpool.Pool
}

// NewHandler incializa un Handler.
func NewHandler(config HandlerConfig) (h *Handler, err error) {

	// Valido
	if config.Conn == nil {
		return h, errors.New("Conn no puede ser nil")
	}

	h = &Handler{
		conn: config.Conn,
	}

	return
}

type EventBus interface {
	CambióCuentasGrupos(crud string, grupo Grupo)
}

func (h *Handler) Create(ctx context.Context, req *Grupo) (err error) {

	// Valido
	if req == nil {
		return deferror.Validation("la cuenta era nil")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no ingresó el ID de comitente ")
	}
	if req.Nombre == "" {
		return deferror.Validation("Debe ingresar un nombre para la cuenta")
	}
	if len(req.Cuentas) == 0 {
		return deferror.Validation("Debe ingresar alguna cuenta")
	}

	// Persisto
	err = create(ctx, h.conn, *req)
	if err != nil {
		return deferror.DB(err, "persistiendo grupo")
	}

	// h.eventbus.CambióCuentasGrupos("creó", *req)

	return
}

func (h *Handler) Update(ctx context.Context, req Grupo) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no ingresó el ID de comitente ")
	}
	if req.Nombre == "" {
		return deferror.Validation("Debe ingresar un nombre para la cuenta")
	}
	if len(req.Cuentas) == 0 {
		return deferror.Validation("Debe ingresar alguna cuenta")
	}

	// Persisto
	err = update(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "modificando grupo")
	}

	// h.eventbus.CambióCuentasGrupos("modificó", req)

	return
}

type ReadOneReq struct {
	Comitente int
	ID        int `json:",string"`
}

func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (out Grupo, err error) {

	// Valido
	if req.ID == 0 {
		return out, deferror.Validation("no se ingresó ID a buscar")
	}
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó comitente a buscar")
	}

	// Busco
	out, err = readOne(ctx, h.conn, req)
	if err != nil {
		return out, errors.Wrap(err, "buscando por ID")
	}

	return
}

type ReadManyReq struct {
	Comitente int
	Tipo      string
}

func (h *Handler) ReadMany(ctx context.Context, req ReadManyReq) (out []Grupo, err error) {
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó comitente a buscar")
	}

	out, err = readMany(ctx, h.conn, req)
	if err != nil {
		return out, errors.Wrap(err, "buscando por ID")
	}

	return
}

func (h *Handler) Delete(ctx context.Context, req ReadOneReq) (err error) {

	// Valido
	if req.ID == 0 {
		return deferror.Validation("no se ingresó ID a buscar")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó comitente a buscar")
	}

	// Busco
	err = delete(ctx, h.conn, req)
	if err != nil {
		return errors.Wrap(err, "buscando por ID")
	}

	return
}

// Devuelve las cuentas que están comprendidas dentro de los grupos ingresados
func (h *Handler) Cuentas(ctx context.Context, comitente int, gg tipos.GrupoInts) (out tipos.GrupoInts, err error) {

	mm := map[int]struct{}{}
	for _, v := range gg {
		gr, err := h.ReadOne(ctx, ReadOneReq{
			Comitente: comitente,
			ID:        v,
		})
		if err != nil {
			return nil, errors.Wrap(err, "buscando grupo de cuentas")
		}

		for _, w := range gr.Cuentas {
			mm[w] = struct{}{}
		}
	}
	for k := range mm {
		out = append(out, k)
	}
	return
}
