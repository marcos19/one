package cuentasgrupos

import (
	"time"

	"bitbucket.org/marcos19/one/pkg/tipos"
)

type Grupo struct {
	ID        int `json:",string"`
	Comitente int
	Nombre    string
	Detalle   string
	Tipo      TipoGrupo
	Cuentas   tipos.GrupoInts
	CreatedAt *time.Time
	UpdatedAt *time.Time
}

type TipoGrupo string

const (
	TipoNinguno = ""
	TipoVentas  = "ventas"
)

func (g Grupo) TableName() string {
	return "cuentas_grupos"
}

/*
CREATE TABLE cuentas_grupos (
	id
		SERIAL PRIMARY KEY,
	comitente
		INT NOT NULL REFERENCES comitentes,
	nombre
		STRING,
	detalle
		STRING,
	cuentas
		INT[],
	created_at
		TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	updated_at
		TIMESTAMP WITH TIME ZONE
);
*/
