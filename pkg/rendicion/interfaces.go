package rendicion

import (
	"context"

	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
)

type DAL interface {
	Create(context.Context, pgx.Tx, *Rendicion) error
	ReadOne(context.Context, ReadOneReq) (Rendicion, error)
	Delete(context.Context, pgx.Tx, ReadOneReq) error
	Existencias(context.Context, ExistenciasReq) ([]Saldo, error)
}

type ReadOneReq struct {
	ID        uuid.UUID
	Comitente int
	Usuario   string
}

type ExistenciasReq struct {
	Comitente int
	Empresa   int `json:",string"`
	Caja      int `json:",string"`
}

type Saldo struct {
	Cuenta   int `json:",string"`
	Monto    dec.D2
	Cantidad int
}
type CreateArgs struct {
	// Es para migraciones. No modifica nada
	OmitirReproceso bool

	OmitirCorreccionFecha bool
}

type PDFReq struct {
	OpID      uuid.UUID
	Comitente int
}
