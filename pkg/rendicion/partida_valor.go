package rendicion

import (
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
)

// Partida es la partida que va en la struct fact
type Partida struct {
	Cuenta  int    `json:",string"`
	Alias   string // Cheque de tercero
	Detalle string `json:",omitempty"` // BANCO MACRO N 4354345

	// Si estoy referenciando una unicidad
	TipoUnicidad *int       `json:",string,omitempty"`
	UnicidadRef  *uuid.UUID `json:",omitempty"`

	// Es el campo PartidaAplicada de unicidad original.
	PartidaAplicada *uuid.UUID `json:"-"`

	Monto       dec.D2
	Vencimiento fecha.Fecha `json:",omitempty"`
}
