package handler

import "bitbucket.org/marcos19/one/pkg/ops"

type OpHandler interface {
	ops.Inserter
	ops.Deleter
	ops.ReaderMany
	ops.ReaderOne
	ops.UnicidadIDGetter
}
