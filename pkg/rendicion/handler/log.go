package handler

import (
	"fmt"

	"bitbucket.org/marcos19/one/pkg/oplog"
	"bitbucket.org/marcos19/one/pkg/ops"
)

// Cuando se inserta una rendición de caja
func logCreate(op ops.Op) oplog.Registro {
	r := oplog.Registro{
		Comitente:    op.Comitente,
		CompID:       op.CompID,
		PuntoDeVenta: op.PuntoDeVenta,
		OpID:         op.ID,
		NComp:        op.NComp,
		Usuario:      op.Usuario,
		Operacion:    oplog.OpCreate,
	}
	return r
}

// Cuando se borra una rendición de caja
func logDelete(usuario string, op ops.Op) oplog.Registro {
	r := oplog.Registro{
		Comitente:    op.Comitente,
		CompID:       op.CompID,
		PuntoDeVenta: op.PuntoDeVenta,
		NComp:        op.NComp,
		OpID:         op.ID,
		Usuario:      usuario,
		Operacion:    oplog.OpDelete,
		Detalle:      fmt.Sprintf("Monto total: %v", op.TotalComp),
	}
	return r
}
