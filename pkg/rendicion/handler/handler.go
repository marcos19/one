package handler

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/marcos19/one/pkg/cajas"
	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"bitbucket.org/marcos19/one/pkg/oplog"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/rendicion"
	"bitbucket.org/marcos19/one/pkg/rendicion/configs"
	"bitbucket.org/marcos19/one/pkg/rendicion/internal/db"
	"bitbucket.org/marcos19/one/pkg/rendicion/pdf"
	"bitbucket.org/marcos19/one/pkg/unicidades"
	"bitbucket.org/marcos19/one/pkg/unicidadesdef"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/niler"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/rs/zerolog/log"
)

// Handler da acceso al a todos los routes de personas.
type Handler struct {
	conn *pgxpool.Pool

	cajaR        cajas.ReaderOne
	configR      configs.Getter
	compR        comps.ReaderOne
	cuentaR      cuentas.ReaderOne
	opInserter   ops.Inserter
	opDeleter    ops.Deleter
	opReaderMany ops.ReaderMany
	opReaderOne  ops.ReaderOne
	unicGetter   ops.UnicidadIDGetter
	empresaR     empresas.ReaderOne
	logoR        empresas.ReaderLogo
	unicidadR    unicidades.ReaderOne
	defR         unicidadesdef.ReaderOne

	dal rendicion.DAL
}

func NewHandler(
	conn *pgxpool.Pool,
	cajaR cajas.ReaderOne,
	configR configs.Getter,
	compR comps.ReaderOne,
	cuentaR cuentas.ReaderOne,
	opHandler OpHandler,
	empresaR empresas.ReaderOne,
	logoR empresas.ReaderLogo,
	unicidadR unicidades.ReaderOne,
	defR unicidadesdef.ReaderOne,
) (h *Handler, err error) {

	if niler.IsNil(conn) {
		return h, errors.New("Conn no puede ser nil")
	}
	if niler.IsNil(cajaR) {
		return h, errors.New("CajasHandler no puede ser nil")
	}
	if niler.IsNil(configR) {
		return h, errors.New("ConfigGetter no puede ser nil")
	}
	if niler.IsNil(compR) {
		return h, errors.New("CompHandler no puede ser nil")
	}
	if niler.IsNil(cuentaR) {
		return h, errors.New("CuentasHandler no puede ser nil")
	}
	if niler.IsNil(opHandler) {
		return h, errors.New("OpHandler no puede ser nil")
	}
	if niler.IsNil(logoR) {
		return h, errors.New("logoReader no puede ser nil")
	}
	if niler.IsNil(compR) {
		return h, errors.New("EmpresasHandler no puede ser nil")
	}
	if niler.IsNil(logoR) {
		return h, errors.New("logoReader no puede ser nil")
	}
	if niler.IsNil(unicidadR) {
		return h, errors.New("UnicidadesHandler no puede ser nil")
	}
	if niler.IsNil(defR) {
		return h, errors.New("UnicidadesDefHandler no puede ser nil")
	}

	h = &Handler{
		conn:         conn,
		cajaR:        cajaR,
		configR:      configR,
		compR:        compR,
		cuentaR:      cuentaR,
		opInserter:   opHandler,
		opDeleter:    opHandler,
		opReaderMany: opHandler,
		opReaderOne:  opHandler,
		unicGetter:   opHandler,
		logoR:        logoR,
		empresaR:     empresaR,
		unicidadR:    unicidadR,
		defR:         defR,
		dal:          db.New(conn),
	}
	return
}

// Insertar valida la op, genera el asiento, inicia transacción e inserta
// en tablas fact, ops y unicidades
func (h *Handler) Create(ctx context.Context, req *rendicion.Rendicion, args rendicion.CreateArgs) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}
	if req.Config == 0 {
		return deferror.Validation("No se ingresó ID de config")
	}
	if req.Fecha == 0 {
		return deferror.Validation("No se ingresó fecha")
	}
	if len(req.Valores) == 0 {
		return deferror.Validation("No se ingresó ningún valor")
	}
	if req.CajaDesde == 0 {
		return deferror.Validation("No se ingresó caja desde")
	}
	if req.CajaHacia == 0 {
		return deferror.Validation("No se ingresó caja hacia")
	}
	if req.CajaDesde == req.CajaHacia {
		return deferror.Validation("Las cajas deben ser distintas")
	}
	if req.Usuario == "" {
		return deferror.Validation("no se ingresó caja usuario")
	}

	// Estamos ok
	req.ID, err = uuid.NewV1()
	if err != nil {
		return errors.Wrap(err, "generando ID")
	}

	// Busco config
	cfg, err := h.configR.ReadOne(ctx, configs.ReadOneReq{Comitente: req.Comitente, ID: req.Config})
	if err != nil {
		return errors.Wrap(err, "buscando config")
	}
	if req.Empresa != cfg.Empresa {
		return deferror.Validation("La empresa ingresada no corresponde con la de la configuración")
	}

	{ // Caja desde
		cajaOK := false
		for _, v := range cfg.CajasDesde {
			if req.CajaDesde == v {
				cajaOK = true
				break
			}
		}
		if !cajaOK {
			return deferror.Validationf("Caja Desde=%v no está permitida en la configuración", req.CajaDesde)
		}
		// Traigo nombre
		caj, err := h.cajaR.ReadOne(ctx, cajas.ReadOneReq{
			ID:        req.CajaDesde,
			Comitente: req.Comitente,
		})
		if err != nil {
			return errors.Wrap(err, "buscando caja desde")
		}
		req.CajaDesdeNombre = caj.Nombre
	}

	{ // Caja hacia
		cajaOK := false
		for _, v := range cfg.CajasHacia {
			if req.CajaHacia == v {
				cajaOK = true
				break
			}
		}
		if !cajaOK {
			return deferror.Validationf("Caja Desde=%v no está permitida en la configuración", req.CajaDesde)
		}
		// Traigo nombre
		caj, err := h.cajaR.ReadOne(ctx, cajas.ReadOneReq{
			ID:        req.CajaHacia,
			Comitente: req.Comitente,
		})
		if err != nil {
			return errors.Wrap(err, "buscando caja desde")
		}
		req.CajaHaciaNombre = caj.Nombre
	}

	for i, v := range req.Valores {

		// Completo nombres de cuentas
		cta, err := h.cuentaR.ReadOne(ctx, cuentas.ReadOneReq{
			ID:        v.Cuenta,
			Comitente: req.Comitente,
		})
		if err != nil {
			return errors.Wrap(err, "buscando cuenta")
		}
		req.Valores[i].Alias = cta.Alias

		// Completo nombres unicidades
		if v.UnicidadRef != nil {

			if *v.UnicidadRef == uuid.Nil {
				return errors.Errorf("%v número %v no tiene determinado ID de unicidad", cta.Nombre, i+1)
			}

			// Busco unicidad
			unic, err := h.unicidadR.ReadOne(ctx, unicidades.ReadOneReq{
				ID:        *v.UnicidadRef,
				Comitente: req.Comitente,
			})
			if err != nil {
				return errors.Wrapf(err, "buscando unicidad '%v'", *v.UnicidadRef)
			}

			// Busco def
			def, err := h.defR.ReadOne(ctx, unicidadesdef.ReadOneReq{
				Comitente: req.Comitente,
				ID:        unic.Definicion,
			})
			if err != nil {
				return errors.Wrap(err, "buscando unicidad def")
			}

			// Creo detalle
			str, err := unicidades.String(unic, def)
			if err != nil {
				// Desconecté porque me jodía con la migración
				log.Error().Int("def", def.ID).Interface("unicidad", unic.ID).Msgf("construyendo string de unicidad")
			}

			req.Valores[i].Detalle = str
			req.Valores[i].Alias = def.Nombre
			req.Valores[i].TipoUnicidad = new(int)
			*req.Valores[i].TipoUnicidad = def.ID

			// Pego partida aplicada
			apli, err := h.unicGetter.GetPartidaAplicada(ctx, req.Comitente, *v.UnicidadRef)
			if err != nil {
				return errors.Wrap(err, "buscando PartidaAplicada ID")
			}
			req.Valores[i].PartidaAplicada = &apli
		}
	}

	req.Detalle = fmt.Sprintf("%v => %v", req.CajaDesdeNombre, req.CajaHaciaNombre)

	// Genero op
	if !args.OmitirReproceso {
		log.Trace().Int("ncomp", req.Op.NComp).Msg("Generando op...")
		req.Op, err = h.GenerarOp(ctx, req)
		if err != nil {
			return errors.Wrap(err, "generando op")
		}
	}

	// Inicio transcacción
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return deferror.DB(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)

	// Persisto rendicion
	err = h.dal.Create(ctx, tx, req)
	if err != nil {
		return errors.Wrap(err, "persistiendo en tabla rendición")
	}

	// Creo flow
	oo := []*ops.Op{&req.Op}

	// Persisto op
	err = h.opInserter.Insert(ctx, tx, oo, nil)
	if err != nil {
		return errors.Wrap(err, "insertando op")
	}

	// Inserto registro log
	reg := logCreate(req.Op)
	err = oplog.Log(ctx, tx, reg)
	if err != nil {
		return errors.Wrap(err, "logging")
	}

	// Confirmo transacción
	err = tx.Commit(ctx)
	if err != nil {
		return deferror.DB(err, "confirmando transacción")
	}
	return
}

func (h *Handler) ReadOne(ctx context.Context, req rendicion.ReadOneReq) (out rendicion.Rendicion, err error) {

	// Valido
	if req.ID == uuid.Nil {
		return out, deferror.Validation("no se ingresó ID de rendición")
	}
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}

	// Busco rendicion
	out, err = h.dal.ReadOne(ctx, req)
	if err != nil {
		return out, deferror.DB(err, "buscando por ID")
	}

	// Busco op
	oo, err := h.opReaderMany.ReadMany(ctx, ops.ReadManyReq{Comitente: req.Comitente, TranID: req.ID})
	if err != nil {
		return rendicion.Rendicion{}, deferror.DB(err, "buscando op")
	}
	switch {
	case len(oo) == 1:
		out.Op = oo[0]

	case len(oo) == 0:
		return out, errors.New("no se encontró la op de la rendición")

	case len(oo) > 1:
		return out, errors.New("había más de una op para la rendición")
	}
	return
}

func (h *Handler) Delete(ctx context.Context, req rendicion.ReadOneReq) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}
	if req.ID == uuid.Nil {
		return deferror.Validation("no se ingresó ID de rendición")
	}
	if req.Usuario == "" {
		return deferror.Validation("no se ingresó usuario")
	}

	// Busco ops id
	id := uuid.UUID{}
	op := ops.Op{Comitente: req.Comitente}
	err = h.conn.QueryRow(ctx, "SELECT id, comp_id, punto_de_venta, n_comp, total_comp FROM ops WHERE tran_id = $1", req.ID).Scan(&id, &op.CompID, &op.PuntoDeVenta, &op.NComp, &op.TotalComp)
	if err != nil {
		return errors.Wrap(err, "buscando ID de op")
	}

	// Inicio transacción
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return errors.Wrap(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)

	// Inicio flow
	err = h.opDeleter.Delete(ctx, tx, req.Comitente, []uuid.UUID{id})
	if err != nil {
		return errors.Wrap(err, "llamando a Delete de DeleteFlow")
	}

	{ // Borro rendición
		err = h.dal.Delete(ctx, tx, req)
		if err != nil {
			return errors.Wrap(err, "borrando rendición")
		}
	}

	// Loggeo
	reg := logDelete(req.Usuario, op)
	err = oplog.Log(ctx, tx, reg)
	if err != nil {
		return errors.Wrap(err, "logging")
	}

	// Confirmo transacción
	err = tx.Commit(ctx)
	if err != nil {
		return errors.Wrap(err, "confirmando transacción")
	}

	return
}

func (h *Handler) Existencias(ctx context.Context, req rendicion.ExistenciasReq) (out []rendicion.Saldo, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}
	if req.Empresa == 0 {
		return out, deferror.Validation("no se ingresó ID de empresa")
	}
	if req.Caja == 0 {
		return out, deferror.Validation("no se ingresó ID de caja")
	}

	return h.dal.Existencias(ctx, req)

}

func (h *Handler) GenerarOp(ctx context.Context, r *rendicion.Rendicion) (op ops.Op, err error) {
	op.ID, err = uuid.NewV1()
	if err != nil {
		return op, err
	}
	op.TranID = r.ID
	op.Comitente = r.Comitente
	op.Empresa = r.Empresa
	op.Fecha = r.Fecha
	op.Programa = rendicion.Programa
	op.TranDef = r.Config
	op.FechaOriginal = r.Fecha
	op.Usuario = r.Usuario
	op.Detalle = r.Detalle
	if r.DetalleManual != "" {
		op.Detalle += " - " + r.DetalleManual
	}

	// Busco config
	cfg, err := h.configR.ReadOne(ctx, configs.ReadOneReq{Comitente: r.Comitente, ID: r.Config})
	if err != nil {
		return op, errors.Wrap(err, "buscando config")
	}

	// Busco comp
	comp, err := h.compR.ReadOne(ctx, comps.ReadOneReq{Comitente: r.Comitente, ID: cfg.Comp})
	if err != nil {
		return op, err
	}
	op.CompID = cfg.Comp

	// Punto de venta
	// TODO Hacer más sofisticado esto, podría tener colisiones de numeración
	if comp.PuntoDeVenta == nil {
		return op, errors.Wrap(err, "la configuración no definió el punto de venta")
	}
	if *comp.PuntoDeVenta == 0 {
		return op, errors.Wrap(err, "la configuración no definió el punto de venta")
	}
	op.PuntoDeVenta = *comp.PuntoDeVenta
	op.NComp = comp.UltimoNumeroUsado + 1
	op.CompNombre = comp.Nombre
	op.NCompStr, err = formatComp(op.PuntoDeVenta, op.NComp, "PPPP-NNNNNNNN")
	if err != nil {
		return op, errors.Wrap(err, "creando string para ncomp")
	}

	// Impresión
	op.Impresion = map[string]interface{}{}

	{ // Datos de la empresa
		empresa, err := h.empresaR.ReadOne(ctx, empresas.ReadOneReq{
			Comitente: r.Comitente,
			ID:        r.Empresa,
		})
		if err != nil {
			return op, errors.Wrap(err, "buscando empresa")
		}

		op.Impresion["EmpresaNombre"] = empresa.ImpresionNombre
		if empresa.ImpresionNombreFantasia != "" {
			op.Impresion["EmpresaNombreFantasia"] = empresa.ImpresionNombreFantasia
		}
		op.Impresion["HeaderRenglonesLeft"] = empresa.ImpresionRenglonesLeft
		op.Impresion["HeaderRenglonesRight"] = empresa.ImpresionRenglonesRight

	}

	op.TotalComp = new(dec.D2)

	// Partidas
	for _, v := range r.Valores {

		{ // Partida de salida
			p := ops.Partida{}
			p.ID, err = uuid.NewV1()
			if err != nil {
				return op, err
			}
			p.Cuenta = v.Cuenta
			p.Caja = new(int)
			*p.Caja = r.CajaDesde
			p.Monto = -v.Monto
			p.FechaFinanciera = v.Vencimiento
			p.UnicidadTipo = v.TipoUnicidad
			p.Unicidad = v.UnicidadRef
			p.PartidaAplicada = v.PartidaAplicada
			p.Detalle = new(string)
			*p.Detalle = v.Detalle

			// TODO: Persona de cheque
			op.PartidasContables = append(op.PartidasContables, p)
		}

		{ // Partida de entrada
			p := ops.Partida{}
			p.ID, err = uuid.NewV1()
			if err != nil {
				return op, err
			}
			p.Cuenta = v.Cuenta
			p.Caja = new(int)
			*p.Caja = r.CajaHacia
			p.Monto = v.Monto
			p.FechaFinanciera = v.Vencimiento
			p.UnicidadTipo = v.TipoUnicidad
			p.Unicidad = v.UnicidadRef
			p.PartidaAplicada = v.PartidaAplicada
			p.Detalle = new(string)
			*p.Detalle = v.Detalle

			// TODO: Persona de cheque
			op.PartidasContables = append(op.PartidasContables, p)
		}

		*op.TotalComp += v.Monto
	}

	return
}

func (h *Handler) PDF(ctx context.Context, req rendicion.PDFReq) (out pdf.Rendicion, err error) {

	// Valido
	if req.OpID == uuid.Nil {
		return out, deferror.Validation("no se ingresó ID de rendición")
	}
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó comitente")
	}

	// Busco la op ingresada
	op, err := h.opReaderOne.ReadOne(ctx, ops.ReadOneReq{Comitente: req.Comitente, ID: req.OpID})
	if err != nil {
		return out, deferror.DB(err, "buscando operación")
	}

	// Fue hecha por este programa?
	if op.Programa != rendicion.Programa {
		return out, errors.New("la operación ingresada no es una rendición de caja")
	}

	// Busco la rendicion
	rend, err := h.dal.ReadOne(ctx, rendicion.ReadOneReq{Comitente: req.Comitente, ID: op.TranID})
	if err != nil {
		return out, deferror.DB(err, "buscando fact")
	}

	// Armo el PDF con los datos
	if op.TotalComp == nil {
		return out, errors.New("op no tiene registrada MontoTotal")
	}

	out = pdf.NewRendicion(pdf.NewFormatoPaginaCompleta())

	// Traigo definición del comp
	comp, err := h.compR.ReadOne(ctx, comps.ReadOneReq{
		Comitente: op.Comitente,
		ID:        op.CompID,
	})
	if err != nil {
		return out, errors.Wrap(err, "buscando comp")
	}
	// Por defecto si la empresa tiene logo, lo pego
	vaLogo := true
	if comp.ImprimeLogo != nil {
		if !*comp.ImprimeLogo {
			vaLogo = false
		}
	}
	if vaLogo {
		// Traigo empresa
		logo, err := h.logoR.ReadLogo(context.Background(), empresas.ReadLogoReq{
			Comitente: op.Comitente,
			Empresa:   op.Empresa,
		})
		if err != nil {
			return out, errors.Wrap(err, "buscando empresa")
		}
		out.Logo = logo
		out.LogoFormato = "png"
	}

	// El resto son los que guardé en el campo Impresion
	{ // Nombre empresa
		nombre, ok := op.Impresion["EmpresaNombre"]
		if !ok {
			return out, errors.Errorf("no se pudo determinar campo NombreEmpresa")
		}
		out.EmpresaNombre, ok = nombre.(string)
		if !ok {
			return out, errors.Errorf("NombreEmpresa no era un string")
		}
	}
	{ // HeaderRenglonesLeft
		rr, ok := op.Impresion["HeaderRenglonesLeft"]
		if !ok {
			return out, errors.Errorf("no se pudo determinar campo HeaderRenglonesLeft")
		}
		str, ok := rr.(string)
		if !ok {
			return out, errors.Errorf("type assertion en HeaderRenglonesLeft")
		}
		out.HeaderRenglonesLeft = strings.Split(str, "\n")
	}
	{ // HeaderRenglonesRight
		rr, ok := op.Impresion["HeaderRenglonesRight"]
		if !ok {
			return out, errors.Errorf("no se pudo determinar campo HeaderRenglonesRight")
		}
		str, ok := rr.(string)
		if !ok {
			return out, errors.Errorf("type assertion en HeaderRenglonesLeft")
		}
		out.HeaderRenglonesRight = strings.Split(str, "\n")
	}

	nombre, _ := op.Impresion["EmpresaNombre"].(string)
	out.EmpresaNombre = nombre
	out.NombreComp = op.CompNombre
	out.NComp = op.NCompStr
	out.CajaDesde = rend.CajaDesdeNombre
	out.CajaHacia = rend.CajaHaciaNombre
	out.Fecha = op.Fecha
	out.Usuario = rend.Usuario
	out.TS = *op.CreatedAt
	out.DetalleManual = rend.DetalleManual
	for _, v := range rend.Valores {
		out.Valores = append(out.Valores, pdf.Valor{
			Alias:       v.Alias,
			Detalle:     v.Detalle,
			Monto:       v.Monto,
			Vencimiento: v.Vencimiento,
		})
	}
	return
}

func formatComp(puntoVenta, numero int, mask string) (out string, err error) {

	mask = strings.ToUpper(mask)
	partes := []string{}

	cantidadPV := strings.Count(mask, "P")
	pv, err := llenarCeros(puntoVenta, cantidadPV)
	if err != nil {
		return out, errors.Wrap(err, "llenando con ceros el punto de venta")
	}
	if pv != "" {
		partes = append(partes, pv)
	}

	cantidadN := strings.Count(mask, "N")
	n, err := llenarCeros(numero, cantidadN)
	if err != nil {
		return out, errors.Wrap(err, "llenando con ceros el comprobante")
	}
	if n != "" {
		partes = append(partes, n)
	}

	return strings.Join(partes, "-"), nil
}

func llenarCeros(n, digitos int) (out string, err error) {
	if digitos == 0 {
		return "", nil
	}
	nStr := fmt.Sprint(n)
	if len(nStr) > digitos {
		return out, errors.Errorf("había más dígitos (%v) que espacios (%v)", len(nStr), digitos)
	}
	cantidad := digitos - len(nStr)
	for i := 1; i <= cantidad; i++ {
		out += "0"
	}
	return out + nStr, nil
}
