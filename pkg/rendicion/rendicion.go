package rendicion

import (
	"time"

	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
)

// Programa es el nombre que se inserta en el campo programa de la tabla ops.
const Programa = "rendicionCaja"

type Rendicion struct {
	ID        uuid.UUID
	Comitente int `json:"-"`
	Empresa   int `json:",string"`

	Config int `json:",string"`
	Fecha  fecha.Fecha

	// Automaticamente pega "Mostrador => Tesorería"
	Detalle string `json:",omitempty"`

	// Ingresado por el usuario
	DetalleManual string `json:",omitempty"`

	Valores []Partida `json:",omitempty"`

	CajaDesde       int `json:",string,omitempty"`
	CajaDesdeNombre string
	CajaHacia       int `json:",string,omitempty"`
	CajaHaciaNombre string

	Monto dec.D2

	Usuario   string
	CreatedAt time.Time

	// Luego de procesada la operación, estos son las structs que se persisten
	Op ops.Op
}
