package pdf

import (
	"io"
	"os"
	"testing"
	"time"

	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/stretchr/testify/assert"
)

func TestFactura(t *testing.T) {

	f := NewRendicion(NewFormatoPaginaCompleta())
	f.EmpresaNombre = "Rafaela Revisión Técnica Vehicular SA"

	// Logo
	logo, err := os.Open("./testdata/vw.png")
	f.LogoFormato = "png"
	assert.Nil(t, err)
	f.Logo, err = io.ReadAll(logo)
	assert.Nil(t, err)

	f.HeaderRenglonesLeft = []string{
		"Domicilio Taller: Ruta Provincial Nº 13 - KM 1.",
		"Domicilio Legal: Gral. Roca 657 - Las Rosas (2520) SF",
		"contacto@rrtvtecnica.com - 03401-449424 / 25",
	}
	f.HeaderRenglonesRight = []string{
		"Fecha Inicio Actividades: 01/11/2012",
		"CUIT: 30-71152196-4",
		"Ingresos Brutos: 121-016857-1",
	}
	// f.Letra = "X"
	f.NombreComp = "Rendición caja 0002-00000154"
	f.Fecha = fecha.Fecha(20200621)

	f.CajaDesde = "Mostrador 1"
	f.CajaHacia = "Tesorería"
	f.DetalleManual = "Este es un detalle manual corto"
	f.DetalleManual = "Este es un detalle manual verdaderamente largo que voy a utilizar para ver como queda dentro de la caja que tiene asignada para ser mostrada"

	f.Valores = []Valor{
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(30000),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(120000),
			Detalle:     "BANCO DE LA NACION ARGENTINA N 165165",
			Vencimiento: fecha.Fecha(20210408),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(8215.45),
			Detalle:     "BANCO MACRO N 165165",
			Vencimiento: fecha.Fecha(20210318),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(94000),
			Detalle:     "BANCO FRANCES N 165165",
			Vencimiento: fecha.Fecha(20210815),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(10000),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(750156),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(30000),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(120000),
			Detalle:     "BANCO DE LA NACION ARGENTINA N 165165",
			Vencimiento: fecha.Fecha(20210408),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(8215.45),
			Detalle:     "BANCO MACRO N 165165",
			Vencimiento: fecha.Fecha(20210318),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(94000),
			Detalle:     "BANCO FRANCES N 165165",
			Vencimiento: fecha.Fecha(20210815),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(10000),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(750156),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		}, {
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(30000),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(120000),
			Detalle:     "BANCO DE LA NACION ARGENTINA N 165165",
			Vencimiento: fecha.Fecha(20210408),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(8215.45),
			Detalle:     "BANCO MACRO N 165165",
			Vencimiento: fecha.Fecha(20210318),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(94000),
			Detalle:     "BANCO FRANCES N 165165",
			Vencimiento: fecha.Fecha(20210815),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(10000),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(750156),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(30000),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(120000),
			Detalle:     "BANCO DE LA NACION ARGENTINA N 165165",
			Vencimiento: fecha.Fecha(20210408),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(8215.45),
			Detalle:     "BANCO MACRO N 165165",
			Vencimiento: fecha.Fecha(20210318),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(94000),
			Detalle:     "BANCO FRANCES N 165165",
			Vencimiento: fecha.Fecha(20210815),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(10000),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(750156),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		}, {
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(30000),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(120000),
			Detalle:     "BANCO DE LA NACION ARGENTINA N 165165",
			Vencimiento: fecha.Fecha(20210408),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(8215.45),
			Detalle:     "BANCO MACRO N 165165",
			Vencimiento: fecha.Fecha(20210318),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(94000),
			Detalle:     "BANCO FRANCES N 165165",
			Vencimiento: fecha.Fecha(20210815),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(10000),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(750156),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(30000),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(120000),
			Detalle:     "BANCO DE LA NACION ARGENTINA N 165165",
			Vencimiento: fecha.Fecha(20210408),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(8215.45),
			Detalle:     "BANCO MACRO N 165165",
			Vencimiento: fecha.Fecha(20210318),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(94000),
			Detalle:     "BANCO FRANCES N 165165",
			Vencimiento: fecha.Fecha(20210815),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(10000),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(750156),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(30000),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(120000),
			Detalle:     "BANCO DE LA NACION ARGENTINA N 165165",
			Vencimiento: fecha.Fecha(20210408),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(8215.45),
			Detalle:     "BANCO MACRO N 165165",
			Vencimiento: fecha.Fecha(20210318),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(94000),
			Detalle:     "BANCO FRANCES N 165165",
			Vencimiento: fecha.Fecha(20210815),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(10000),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(750156),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(30000),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(120000),
			Detalle:     "BANCO DE LA NACION ARGENTINA N 165165",
			Vencimiento: fecha.Fecha(20210408),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(8215.45),
			Detalle:     "BANCO MACRO N 165165",
			Vencimiento: fecha.Fecha(20210318),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(94000),
			Detalle:     "BANCO FRANCES N 165165",
			Vencimiento: fecha.Fecha(20210815),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(10000),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		},
		{
			Alias:       "Cheque de tercero",
			Monto:       dec.NewD2(750156),
			Detalle:     "BANCO SANTANDER N 165165",
			Vencimiento: fecha.Fecha(20210602),
		},
		{
			Alias: "Efectivo (ARS)",
			Monto: dec.NewD2(85432.14),
		},
		{
			Alias: "Efectivo (USD)",
			Monto: dec.NewD2(300),
		},
	}

	f.TotalComp = dec.NewD2(5500)
	f.Usuario = "Marcos Bortolussi"
	f.TS = time.Now()

	{ // Con logo
		file, err := os.Create("./testdata/factura_con_logo.pdf")
		assert.Nil(t, err)
		err = f.Output(file)
		assert.Nil(t, err)
	}

	{ // Sin logo
		file, err := os.Create("./testdata/factura.pdf")
		assert.Nil(t, err)
		f.Logo = []byte{}
		err = f.Output(file)
		assert.Nil(t, err)
	}

}
