package pdf

import (
	"bytes"
	"fmt"
	"io"
	"math"
	"sort"
	"time"

	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/jung-kurt/gofpdf"
	"github.com/rs/zerolog/log"
)

type Rendicion struct {
	fpdf *gofpdf.Fpdf
	tr   func(string) string

	// Header
	EmpresaNombre string
	Logo          []byte
	LogoFormato   string

	HeaderRenglonesLeft  []string
	HeaderRenglonesRight []string

	NombreComp string
	NComp      string

	Fecha fecha.Fecha

	CajaDesde     string
	CajaHacia     string
	DetalleManual string

	// Valores
	Valores []Valor

	// Resumen
	TotalComp dec.D2

	Usuario string
	TS      time.Time

	// Formato

	MargenIzq            float64
	MargenDer            float64
	MargenSup            float64
	MargenInf            float64
	MargenEntreSecciones float64

	HeaderHeight  float64
	PersonaHeight float64
	FooterHeight  float64

	Bordes  bool
	padding float64
}

func (c Rendicion) AnchoImprimible() float64 {
	ancho, _ := c.fpdf.GetPageSize()
	return ancho - c.MargenIzq - c.MargenDer - c.padding*2
}

var oro = math.Sqrt(2.0) / 2.

const (
	A4Ancho = 210.0
	A4Alto  = 297.0
)

type Valor struct {
	Alias       string
	Monto       dec.D2
	Detalle     string
	Vencimiento fecha.Fecha
}

func NewFormatoPaginaCompleta() (f FormatoArgs) {

	// Seteo margenes
	f.MargenIzq = A4Ancho * oro / 14
	f.MargenDer = A4Ancho * oro / 14
	f.MargenSup = A4Ancho * oro / 14
	f.MargenInf = A4Ancho * oro / 14
	f.MargenEntreSecciones = A4Ancho * oro / 200 //0.6

	// Secciones
	f.HeaderHeight = A4Alto * oro / 5
	f.PersonaHeight = A4Alto * oro / 16
	f.FooterHeight = A4Alto * oro / 16

	return
}

type FormatoArgs struct {
	MargenIzq            float64
	MargenDer            float64
	MargenSup            float64
	MargenInf            float64
	MargenEntreSecciones float64

	HeaderHeight  float64
	PersonaHeight float64
	FooterHeight  float64
}

// NewRendicion crea un nuevo comprobante con los valores por defecto.
func NewRendicion(a FormatoArgs) (f Rendicion) {

	// Margenes
	f.MargenIzq = a.MargenIzq
	f.MargenDer = a.MargenDer
	f.MargenSup = a.MargenSup
	f.MargenInf = a.MargenInf
	f.MargenEntreSecciones = a.MargenEntreSecciones

	// Secciones
	f.HeaderHeight = a.HeaderHeight
	f.PersonaHeight = a.PersonaHeight
	f.FooterHeight = a.FooterHeight
	f.Bordes = true
	return f
}

// Output graba en el io.Writer la factura PDF
func (c *Rendicion) Output(w io.Writer) (err error) {

	c.fpdf = gofpdf.New("P", "mm", "A4", "")
	c.tr = c.fpdf.UnicodeTranslatorFromDescriptor("")
	log.Debug().Msgf("Fpdf %p", c.fpdf)

	p := c.fpdf
	log.Debug().Msgf("Fpdf %p", p)
	if c.Bordes {
		c.Bordes = true
		c.padding = A4Ancho * oro / 30
		c.MargenIzq -= 3
		c.MargenDer -= 3
		c.MargenInf -= 3
	}
	p.SetAutoPageBreak(true, c.MargenInf+c.FooterHeight+c.MargenEntreSecciones)

	// Fuentes
	fontSize := 8.

	p.SetLeftMargin(c.MargenIzq)
	p.SetRightMargin(c.MargenDer)
	p.SetTopMargin(c.MargenSup)
	p.SetFont("Arial", "", fontSize)
	_, h := c.fpdf.GetPageSize()

	paginas := []int{}

	p.SetHeaderFunc(func() {
		p.SetMargins(c.MargenIzq, c.MargenSup, c.MargenDer)
		p.SetDrawColor(0, 0, 0)
		c.printHeader()
		log.Debug().Msgf("despues del header %v", p.Error())
		c.printCuadroCajas()

		y := c.MargenSup + c.HeaderHeight + c.PersonaHeight + c.MargenEntreSecciones*4
		alto := h - y - c.MargenInf - c.FooterHeight - c.MargenEntreSecciones*2
		// Recuadro
		if c.Bordes {
			p.ClipRoundedRect(c.MargenIzq, y, c.AnchoImprimible()+c.padding*2, alto, 3, true)
			p.ClipEnd()
			p.SetLeftMargin(c.MargenIzq + 3)
		}
		c.printFooter()

		margenSup := c.MargenSup + c.HeaderHeight + c.PersonaHeight + c.MargenEntreSecciones*4 + c.padding
		p.SetTopMargin(margenSup)
		p.SetY(margenSup)

		paginas = append(paginas, p.PageNo())

	})

	p.AddPage()

	// log.Debug().Msgf("antes del loop de copias %v", p.Error())
	c.printBody()
	// log.Debug().Msgf("despues del body %v", p.Error())

	// Número de página
	p.SetFont("Arial", "", 8)
	p.SetHeaderFunc(nil)
	p.SetAutoPageBreak(false, 0)
	for _, v := range paginas {
		// if v > 10 {
		// 	break
		// }
		log.Debug().Int("pageNum", p.PageNo()).Int("pageCount", p.PageCount()).Msgf("Fijando page %v", v)
		p.SetPage(v)

		str := fmt.Sprintf("Página %v de %v", v, p.PageCount())
		x := c.MargenIzq + c.padding
		y := h - c.MargenInf - c.FooterHeight - c.MargenEntreSecciones
		// fmt.Println("X y Y fijados:", x, y, "footerHeight", c.FooterHeight, "Alto pagina", h)
		p.SetXY(x, y)
		p.CellFormat(c.AnchoImprimible(), c.FooterHeight, c.tr(str), "", 2, "MR", false, 0, "")

	}

	// log.Debug().Msgf("p antes del output %p", p)

	// Corroboro que no haya habido un error
	err = p.Error()
	if err != nil {
		return errors.Wrap(err, "error durante la creación del PDF")
	}

	err = p.Output(w)
	if err != nil {
		return errors.Wrap(err, "exportando el PDF al responseWriter")
	}
	return
}

func (c *Rendicion) printHeader() {
	// NombreEmpresa
	p := c.fpdf
	top := c.MargenSup
	offsetY := top
	// anchoCuadritoLetra := 10.0

	// difPorImg := 20.
	if len(c.Logo) > 0 {
		switch c.LogoFormato {
		case "png", "jpg":
			// Ok
		default:
			offsetY += c.HeaderHeight*(1-oro) + 1
			goto saltearLogo
		}
		buf := bytes.NewReader(c.Logo)
		p.RegisterImageOptionsReader("logo", gofpdf.ImageOptions{ImageType: c.LogoFormato}, buf)
		info := p.GetImageInfo("logo")
		if info == nil {
			log.Error().Msg("no se pudo encontrar información de imagen")
			goto saltearLogo
		}

		// Limito altura, mantengo proporcion original
		alto := 20.0
		offsetY += alto
		ancho := alto * info.Width() / info.Height()
		p.Image("logo", +c.MargenIzq+c.AnchoImprimible()/4-ancho/2,
			top+2, ancho, alto, false, "", 0, "")
		offsetY += 7
	} else {
		// Lo pongo a la misma altura que 'Factura A 0001'
		offsetY += c.HeaderHeight*(1-oro) + 1
	}

saltearLogo:

	// Nombre empresa
	p.SetLeftMargin(c.MargenIzq)
	p.SetY(offsetY)
	p.SetFont("Arial", "B", 10)
	p.CellFormat(c.AnchoImprimible()/2+c.padding, 1, c.tr(c.EmpresaNombre), "", 1, "C", false, 0, "")
	p.Ln(1)
	// p.WriteAligned(
	// 	c.AnchoImprimible()/2, // Width
	// 	0,                     // Line height
	// 	c.tr(c.EmpresaNombre), "C")

	// Renglones de la izquierda
	p.SetFont("Arial", "", 8)
	offsetY += 3
	p.SetY(offsetY)
	for _, v := range c.HeaderRenglonesLeft {
		p.SetX(c.MargenIzq)
		p.CellFormat(c.AnchoImprimible()/2+c.padding, 3, c.tr(v), "", 1, "C", false, 0, "")
		p.Ln(0)
	}

	// Factura A 0003-23244323
	p.SetXY(c.MargenIzq+c.AnchoImprimible()/2+c.padding, top+7+5)
	p.SetFont("Arial", "B", 12)
	nombre := fmt.Sprintf("%v %v", c.NombreComp, c.NComp)
	p.CellFormat(c.AnchoImprimible()/2+c.padding, 1, c.tr(nombre), "", 1, "C", false, 0, "")

	// Fecha
	p.SetXY(c.MargenIzq+c.AnchoImprimible()/2+c.padding, top+7+5+5)
	p.SetFont("Arial", "", 9)
	fchStr := c.tr("Fecha emisión: " + c.Fecha.String())
	// p.Text(c.MargenIzq+c.AnchoImprimible()/2+15, top+20, fchStr)
	p.CellFormat(c.AnchoImprimible()/2+c.padding, 3, fchStr, "", 1, "C", false, 0, "")

	// Renglones de la derecha
	p.SetY(top + 7 + 20)
	p.SetFont("Arial", "", 8)
	for _, v := range c.HeaderRenglonesRight {
		// p.SetX(c.MargenIzq + c.AnchoImprimible()/2)
		p.SetX(c.AnchoImprimible()/2 + c.MargenIzq + c.padding)
		p.CellFormat(c.AnchoImprimible()/2+c.padding, 3, c.tr(v), "", 1, "C", false, 0, "")
		p.Ln(0)
	}

	// Cuadrado del header
	if c.Bordes {
		p.ClipRoundedRect(c.MargenIzq, top, c.AnchoImprimible()+c.padding*2, c.HeaderHeight, 3, true)
		p.ClipEnd()
	}

}

// Dibuja el cuadro del cliente a la altura indicada
func (c *Rendicion) printCuadroCajas() {
	p := c.fpdf

	offsetY := c.MargenSup + c.HeaderHeight + c.MargenEntreSecciones*2
	offsetX := c.MargenIzq

	// Cuadrado del header
	if c.Bordes {
		p.ClipRoundedRect(offsetX, offsetY, c.AnchoImprimible()+c.padding*2, c.PersonaHeight, 3, true)
		p.ClipEnd()
	}
	offsetX += c.padding
	p.SetLeftMargin(offsetX)
	p.SetY(offsetY)

	// Caja1 => Caja 2
	p.SetFont("Arial", "B", 10)
	cajasString := c.tr(fmt.Sprintf("Cajas: %v => %v", c.CajaDesde, c.CajaHacia))

	if c.DetalleManual == "" {
		p.CellFormat(c.AnchoImprimible(), c.PersonaHeight, cajasString, "", 1, "MC", false, 0, "")

	} else {
		p.Ln(2)
		p.CellFormat(c.AnchoImprimible(), 5, cajasString, "", 1, "TC", false, 0, "")
		det := "Detalle: " + c.DetalleManual
		p.SetFont("Arial", "", 9)
		p.MultiCell(c.AnchoImprimible(), 3, det, "", "TL", false)
	}
	p.Ln(1.125)

}

// Dibuja el cuadro del footer
func (c *Rendicion) printBody() {
	p := c.fpdf

	offsetX := c.MargenIzq + c.padding

	p.SetDrawColor(249, 249, 249)
	p.SetX(offsetX)

	// Renglones producto
	p.SetFont("Arial", "", 10)
	p.SetFillColor(249, 249, 249)

	unics := map[string][]Valor{}
	totales := map[string]dec.D2{}

	// Agrupo por alias
	for _, v := range c.Valores {
		// Sumo
		totales[v.Alias] += v.Monto
		c.TotalComp += v.Monto

		// Unicidades
		anterior := unics[v.Alias]
		anterior = append(anterior, v)
		sort.Slice(anterior, func(i, j int) bool {
			if anterior[i].Vencimiento < anterior[j].Vencimiento {
				return true
			} else {
				return anterior[i].Monto < anterior[j].Monto
			}
		})
		unics[v.Alias] = anterior
	}
	// Ordeno keys
	sortedKeys := []string{}
	for k := range totales {
		sortedKeys = append(sortedKeys, k)
	}
	sort.Strings(sortedKeys)

	margenTablita := 0.
	anchoTablita := c.AnchoImprimible() - margenTablita
	anchoItem := 6.
	anchoVto := 25.
	anchoMonto := 30.
	anchoDetalle := anchoTablita - anchoItem - anchoVto - anchoMonto //anchoTablita - anchoVto - anchoItem - anchoMonto
	p.SetTopMargin(c.MargenSup + c.HeaderHeight + c.PersonaHeight + c.MargenEntreSecciones*2 + 50)
	// p.SetXY(c.MargenIzq+c.padding, c.MargenSup+c.HeaderHeight+c.PersonaHeight+c.MargenEntreSecciones*4+c.padding)
	for _, v := range sortedKeys {
		p.SetFont("Arial", "B", 10)
		cantidad := v
		if len(unics[v]) > 0 {
			cantidad += fmt.Sprintf(" (%v)", len(unics[v]))
		}
		p.SetLeftMargin(c.MargenIzq + c.padding)
		p.SetFillColor(220, 220, 220)
		p.CellFormat(c.AnchoImprimible()/3*2, 7, c.tr(cantidad), "TBL", 0, "ML", true, 0, "")
		// p.CellFormat(c.AnchoImprimible()/3, 7, cantidad, "", 0, "MR", true, 0, "")
		p.CellFormat(c.AnchoImprimible()/3, 7, c.tr(totales[v].String()), "TBR", 1, "MR", true, 0, "")

		p.SetFont("Arial", "", 9)
		fill := false
		p.SetFillColor(249, 249, 249)
		p.SetDrawColor(249, 249, 249)
		for i, w := range unics[v] {
			p.SetLeftMargin(c.MargenIzq + c.padding + margenTablita)
			fill = !fill
			p.CellFormat(anchoItem, 5, fmt.Sprint(i+1), "", 0, "MR", fill, 0, "")
			vto := ""
			if w.Vencimiento != 0 {
				vto = w.Vencimiento.String()
			}

			p.CellFormat(anchoVto, 5, vto, "", 0, "MC", fill, 0, "")
			p.CellFormat(anchoDetalle, 5, c.tr(w.Detalle), "", 0, "ML", fill, 0, "")
			p.CellFormat(anchoMonto, 5, c.tr(w.Monto.String()), "", 1, "MR", fill, 0, "")
		}
		p.Ln(6)
	}
	// Cuadrito total
	anchoPintado := 44.0
	p.ClipRoundedRect(
		c.MargenIzq+c.AnchoImprimible()+c.padding-anchoPintado,
		p.GetY(),
		anchoPintado,
		7, 2, false)
	p.SetFillColor(220, 220, 220)
	p.Rect(
		c.MargenIzq+c.AnchoImprimible()+c.padding-anchoPintado,
		p.GetY(),
		anchoPintado, 7,
		"F")
	p.ClipEnd()

	p.SetFont("Arial", "B", 10)
	p.SetXY(c.MargenIzq+c.padding, p.GetY()+1.5)
	p.CellFormat(c.AnchoImprimible()-30, 4, c.tr("Total"), "", 0, "MR", false, 0, "")
	p.CellFormat(30, 4, c.tr(c.TotalComp.String()), "", 1, "MR", false, 0, "")
	p.Ln(10)
	p.Write(3, "SON "+c.TotalComp.EnPalabras("PESOS"))

}

// Dibuja  la sección footer
func (c *Rendicion) printFooter() {
	p := c.fpdf
	_, h := c.fpdf.GetPageSize()
	offsetY := h - c.MargenInf - c.FooterHeight - c.MargenEntreSecciones
	offsetX := c.MargenIzq

	p.SetDrawColor(0, 0, 0)
	// p.SetFillColor(0, 0, 0)
	// Recuadro
	if c.Bordes {
		p.ClipRoundedRect(offsetX, offsetY, c.AnchoImprimible()+c.padding*2, c.FooterHeight, 3, true)
		p.ClipEnd()
	}

	p.SetLeftMargin(c.MargenIzq + c.padding)
	p.SetY(offsetY + 3)
	p.SetFont("Arial", "B", 8)
	p.CellFormat(15, 3, "Usuario:", "", 0, "MR", false, 0, "")
	p.SetFont("Arial", "", 8)
	p.CellFormat(c.AnchoImprimible()/2, 3, c.Usuario, "", 1, "ML", false, 0, "")
	p.SetFont("Arial", "B", 8)
	p.CellFormat(15, 3, "Registro:", "", 0, "MR:", false, 0, "")
	p.SetFont("Arial", "", 8)
	p.CellFormat(c.AnchoImprimible()/2, 3, c.TS.Local().Format("02/01/06 15:04:05"+" hs"), "", 0, "ML", false, 0, "")

}
