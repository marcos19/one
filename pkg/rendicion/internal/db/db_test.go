package db

import (
	"context"
	"testing"
	"time"

	"bitbucket.org/marcos19/one/pkg/database"
	"bitbucket.org/marcos19/one/pkg/migrations/ddl"
	"bitbucket.org/marcos19/one/pkg/rendicion"
	"github.com/davecgh/go-spew/spew"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func Prepare(ctx context.Context, conn *pgxpool.Pool) error {

	{ // Borro base de datos
		q := `DROP DATABASE IF EXISTS test_rendiciones;`
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "borro base de datos")
		}
	}
	{ // Creo base de datos
		q := `CREATE DATABASE test_rendiciones; USE test_rendiciones;`
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando base de datos")
		}
	}

	{ // Comitentes
		_, err := conn.Exec(ctx, ddl.Comitentes().SQL)
		if err != nil {
			return errors.Wrap(err, "creando tabla de comitentes")
		}
		_, err = conn.Exec(ctx, "UPSERT INTO comitentes (id) VALUES (1);")
		if err != nil {
			return errors.Wrap(err, "insertando comitente")
		}
	}

	{ // Empresas
		_, err := conn.Exec(ctx, ddl.Empresas().SQL)
		if err != nil {
			return errors.Wrap(err, "creando tabla de empresas")
		}
		_, err = conn.Exec(ctx, "UPSERT INTO empresas (id) VALUES (1);")
		if err != nil {
			return errors.Wrap(err, "insertando comitente")
		}
	}

	{ // Creo table de comps
		q := `CREATE TABLE comps (
			id INT PRIMARY KEY,
			comitente INT NOT NULL REFERENCES comitentes,
			empresa INT NOT NULL REFERENCES empresas);`
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla de comps")
		}
		_, err = conn.Exec(ctx, "UPSERT INTO comps (id, comitente, empresa) VALUES (1,1,1);")
		if err != nil {
			return errors.Wrap(err, "insertando comp")
		}
	}

	{ // Creo table de rendiciones config
		_, err := conn.Exec(ctx, ddl.RendicionConfig().SQL)
		if err != nil {
			return errors.Wrap(err, "creando tabla de rendicion config")
		}
		_, err = conn.Exec(ctx, "UPSERT INTO rendiciones_config (id, comitente, empresa, comp) VALUES (1,1,1,1);")
		if err != nil {
			return errors.Wrap(err, "insertando rendicion config")
		}
	}

	{ // Creo table de rendiciones
		_, err := conn.Exec(ctx, ddl.Rendicion().SQL)
		if err != nil {
			return errors.Wrap(err, "creando tabla de rendiciones")
		}
	}

	{ // Partidas (la necesito para borrar
		q := `CREATE TABLE IF NOT EXISTS partidas (ID int PRIMARY KEY, comitente int references comitentes, centro int)`
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla partidas")
		}
	}

	return nil
}

func TestCreate(t *testing.T) {

	ctx := context.Background()

	cn := database.TestDBConnectionString()
	spew.Dump(cn)

	cfg, err := pgxpool.ParseConfig(cn)
	require.Nil(t, err)
	conn, err := pgxpool.ConnectConfig(context.Background(), cfg)
	require.Nil(t, err)

	dal := New(conn)

	// Preparo base de datos
	err = Prepare(ctx, conn)
	require.Nil(t, err)

	t.Run("CREATE", func(t *testing.T) {

		tx, err := conn.Begin(ctx)
		require.Nil(t, err)

		rend := rendicionValida()
		err = dal.Create(ctx, tx, &rend)
		require.Nil(t, err)

		err = tx.Commit(ctx)
		require.Nil(t, err)

		assert.NotEqual(t, 0, id)
	})

	t.Run("READ", func(t *testing.T) {

		r, err := dal.ReadOne(ctx, rendicion.ReadOneReq{Comitente: 1, ID: id})
		require.Nil(t, err)

		prev := rendicionValida()
		assert.NotEqual(t, r.CreatedAt, prev.CreatedAt)
		r.CreatedAt = time.Time{}
		assert.Equal(t, prev, r)
		assert.NotEqual(t, 0, id)
	})

	t.Run("DELETE", func(t *testing.T) {

		// Tx begin
		tx, err := conn.Begin(ctx)
		require.Nil(t, err)

		// Delete
		err = dal.Delete(ctx, tx, rendicion.ReadOneReq{Comitente: 1, ID: id})
		require.Nil(t, err)

		// Tx commit
		err = tx.Commit(ctx)
		require.Nil(t, err)

		// Tx begin
		tx, err = conn.Begin(ctx)
		require.Nil(t, err)

		_, err = dal.ReadOne(ctx, rendicion.ReadOneReq{Comitente: 1, ID: id})
		require.NotNil(t, err)

		// Tx commit
		err = tx.Commit(ctx)
		require.Nil(t, err)

	})
}

var id = uuid.FromStringOrNil("a823b0c6-4f6b-4ccf-be4c-76fc4129a07f")

func rendicionValida() rendicion.Rendicion {
	return rendicion.Rendicion{
		ID:            id,
		Comitente:     1,
		Empresa:       1,
		Fecha:         20221201,
		Config:        1,
		Detalle:       "Detalle",
		DetalleManual: "Detalle manual",
		Valores: []rendicion.Partida{
			{
				Cuenta:      1,
				Alias:       "Efectivo",
				Detalle:     "Detalle",
				Monto:       10000,
				Vencimiento: 20201231,
				//MontoMonedaOriginal: 50,
			},
			{
				Cuenta:      2,
				Alias:       "Alias",
				Detalle:     "Detalle",
				Monto:       500000,
				Vencimiento: 20201231,
				//MontoMonedaOriginal: 50,
			},
		},
		CajaDesde:       1,
		CajaDesdeNombre: "Tesorería",
		CajaHacia:       2,
		CajaHaciaNombre: "Otra caja",
		Monto:           100,
		Usuario:         "usuario",
	}
}
