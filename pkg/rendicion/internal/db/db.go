package db

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/rendicion"
	"github.com/crosslogic/dec"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

type DAL struct {
	conn *pgxpool.Pool
}

func New(conn *pgxpool.Pool) *DAL {
	return &DAL{conn}
}

func (d *DAL) ReadOne(ctx context.Context, req rendicion.ReadOneReq) (out rendicion.Rendicion, err error) {
	query := `SELECT id, comitente, empresa, config, fecha, detalle, detalle_manual, valores, caja_desde, caja_desde_nombre,
	caja_hacia, caja_hacia_nombre, monto, usuario, created_at
	FROM rendiciones
	WHERE comitente=$1 AND id=$2`

	err = d.conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(&out.ID, &out.Comitente, &out.Empresa,
		&out.Config, &out.Fecha, &out.Detalle, &out.DetalleManual, &out.Valores, &out.CajaDesde, &out.CajaDesdeNombre,
		&out.CajaHacia, &out.CajaHaciaNombre, &out.Monto, &out.Usuario, &out.CreatedAt)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}

	return
}

// Persistir persiste la struct Rendicion en la tabla rendiciones.
func (d *DAL) Create(ctx context.Context, tx pgx.Tx, r *rendicion.Rendicion) (err error) {

	insertQuery := `
INSERT INTO rendiciones
(
id,
comitente,
empresa,
config,
fecha,
detalle,
detalle_manual,
valores,
caja_desde,
caja_desde_nombre,
caja_hacia,
caja_hacia_nombre,
monto,
usuario,
created_at
) VALUES (
$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15
);
	`
	_, err = tx.Exec(ctx, insertQuery,
		r.ID,
		r.Comitente,
		r.Empresa,
		r.Config,
		r.Fecha,
		r.Detalle,
		r.DetalleManual,
		r.Valores,
		r.CajaDesde,
		r.CajaDesdeNombre,
		r.CajaHacia,
		r.CajaHaciaNombre,
		r.Monto,
		r.Usuario,
		r.CreatedAt,
	)

	if err != nil {
		return deferror.DB(err, "persistiendo rendición")
	}
	return

}

func (d *DAL) Delete(ctx context.Context, tx pgx.Tx, req rendicion.ReadOneReq) error {
	query := `DELETE FROM rendiciones WHERE id = $1 AND comitente = $2`
	res, err := tx.Exec(ctx, query, req.ID, req.Comitente)
	if err != nil {
		return err
	}
	if res.RowsAffected() == 0 {
		return errors.Errorf("no se borró ningún registro")
	}
	return nil
}

func (d *DAL) Existencias(ctx context.Context, req rendicion.ExistenciasReq) (out []rendicion.Saldo, err error) {

	// Busco
	query := `
		SELECT cuenta, SUM(monto), COUNT(monto)
		FROM saldos 
		WHERE 
			caja = $1 AND
			comitente = $2 AND 
			empresa = $3
		GROUP BY cuenta;
		`

	rows, err := d.conn.Query(ctx, query, req.Caja, req.Comitente, req.Empresa)
	if err != nil {
		return out, deferror.DB(err, "buscando saldos")
	}
	defer rows.Close()

	for rows.Next() {
		v := rendicion.Saldo{}
		var monto int64
		var count int64
		err = rows.Scan(&v.Cuenta, &monto, &count)
		if err != nil {
			return out, deferror.DB(err, "escaneando row de saldo")
		}
		v.Cantidad = int(count)
		v.Monto = dec.D2(monto)
		out = append(out, v)
	}

	return
}
