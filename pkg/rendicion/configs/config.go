package configs

import (
	"time"

	"bitbucket.org/marcos19/one/pkg/tipos"
)

type Config struct {
	ID        *int `json:",string"`
	Comitente int
	Empresa   int `json:",string"`
	Nombre    string

	Comp       int `json:",string"`
	CajasDesde tipos.GrupoInts
	CajasHacia tipos.GrupoInts
	Cuentas    tipos.GrupoInts

	Usuario   string
	CreatedAt *time.Time
	UpdatedAt *time.Time
}

func (c Config) TableName() string {
	return "rendiciones_config"
}
