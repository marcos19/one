package configs

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Handler struct {
	conn *pgxpool.Pool
}

func NewHandler(db *pgxpool.Pool) *Handler {
	return &Handler{db}
}

type ReadOneReq struct {
	ID        int `json:",string"`
	Comitente int
}

type Getter interface {
	ReadOne(context.Context, ReadOneReq) (Config, error)
}

func (h *Handler) Create(ctx context.Context, req Config) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}
	if req.Nombre == "" {
		return deferror.Validation("no se ingresó ID de comitente")
	}
	if req.Comp == 0 {
		return deferror.Validation("no se ingresó comp")
	}
	if req.Empresa == 0 {
		return deferror.Validation("no se ingresó empresa")
	}

	query := `INSERT INTO rendiciones_config (comitente, empresa, nombre,
	comp, cajas_desde, cajas_hacia, cuentas, usuario, created_at)
	VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9)`

	// Inserto
	_, err = h.conn.Exec(ctx, query, req.Comitente, req.Empresa, req.Nombre,
		req.Comp, req.CajasDesde, req.CajasHacia, req.Cuentas, req.Usuario, time.Now())
	if err != nil {
		return deferror.DB(err, "creando config")
	}

	return
}

func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (v Config, err error) {

	// Valido
	if req.Comitente == 0 {
		return v, deferror.Validation("no se ingresó ID de comitente")
	}
	if req.ID == 0 {
		return v, deferror.Validation("No se ingresó ID de config")
	}

	query := `SELECT id, comitente, empresa, nombre,
	comp, cajas_desde, cajas_hacia, cuentas, usuario, created_at, updated_at
	FROM rendiciones_config
	WHERE comitente=$1 AND id=$2`

	// Busco
	err = h.conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(
		&v.ID, &v.Comitente, &v.Empresa, &v.Nombre, &v.Comp, &v.CajasDesde,
		&v.CajasHacia, &v.Cuentas, &v.Usuario, &v.CreatedAt, &v.UpdatedAt)
	if err != nil {
		return v, deferror.DBf(err, "buscando rendición config por ID=%v", req.ID)
	}

	return
}

type ReadManyReq struct {
	Comitente int
	IDs       tipos.GrupoInts
	Empresa   int `json:",string"`
}

func (h *Handler) ReadMany(ctx context.Context, req ReadManyReq) (out []Config, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}

	ww := []string{"comitente=$1"}
	pp := []interface{}{req.Comitente}

	if req.Empresa != 0 {
		pp = append(pp, req.Empresa)
		ww = append(ww, fmt.Sprintf("empresa=$%v", len(pp)))
	}
	if len(req.IDs) > 0 {
		ww = append(ww, fmt.Sprintf("id IN %v", req.IDs.ParaClausulaIn()))
	}

	where := "WHERE " + strings.Join(ww, " AND ")

	query := fmt.Sprintf(`SELECT id, comitente, empresa, nombre,
	comp, cajas_desde, cajas_hacia, cuentas, usuario, created_at, updated_at
	FROM rendiciones_config
	%v`, where)

	// Busco
	rows, err := h.conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "buscando rendiciones config")
	}
	defer rows.Close()
	for rows.Next() {
		v := Config{}
		err = rows.Scan(&v.ID, &v.Comitente, &v.Empresa, &v.Nombre,
			&v.Comp, &v.CajasDesde, &v.CajasHacia, &v.Cuentas, &v.Usuario,
			&v.CreatedAt, &v.UpdatedAt)
		if err != nil {
			return out, deferror.DB(err, "escaneando config")
		}
		out = append(out, v)
	}

	return
}

func (h *Handler) Update(ctx context.Context, req Config) (err error) {

	// Valido
	if req.ID == nil {
		return deferror.Validation("no se ingresó ID")
	}
	if *req.ID == 0 {
		return deferror.Validation("no se ingresó ID")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}
	if req.Nombre == "" {
		return deferror.Validation("no se ingresó ID de comitente")
	}
	if req.Comp == 0 {
		return deferror.Validation("no se ingresó comp")
	}
	if req.Empresa == 0 {
		return deferror.Validation("no se ingresó empresa")
	}

	query := `UPDATE rendiciones_config SET
		nombre=$3,
		comp=$4,
		cajas_desde=$5,
		cajas_hacia=$6,
		cuentas=$7,
		usuario=$8,
		updated_at=$9
	WHERE comitente=$1 AND id=$2
	`
	// Inserto
	res, err := h.conn.Exec(ctx, query, req.Comitente, req.ID,
		req.Nombre, req.Comp, req.CajasDesde, req.CajasHacia,
		req.Cuentas, req.Usuario, time.Now())
	if err != nil {
		return deferror.DB(err, "creando config")
	}
	if res.RowsAffected() == 0 {
		return errors.New("ningún registro fue modificado")
	}

	return
}

func (h *Handler) Delete(ctx context.Context, req ReadOneReq) (err error) {

	// Valido
	if req.ID == 0 {
		return deferror.Validation("no se ingresó ID")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}

	{ // Está usado?
		count := 0
		err = h.conn.QueryRow(ctx, "SELECT COUNT(id) FROM rendiciones WHERE comitente=$1 AND config=$2",
			req.Comitente, req.ID).Scan(&count)
		if err != nil {
			return deferror.DB(err, "determinando si la configuración estaba usada")
		}
		if count > 0 {
			return deferror.Validationf("no se puede borrar porque la configuración ya fue usada %v veces", count)
		}
	}

	res, err := h.conn.Exec(ctx,
		"DELETE FROM rendiciones_config WHERE comitente=$1 AND id=$2", req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "deleting")
	}
	if res.RowsAffected() != 0 {
		return errors.New("no se borró ningún registro")
	}

	return
}
