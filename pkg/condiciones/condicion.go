package condiciones

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"reflect"
	"time"

	"github.com/cockroachdb/errors"
	"github.com/crosslogic/fecha"
)

type Condicion struct {
	ID             int `json:",string"`
	Comitente      int `json:",string"`
	Nombre         string
	Modalidad      string
	CantidadCuotas int
	Cuotas         Cuotas
	Modificable    bool
	ValidaDesde    *fecha.Fecha
	ValidaHasta    *fecha.Fecha
	CreatedAt      time.Time
	UpdatedAt      *time.Time
}

func (c Condicion) TableName() string {
	return "condiciones"
}

type Cuota struct {
	Numero int
	Dias   int
	// Vence el 31/05/2020 independientemente de cuando se facture
	FechaFija  *fecha.Fecha
	Porcentaje float64
}

type Cuotas []Cuota

// Value cumple con la interface SQL
func (j Cuotas) Value() (driver.Value, error) {
	return json.Marshal(j)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (j *Cuotas) Scan(src interface{}) error {
	if src == nil {
		*j = Cuotas{}
		return nil
	}

	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New(fmt.Sprint(
			"Type assertion error .([]byte). Era: ",
			reflect.TypeOf(src),
		))
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, j)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("Unmarshalling los siguientes bytes: '%s'", source))
	}

	return nil
}
