package condiciones

import (
	"context"
	"fmt"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/dgraph-io/ristretto"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler es el encargado de recibir los http requests y emitir los responses.
type Handler struct {
	conn  *pgxpool.Pool
	cache *ristretto.Cache
}

type HandlerConfig struct {
	Conn  *pgxpool.Pool
	Cache *ristretto.Cache
}

// NewHandler inicializa un handler.
func NewHandler(c HandlerConfig) (h *Handler, err error) {

	h = &Handler{}
	if c.Conn == nil {
		return h, errors.New("DB era nil")
	}
	h.conn = c.Conn

	if c.Cache == nil {
		return h, errors.New("cache era nil")
	}
	h.cache = c.Cache

	return
}

func (h *Handler) Create(ctx context.Context, req Condicion) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó comitente")
	}

	// Persisto
	err = create(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "persistiendo")
	}
	return
}

func (h *Handler) Update(ctx context.Context, req Condicion) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó comitente")
	}
	if req.Nombre == "" {
		return deferror.Validation("Debe ingresar un nombre para la condición")
	}

	// Borro del cache
	key := hashInt(ReadOneReq{Comitente: req.Comitente, ID: req.ID})
	h.cache.Del(key)

	// Persisto
	err = update(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "persistiendo")
	}
	return
}

func (h *Handler) ReadMany(ctx context.Context, req ReadManyReq) (out []Condicion, err error) {

	// Busco las condiciones
	out, err = readMany(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando condiciones")
	}
	return
}

func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (out Condicion, err error) {

	// Valido
	if req.ID == 0 {
		return out, deferror.Validation("no se determinó ID")
	}
	if req.Comitente == 0 {
		return out, deferror.Validation("no se determinó comitente")
	}

	// Busco en cache
	key := hashInt(req)
	val, estaba := h.cache.Get(key)
	if estaba {
		out, ok := val.(Condicion)
		if ok && out.Comitente == req.Comitente {
			return out, nil
		}
	}

	// No estaba, busco en DB
	out, err = readOne(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando condición por ID")
	}

	// Agrego al cache
	h.cache.Set(key, out, 0)
	return
}

// // ParaStore inserta una nueva condición de pago.
// func (h *Handler) ParaStore(comitente int) http.HandlerFunc {
// 	const op = errors.Operation("condiciones.HandleMany()")
// 	return func(w http.ResponseWriter, r *http.Request) {
// 		rr := []Condicion{}

// 		// Busco las condiciones
// 		err := h.db.Find(&rr, "comitente = ?", comitente).Error
// 		if err != nil {
// 			errors.HTTP(w, errors.E(err, op), r, errors.Database)
// 			return
// 		}

//			// Codifico
//			err = json.NewEncoder(w).Encode(&rr)
//			if err != nil {
//				errors.HTTP(w, errors.E(err, op, "codificando JSON"), r, errors.Internal)
//				return
//			}
//		}
//	}
func hashInt(req ReadOneReq) string {
	return fmt.Sprintf("%v/%v/%v", "Condicion", req.Comitente, req.ID)
}
