package condiciones

import (
	"context"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/jackc/pgx/v4/pgxpool"
)

func create(ctx context.Context, conn *pgxpool.Pool, req Condicion) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó comitente")
	}
	if req.Nombre == "" {
		return deferror.Validation("Debe ingresar un nombre para la condición")
	}

	query := `INSERT INTO condiciones (
		comitente, nombre, modalidad, cantidad_cuotas, cuotas,
		modificable, valida_desde, valida_hasta, created_at)
	VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9)`

	_, err = conn.Exec(ctx, query,
		req.Comitente, req.Nombre, req.Modalidad, req.CantidadCuotas, req.Cuotas,
		req.Modificable, req.ValidaDesde, req.ValidaHasta, time.Now())
	if err != nil {
		return deferror.DB(err, "inserting")
	}

	return
}
func update(ctx context.Context, conn *pgxpool.Pool, req Condicion) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó comitente")
	}
	if req.Nombre == "" {
		return deferror.Validation("Debe ingresar un nombre para la condición")
	}

	query := `UPDATE condiciones SET 
		nombre=$3, modalidad=$4, cantidad_cuotas=$5, cuotas=$6, 
		valida_desde=$7, valida_hasta=$8, updated_at=$9, modificable=$10
	WHERE comitente=$1 AND id=$2`

	_, err = conn.Exec(ctx, query, req.Comitente, req.ID, req.Nombre, req.Modalidad,
		req.CantidadCuotas, req.Cuotas, req.ValidaDesde, req.ValidaHasta, time.Now(), req.Modificable)
	if err != nil {
		return deferror.DB(err, "updating")
	}
	return
}

type ReadManyReq struct {
	Comitente int `json:",string"`
}

func readMany(ctx context.Context, conn *pgxpool.Pool, req ReadManyReq) (out []Condicion, err error) {
	out = []Condicion{}

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó comitente")
	}
	query := `SELECT id, comitente, nombre, modalidad, cantidad_cuotas, cuotas, modificable, valida_desde, valida_hasta, created_at, updated_at
	FROM condiciones
	WHERE comitente=$1
	ORDER BY nombre`

	rows, err := conn.Query(ctx, query, req.Comitente)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		v := Condicion{}
		err = rows.Scan(
			&v.ID, &v.Comitente, &v.Nombre, &v.Modalidad, &v.CantidadCuotas,
			&v.Cuotas, &v.Modificable, &v.ValidaDesde, &v.ValidaHasta,
			&v.CreatedAt, &v.UpdatedAt)
		if err != nil {
			return out, deferror.DB(err, "escaneando")
		}
		out = append(out, v)
	}
	return
}

type ReadOneReq struct {
	ID        int `json:",string"`
	Comitente int `json:",string"`
}

func readOne(ctx context.Context, conn *pgxpool.Pool, req ReadOneReq) (out Condicion, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó comitente")
	}
	if req.ID == 0 {
		return out, deferror.Validation("no se ingresó comitente")
	}
	query := `SELECT id, comitente, nombre, modalidad, cantidad_cuotas, cuotas, 
		modificable, valida_desde, valida_hasta, 
		created_at, updated_at
	FROM condiciones
	WHERE comitente=$1 AND id=$2
	ORDER BY nombre`

	err = conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(
		&out.ID, &out.Comitente, &out.Nombre, &out.Modalidad, &out.CantidadCuotas, &out.Cuotas,
		&out.Modificable, &out.ValidaDesde, &out.ValidaHasta,
		&out.CreatedAt, &out.UpdatedAt,
	)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}
	return
}
