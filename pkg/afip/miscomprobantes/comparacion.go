package miscomprobantes

import (
	"bytes"
	"context"
	"encoding/base64"
	"fmt"
	"sort"
	"strings"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/iva"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/cuits"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Propio struct {
	OpID       uuid.UUID
	Fecha      fecha.Fecha
	CUIT       *cuits.CUIT
	TipoComp   *int
	PuntoVenta int
	NComp      int

	Denominacion string
	Neto         dec.D2
	Exento       dec.D2
	NoGravado    dec.D2
	IVA          dec.D2
	Total        dec.D2
}

type Diferencia struct {
	Campo string
	Dif   dec.D2
}

type Comparacion struct {
	DeAFIP             []Recibido
	Propios            []Propio
	FaltanContabilizar []Recibido
	FaltanEnAFIP       []Propio
	// Diferencias        map[Key]Diferencia
	Periodo string
}

type CompararReq struct {
	Comitente int
	Empresa   int    `json:",string"`
	Archivo   string // B64
	Mes       fecha.Mes

	Version int
	mes     int
	año     int
}

func Comparar(ctx context.Context, conn *pgxpool.Pool, req CompararReq) (out Comparacion, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, errors.New("no se definió ID de comitente")
	}
	if req.Empresa == 0 {
		return out, errors.New("no se definió ID de empresa")
	}

	// if req.Mes < 1 || req.Mes > 12 {
	// 	return out, deferror.Validation("mes debe encontrarse entre 1 y 12")
	// }
	// if req.Año < 1991 {
	// 	return out, deferror.Validation("el año ingresado no es correcto")
	// }
	// Leo el archivo
	data, err := base64.StdEncoding.DecodeString(req.Archivo)
	if err != nil {
		return out, errors.Wrap(err, "decoding b64")
	}

	r := bytes.NewBuffer(data)
	switch req.Version {
	case 1:
		out.DeAFIP, err = parseV1(r)
	default:
		// Supongo que es la última
		out.DeAFIP, err = parseV2(r)
	}
	if err != nil {
		return out, errors.Wrap(err, "leyendo CSV")
	}

	req.mes, req.año, err = determinarPeriodos(out.DeAFIP)
	if err != nil {
		return out, errors.Wrap(err, "determinando período")
	}

	// Busco os comprobantes de la contabilidad
	out.Propios, err = traerComps(ctx, conn, req)
	if err != nil {
		return out, errors.Wrap(err, "trayendo comprobantes contabilizados")
	}

	out, err = comparar(out.DeAFIP, out.Propios)
	if err != nil {
		return out, errors.Wrap(err, "comparando")
	}

	out.Periodo = fmt.Sprintf("%v/%v", req.mes, req.año)
	return
}

// Compara el CSV que se descarga desde el servicio 'Portal IVA', IVA Compras
// con los datos que están cargados al sistema para ese mismo mes.
func CompararLibroIVA(ctx context.Context, conn *pgxpool.Pool, req CompararReq) (out Comparacion, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, errors.New("no se definió ID de comitente")
	}
	if req.Empresa == 0 {
		return out, errors.New("no se definió ID de empresa")
	}

	// Leo el archivo
	data, err := base64.StdEncoding.DecodeString(req.Archivo)
	if err != nil {
		return out, errors.Wrap(err, "decoding b64")
	}

	r := bytes.NewBuffer(data)
	out.DeAFIP, err = parseCompras(r)
	if err != nil {
		return out, errors.Wrap(err, "leyendo CSV")
	}

	if req.Mes.Zero() {
		return out, errors.Errorf("no se ingresó período a comparar")
	}
	req.mes = req.Mes.MesDelAño()
	req.año = req.Mes.Año()

	// Busco os comprobantes de la contabilidad
	out.Propios, err = traerComps(ctx, conn, req)
	if err != nil {
		return out, errors.Wrap(err, "trayendo comprobantes contabilizados")
	}

	out, err = comparar(out.DeAFIP, out.Propios)
	if err != nil {
		return out, errors.Wrap(err, "comparando")
	}

	out.Periodo = fmt.Sprintf("%v/%v", req.mes, req.año)
	return
}

func traerComps(ctx context.Context, conn *pgxpool.Pool, req CompararReq) (out []Propio, err error) {

	desde := fecha.NewFechaFromInts(req.año, req.mes, 1)
	hasta := desde.AgregarMeses(1)
	hasta = hasta.AgregarDias(-1)

	m := map[uuid.UUID]Propio{}
	ids := []string{}
	{ // Traigo las cabeceras
		query := `SELECT id, fecha_original, n_identificacion, afip_comprobante_id, punto_de_venta, n_comp,
	persona_nombre 
	FROM ops
	WHERE 
		comitente=$1 AND 
		empresa=$2 AND 
		fecha >= $3 AND 
		fecha <=$4 AND 
		comp_id IN (SELECT id FROM comps WHERE compra_o_venta = 'C' AND va_al_libro_de_iva=TRUE AND comitente = $1);`
		rows, err := conn.Query(ctx, query, req.Comitente, req.Empresa, desde, hasta)
		if err != nil {
			return out, deferror.DB(err, "querying ops")
		}
		defer rows.Close()

		for rows.Next() {
			p := Propio{}
			err = rows.Scan(&p.OpID, &p.Fecha, &p.CUIT, &p.TipoComp, &p.PuntoVenta, &p.NComp, &p.Denominacion)
			if err != nil {
				return out, deferror.DB(err, "scanning ops")
			}
			ids = append(ids, "'"+p.OpID.String()+"'")
			m[p.OpID] = p
		}
	}

	{ // Traigo las partidas
		query := fmt.Sprintf(`SELECT op_id, concepto_iva, SUM(monto) 
	FROM partidas
	WHERE op_id IN (%v) AND concepto_iva IS NOT NULL
	GROUP BY op_id, concepto_iva`, strings.Join(ids, ","))
		rows, err := conn.Query(ctx, query)
		if err != nil {
			return out, deferror.DB(err, "querying partidas")
		}
		defer rows.Close()
		for rows.Next() {
			id := uuid.UUID{}
			concepto := iva.Concepto(0)
			montoInt := 0
			err = rows.Scan(&id, &concepto, &montoInt)
			if err != nil {
				return out, deferror.DB(err, "escaneando partidas")
			}
			monto := dec.D2(montoInt)

			anterior := m[id]
			switch concepto {
			case iva.BaseImponible:
				anterior.Neto += monto

			case iva.IVA:
				anterior.IVA += monto

			case iva.NoGravado:
				anterior.NoGravado += monto

			case iva.Exento:
				anterior.Exento += monto
			}
			anterior.Total += monto
			m[id] = anterior
		}
	}

	for _, v := range m {
		out = append(out, v)
	}

	sort.Slice(out, func(i, j int) bool {
		if out[i].Denominacion < out[j].Denominacion {
			return true
		}
		if out[i].Denominacion > out[j].Denominacion {
			return true
		}
		return out[i].Fecha < out[j].Fecha
	})

	return
}

type Key struct {
	cuit       cuits.CUIT
	tipoComp   int
	puntoVenta int
	nComp      int
}

func determinarPeriodos(pp []Recibido) (mes, año int, err error) {

	m := map[fecha.Fecha]struct{}{}
	for i, v := range pp {
		if !v.Fecha.IsValid() {
			return 0, 0, errors.Errorf("renglón %v no tenía fecha", i+2)
		}
		m[v.Fecha] = struct{}{}
	}

	out := []fecha.Fecha{}
	for k := range m {
		out = append(out, k)
	}
	sort.Slice(out, func(i, j int) bool {
		return out[i] < out[j]
	})
	desde := out[0]
	hasta := out[len(out)-1]

	if desde.Año() != hasta.Año() {
		return 0, 0, deferror.Validation("en el archivo había comprobantes de otros períodos")
	}
	if desde.Mes() != hasta.Mes() {
		return 0, 0, deferror.Validation("en el archivo había comprobantes de otros períodos")
	}
	mes = desde.Mes()
	año = desde.Año()
	return
}

func comparar(rr []Recibido, oo []Propio) (out Comparacion, err error) {

	out.FaltanEnAFIP = []Propio{}
	out.FaltanContabilizar = []Recibido{}
	out.DeAFIP = []Recibido{}
	out.Propios = []Propio{}

	// Recibidos
	af := map[Key]Recibido{}
	for _, v := range rr {
		k := Key{
			cuit:       cuits.CUIT(v.NumeroEmisor),
			tipoComp:   v.Tipo,
			puntoVenta: v.PuntoDeVenta,
			nComp:      v.NumeroDesde,
		}
		af[k] = v
	}

	// Propios
	pr := map[Key]Propio{}
	for _, v := range oo {
		if v.CUIT == nil {
			return out, errors.Errorf("El comprobante %v-%v de %v no tiene definido CUIT", v.PuntoVenta, v.NComp, v.Denominacion)
		}
		if v.TipoComp == nil {
			return out, errors.Errorf("El comprobante %v-%v de %v no tiene definido tipo de comprobante", v.PuntoVenta, v.NComp, v.Denominacion)
		}
		k := Key{
			cuit:       *v.CUIT,
			tipoComp:   *v.TipoComp,
			puntoVenta: v.PuntoVenta,
			nComp:      v.NComp,
		}
		pr[k] = v
	}

	faltanContabilizar := map[Key]Recibido{}
	diferencias := map[Key][]Diferencia{}
	sobran := map[Key]Propio{}

	estabEnAmbos := map[Key]struct{}{}

	// Falta contabilizar algún comprobante?
	for k, v := range af {

		// Está contabilizado?
		_, ok := pr[k]
		if !ok {
			faltanContabilizar[k] = v
			continue
		}
		estabEnAmbos[k] = struct{}{}
	}

	// Contabilizados de más
	for k, v := range pr {

		// Está en AFIP?
		_, ok := af[k]
		if !ok {
			sobran[k] = v
		}
		estabEnAmbos[k] = struct{}{}
	}

	for k := range estabEnAmbos {
		p := af[k]
		v := pr[k]

		// Coinciden los valores?
		if p.Neto != v.Neto {
			anterior := diferencias[k]
			dif := Diferencia{Campo: "Neto", Dif: p.Neto - v.Neto}
			anterior = append(anterior, dif)
			diferencias[k] = anterior
		}
		if p.NoGravado != v.NoGravado {
			anterior := diferencias[k]
			dif := Diferencia{Campo: "No gravado", Dif: p.NoGravado - v.NoGravado}
			anterior = append(anterior, dif)
			diferencias[k] = anterior
		}
		if p.Exento != v.Exento {
			anterior := diferencias[k]
			dif := Diferencia{Campo: "Exento", Dif: p.Exento - v.Exento}
			anterior = append(anterior, dif)
			diferencias[k] = anterior
		}
		if p.IVA != v.IVA {
			anterior := diferencias[k]
			dif := Diferencia{Campo: "IVA", Dif: p.IVA - v.IVA}
			anterior = append(anterior, dif)
			diferencias[k] = anterior
		}
		if p.Total != v.Total {
			anterior := diferencias[k]
			dif := Diferencia{Campo: "Total", Dif: p.IVA - v.IVA}
			anterior = append(anterior, dif)
			diferencias[k] = anterior
		}
	}

	for _, v := range faltanContabilizar {
		out.FaltanContabilizar = append(out.FaltanContabilizar, v)
	}
	for _, v := range sobran {
		out.FaltanEnAFIP = append(out.FaltanEnAFIP, v)
	}

	sort.Slice(out.FaltanContabilizar, func(i, j int) bool {
		if out.FaltanContabilizar[i].Denominacion < out.FaltanContabilizar[j].Denominacion {
			return true
		}
		if out.FaltanContabilizar[i].Denominacion > out.FaltanContabilizar[j].Denominacion {
			return true
		}
		return out.FaltanContabilizar[i].Fecha > out.FaltanContabilizar[j].Fecha
	})

	sort.Slice(out.FaltanEnAFIP, func(i, j int) bool {
		if out.FaltanEnAFIP[i].Denominacion < out.FaltanEnAFIP[j].Denominacion {
			return true
		}
		if out.FaltanEnAFIP[i].Denominacion > out.FaltanEnAFIP[j].Denominacion {
			return true
		}
		return out.FaltanEnAFIP[i].Fecha < out.FaltanEnAFIP[j].Fecha
	})
	return
}
