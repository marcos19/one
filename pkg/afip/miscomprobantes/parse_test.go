package miscomprobantes

import (
	"os"
	"testing"

	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestParseRecibidosV1(t *testing.T) {
	f, err := os.Open("testdata/miscomprobantes.csv")
	require.Nil(t, err)

	out, err := parseV1(f)
	require.Nil(t, err)

	assert.Len(t, out, 176)

	r := out[0]
	assert.Equal(t, fecha.Fecha(20211201), r.Fecha)
	assert.Equal(t, 1, r.Tipo)
	assert.Equal(t, 4, r.PuntoDeVenta)
	assert.Equal(t, 117094, r.NumeroDesde)
	assert.Equal(t, 0, r.NumeroHasta)
	assert.Equal(t, "AFIANZADORA LATINOAMERICANA COMPAÑIA DE SEGUROS SA", r.Denominacion)
	assert.Equal(t, dec.NewD4(1), r.TipoDeCambio)
	assert.Equal(t, "$", r.Moneda)
	assert.Equal(t, dec.NewD2(3185.06), r.Neto)
	assert.Equal(t, dec.NewD2(35.04), r.NoGravado)
	assert.Equal(t, dec.NewD2(0), r.Exento)
	assert.Equal(t, dec.NewD2(668.86), r.IVA)
	assert.Equal(t, dec.NewD2(3995.67), r.Total)
}

func TestParseRecibidosV2(t *testing.T) {
	f, err := os.Open("testdata/miscomprobantesv2.csv")
	require.Nil(t, err)

	out, err := parseV2(f)
	require.Nil(t, err)

	assert.Len(t, out, 5)

	r := out[0]
	assert.Equal(t, fecha.Fecha(20230901), r.Fecha)
	assert.Equal(t, 1, r.Tipo)
	assert.Equal(t, 1, r.PuntoDeVenta)
	assert.Equal(t, 112, r.NumeroDesde)
	assert.Equal(t, 112, r.NumeroHasta)
	assert.Equal(t, "PAIZAL JULIAN", r.Denominacion)
	assert.Equal(t, dec.NewD4(1), r.TipoDeCambio)
	assert.Equal(t, "PES", r.Moneda)
	assert.Equal(t, dec.NewD2(300000), r.Neto)
	assert.Equal(t, dec.NewD2(0), r.NoGravado)
	assert.Equal(t, dec.NewD2(0), r.Exento)
	assert.Equal(t, dec.NewD2(63000), r.IVA)
	assert.Equal(t, dec.NewD2(363000), r.Total)
}
