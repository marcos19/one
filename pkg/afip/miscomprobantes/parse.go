package miscomprobantes

import (
	"encoding/csv"
	"io"
	"strconv"
	"strings"
	"time"

	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/dimchansky/utfbom"
)

type Recibido struct {
	Fecha         fecha.Fecha
	Tipo          int
	PuntoDeVenta  int
	NumeroDesde   int
	NumeroHasta   int
	CAE           int
	TipoDocEmisor string
	NumeroEmisor  int
	Denominacion  string
	TipoDeCambio  dec.D4
	Moneda        string
	Neto          dec.D2
	NoGravado     dec.D2
	Exento        dec.D2
	OtrosTributos dec.D2
	IVA           dec.D2
	Total         dec.D2
}

// Lee el archivo CSV descargado de AFIP, servicio Mis Comprobantes -> Comprobantes recibidos
func parseV1(r io.Reader) (out []Recibido, err error) {

	// El formato del archivo es UTF-8 with BOM
	// Este reader lo deja UTF-8 común
	sr, _ := utfbom.Skip(r)

	reader := csv.NewReader(sr)
	records, err := reader.ReadAll()
	if err != nil {
		return out, errors.Wrap(err, "leyendo CSV file")
	}

	for i, record := range records {
		if i == 0 {
			continue
		}
		r, err := parseRecordV1(record)
		if err != nil {
			return out, errors.Wrapf(err, "parseando renglón %v", i+1)
		}
		out = append(out, r)
	}

	return
}

// Lee el archivo CSV descargado de AFIP, servicio Mis Comprobantes -> Comprobantes recibidos
// Arrancó con este formato en 10/2023
func parseV2(r io.Reader) (out []Recibido, err error) {

	// El formato del archivo es UTF-8 with BOM
	// Este reader lo deja UTF-8 común
	sr, _ := utfbom.Skip(r)

	reader := csv.NewReader(sr)
	reader.Comma = ';'
	records, err := reader.ReadAll()
	if err != nil {
		return out, errors.Wrap(err, "leyendo CSV file")
	}

	for i, record := range records {
		if i == 0 {
			continue
		}
		r, err := parseRecordV2(record)
		if err != nil {
			return out, errors.Wrapf(err, "parseando renglón %v", i+1)
		}
		out = append(out, r)
	}

	return
}

func parseRecordV1(vv []string) (r Recibido, err error) {

	// Fecha
	r.Fecha, err = fecha.NewFechaFromLayout("02/01/2006", vv[0])
	if err != nil {
		return r, errors.Wrapf(err, "leyendo fecha")
	}

	{ // Codigo
		id := strings.Split(vv[1], "-")
		if len(id) == 0 {
			return r, errors.Errorf("leyendo código comp")
		}
		id[0] = strings.Trim(id[0], " ")
		r.Tipo, err = strconv.Atoi(id[0])
		if err != nil {
			return r, errors.Wrapf(err, "convirtiendo a número tipo de comprobante %v", id[0])
		}
		if len(id) == 0 {
			return r, errors.Errorf("leyendo código comp, '%v' no era número")
		}
	}

	{ // Punto de venta
		r.PuntoDeVenta, err = strconv.Atoi(vv[2])
		if err != nil {
			return r, errors.Wrap(err, "leyendo punto de venta")
		}
	}

	{ // Número desde
		r.NumeroDesde, err = strconv.Atoi(vv[3])
		if err != nil {
			return r, errors.Wrap(err, "leyendo número desde")
		}
	}

	// Número hasta
	if vv[4] != "" {
		r.NumeroHasta, err = strconv.Atoi(vv[4])
		if err != nil {
			return r, errors.Wrap(err, "leyendo número hasta")

		}
	}

	{ // CAE
		r.CAE, err = strconv.Atoi(vv[5])
		if err != nil {
			return r, errors.Wrap(err, "leyendo CAE")
		}
	}

	r.TipoDocEmisor = vv[6]

	{ // CAE
		r.NumeroEmisor, err = strconv.Atoi(vv[7])
		if err != nil {
			return r, errors.Wrap(err, "leyendo numero de documento emisor")
		}
	}

	r.Denominacion = vv[8]

	{ // Tipo de cambio
		fl, err := strconv.ParseFloat(vv[9], 64)
		if err != nil {
			return r, errors.Wrap(err, "convirtiendo a número tipo de cambio")
		}
		r.TipoDeCambio = dec.NewD4(fl)
	}

	r.Moneda = vv[10]

	// Neto
	if vv[11] != "" {
		fl, err := strconv.ParseFloat(vv[11], 64)
		if err != nil {
			return r, errors.Wrap(err, "convirtiendo a número el neto")
		}
		r.Neto = dec.NewD2(fl)
	}

	// No gravado
	if vv[12] != "" {
		fl, err := strconv.ParseFloat(vv[12], 64)
		if err != nil {
			return r, errors.Wrap(err, "convirtiendo a número el no gravado")
		}
		r.NoGravado = dec.NewD2(fl)
	}

	// Exentas
	if vv[13] != "" {
		fl, err := strconv.ParseFloat(vv[13], 64)
		if err != nil {
			return r, errors.Wrap(err, "convirtiendo a número el exento")
		}
		r.Exento = dec.NewD2(fl)
	}

	// IVA
	if vv[14] != "" {
		fl, err := strconv.ParseFloat(vv[14], 64)
		if err != nil {
			return r, errors.Wrap(err, "convirtiendo a número el IVA")
		}
		r.IVA = dec.NewD2(fl)
	}

	// Total
	if vv[15] != "" {
		fl, err := strconv.ParseFloat(vv[15], 64)
		if err != nil {
			return r, errors.Wrap(err, "convirtiendo a número el total")
		}
		r.Total = dec.NewD2(fl)
	}

	return
}

func parseRecordV2(vv []string) (r Recibido, err error) {

	// Fecha
	r.Fecha, err = fecha.NewFechaFromLayout(time.DateOnly, vv[0])
	if err != nil {
		return r, errors.Wrapf(err, "leyendo fecha")
	}

	{ // Codigo
		id := strings.Split(vv[1], "-")
		if len(id) == 0 {
			return r, errors.Errorf("leyendo código comp")
		}
		id[0] = strings.Trim(id[0], " ")
		r.Tipo, err = strconv.Atoi(id[0])
		if err != nil {
			return r, errors.Wrapf(err, "convirtiendo a número tipo de comprobante %v", id[0])
		}
		if len(id) == 0 {
			return r, errors.Errorf("leyendo código comp, '%v' no era número")
		}
	}

	{ // Punto de venta
		r.PuntoDeVenta, err = strconv.Atoi(vv[2])
		if err != nil {
			return r, errors.Wrap(err, "leyendo punto de venta")
		}
	}

	{ // Número desde
		r.NumeroDesde, err = strconv.Atoi(vv[3])
		if err != nil {
			return r, errors.Wrap(err, "leyendo número desde")
		}
	}

	// Número hasta
	if vv[4] != "" {
		r.NumeroHasta, err = strconv.Atoi(vv[4])
		if err != nil {
			return r, errors.Wrap(err, "leyendo número hasta")

		}
	}

	{ // CAE
		r.CAE, err = strconv.Atoi(vv[5])
		if err != nil {
			return r, errors.Wrap(err, "leyendo CAE")
		}
	}

	r.TipoDocEmisor = vv[6]

	{ // CAE
		r.NumeroEmisor, err = strconv.Atoi(vv[7])
		if err != nil {
			return r, errors.Wrap(err, "leyendo numero de documento emisor")
		}
	}

	r.Denominacion = vv[8]

	{ // Tipo de cambio
		vv[9] = strings.ReplaceAll(vv[9], ",", ".")
		fl, err := strconv.ParseFloat(vv[9], 64)
		if err != nil {
			return r, errors.Wrap(err, "convirtiendo a número tipo de cambio")
		}
		r.TipoDeCambio = dec.NewD4(fl)
	}

	r.Moneda = vv[10]

	// Neto
	if vv[11] != "" {
		vv[11] = strings.ReplaceAll(vv[11], ",", ".")
		fl, err := strconv.ParseFloat(vv[11], 64)
		if err != nil {
			return r, errors.Wrap(err, "convirtiendo a número el neto")
		}
		r.Neto = dec.NewD2(fl)
	}

	// No gravado
	if vv[12] != "" {
		vv[12] = strings.ReplaceAll(vv[12], ",", ".")
		fl, err := strconv.ParseFloat(vv[12], 64)
		if err != nil {
			return r, errors.Wrap(err, "convirtiendo a número el no gravado")
		}
		r.NoGravado = dec.NewD2(fl)
	}

	// Exentas
	if vv[13] != "" {
		vv[13] = strings.ReplaceAll(vv[13], ",", ".")
		fl, err := strconv.ParseFloat(vv[13], 64)
		if err != nil {
			return r, errors.Wrap(err, "convirtiendo a número el exento")
		}
		r.Exento = dec.NewD2(fl)
	}

	// Otros tributos
	if vv[14] != "" {
		vv[14] = strings.ReplaceAll(vv[14], ",", ".")
		fl, err := strconv.ParseFloat(vv[14], 64)
		if err != nil {
			return r, errors.Wrap(err, "convirtiendo a número el IVA")
		}
		r.OtrosTributos = dec.NewD2(fl)
	}

	// IVA
	if vv[15] != "" {
		vv[15] = strings.ReplaceAll(vv[15], ",", ".")
		fl, err := strconv.ParseFloat(vv[15], 64)
		if err != nil {
			return r, errors.Wrap(err, "convirtiendo a número el IVA")
		}
		r.IVA = dec.NewD2(fl)
	}

	// Total
	if vv[16] != "" {
		vv[16] = strings.ReplaceAll(vv[16], ",", ".")
		fl, err := strconv.ParseFloat(vv[16], 64)
		if err != nil {
			return r, errors.Wrap(err, "convirtiendo a número el total")
		}
		r.Total = dec.NewD2(fl)
	}

	return
}
