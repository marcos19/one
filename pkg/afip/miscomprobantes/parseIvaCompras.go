package miscomprobantes

import (
	"encoding/csv"
	"io"
	"strconv"
	"strings"

	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/pkg/errors"
)

// Lee el CSV que se descarga desde el servicio 'Portal IVA', IVA Compras
func parseCompras(r io.Reader) (out []Recibido, err error) {
	reader := csv.NewReader(r)
	reader.LazyQuotes = true
	reader.Comma = ';'
	rows, err := reader.ReadAll()
	if err != nil {
		return out, errors.Wrap(err, "leyendo CSV")
	}

	for rowIdx, row := range rows {
		if rowIdx == 0 {
			continue
		}
		r, err := parseRow(row)
		if err != nil {
			return out, errors.Wrapf(err, "leyendo renglón %v", rowIdx)
		}
		out = append(out, r)
	}
	return
}

func parseRow(row []string) (r Recibido, err error) {

	r.Fecha, err = fecha.NewFecha(row[0])
	if err != nil {
		return r, errors.Wrap(err, "leyendo fecha emisión")
	}

	r.Tipo, err = strconv.Atoi(row[1])
	if err != nil {
		return r, errors.Wrap(err, "leyendo tipo comp")
	}

	r.PuntoDeVenta, err = strconv.Atoi(row[2])
	if err != nil {
		return r, errors.Wrap(err, "leyendo punto de venta")
	}

	r.NumeroDesde, err = strconv.Atoi(row[3])
	if err != nil {
		return r, errors.Wrap(err, "leyendo ncomp")
	}

	r.TipoDocEmisor = row[4]

	r.NumeroEmisor, err = strconv.Atoi(row[5])
	if err != nil {
		return r, errors.Wrap(err, "leyendo n doc vendedor")
	}

	r.Denominacion = row[6]

	{ // Importe total
		r.Total, err = fl(row[7])
		if err != nil {
			return r, errors.Wrap(err, "leyendo importe total")
		}
	}
	if row[10] != "" { // No gravado
		r.NoGravado, err = fl(row[10])
		if err != nil {
			return r, errors.Wrap(err, "leyendo no gravado")
		}
	}
	if row[11] != "" { // Exento
		r.Exento, err = fl(row[11])
		if err != nil {
			return r, errors.Wrap(err, "leyendo importe exento")
		}
	}
	// { // Otros impuestos nacionales
	// 	r.OtrosImpNacionales, err = fl(row[13])
	// 	if err != nil {
	// 		return out, errors.Wrap(err, "leyendo otros impuestos nacionales")
	// 	}
	// }
	// { // Percepciones ISIB
	// 	r.PercepcionesISIB, err = fl(row[14])
	// 	if err != nil {
	// 		return out, errors.Wrap(err, "leyendo percepciones ISIB")
	// 	}
	// }
	// { // Percepciones IVA
	// 	r.PercepcionesIVA, err = fl(row[16])
	// 	if err != nil {
	// 		return out, errors.Wrap(err, "leyendo percepciones IVA")
	// 	}
	// }
	// { // Impuestos internos
	// 	r.ImpuestosInternos, err = fl(row[17])
	// 	if err != nil {
	// 		return out, errors.Wrap(err, "leyendo impuestos internos")
	// 	}
	// }
	// { // Otros tributos
	// 	r.OtrosTributos, err = fl(row[18])
	// 	if err != nil {
	// 		return out, errors.Wrap(err, "leyendo impuestos internos")
	// 	}
	// }
	if row[30] != "" { // Neto gravado
		r.Neto, err = fl(row[30])
		if err != nil {
			return r, errors.Wrap(err, "leyendo neto gravado")
		}
	}
	if row[31] != "" { // IVA
		r.IVA, err = fl(row[31])
		if err != nil {
			return r, errors.Wrap(err, "leyendo IVA")
		}
	}
	return
}

func fl(in string) (out dec.D2, err error) {
	in = strings.Replace(in, ",", ".", 1)
	fl, err := strconv.ParseFloat(in, 64)
	if err != nil {
		return
	}
	return dec.NewD2(fl), nil

}
