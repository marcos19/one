package cert

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/afip/cert/core"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"github.com/cockroachdb/errors"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Handler struct {
	conn          *pgxpool.Pool
	empresaGetter empresas.Getter
	entorno       string
}

func NewHandler(conn *pgxpool.Pool, empresaGetter empresas.Getter, entorno string) (out *Handler, err error) {
	if conn == nil {
		return out, errors.Errorf("conn no puede ser nil")
	}
	if empresaGetter == nil {
		return out, errors.Errorf("empresa getter no puede ser nil")
	}
	if entorno == "" {
		return out, errors.Errorf("entorno no definido")
	}

	out = &Handler{
		conn:          conn,
		empresaGetter: empresaGetter,
		entorno:       entorno,
	}
	return
}

func (h *Handler) GenerarCSR(ctx context.Context, req GenerarCSRReq) (csr string, err error) {
	csr, err = GenerarCSR(ctx, req, h.empresaGetter)
	if err != nil {
		return csr, errors.Wrap(err, "generando CSR")
	}
	return
}

func (h *Handler) Certificados(ctx context.Context, comitente int) (out []CertAFIP, err error) {
	out, err = certificados(ctx, h.conn, comitente, h.entorno)
	if err != nil {
		return out, errors.Wrap(err, "buscando certificados")
	}
	return
}
func (h *Handler) Certificado(ctx context.Context, req CertificadoReq) (out CertAFIP, err error) {
	out, err = certificadoPorID(ctx, h.conn, req)
	if err != nil {
		return out, errors.Wrap(err, "buscando certificado")
	}
	return
}

func (h *Handler) CertificadoPorEmpresa(ctx context.Context, req CertificadoPorEmpresaReq) (out CertAFIP, err error) {
	out, err = CertificadoPorEmpresa(ctx, req, h.conn, h.entorno)
	if err != nil {
		return out, errors.Wrap(err, "buscando certificado por empresa")
	}
	return
}
func (h *Handler) DeleteCertificado(ctx context.Context, req DeleteCertificadoReq) (err error) {
	err = deleteCertificado(ctx, h.conn, req)
	if err != nil {
		return errors.Wrap(err, "borrando certificado")
	}
	return
}

func (h *Handler) SubirCertificado(ctx context.Context, req CertAFIP) (err error) {
	err = subirCertificado(ctx, h.conn, req, h.entorno)
	if err != nil {
		return errors.Wrap(err, "subiendo certificado")
	}
	return
}

type GenerarCSRReq struct {
	Comitente int
	Empresa   int `json:",string"`
	// Nombre    string // Lo pongo en campo de OrganizationalUnitName

}

func GenerarCSR(ctx context.Context, req GenerarCSRReq, empresaGetter empresas.Getter) (csr string, err error) {

	// Valido
	if req.Comitente == 0 {
		return csr, deferror.Validation("no se ingresó ID de comitente")
	}
	if req.Empresa == 0 {
		return csr, deferror.Validation("no se ingresó ID de empresa")
	}
	if req.Empresa == 0 {
		return csr, deferror.Validation("no se ingresó ID de empresa")
	}
	empresa, err := empresaGetter.ReadOne(ctx, empresas.ReadOneReq{ID: req.Empresa, Comitente: req.Comitente})
	if err != nil {
		return csr, errors.Wrap(err, "buscando empresa")
	}

	// Busco la private key de la empresa
	key, err := empresaGetter.PrivateKey(ctx, empresas.PrivateKeyReq{
		Comitente: req.Comitente,
		Empresa:   req.Empresa,
	})
	if err != nil {
		return csr, err
	}

	csr, err = core.GenerarCSR(empresa.Nombre, empresa.CUIT, key)
	if err != nil {
		return csr, errors.Wrap(err, "generando CSR")
	}

	return
}
