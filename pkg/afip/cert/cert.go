package cert

import "time"

// CertAFIP guarda los certificados generados por AFIP luego de presentado el CSR.
type CertAFIP struct {
	ID        *int `json:",string"`
	Comitente int
	Empresa   int `json:",string"`

	Alias   string
	Desde   time.Time
	Hasta   time.Time
	Entorno string

	// CommonName string // Es el alias que pide AFIP

	CertificadoBase64 string
	Certificado       []byte
	CreatedAt         *time.Time
	UpdatedAt         *time.Time
}

func (w *CertAFIP) TableName() string {
	return "afip_certificados"
}
