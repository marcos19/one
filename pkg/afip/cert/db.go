package cert

import (
	"context"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Trae el primero que encuentra no vencido.
func CertificadoPorEmpresa(ctx context.Context, req CertificadoPorEmpresaReq, conn *pgxpool.Pool, entorno string) (out CertAFIP, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, errors.New("no se ingresó ID de comitente")
	}
	if req.Empresa == 0 {
		return out, deferror.Validation("no se ingresó ID de empresa")
	}
	if entorno != "Testing" && entorno != "Producción" {
		return out, errors.Errorf("no se determinó entorno")
	}

	query := `SELECT id, comitente, empresa, alias, desde, hasta, certificado, created_at, updated_at 
	FROM afip_certificados
	WHERE comitente=$1 AND empresa=$2 AND entorno=$3 AND desde<$4 AND hasta>$4`
	err = conn.QueryRow(ctx, query, req.Comitente, req.Empresa, entorno, time.Now()).Scan(
		&out.ID, &out.Comitente, &out.Empresa, &out.Alias, &out.Desde, &out.Hasta, &out.Certificado, &out.CreatedAt, &out.UpdatedAt)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}

	return
}

type CertificadoReq struct {
	ID        int `json:",string"`
	Comitente int
}

func certificadoPorID(ctx context.Context, conn *pgxpool.Pool, req CertificadoReq) (out CertAFIP, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}
	if req.ID == 0 {
		return out, deferror.Validation("no se ingresó ID de certificado")
	}

	query := `SELECT id, comitente, empresa, alias, desde, hasta, certificado, created_at, updated_at 
		FROM afip_certificados
		WHERE comitente=$1 AND id=$2`

	err = conn.
		QueryRow(ctx, query, req.Comitente, req.ID).
		Scan(&out.ID, &out.Comitente, &out.Empresa, &out.Alias, &out.Desde, &out.Hasta, &out.Certificado, &out.CreatedAt, &out.UpdatedAt)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}

	return
}

// Devuelve un listado de todos los certificados del comitente
func certificados(ctx context.Context, conn *pgxpool.Pool, comitente int, entorno string) (out []CertAFIP, err error) {

	// Valido
	if entorno != "Testing" && entorno != "Producción" {
		return out, errors.Errorf("no se determinó entorno (%v)", entorno)
	}
	if comitente == 0 {
		return out, errors.Errorf("no se ingresó ID de comitente")
	}

	query := `SELECT id, comitente, empresa, alias, desde, hasta, created_at, updated_at 
		FROM afip_certificados
		WHERE comitente=$1 AND entorno=$2
		ORDER BY empresa`
	rows, err := conn.Query(ctx, query, comitente, entorno)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		v := CertAFIP{}
		err = rows.Scan(&v.ID, &v.Comitente, &v.Empresa, &v.Alias, &v.Desde, &v.Hasta, &v.CreatedAt, &v.UpdatedAt)
		if err != nil {
			return out, deferror.DB(err, "scanning")
		}
		out = append(out, v)
	}
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}

	return
}

type DeleteCertificadoReq struct {
	Comitente int
	ID        int `json:",string"`
}

func deleteCertificado(ctx context.Context, conn *pgxpool.Pool, req DeleteCertificadoReq) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}
	if req.ID == 0 {
		return deferror.Validation("no se ingresó ID de certificado")
	}

	query := `DELETE FROM afip_certificados WHERE comitente=$1 AND id=$2`
	_, err = conn.Exec(ctx, query, req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "borando certificado")
	}

	return
}

type CertificadoPorEmpresaReq struct {
	Comitente int
	Empresa   int `json:",string"`
	// Todos     bool // Trae los vencidos
}

func subirCertificado(ctx context.Context, conn *pgxpool.Pool, req CertAFIP, entorno string) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}
	if req.Empresa == 0 {
		return deferror.Validation("no se ingresó ID de empresa	")
	}
	if req.Alias == "" {
		return deferror.Validation("no se ingresó campo Alias")
	}
	if len(req.CertificadoBase64) == 0 {
		return deferror.Validation("no se ingresó certificado")
	}
	if entorno != "Testing" && entorno != "Producción" {
		return errors.Errorf("no se determinó entorno")
	}

	// Convierto a bytes
	req.Certificado, err = base64.StdEncoding.DecodeString(req.CertificadoBase64)
	if err != nil {
		return errors.Wrap(err, "leyendo certificado B64")
	}

	// Parseo
	block, _ := pem.Decode(req.Certificado)
	cert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		return errors.Wrap(err, "parseando certificado")
	}
	req.Desde = cert.NotBefore
	req.Hasta = cert.NotAfter
	req.Entorno = entorno

	query := `INSERT INTO afip_certificados (comitente, empresa, alias, desde, hasta, certificado, created_at, entorno)
	VALUES ($1,$2,$3,$4,$5,$6,$7,$8)`

	_, err = conn.Exec(ctx, query, req.Comitente, req.Empresa, req.Alias, req.Desde, req.Hasta, req.Certificado, req.CreatedAt, req.Entorno)
	if err != nil {
		return errors.Wrap(err, "inserting")
	}

	return
}
