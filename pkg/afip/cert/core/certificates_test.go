package core

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParsearCertificado(t *testing.T) {
	require := require.New(t)

	file, err := os.Open("./testdata/testing.pem")
	require.Nil(err)

	_, err = ParsearCertificado(file)
	require.Nil(err)
}

func TestParsearPrivateKey(t *testing.T) {
	require := require.New(t)

	file, err := os.Open("./testdata/privatekey")
	require.Nil(err)

	_, err = ParsearPrivateKey(file)
	require.Nil(err)
}
