package core

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"io"

	"github.com/cockroachdb/errors"
)

func ParsearCertificado(r io.Reader) (out *x509.Certificate, err error) {

	by, err := io.ReadAll(r)
	if err != nil {
		return out, errors.Wrap(err, "leyendo bytes de certificado")
	}

	block, _ := pem.Decode(by)
	out, err = x509.ParseCertificate(block.Bytes)
	if err != nil {
		return out, errors.Wrap(err, "parseando certificado")
	}
	return
}

func ParsearPrivateKey(r io.Reader) (out *rsa.PrivateKey, err error) {

	keyBytes, err := io.ReadAll(r)
	if err != nil {
		return out, errors.Wrap(err, "leyendo bytes de private key")
	}

	block, _ := pem.Decode(keyBytes)
	out, err = x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		return out, errors.Wrap(err, "parseando private key")
	}
	return
}

func GenerarCSR(empresaNombre string, empresaCUIT int, key *rsa.PrivateKey) (out string, err error) {

	// Cargo datos
	cert := x509.CertificateRequest{
		Subject: pkix.Name{
			Country:      []string{"AR"},
			Organization: []string{empresaNombre},
			SerialNumber: fmt.Sprintf("CUIT %v", empresaCUIT),
			CommonName:   "CrossLogic",
			// OrganizationalUnit: []string{"CrossLogic"},
		},
	}

	// Genero CSR
	csrBytes, err := x509.CreateCertificateRequest(rand.Reader, &cert, key)
	if err != nil {
		return out, errors.Wrap(err, "creando CSR")
	}

	// AFIP lo pide en formato PEM
	block := &pem.Block{
		Type:  "CERTIFICATE REQUEST",
		Bytes: csrBytes,
	}
	var buf bytes.Buffer
	err = pem.Encode(&buf, block)
	if err != nil {
		errors.Wrap(err, "encoding PEM")
	}

	out = buf.String()
	return
}
