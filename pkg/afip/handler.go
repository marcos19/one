package afip

import (
	"context"
	"sync"
	"time"

	"bitbucket.org/marcos19/one/pkg/afip/afipmodels"
	"bitbucket.org/marcos19/one/pkg/afip/libroiva"
	"bitbucket.org/marcos19/one/pkg/afip/libroiva/actpdf"
	"bitbucket.org/marcos19/one/pkg/afip/libroiva/actxlsx"
	"bitbucket.org/marcos19/one/pkg/afip/miscomprobantes"
	"bitbucket.org/marcos19/one/pkg/afip/wsaa"
	aacore "bitbucket.org/marcos19/one/pkg/afip/wsaa/core"
	"bitbucket.org/marcos19/one/pkg/afip/wsinsc"
	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"bitbucket.org/marcos19/one/pkg/geo"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/fecha"
	"github.com/crosslogic/niler"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/jung-kurt/gofpdf"
	"github.com/xuri/excelize/v2"
)

// Handler es la estructura que recibe los request y emite los responses
type Handler struct {
	conn           *pgxpool.Pool
	Store          *MapStore
	wsaaStore      *wsaa.Store
	empresasGetter empresas.Getter
	cuentas        cuentas.ReaderOne
	entorno        aacore.Entorno
}

type AfipArgs struct {
	Conn           *pgxpool.Pool
	WSAAStore      *wsaa.Store
	EmpresasGetter empresas.Getter
	Cuentas        cuentas.ReaderOne
	Entorno        aacore.Entorno
}

// NewHandler instancia el handler de productos.
func NewHandler(args AfipArgs) (h *Handler, err error) {
	// Valido
	if args.EmpresasGetter == nil {
		return h, deferror.Validation("EmpresasGetter no puede ser nil")
	}
	if args.Conn == nil {
		return h, deferror.Validation("Conn no puede ser nil")
	}
	if args.Cuentas == nil {
		return h, deferror.Validation("CuentasHandler no puede ser nil")
	}
	if args.WSAAStore == nil {
		return h, deferror.Validation("WSAAStore no puede ser nil")
	}
	if niler.IsNil(args.Cuentas) {
		return h, deferror.Validation("cuentas.ReaderOne no puede ser nil")
	}
	if args.Entorno == "" {
		return h, deferror.Validation("WSAAStore no puede ser nil")
	}

	ctx := context.Background()

	h = &Handler{
		empresasGetter: args.EmpresasGetter,
		conn:           args.Conn,
		cuentas:        args.Cuentas,
		wsaaStore:      args.WSAAStore,
		entorno:        args.Entorno,
	}

	h.Store = &MapStore{}

	{ // Comprobantes
		cc, err := ReadComprobantesAFIP(ctx, h.conn)
		if err != nil {
			return h, errors.Wrap(err, "cargando comprobantes AFIP al cache")
		}
		h.Store.comps = map[int]afipmodels.ComprobanteAFIP{}
		h.Store.muComps = &sync.RWMutex{}
		h.Store.muComps.Lock()

		for _, v := range cc {
			h.Store.comps[v.ID] = v
		}
		h.Store.muComps.Unlock()
	}

	{ // Identificaciones
		cc, err := ReadIdentificacionesAFIP(ctx, h.conn)
		if err != nil {
			return h, errors.Wrap(err, "cargando identificaciones AFIP al cache")
		}
		h.Store.identificaciones = map[int]afipmodels.Identificacion{}
		h.Store.muIden = &sync.RWMutex{}
		h.Store.muIden.Lock()

		for _, v := range cc {
			h.Store.identificaciones[v.ID] = v
		}
		h.Store.muIden.Unlock()
	}

	{ // Condiciones fiscales
		h.Store.condicionesIVA = map[afipmodels.CondicionIVA]CondicionFiscalFacturacion{}
		h.Store.muCond = &sync.RWMutex{}
		h.Store.muCond.Lock()
		for _, v := range condicionesFacturacion {
			h.Store.condicionesIVA[v.ID] = v
		}
		h.Store.muCond.Unlock()
	}

	return
}

type LibroIVAReq struct {
	Comitente int
	Empresa   int `json:",string"`
	Desde     fecha.Fecha
	Hasta     fecha.Fecha
	TipoLibro string
}

func (h *Handler) LibroIVAPDF(ctx context.Context, req LibroIVAReq) (out *gofpdf.Fpdf, err error) {

	e, err := h.empresasGetter.ReadOne(ctx, empresas.ReadOneReq{
		Comitente: req.Comitente,
		ID:        req.Empresa,
	})
	if err != nil {
		return out, errors.Wrap(err, "buscando empresa")
	}

	r := libroiva.Req{
		Comitente: req.Comitente,
		Empresa:   e,
		Desde:     req.Desde,
		Hasta:     req.Hasta,
		TipoLibro: req.TipoLibro,
	}

	// Genero libro
	libro, err := libroiva.NewLibroIVA(ctx, h.conn, r)
	if err != nil {
		return
	}

	return actpdf.PDF(libro)
}

func (h *Handler) LibroIVAExcel(ctx context.Context, req LibroIVAReq) (out *excelize.File, err error) {

	e, err := h.empresasGetter.ReadOne(ctx, empresas.ReadOneReq{
		Comitente: req.Comitente,
		ID:        req.Empresa,
	})
	if err != nil {
		return out, errors.Wrap(err, "buscando empresa")
	}

	r := libroiva.Req{
		Comitente: req.Comitente,
		Empresa:   e,
		Desde:     req.Desde,
		Hasta:     req.Hasta,
		TipoLibro: req.TipoLibro,
	}

	// Genero libro
	libro, err := libroiva.NewLibroIVA(ctx, h.conn, r)
	if err != nil {
		return
	}

	return actxlsx.XLSX(libro)
}

type ConstanciaInscripcionReq struct {
	Comitente int
	Empresa   int `json:",string"`
	CUIT      int `json:",string"`
}

// Devuelve la constancia de inscripión de un expoliado
func (h *Handler) ConstanciaInscripcion(ctx context.Context, req ConstanciaInscripcionReq) (out wsinsc.Constancia, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, errors.Errorf("no se ingresó ID de comitente")
	}
	if req.Empresa == 0 {
		return out, errors.Errorf("no se ingresó ID de empresa representada")
	}
	if req.CUIT == 0 {
		return out, errors.Errorf("no se ingresó CUIT a buscar")
	}

	// Busco CUIT de empresa
	emp, err := h.empresasGetter.ReadOne(ctx, empresas.ReadOneReq{Comitente: req.Comitente, ID: req.Empresa})
	if err != nil {
		return out, errors.Wrap(err, "buscando empresa")
	}

	// Busco ticket
	cred, err := h.wsaaStore.GetTicket(ctx, req.Comitente, req.Empresa, wsinsc.ServiceName)
	if err != nil {
		return out, errors.Wrap(err, "obteniendo ticket de acceso WSAA")
	}

	// Pido constancia
	out, err = wsinsc.ActionGetPersona(emp.CUIT, req.CUIT, cred)
	if err != nil {
		return out, errors.Wrap(err, "solicitando constancia")
	}

	// Pongo mi ID de provincia
	pr, err := geo.ProvinciasIDAfip(ctx, h.conn, out.DatosGenerales.DomicilioFiscal.IDProvincia)
	if err != nil {
		return out, errors.Wrapf(err, "buscando provincia %v", out.DatosGenerales.DomicilioFiscal.IDProvincia)
		// Dejo pasar
	}
	out.DatosGenerales.DomicilioFiscal.IDProvincia = pr.IDAfip

	return
}

// Es un DTO para la consulta del frontend sobre tickets vigentes
type Ticket struct {
	EmpresaID     int `json:",string"`
	EmpresaNombre string
	CUIT          int
	Servicio      string
	Entorno       string
	Expiracion    time.Time
	Found         bool
}

func (h *Handler) MisComprobantes(ctx context.Context, req miscomprobantes.CompararReq) (out miscomprobantes.Comparacion, err error) {
	return miscomprobantes.Comparar(ctx, h.conn, req)
}

func (h *Handler) CompararIVACompras(ctx context.Context, req miscomprobantes.CompararReq) (out miscomprobantes.Comparacion, err error) {
	return miscomprobantes.CompararLibroIVA(ctx, h.conn, req)
}

// HandleComps devuelve el listado de comprobantes definidos por AFIP
func (h *Handler) Comps(ctx context.Context) (out []afipmodels.ComprobanteAFIP, err error) {
	out, err = ReadComprobantesAFIP(ctx, h.conn)
	if err != nil {
		return out, errors.Wrap(err, "buscando comprobantes AFIP")
	}
	return
}

// HandleComps devuelve el listado de comprobantes definidos por AFIP
func (h *Handler) Comp(ctx context.Context, id int) (out afipmodels.ComprobanteAFIP, err error) {
	out, err = ReadComprobanteAFIP(ctx, h.conn, id)
	if err != nil {
		return out, errors.Wrap(err, "buscando comprobante AFIP")
	}
	return
}

// HandleIdentificaciones devuelve el listado de alícuotas habilitados
func (h *Handler) Identificaciones(ctx context.Context) (out []afipmodels.Identificacion, err error) {

	out, err = ReadIdentificacionesAFIP(ctx, h.conn)
	if err != nil {
		return out, errors.Wrap(err, "buscando identificaciones AFIP")
	}
	return
}

func (h *Handler) IdentificacionPorID(ctx context.Context, id int) (out afipmodels.Identificacion, err error) {
	out, err = ReadIdentificacionAFIP(ctx, h.conn, id)
	if err != nil {
		return out, errors.Wrap(err, "buscando identificacion AFIP")
	}
	return
}
