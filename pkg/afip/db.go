package afip

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/afip/afipmodels"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/jackc/pgx/v4/pgxpool"
)

func ReadComprobantesAFIP(ctx context.Context, conn *pgxpool.Pool) (out []afipmodels.ComprobanteAFIP, err error) {

	rows, err := conn.Query(ctx, "SELECT id, nombre, letra, punto_venta_obligatorio FROM afip_comprobantes")
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()
	for rows.Next() {
		c := afipmodels.ComprobanteAFIP{}
		err = rows.Scan(&c.ID, &c.Nombre, &c.Letra, &c.PuntoVentaObligatorio)
		if err != nil {
			return out, deferror.DB(err, "escaneando")
		}
		out = append(out, c)
	}
	return
}

func ReadComprobanteAFIP(ctx context.Context, conn *pgxpool.Pool, id int) (out afipmodels.ComprobanteAFIP, err error) {

	err = conn.
		QueryRow(ctx, "SELECT id, nombre, letra, punto_venta_obligatorio FROM afip_comprobantes WHERE id=$1", id).
		Scan(&out.ID, &out.Nombre, &out.Letra, &out.PuntoVentaObligatorio)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}
	return
}

func ReadIdentificacionesAFIP(ctx context.Context, conn *pgxpool.Pool) (out []afipmodels.Identificacion, err error) {

	rows, err := conn.Query(ctx, "SELECT id, nombre FROM afip_identificaciones")
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()
	for rows.Next() {
		c := afipmodels.Identificacion{}
		err = rows.Scan(&c.ID, &c.Nombre)
		if err != nil {
			return out, deferror.DB(err, "escaneando")
		}
		out = append(out, c)
	}
	return
}
func ReadIdentificacionAFIP(ctx context.Context, conn *pgxpool.Pool, id int) (out afipmodels.Identificacion, err error) {

	err = conn.
		QueryRow(ctx, "SELECT id, nombre FROM afip_identificaciones WHERE id=$1", id).
		Scan(&out.ID, &out.Nombre)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}
	return
}
