package afip

import (
	"sync"

	"bitbucket.org/marcos19/one/pkg/afip/afipmodels"
	"github.com/cockroachdb/errors"
)

var condicionesFacturacion = []CondicionFiscalFacturacion{
	{
		ID:           afipmodels.CondicionConsumidorFinal,
		Nombre:       afipmodels.CondicionConsumidorFinal.Nombre(),
		NombrePlural: "Consumidores finales",
	},
	{
		ID:           afipmodels.CondicionMonotributo,
		Nombre:       afipmodels.CondicionMonotributo.Nombre(),
		NombrePlural: "Monotributistas",
	},
	{
		ID:           afipmodels.CondicionExento,
		Nombre:       afipmodels.CondicionExento.Nombre(),
		NombrePlural: "Exentos",
	},
	{
		ID:           afipmodels.CondicionNoInscripto,
		Nombre:       afipmodels.CondicionNoInscripto.Nombre(),
		NombrePlural: "No inscriptos",
	},
	{
		ID:           afipmodels.CondicionResponsableInscripto,
		Nombre:       afipmodels.CondicionResponsableInscripto.Nombre(),
		NombrePlural: "Responsables inscriptos",
	},
}

func CondicionesFacturacion() (out []CondicionFiscalFacturacion) {
	return append(out, condicionesFacturacion...)
}

type MapStore struct {
	muCond         *sync.RWMutex
	condicionesIVA map[afipmodels.CondicionIVA]CondicionFiscalFacturacion

	muIden           *sync.RWMutex
	identificaciones map[int]afipmodels.Identificacion

	muComps *sync.RWMutex
	comps   map[int]afipmodels.ComprobanteAFIP
}

func (m *MapStore) GetComp(id int) (comp afipmodels.ComprobanteAFIP, err error) {
	m.muComps.RLock()
	comp, ok := m.comps[id]
	if !ok {
		return comp, errors.Wrapf(err, "no se encontró el comprobante ID %v", id)
	}
	m.muComps.RUnlock()
	return
}

func (m *MapStore) GetIdentificacion(id int) (iden afipmodels.Identificacion, err error) {
	m.muIden.RLock()
	iden, ok := m.identificaciones[id]
	if !ok {
		return iden, errors.Wrapf(err, "no se encontró la identificación ID %v", id)
	}
	m.muIden.RUnlock()
	return
}

func (m *MapStore) GetCondicionIVA(id afipmodels.CondicionIVA) (cond CondicionFiscalFacturacion, err error) {
	m.muCond.RLock()
	cond, ok := m.condicionesIVA[id]
	if !ok {
		return cond, errors.Errorf("no se encontró la condificion fiscal ID %v", id)
	}
	m.muCond.RUnlock()
	return
}

type CondicionFiscalFacturacion struct {
	ID           afipmodels.CondicionIVA `json:",string"`
	Nombre       string
	NombrePlural string
}
