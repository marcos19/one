//go:build wsaa

package core

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/json"
	"os"
	"testing"

	certcore "bitbucket.org/marcos19/one/pkg/afip/cert/core"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestParse(t *testing.T) {

	var cert *x509.Certificate
	{
		file, err := os.Open("./testdata/testing2.pem")
		assert.Nil(t, err)
		cert, err = certcore.ParsearCertificado(file)
		require.Nil(t, err)
	}

	var privatekey *rsa.PrivateKey
	{
		file, err := os.Open("./testdata/privatekey")
		assert.Nil(t, err)
		privatekey, err = certcore.ParsearPrivateKey(file)
		require.Nil(t, err)
	}

	// Creo ticket
	source := CertificateData{
		CommonName: "marcos2",
		CUIT:       20328896479,
	}

	ticket, err := NewLoginTicketRequest(source, EntornoTesting, "wsfe")
	assert.Nil(t, err)

	credenciales, err := ticket.SolicitarAcceso(cert, privatekey)
	require.Nil(t, err)

	assert.NotEqual(t, "", credenciales.Sign)
	assert.NotEqual(t, "", credenciales.Token)
	assert.NotEqual(t, "", credenciales.CUIT)
	assert.NotEqual(t, "", credenciales.Entorno)

	// spew.Dump(credenciales)
	file, err := os.Create("./testdata/credenciales.json")
	require.Nil(t, err)
	defer file.Close()
	err = json.NewEncoder(file).Encode(&credenciales)
	assert.Nil(t, err)
}
