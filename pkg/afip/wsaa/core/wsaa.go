package core

import (
	"bytes"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/xml"
	"fmt"
	"html"
	"io"
	"net/http"
	"strings"
	"text/template"
	"time"

	"github.com/antchfx/xmlquery"
	"github.com/cockroachdb/errors"
	"github.com/rs/zerolog/log"
	"go.mozilla.org/pkcs7"
)

type LoginTicketRequest struct {
	Source      CertificateData
	destination certificateDataDest
	uniqueID    int64
	generation  string
	expiration  string
	service     string

	entorno Entorno
}

func NewLoginTicketRequest(source CertificateData, entorno Entorno, service string) (t LoginTicketRequest, err error) {

	t.Source = source
	switch entorno {
	case EntornoProduccion:
		t.destination = produccionDestination
	case EntornoTesting:
		t.destination = testingDestination
	default:
		return t, errors.Errorf("el entorno debe ser %v o %v", EntornoProduccion, EntornoTesting)
	}
	t.entorno = entorno

	t.service = service
	ahora := time.Now().Add(-time.Second * 5)
	t.generation = ahora.Format("2006-01-02T15:04:05Z07:00")
	t.expiration = ahora.Add(time.Hour * 12).Format("2006-01-02T15:04:05Z07:00")
	t.uniqueID = ahora.Unix()
	return
}

func (t LoginTicketRequest) SolicitarAcceso(cert *x509.Certificate, privatekey *rsa.PrivateKey) (cred Credenciales, err error) {
	// Creo XML
	xml, err := t.generarXML()
	if err != nil {
		return cred, err
	}

	log.Debug().Msg(xml)

	// Lo firmo
	signed, err := firmarXML(xml, cert, privatekey)
	if err != nil {
		return cred, err
	}

	// Lo paso a B64
	b64 := base64.StdEncoding.EncodeToString(signed)

	// Envío la solicitud
	uri := ""
	switch t.entorno {
	case EntornoProduccion:
		uri = produccionURI
	case EntornoTesting:
		uri = testingURI
	default:
		return cred, errors.Errorf("entorno '%v' inválido", t.entorno)
	}

	cred, err = enviarRequest(b64, uri)
	if err != nil {
		return cred, errors.Wrap(err, "enviando request a "+uri)
	}
	cred.CUIT = t.Source.CUIT
	cred.Entorno = t.entorno
	cred.Servicio = t.service
	return

}

// CertificateData son los datos que se ingresan cuando se genera un certificado.
// No es sensitivo a mayúsculas y minúsculas, pero sí al orden de los campos.
type CertificateData struct {
	CommonName            string
	OrganzationalUnitName string // ou= This attribute contains the name of an organizational unit
	OrganizationName      string // o=
	Country               string // c=
	CUIT                  int
}

// StringSource devuelve el valor que debe dentro de los campos <source> del
// ticket.
func (c CertificateData) String() (str string) {
	partes := []string{}
	partes = append(partes, fmt.Sprintf("serialNumber=CUIT %v,cn=%v", c.CUIT, c.CommonName))

	if c.OrganizationName != "" {
		partes = append(partes, fmt.Sprintf("o=%v", c.OrganizationName))
	}
	if c.OrganzationalUnitName != "" {
		partes = append(partes, fmt.Sprintf("ou=%v", c.OrganzationalUnitName))
	}
	if c.Country != "" {
		partes = append(partes, fmt.Sprintf("c=%v", c.Country))
	}

	return strings.Join(partes, ",")
}

// CertificateDataDest es el string que va en el campo <destination> del ticket.
type certificateDataDest string

// firmarXML crea un SignedData con el XML del ticket.
func firmarXML(xml string, cert *x509.Certificate, privatekey *rsa.PrivateKey) (signed []byte, err error) {

	// Ingreso el ticket
	sd, err := pkcs7.NewSignedData([]byte(xml))
	if err != nil {
		return signed, errors.Wrap(err, "creando LoginTicketRequest")
	}

	// Pongo certificado y private key
	sd.AddSigner(cert, privatekey, pkcs7.SignerInfoConfig{})
	if err != nil {
		return signed, errors.Wrap(err, "creando LoginTicketRequest")
	}

	// Genero el signed message
	signed, err = sd.Finish()
	if err != nil {
		return signed, errors.Wrap(err, "creando LoginTicketRequest")
	}
	return
}

// Credenciales son los datos que devuelve la AFIP luego de aceptado el ticket.
type Credenciales struct {
	Token          string
	Sign           string
	CUIT           int `json:",string"`
	Entorno        Entorno
	ExpirationTime time.Time
	Servicio       string
}

func enviarRequest(b64 string, uri string) (cr Credenciales, err error) {

	// Lo pongo en el sobre
	env := fmt.Sprintf(envelope, b64)
	// fmt.Println(env)

	// Creo request
	buf := bytes.NewBuffer([]byte(env))
	req, err := http.NewRequest(http.MethodPost, uri, buf)
	if err != nil {
		return cr, errors.Wrap(err, "creando request")
	}
	req.Header.Add("SOAPAction", "")

	// Envío
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return cr, errors.Wrap(err, "enviando WSAA request")
	}
	if res.StatusCode != 200 {
		// leo error
		node, err := xmlquery.Parse(res.Body)
		if err != nil {
			return cr, errors.Wrap(err, "al solicitar ticket de acceso. Tampoco se pudo determinar el error")
		}
		return cr, errors.New("al solicitar ticket de acceso a AFIP: " + node.InnerText())
	}

	// Leo respuesta
	data, err := io.ReadAll(res.Body)
	if err != nil {
		return cr, errors.Wrap(err, "leyendo respuesta")
	}
	defer res.Body.Close()

	// Unmarshal
	bodyStr := html.UnescapeString(string(data))
	ticketRes := Envelope{}
	err = xml.Unmarshal([]byte(bodyStr), &ticketRes)
	if err != nil {
		return cr, errors.Wrap(err, "parseando ticket response")
	}

	cr.Token = ticketRes.Body.LoginCmsResponse.LoginCmsReturn.LoginTicketResponse.Credentials.Token
	cr.Sign = ticketRes.Body.LoginCmsResponse.LoginCmsReturn.LoginTicketResponse.Credentials.Sign
	cr.ExpirationTime = ticketRes.Body.LoginCmsResponse.LoginCmsReturn.LoginTicketResponse.Header.ExpirationTime

	return
}

const envelope = `
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:wsaa="http://wsaa.view.sua.dvadac.desein.afip.gov">
	<soapenv:Header/>
	<soapenv:Body>
		<wsaa:loginCms>
			<wsaa:in0>
				%v
			</wsaa:in0>
		</wsaa:loginCms>
	</soapenv:Body>
</soapenv:Envelope>
`

// GenerarXML devuelve el string con todos los datos del ticket.
func (t LoginTicketRequest) generarXML() (str string, err error) {
	data := struct {
		Source      CertificateData
		Destination certificateDataDest
		UniqueID    int64
		Generation  string
		Expiration  string
		Service     string
	}{
		Source:      t.Source,
		Destination: t.destination,
		UniqueID:    t.uniqueID,
		Generation:  t.generation,
		Expiration:  t.expiration,
		Service:     t.service,
	}
	const ticketStruct = `<?xml version="1.0" encoding="UTF-8"?><loginTicketRequest version="1.0"><header><source>{{.Source}}</source><destination>{{.Destination}}</destination><uniqueId>{{.UniqueID}}</uniqueId><generationTime>{{.Generation}}</generationTime><expirationTime>{{.Expiration}}</expirationTime></header><service>{{.Service}}</service></loginTicketRequest>`
	template, err := template.New("TicketRequest").Parse(ticketStruct)
	if err != nil {
		return str, errors.Wrap(err, "generando ticket WSAA")
	}

	doc := &bytes.Buffer{}
	// Replacing the doc from template with actual req values
	err = template.Execute(doc, data)
	if err != nil {
		return str, errors.Wrap(err, "ejecutando template ticket WSAA")
	}
	return doc.String(), nil
}

// const schema = `
// <?xml version="1.0" encoding="UTF­8"?>
// <schema xmlns:xsd="http://www.w3.org/2001/XMLSchema">
// 	<annotation>
// 		<documentation xml:lang="es">
// 			Esquema de Ticket de pedido de acceso a un WSN por parte de un CEE. Nro revision SVN: $Rev:1869 $
// 		</documentation>
// 	</annotation>
// 	<element name="loginTicketRequest" type="loginTicketRequest" />
// 	<complexType name="loginTicketRequest">
// 		<sequence>
// 			<element name="header" type="headerType" minOccurs="1" maxOccurs="1" />
// 			<element name="service" type="serviceType" minOccurs="1" maxOccurs="1" />
// 		</sequence>
// 		<attribute name="version" type="xsd:decimal" use="optional" default="1.0" />
// 	</complexType>
// 	<complexType name="headerType">
// 		<sequence>
// 			<element name="source" type="xsd:string" minOccurs="0" maxOccurs="1" />
// 			<element name="destination" type="xsd:string" minOccurs="0" maxOccurs="1" />
// 			<element name="uniqueId" type="xsd:unsignedInt" minOccurs="1" maxOccurs="1" />
// 			<element name="generationTime" type="xsd:dateTime" minOccurs="1" maxOccurs="1" />
// 			<element name="expirationTime" type="xsd:dateTime" minOccurs="1" maxOccurs="1" />
// 		</sequence>
// 	</complexType>
// 	<simpleType name="serviceType">
// 		<restriction base="xsd:string">
// 			<pattern value="[a­z,A­Z][a­z,A­Z,\­,_,0­9]*" />
// 			<minLength value="3" />
// 			<maxLength value="32" />
// 		</restriction>
// 	</simpleType>
// </schema>

// `
