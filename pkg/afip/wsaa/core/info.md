Para obtener el certificado por primera vez, hay que dar de alta al DN. Para esto hay que presentar una "solicitud de certificado" o "Certificate Signing Request" (CSR).

El CSR se genera en su computadora, usando la herramienta OpenSSL (disponible para Windows, UNIX/Linux y MacOSX). Se hace en dos pasos

    primero hay que generar una clave privada en formato PKCS#10 con un mínimo de 2048 bits

    openssl genrsa -out MiClavePrivada 2048
    y segundo, generar el CSR. Para ello, la forma de ejecutar 'openssl' en la línea de comandos es así:

    openssl req -new -key MiClavePrivada -subj "/C=AR/O=subj_o/CN=subj_cn/serialNumber=CUIT subj_cuit" -out MiPedidoCSR
    openssl req -new -key private2 -subj "/C=AR/O=marcos/CN=facturacion/serialNumber=CUIT 20328896479" -out MiPedidoCSR

donde hay que reemplazar

    MiClavePrivada por nombre del archivo elegido en el primer paso.
    subj_o por el nombre de su empresa
    subj_cn por el nombre de su sistema cliente
    subj_cuit por la CUIT (sólo los 11 dígitos, sin guiones) de la empresa o del programador (persona jurídica)
    MiClavePrivada por el nombre del archivo de la clave privada generado antes
    MiPedidoCSR por el nombre del archivo CSR que se va a crear

Por ejemplo para

    una empresa llamada EmpresaPrueba
    un sistema TestSystem
    el cuit = 20123456789
    con el archivo MiClavePrivada generado en el punto anterior:

openssl req -new -key MiClavePrivada -subj "/C=AR/O=EmpresaPrueba/CN=TestSystem/serialNumber=CUIT 20123456789" -out MiPedidoCSR
Si no hay errores, el archivo 'MiPedidoCSR' será utilizado al momento de obtener el DN y el certificado.
