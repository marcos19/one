package core

type Entorno string

const (
	EntornoTesting    = Entorno("Testing")
	EntornoProduccion = Entorno("Producción")

	testingURI    = "https://wsaahomo.afip.gov.ar/ws/services/LoginCms"
	produccionURI = "https://wsaa.afip.gov.ar/ws/services/LoginCms"

	// Testing son los datos que van en el campo <destination> del ticket
	// para el entorno testing
	testingDestination = certificateDataDest(`CN=wsaahomo, O=AFIP, C=AR, SERIALNUMBER=CUIT 33693450239`)

	// Produccion son los datos que van en el campo <destination> del ticket
	// para el entorno producción.
	produccionDestination = certificateDataDest(`CN=wsaa, O=AFIP, C=AR, SERIALNUMBER=CUIT 33693450239`)
)
