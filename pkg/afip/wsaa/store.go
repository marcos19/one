package wsaa

import (
	"context"
	"crypto/x509"
	"encoding/pem"
	"time"

	"bitbucket.org/marcos19/one/pkg/afip/cert"
	"bitbucket.org/marcos19/one/pkg/afip/wsaa/core"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"github.com/cockroachdb/errors"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Store se encarga de solicitar, guardar en cache y devolver tickets
// para que los use el usuario.
type Store struct {
	// Cache con los tickets descargados
	credsWSAA map[credKey]core.Credenciales

	// Determina si estoy trabajando en testing o producción
	entorno core.Entorno

	conn           *pgxpool.Pool
	empresasGetter empresas.Getter
}

type TicketGetter interface {
	GetTicket(ctx context.Context, comitente, empresa int, servicio string) (core.Credenciales, error)
}

func NewStore(entorno core.Entorno, conn *pgxpool.Pool, empresaGetter empresas.Getter) (out *Store, err error) {

	out = &Store{}
	switch entorno {
	case core.EntornoProduccion, core.EntornoTesting:
		//ok
	default:
		return out, errors.Errorf("entorno %v inválido")
	}
	if conn == nil {
		return out, errors.Wrap(err, "conn puede ser nil")
	}

	if empresaGetter == nil {
		return out, errors.Errorf("empresa getter no puede ser nil")
	}
	out.entorno = entorno
	out.conn = conn
	out.empresasGetter = empresaGetter
	out.credsWSAA = map[credKey]core.Credenciales{}
	return
}

type credKey struct {
	Comitente int
	Empresa   int
	Servicio  string
}

// Devuelve el ticket, si está vencido, o no hay ninguno pide uno nuevo.
func (h *Store) GetTicket(ctx context.Context, comitente, empresa int, servicio string) (out core.Credenciales, err error) {

	// Tengo credencial en cache?
	out, hayCredenciales := h.credsWSAA[credKey{Comitente: comitente, Empresa: empresa, Servicio: servicio}]
	estáVencida := out.ExpirationTime.Before(time.Now())

	if !hayCredenciales || estáVencida {
		// Pido ticket de acceso a AFIP
		err = h.solicitarTicketAcceso(ctx, comitente, empresa, servicio)
		if err != nil {
			return out, errors.Wrap(err, "no había ticket, intentando solicitar uno nuevo")
		}
	}

	// Traigo el ticket
	out, ok := h.credsWSAA[credKey{
		Comitente: comitente,
		Empresa:   empresa,
		Servicio:  servicio,
	}]
	if !ok {
		return out, errors.Wrap(err, "no había ticket")
	}
	return
}

// Devuelve el listado de tickets disponibles que tiene el comitente
func (h *Store) Tickets(comitente int) (out []core.Credenciales, err error) {

	for k, v := range h.credsWSAA {
		if k.Comitente != comitente {
			continue
		}

		// Busco credencial
		// empresa, err := h.empresasGetter.ReadOne(empresas.ReadOneReq{ID: k.Empresa, Comitente: k.Comitente})
		// if err != nil {
		// 	return nil, errors.Wrap(err, "buscando empresa")
		// }

		// t := Ticket{
		// 	EmpresaID:     *empresa.ID,
		// 	EmpresaNombre: empresa.Nombre,
		// 	CUIT:          empresa.CUIT,
		// 	Entorno:       string(core.Entorno),
		// 	Expiracion:    v.ExpirationTime,
		// 	Found:         ok,
		// }

		out = append(out, v)
	}

	// // Tickets de factura electrónica
	// for k, v := range h.credsWSFE {
	// 	if k.Comitente == comitente {
	// 		out = append(out, v)
	// 	}
	// }
	return
}
func (h *Store) solicitarTicketAcceso(ctx context.Context, comitente, empresa int, servicio string) (err error) {

	// Valido
	if comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}
	if empresa == 0 {
		return deferror.Validation("no se ingresó ID de empresa")
	}
	if servicio == "" {
		return deferror.Validation("no se ingresó el servicio")
	}

	// Busco certificado
	certReg, err := cert.CertificadoPorEmpresa(ctx, cert.CertificadoPorEmpresaReq{
		Comitente: comitente,
		Empresa:   empresa,
	}, h.conn, string(h.entorno))
	if err != nil {
		return errors.Wrap(err, "buscando certificado parar la empresa")
	}

	if len(certReg.Certificado) == 0 {
		return errors.Errorf("no se encontró certificado")
	}
	// Parseo
	block, _ := pem.Decode(certReg.Certificado)
	cert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		return errors.Wrap(err, "parseando certificado")
	}

	// Busco private key
	key, err := h.empresasGetter.PrivateKey(ctx, empresas.PrivateKeyReq{
		Comitente: comitente,
		Empresa:   empresa,
	})
	if err != nil {
		return errors.Wrap(err, "buscando key")
	}
	// Busco empresa
	emp, err := h.empresasGetter.ReadOne(ctx, empresas.ReadOneReq{
		Comitente: comitente,
		ID:        empresa,
	})
	if err != nil {
		return errors.Wrap(err, "buscando empresa")
	}

	// Creo solicitud de ticket
	data := core.CertificateData{
		CommonName: certReg.Alias,
		CUIT:       emp.CUIT,
	}
	tiquetReq, err := core.NewLoginTicketRequest(data, h.entorno, servicio)
	if err != nil {
		return errors.Wrap(err, "generando solicitud de ticket")
	}

	// Solicito acceso
	tiquet, err := tiquetReq.SolicitarAcceso(cert, key)
	if err != nil {
		return errors.Wrap(err, "solicitando acceso")
	}

	// Guardo el ticket
	h.credsWSAA[credKey{Comitente: comitente, Empresa: empresa, Servicio: servicio}] = tiquet

	return
}
