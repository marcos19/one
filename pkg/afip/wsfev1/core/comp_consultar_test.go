//go:build ws

package core

import (
	"encoding/json"
	"os"
	"testing"

	aa "bitbucket.org/marcos19/one/pkg/afip/wsaa/core"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestCompConsultar(t *testing.T) {

	// Leo ticket
	file, err := os.Open(credencialesPath)
	require.Nil(t, err)
	cred := aa.Credenciales{}
	err = json.NewDecoder(file).Decode(&cred)
	require.Nil(t, err)

	comp := Comp{
		NComp:      8,
		Tipo:       1,
		PuntoVenta: 1,
	}
	resp, err := ActionFECompConsultar(comp, cred)
	require.Nil(t, err)

	habiaDatos := resp.Body.FECompConsultarResponse.FECompConsultarResult.ResultGet.Resultado != ""
	noHabiaRegistros := false

	if len(resp.Body.FECompConsultarResponse.FECompConsultarResult.Errors) > 0 {
		if resp.Body.FECompConsultarResponse.FECompConsultarResult.Errors[0].Code == 602 {
			noHabiaRegistros = true
		}
	}

	assert.True(t, habiaDatos || noHabiaRegistros)

	// spew.Dump(resp)
}
