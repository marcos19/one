package core

import (
	"encoding/xml"
	"fmt"
	"html"
	"io"
	"math"
	"net/http"
	"strings"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/iva"
	"bitbucket.org/marcos19/one/pkg/ops"

	aa "bitbucket.org/marcos19/one/pkg/afip/wsaa/core"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/davecgh/go-spew/spew"
)

// SolcitarCAE busca la op en la base de datos, genera la solicitud y le pega
// los resultados en la tabla ops.
func SolicitarCAE(op ops.Op, inscriptoIVA bool, cred aa.Credenciales) (resp FECAESolicitarResponse, err error) {

	// Genero el XML
	req, err := GenerarSolicitud(op, inscriptoIVA)
	if err != nil {
		return resp, errors.Wrapf(err, "autorizando %v %v ", op.CompNombre, op.NCompStr)
	}

	// Llamo al web service
	resp, err = ActionFECAESolicitar(req, cred)
	if err != nil {
		return resp, errors.Wrapf(err, "solicitando CAE de %v %v", op.CompNombre, op.NCompStr)
	}
	return
}

// GenerarSolicitud transforma una Op en una solicitud de autorización
func GenerarSolicitud(op ops.Op, inscriptoIVA bool) (req FeCAEReq, err error) {

	req.FeCabReq.CantReg = 1
	req.FeCabReq.PtoVta = op.PuntoDeVenta

	if op.AFIPComprobanteID == nil {
		spew.Dump(op)
		return req, errors.Errorf("no tiene definido código de comprobante AFIP")
	}
	req.FeCabReq.CbteTipo = *op.AFIPComprobanteID

	req.FeDetReq.FECAEDetRequest.Concepto = ConceptoProductosYServicios

	// Tipo identificación
	if op.TipoIdentificacion == nil {
		return req, errors.Errorf("no tiene definido tipo de identificación de persona")
	}
	req.FeDetReq.FECAEDetRequest.DocTipo = *op.TipoIdentificacion

	// Número identificación
	if op.NIdentificacion == nil {
		return req, errors.Errorf("no tiene definido número de identificación de persona")
	}
	req.FeDetReq.FECAEDetRequest.DocNro = *op.NIdentificacion

	// NComp
	req.FeDetReq.FECAEDetRequest.CbteDesde = op.NComp
	req.FeDetReq.FECAEDetRequest.CbteHasta = op.NComp

	// Fecha
	req.FeDetReq.FECAEDetRequest.CbteFch = Fech(op.FechaOriginal)
	if op.TotalComp == nil {
		return req, errors.Errorf("no tiene definido total comprobante")
	}

	// Importes totales
	req.FeDetReq.FECAEDetRequest.ImpTotal = Double((*op.TotalComp).Float())
	req.FeDetReq.FECAEDetRequest.ImpTotConc = impTotConc(op.PartidasContables, inscriptoIVA)
	req.FeDetReq.FECAEDetRequest.ImpNeto = impNeto(op.PartidasContables, inscriptoIVA)
	req.FeDetReq.FECAEDetRequest.ImpOpEx = impOpEx(op.PartidasContables)
	req.FeDetReq.FECAEDetRequest.ImpIVA = impIva(op.PartidasContables)
	req.FeDetReq.FECAEDetRequest.ImpTrib = impTributos(op.PartidasContables)

	// Si ImpTrib es igual a 0 el objeto Tributos y Tributo no deben informarse. (10024)
	if req.FeDetReq.FECAEDetRequest.ImpTrib == 0 {
		req.FeDetReq.FECAEDetRequest.Tributos = nil
	}

	// Moneda
	req.FeDetReq.FECAEDetRequest.MonID, err = Moneda(op.TipoMoneda)
	if err != nil {
		return req, err
	}
	// Cotización
	switch op.TipoMoneda {
	case "PES", "":
		req.FeDetReq.FECAEDetRequest.MonCotiz = Cotizacion(1)
	default:
		req.FeDetReq.FECAEDetRequest.MonCotiz = Cotizacion(op.TipoCambio.Float())
	}

	// Alic
	req.FeDetReq.FECAEDetRequest.Iva, err = determinarIVA(op.PartidasContables)
	if err != nil {
		return req, errors.Wrap(err, "determinando alicuotas de IVA")
	}

	// Fecha servicio desde
	switch req.FeDetReq.FECAEDetRequest.Concepto {
	case ConceptoServicios, ConceptoProductosYServicios:
		// Si está definida, pongo la que es
		if op.AfipFields != nil {
			if op.AfipFields.FchServDesde != 0 {
				f := Fech(op.AfipFields.FchServDesde)
				req.FeDetReq.FECAEDetRequest.FchServDesde = &f
				break
			}
		}
		// Pongo fecha de comprobante
		f := req.FeDetReq.FECAEDetRequest.CbteFch
		req.FeDetReq.FECAEDetRequest.FchServDesde = &f
	}

	// Fecha servicio hasta
	switch req.FeDetReq.FECAEDetRequest.Concepto {
	case ConceptoServicios, ConceptoProductosYServicios:
		// Si está definida, pongo la que es
		if op.AfipFields != nil {
			if op.AfipFields.FchServHasta != 0 {
				f := Fech(op.AfipFields.FchServHasta)
				req.FeDetReq.FECAEDetRequest.FchServHasta = &f
				break
			}
		}
		// Pongo fecha de comprobante
		f := req.FeDetReq.FECAEDetRequest.CbteFch
		req.FeDetReq.FECAEDetRequest.FchServHasta = &f
	}

	// Fecha vencimiento
	switch req.FeDetReq.FECAEDetRequest.Concepto {
	case ConceptoServicios, ConceptoProductosYServicios:
		// Si está definida, pongo la que es
		if op.AfipFields != nil {
			if op.AfipFields.FchVtoPago != 0 {
				f := Fech(op.AfipFields.FchServHasta)
				req.FeDetReq.FECAEDetRequest.FchServHasta = &f
				break
			}
		}
		// Pongo fecha de comprobante
		f := req.FeDetReq.FECAEDetRequest.CbteFch
		req.FeDetReq.FECAEDetRequest.FchVtoPago = &f
	}

	// Comprobantes asociados
	// Para notas de débito/crédito.
	// Si no hay comprobantes asociados, tiene que haber período
	// Pongo fecha de la nota de débito
	switch *op.AFIPComprobanteID {
	case 2, 3, 7, 8, 12, 13:
		listo := false
		if op.AfipFields != nil {
			// Había comprobantes asociados
			if len(op.AfipFields.CbtesAsoc) > 0 {
				req.FeDetReq.FECAEDetRequest.CbtesAsoc = &CbtesAsoc{}
				for _, v := range op.AfipFields.CbtesAsoc {
					asoc := CbteAsoc{
						Tipo:    v.Tipo,
						PtoVta:  v.PtoVta,
						Nro:     v.Nro,
						Cuit:    v.Cuit,
						CbteFch: v.CbteFch,
					}
					*req.FeDetReq.FECAEDetRequest.CbtesAsoc = append(*req.FeDetReq.FECAEDetRequest.CbtesAsoc, asoc)
					listo = true
				}
			} else {

				if op.AfipFields.FchServDesde != 0 && op.AfipFields.FchServHasta != 0 {
					// No había comprobantes asociados
					// Había periodos definidos?
					pp := PeriodoAsoc{
						FchDesde: Fech(op.AfipFields.FchServDesde),
						FchHasta: Fech(op.AfipFields.FchServHasta),
					}
					req.FeDetReq.FECAEDetRequest.PeriodoAsoc = &pp
					listo = true
				}
			}
		}

		// Si no hay ningún dato, pongo fechas de factura
		if !listo {
			pp := PeriodoAsoc{
				FchDesde: req.FeDetReq.FECAEDetRequest.CbteFch,
				FchHasta: req.FeDetReq.FECAEDetRequest.CbteFch,
			}
			req.FeDetReq.FECAEDetRequest.PeriodoAsoc = &pp
		}
	}

	return
}

// Conceptos no gravados
func impTotConc(pp []ops.Partida, inscriptoIVA bool) Double {
	if !inscriptoIVA {
		// Se informa todo en el Neto
		return 0
	}
	tot := 0.
	for _, v := range pp {
		if v.AlicuotaIVA == nil {
			continue
		}
		if *v.AlicuotaIVA != iva.IVANoGravado {
			continue
		}

		tot += v.Monto.Float()
	}
	return Double(math.Abs(tot))
}

func impNeto(pp []ops.Partida, inscriptoIVA bool) Double {
	tot := 0.

	// Si el emisor es monotributista o exento
	if !inscriptoIVA {
		for _, v := range pp {
			if v.ConceptoIVA == nil {
				continue
			}
			tot += v.Monto.Float()
		}
		return Double(math.Abs(tot))
	}

	// Si es inscripto en IVA
	for _, v := range pp {
		if v.ConceptoIVA == nil {
			continue
		}
		if *v.ConceptoIVA != iva.BaseImponible {
			continue
		}
		switch *v.AlicuotaIVA {
		case 1, 2:
			// Exento y no gravado
			continue
		}
		tot += v.Monto.Float()
	}
	return Double(math.Abs(tot))
}

func impOpEx(pp []ops.Partida) Double {
	tot := 0.
	for _, v := range pp {
		if v.ConceptoIVA == nil {
			continue
		}
		if *v.ConceptoIVA != iva.Exento {
			continue
		}
		tot += v.Monto.Float()
	}
	return Double(math.Abs(tot))
}

func impIva(pp []ops.Partida) Double {
	tot := 0.
	for _, v := range pp {
		if v.ConceptoIVA == nil {
			continue
		}
		if *v.ConceptoIVA != iva.IVA {
			continue
		}
		tot += v.Monto.Float()
	}
	return Double(math.Abs(tot))
}

func impTributos(pp []ops.Partida) Double {
	tot := 0.
	for _, v := range pp {
		if v.ConceptoIVA == nil {
			continue
		}
		if *v.ConceptoIVA < 30 {
			continue
		}
		tot += v.Monto.Float()
	}
	return Double(math.Abs(tot))
}

func determinarIVA(pp []ops.Partida) (aa *AlicsIva, err error) {

	base := map[iva.Alicuota]dec.D2{}
	imp := map[iva.Alicuota]dec.D2{}

	for _, v := range pp {
		if v.ConceptoIVA == nil {
			continue
		}
		switch *v.ConceptoIVA {

		case iva.BaseImponible:
			if v.AlicuotaIVA == nil {
				return aa, errors.Errorf("la partida %v tiene definida ConceptoIVA pero no AlicuotaIVA", v.ID)
			}
			switch *v.AlicuotaIVA {
			case 1, 2:
				// No gravado y exentos, los ignoro
			default:
				base[*v.AlicuotaIVA] += v.Monto
			}

		case iva.IVA:
			if v.AlicuotaIVA == nil {
				return aa, errors.Errorf("la partida %v tiene definida ConceptoIVA pero no AlicuotaIVA", v.ID)
			}
			imp[*v.AlicuotaIVA] += v.Monto
		}
	}

	if len(base) > 0 {
		aa = &AlicsIva{}
	}

	for k, v := range base {
		a := AlicIva{}
		a.ID = int(k)
		a.BaseImp = Double(math.Abs(v.Float()))
		imp := imp[k]
		a.Importe = Double(math.Abs(imp.Float()))
		*aa = append(*aa, a)
	}

	return
}

func ActionFECAESolicitar(req FeCAEReq, cred aa.Credenciales) (resp FECAESolicitarResponse, err error) {

	// Agerego el envelope
	enveloped, err := addEnvelope(req, cred)
	if err != nil {
		return resp, err
	}

	// Genero HTTP request
	uri := ""
	switch cred.Entorno {
	case aa.EntornoTesting:
		uri = testingURL
	case aa.EntornoProduccion:
		uri = produccionURL
	}
	r, err := http.NewRequest(http.MethodPost, uri, strings.NewReader(enveloped))
	if err != nil {
		return resp, errors.Wrap(err, "creando request")
	}
	r.Header.Add("Content-Type", "text/xml;charset=UTF-8")
	r.Header.Add("SOAPAction", "http://ar.gov.afip.dif.FEV1/FECAESolicitar")

	// Envío HTTP request
	res, err := http.DefaultClient.Do(r)
	if err != nil {
		return resp, errors.Wrap(err, "realizando HTTP request")
	}

	// Leo respuesta
	data, err := io.ReadAll(res.Body)
	if err != nil {
		return resp, errors.Wrap(err, "leyendo respuesta")
	}

	if res.StatusCode != http.StatusOK {
		return resp, deferror.WebService("%v (%v)", string(data), res.Status)
	}

	// Unmarshal
	bodyStr := html.UnescapeString(string(data))
	err = xml.Unmarshal([]byte(bodyStr), &resp)
	if err != nil {
		return resp, errors.Wrap(err, "parseando FECAESolicitar response")
	}

	return
}

var envelopeStr = `
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ar="http://ar.gov.afip.dif.FEV1/"><soapenv:Header/><soapenv:Body>%v</soapenv:Body></soapenv:Envelope>
`

func addEnvelope(fecaeReq FeCAEReq, cred aa.Credenciales) (out string, err error) {
	request := FECAESolicitar{}
	request.Auth.Cuit = cred.CUIT
	request.Auth.Token = cred.Token
	request.Auth.Sign = cred.Sign
	request.FeCAEReq = fecaeReq

	// Paso a XML
	by, err := xml.Marshal(request)
	if err != nil {
		return out, errors.Wrap(err, "Marshaling FeCAEReq")
	}

	out = fmt.Sprintf(envelopeStr, string(by))
	return
}

type FECAESolicitar struct {
	XMLName xml.Name `xml:"ar:FECAESolicitar"`
	Auth    struct {
		Token string `xml:"ar:Token"`
		Sign  string `xml:"ar:Sign"`
		Cuit  int    `xml:"ar:Cuit"`
	} `xml:"ar:Auth"`
	FeCAEReq FeCAEReq `xml:"ar:FeCAEReq"`
}

type FeCAEReq struct {
	FeCabReq struct {
		CantReg  int `xml:"ar:CantReg"`
		PtoVta   int `xml:"ar:PtoVta"`
		CbteTipo int `xml:"ar:CbteTipo"`
	} `xml:"ar:FeCabReq"`
	FeDetReq struct {
		FECAEDetRequest struct {
			Concepto     Concepto     `xml:"ar:Concepto"`
			DocTipo      int          `xml:"ar:DocTipo"`
			DocNro       int          `xml:"ar:DocNro"`
			CbteDesde    int          `xml:"ar:CbteDesde"`
			CbteHasta    int          `xml:"ar:CbteHasta"`
			CbteFch      Fech         `xml:"ar:CbteFch"`
			ImpTotal     Double       `xml:"ar:ImpTotal"`
			ImpTotConc   Double       `xml:"ar:ImpTotConc"` // No gravado
			ImpNeto      Double       `xml:"ar:ImpNeto"`
			ImpOpEx      Double       `xml:"ar:ImpOpEx"`
			ImpTrib      Double       `xml:"ar:ImpTrib"`
			ImpIVA       Double       `xml:"ar:ImpIVA"`
			FchServDesde *Fech        `xml:"ar:FchServDesde"`
			FchServHasta *Fech        `xml:"ar:FchServHasta"`
			FchVtoPago   *Fech        `xml:"ar:FchVtoPago"`
			MonID        string       `xml:"ar:MonId"`
			MonCotiz     Cotizacion   `xml:"ar:MonCotiz"`
			CbtesAsoc    *CbtesAsoc   `xml:"ar:CbtesAsoc>ar:CbteAsoc,omitempty"`
			Tributos     *Tributos    `xml:"ar:Tributos>ar:Tributo,omitempty"`
			Iva          *AlicsIva    `xml:"ar:Iva>ar:AlicIva"`
			Opcionales   *Opcionales  `xml:"ar:Opcionales>ar:Opcional,omitempty"`
			Compradores  *Compradores `xml:"ar:Compradores>ar:Comprador,omitempty"`
			PeriodoAsoc  *PeriodoAsoc `xml:"ar:PeriodoAsoc,omitempty"`
		} `xml:"ar:FECAEDetRequest"`
	} `xml:"ar:FeDetReq"`
}

// Concepto dice si se trata de producto o servicio.
type Concepto int

// Coceptos definidos por AFIP
const (
	ConceptoProductos           = Concepto(1)
	ConceptoServicios           = Concepto(2)
	ConceptoProductosYServicios = Concepto(3)
)

type CbtesAsoc []CbteAsoc
type CbteAsoc struct {
	Tipo    int    `xml:"ar:Tipo"`
	PtoVta  int    `xml:"ar:PtoVta"`
	Nro     int    `xml:"ar:Nro"`
	Cuit    string `xml:"ar:Cuit,omitempty"`    // Opcional
	CbteFch string `xml:"ar:CbteFch,omitempty"` // Opcional
}

type Tributos []Tributo
type Tributo struct {
	ID      int    `xml:"ar:Id"`
	Desc    string `xml:"ar:Desc"`
	BaseImp Double `xml:"ar:BaseImp"`
	Alic    Double `xml:"ar:Alic"`
	Importe Double `xml:"ar:Importe"`
}

type AlicsIva []AlicIva
type AlicIva struct {
	ID      int    `xml:"ar:Id"`
	BaseImp Double `xml:"ar:BaseImp"`
	Importe Double `xml:"ar:Importe"`
}

type Opcionales []Opcional
type Opcional struct {
	ID    int    `xml:"ar:Id"`
	Valor string `xml:"ar:Valor"`
}

type Compradores []Comprador
type Comprador struct {
	DocTipo    int    `xml:"ar:DocTipo"`
	DocNro     int    `xml:"ar:DocNro"`
	Porcentaje Double `xml:"ar:Porcentaje"`
}

// Lo saqué porque me lo marshalizaba aunque estuviera vacío
type PeriodoAsoc struct {
	FchDesde Fech `xml:"ar:FchDesde,omitempty"`
	FchHasta Fech `xml:"ar:FchHasta,omitempty"`
}
