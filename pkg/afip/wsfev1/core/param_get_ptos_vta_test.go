//go:build ws

package core

import (
	"encoding/json"
	"os"
	"testing"

	aa "bitbucket.org/marcos19/one/pkg/afip/wsaa/core"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestPuntosVta(t *testing.T) {

	// Busco ticket
	file, err := os.Open(credencialesPath)
	require.Nil(t, err)
	cred := aa.Credenciales{}
	err = json.NewDecoder(file).Decode(&cred)
	require.Nil(t, err)

	resp, err := ActionParamGetPtosVenta(cred)
	require.Nil(t, err)

	habiaPtos := len(resp.Body.FEParamGetPtosVentaResponse.FEParamGetPtosVentaResult.ResultGet) > 0

	sinResultados := false
	if len(resp.Body.FEParamGetPtosVentaResponse.FEParamGetPtosVentaResult.Errors) == 1 {
		if resp.Body.FEParamGetPtosVentaResponse.FEParamGetPtosVentaResult.Errors[0].Code == 602 {
			sinResultados = true
		}
	}

	assert.True(t, habiaPtos || sinResultados)
}
