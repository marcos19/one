package core

import (
	"encoding/xml"
	"fmt"

	"github.com/cockroachdb/errors"
	"github.com/crosslogic/fecha"
)

const ServiceName = "wsfe"
const testingURL = "https://wswhomo.afip.gov.ar/wsfev1/service.asmx"
const produccionURL = "https://servicios1.afip.gov.ar/wsfev1/service.asmx"

type Err struct {
	Code int    //`xml:"Code"`
	Msg  string //`xml:"Msg"`
}

type Evt struct {
	Code int    `xml:"Code"`
	Msg  string `xml:"Msg"`
}

// Double se marshaliza con dos decimales
type Double float64

func (d Double) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	return e.EncodeElement(fmt.Sprintf("%.2f", d), start)
}

// Cotización se marshaliza con 6 decimales
type Cotizacion float64

func (d Cotizacion) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	return e.EncodeElement(fmt.Sprintf("%.6f", d), start)
}

// Fech se marshaliza como YYYYMMDD
type Fech fecha.Fecha

func (f Fech) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	fch := fecha.Fecha(f)

	if fch.IsZero() {
		return e.EncodeElement("", start)
	}
	if !fch.IsValid() {
		return errors.Errorf("la fecha %v no es válida", f)
	}

	formateada := fch.Time().Format("20060102")
	return e.EncodeElement(formateada, start)
}

// Bool se marshaliza a S o N
type Bool bool

func (b Bool) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	if b {
		return e.EncodeElement("S", start)
	}
	return e.EncodeElement("N", start)
}

func (b Bool) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	if b {
		return d.DecodeElement("S", &start)

	}
	return d.DecodeElement("N", &start)
}

func Moneda(moneda string) (out string, err error) {
	switch moneda {
	case "", "PES", "ARS":
		return "PES", nil
	default:
		return out, errors.Errorf("no se pudo definir código de moneda %v", moneda)
	}
}
