package core

import (
	"encoding/xml"
	"fmt"
	"html"
	"io"
	"net/http"
	"strings"

	"github.com/cockroachdb/errors"

	aa "bitbucket.org/marcos19/one/pkg/afip/wsaa/core"
)

type CompUltimoAutorizadoRequest struct {
	XMLName xml.Name `xml:"ar:FECompUltimoAutorizado"`
	Auth    struct {
		Token string `xml:"ar:Token"`
		Sign  string `xml:"ar:Sign"`
		Cuit  int    `xml:"ar:Cuit"`
	} `xml:"ar:Auth"`

	PtoVta   int `xml:"ar:PtoVta"`
	CbteTipo int `xml:"ar:CbteTipo"`
}

type CompUltimoAutorizadoResponse struct {
	XMLName xml.Name `xml:"Envelope"`
	Headers struct{} `xml:"Headers"`
	Body    struct {
		FECompUltimoAutorizadoResponse struct {
			FECompUltimoAutorizadoResult struct {
				PtoVta   int
				CbteTipo int
				CbteNro  int
				Errors   []Err `xml:"Errors>Err"`
				Events   []Evt `xml:"Events>Event"`
			}
		}
	} `xml:"Body"`

	Err    error
	Events *string
}

func ActionFECompUltimoAutorizado(ptoVta, cbteTipo int, cred aa.Credenciales) (resp CompUltimoAutorizadoResponse, err error) {
	req := CompUltimoAutorizadoRequest{}
	req.CbteTipo = cbteTipo
	req.PtoVta = ptoVta

	// Completo auth
	req.Auth.Token = cred.Token
	req.Auth.Sign = cred.Sign
	req.Auth.Cuit = cred.CUIT

	// Paso a XML
	by, err := xml.Marshal(req)
	if err != nil {
		return resp, errors.Wrap(err, "Marshaling CompUltimoAutorizadoRequest")
	}

	// Agrego sobre
	enveloped := fmt.Sprintf(envelopeStr, string(by))

	// Genero HTTP request
	uri := ""
	switch cred.Entorno {
	case aa.EntornoTesting:
		uri = testingURL
	case aa.EntornoProduccion:
		uri = produccionURL
	}

	r, err := http.NewRequest(http.MethodPost, uri, strings.NewReader(enveloped))
	if err != nil {
		return resp, errors.Wrap(err, "creando request")
	}
	r.Header.Add("Content-Type", "text/xml;charset=UTF-8")
	r.Header.Add("SOAPAction", "http://ar.gov.afip.dif.FEV1/FECompUltimoAutorizado")

	// Envío HTTP request
	res, err := http.DefaultClient.Do(r)
	if err != nil {
		return resp, errors.Wrap(err, "enviando WSFE.CompUltimoAutorizado request")
	}

	// Leo respuesta
	data, err := io.ReadAll(res.Body)
	if err != nil {
		return resp, errors.Wrap(err, "leyendo respuesta")
	}
	// fmt.Println(string(data))

	// Unmarshal
	bodyStr := html.UnescapeString(string(data))
	err = xml.Unmarshal([]byte(bodyStr), &resp)
	if err != nil {
		// fmt.Println(err)
		// fmt.Println(string(bodyStr))
		return resp, errors.Wrap(err, "unmarshaling response")
	}

	return
}
