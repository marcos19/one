//go:build ws

package core

import (
	"encoding/json"
	"encoding/xml"
	"os"
	"testing"

	aa "bitbucket.org/marcos19/one/pkg/afip/wsaa/core"
	"github.com/davecgh/go-spew/spew"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const credencialesPath = "../../wsaa/core/testdata/credenciales.json"

func TestCAESolicitar(t *testing.T) {

	// Ticket para usar
	// cred := wsaa.Credenciales{
	// 	Token:   "PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/Pgo8c3NvIHZlcnNpb249IjIuMCI+CiAgICA8aWQgc3JjPSJDTj13c2FhaG9tbywgTz1BRklQLCBDPUFSLCBTRVJJQUxOVU1CRVI9Q1VJVCAzMzY5MzQ1MDIzOSIgZHN0PSJDTj13c2ZlLCBPPUFGSVAsIEM9QVIiIHVuaXF1ZV9pZD0iMzk0Mjc2NzE3NiIgZ2VuX3RpbWU9IjE2MDA1NDMyNzciIGV4cF90aW1lPSIxNjAwNTg2NTM3Ii8+CiAgICA8b3BlcmF0aW9uIHR5cGU9ImxvZ2luIiB2YWx1ZT0iZ3JhbnRlZCI+CiAgICAgICAgPGxvZ2luIGVudGl0eT0iMzM2OTM0NTAyMzkiIHNlcnZpY2U9IndzZmUiIHVpZD0iU0VSSUFMTlVNQkVSPUNVSVQgMjAzMjg4OTY0NzksIENOPW1hcmNvczIiIGF1dGhtZXRob2Q9ImNtcyIgcmVnbWV0aG9kPSIyMiI+CiAgICAgICAgICAgIDxyZWxhdGlvbnM+CiAgICAgICAgICAgICAgICA8cmVsYXRpb24ga2V5PSIyMDMyODg5NjQ3OSIgcmVsdHlwZT0iNCIvPgogICAgICAgICAgICA8L3JlbGF0aW9ucz4KICAgICAgICA8L2xvZ2luPgogICAgPC9vcGVyYXRpb24+Cjwvc3NvPgo=",
	// 	Sign:    "c/yTG7nQb9GYu4yT2W9dplZk4K+4XRMgRCKB71/29z4tgDoPcJyRK6XbzBklCPz+hHV/MwQT7FrRo8qN8dwhUAwQSpg0vEtemujh8JdchMiiaSEvBp4ss/M5uXMcnzCmQzCpvV52w8ZErWVtS/J+rHbydHEj7KdFz9Rrpbdebmg=",
	// 	Entorno: wsaa.EntornoTesting,
	// 	CUIT:    20328896479,
	// }

	file, err := os.Open(credencialesPath)
	require.Nil(t, err)
	cred := aa.Credenciales{}
	err = json.NewDecoder(file).Decode(&cred)
	require.Nil(t, err)

	// Busco ultimo comp
	resp, err := ActionFECompUltimoAutorizado(1, 1, cred)
	require.Nil(t, err)
	proximoN := 1 + resp.Body.FECompUltimoAutorizadoResponse.FECompUltimoAutorizadoResult.CbteNro
	r := FeCAEReq{}
	r.FeCabReq.CantReg = 1
	r.FeCabReq.PtoVta = 1
	r.FeCabReq.CbteTipo = 1

	r.FeDetReq.FECAEDetRequest.Concepto = ConceptoProductos
	r.FeDetReq.FECAEDetRequest.DocTipo = 96
	r.FeDetReq.FECAEDetRequest.DocNro = 32889648
	r.FeDetReq.FECAEDetRequest.CbteDesde = proximoN
	r.FeDetReq.FECAEDetRequest.CbteHasta = proximoN
	r.FeDetReq.FECAEDetRequest.CbteFch = Fech(20200919)
	r.FeDetReq.FECAEDetRequest.ImpTotal = 125
	r.FeDetReq.FECAEDetRequest.ImpTotConc = 4
	r.FeDetReq.FECAEDetRequest.ImpNeto = 100
	r.FeDetReq.FECAEDetRequest.ImpIVA = 21
	r.FeDetReq.FECAEDetRequest.Iva = &AlicsIva{
		{
			ID:      5,
			BaseImp: 100,
			Importe: 21,
		},
	}
	r.FeDetReq.FECAEDetRequest.MonID = "PES"
	r.FeDetReq.FECAEDetRequest.MonCotiz = 1
	// Envío solicitud
	response, err := ActionFECAESolicitar(r, cred)
	require.Nil(t, err)

	assert.Len(t, response.Body.FECAESolicitarResponse.FECAESolicitarResult.Errors, 0, spew.Sdump(response.Body.FECAESolicitarResponse.FECAESolicitarResult.Errors))

}

func TestUnmarshalResponseErrors(t *testing.T) {
	str := `
<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
   <soap:Header>
      <FEHeaderInfo xmlns="http://ar.gov.afip.dif.FEV1/">
         <ambiente>HomologacionExterno - efa</ambiente>
         <fecha>2020-09-19T20:03:31.3898538-03:00</fecha>
         <id>4.0.0.0</id>
      </FEHeaderInfo>
   </soap:Header>
   <soap:Body>
      <FECAESolicitarResponse xmlns="http://ar.gov.afip.dif.FEV1/">
         <FECAESolicitarResult>
            <Errors>
               <Err>
                  <Code>500</Code>
                  <Msg>Error interno de aplicación: - Metodo FECAESolicitar</Msg>
               </Err>
            </Errors>
         </FECAESolicitarResult>
      </FECAESolicitarResponse>
   </soap:Body>
</soap:Envelope>
`
	env := FECAESolicitarResponse{}
	err := xml.Unmarshal([]byte(str), &env)
	assert.Nil(t, err)

	// spew.Dump(env)

	assert.Len(t, env.Body.FECAESolicitarResponse.FECAESolicitarResult.Errors, 1)
	assert.NotEqual(t, 0, env.Body.FECAESolicitarResponse.FECAESolicitarResult.Errors[0].Code)
	assert.NotEqual(t, "", env.Body.FECAESolicitarResponse.FECAESolicitarResult.Errors[0].Msg)
}
