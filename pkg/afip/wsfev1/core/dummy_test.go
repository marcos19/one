//go:build ws

package core

import (
	"testing"

	aa "bitbucket.org/marcos19/one/pkg/afip/wsaa/core"
	"github.com/stretchr/testify/assert"
)

func TestDummy(t *testing.T) {
	cred := aa.Credenciales{
		Entorno: aa.EntornoTesting,
	}

	resp, err := ActionFEDummy(cred)
	assert.Nil(t, err)

	assert.NotEqual(t, "", resp.Body.FEDummyResponse.FEDummyResult.AppServer)
	assert.NotEqual(t, "", resp.Body.FEDummyResponse.FEDummyResult.DbServer)
	assert.NotEqual(t, "", resp.Body.FEDummyResponse.FEDummyResult.AuthServer)
}
