package core

import (
	"bytes"
	"encoding/xml"
	"html"
	"io"
	"net/http"

	"github.com/cockroachdb/errors"

	aa "bitbucket.org/marcos19/one/pkg/afip/wsaa/core"
)

type CompConsultarRequest struct {
	XMLName   xml.Name `xml:"soapenv:Envelope"`
	XmlnsSoap string   `xml:"xmlns:soapenv,attr"`
	XmlnsAr   string   `xml:"xmlns:ar,attr"`

	Header struct{} `xml:"soapenv:Header"`
	Body   struct {
		FECompConsultar struct {
			Auth struct {
				Token string `xml:"ar:Token"`
				Sign  string `xml:"ar:Sign"`
				Cuit  int    `xml:"ar:Cuit"`
			} `xml:"ar:Auth"`
			FeCompConsReq struct {
				CbteTipo int `xml:"ar:CbteTipo"`
				CbteNro  int `xml:"ar:CbteNro"`
				PtoVta   int `xml:"ar:PtoVta"`
			} `xml:"ar:FeCompConsReq"`
		} `xml:"ar:FECompConsultar"`
	} `xml:"soapenv:Body"`
}

type CompConsultarResponse struct {
	XMLName xml.Name `xml:"Envelope"`
	Header  struct{}
	Body    struct {
		FECompConsultarResponse struct {
			FECompConsultarResult struct {
				ResultGet struct {
					// Mismos campos de solicitar CAE
					Concepto     Concepto    `xml:"Concepto"`
					DocTipo      int         `xml:"DocTipo"`
					DocNro       int         `xml:"DocNro"`
					CbteDesde    int         `xml:"CbteDesde"`
					CbteHasta    int         `xml:"CbteHasta"`
					CbteFch      Fech        `xml:"CbteFch"`
					ImpTotal     Double      `xml:"ImpTotal"`
					ImpTotConc   Double      `xml:"ImpTotConc"`
					ImpNeto      Double      `xml:"ImpNeto"`
					ImpOpEx      Double      `xml:"ImpOpEx"`
					ImpTrib      Double      `xml:"ImpTrib"`
					ImpIVA       Double      `xml:"ImpIVA"`
					FchServDesde Fech        `xml:"FchServDesde"`
					FchServHasta Fech        `xml:"FchServHasta"`
					FchVtoPago   string      `xml:"FchVtoPago"`
					MonID        string      `xml:"MonId"`
					MonCotiz     Double      `xml:"MonCotiz"`
					CbtesAsoc    []CbteAsoc  `xml:"CbtesAsoc>CbteAsoc,omitempty"`
					Tributos     []Tributo   `xml:"Tributos>Tributo,omitempty"`
					Iva          []AlicIva   `xml:"Iva>AlicIva"`
					Opcionales   []Opcional  `xml:"Opcionales>Opcional,omitempty"`
					Compradores  []Comprador `xml:"Compradores>Comprador,omitempty"`
					PeriodoAsoc  struct {
						FchDesde Fech `xml:"FchDesde"`
						FchHasta Fech `xml:"FchHasta"`
					} `xml:"PeriodoAsoc,omitempty"`

					// Propios de este método
					Resultado       Resultado `xml:"Resultado"`
					CodAutorizacion string    `xml:"CodAutorizacion"`
					EmisionTipo     string    `xml:"EmisionTipo"`
					FchVto          Fech      `xml:"FchVto"`
					FchProceso      string    `xml:"FchProceso"`
					Observaciones   []Obs     `xml:"Observaciones>Obs,omitempty"`
					PtoVta          int       `xml:"PtoVta"`
					CbteTipo        int       `xml:"CbteTipo"`
				} `xml:"ResultGet"`
				Events []Evt `xml:"Events>Evt"`
				Errors []Err `xml:"Errors>Err"`
			} `xml:"FECompConsultarResult"`
		} `xml:"FECompConsultarResponse"`
	} `xml:"Body"`
}
type Comp struct {
	Tipo       int
	PuntoVenta int
	NComp      int
}

func ActionFECompConsultar(comp Comp, cred aa.Credenciales) (resp CompConsultarResponse, err error) {

	// Creo Request
	request := CompConsultarRequest{
		XmlnsSoap: "http://schemas.xmlsoap.org/soap/envelope/",
		XmlnsAr:   "http://ar.gov.afip.dif.FEV1/",
	}
	request.Body.FECompConsultar.Auth.Token = cred.Token
	request.Body.FECompConsultar.Auth.Sign = cred.Sign
	request.Body.FECompConsultar.Auth.Cuit = cred.CUIT
	request.Body.FECompConsultar.FeCompConsReq.CbteTipo = comp.Tipo
	request.Body.FECompConsultar.FeCompConsReq.PtoVta = comp.PuntoVenta
	request.Body.FECompConsultar.FeCompConsReq.CbteNro = comp.NComp

	by, err := xml.Marshal(request)
	if err != nil {
		return resp, errors.Wrap(err, "marshaling request")
	}

	// Genero HTTP request
	uri := ""
	switch cred.Entorno {
	case aa.EntornoTesting:
		uri = testingURL
	case aa.EntornoProduccion:
		uri = produccionURL
	}
	r, err := http.NewRequest(http.MethodPost, uri, bytes.NewBuffer(by))
	if err != nil {
		return resp, errors.Wrap(err, "creando request")
	}
	r.Header.Add("Content-Type", "text/xml;charset=UTF-8")
	r.Header.Add("SOAPAction", "http://ar.gov.afip.dif.FEV1/FECompConsultar")

	// Envío HTTP request
	res, err := http.DefaultClient.Do(r)
	if err != nil {
		return resp, errors.Wrap(err, "enviando WSFE request")
	}
	if res.StatusCode != 200 {
		return resp, errors.Wrap(err, "realizando solicitud HTTP")
	}

	// Leo respuesta
	data, err := io.ReadAll(res.Body)
	if err != nil {
		return resp, errors.Wrap(err, "leyendo respuesta")
	}
	defer res.Body.Close()

	// Unmarshal
	bodyStr := html.UnescapeString(string(data))

	err = xml.Unmarshal([]byte(bodyStr), &resp)
	if err != nil {
		return resp, errors.Wrap(err, "unmarshaling response")
	}

	return
}
