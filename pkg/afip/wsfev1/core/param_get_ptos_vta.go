package core

import (
	"bytes"
	"encoding/xml"
	"html"
	"io"
	"net/http"

	aa "bitbucket.org/marcos19/one/pkg/afip/wsaa/core"
	"github.com/cockroachdb/errors"
)

type ParamGetPtosVentaRequest struct {
	XMLName   xml.Name `xml:"soapenv:Envelope"`
	XmlnsSoap string   `xml:"xmlns:soapenv,attr"`
	XmlnsAr   string   `xml:"xmlns:ar,attr"`

	Header struct{} `xml:"soapenv:Header"`
	Body   struct {
		FEParamGetPtosVenta struct {
			Auth struct {
				Token string `xml:"ar:Token"`
				Sign  string `xml:"ar:Sign"`
				Cuit  int    `xml:"ar:Cuit"`
			} `xml:"ar:Auth"`
		} `xml:"ar:FEParamGetPtosVenta"`
	} `xml:"soapenv:Body"`
}

type ParamGetPtosVentaResponse struct {
	XMLName xml.Name `xml:"Envelope"`
	Header  struct{}
	Body    struct {
		FEParamGetPtosVentaResponse struct {
			FEParamGetPtosVentaResult struct {
				ResultGet []PtoVenta `xml:"ResultGet>PtoVenta"`
				Events    []Evt      `xml:"Events>Evt"`
				Errors    []Err      `xml:"Errors>Err"`
			}
		}
	}
}

type PtoVenta struct {
	Nro         int
	EmisionTipo string
	Bloqueado   Bool
	FchBaja     Fech
}

func ActionParamGetPtosVenta(cred aa.Credenciales) (resp ParamGetPtosVentaResponse, err error) {

	// Creo Request
	request := ParamGetPtosVentaRequest{
		XmlnsSoap: "http://schemas.xmlsoap.org/soap/envelope/",
		XmlnsAr:   "http://ar.gov.afip.dif.FEV1/",
	}
	request.Body.FEParamGetPtosVenta.Auth.Token = cred.Token
	request.Body.FEParamGetPtosVenta.Auth.Sign = cred.Sign
	request.Body.FEParamGetPtosVenta.Auth.Cuit = cred.CUIT
	by, err := xml.Marshal(request)
	if err != nil {
		return resp, errors.Wrap(err, "Marshaling ParamGetPtosVtaRequest")
	}

	// Genero HTTP request
	uri := ""
	switch cred.Entorno {
	case aa.EntornoTesting:
		uri = testingURL
	case aa.EntornoProduccion:
		uri = produccionURL
	}
	r, err := http.NewRequest(http.MethodPost, uri, bytes.NewBuffer(by))
	if err != nil {
		return resp, errors.Wrap(err, "creando request")
	}
	r.Header.Add("Content-Type", "text/xml;charset=UTF-8")
	r.Header.Add("SOAPAction", "http://ar.gov.afip.dif.FEV1/FEParamGetPtosVenta")

	// Envío HTTP request
	res, err := http.DefaultClient.Do(r)
	if err != nil {
		return resp, errors.Wrap(err, "enviando WSFE request")
	}

	// Leo respuesta
	data, err := io.ReadAll(res.Body)
	if err != nil {
		return resp, errors.Wrap(err, "leyendo respuesta")
	}

	// Unmarshal
	bodyStr := html.UnescapeString(string(data))
	err = xml.Unmarshal([]byte(bodyStr), &resp)
	if err != nil {
		return resp, errors.Wrap(err, "unmarshaling response")
	}

	return
}
