//go:build ws

package core

import (
	"encoding/json"
	"os"
	"testing"

	aa "bitbucket.org/marcos19/one/pkg/afip/wsaa/core"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestCompUltimoAutorizado(t *testing.T) {

	// Leo el ultimo ticket
	file, err := os.Open(credencialesPath)
	require.Nil(t, err)
	cred := aa.Credenciales{}
	err = json.NewDecoder(file).Decode(&cred)
	require.Nil(t, err)

	req := CompUltimoAutorizadoRequest{}
	req.PtoVta = 1
	req.CbteTipo = 1

	resp, err := ActionFECompUltimoAutorizado(1, 1, cred)
	require.Nil(t, err)

	assert.NotEqual(t, resp.Body.FECompUltimoAutorizadoResponse.FECompUltimoAutorizadoResult.CbteNro, 0)
	assert.NotEqual(t, resp.Body.FECompUltimoAutorizadoResponse.FECompUltimoAutorizadoResult.CbteTipo, 0)
	assert.NotEqual(t, resp.Body.FECompUltimoAutorizadoResponse.FECompUltimoAutorizadoResult.PtoVta, 0)
	assert.True(t, len(resp.Body.FECompUltimoAutorizadoResponse.FECompUltimoAutorizadoResult.Errors) == 0)
	assert.True(t, len(resp.Body.FECompUltimoAutorizadoResponse.FECompUltimoAutorizadoResult.Events) == 0)
}
