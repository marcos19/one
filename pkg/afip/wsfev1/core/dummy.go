package core

import (
	"encoding/xml"
	"html"
	"io"
	"net/http"
	"strings"

	aa "bitbucket.org/marcos19/one/pkg/afip/wsaa/core"
	"github.com/cockroachdb/errors"
)

type DummyResponse struct {
	XMLName xml.Name `xml:"Envelope"`
	Header  struct{}
	Body    struct {
		FEDummyResponse struct {
			FEDummyResult struct {
				AppServer  string
				DbServer   string
				AuthServer string
			}
		}
	}
}

func ActionFEDummy(cred aa.Credenciales) (resp DummyResponse, err error) {

	// Genero HTTP request
	uri := ""
	switch cred.Entorno {
	case aa.EntornoTesting:
		uri = testingURL
	case aa.EntornoProduccion:
		uri = produccionURL
	}

	const request = `
	<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ar="http://ar.gov.afip.dif.FEV1/">
		<soapenv:Header/>
		<soapenv:Body>
	   		<ar:FEDummy/>
		</soapenv:Body>
 	</soapenv:Envelope>`
	r, err := http.NewRequest(http.MethodPost, uri, strings.NewReader(request))
	if err != nil {
		return resp, errors.Wrap(err, "creando request")
	}
	r.Header.Add("Content-Type", "text/xml;charset=UTF-8")
	r.Header.Add("SOAPAction", "http://ar.gov.afip.dif.FEV1/FEDummy")

	// Envío HTTP request
	res, err := http.DefaultClient.Do(r)
	if err != nil {
		return resp, errors.Wrap(err, "solicitando CAE")
	}

	// Leo respuesta
	data, err := io.ReadAll(res.Body)
	if err != nil {
		return resp, errors.Wrap(err, "leyendo respuesta")
	}
	// fmt.Println("RESPONSE\n", string(data))

	// Unmarshal
	bodyStr := html.UnescapeString(string(data))
	err = xml.Unmarshal([]byte(bodyStr), &resp)
	if err != nil {
		// fmt.Println(err)
		// fmt.Println(string(bodyStr))
		return resp, errors.Wrap(err, "unmarshaling response")
	}

	return
}
