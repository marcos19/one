package core

import "encoding/xml"

type FECAESolicitarResponse struct {
	XMLName xml.Name `xml:"Envelope"`
	Header  struct{} `xml:"Header"`
	Body    struct {
		FECAESolicitarResponse struct {
			FECAESolicitarResult struct {
				FeCabResp struct {
					Cuit       int       `xml:"Cuit"`
					PtoVta     int       `xml:"PtoVta"`
					CbteTipo   int       `xml:"CbteTipo"`
					FchProceso string    `xml:"FchProceso"`
					CantReg    int       `xml:"CatReg"`
					Resultado  Resultado `xml:"Resultado"`
					Reproceso  string    `xml:"Reproceso"`
				}
				FeDetResp struct {
					FECAEDetResponse struct {
						Concepto      int    `xml:"Concepto"`
						DocTipo       int    `xml:"DocTipo"`
						DocNro        int    `xml:"DocNro"`
						CbteDesde     int    `xml:"CbteDesde"`
						CbteHasta     int    `xml:"CbteHasta"`
						Resultado     string `xml:"Resultado"`
						CAE           string `xml:"CAE"`
						CbteFch       Fech   `xml:"CbteFch"`
						CAEFchVto     Fech   `xml:"CAEFchVto"`
						Observaciones []Obs  `xml:"Observaciones>Obs"`
					}
				}
				Events []Evt `xml:"Events>Evt"`
				Errors []Err `xml:"Errors>Err"`
			} `xml:"FECAESolicitarResult"`
		} `xml:"FECAESolicitarResponse"`
	} `xml:"Body"`
}

type Obs struct {
	Code int    `xml:"Code"`
	Msg  string `xml:"Msg"`
}

type Resultado string

const (
	ResultadoAprobado  = "A"
	ResultadoRechazado = "R"
	ResultadoParcial   = "P"
)

func (resp FECAESolicitarResponse) AutorizadoOK() bool {
	return resp.Body.FECAESolicitarResponse.FECAESolicitarResult.FeCabResp.Resultado == ResultadoAprobado
}
