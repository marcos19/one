package wsfev1

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/marcos19/one/pkg/afip/qr"
	"bitbucket.org/marcos19/one/pkg/afip/wsaa"
	"bitbucket.org/marcos19/one/pkg/afip/wsfev1/core"
	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"bitbucket.org/marcos19/one/pkg/ops"
	opshandler "bitbucket.org/marcos19/one/pkg/ops/handler"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/cuits"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/crosslogic/niler"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Handler struct {
	conn          *pgxpool.Pool
	store         wsaa.TicketGetter
	empresaGetter empresas.Getter
	compGetter    comps.ReaderOne
	opsHandler    *opshandler.Handler
}

func NewHandler(conn *pgxpool.Pool, store wsaa.TicketGetter, e empresas.Getter, comp comps.ReaderOne, op *opshandler.Handler) (*Handler, error) {
	if niler.IsNil(conn) {
		return nil, errors.Errorf("conn era nil")
	}
	if niler.IsNil(store) {
		return nil, errors.Errorf("ticket getter era nil")
	}
	if niler.IsNil(e) {
		return nil, errors.Errorf("empresa getter era nil")
	}
	if niler.IsNil(comp) {
		return nil, errors.Errorf("compGetter era nil")
	}
	if niler.IsNil(op) {
		return nil, errors.Errorf("opsReaderOne era nil")
	}

	h := &Handler{
		conn:          conn,
		store:         store,
		empresaGetter: e,
		compGetter:    comp,
		opsHandler:    op,
	}
	return h, nil
}

type SolicitarCAEReq struct {
	OpID      uuid.UUID
	Comitente int
}

func (h *Handler) SolicitarCAE(ctx context.Context, req SolicitarCAEReq) (out core.FECAESolicitarResponse, err error) {
	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se definió ID de comitente")
	}
	if req.OpID == uuid.Nil {
		return out, deferror.Validation("no se definió ID de Op")
	}

	// Busco cabecera de Op
	op, err := h.opsHandler.ReadOne(ctx, ops.ReadOneReq{ID: req.OpID, Comitente: req.Comitente})
	if err != nil {
		return out, deferror.DB(err, "buscando op")
	}

	// Busco partidas
	op.PartidasContables, err = h.opsHandler.Partidas(ctx, ops.PartidasReq{Comitente: req.Comitente, ID: req.OpID})
	if err != nil {
		return out, deferror.DB(err, "buscando partidas de op")
	}

	// Busco credenciales
	cred, err := h.store.GetTicket(ctx, req.Comitente, op.Empresa, core.ServiceName)
	if err != nil {
		return out, errors.Wrap(err, "obteniendo ticket WSFE")
	}

	// Está inscripto en IVA?
	empresa, err := h.empresaGetter.ReadOne(ctx, empresas.ReadOneReq{Comitente: req.Comitente, ID: op.Empresa})
	if err != nil {
		return out, errors.Wrap(err, "buscando empresa")
	}
	inscripto := empresa.IVA == empresas.Inscripto

	// Solicito CAE
	out, err = core.SolicitarCAE(op, inscripto, cred)
	if err != nil {
		return out, errors.Wrap(err, "solicitando CAE a AFIP")
	}

	// Vino rechazado?
	if out.Body.FECAESolicitarResponse.FECAESolicitarResult.FeCabResp.Resultado == "R" {
		obs := []string{}
		for _, v := range out.Body.FECAESolicitarResponse.FECAESolicitarResult.FeDetResp.FECAEDetResponse.Observaciones {
			obs = append(obs, fmt.Sprintf("%v (%v)\n", v.Msg, v.Code))
		}

		for _, v := range out.Body.FECAESolicitarResponse.FECAESolicitarResult.Errors {
			obs = append(obs, fmt.Sprintf("%v (%v)\n", v.Msg, v.Code))
		}
		return out, errors.Errorf("Solicitud rechazada: %v", strings.Join(obs, "; "))
	}

	// Vino OK. Persisto los datos en Op
	caeInt, err := strconv.ParseInt(out.Body.FECAESolicitarResponse.FECAESolicitarResult.FeDetResp.FECAEDetResponse.CAE, 10, 0)
	if err != nil {
		return out, errors.Wrap(err, "transformando CAE a int")
	}

	op.CAE = new(int)
	*op.CAE = int(caeInt)

	op.CAEVto = new(fecha.Fecha)
	*op.CAEVto = fecha.Fecha(out.Body.FECAESolicitarResponse.FECAESolicitarResult.FeDetResp.FECAEDetResponse.CAEFchVto)

	err = h.persistirCAE(ctx, &op)
	if err != nil {
		return core.FECAESolicitarResponse{}, errors.Wrap(err, "grabando CAE")
	}

	return
}

func (h *Handler) RegrabarCAE(ctx context.Context, req SolicitarCAEReq) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se definió ID de comitente")
	}
	if req.OpID == uuid.Nil {
		return deferror.Validation("no se definió ID de Op")
	}

	// Busco cabecera de Op
	op, err := h.opsHandler.ReadOne(ctx, ops.ReadOneReq{Comitente: req.Comitente, ID: req.OpID})
	if err != nil {
		return deferror.DB(err, "buscando Op")
	}

	// Armo req
	if op.AFIPComprobanteID == nil {
		return errors.New("El comprobante que intenta consultar no tiene definido AFIPComprobanteID")
	}
	comp := core.Comp{
		Tipo:       *op.AFIPComprobanteID,
		PuntoVenta: op.PuntoDeVenta,
		NComp:      op.NComp,
	}

	// Busco credencial
	cred, err := h.store.GetTicket(ctx, req.Comitente, op.Empresa, core.ServiceName)
	if err != nil {
		return errors.Wrap(err, "solicitando ticket")
	}

	// Solicito
	consulta, err := core.ActionFECompConsultar(comp, cred)
	if err != nil {
		return errors.Wrap(err, "solcitando a AFIP")
	}

	// Corroboro que sea la misma
	res := consulta.Body.FECompConsultarResponse.FECompConsultarResult.ResultGet
	total := *op.TotalComp
	if float64(res.ImpTotal) != total.Float() {
		return errors.Errorf("el total del comprobante autorizado en AFIP (%v) difiere del que está registrado (%v)", res.ImpTotal, total)
	}

	// Registro en op los datos que traje de la consulta
	if op.CAE == nil {
		codInt, err := strconv.ParseInt(res.CodAutorizacion, 10, 0)
		if err != nil {
			return errors.Wrap(err, "convirtiendo a número el CAE")
		}
		op.CAE = new(int)
		*op.CAE = int(codInt)

		op.CAEVto = new(fecha.Fecha)
		*op.CAEVto = fecha.Fecha(res.FchVto)

		err = h.persistirCAE(ctx, &op)
		if err != nil {
			return errors.Wrap(err, "grabando datos en la op")
		}
	}

	return
}

// Calcula el QR y graba cae, vto y qr en la op
func (h *Handler) persistirCAE(ctx context.Context, op *ops.Op) (err error) {

	// Valido
	if op == nil {
		return errors.New("op era nil")
	}
	if op.TotalComp == nil {
		return errors.New("total comp era nil")
	}
	if op.AFIPComprobanteID == nil {
		return errors.New("AFIPComprobanteID era nil")
	}
	if op.CAE == nil {
		return errors.New("CAE era nil")
	}
	if op.TipoMoneda == "" {
		op.TipoMoneda = "PES"
	}
	if op.TipoMoneda == "PES" {
		op.TipoCambio = dec.NewD4(1)
	}
	if op.TipoMoneda != "PES" && op.TipoCambio == 0 {
		return errors.Errorf("El comprobante está en moneda %v pero no se definó tipo de cambio", op.TipoMoneda)
	}
	tipoReceptor := 0
	if op.TipoIdentificacion != nil {
		tipoReceptor = *op.TipoIdentificacion
	}
	nident := 0
	if op.NIdentificacion != nil {
		nident = *op.NIdentificacion
	}

	// Busco empresa
	emp, err := h.empresaGetter.ReadOne(ctx, empresas.ReadOneReq{ID: op.Empresa, Comitente: op.Comitente})
	if err != nil {
		return errors.Wrap(err, "buscando empresa emisora")
	}

	// Junto datos
	qrData := qr.Data{
		Version:         1,
		Fecha:           op.Fecha,
		CUIT:            cuits.CUIT(emp.CUIT),
		PuntoDeVenta:    op.PuntoDeVenta,
		NComp:           op.NComp,
		TipoComp:        *op.AFIPComprobanteID,
		Importe:         *op.TotalComp,
		Moneda:          op.TipoMoneda,
		Cotizacion:      op.TipoCambio,
		TipoDocReceptor: tipoReceptor,
		NDocReceptor:    nident,
		TipoCodAut:      "E",
		CAE:             *op.CAE,
	}

	// Genero código
	codigoQR, err := qr.Version1(qrData)
	if err != nil {
		return errors.Wrap(err, "generando código QR")
	}

	// Grabo
	query := `
	UPDATE ops SET cae=$1, cae_vto=$2, qr=$3 WHERE id = $4;
	`
	_, err = h.conn.Exec(ctx, query, op.CAE, op.CAEVto, codigoQR, op.ID)
	if err != nil {
		return deferror.DB(err, "guardando CAE en base de datos")
	}

	return
}

func (h *Handler) ConsultarCAE(ctx context.Context, req SolicitarCAEReq) (out core.CompConsultarResponse, err error) {
	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se definió ID de comitente")
	}
	if req.OpID == uuid.Nil {
		return out, deferror.Validation("no se definió ID de Op")
	}

	// Busco cabecera de Op
	op, err := h.opsHandler.ReadOne(ctx, ops.ReadOneReq{Comitente: req.Comitente, ID: req.OpID})
	if err != nil {
		return out, deferror.DB(err, "buscando Op")
	}

	// Armo req
	if op.AFIPComprobanteID == nil {
		return out, errors.New("El comprobante que intenta consultar no tiene definido AFIPComprobanteID")
	}
	comp := core.Comp{
		Tipo:       *op.AFIPComprobanteID,
		PuntoVenta: op.PuntoDeVenta,
		NComp:      op.NComp,
	}

	// Busco credencial
	cred, err := h.store.GetTicket(ctx, req.Comitente, op.Empresa, core.ServiceName)
	if err != nil {
		return out, errors.Wrap(err, "buscando ticket")
	}

	// Solicito
	out, err = core.ActionFECompConsultar(comp, cred)
	if err != nil {
		return out, errors.Wrap(err, "solcitando a AFIP")
	}

	return
}

type CompUltimoAutorizadoReq struct {
	CompID    int `json:",string"`
	Comitente int
}

func (h *Handler) CompUltimoAutorizado(ctx context.Context, req CompUltimoAutorizadoReq) (out core.CompUltimoAutorizadoResponse, err error) {
	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se definió ID de comitente")
	}
	if req.CompID == 0 {
		return out, deferror.Validation("no se definió ID de Op")
	}

	// Busco cabecera de Op
	comp, err := h.compGetter.ReadOne(ctx, comps.ReadOneReq{Comitente: req.Comitente, ID: req.CompID})
	if err != nil {
		return out, deferror.DB(err, "buscando Op")
	}

	// Valido
	if len(comp.OriginalesAceptados) != 1 {
		return out, errors.New("debe haber un solo comprobante original")
	}
	if comp.PuntoDeVenta == nil {
		return out, errors.New("no tiene definido punto de venta")
	}

	cred, err := h.store.GetTicket(ctx, req.Comitente, comp.Empresa, core.ServiceName)
	if err != nil {
		return out, errors.Wrap(err, "buscando ticket")
	}

	// Solicito
	out, err = core.ActionFECompUltimoAutorizado(*comp.PuntoDeVenta, comp.OriginalesAceptados[0], cred)
	if err != nil {
		return out, errors.Wrap(err, "solcitando a AFIP")
	}

	return
}

type CompsPendientesAutorizarReq struct {
	Comitente int
	Empresa   int `json:",string"`
	Desde     fecha.Fecha
	Hasta     fecha.Fecha
}
type CompsPendientesAutorizarResp struct {
	ID         uuid.UUID
	Fecha      fecha.Fecha
	CompNombre string
	Persona    string
	NCompStr   string
	Total      dec.D2
}

func (h *Handler) CompsPendientesAutorizar(ctx context.Context, req CompsPendientesAutorizarReq) (out []CompsPendientesAutorizarResp, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se determinó ID de comitente")
	}
	if req.Empresa == 0 {
		return out, deferror.Validation("no se determinó la empresa")
	}

	// Armo filtros
	filtros := []string{}
	if req.Desde != 0 {
		filtros = append(filtros, fmt.Sprintf("fecha >= '%v'", req.Desde.JSONString()))
	}
	if req.Hasta != 0 {
		filtros = append(filtros, fmt.Sprintf("fecha <= '%v'", req.Hasta.JSONString()))
	}
	where := ""
	if len(filtros) > 0 {
		where = " AND " + strings.Join(filtros, " AND ")
	}

	query := fmt.Sprintf(`
		SELECT  id, fecha, persona_nombre, comp_nombre, n_comp_str, total_comp
		FROM ops 
		WHERE 
			comp_id IN (
				SELECT id FROM comps WHERE tipo_emision = 'wsfev1'
			) AND 
			cae IS NULL AND
			empresa = $1
			%v
		ORDER BY comp_nombre, n_comp_str
			;
		;`, where)

	// Busco
	rows, err := h.conn.Query(ctx, query, req.Empresa)
	if err != nil {
		return out, deferror.DB(err, "buscando ops pendientes")
	}
	defer rows.Close()

	for rows.Next() {
		resp := CompsPendientesAutorizarResp{}
		err = rows.Scan(&resp.ID, &resp.Fecha, &resp.Persona, &resp.CompNombre, &resp.NCompStr, &resp.Total)
		if err != nil {
			return out, deferror.DB(err, "escaneando ops pendientes")
		}
		out = append(out, resp)
	}

	return

}

type CompsPendientesAutorizarGeneralResp struct {
	Empresa       int `json:",string"`
	EmpresaNombre string
	Cantidad      int
}

// Informe para dashboard general
func (h *Handler) CompsPendientesAutorizarGeneral(ctx context.Context, comitente int) (out []CompsPendientesAutorizarGeneralResp, err error) {

	// Valido
	if comitente == 0 {
		return out, deferror.Validation("no se determinó ID de comitente")
	}

	query := `
		SELECT  empresa, empresas.nombre, COUNT(ops.id)
		FROM ops 
		INNER JOIN empresas ON ops.empresa = empresas.id
		WHERE 
			ops.comitente = $1 AND
			ops.comp_id IN (
				SELECT id FROM comps WHERE tipo_emision = 'wsfev1'
			) AND 
			ops.cae IS NULL
		GROUP BY empresa, empresas.nombre
		HAVING COUNT(ops.id) > 0
		ORDER BY empresas.nombre;`

	// Busco
	rows, err := h.conn.Query(ctx, query, comitente)
	if err != nil {
		return out, deferror.DB(err, "buscando ops pendientes")
	}
	defer rows.Close()

	for rows.Next() {
		v := CompsPendientesAutorizarGeneralResp{}
		err = rows.Scan(&v.Empresa, &v.EmpresaNombre, &v.Cantidad)
		if err != nil {
			return out, deferror.DB(err, "escaneando ops pendientes")
		}
		out = append(out, v)
	}

	return

}
