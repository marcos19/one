package libroiva

import (
	"context"
	"fmt"

	"bitbucket.org/marcos19/one/pkg/afip/afipmodels"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"bitbucket.org/marcos19/one/pkg/iva"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Req struct {
	Comitente int
	Empresa   empresas.Empresa
	Desde     fecha.Fecha
	Hasta     fecha.Fecha
	TipoLibro string
	// IDs de comprobantes de la tambla comps que están tildados
	// como que VanAlLibroIVA
	// CompsIncluidos tipos.GrupoInts
}

// LibroIVA contiene todos los datos de un libro de IVA.
type LibroIVA struct {
	Empresa empresas.Empresa

	Tipo        string
	Desde       fecha.Fecha
	Hasta       fecha.Fecha
	PrimeraHoja int
	Cabeceras   []Cabecera

	// Los códigos AFIP de comprobantes
	Originales map[int]string

	// Dice para cada cuenta contable, a qué categoría corresponden
	// Son las que solicita al cargar IVA por ACTIVIDAD
	// (Servicios, Bienes, Locaciones)
	ImputacionIVACompras map[int]string

	NombresCuentas map[int]string
}

// Devuelve las cuentas usadas en los renglones que tienen concepto y alícuota.
func (l *LibroIVA) CuentasUsadas() (out []int) {

	m := map[int]struct{}{}
	for _, c := range l.Cabeceras {
		for _, r := range c.Renglones {
			if r.Concepto == nil {
				continue
			}
			if r.Alicuota == nil {
				continue
			}
			m[r.Cuenta] = struct{}{}
		}
	}

	for k := range m {
		out = append(out, k)
	}

	return
}

type Cabecera struct {
	// id de tabla ops
	ID                 uuid.UUID
	Fecha              fecha.Fecha
	CompID             *int
	CompNombre         string
	NComp              string
	TipoIdentificacion *int
	NIdentificacion    *int
	CondicionFiscal    *afipmodels.CondicionIVA
	PersonaNombre      string
	Renglones          []Renglon
}

func (c *LibroIVA) Total(concepto iva.Concepto, alicuota iva.Alicuota) (out dec.D2) {
	for _, v := range c.Cabeceras {
		out += v.Total(concepto, alicuota)
	}
	return
}

// Devuelve el total por concepto y alicuota. Si alicuota es cero, suma
// independientemente de la alicuota de la partida
func (c *Cabecera) Total(concepto iva.Concepto, alicuota iva.Alicuota) (out dec.D2) {
	for _, v := range c.Renglones {
		if v.Concepto == nil {
			continue
		}

		// Total total?
		if concepto == 0 {
			out += v.Monto
			continue
		}

		if *v.Concepto != concepto {
			continue
		}
		// No discrimina por alicuota, sumo todo
		if alicuota == 0 {
			out += v.Monto
			continue
		}

		// Sólo sumo esta alícuota
		if v.Alicuota == nil {
			continue
		}
		if *v.Alicuota != alicuota {
			continue
		}

		out += v.Monto
	}
	return
}

type Renglon struct {
	ID       uuid.UUID
	Concepto *iva.Concepto
	Alicuota *iva.Alicuota
	Cuenta   int
	Monto    dec.D2
}

func NewLibroIVA(ctx context.Context, db *pgxpool.Pool, req Req) (out *LibroIVA, err error) {

	if req.Empresa.ID == 0 {
		return out, errors.New("no se definió la empresa")
	}
	if req.Desde == 0 {
		return out, errors.New("no se definió la fecha desde")
	}
	if req.Hasta == 0 {
		return out, errors.New("no se definió la fecha hasta")
	}
	switch req.TipoLibro {
	case "Compras", "Ventas":
		// ok
	default:
		return out, errors.New("no se definió tipo de libro")
	}

	out = &LibroIVA{}
	out.Desde = req.Desde
	out.Hasta = req.Hasta
	out.Empresa = req.Empresa
	out.Tipo = req.TipoLibro

	// Determino comps
	cc, err := determinarComps(ctx, db, req.Comitente, req.Empresa.ID, req.TipoLibro)
	if err != nil {
		return out, errors.Wrap(err, "determinando comps")
	}

	// Traigo cabeceras
	hh, err := traerCabeceras(ctx, db, req.Desde, req.Hasta, cc, req.Comitente, req.Empresa.ID)
	if err != nil {
		return out, errors.Wrap(err, "trayendo cabeceras")
	}

	// Traigo renglones
	rr, err := traerRenglones(ctx, db, req.Desde, req.Hasta, cc, req.Comitente, req.Empresa.ID, req.TipoLibro)
	if err != nil {
		return out, errors.Wrap(err, "trayendo renglones")
	}

	// Conceptos de IVA Compra
	out.ImputacionIVACompras, err = determinarConceptosIVACompra(ctx, db, req.Comitente)
	if err != nil {
		return out, errors.Wrap(err, "determinando conceptos IVA compra")
	}

	//  Nombres de las cuentas
	out.NombresCuentas, err = determinarCuentasNombres(ctx, db, req.Comitente)
	if err != nil {
		return out, errors.Wrap(err, "determinando cuentas nombres")
	}

	// Pongo renglones a cada cabecera
	for i, v := range hh {
		hh[i].Renglones = rr[v.ID]
	}

	out.Cabeceras = hh
	return
}

// Devuelve un map que me dice para cada cuenta contable, a que concepto pertenece.
func determinarConceptosIVACompra(ctx context.Context, db *pgxpool.Pool, comitente int) (out map[int]string, err error) {

	out = map[int]string{}
	query := `
		SELECT id, concepto_iva_compras 
		FROM cuentas 
		WHERE 
			comitente = $1 AND 
			concepto_iva_compras IS NOT NULL AND 
			concepto_iva_compras <> ''
	`

	rows, err := db.Query(ctx, query, comitente)
	if err != nil {
		return out, errors.Wrap(err, "querying conceptos iva compra")
	}
	defer rows.Close()

	for rows.Next() {
		id := 0
		concepto := ""

		err = rows.Scan(&id, &concepto)
		if err != nil {
			return out, errors.Wrap(err, "escaneando concepto de IVA compras")
		}

		out[id] = concepto
	}
	return

}

// Devuelve un map con los nombres de las cuentas.
func determinarCuentasNombres(ctx context.Context, db *pgxpool.Pool, comitente int) (out map[int]string, err error) {

	out = map[int]string{}
	query := `
		SELECT id, nombre 
		FROM cuentas 
		WHERE 
			comitente = $1 
	`

	rows, err := db.Query(ctx, query, comitente)
	if err != nil {
		return out, errors.Wrap(err, "querying conceptos iva compra")
	}
	defer rows.Close()

	for rows.Next() {
		id := 0
		nombre := ""

		err = rows.Scan(&id, &nombre)
		if err != nil {
			return out, errors.Wrap(err, "escaneando concepto de IVA compras")
		}

		out[id] = nombre
	}
	return
}

// Determina cuales son los comprobantes que pertenecen a este libro de IVA.
func determinarComps(
	ctx context.Context,
	db *pgxpool.Pool,
	comitente, empresa int,
	libro string,
) (out tipos.GrupoInts, err error) {

	// Determino los comprobantes alcanzados
	query := `
			SELECT id 
			FROM comps 
			WHERE 
				comitente = $1 AND 
				empresa = $2 AND
				libro_iva = $3
		`
	rows, err := db.Query(ctx, query, comitente, empresa, libro)
	if err != nil {
		return out, errors.Wrap(err, "buscando comprobantes que corresponden ir al libro de IVA")
	}
	defer rows.Close()

	for rows.Next() {
		comp := 0
		err = rows.Scan(&comp)
		if err != nil {
			return out, errors.Wrap(err, "escaneando row de config")
		}
		out = append(out, comp)
	}

	return
}

// Trae las cabeceras que va a usar el libro de IVA
func traerCabeceras(
	ctx context.Context,
	db *pgxpool.Pool,
	desde, hasta fecha.Fecha,
	comps tipos.GrupoInts,
	comitente, empresa int) (out []Cabecera, err error) {

	query := fmt.Sprintf(`
		SELECT id, fecha_original, afip_comprobante_id, n_comp_str, tipo_identificacion,
			n_identificacion, condicion_fiscal, persona_nombre
		FROM ops
		WHERE 
			comitente = $1 AND
			empresa = $2 AND
			fecha BETWEEN '%v' AND '%v' AND
			comp_id IN %v
		ORDER BY fecha_original, n_comp_str;
	`, desde.JSONString(), hasta.JSONString(), comps.ParaClausulaIn())

	rows, err := db.Query(ctx, query, comitente, empresa)
	if err != nil {
		return out, errors.Wrap(err, "querying cabeceras")
	}
	defer rows.Close()

	for rows.Next() {
		h := Cabecera{}
		err = rows.Scan(&h.ID, &h.Fecha, &h.CompID, &h.NComp, &h.TipoIdentificacion, &h.NIdentificacion, &h.CondicionFiscal, &h.PersonaNombre)
		if err != nil {
			return out, errors.Wrap(err, "escaneando headers")
		}
		if h.CompID == nil {
			return out, errors.Errorf("op %v no tenía ComprobanteAFIP ID", h.ID)
		}
		if h.TipoIdentificacion == nil {
			return out, errors.Errorf("op %v no tenía TipoIdentificacion", h.ID)
		}
		if h.TipoIdentificacion == nil {
			return out, errors.Errorf("op %v no tenía NIdentificacion", h.ID)
		}
		if h.CondicionFiscal == nil {
			return out, errors.Errorf("op %v no tenía CondicionFiscal", h.ID)
		}
		out = append(out, h)
	}

	return
}

// Trae los renglones que va a usar el libro de IVA
func traerRenglones(
	ctx context.Context,
	db *pgxpool.Pool,
	desde, hasta fecha.Fecha,
	comps tipos.GrupoInts,
	comitente, empresa int,
	tipoLibro string,
) (out map[uuid.UUID][]Renglon, err error) {

	query := fmt.Sprintf(`
		SELECT id, op_id, concepto_iva, alicuota_iva, cuenta, monto
		FROM partidas
		WHERE 
			op_id IN (
				SELECT id 
				FROM ops 
				WHERE 
					comitente = $1 AND
					empresa = $2 AND
					fecha BETWEEN '%v' AND '%v' AND
					comp_id IN %v
			);
	`, desde.JSONString(), hasta.JSONString(), comps.ParaClausulaIn())

	rows, err := db.Query(ctx, query, comitente, empresa)
	if err != nil {
		return out, errors.Wrap(err, "querying renglones")
	}
	defer rows.Close()

	out = map[uuid.UUID][]Renglon{}

	for rows.Next() {
		r := Renglon{}
		opID := uuid.UUID{}
		err = rows.Scan(&r.ID, &opID, &r.Concepto, &r.Alicuota, &r.Cuenta, &r.Monto)
		if err != nil {
			return out, errors.Wrap(err, "escaneando renglones")
		}
		if tipoLibro == "Ventas" {
			r.Monto = -r.Monto
		}
		anterior := out[opID]
		anterior = append(anterior, r)
		out[opID] = anterior
	}

	return
}
