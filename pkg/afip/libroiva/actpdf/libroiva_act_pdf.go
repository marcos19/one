// Genera el PDF del libro de iva con el formato "IVA por actividad"
package actpdf

import (
	"fmt"
	"io"
	"sort"

	"bitbucket.org/marcos19/one/pkg/afip/afipmodels"
	libroiva "bitbucket.org/marcos19/one/pkg/afip/libroiva"
	"bitbucket.org/marcos19/one/pkg/iva"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/jung-kurt/gofpdf"
	"github.com/rs/zerolog/log"
)

type Outputer interface {
	Output(w io.Writer) error
}

// PDF devuelve un archivo PDF del libro diario.
func PDF(l *libroiva.LibroIVA) (out *gofpdf.Fpdf, err error) {
	p := gofpdf.New("L", "mm", "A4", "")
	tr := p.UnicodeTranslatorFromDescriptor("")

	tiposComps := map[int]string{}

	// Seteo margenes
	margenIzq := 5.
	margenDer := 5.
	margenSup := 10.
	ancho, _ := p.GetPageSize()
	anchoImprimible := ancho - margenIzq - margenDer
	// margenInf := 10.
	p.SetMargins(margenIzq, margenSup, margenDer)

	// Fuentes
	fontSize := 8.

	// Encabezado
	p.SetHeaderFunc(func() {
		// NombreEmpresa
		p.SetFont("Arial", "B", 15)
		p.Text(margenIzq+3, 17, tr(l.Empresa.Nombre))

		// NombreEmpresa
		p.SetFont("Arial", "", fontSize)
		p.Text(margenIzq+3, 21, tr(fmt.Sprintf("CUIT: %v", l.Empresa.CUIT)))
		p.Text(margenIzq+3, 24, tr(fmt.Sprintf("Domicilio: %v", l.Empresa.Domicilio)))

		// "Libro IVA Ventas"
		p.SetFont("Arial", "B", 15)
		p.Text(margenIzq+anchoImprimible-65, 17, "Libro IVA "+l.Tipo)

		// Desde - hasta
		p.SetFont("Arial", "", fontSize)
		p.Text(margenIzq+anchoImprimible-65, 21, fmt.Sprintf("Desde el %v hasta el %v", l.Desde, l.Hasta))

		// "Número de hoja"
		p.Ln(6)
		text := fmt.Sprintf(tr("Página: %v.-"), p.PageNo()+l.PrimeraHoja)
		p.Text(margenIzq+anchoImprimible-65, 24, text)

		// Cuadrado del header
		p.ClipRoundedRect(margenIzq, margenSup, anchoImprimible, 16, 2, true)
		p.ClipEnd()

		p.Ln(8)

		p.SetFont("Arial", "B", fontSize)

		p.CellFormat(16., 10., "Fecha", "", 0, "MC", false, 0, "")
		p.CellFormat(7., 10., "Tipo", "", 0, "MC", false, 0, "")
		p.CellFormat(22., 10., tr("Número"), "", 0, "MC", false, 0, "")
		p.CellFormat(20., 10., "CUIT", "", 0, "MC", false, 0, "")
		p.CellFormat(8., 10., "Cond", "", 0, "MC", false, 0, "")
		p.CellFormat(62., 10., "Nombre", "", 0, "MC", false, 0, "")

		p.CellFormat(25., 10., "Neto", "", 0, "MC", false, 0, "")
		p.CellFormat(10., 10., tr("Alícuota"), "", 0, "MC", false, 0, "")
		p.CellFormat(15., 10., "IVA", "", 0, "MC", false, 0, "")
		p.CellFormat(20., 10., "Exento", "", 0, "MC", false, 0, "")
		p.CellFormat(20., 10., "No Gravado", "", 0, "MC", false, 0, "")
		p.CellFormat(20., 10., "Percepciones", "", 0, "MC", false, 0, "")
		p.CellFormat(20., 10., "Otros Impuestos", "", 0, "MC", false, 0, "")
		p.CellFormat(20., 10., "Total", "", 0, "MC", false, 0, "")

		// Cuadrado de las cabeceras de renglones
		p.ClipRoundedRect(margenIzq, margenSup+16+1, anchoImprimible, 4, 1, true)
		p.ClipEnd()

		p.Ln(7.5)

	})

	p.AddPage()
	p.SetFont("Arial", "", fontSize)

	distanciaAlNeto := float64(16 + 7 + 22 + 20 + 8 + 62)

	// Renglones
	pintar := false
	altoRenglon := 3.
	p.SetFillColor(243, 243, 243)
	for _, v := range l.Cabeceras {
		pintar = !pintar
		p.CellFormat(16., altoRenglon, v.Fecha.String(), "", 0, "MC", pintar, 0, "")

		p.CellFormat(7., altoRenglon, fmt.Sprint(*v.CompID), "", 0, "MR", pintar, 0, "")

		tiposComps[*v.CompID] = v.CompNombre
		p.CellFormat(22., altoRenglon, v.NComp, "", 0, "MR", pintar, 0, "")

		p.CellFormat(20., altoRenglon, fmt.Sprint(*v.NIdentificacion), "", 0, "MR", pintar, 0, "")
		p.CellFormat(8., altoRenglon, condicionAbrev(*v.CondicionFiscal), "", 0, "MC", pintar, 0, "")
		nombre := tr(v.PersonaNombre)
		if len(nombre) > 40 {
			nombre = nombre[:40] + "..."
		}
		p.CellFormat(62., altoRenglon, nombre, "", 0, "ML", pintar, 0, "")

		// neto := dec.D2(0)
		// iva := dec.D2(0)
		// alicuota := ""

		p.CellFormat(50., altoRenglon, "", "", 0, "MR", pintar, 0, "")
		p.CellFormat(20., altoRenglon, v.Total(iva.Exento, 0).String(), "", 0, "MR", pintar, 0, "")
		p.CellFormat(20., altoRenglon, v.Total(iva.NoGravado, 0).String(), "", 0, "MR", pintar, 0, "")
		p.CellFormat(20., altoRenglon, sumarPercepciones(v).String(), "", 0, "MR", pintar, 0, "")
		p.CellFormat(20., altoRenglon, v.Total(iva.OtrosTributos, 0).String(), "", 0, "MR", pintar, 0, "")
		p.CellFormat(20., altoRenglon, v.Total(0, 0).String(), "", 0, "MR", pintar, 0, "")
		p.SetX(p.GetX() - 50 - 20 - 20 - 20 - 20 - 20)

		aa := alicuotas(v)

		switch len(aa) {
		case 0:
		// Los dejo en cero
		// case 1:
		// 	neto += v.Alics[0].Neto
		// 	iva += v.Alics[0].IVA
		// 	porc, ok := v.Alics[0].Alicuota.Porcentaje()
		// 	if !ok {
		// 		errors.Errorf("no se pudo determinar la alícuouta de %v", v.Alics[0].Alicuota)
		// 	}
		// 	alicuota = fmt.Sprint(porc*100, " %")

		default:
			for i, w := range aa {

				if i > 0 {
					p.Cell(distanciaAlNeto, altoRenglon, "")
				}

				p.CellFormat(25., altoRenglon, w.BaseImponible.String(), "", 0, "MR", pintar, 0, "")
				porc, err := w.Alicuota.Porcentaje()
				if err != nil {
					return out, errors.Wrap(err, "determinando porcentaje de alícuota")
				}
				p.CellFormat(9, altoRenglon, fmt.Sprint(porc*100, "%"), "", 0, "ML", pintar, 0, "")
				p.CellFormat(16., altoRenglon, w.IVA.String(), "", 0, "MR", pintar, 0, "")

				if i+1 < len(aa) {
					p.Ln(3)
				}
			}
		}

		// // Total de asiento
		// p.Cell(anchoImprimible-5-20-25-25, 10, tr(det))
		// p.CellFormat(anchoImprimible-25-25, 10., "Total", "", 0, "MR", false, 0, "")
		// p.SetFont("Arial", "B", fontSize)
		// p.CellFormat(25., 10., v.TotalDebe().String(), "", 0, "MR", false, 0, "")
		// p.CellFormat(25., 10., v.TotalHaber().String(), "", 0, "MR", false, 0, "")
		p.Ln(3)
		// p.SetFont("Arial", "", fontSize)

	}
	// Totales
	//p.Ln(1)
	p.ClipRoundedRect(margenIzq, p.GetY(), anchoImprimible, 6, 1, true)
	p.ClipEnd()
	p.SetY(p.GetY() - 2)

	p.SetFont("Arial", "B", fontSize)
	p.CellFormat(16.+7.+22.+20.+70., 10., "TOTALES", "", 0, "MR", false, 0, "")
	p.CellFormat(25., 10., l.Total(iva.BaseImponible, 0).String(), "", 0, "MR", false, 0, "")
	p.CellFormat(25., 10., l.Total(iva.IVA, 0).String(), "", 0, "MR", false, 0, "")
	p.CellFormat(20., 10., l.Total(iva.Exento, 0).String(), "", 0, "MR", false, 0, "")
	p.CellFormat(20., 10., l.Total(iva.NoGravado, 0).String(), "", 0, "MR", false, 0, "")
	p.CellFormat(20., 10., totalPercepciones(l).String(), "", 0, "MR", false, 0, "")
	p.CellFormat(20., 10., l.Total(iva.OtrosTributos, 0).String(), "", 0, "MR", false, 0, "")
	p.CellFormat(20., 10., l.Total(0, 0).String(), "", 0, "MR", false, 0, "")

	// Referencia de comprobantes
	ordenado := []int{}
	for k := range tiposComps {
		ordenado = append(ordenado, k)
	}
	sort.Slice(ordenado, func(i, j int) bool {
		return ordenado[i] < ordenado[j]
	})
	p.Ln(10)

	p.SetFont("Arial", "B", fontSize+1)
	p.Cell(200, 10, "Tipos de comprobantes:")
	p.SetFont("Arial", "", fontSize)
	p.Ln(5)

	for _, v := range ordenado {
		p.Cell(200, 10, fmt.Sprintf("%v: %v", v, tr(tiposComps[v])))
		p.Ln(3)
	}

	// Resúmenes
	switch l.Tipo {
	case "Ventas":
		err = agregarResumenVentas(l, p)
		if err != nil {
			return out, errors.Wrap(err, "agregando resumen venta")
		}
	case "Compras":
		err = agregarResumenCompras(l, p)
		if err != nil {
			return out, errors.Wrap(err, "generando cuadro resumen de IVA Compras")
		}
	}

	return p, nil
}
func condicionAbrev(id afipmodels.CondicionIVA) string {
	switch id {
	case afipmodels.CondicionNoInscripto:
		return "NI"
	case afipmodels.CondicionResponsableInscripto:
		return "RI"
	case afipmodels.CondicionExento:
		return "EX"
	case afipmodels.CondicionMonotributo:
		return "MT"
	case afipmodels.CondicionConsumidorFinal:
		return "CF"
	default:
		return "OT"
	}
}

type alic struct {
	Alicuota      iva.Alicuota
	BaseImponible dec.D2
	IVA           dec.D2
}

func alicuotas(c libroiva.Cabecera) (out []alic) {
	sumBI := map[iva.Alicuota]dec.D2{}
	sumIVA := map[iva.Alicuota]dec.D2{}

	for _, v := range c.Renglones {
		if v.Alicuota == nil {
			continue
		}

		switch *v.Concepto {

		case iva.BaseImponible:
			sumBI[*v.Alicuota] += v.Monto

		case iva.IVA:
			sumIVA[*v.Alicuota] += v.Monto
		}
	}

	ids := map[iva.Alicuota]struct{}{}
	for k := range sumBI {
		ids[k] = struct{}{}
	}
	for k := range sumIVA {
		ids[k] = struct{}{}
	}

	for k := range ids {
		a := alic{}
		a.Alicuota = k
		a.BaseImponible = sumBI[k]
		a.IVA = sumIVA[k]
		out = append(out, a)
	}

	sort.Slice(out, func(i, j int) bool {
		return out[i].Alicuota < out[j].Alicuota
	})

	return
}

func sumarPercepciones(c libroiva.Cabecera) (sum dec.D2) {
	for _, v := range c.Renglones {
		if v.Concepto == nil {
			continue
		}
		switch *v.Concepto {
		case iva.PercepcionNoCategorizados, iva.PercepcionesISIB, iva.PercepcionesMunicipales, iva.PercepcionesIVA, iva.PercepcionesNacionales:
			sum += v.Monto
		}
	}
	return
}
func totalPercepciones(l *libroiva.LibroIVA) (sum dec.D2) {
	for _, v := range l.Cabeceras {
		sum += sumarPercepciones(v)
	}
	return
}

func agregarResumenVentas(l *libroiva.LibroIVA, p *gofpdf.Fpdf) (err error) {
	tr := p.UnicodeTranslatorFromDescriptor("")

	type Agg struct {
		Condicion afipmodels.CondicionIVA
		Alic      iva.Alicuota
	}

	type Sum struct {
		Neto dec.D2
		IVA  dec.D2
	}
	// Guardo las condiciones que voy encontrando en facturas
	condiciones := map[afipmodels.CondicionIVA]struct{}{}

	// Guardo las condiciones que voy encontrando en notas de crédito
	condicionesNC := map[afipmodels.CondicionIVA]struct{}{}

	res := map[Agg]Sum{}
	nc := map[Agg]Sum{}
	for _, v := range l.Cabeceras {
		aa := alicuotas(v)
		for _, alic := range aa {

			// Las facturas
			if !afipmodels.EsNotaDeCredito(*v.CompID) {
				agg := Agg{
					Condicion: *v.CondicionFiscal,
					Alic:      alic.Alicuota,
				}
				condiciones[*v.CondicionFiscal] = struct{}{}
				// Traigo el saldo que venía acumulado
				saldoAnterior := res[agg]
				saldoAnterior.Neto += alic.BaseImponible
				saldoAnterior.IVA += alic.IVA
				// Guardo el saldo con este nuevo movimiento
				res[agg] = saldoAnterior
			}

			// Las notas de crédito
			if afipmodels.EsNotaDeCredito(*v.CompID) {
				agg := Agg{
					Condicion: *v.CondicionFiscal,
					Alic:      alic.Alicuota,
				}
				condicionesNC[*v.CondicionFiscal] = struct{}{}
				// Traigo el saldo que venía acumulado
				saldoAnterior := nc[agg]
				saldoAnterior.Neto += alic.BaseImponible
				saldoAnterior.IVA += alic.IVA
				// Guardo el saldo con este nuevo movimiento
				nc[agg] = saldoAnterior
			}
		}
	}

	p.Ln(10)

	totalIVA := dec.D2(0)

	headerHeight := 4.
	renglonHeight := 4.

	// Imprimo resumen de facturas
	for k := range condiciones {

		p.SetFont("Arial", "B", 11)
		p.CellFormat(90., 5, k.Nombre(), "", 1, "ML", false, 0, "")
		// p.Ln(5)
		// Hago un cuadrito para cada condición

		// p.CellFormat(20., 10., tr("Alícuota"), "1", 0, "MR", false, 0, "")
		// p.Ln(4)
		p.SetFont("Arial", "B", 9)
		p.CellFormat(30., headerHeight, "", "TBLR", 0, "MR", false, 0, "")
		p.CellFormat(30., headerHeight, "Neto", "TBLR", 0, "MR", false, 0, "")
		p.CellFormat(30., headerHeight, "IVA", "TBLR", 0, "MR", false, 0, "")
		p.CellFormat(30., headerHeight, "Total", "TBLR", 1, "MR", false, 0, "")
		for _, alic := range iva.Alicuotas() {

			key := Agg{k, alic.ID}
			val := res[key]
			if val.IVA == 0 && val.Neto == 0 {
				continue
			}
			totalIVA += val.IVA

			p.SetFont("Arial", "", 9)
			p.CellFormat(30., renglonHeight, alic.ID.String(), "TLBR", 0, "MR", false, 0, "")
			p.CellFormat(30., renglonHeight, val.Neto.String(), "TBLR", 0, "MR", false, 0, "")
			p.CellFormat(30., renglonHeight, val.IVA.String(), "TBLR", 0, "MR", false, 0, "")
			p.CellFormat(30., renglonHeight, (val.Neto + val.IVA).String(), "TBLR", 1, "MR", false, 0, "")
		}

		p.Ln(3)
	}

	p.SetFont("Arial", "B", 9)

	p.SetFillColor(192, 225, 231)
	p.CellFormat(90., 5, tr("Total del débito fiscal generado por operaciones de venta"), "TBLR", 0, "MR", false, 0, "")
	p.CellFormat(30., 5, fmt.Sprint(totalIVA), "TBLR", 1, "MR", true, 0, "")
	p.Ln(10)

	// Imprimo resumen de notas de crédito
	totalIVANC := dec.D2(0)
	for k := range condicionesNC {

		p.SetFont("Arial", "B", 11)
		p.CellFormat(90., 10., tr(k.Nombre()), "", 1, "ML", false, 0, "")
		// Hago un cuadrito para cada condición

		p.SetFont("Arial", "B", 9)
		p.CellFormat(30., headerHeight, "", "TLBR", 0, "MR", false, 0, "")
		p.CellFormat(30., headerHeight, "Neto", "TBLR", 0, "MR", false, 0, "")
		p.CellFormat(30., headerHeight, "IVA", "TBLR", 0, "MR", false, 0, "")
		p.CellFormat(30., headerHeight, "Total", "TBLR", 1, "MR", false, 0, "")

		for _, alic := range iva.Alicuotas() {

			key := Agg{k, alic.ID}
			val := nc[key]
			if val.IVA == 0 && val.Neto == 0 {
				continue
			}

			totalIVANC += val.IVA
			p.SetFont("Arial", "", 9)
			p.CellFormat(30., renglonHeight, alic.ID.String(), "TBLF", 0, "MR", false, 0, "")
			p.CellFormat(30., renglonHeight, val.Neto.String(), "TBLF", 0, "MR", false, 0, "")
			p.CellFormat(30., renglonHeight, val.IVA.String(), "TBLR", 0, "MR", false, 0, "")
			p.CellFormat(30., renglonHeight, (val.Neto + val.IVA).String(), "TBLR", 1, "MR", false, 0, "")
		}

		p.Ln(3)
	}

	p.SetFont("Arial", "B", 9)
	if len(condicionesNC) > 0 {
		p.CellFormat(90., 5, tr("Crédito fiscal a restituir"), "TBLR", 0, "MR", false, 0, "")
		p.CellFormat(30., 5, fmt.Sprint(totalIVANC), "TBLR", 1, "MR", true, 0, "")
		p.Ln(5)
	}
	p.CellFormat(120, 5, tr("Total IVA débito fiscal"), "TBLR", 0, "MR", false, 0, "")
	p.CellFormat(30, 5, fmt.Sprint(totalIVA+totalIVANC), "TBLR", 1, "MR", true, 0, "")

	return
}

func agregarResumenCompras(l *libroiva.LibroIVA, p *gofpdf.Fpdf) (err error) {
	tr := p.UnicodeTranslatorFromDescriptor("")

	type key struct {
		concepto string
		alicuota iva.Alicuota
	}

	// Determino grupos usados
	conceptosDeCompra := map[string]struct{}{}

	sumBI := map[key]dec.D2{}
	sumIVA := map[key]dec.D2{}

	sumBINC := map[key]dec.D2{}
	sumIVANC := map[key]dec.D2{}
	operacionesConRI := "Operaciones con Responsables Inscriptos"
	operacionesConOtros := "Operaciones con Sujetos Exentos, No Alcanzados, Monotributistas y Consumidores Finales"
	condicionesEnNC := map[string]struct{}{}

	// Los sumo
	for _, c := range l.Cabeceras {

		// Sumo NOTAS DE CREDITO
		if afipmodels.EsNotaDeCredito(*c.CompID) {
			cto := ""
			if *c.CondicionFiscal == afipmodels.CondicionResponsableInscripto {
				cto = operacionesConRI
			} else {
				cto = operacionesConOtros
			}
			condicionesEnNC[cto] = struct{}{}

			for _, r := range c.Renglones {
				if r.Concepto == nil {
					continue
				}
				if *r.Concepto == iva.BaseImponible {
					sumBINC[key{concepto: cto, alicuota: *r.Alicuota}] += r.Monto
				}
				if *r.Concepto == iva.IVA {
					sumIVANC[key{concepto: cto, alicuota: *r.Alicuota}] += r.Monto
				}
			}

		}

		// Sumo FACTURAS
		if !afipmodels.EsNotaDeCredito(*c.CompID) {
			for _, r := range c.Renglones {
				if r.Concepto == nil {
					continue
				}

				cto := l.ImputacionIVACompras[r.Cuenta]
				if *r.Concepto == iva.BaseImponible {
					sumBI[key{concepto: cto, alicuota: *r.Alicuota}] += r.Monto
					log.Debug().Msgf("Agregando concepto %v", r.Concepto)
					conceptosDeCompra[cto] = struct{}{}
					if cto == "" {
						log.Debug().Msgf("cuenta %v no tenía concepto", r.Cuenta)
					}
				}

				if *r.Concepto == iva.IVA {
					sumIVA[key{concepto: cto, alicuota: *r.Alicuota}] += r.Monto
				}
			}
		}
	}

	p.Ln(15)
	p.SetFont("Arial", "B", 14)
	p.CellFormat(150., 10., tr("Crédito Fiscal generado por operaciones de compras"), "", 0, "ML", false, 0, "")
	p.Ln(8)

	headerHeight := 4.
	renglonHeight := 4.
	ivaCalculado := dec.D2(0)

	// Imprimo resumen de facturas
	for k := range conceptosDeCompra {
		p.SetFont("Arial", "B", 11)
		titulo := ""
		if k == "" {
			titulo = "COMPRAS NO DETERMINADAS"
		} else {
			titulo = k
		}
		p.Cell(8, 10, "")
		p.CellFormat(90., 10., tr(titulo), "", 1, "ML", false, 0, "")
		// Hago un cuadrito para cada condición

		// p.CellFormat(20., 10., tr("Alícuota"), "1", 0, "MR", false, 0, "")
		// p.Ln(4)
		p.SetFont("Arial", "B", 9)
		p.CellFormat(30., headerHeight, "", "TBLR", 0, "MR", false, 0, "")
		p.CellFormat(30., headerHeight, "Neto", "TBLR", 0, "MR", false, 0, "")
		p.CellFormat(30., headerHeight, "IVA", "TBLR", 0, "MR", false, 0, "")
		p.CellFormat(30., headerHeight, "Total", "TBLR", 1, "MR", false, 0, "")

		subTotalIVA := dec.D2(0)
		for _, alic := range iva.Alicuotas() {
			p.SetFont("Arial", "", 9)
			key := key{k, alic.ID}
			base := sumBI[key]

			iv := dec.NewD2(base.Float() * alic.Porcentaje)

			total := base + iv
			if total == 0 {
				continue
			}
			subTotalIVA += iv
			ivaCalculado += iv

			p.CellFormat(30., renglonHeight, alic.ID.String(), "TBLR", 0, "MR", false, 0, "")
			p.CellFormat(30., renglonHeight, base.String(), "TBLR", 0, "MR", false, 0, "")
			p.CellFormat(30., renglonHeight, iv.String(), "TBLR", 0, "MR", false, 0, "")
			p.CellFormat(30., renglonHeight, fmt.Sprint(total), "TBLR", 1, "MR", false, 0, "")
		}
		p.CellFormat(30., renglonHeight, "", "", 0, "MR", false, 0, "")
		p.CellFormat(30., renglonHeight, "Total", "", 0, "MR", false, 0, "")
		p.CellFormat(30., renglonHeight, subTotalIVA.String(), "TBLR", 1, "MR", false, 0, "")
		p.Ln(3)
	}

	p.SetFillColor(192, 225, 231)
	p.CellFormat(90., 5, tr("Total de Crédito Fiscal generado por operaciones de compras"), "TBLR", 0, "MR", false, 0, "")
	p.CellFormat(30., 5, fmt.Sprint(ivaCalculado), "TBLR", 1, "MR", true, 0, "")
	p.Ln(10)

	p.Ln(15)
	p.SetFont("Arial", "B", 14)
	p.CellFormat(150., 10., tr("Débito fiscal a restituir"), "", 0, "ML", false, 0, "")
	p.Ln(8)

	// Notas de crédito de RI
	restituir := dec.D2(0)
	_, hayNotasDeCreditoConRI := condicionesEnNC[operacionesConRI]
	if hayNotasDeCreditoConRI {
		p.SetFont("Arial", "B", 11)
		p.Cell(8, 10, "")
		p.CellFormat(90., 10., tr(operacionesConRI), "", 1, "ML", false, 0, "")
		// Hago un cuadrito para cada condición

		// p.CellFormat(20., 10., tr("Alícuota"), "1", 0, "MR", false, 0, "")
		// p.Ln(4)
		p.SetFont("Arial", "B", 9)
		p.CellFormat(30., headerHeight, "", "TBLR", 0, "MR", false, 0, "")
		p.CellFormat(30., headerHeight, "Neto", "TBLR", 0, "MR", false, 0, "")
		p.CellFormat(30., headerHeight, "IVA (calculado)", "TBLR", 0, "MR", false, 0, "")
		p.CellFormat(30., headerHeight, "Total", "TBLR", 1, "MR", false, 0, "")

		subTotalIVA := dec.D2(0)
		for _, alic := range iva.Alicuotas() {
			p.SetFont("Arial", "", 9)
			key := key{concepto: operacionesConRI, alicuota: alic.ID}
			base := sumBINC[key]

			iv := dec.NewD2(base.Float() * alic.Porcentaje)

			total := base + iv
			if total == 0 {
				continue
			}
			subTotalIVA += iv
			restituir += iv
			// log.Debug().Msgf("sumando IVA NC %v", nc[ivaKey])
			p.CellFormat(30., renglonHeight, alic.ID.String(), "TBLR", 0, "MR", false, 0, "")
			p.CellFormat(30., renglonHeight, base.String(), "TBLR", 0, "MR", false, 0, "")
			p.CellFormat(30., renglonHeight, iv.String(), "TBLR", 0, "MR", false, 0, "")
			p.CellFormat(30., renglonHeight, fmt.Sprint(total), "TBLR", 1, "MR", false, 0, "")
		}
		p.CellFormat(30., renglonHeight, "", "", 0, "MR", false, 0, "")
		p.CellFormat(30., renglonHeight, "Total", "", 0, "MR", false, 0, "")
		p.CellFormat(30., renglonHeight, subTotalIVA.String(), "TBLR", 1, "MR", false, 0, "")
		p.Ln(3)
	}

	// Notas de crédito de Otros
	_, hayNotasDeCreditosConOtros := condicionesEnNC[operacionesConOtros]
	if hayNotasDeCreditosConOtros {
		p.SetFont("Arial", "B", 11)
		p.Cell(8, 10, "")
		p.CellFormat(90., 10., tr(operacionesConOtros), "", 1, "ML", false, 0, "")
		// Hago un cuadrito para cada condición

		// p.CellFormat(20., 10., tr("Alícuota"), "1", 0, "MR", false, 0, "")
		// p.Ln(4)
		p.SetFont("Arial", "B", 9)
		p.CellFormat(30., headerHeight, "", "TBLR", 0, "MR", false, 0, "")
		p.CellFormat(30., headerHeight, "Neto", "TBLR", 0, "MR", false, 0, "")
		p.CellFormat(30., headerHeight, "IVA (calculado)", "TBLR", 0, "MR", false, 0, "")
		p.CellFormat(30., headerHeight, "Total", "TBLR", 1, "MR", false, 0, "")

		subTotalIVA := dec.D2(0)
		for _, alic := range iva.Alicuotas() {
			p.SetFont("Arial", "", 9)
			key := key{concepto: operacionesConOtros, alicuota: alic.ID}
			base := sumBINC[key]

			iv := dec.NewD2(base.Float() * alic.Porcentaje)

			total := base + iv
			if total == 0 {
				continue
			}
			subTotalIVA += iv
			restituir += iv
			p.CellFormat(30., renglonHeight, alic.ID.String(), "TBLR", 0, "MR", false, 0, "")
			p.CellFormat(30., renglonHeight, base.String(), "TBLR", 0, "MR", false, 0, "")
			p.CellFormat(30., renglonHeight, iv.String(), "TBLR", 0, "MR", false, 0, "")
			p.CellFormat(30., renglonHeight, fmt.Sprint(total), "TBLR", 1, "MR", false, 0, "")
		}
		p.CellFormat(30., renglonHeight, "", "", 0, "MR", false, 0, "")
		p.CellFormat(30., renglonHeight, "Total", "", 0, "MR", false, 0, "")
		p.CellFormat(30., renglonHeight, subTotalIVA.String(), "TBLR", 1, "MR", false, 0, "")
		p.Ln(3)
	}

	if len(condicionesEnNC) > 0 {
		p.SetFont("Arial", "B", 9)
		p.SetFillColor(192, 225, 231)
		p.CellFormat(90., 5, tr("Total débito fiscal a restituir"), "TBLR", 0, "MR", false, 0, "")
		p.CellFormat(30., 5, fmt.Sprint(restituir), "TBLR", 1, "MR", true, 0, "")
		p.Ln(10)
	}

	// Sumo IVA por Facturas
	ivaContabilizado := dec.D2(0)
	for _, v := range sumIVA {
		ivaContabilizado += v
	}
	// Sumo IVA por Facturas
	for _, v := range sumIVANC {
		ivaContabilizado += v
	}

	p.CellFormat(120, 5, tr("Total IVA crédito fiscal"), "TBLR", 0, "MR", false, 0, "")
	p.CellFormat(30, 5, fmt.Sprint(ivaCalculado+restituir), "TBLR", 1, "MR", true, 0, "")
	p.Ln(5)

	p.SetFont("Arial", "", 9)
	p.CellFormat(60., 5, tr("IVA contabilizado"), "TBLR", 0, "MR", false, 0, "")
	p.CellFormat(30., 5, fmt.Sprint(ivaContabilizado), "TBLR", 1, "MR", false, 0, "")

	p.CellFormat(60., 5, tr("(IVA calculado) - (IVA contabilizado)"), "TBLR", 0, "MR", false, 0, "")
	p.CellFormat(30., 5, fmt.Sprint(ivaCalculado+restituir-ivaContabilizado), "TBLR", 1, "MR", false, 0, "")
	p.Ln(10)
	return
}
