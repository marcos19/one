package actxlsx

import (
	"fmt"

	"bitbucket.org/marcos19/one/pkg/afip/libroiva"
	"bitbucket.org/marcos19/one/pkg/excel"
	"bitbucket.org/marcos19/one/pkg/iva"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/xuri/excelize/v2"
)

// XLSX exporta el libro a un archivo Excel
func XLSX(l *libroiva.LibroIVA) (out *excelize.File, err error) {
	f, estilos, err := excel.New()
	if err != nil {
		return out, errors.Wrap(err, "creando Excel")
	}
	f.NewSheet(l.Tipo)
	s := l.Tipo

	cabeceras := []string{
		"Item",
		"Fecha original",
		"Código comprobante",
		"Tipo comprobante",
		"Número",
		"Nombre persona",
		"Tipo doc persona",
		"N Doc persona",
		"Condición",
		"Cuenta ID",
		"Cuenta nombre",
		"Concepto",
		"Alícuota",
		"Monto",
		"Imputación",
		"Op ID",
	}
	f.SetSheetRow(s, "a1", &cabeceras)

	// Estilo fecha
	err = f.SetColStyle(s, "B", estilos.Fecha)
	if err != nil {
		return out, errors.Wrap(err, "formateando fecha")
	}

	// Estilo pesos
	err = f.SetColStyle(s, "M:M", estilos.Dinero)
	if err != nil {
		return out, errors.Wrap(err, "formateando dinero")
	}

	// Estilo header
	err = f.SetCellStyle(s, "A1", "R1", estilos.Header)
	if err != nil {
		return out, errors.Wrap(err, "pegando formato header")
	}
	err = f.SetRowHeight(s, 1, 22)
	if err != nil {
		return out, errors.Wrap(err, "dando altura header")
	}
	f.SetColWidth(s, "A", "Z", 14)
	f.SetColWidth(s, "E", "E", 17) // Numero
	f.SetColWidth(s, "F", "F", 36) // Nombre
	f.SetColWidth(s, "I", "I", 22) // Condición
	f.SetColWidth(s, "Q", "Q", 38) // ID

	i := 0
	for _, v := range l.Cabeceras {
		for _, r := range v.Renglones {
			if r.Concepto == nil {
				continue
			}

			r := []interface{}{
				i + 1,
				v.Fecha.Time(),
				*v.CompID,
				v.CompNombre,
				v.NComp,
				v.PersonaNombre,
				*v.TipoIdentificacion,
				*v.NIdentificacion,
				v.CondicionFiscal.Nombre(),
				fmt.Sprint(r.Cuenta),
				l.NombresCuentas[r.Cuenta],
				r.Concepto.StringDetallada(),
				r.Alicuota.String(),
				r.Monto.Float(),
				l.ImputacionIVACompras[r.Cuenta],
				v.ID.String(),
			}
			f.SetSheetRow(s, fmt.Sprintf("A%v", i+2), &r)
			i++
		}
	}
	f.DeleteSheet("Sheet1")

	return f, nil
}

// XLSX exporta el libro a un archivo Excel
func XLSXParaMostrar(l *libroiva.LibroIVA) (out *excelize.File, err error) {
	f, estilos, err := excel.New()
	if err != nil {
		return out, errors.Wrap(err, "creando Excel")
	}
	f.NewSheet(l.Tipo)
	s := l.Tipo

	cabeceras := []string{
		"Item",
		"Fecha original",
		"Código comprobante",
		"Tipo comprobante",
		"Número",
		"Nombre persona",
		"Tipo doc persona",
		"N Doc persona",
		"Condición",
		"Base imponible",
		"IVA",
		"No gravado",
		"Exento",
		"Percepciones",
		"Otros tributos",
		"Total",
		"Op ID",
	}
	f.SetSheetRow(s, "a1", &cabeceras)

	// Estilo fecha
	err = f.SetColStyle(s, "B", estilos.Fecha)
	if err != nil {
		return out, errors.Wrap(err, "formateando fecha")
	}

	// Estilo pesos
	err = f.SetColStyle(s, "J:P", estilos.Dinero)
	if err != nil {
		return out, errors.Wrap(err, "formateando dinero")
	}

	// Estilo header
	err = f.SetCellStyle(s, "A1", "R1", estilos.Header)
	if err != nil {
		return out, errors.Wrap(err, "pegando formato header")
	}
	err = f.SetRowHeight(s, 1, 22)
	if err != nil {
		return out, errors.Wrap(err, "dando altura header")
	}
	f.SetColWidth(s, "A", "Z", 14)
	f.SetColWidth(s, "E", "E", 17) // Numero
	f.SetColWidth(s, "F", "F", 36) // Nombre
	f.SetColWidth(s, "I", "I", 22) // Condición
	f.SetColWidth(s, "Q", "Q", 38) // ID

	for i, v := range l.Cabeceras {
		total := dec.D2(0)
		for _, r := range v.Renglones {

			if r.Concepto == nil {
				continue
			}
			total += r.Monto

			// Es base imponible?
			base := dec.D2(0)
			iv := dec.D2(0)
			noGravado := dec.D2(0)
			perc := dec.D2(0)
			exento := dec.D2(0)
			otrosTributos := dec.D2(0)

			switch *r.Concepto {
			case iva.BaseImponible:
				base = r.Monto
			case iva.IVA:
				iv = r.Monto
			case iva.NoGravado:
				noGravado = r.Monto
			case iva.PercepcionNoCategorizados, iva.PercepcionesISIB, iva.PercepcionesIVA, iva.PercepcionesMunicipales, iva.PercepcionesNacionales:
				perc = r.Monto
			case iva.Exento:
				exento = r.Monto
			case iva.OtrosTributos:
				otrosTributos = r.Monto
			}

			// Percepciones
			noGravado += r.Monto

			r := []interface{}{
				i + 1,
				v.Fecha.Time(),
				*v.CompID,
				v.CompNombre,
				v.NComp,
				v.PersonaNombre,
				*v.TipoIdentificacion,
				*v.NIdentificacion,
				v.CondicionFiscal.Nombre(),
				base.Float(),
				iv.Float(),
				noGravado.Float(),
				exento.Float(),
				perc.Float(),
				otrosTributos.Float(),
				total.Float(),
				v.ID.String(),
			}
			f.SetSheetRow(s, fmt.Sprintf("A%v", i+2), &r)
		}
	}
	f.DeleteSheet("Sheet1")

	return f, nil
}
