package libroivadigital

import (
	"testing"

	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/stretchr/testify/assert"
)

func TestConvertirTXTVenta(t *testing.T) {

	c := VentasCbte{
		Fecha:                   fecha.NewFechaFromInts(2020, 3, 1),
		TipoCbte:                1,
		PuntoVenta:              34,
		Numero:                  156,
		CodigoComprador:         80,
		IdentificacionComprador: 20328896479,
		NombreComprador:         "BORTOLUSSI MARCOS",
		Total:                   dec.NewD2(8500),
		Moneda:                  "PES",
		FechaVto:                fecha.NewFechaFromInts(2020, 4, 1),
		Alics: []VentasAlicuota{
			{
				Neto:     dec.NewD2(1000),
				Alicuota: 5,
				IVA:      dec.NewD2(210),
			},
			{
				Neto:     dec.NewD2(1000),
				Alicuota: 4,
				IVA:      dec.NewD2(105),
			},
		},
	}

	cbte, alics := c.ConvertirTXT()
	assert.Len(t, cbte, 266)
	assert.Len(t, alics, 2)
	for _, v := range alics {
		assert.Len(t, v, 62)
	}
}

func TestAFecha(t *testing.T) {
	f := fecha.NewFechaFromInts(2020, 1, 3)

	assert.Equal(t, "20200103", aFch(f))
}

func TestAInt(t *testing.T) {

	assert.Equal(t, "1", aInt(1, 1))
	assert.Equal(t, "01", aInt(1, 2))
	assert.Equal(t, "0000000001", aInt(1, 10))
}

func TestAStr(t *testing.T) {

	assert.Equal(t, "MARCOS    ", aStr("MARCOS", 10))
	assert.Equal(t, "MÁRCOS    ", aStr("MÁRCOS", 10))
	assert.Equal(t, "MÁR", aStr("MÁRCOS", 3))
	assert.Equal(t, "MAR", aStr("MARCOS", 3))
}

func TestAMonetario(t *testing.T) {

	assert.Equal(t, "001000", aMo2(dec.NewD2(10), 4, 2))
	assert.Equal(t, "001034", aMo2(dec.NewD2(10.34), 4, 2))
	assert.Equal(t, "0000000500", aMo4(dec.NewD4(0.0005), 4, 6))
	assert.Equal(t, "0000005000", aMo4(dec.NewD4(0.005), 4, 6))
	assert.Equal(t, "0000050000", aMo4(dec.NewD4(0.05), 4, 6))
	assert.Equal(t, "0000500000", aMo4(dec.NewD4(0.5), 4, 6))
	assert.Equal(t, "0005000000", aMo4(dec.NewD4(5), 4, 6))
}
