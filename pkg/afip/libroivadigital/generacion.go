package libroivadigital

import (
	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/crosslogic/fecha"
)

func Generar(cfg Config) (err error) {
	if cfg.Comitente == 0 {
		return deferror.Validation("No se definió comitente")
	}
	if cfg.Empresa == 0 {
		return deferror.Validation("No se definió empresa")
	}
	if cfg.Desde == 0 {
		return deferror.Validation("No se definió fecha desde")
	}
	if cfg.Hasta == 0 {
		return deferror.Validation("No se definió fecha hasta")
	}

	return
}

type Config struct {
	Comitente int
	Empresa   int
	Desde     fecha.Fecha
	Hasta     fecha.Fecha
	Libro     TipoLibro
}

type TipoLibro string

const (
	Ventas  = "Ventas"
	Compras = "Compras"
)
