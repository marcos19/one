package libroivadigital

import (
	"strings"

	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
)

type ComprasCbte struct {
	Fecha                      fecha.Fecha
	TipoCbte                   int
	PuntoVenta                 int
	Numero                     int
	Despacho                   string
	NumeroHasta                int
	CodigoComprador            int
	IdentificacionComprador    int
	NombreComprador            string
	Total                      dec.D2
	ConceptosNoIntegran        dec.D2
	OperacionesExentas         dec.D2
	PercepcionesIVA            dec.D2
	PercepcionesNacionales     dec.D2
	PercepcionesISIB           dec.D2
	PercepcionesMunicipales    dec.D2
	ImpuestosInternos          dec.D2
	Moneda                     string
	TipoCambio                 dec.D4
	CantidadAlicuotas          int
	CodigoOperacion            string // Dejo vacío
	CreditoFiscalComputable    dec.D2
	OtrosTributos              dec.D2
	CuitEmisorCorredor         int
	DenominacionEmisorCorredor string
	ComisionCorredor           dec.D2
	Alics                      []ComprasAlicuota
}

type ComprasAlicuota struct {
	Neto     dec.D2
	Alicuota int
	IVA      dec.D2
}

func (c ComprasCbte) ConvertirTXT() (cab string, alics []string) {
	b := strings.Builder{}
	b.WriteString(aFch(c.Fecha))
	b.WriteString(aInt(c.TipoCbte, 3))
	b.WriteString(aInt(c.PuntoVenta, 5))
	b.WriteString(aInt(c.Numero, 20))
	b.WriteString(aStr(c.Despacho, 16))
	b.WriteString(aInt(c.CodigoComprador, 2))
	b.WriteString(aInt(c.IdentificacionComprador, 20))
	b.WriteString(aStr(c.NombreComprador, 30))
	b.WriteString(aMo2(c.Total, 13, 2))
	b.WriteString(aMo2(c.ConceptosNoIntegran, 13, 2))
	b.WriteString(aMo2(c.OperacionesExentas, 13, 2))
	b.WriteString(aMo2(c.PercepcionesIVA, 13, 2))
	b.WriteString(aMo2(c.PercepcionesNacionales, 13, 2))
	b.WriteString(aMo2(c.PercepcionesISIB, 13, 2))
	b.WriteString(aMo2(c.PercepcionesMunicipales, 13, 2))
	b.WriteString(aMo2(c.ImpuestosInternos, 13, 2))
	b.WriteString(aStr(c.Moneda, 3))
	b.WriteString(aMo4(c.TipoCambio, 4, 6))
	b.WriteString(aInt(c.CantidadAlicuotas, 1))
	b.WriteString(aStr(c.CodigoOperacion, 1))
	b.WriteString(aMo2(c.CreditoFiscalComputable, 13, 2))
	b.WriteString(aMo2(c.OtrosTributos, 13, 2))
	b.WriteString(aInt(c.CuitEmisorCorredor, 11))
	b.WriteString(aStr(c.DenominacionEmisorCorredor, 30))
	b.WriteString(aMo2(c.ComisionCorredor, 13, 2))

	cab = b.String()

	for _, v := range c.Alics {
		b := strings.Builder{}
		b.WriteString(aInt(c.TipoCbte, 3))
		b.WriteString(aInt(c.PuntoVenta, 5))
		b.WriteString(aInt(c.Numero, 20))
		b.WriteString(aInt(c.CodigoComprador, 2))
		b.WriteString(aInt(c.IdentificacionComprador, 20))
		b.WriteString(aMo2(v.Neto, 13, 2))
		b.WriteString(aInt(v.Alicuota, 4))
		b.WriteString(aMo2(v.IVA, 13, 2))

		alics = append(alics, b.String())
	}
	return cab, alics
}
