package libroivadigital

import (
	"fmt"

	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
)

func aFch(fch fecha.Fecha) (out string) {
	formateada := fch.Time().Format("20060102")
	return formateada
}

func aInt(n int, digitos int) (out string) {
	if digitos == 0 {
		return ""
	}
	nStr := fmt.Sprint(n)
	cantidad := digitos - len(nStr)
	for i := 1; i <= cantidad; i++ {
		out += "0"
	}
	return out + nStr
}

func aStr(str string, largo int) (out string) {
	rr := []rune(str)
	cantidad := largo - len(rr)
	if cantidad < 0 {
		return string(rr[:largo])
	}

	for i := 1; i <= cantidad; i++ {
		rr = append(rr, ' ')
	}
	return string(rr)
}

func aMo2(n dec.D2, enteros, decimales int) string {
	return aInt(int(n), enteros+decimales)
}
func aMo4(n dec.D4, enteros, decimales int) (out string) {
	return aInt(int(n)*100, enteros+decimales)
}
