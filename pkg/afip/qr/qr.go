package qr

import (
	"encoding/base64"
	"encoding/json"
	"fmt"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/cuits"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
)

// func BarCode(cuit, tipoComp, puntoDeVenta, cae int, vtoCae fecha.Fecha) string {
// 	fmt.Sprintf("%v%v",cuit, tipoComp, cae, )
// }

// func llenarCeros(n, digitos int) (out string, err error) {
// 	if digitos == 0 {
// 		return "", nil
// 	}
// 	nStr := fmt.Sprint(n)
// 	if len(nStr) > digitos {
// 		return out, errors.Errorf("había más dígitos (%v) que espacios (%v)", len(nStr), digitos)
// 	}
// 	cantidad := digitos - len(nStr)
// 	for i := 1; i <= cantidad; i++ {
// 		out += "0"
// 	}
// 	return out + nStr, nil
// }

type Data struct {
	Version         int         `json:"ver"`
	Fecha           fecha.Fecha `json:"fecha"`
	CUIT            cuits.CUIT  `json:"cuit"`
	PuntoDeVenta    int         `json:"ptoVta"`
	TipoComp        int         `json:"tipoCmp"`
	NComp           int         `json:"nroCmp"`
	Importe         dec.D2      `json:"importe"`
	Moneda          string      `json:"moneda"`
	Cotizacion      dec.D4      `json:"ctz"`
	TipoDocReceptor int         `json:"tipoDocRec"`
	NDocReceptor    int         `json:"nroDocRec"`
	TipoCodAut      string      `json:"tipoCodAut"`
	CAE             int         `json:"codAut"`
}

// Devuelve el string que debe ir en el QR de las facturas.
func Version1(data Data) (out string, err error) {

	// Valido
	if data.Version == 0 {
		data.Version = 1
	}
	if data.Fecha == 0 {
		return out, deferror.Validation("la fecha no puede ser cero")
	}
	if data.CUIT == 0 {
		return out, deferror.Validation("CUIT no puede ser cero")
	}
	if !data.CUIT.Valid() {
		return out, deferror.Validation("CUIT es inválido")
	}
	if data.PuntoDeVenta <= 0 || data.PuntoDeVenta > 99999 {
		return out, deferror.Validation("punto de venta inválido")
	}
	if data.TipoComp <= 0 || data.TipoComp > 999 {
		return out, deferror.Validation("tipo de comprobante inválido")
	}
	if data.NComp <= 0 || data.NComp > 999999999 {
		return out, deferror.Validation("tipo de comprobante inválido")
	}
	if data.Importe > 999999999999999 {
		return out, deferror.Validation("monto supera máximo")
	}
	if data.Importe < 0 {
		return out, deferror.Validation("monto era negativo")
	}
	// Factura electrónica permite autorizar con monto cero
	//if data.Importe == 0 {
	//return out, deferror.Validation("monto era cero")
	//}
	if len(data.Moneda) != 3 {
		return out, deferror.Validation("moneda inválida")
	}
	if data.Cotizacion == 0 {
		return out, deferror.Validation("cotización era cero")
	}
	if data.Moneda == "PES" && data.Cotizacion != dec.NewD4(1) {
		return out, deferror.Validation("si la moneda es PES, la cotización debe ser 1")
	}
	if data.TipoDocReceptor < 0 || data.TipoDocReceptor > 99 {
		return out, deferror.Validation("tipo documento receptor debe estar entre 0 y 99")
	}
	if len(fmt.Sprint(data.NDocReceptor)) > 20 {
		return out, deferror.Validation("número documento receptor no puede ser mayor a 20")
	}
	switch data.TipoCodAut {
	case "A", "E":
		// ok
	default:
		return out, deferror.Validation("tipo de código de autorización debe ser E o A")
	}
	if len(fmt.Sprint(data.CAE)) > 14 {
		return out, deferror.Validation("CAE tenía más de 14 dígitos")
	}

	// Hago JSON
	j, err := toJSON(data)
	if err != nil {
		return "", errors.Wrap(err, "codificando JSON")
	}

	// Codifico Base64
	b64 := base64.StdEncoding.EncodeToString(j)

	out = fmt.Sprintf("%v?p=%v", "https://www.afip.gob.ar/fe/qr/", b64)
	return
}

func toJSON(data Data) (out []byte, err error) {

	out, err = json.Marshal(&data)
	if err != nil {
		return out, errors.Wrap(err, "marshalizando")
	}
	return

}
