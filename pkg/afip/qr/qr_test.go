package qr

import (
	"testing"

	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/stretchr/testify/assert"
)

func TestQR(t *testing.T) {
	data := Data{
		Version:         1,
		Fecha:           fecha.Fecha(20201013),
		CUIT:            30000000007,
		PuntoDeVenta:    10,
		TipoComp:        1,
		NComp:           94,
		Importe:         dec.NewD2(12100),
		Moneda:          "DOL",
		Cotizacion:      dec.NewD4(65),
		TipoDocReceptor: 80,
		NDocReceptor:    20000000001,
		TipoCodAut:      "E",
		CAE:             70417054367476,
	}

	{ // JSON
		enJSON, err := toJSON(data)
		assert.Nil(t, err)
		expected := `{"ver":1,"fecha":"2020-10-13","cuit":30000000007,"ptoVta":10,"tipoCmp":1,"nroCmp":94,"importe":12100,"moneda":"DOL","ctz":65,"tipoDocRec":80,"nroDocRec":20000000001,"tipoCodAut":"E","codAut":70417054367476}`
		assert.Equal(t, expected, string(enJSON))
	}

	{ // El código final
		enQR, err := Version1(data)
		assert.Nil(t, err)

		expected := `https://www.afip.gob.ar/fe/qr/?p=eyJ2ZXIiOjEsImZlY2hhIjoiMjAyMC0xMC0xMyIsImN1aXQiOjMwMDAwMDAwMDA3LCJwdG9WdGEiOjEwLCJ0aXBvQ21wIjoxLCJucm9DbXAiOjk0LCJpbXBvcnRlIjoxMjEwMCwibW9uZWRhIjoiRE9MIiwiY3R6Ijo2NSwidGlwb0RvY1JlYyI6ODAsIm5yb0RvY1JlYyI6MjAwMDAwMDAwMDEsInRpcG9Db2RBdXQiOiJFIiwiY29kQXV0Ijo3MDQxNzA1NDM2NzQ3Nn0=`
		assert.Equal(t, expected, enQR)

	}
}
