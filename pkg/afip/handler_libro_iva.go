package afip

import (
	"context"
	"fmt"
	"log"
	"regexp"
	"sort"
	"strings"

	"bitbucket.org/marcos19/one/pkg/afip/afipmodels"
	"bitbucket.org/marcos19/one/pkg/afip/libroivadigital"
	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/excel"
	"bitbucket.org/marcos19/one/pkg/iva"
	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/xuri/excelize/v2"
)

type LibroIVADigitalReq struct {
	Comitente int
	Empresa   int `json:",string"`
	Desde     fecha.Fecha
	Hasta     fecha.Fecha
	Tipo      string

	AbrirCuentas bool
}

type LibroIVADigitalResponse struct {
	ComprasCabeceras string
	ComprasAlicuotas string
	VentasCabeceras  string
	VentasAlicuotas  string
	Resumen          struct {
		VentasNeto        dec.D2
		VentasNoGravado   dec.D2
		VentasIVA         dec.D2
		VentasNCNeto      dec.D2
		VentasNCNoGravado dec.D2
		VentasNCIVA       dec.D2

		ComprasNeto        dec.D2
		ComprasNoGravado   dec.D2
		ComprasIVA         dec.D2
		ComprasNCNeto      dec.D2
		ComprasNCNoGravado dec.D2
		ComprasNCIVA       dec.D2
	}
}

func (h *Handler) LibroIVADigital(ctx context.Context, req LibroIVADigitalReq) (out LibroIVADigitalResponse, err error) {

	// valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó id de comitente")
	}
	if req.Empresa == 0 {
		return out, deferror.Validation("no se ingresó id de empresa")
	}
	if req.Desde == 0 {
		return out, deferror.Validation("no se ingresó fecha desde")
	}
	if req.Hasta == 0 {
		return out, deferror.Validation("no se ingresó fecha hasta")
	}

	switch req.Tipo {
	case libroivadigital.Compras, "Ambos":
		// Busco compras en DB
		cc, rr, err := h.ivaRegistros(ctx, req, "C")
		if err != nil {
			return out, errors.Wrap(err, "buscando registros")
		}

		// Formateo IVA digital
		out.ComprasCabeceras, out.ComprasAlicuotas, err = ivaComprasDigital(cc, rr)
		if err != nil {
			return out, errors.Wrap(err, "formateando registros")
		}

		// Datos resumen
		for _, v := range cc {
			if afipmodels.EsNotaDeCredito(*v.AFIPComprobanteID) {
				alics := rr[v.ID]
				for _, alic := range alics {
					switch alic.Concepto {

					case iva.BaseImponible:
						out.Resumen.ComprasNCNeto += alic.Monto

					case iva.IVA:
						out.Resumen.ComprasNCIVA += alic.Monto

					default:
						out.Resumen.ComprasNCNoGravado += alic.Monto
					}
				}
			}

			if !afipmodels.EsNotaDeCredito(*v.AFIPComprobanteID) {
				alics := rr[v.ID]
				for _, alic := range alics {
					switch alic.Concepto {

					case iva.BaseImponible:
						out.Resumen.ComprasNeto += alic.Monto

					case iva.IVA:
						out.Resumen.ComprasIVA += alic.Monto

					default:
						out.Resumen.ComprasNoGravado += alic.Monto
					}
				}
			}
		}

	}

	switch req.Tipo {
	case libroivadigital.Ventas, "Ambos":
		// Busco ventas en DB
		cc, rr, err := h.ivaRegistros(ctx, req, "V")
		if err != nil {
			return out, errors.Wrap(err, "buscando registros")
		}

		// Formateo IVA digital
		out.VentasCabeceras, out.VentasAlicuotas, err = ivaVentasDigital(cc, rr)
		if err != nil {
			return out, errors.Wrap(err, "formateando registros")
		}

		// Datos resumen
		for _, v := range cc {
			if afipmodels.EsNotaDeCredito(*v.AFIPComprobanteID) {
				alics := rr[v.ID]
				for _, alic := range alics {
					switch alic.Concepto {

					case iva.BaseImponible:
						out.Resumen.VentasNCNeto += alic.Monto

					case iva.IVA:
						out.Resumen.VentasNCIVA += alic.Monto

					default:
						out.Resumen.VentasNCNoGravado += alic.Monto
					}
				}
			}

			if !afipmodels.EsNotaDeCredito(*v.AFIPComprobanteID) {
				alics := rr[v.ID]
				for _, alic := range alics {
					switch alic.Concepto {

					case iva.BaseImponible:
						out.Resumen.VentasNeto += alic.Monto

					case iva.IVA:
						out.Resumen.VentasIVA += alic.Monto

					default:
						out.Resumen.VentasNoGravado += alic.Monto
					}
				}
			}
		}
	}

	return
}

func (h *Handler) LibroIVADigitalExcel(ctx context.Context, req LibroIVADigitalReq) (out *excelize.File, err error) {

	log.Println("Entrando a libro EXCEL")
	// valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó id de comitente")
	}
	if req.Empresa == 0 {
		return out, deferror.Validation("no se ingresó id de empresa")
	}
	if req.Desde == 0 {
		return out, deferror.Validation("no se ingresó fecha desde")
	}
	if req.Hasta == 0 {
		return out, deferror.Validation("no se ingresó fecha hasta")
	}

	libro := ""
	switch req.Tipo {
	case libroivadigital.Compras:
		libro = "C"
	case libroivadigital.Ventas:
		libro = "V"
	}

	// Busco compras en DB
	cc, rr, err := h.ivaRegistrosAgrupados(ctx, req, libro)
	if err != nil {
		return out, errors.Wrap(err, "buscando registros")
	}

	// Ordeno comps
	oo := []ops.Op{}
	for _, v := range cc {
		oo = append(oo, v)
	}
	sort.Slice(oo, func(i, j int) bool {
		return oo[i].FechaOriginal < oo[j].FechaOriginal
	})

	out, estilos, err := excel.New()
	if err != nil {
		return out, errors.Wrap(err, "creando Excel")
	}
	s := req.Tipo
	out.NewSheet(s)

	renglonIdx := 1
	cabeceras := []string{
		"Item",
		"Fecha original",
		"Codigo comprobante",
		"Tipo comprobante",
		"Punto de venta",
		"Número",
		"Nombre persona",
		"Tipo doc persona",
		"N Doc persona",
		"Moneda",
		"Tipo de cambio",
		"Cuenta número",
		"Cuenta nombre",
		"Concepto",
		"Alícuota",
		"Monto",
	}
	out.SetSheetRow(s, "a1", &cabeceras)

	// Estilo fecha
	err = out.SetColStyle(s, "B", estilos.Fecha)
	if err != nil {
		return out, errors.Wrap(err, "formateando fecha")
	}

	// Estilo pesos
	err = out.SetColStyle(s, "P", estilos.Dinero)
	if err != nil {
		return out, errors.Wrap(err, "formateando dinero")
	}

	// Estilo header
	err = out.SetCellStyle(s, "A1", "P1", estilos.Header)
	if err != nil {
		return out, errors.Wrap(err, "pegando formato header")
	}
	err = out.SetRowHeight(s, 1, 22)
	if err != nil {
		return out, errors.Wrap(err, "dando altura header")
	}
	out.SetColWidth(s, "A", "Z", 12)
	out.SetColWidth(s, "G", "G", 36)

	for compIdx, op := range oo {
		compIdx++
		comp := h.Store.comps[*op.AFIPComprobanteID]
		for _, renglon := range rr[op.ID] {
			renglonIdx++

			cta := cuentas.Cuenta{}
			if req.AbrirCuentas {
				cta, err = h.cuentas.ReadOne(ctx, cuentas.ReadOneReq{
					Comitente: req.Comitente,
					ID:        renglon.Cuenta,
				})
				if err != nil {
					return out, errors.Wrap(err, "buscando cuenta contable")
				}
			}
			r := []interface{}{
				compIdx,
				op.FechaOriginal.Time(),
				*op.AFIPComprobanteID,
				comp.Nombre,
				op.PuntoDeVenta,
				op.NComp,
				op.PersonaNombre,
				*op.TipoIdentificacion,
				*op.NIdentificacion,
				op.TipoMoneda,
				op.TipoCambio.Float(),
				cta.Codigo,
				cta.Nombre,
				renglon.Concepto.StringDetallada(),
				renglon.Alicuota.String(),
				renglon.Monto.Float(),
			}
			out.SetSheetRow(s, fmt.Sprintf("A%v", renglonIdx), &r)
			// out.SetCellValue(s, fmt.Sprintf("a%v", count), op.FechaOriginal.Time())
			// out.SetCellInt(s, fmt.Sprintf("b%v", count), *op.AFIPComprobanteID)
			// out.SetCellStr(s, fmt.Sprintf("b%v", count), comp.Nombre)
			// out.SetCellInt(s, fmt.Sprintf("c%v", count), op.PuntoDeVenta)
			// out.SetCellInt(s, fmt.Sprintf("d%v", count), op.NComp)
			// out.SetCellStr(s, fmt.Sprintf("e%v", count), op.PersonaNombre)
			// out.SetCellInt(s, fmt.Sprintf("f%v", count), *op.TipoIdentificacion)
			// out.SetCellInt(s, fmt.Sprintf("g%v", count), *op.NIdentificacion)
			// out.SetCellStr(s, fmt.Sprintf("h%v", count), op.TipoMoneda)
			// out.SetCellFloat(s, fmt.Sprintf("i%v", count), op.TipoCambio.Float(), -1, 64)
			// out.SetCellInt(s, fmt.Sprintf("j%v", count), renglon.Cuenta)
			// out.SetCellInt(s, fmt.Sprintf("k%v", count), int(renglon.Concepto))
			// out.SetCellInt(s, fmt.Sprintf("l%v", count), int(renglon.Alicuota))
			// out.SetCellFloat(s, fmt.Sprintf("m%v", count), renglon.Monto.Float(), -1, 64)
		}
	}

	return
}

type value struct {
	Concepto iva.Concepto
	Alicuota iva.Alicuota
	Monto    dec.D2
	Cuenta   int
}

func ivaComprasDigital(cabeceras map[uuid.UUID]ops.Op, renglones map[uuid.UUID][]value) (cabs string, reng string, err error) {
	cabBuilder := strings.Builder{}
	alicsBuilder := strings.Builder{}

	// Creo los DTO que usa libroivadigital
	for _, op := range cabeceras {

		v := libroivadigital.ComprasCbte{}
		v.Fecha = op.FechaOriginal
		if op.AFIPComprobanteID == nil {
			return cabs, reng, errors.Errorf("comprobante %v no tenía definido AfipComprobanteID", op.ID)
		}
		v.TipoCbte = *op.AFIPComprobanteID
		v.PuntoVenta = op.PuntoDeVenta
		v.Numero = op.NComp
		if op.TipoIdentificacion == nil {
			return cabs, reng, errors.Errorf("comprobante %v no tenía definido tipo de identificación", op.ID)
		}
		v.CodigoComprador = *op.TipoIdentificacion
		if op.NIdentificacion == nil {
			return cabs, reng, errors.Errorf("comprobante %v no tenía definido número de identificación", op.ID)
		}
		v.IdentificacionComprador = *op.NIdentificacion
		v.NombreComprador = sanitize(op.PersonaNombre)
		// if v.NombreComprador == "" {
		// 	per, err := h.ReadOne(personas.ReadOneReq{
		// 		Comitente: op.Comitente,
		// 		ID:        *op.Persona,
		// 	})
		// 	if err != nil {
		// 		return cabs, reng, errors.Wrap(err, "buscando persona para determinar su denominación")
		// 	}
		// 	v.NombreComprador = per.Nombre
		// }

		// Genero alics
		netos := map[iva.Alicuota]dec.D2{}
		ivas := map[iva.Alicuota]dec.D2{}
		alics := map[iva.Alicuota]struct{}{}

		for _, w := range renglones[op.ID] {

			switch w.Alicuota {
			case 3, 4, 5, 6, 8, 9:
				alics[w.Alicuota] = struct{}{}
			}

			switch w.Concepto {

			case iva.BaseImponible:
				netos[w.Alicuota] += w.Monto

			case iva.IVA:
				ivas[w.Alicuota] += w.Monto
				v.CreditoFiscalComputable += w.Monto

			case iva.NoGravado:
				v.ConceptosNoIntegran += w.Monto

			case iva.Exento:
				v.OperacionesExentas += w.Monto

			case iva.PercepcionesIVA:
				v.PercepcionesIVA += w.Monto

			case iva.PercepcionesNacionales:
				v.PercepcionesNacionales += w.Monto

			case iva.PercepcionesISIB:
				v.PercepcionesISIB += w.Monto

			case iva.PercepcionesMunicipales:
				v.PercepcionesMunicipales += w.Monto

			case iva.ImpuestosInternos:
				v.ImpuestosInternos += w.Monto

			case iva.OtrosTributos:
				v.OtrosTributos += w.Monto

			default:
				return cabs, reng, errors.Errorf("no se pudo determinar categoría de concepto %v en op %v", w.Alicuota, op.ID)
			}
			v.Total += w.Monto
		}

		signo := dec.D2(1)
		if afipmodels.EsNotaDeCredito(v.TipoCbte) {
			signo = dec.D2(-1)
		}
		if v.Total < 0 {
			v.ConceptosNoIntegran *= signo
			v.CreditoFiscalComputable *= signo
			v.OperacionesExentas *= signo
			v.PercepcionesIVA *= signo
			v.PercepcionesNacionales *= signo
			v.PercepcionesISIB *= signo
			v.PercepcionesMunicipales *= signo
			v.ImpuestosInternos *= signo
			v.OtrosTributos *= signo
			v.Total *= signo
		}

		v.Moneda = op.TipoMoneda
		if op.TipoMoneda == "" {
			v.Moneda = "PES"
		}
		if v.Moneda == "PES" {
			v.TipoCambio = dec.NewD4(1)
		} else {
			v.TipoCambio = op.TipoCambio
		}
		v.CantidadAlicuotas = len(alics)
		for k := range alics {

			a := libroivadigital.ComprasAlicuota{
				Alicuota: int(k),
				Neto:     netos[k] * signo,
				IVA:      ivas[k] * signo,
			}
			v.Alics = append(v.Alics, a)

		}

		cab, aa := v.ConvertirTXT()
		cabBuilder.WriteString(cab)
		cabBuilder.WriteString("\n")

		for _, v := range aa {
			alicsBuilder.WriteString(v)
			alicsBuilder.WriteString("\n")
		}
	}
	cabs = cabBuilder.String()
	reng = alicsBuilder.String()
	return
}

func ivaVentasDigital(cabeceras map[uuid.UUID]ops.Op, renglones map[uuid.UUID][]value) (cabs string, reng string, err error) {
	cabBuilder := strings.Builder{}
	alicsBuilder := strings.Builder{}

	// Creo los DTO que usa libroivadigital
	for _, op := range cabeceras {

		v := libroivadigital.VentasCbte{}
		v.Fecha = op.FechaOriginal
		if op.AFIPComprobanteID == nil {
			return cabs, reng, errors.Errorf("comprobante %v no tenía definido AfipComprobanteID", op.ID)
		}
		v.TipoCbte = *op.AFIPComprobanteID
		v.PuntoVenta = op.PuntoDeVenta
		v.Numero = op.NComp
		if op.TipoIdentificacion == nil {
			return cabs, reng, errors.Errorf("comprobante %v no tenía definido tipo de identificación", op.ID)
		}
		v.NumeroHasta = v.Numero
		v.CodigoComprador = *op.TipoIdentificacion
		if op.NIdentificacion == nil {
			return cabs, reng, errors.Errorf("comprobante %v no tenía definido número de identificación", op.ID)
		}
		v.IdentificacionComprador = *op.NIdentificacion
		v.NombreComprador = sanitize(op.PersonaNombre)

		// Si no dice nada => Pesos
		v.Moneda = op.TipoMoneda
		if v.Moneda == "" {
			v.Moneda = "PES"
		}

		// Si es pesos, tipo de cambio tiene que ser 1
		v.TipoCambio = op.TipoCambio
		if v.Moneda == "PES" {
			v.TipoCambio = dec.NewD4(1)
		}

		// Genero alics
		netos := map[iva.Alicuota]dec.D2{}
		ivas := map[iva.Alicuota]dec.D2{}
		alics := map[iva.Alicuota]struct{}{}

		for _, w := range renglones[op.ID] {

			switch w.Alicuota {
			case 3, 4, 5, 6, 8, 9:
				alics[w.Alicuota] = struct{}{}
			}

			// En contabilidad todas las partidas de ventas están en negativo,
			// => les doy vuelta el signo
			w.Monto *= -1

			switch w.Concepto {

			case iva.BaseImponible:
				netos[w.Alicuota] += w.Monto

			case iva.IVA:
				ivas[w.Alicuota] += w.Monto

			case iva.NoGravado:
				v.ConceptosNoIntegran += w.Monto

			case iva.Exento:
				v.OperacionesExentas += w.Monto

			case iva.PercepcionesNacionales:
				v.PercepcionesNacionales += w.Monto

			case iva.PercepcionesISIB:
				v.PercepcionesISIB += w.Monto

			case iva.PercepcionesMunicipales:
				v.PercepcionesMunicipales += w.Monto

			case iva.ImpuestosInternos:
				v.ImpuestosInternos += w.Monto

			case iva.OtrosTributos:
				v.OtrosTributos += w.Monto

			default:
				continue
			}
			v.Total += w.Monto
		}

		// Corrección de signo para notas de crédito
		signo := dec.D2(1)
		if afipmodels.EsNotaDeCredito(v.TipoCbte) {
			signo = dec.D2(-1)
		}
		if v.Total < 0 {
			v.ConceptosNoIntegran *= dec.D2(signo)
			v.OperacionesExentas *= dec.D2(signo)
			v.PercepcionesNacionales *= dec.D2(signo)
			v.PercepcionesISIB *= dec.D2(signo)
			v.PercepcionesMunicipales *= dec.D2(signo)
			v.ImpuestosInternos *= dec.D2(signo)
			v.OtrosTributos *= dec.D2(signo)
			v.Total *= dec.D2(signo)
		}

		// Si no hay alícuotas debe informarse como NO GRAVADO
		// Exige que la cantidad de alícuotas sean mayor a cero, por lo que
		// le agrego una alicuota código 0% con valor 0%
		if len(alics) == 0 {
			v.CodigoOperacion = "N"
			alics[iva.IVA0] = struct{}{}
		}
		v.CantidadAlicuotas = len(alics)
		v.FechaVto = v.Fecha
		for k := range alics {
			a := libroivadigital.VentasAlicuota{
				Alicuota: int(k),
				Neto:     netos[k] * signo,
				IVA:      ivas[k] * signo,
			}
			v.Alics = append(v.Alics, a)
		}

		cab, aa := v.ConvertirTXT()
		cabBuilder.WriteString(cab)
		cabBuilder.WriteString("\n")

		for _, v := range aa {
			alicsBuilder.WriteString(v)
			alicsBuilder.WriteString("\n")
		}
	}
	cabs = cabBuilder.String()
	reng = alicsBuilder.String()
	return
}

func (h *Handler) ivaRegistrosAgrupados(ctx context.Context, req LibroIVADigitalReq, libro string) (cabeceras map[uuid.UUID]ops.Op, renglones map[uuid.UUID][]value, err error) {

	query := `
		SELECT
			ops.id, ops.fecha_original, ops.afip_comprobante_id, ops.punto_de_venta, ops.n_comp,
			ops.tipo_identificacion, ops.n_identificacion, ops.persona_nombre,
			ops.tipo_moneda, ops.tipo_cambio, 
			partidas.concepto_iva, sum(partidas.monto) as monto
		FROM partidas
		INNER JOIN ops ON ops.id = partidas.op_id
		WHERE
			partidas.fecha_contable >= $1 AND partidas.fecha_contable <= $2 AND
			partidas.comitente = $3 AND
			partidas.empresa = $4 AND
			ops.comp_id IN (
				SELECT id FROM comps WHERE compra_o_venta = $5 AND comitente = $3
			) AND
			partidas.alicuota_iva IS NOT NULL AND
			partidas.concepto_iva IS NOT NULL

		GROUP BY 
			ops.id, ops.fecha_original, ops.afip_comprobante_id, ops.punto_de_venta, ops.n_comp,
			ops.tipo_identificacion, ops.n_identificacion, ops.persona_nombre,
			ops.tipo_moneda, ops.tipo_cambio,
		    partidas.concepto_iva
		;`
	// Busco
	rows, err := h.conn.Query(ctx, query, req.Desde, req.Hasta, req.Comitente, req.Empresa, libro)
	if err != nil {
		return cabeceras, renglones, deferror.DB(err, "buscando ops")
	}
	defer rows.Close()

	cabeceras = map[uuid.UUID]ops.Op{}
	renglones = map[uuid.UUID][]value{}

	// Creo map de cabeceras y alicuotas
	for rows.Next() {
		op := ops.Op{}
		value := value{}
		monto := 0
		err = rows.Scan(
			&op.ID, &op.FechaOriginal, &op.AFIPComprobanteID, &op.PuntoDeVenta, &op.NComp,
			&op.TipoIdentificacion, &op.NIdentificacion, &op.PersonaNombre,
			&op.TipoMoneda, &op.TipoCambio,
			&value.Concepto, &monto,
		)
		if err != nil {
			return cabeceras, renglones, deferror.DB(err, "escaneando comprobantes de compra")
		}
		value.Monto = dec.D2(float64(monto))

		cabeceras[op.ID] = op
		anterior := renglones[op.ID]
		anterior = append(anterior, value)
		renglones[op.ID] = anterior
	}

	return
}

func (h *Handler) ivaRegistros(ctx context.Context, req LibroIVADigitalReq, libro string) (cabeceras map[uuid.UUID]ops.Op, renglones map[uuid.UUID][]value, err error) {

	query := `SELECT
			ops.id, ops.fecha_original, ops.afip_comprobante_id, ops.punto_de_venta, ops.n_comp,
			ops.tipo_identificacion, ops.n_identificacion, ops.persona_nombre,
			ops.tipo_moneda, ops.tipo_cambio, partidas.cuenta,  
			partidas.alicuota_iva, partidas.concepto_iva, sum(partidas.monto) as monto
		FROM partidas
		INNER JOIN ops ON ops.id = partidas.op_id
		WHERE
			partidas.fecha_contable >= $1 AND partidas.fecha_contable <= $2 AND
			partidas.comitente = $3 AND
			partidas.empresa = $4 AND
			ops.comp_id IN (
				SELECT id FROM comps WHERE compra_o_venta = $5 AND comitente = $3
			) AND
			partidas.alicuota_iva IS NOT NULL AND
			partidas.concepto_iva IS NOT NULL

		GROUP BY 
			ops.id, ops.fecha_original, ops.afip_comprobante_id, ops.punto_de_venta, ops.n_comp,
			ops.tipo_identificacion, ops.n_identificacion, ops.persona_nombre,
			ops.tipo_moneda, ops.tipo_cambio, partidas.cuenta,
			partidas.alicuota_iva, partidas.concepto_iva
		;`

	// Busco
	rows, err := h.conn.Query(ctx, query, req.Desde, req.Hasta, req.Comitente, req.Empresa, libro)
	if err != nil {
		return cabeceras, renglones, deferror.DB(err, "buscando ops")
	}
	defer rows.Close()

	cabeceras = map[uuid.UUID]ops.Op{}
	renglones = map[uuid.UUID][]value{}

	// Creo map de cabeceras y alicuotas
	for rows.Next() {
		op := ops.Op{}
		value := value{}
		monto := 0
		err = rows.Scan(
			&op.ID, &op.FechaOriginal, &op.AFIPComprobanteID, &op.PuntoDeVenta, &op.NComp,
			&op.TipoIdentificacion, &op.NIdentificacion, &op.PersonaNombre,
			&op.TipoMoneda, &op.TipoCambio, &value.Cuenta,
			&value.Alicuota, &value.Concepto, &monto,
		)
		if err != nil {
			return cabeceras, renglones, deferror.DB(err, "escaneando comprobantes de compra")
		}
		value.Monto = dec.D2(float64(monto))

		cabeceras[op.ID] = op
		anterior := renglones[op.ID]
		anterior = append(anterior, value)
		renglones[op.ID] = anterior
	}

	return
}

func sanitize(in string) (out string) {
	out = strings.Replace(in, "\t", "", -1)
	out = strings.Replace(out, "\n", "", -1)

	reg, err := regexp.Compile("[^a-zA-Z0-9]+")
	if err != nil {
		log.Fatal(err)
	}
	out = reg.ReplaceAllString(out, " ")
	return
}
