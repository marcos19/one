package afipmodels

// ComprobanteAFIP es el listado de comprobantes que tiene definidos
// la AFIP.
type ComprobanteAFIP struct {
	ID                    int `json:",string"`
	Nombre                string
	Letra                 *string
	PuntoVentaObligatorio *bool
}

func (c ComprobanteAFIP) TableName() string {
	return "afip_comprobantes"
}

// En base al ID del comprobante determina es una nota de crédito
func EsNotaDeCredito(id int) bool {
	switch id {
	case 3, 8, 13, 21, 38, 53, 90, 112, 113, 114, 119, 203, 208, 213:
		return true
	}
	return false
}
