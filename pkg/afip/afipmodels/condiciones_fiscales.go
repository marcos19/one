package afipmodels

// CondicionIVA es el código que deterina que condición tiene la persona
// frente a AFIP en el impuesto al IVA.
type CondicionIVA int

const (
	CondicionNoInscripto          = CondicionIVA(1)
	CondicionResponsableInscripto = CondicionIVA(2)
	CondicionExento               = CondicionIVA(4)
	CondicionConsumidorFinal      = CondicionIVA(5)
	CondicionMonotributo          = CondicionIVA(6)
	CondicionProveedorExterior    = CondicionIVA(8)
	CondicionClienteExterior      = CondicionIVA(9)
	CondicionIVALiberado          = CondicionIVA(10)
	CondicionMonotributoSocial    = CondicionIVA(13)
	CondicionIVANoAlcanzado       = CondicionIVA(15)
)

func (c CondicionIVA) Nombre() string {
	switch c {
	case 1:
		return "No Inscripto"
	case 2:
		return "Responsable Inscripto"
	case 4:
		return "Exento"
	case 5:
		return "Consumidor Final"
	case 6:
		return "Monotributista"
	case 8:
		return "Proveedor del Exterior"
	case 9:
		return "Cliente del Exterior"
	case 10:
		return "IVA Liberado"
	case 13:
		return "Monotributo social"
	case 15:
		return "IVA no alcanzado"
	}
	return "N/D"
}
