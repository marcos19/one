package afipmodels

type Identificacion struct {
	ID     int
	Nombre string
}

func (I *Identificacion) TableName() string {
	return "afip_identificaciones"
}
