//go:build ws

package wsinsc

import (
	"encoding/json"
	"os"
	"testing"

	aa "bitbucket.org/marcos19/one/pkg/afip/wsaa/core"
	"github.com/davecgh/go-spew/spew"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestCreateEnvelopedReq(t *testing.T) {
	require := require.New(t)
	assert := assert.New(t)

	// Credentials
	file, err := os.Open("testdata/credenciales.json")
	require.Nil(err)
	cred := aa.Credenciales{}
	err = json.NewDecoder(file).Decode(&cred)
	require.Nil(err)

	req, err := createEnvelopedReq(20328896479, 20328896479, cred)
	require.Nil(err)

	assert.True(len(req) > 200)
}

func TestParseResponse(t *testing.T) {
	require := require.New(t)
	assert := assert.New(t)

	f, err := os.Open("testdata/getPersonaResponse.xml")
	require.Nil(err)

	out, err := parseResponse(f)
	require.Nil(err)
	assert.Equal("MEASURE", out.Body.GetPersonaV2Resp.PersonaReturn.DatosGenerales.Apellido, "%v", spew.Sdump(out))
	require.Len(
		out.Body.GetPersonaV2Resp.PersonaReturn.DatosMonotributo.Actividades,
		3, "se esperaban %v actividades, hubo %v",
		3, len(out.Body.GetPersonaV2Resp.PersonaReturn.DatosMonotributo.Actividades),
	)
	require.Len(
		out.Body.GetPersonaV2Resp.PersonaReturn.DatosMonotributo.ActividadMonotributista,
		1, "se esperaban %v actividades, hubo %v",
		1, len(out.Body.GetPersonaV2Resp.PersonaReturn.DatosMonotributo.ActividadMonotributista),
	)
	require.Len(
		out.Body.GetPersonaV2Resp.PersonaReturn.DatosMonotributo.CategoriaMonotributo,
		1, "se esperaban %v actividades, hubo %v",
		1, len(out.Body.GetPersonaV2Resp.PersonaReturn.DatosMonotributo.CategoriaMonotributo),
	)
	require.Len(
		out.Body.GetPersonaV2Resp.PersonaReturn.DatosMonotributo.Impuestos,
		1, "se esperaban %v actividades, hubo %v",
		1, len(out.Body.GetPersonaV2Resp.PersonaReturn.DatosMonotributo.Impuestos),
	)

	type e struct {
		expected interface{}
		actual   interface{}
	}
	aa := out.Body.GetPersonaV2Resp.PersonaReturn.DatosMonotributo.Actividades
	aaMon := out.Body.GetPersonaV2Resp.PersonaReturn.DatosMonotributo.ActividadMonotributista
	aaCat := out.Body.GetPersonaV2Resp.PersonaReturn.DatosMonotributo.CategoriaMonotributo
	vals := []e{
		// Actividades
		{
			"PRODUCCIÓN DE LANA Y PELO DE OVEJA YCABRA (CRUDA)",
			aa[0].DescripcionActividad,
		},
		{
			14710,
			aa[0].IDActividad,
		},
		{
			883,
			aa[0].Nomenclador,
		},
		{
			2,
			aa[0].Orden,
		},
		{
			"201403",
			aa[0].Periodo,
		},
		// ActividadMonotributista
		{
			"SERVICIOS DE CONSULTORES EN EQUIPO DE INFORMÁTICA",
			aaMon[0].DescripcionActividad,
		},
		{
			620200,
			aaMon[0].IDActividad,
		},
		{
			883,
			aaMon[0].Nomenclador,
		},
		{
			1,
			aaMon[0].Orden,
		},
		{
			"201311",
			aaMon[0].Periodo,
		},
		// CategoriaMonotributo
		{
			"C LOCACIONES DE SERVICIO",
			aaCat[0].DescripcionCategoria,
		},
		{
			37,
			aaCat[0].IDCategoria,
		},
		{
			20,
			aaCat[0].IDImpuesto,
		},
		{
			"201701",
			aaCat[0].Periodo,
		},
	}

	for _, v := range vals {
		assert.Equal(v.expected, v.actual)
	}

}

func TestActionGetPersona(t *testing.T) {
	require := require.New(t)
	// assert := assert.New(t)

	// Credentials
	file, err := os.Open("testdata/credenciales.json")
	require.Nil(err)

	cred := aa.Credenciales{}
	err = json.NewDecoder(file).Decode(&cred)
	require.Nil(err)

	_, err = ActionGetPersona(20328896479, 30711521964, cred)
	require.Nil(err)
}
