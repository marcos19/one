//go:build wsaa

package wsinsc

import (
	certpack "bitbucket.org/marcos19/one/pkg/afip/cert/core"
	"bitbucket.org/marcos19/one/pkg/afip/wsaa/core"
	"encoding/json"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"os"
	"testing"
)

// Se pide el ticket una vez, tiene build tags para evitar se repita la prueba
// cada vez que se solicita.
func TestPedirTicketParawsinsc(t *testing.T) {

	require := require.New(t)
	assert := assert.New(t)

	// Pido autorización
	crtData := core.CertificateData{
		CommonName: "marcos2",
		CUIT:       20328896479,
	}
	ticketReq, err := core.NewLoginTicketRequest(crtData, core.EntornoTesting, ServiceName)
	require.Nil(err)

	// Certificado
	certFile, err := os.Open("../wsaa/core/testdata/testing2.pem")
	require.Nil(err)
	cert, err := certpack.ParsearCertificado(certFile)
	require.Nil(err)

	// Private key
	keyFile, err := os.Open("../wsaa/core/testdata/privatekey")
	require.Nil(err)
	key, err := certpack.ParsearPrivateKey(keyFile)
	require.Nil(err)

	cred, err := ticketReq.SolicitarAcceso(cert, key)
	require.Nil(err)

	// Guardo ticket localmente para no tener que pedirlo cada vez que hago el test
	file, err := os.Create("./testdata/credenciales.json")
	defer file.Close()
	err = json.NewEncoder(file).Encode(&cred)
	assert.Nil(err)
}
