package wsinsc

const testingURL = "https://awshomo.afip.gov.ar/sr-padron/webservices/personaServiceA5"
const produccionURL = "https://aws.afip.gov.ar/sr-padron/webservices/personaServiceA5"

// Es el nombre con el que se solicita el ticket.
const ServiceName = "ws_sr_constancia_inscripcion"
