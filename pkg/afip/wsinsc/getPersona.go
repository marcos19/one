package wsinsc

import (
	"encoding/xml"
	"fmt"
	"io"
	"net/http"
	"strings"

	aacore "bitbucket.org/marcos19/one/pkg/afip/wsaa/core"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
)

type GetPersonaReq struct {
	XMLName          xml.Name `xml:"a5:getPersona_v2"`
	Token            string   `xml:"token"`
	Sign             string   `xml:"sign"`
	CUITRepresentada int      `xml:"cuitRepresentada"`
	IDPersona        int      `xml:"idPersona"`
}

type GetPersonaResp struct {
}

func ActionGetPersona(cuitRepresentada, idPersona int, cred aacore.Credenciales) (resp Constancia, err error) {

	// Agerego el envelope
	enveloped, err := createEnvelopedReq(cuitRepresentada, idPersona, cred)
	if err != nil {
		return resp, err
	}

	// Genero HTTP request
	uri := ""
	switch cred.Entorno {
	case aacore.EntornoTesting:
		uri = testingURL
	case aacore.EntornoProduccion:
		uri = produccionURL
	}
	r, err := http.NewRequest(http.MethodPost, uri, strings.NewReader(enveloped))
	if err != nil {
		return resp, errors.Wrap(err, "creando request")
	}
	r.Header.Add("Content-Type", "text/xml;charset=UTF-8")
	// r.Header.Add("SOAPAction", "http://ar.gov.afip.dif.FEV1/FECAESolicitar")

	// Envío HTTP request
	res, err := http.DefaultClient.Do(r)
	if err != nil {
		return resp, errors.Wrap(err, "realizando HTTP request")
	}

	// Status OK?
	if res.StatusCode != http.StatusOK {
		data, err := io.ReadAll(res.Body)
		if err != nil {
			return resp, errors.Wrap(err, "leyendo respuesta")
		}
		return resp, deferror.WebService("%v (%v)", string(data), res.Status)
	}

	// Parse response
	env, err := parseResponse(res.Body)
	if err != nil {
		return resp, errors.Wrap(err, "parsing response")
	}

	return env.Body.GetPersonaV2Resp.PersonaReturn, nil
}

var envelopeStr = `
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:a5="http://a5.soap.ws.server.puc.sr/">
	<soapenv:Header/>
	<soapenv:Body>
		%v
	</soapenv:Body>
</soapenv:Envelope>
`

func createEnvelopedReq(cuitRepresentada, idPersona int, cred aacore.Credenciales) (out string, err error) {
	request := GetPersonaReq{}
	request.Token = cred.Token
	request.Sign = cred.Sign
	request.CUITRepresentada = cuitRepresentada
	request.IDPersona = idPersona

	// Paso a XML
	by, err := xml.Marshal(request)
	if err != nil {
		return out, errors.Wrap(err, "Marshaling GetPersonaReq")
	}

	out = fmt.Sprintf(envelopeStr, string(by))
	return
}

func parseResponse(r io.Reader) (out EnvelopedResponse, err error) {

	by, err := io.ReadAll(r)
	if err != nil {
		return out, errors.Wrap(err, "leyendo body")
	}

	err = xml.Unmarshal(by, &out)
	if err != nil {
		return out, errors.Wrap(err, "parseando getPersonaV2response")
	}

	// Unmarshal
	// err = xml.NewDecoder(r).Decode(&out)
	// if err != nil {
	// 	return out, errors.Wrap(err, "parseando getPersonaV2response")
	// }
	return
}

type EnvelopedResponse struct {
	XMLName xml.Name `xml:"Envelope"`
	Body    struct {
		GetPersonaV2Resp struct {
			PersonaReturn Constancia `xml:"personaReturn"`
		} `xml:"getPersona_v2Response"`
	} `xml:"Body"`
}

type Constancia struct {
	DatosGenerales struct {
		Apellido        string `xml:"apellido"`
		Nombre          string `xml:"nombre"`
		RazonSocial     string `xml:"razonSocial"`
		DomicilioFiscal struct {
			CodPostal            int    `xml:"codPostal"`
			DescripcionProvincia string `xml:"descripcionProvincia"`
			Direccion            string `xml:"direccion"`
			IDProvincia          int    `xml:"idProvincia"`
			Localidad            string `xml:"localidad"`
			TipoDomicilio        string `xml:"tipoDomicilio"`
		} `xml:"domicilioFiscal"`
		EstadoClave string `xml:"estadoClave"`
		IDPersona   int    `xml:"idPersona"`
		MesCierre   int    `xml:"mesCierre"`
		TipoClave   string `xml:"tipoClave"`
		TipoPersona string `xml:"tipoPersona"`
	} `xml:"datosGenerales"`
	DatosRegimenGeneral struct {
		Actividades []Actividad `xml:"actividad"`
		Impuestos   []Impuesto  `xml:"impuesto"`
		Regimen     []Regimen   `xml:"regimen"`
	} `xml:"datosRegimenGeneral"`
	DatosMonotributo struct {
		Actividades             []Actividad            `xml:"actividad"`
		ActividadMonotributista []Actividad            `xml:"actividadMonotributista"`
		CategoriaMonotributo    []CategoriaMonotributo `xml:"categoriaMonotributo"`
		Impuestos               []Impuesto             `xml:"impuesto"`
	} `xml:"datosMonotributo"`
	ErrorConstancia struct {
	} `xml:"errorConstancia"`
	ErrorRegimenGeneral struct {
	} `xml:"errorRegimenGeneral"`
	ErrorMonotributo struct {
	} `xml:"errorMonotributo"`
}

type Actividad struct {
	// XMLName              xml.Name `xml:"actividad"`
	DescripcionActividad string `xml:"descripcionActividad"`
	IDActividad          int    `xml:"idActividad"`
	Nomenclador          int    `xml:"nomenclador"`
	Orden                int    `xml:"orden"`
	Periodo              string `xml:"periodo"`
}

type Regimen struct {
	DescripcionRegimen string `xml:"descripcionRegimen"`
	IDImpuesto         int    `xml:"idImpuesto"`
	IDRegimen          int    `xml:"idRegimen"`
	Periodo            string `xml:"periodo"`
	TipoRegimen        string `xml:"tipoRegimen"`
}

type Impuesto struct {
	DescripcionImpuesto string `xml:"descripcionImpuesto"`
	IDImpuesto          int    `xml:"idImpuesto"`
	Periodo             string `xml:"periodo"`
}

type CategoriaMonotributo struct {
	// XMLName              xml.Name `xml:"categoriaMonotributo"`
	DescripcionCategoria string `xml:"descripcionCategoria"`
	IDCategoria          int    `xml:"idCategoria"`
	IDImpuesto           int    `xml:"idImpuesto"`
	Periodo              string `xml:"periodo"`
}
