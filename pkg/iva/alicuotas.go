package iva

import (
	"fmt"

	"github.com/cockroachdb/errors"
)

type Alicuota int

type AlicuotaDTO struct {
	ID         Alicuota `json:",string"`
	Nombre     string
	Porcentaje float64
}

// Alicuotas devuelve un slice con las alícuotas de IVA
func Alicuotas() []AlicuotaDTO {
	return []AlicuotaDTO{
		{IVANoGravado, IVANoGravado.String(), 0},
		{IVAExento, IVAExento.String(), 0},
		{IVA0, IVA0.String(), 0},
		{IVA2yMedio, IVA2yMedio.String(), 0.025},
		{IVA5, IVA5.String(), 0.05},
		{IVA10yMedio, IVA10yMedio.String(), 0.105},
		{IVA21, IVA21.String(), 0.21},
		{IVA27, IVA27.String(), 0.27},
	}
}

// Alícuotas según están definidas por AFIP
// Estos datos están replicados en iva.js
const (
	IVANoGravado = Alicuota(1)
	IVAExento    = Alicuota(2)
	IVA0         = Alicuota(3)
	IVA10yMedio  = Alicuota(4)
	IVA21        = Alicuota(5)
	IVA27        = Alicuota(6)
	IVA5         = Alicuota(8)
	IVA2yMedio   = Alicuota(9)
)

// Porcentaje devuelve el porcenaje a aplicar de IVA en base
// al id de la alícuota
func (a Alicuota) Porcentaje() (float64, error) {
	switch a {
	case IVANoGravado, IVAExento, IVA0:
		return 0, nil
	case IVA10yMedio:
		return 0.105, nil
	case IVA21:
		return 0.21, nil
	case IVA27:
		return 0.27, nil
	case IVA5:
		return 0.05, nil
	case IVA2yMedio:
		return 0.025, nil
	}
	return 0, errors.Errorf("porcentaje no existente para alícuota ID=%v", int(a))
}

// String devuelve la alícuota formateada linda
func (a Alicuota) String() string {
	switch a {
	case IVANoGravado:
		return "No gravado"
	case IVAExento:
		return "Exento"
	case IVA0:
		return "0%"
	case IVA10yMedio:
		return "10,5%"
	case IVA21:
		return "21%"
	case IVA27:
		return "27%"
	case IVA5:
		return "5%"
	case IVA2yMedio:
		return "2,5%"
	}
	return fmt.Sprint(int(a))
}

func (a Alicuota) GeneraIVA() bool {
	switch a {
	case IVANoGravado, IVAExento:
		return false
	}
	return true
}

// Valid devueve true si el ID de alicuota existe
func (a Alicuota) Valid() bool {
	switch a {
	case 1, 2, 3, 4, 5, 6, 8, 9:
		return true

	}
	return false
}
