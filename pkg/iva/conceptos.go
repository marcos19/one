package iva

// Conceptos definidos por mi. Se usan para:
//  - Libro de IVA digital
//  - Libro de IVA (ya deprecado)
//  - Facturas: los totales pertencen a un tipo de concepto
//  - Resumen bancario:
type Concepto int

const (
	// ConceptoBI es Base imponible
	BaseImponible = Concepto(1)
	IVA           = Concepto(2)

	NoGravado = Concepto(5)
	Exento    = Concepto(6)

	// Estos conceptos están definidos en el libro de IVA Digital.
	PercepcionNoCategorizados = Concepto(30) // Solo para ventas
	PercepcionesIVA           = Concepto(31) // Solo para compras
	PercepcionesNacionales    = Concepto(32)
	PercepcionesISIB          = Concepto(33)
	PercepcionesMunicipales   = Concepto(34)
	ImpuestosInternos         = Concepto(35)
	OtrosTributos             = Concepto(36)
)

type ConceptoDTO struct {
	ID     Concepto `json:",string"`
	Nombre string
}

// Conceptos devuelve un slice con todos los conceptos frente a IVA
func Conceptos() []ConceptoDTO {
	return []ConceptoDTO{
		{BaseImponible, BaseImponible.StringDetallada()},
		{IVA, IVA.StringDetallada()},

		{NoGravado, NoGravado.StringDetallada()},
		{Exento, Exento.StringDetallada()},

		{PercepcionNoCategorizados, PercepcionNoCategorizados.StringDetallada()},
		{PercepcionesIVA, PercepcionesIVA.StringDetallada()},
		{PercepcionesNacionales, PercepcionesNacionales.StringDetallada()},
		{PercepcionesISIB, PercepcionesISIB.StringDetallada()},
		{PercepcionesMunicipales, PercepcionesMunicipales.StringDetallada()},
		{ImpuestosInternos, ImpuestosInternos.StringDetallada()},
		{OtrosTributos, OtrosTributos.StringDetallada()},
	}
}

// Valid devuelve true si el código corresponde a un código de concepto.
func (c Concepto) Valid() bool {
	switch c {
	case 1, 2, 5, 6, 30, 31, 32, 33, 34, 35, 36:
		return true
	}
	return false
}

func (c Concepto) StringFactura() string {
	switch c {
	case BaseImponible:
		return "Neto gravado"
	case Exento:
		return "Exento"
	case NoGravado:
		return "No gravado"
	case IVA:
		return "IVA"
	case PercepcionNoCategorizados:
		return "Percepción IVA no categorizado"
	case PercepcionesIVA:
		return "Percepción IVA"
	case PercepcionesNacionales:
		return "Percepciones nacionales"
	case PercepcionesISIB:
		return "Percepciones IIBB"
	case PercepcionesMunicipales:
		return "Percepciones municipales"
	case ImpuestosInternos:
		return "Impuestos internos"
	case OtrosTributos:
		return "Otros tributos"
	}
	return "N/D"
}

func (c Concepto) StringDetallada() string {
	switch c {
	case BaseImponible:
		return "Neto gravado"
	case IVA:
		return "IVA"
	case NoGravado:
		return "No gravado"
	case Exento:
		return "Exento"
	case PercepcionNoCategorizados:
		return "Percepción IVA no categorizado"
	case PercepcionesIVA:
		return "Percepción IVA"
	case PercepcionesNacionales:
		return "Percepciones nacionales"
	case PercepcionesISIB:
		return "Percepciones IIBB"
	case PercepcionesMunicipales:
		return "Percepciones municipales"
	case ImpuestosInternos:
		return "Impuestos internos"
	case OtrosTributos:
		return "Otros tributos"
	}
	return "N/D"
}

func (c Concepto) String() string {
	return c.StringDetallada()
}
