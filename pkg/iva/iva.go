package iva

import "fmt"

// NombreCombinado devuelve la leyenda para el total del
// cuadrito de conceptos de la factura.
// Por ejemplo Neto Gravado 21% e IVA 21%
func NombreCombinado(c Concepto, a Alicuota) string {

	switch a {
	case IVANoGravado:
		return "No gravado"
	case IVAExento:
		return "Exento"
	}

	switch c {
	case BaseImponible, IVA:
		return fmt.Sprintf("%v %v", c.StringFactura(), a)
	default:
		return c.StringFactura()
	}
}
