package emailsender

import (
	"context"
	"crypto/tls"
	"io"
	"strings"

	"github.com/cockroachdb/errors"
	"github.com/go-mail/mail"
	"github.com/rs/zerolog/log"
)

type Outputer interface {
	Output(io.Writer) error
}

type Sender struct {
	dialer *mail.Dialer

	host string
	port int
	user string
	pass string
}

func (s *Sender) SetConfig(host string, port int, user, pass string) {
	s.host = host
	s.port = port
	s.user = user
	s.pass = pass
	s.dialer = mail.NewDialer(s.host, s.port, s.user, s.pass)

	s.dialer = mail.NewDialer(host, port, user, pass)
	s.dialer.TLSConfig = &tls.Config{InsecureSkipVerify: true}
}

func (s Sender) Send(ctx context.Context, req Req) (err error) {

	// Creo mensaje
	m := mail.NewMessage()
	for i := range req.Attachments {
		r, w := io.Pipe()
		m.AttachReader(req.AttachmentsNames[i], r)
		go func(idx int) {
			err2 := req.Attachments[idx].Output(w)
			if err2 != nil {
				log.Error().Err(err2).Msgf("exportando outputer %v", idx)
			}
			w.Close()
		}(i)
	}

	// From
	m.SetHeader("From", req.From)

	// To
	for i, v := range req.To {
		req.To[i] = strings.Trim(v, " ")
		req.To[i] = strings.ToLower(req.To[i])
	}
	m.SetHeader("To", req.To...)

	// Subject
	m.SetHeader("Subject", req.Subject)

	// Body
	m.SetBody("text/html", req.Body)

	// Envío
	err = s.dialer.DialAndSend(m)
	if err != nil {
		return errors.Wrap(err, "enviando e-mail")
	}

	return
}

type Req struct {
	// Quién envía el mail.
	// El formato es: Marcos Bortolussi <marcosbortolussi@gmail.com>
	From string

	// Direcciones a las que se envía
	To      []string
	Subject string
	Body    string

	// Comprobantes
	Attachments      []Outputer
	AttachmentsNames []string
}

// Crea un Sender con la configuración listo para mandar mails.
func NewSender(host string, port int, user, pass string) (out *Sender) {
	out = &Sender{}
	out.SetConfig(host, port, user, pass)
	return
}
