package autoaplicador

import (
	"context"
	"fmt"
	"sort"
	"strings"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/cuentasgrupos"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/crosslogic/niler"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
)

type ConfirmarReq struct {
	Comitente    int
	Empresa      int `json:",string"`
	GrupoCuentas int `json:",string"`
	Personas     []uuid.UUID
	Fecha        fecha.Fecha
	Comp         int `json:",string"`
}

func Confirmar(
	ctx context.Context,
	conn *pgxpool.Pool,
	gr *cuentasgrupos.Handler,
	oh ops.Inserter,
	compReader comps.ReaderOne,
	compNumerador comps.Numerador,
	req ConfirmarReq) (err error) {

	// Valido
	if req.Comitente == 0 {
		return errors.Errorf("comitente no defindo")
	}
	if req.Empresa == 0 {
		return errors.Errorf("empresa no definida")
	}
	if req.GrupoCuentas == 0 {
		return errors.Errorf("grupo de cuentas no definido")
	}
	if len(req.Personas) == 0 {
		return errors.Errorf("no se ingresó ninguna persona")
	}
	if req.Fecha == 0 {
		return errors.Errorf("no se ingresó fecha para registrar")
	}
	if niler.IsNil(oh) {
		return errors.Errorf("ops.Inserter no puede ser nil")
	}
	if niler.IsNil(compReader) {
		return errors.Errorf("comps.Reader no puede ser nil")
	}
	if niler.IsNil(compNumerador) {
		return errors.Errorf("comps.Numerador no puede ser nil")
	}

	var ctasID tipos.GrupoInts
	{ // Busco cuentas del grupo
		grupo, err := gr.ReadOne(ctx, cuentasgrupos.ReadOneReq{Comitente: req.Comitente, ID: req.GrupoCuentas})
		if err != nil {
			return errors.Wrap(err, "buscando cuentas del grupo")
		}
		ctasID = grupo.Cuentas
	}

	nombres := map[uuid.UUID]string{}
	{ // Busco nombres personas

		query := fmt.Sprintf(`
			SELECT id, nombre	
			FROM personas 
			WHERE 
				comitente = $1 AND 
				id IN (%v)
		`, arrayUUID(req.Personas))
		rows, err := conn.Query(ctx, query, req.Comitente)
		if err != nil {
			return errors.Wrap(err, "querying personas")
		}
		defer rows.Close()

		for rows.Next() {
			id := uuid.UUID{}
			nombre := ""
			err = rows.Scan(&id, &nombre)
			if err != nil {
				return errors.Wrap(err, "escaneando nombre")
			}
			nombres[id] = nombre
		}
	}

	type key struct {
		Persona uuid.UUID
		Partida uuid.UUID
	}
	partidasAnteriores := map[key]dec.D2{}

	{ // Busco partidas disponibles en tablas de saldo
		query := fmt.Sprintf(`
			SELECT persona, partida_aplicada, monto
			FROM saldos 
			WHERE 
				comitente=$1 AND
				empresa=$2 AND 
				cuenta IN %v AND
				persona IN (%v)
			`, ctasID.ParaClausulaIn(), arrayUUIDSaldos(req.Personas))

		rows, err := conn.Query(ctx, query, req.Comitente, req.Empresa)
		if err != nil {
			return errors.Wrap(err, "querying saldos pendientes")
		}
		defer rows.Close()

		for rows.Next() {
			persona := uuid.UUID{}
			partida := uuid.UUID{}
			monto := 0
			err = rows.Scan(&persona, &partida, &monto)
			if err != nil {
				return errors.Wrap(err, "escanenado saldos pendientes")
			}
			partidasAnteriores[key{persona, partida}] = dec.D2(monto)
		}
	}

	// Tengo que determinar si alguna de las partidas pendientes de
	// la tabla de saldo tiene fecha posterior a la de corte.
	{
		query := fmt.Sprintf(`
			SELECT persona, partida_aplicada, monto
			FROM partidas
			WHERE 
				comitente=$1 AND
				empresa=$2 AND 
				fecha_contable > $3 AND
				cuenta IN %v AND
				persona IN (%v)
			`, ctasID.ParaClausulaIn(), arrayUUID(req.Personas))

		rows, err := conn.Query(ctx, query, req.Comitente, req.Empresa, req.Fecha)
		if err != nil {
			return errors.Wrap(err, "querying movimientos posteriores fecha de corte")
		}
		defer rows.Close()

		for rows.Next() {
			persona := uuid.UUID{}
			partida := uuid.UUID{}
			monto := 0
			err = rows.Scan(&persona, &partida, &monto)
			if err != nil {
				return errors.Wrap(err, "escanenado movimientos posteriores a fecha de corte")
			}
			anterior, ok := partidasAnteriores[key{persona, partida}]
			if !ok {
				// La partida no estaba pendiente en la tabla de saldos,
				// Se aplicó con fecha posterior a la de corte.
				continue
			}
			anterior -= dec.D2(monto)
			partidasAnteriores[key{persona, partida}] = anterior
		}
	}

	type partida struct {
		Persona uuid.UUID
		Partida uuid.UUID
		Monto   dec.D2
	}

	// Determino los que tienen saldo a la fecha solicitada
	prev := []partida{}
	for k, v := range partidasAnteriores {
		if v == 0 {
			// Significa que el saldo que tenía la persona corresponde a movimientos
			// posteriores a la fecha de corte.
			continue
		}
		prev = append(prev, partida{
			Persona: k.Persona,
			Partida: k.Partida,
			Monto:   v,
		})
	}

	// Agrupo por persona
	sum := map[uuid.UUID]dec.D2{}
	for _, v := range prev {
		sum[v.Persona] += v.Monto
	}

	// Queda con saldo cero?
	personasConSaldoCero := map[uuid.UUID]struct{}{}
	for k, v := range sum {
		if v == 0 {
			personasConSaldoCero[k] = struct{}{}
		}
	}

	// De las partidas disponibles elijo las de personas con saldo cero
	partidasACancelar := []uuid.UUID{}
	for _, v := range prev {
		_, ok := personasConSaldoCero[v.Persona]
		if !ok {
			continue
		}
		partidasACancelar = append(partidasACancelar, v.Partida)
	}

	parts := map[uuid.UUID][]ops.Partida{}
	{ // Busco IDs de partidas que voy a cancelar
		query := fmt.Sprintf(`
SELECT partida_aplicada, cuenta, centro, persona, sucursal, caja, producto, sku, deposito, unicidad, unicidad_tipo, contrato, monto 
FROM partidas 
WHERE partida_aplicada IN (%v)
;`, arrayUUID(partidasACancelar))

		rows, err := conn.Query(ctx, query)
		if err != nil {
			return errors.Wrap(err, "buscando IDs")
		}
		defer rows.Close()

		for rows.Next() {
			p := ops.Partida{}
			err = rows.Scan(
				&p.PartidaAplicada,
				&p.Cuenta,
				&p.Centro,
				&p.Persona,
				&p.Sucursal,
				&p.Caja,
				&p.Producto,
				&p.SKU,
				&p.Deposito,
				&p.Unicidad,
				&p.UnicidadTipo,
				&p.Contrato,
				&p.Monto,
			)
			if err != nil {
				return errors.Wrap(err, "escaneando partidas")
			}

			prev := parts[*p.Persona]
			prev = append(prev, p)
			parts[*p.Persona] = prev
		}
	}

	oo := []*ops.Op{}

	tranID, err := uuid.NewV1()
	if err != nil {
		return err
	}

	for k, partidas := range parts {
		o := ops.Op{}
		o.ID, err = uuid.NewV1()
		if err != nil {
			return err
		}
		o.TranID = tranID
		o.Empresa = req.Empresa
		o.Comitente = req.Comitente
		o.Fecha = req.Fecha
		o.Programa = "autoaplicador"
		o.TranDef = 1
		o.TranID = o.ID
		o.CompID = req.Comp
		o.Persona = new(uuid.UUID)
		*o.Persona = k
		o.PersonaNombre = nombres[k]
		o.FechaOriginal = o.Fecha
		o.Detalle = "Aplicación automática"

		sumDebe := dec.D2(0)
		for _, v := range partidas {
			v.ID, err = uuid.NewV1()
			if err != nil {
				return err
			}
			v.Monto *= -1
			if v.Monto > 0 {
				sumDebe += v.Monto
			}
			o.PartidasContables = append(o.PartidasContables, v)
		}
		o.TotalComp = new(dec.D2)
		*o.TotalComp = sumDebe
		oo = append(oo, &o)
	}

	// Preparo TX
	log.Debug().Msg("Preparando TX")
	tx, err := conn.Begin(ctx)
	if err != nil {
		return errors.Wrap(err, "inciciando transacción")
	}
	defer tx.Rollback(ctx)

	// Inserto ops
	err = oh.Insert(ctx, tx, oo, nil)
	if err != nil {
		return errors.Wrap(err, "insertando ops")
	}

	// Confirmo TX
	log.Debug().Msg("Confirmando transacción...")
	err = tx.Commit(ctx)
	if err != nil {
		return errors.Wrap(err, "insertando ops")
	}
	return
}

type BuscarReq struct {
	Comitente    int
	Empresa      int `json:",string"`
	GrupoCuentas int `json:",string"`
	Fecha        fecha.Fecha
	Limit        int `json:",string"`
}

// func Buscar(ctx context.Context, conn *pgxpool.Pool, gr *cuentasgrupos.Handler, req BuscarReq) (out []Partida, err error) {

// 	// Valido
// 	if req.Comitente == 0 {
// 		return out, errors.Errorf("comitente no defindo")
// 	}
// 	if req.Empresa == 0 {
// 		return out, errors.Errorf("empresa no definida")
// 	}
// 	if req.GrupoCuentas == 0 {
// 		return out, errors.Errorf("grupo de cuentas no definido")
// 	}
// 	if req.Fecha == 0 {
// 		return out, errors.Errorf("fecha no definida")
// 	}

// 	personasID := []uuid.UUID{}
// 	cantidades := map[uuid.UUID]int{}
// 	ctasID := tipos.GrupoInts{}

// 	{ // Busco cuentas del grupo
// 		grupo, err := gr.ReadOne(ctx, cuentasgrupos.ReadOneReq{Comitente: req.Comitente, ID: req.GrupoCuentas})
// 		if err != nil {
// 			return out, errors.Wrap(err, "buscando cuentas del grupo")
// 		}
// 		ctasID = grupo.Cuentas
// 	}

// 	{ // Busco partidas disponibles
// 		query := fmt.Sprintf(`
// SELECT persona, COUNT(persona)
// FROM (
// 	SELECT persona
// 	FROM partidas
// 	WHERE
// 		comitente = $1 AND
// 		empresa = $2 AND
// 		fecha_contable <= $3 AND
// 		cuenta IN %v AND
// 		partida_aplicada IS NOT NULL AND
// 		persona IN (
// 		-- Elijo las personas a las que le da cero
// 		SELECT persona
// 			FROM partidas
// 			WHERE
// 				comitente = $1 AND
// 				empresa = $2 AND
// 				fecha_contable <= $3 AND
// 				cuenta IN %v AND
// 				partida_aplicada IS NOT NULL
// 			GROUP BY persona
// 			HAVING SUM(monto) = 0
// 	)
// 	GROUP BY persona, partida_aplicada
// 	HAVING SUM(monto) <> 0
// )
// GROUP BY persona
// LIMIT $4
// ;
// 	`, ctasID.ParaClausulaIn(), ctasID.ParaClausulaIn())
// 		rows, err := conn.Query(ctx, query, req.Comitente, req.Empresa, req.Fecha, req.Limit)
// 		if err != nil {
// 			return out, errors.Wrap(err, "querying")
// 		}
// 		defer rows.Close()

// 		for rows.Next() {
// 			persona := uuid.UUID{}
// 			cantidad := 0
// 			err = rows.Scan(&persona, &cantidad)
// 			if err != nil {
// 				return out, errors.Wrap(err, "scanning")
// 			}
// 			personasID = append(personasID, persona)
// 			cantidades[persona] = cantidad
// 		}
// 	}

// 	{ // Busco los datos de las personas
// 		query := fmt.Sprintf(`
// 			SELECT id, nombre
// 			FROM personas
// 			WHERE
// 				comitente = $1 AND
// 				id IN (%v)
// 			ORDER BY nombre
// 		`, arrayUUID(personasID))
// 		rows, err := conn.Query(ctx, query, req.Comitente)
// 		if err != nil {
// 			return out, errors.Wrap(err, "querying personas")
// 		}
// 		defer rows.Close()

// 		for rows.Next() {
// 			p := Partida{}
// 			err = rows.Scan(&p.PersonaID, &p.PersonaNombre)
// 			if err != nil {
// 				return out, errors.Wrap(err, "scanning personas")
// 			}
// 			p.Cantidad = cantidades[p.PersonaID]
// 			out = append(out, p)
// 		}
// 	}

// 	if out == nil {
// 		out = []Partida{}
// 	}
// 	return
// }

type DeleteReq struct {
	Comitente int
	ID        uuid.UUID // op ID
}

// Devuelve un listado de personas con la cantidad de partidas que tienen pendientes
func BuscarPendientesRetroactivo(ctx context.Context, conn *pgxpool.Pool, gr *cuentasgrupos.Handler, req BuscarReq) (out []Partida, err error) {

	// Clauslas where
	var ctasID tipos.GrupoInts
	{ // Busco cuentas del grupo
		grupo, err := gr.ReadOne(ctx, cuentasgrupos.ReadOneReq{Comitente: req.Comitente, ID: req.GrupoCuentas})
		if err != nil {
			return out, errors.Wrap(err, "buscando cuentas del grupo")
		}
		ctasID = grupo.Cuentas
	}

	type key struct {
		Persona uuid.UUID
		Partida uuid.UUID
	}
	partidasAnteriores := map[key]dec.D2{}
	personasID := []uuid.UUID{}

	{ // Tabla de saldos
		limit := ""
		if req.Limit != 0 {
			limit = "LIMIT $3"
		}
		query := fmt.Sprintf(`
			SELECT persona, partida_aplicada, monto
			FROM saldos 
			WHERE 
				comitente=$1 AND
				empresa=$2 AND 
				cuenta IN %v AND
				persona IN (
					SELECT persona
					FROM saldos 
					WHERE comitente=$1 AND empresa=$2 AND cuenta IN %v AND persona <> ''
					GROUP BY persona
					HAVING sum(monto) = 0
					%v
				)
			`, ctasID.ParaClausulaIn(), ctasID.ParaClausulaIn(), limit)

		rows, err := conn.Query(ctx, query, req.Comitente, req.Empresa, req.Limit)
		if err != nil {
			return out, errors.Wrap(err, "querying saldos pendientes")
		}
		defer rows.Close()

		for rows.Next() {
			persona := uuid.UUID{}
			partida := uuid.UUID{}
			monto := 0
			err = rows.Scan(&persona, &partida, &monto)
			if err != nil {
				return out, errors.Wrap(err, "escanenado saldos pendientes")
			}
			personasID = append(personasID, persona)
			partidasAnteriores[key{persona, partida}] = dec.D2(monto)
		}
	}

	{ // Busco movimientos posteriores a la fecha de corte.
		query := fmt.Sprintf(`
			SELECT persona, partida_aplicada, monto
			FROM partidas
			WHERE 
				comitente=$1 AND
				empresa=$2 AND 
				fecha_contable > $3 AND
				cuenta IN %v AND
				persona IN (%v)
			`, ctasID.ParaClausulaIn(), arrayUUID(personasID))

		rows, err := conn.Query(ctx, query, req.Comitente, req.Empresa, req.Fecha)
		if err != nil {
			return out, errors.Wrap(err, "querying movimientos posteriores fecha de corte")
		}
		defer rows.Close()

		for rows.Next() {
			persona := uuid.UUID{}
			partida := uuid.UUID{}
			monto := 0
			err = rows.Scan(&persona, &partida, &monto)
			if err != nil {
				return out, errors.Wrap(err, "escanenado movimientos posteriores a fecha de corte")
			}
			anterior, ok := partidasAnteriores[key{persona, partida}]
			if !ok {
				// La partida no estaba pendiente en la tabla de saldos,
				// Se aplicó con fecha posterior a la de corte.
				continue
			}
			anterior -= dec.D2(monto)
			partidasAnteriores[key{persona, partida}] = anterior

		}
	}

	// Determino los que tienen saldo a la fecha solicitada
	prev := []Partida{}
	for k, v := range partidasAnteriores {
		if v == 0 {
			// Significa que el saldo que tenía la persona corresponde a movimientos
			// posteriores a la fecha de corte.
			continue
		}
		prev = append(prev, Partida{
			PersonaID: k.Persona,
			Monto:     v,
		})
	}

	// Agrupo por persona
	count := map[uuid.UUID]int{}
	sum := map[uuid.UUID]dec.D2{}
	for _, v := range prev {
		count[v.PersonaID] += 1
		sum[v.PersonaID] += v.Monto
	}

	// Queda con saldo cero?
	conSaldoCero := []uuid.UUID{}
	for k, v := range sum {
		if v == 0 {
			conSaldoCero = append(conSaldoCero, k)
		}
	}

	// Si tiro una consulta retroactiva, puede ser que:
	//  - Haya movimientos posteriores que me modifiquen los saldos
	//  - Haya movimientos
	// Si hago una consulta retroactiva, puede que algún saldo tenga movimientos
	// posteriores
	// Estos saldos que trajen pueden tener movimientos posteriores
	personasNombres := map[uuid.UUID]string{}
	{ // Busco nombres personas

		query := fmt.Sprintf(`
			SELECT id, nombre	
			FROM personas 
			WHERE 
				comitente = $1 AND 
				id IN (%v)
		`, arrayUUID(conSaldoCero))
		rows, err := conn.Query(ctx, query, req.Comitente)
		if err != nil {
			return out, errors.Wrap(err, "querying personas")
		}
		defer rows.Close()

		for rows.Next() {
			id := uuid.UUID{}
			nombre := ""
			err = rows.Scan(&id, &nombre)
			if err != nil {
				return out, errors.Wrap(err, "escaneando nombre")
			}
			personasNombres[id] = nombre
		}
	}

	// Pego nombres
	for _, v := range conSaldoCero {
		p := Partida{
			PersonaID:     v,
			Cantidad:      count[v],
			PersonaNombre: personasNombres[v],
		}
		out = append(out, p)
	}

	// Ordeno
	sort.Slice(out, func(i, j int) bool {
		return out[i].PersonaNombre < out[j].PersonaNombre
	})

	if out == nil {
		out = []Partida{}
	}
	return
}

// Devuelve un listado de personas con la cantidad de partidas que tienen pendientes
func BuscarPendientesSinFiltroFechas(ctx context.Context, conn *pgxpool.Pool, gr *cuentasgrupos.Handler, req BuscarReq) (out []Partida, err error) {

	// Clauslas where
	pp := []any{}
	{
		pp = append(pp, req.Comitente)
	}

	if req.Empresa != 0 {
		pp = append(pp, req.Empresa)
	}

	var ctasID tipos.GrupoInts
	{ // Busco cuentas del grupo
		grupo, err := gr.ReadOne(ctx, cuentasgrupos.ReadOneReq{Comitente: req.Comitente, ID: req.GrupoCuentas})
		if err != nil {
			return out, errors.Wrap(err, "buscando cuentas del grupo")
		}
		ctasID = grupo.Cuentas
	}

	personasID := []uuid.UUID{}
	{ // Tabla de saldos

		// Para determinar personas
		query := fmt.Sprintf(`
			SELECT persona, count(persona)
			FROM saldos 
			WHERE 
				comitente=$1 AND
				empresa=$2 AND 
				persona <> '' AND
				cuenta IN %v
			GROUP BY persona
			HAVING COUNT(persona) > 0
	`, ctasID.ParaClausulaIn())

		if req.Limit != 0 {
			pp = append(pp, req.Limit)
			query += fmt.Sprintf("LIMIT $%v", len(pp))
		}

		rows, err := conn.Query(ctx, query, pp...)
		if err != nil {
			return out, errors.Wrap(err, "querying saldos pendientes")
		}
		defer rows.Close()

		for rows.Next() {
			p := Partida{}
			err = rows.Scan(&p.PersonaID, &p.Cantidad)
			if err != nil {
				return out, errors.Wrap(err, "escanenado saldos pendientes")
			}
			out = append(out, p)
			personasID = append(personasID, p.PersonaID)
		}
	}

	// Si tiro una consulta retroactiva, puede ser que:
	//  - Haya movimientos posteriores que me modifiquen los saldos
	//  - Haya movimientos
	// Si hago una consulta retroactiva, puede que algún saldo tenga movimientos
	// posteriores
	// Estos saldos que trajen pueden tener movimientos posteriores

	personasNombres := map[uuid.UUID]string{}
	{ // Busco nombres personas

		query := fmt.Sprintf(`
			SELECT id, nombre	
			FROM personas 
			WHERE 
				comitente = $1 AND 
				id IN (%v)
		`, arrayUUID(personasID))
		rows, err := conn.Query(ctx, query, req.Comitente)
		if err != nil {
			return out, errors.Wrap(err, "querying personas")
		}
		defer rows.Close()

		for rows.Next() {
			id := uuid.UUID{}
			nombre := ""
			err = rows.Scan(&id, &nombre)
			if err != nil {
				return out, errors.Wrap(err, "escaneando nombre")
			}
			personasNombres[id] = nombre
		}
	}

	// Pego nombres
	for i := range out {
		out[i].PersonaNombre = personasNombres[out[i].PersonaID]
	}

	// Ordeno
	sort.Slice(out, func(i, j int) bool {
		return out[i].PersonaNombre < out[j].PersonaNombre
	})

	if out == nil {
		out = []Partida{}
	}
	return
}

// query := fmt.Sprintf(`
// SELECT partida_aplicada, monto FROM saldos
// WHERE
// 	comitente=$1 AND
// 	empresa=$2 AND
// 	cuenta IN %v
// GROUP BY comitente, cuenta,centro, persona, sucursal, caja, producto, sku, deposito, unicidad, partida_aplicada, contrato, empresa
// HAVING COUNT(comitente) > 0
// `, ctasID.ParaClausulaIn())

// TODO: ponerle permisos
func Delete(ctx context.Context, conn *pgxpool.Pool, del ops.Deleter, req DeleteReq) (err error) {

	if req.Comitente == 0 {
		return errors.Errorf("comitente no determinado")
	}
	if req.ID == uuid.Nil {
		return errors.Errorf("id no determinado")
	}

	// Inicio transacción
	tx, err := conn.Begin(ctx)
	if err != nil {
		return errors.Wrap(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)

	// Borro
	err = del.Delete(ctx, tx, req.Comitente, []uuid.UUID{req.ID})
	if err != nil {
		return errors.Wrap(err, "deleteflow")
	}

	// Confirmo transacción
	err = tx.Commit(ctx)
	if err != nil {
		return errors.Wrap(err, "confirmando transacción")
	}

	return
}

func arrayUUID(ids []uuid.UUID) string {
	idsStr := []string{}
	for _, v := range ids {
		idsStr = append(idsStr, fmt.Sprintf("'%v'", v.String()))
	}
	return strings.Join(idsStr, ", ")
}

func arrayUUIDSaldos(ids []uuid.UUID) string {
	arr := []string{}
	for _, v := range ids {
		val := fmt.Sprintf("to_uuid('%v')", v)
		arr = append(arr, val)
	}
	return strings.Join(arr, ",")
}
