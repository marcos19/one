package autoaplicador

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/cuentasgrupos"
	opshandler "bitbucket.org/marcos19/one/pkg/ops/handler"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Handler struct {
	conn          *pgxpool.Pool
	cuentasgrupos *cuentasgrupos.Handler
	opsHandler    *opshandler.Handler
	compReader    comps.ReaderOne
	compNumerador comps.Numerador
}

func NewHandler(
	conn *pgxpool.Pool,
	gr *cuentasgrupos.Handler,
	opsHandler *opshandler.Handler,
	compHandler comps.ReaderOne,
	compNumerador comps.Numerador,
) *Handler {
	return &Handler{conn, gr, opsHandler, compHandler, compNumerador}
}

func (h *Handler) Buscar(ctx context.Context, req BuscarReq) (out []Partida, err error) {
	if req.Fecha == 0 {
		return BuscarPendientesSinFiltroFechas(ctx, h.conn, h.cuentasgrupos, req)
	} else {
		return BuscarPendientesRetroactivo(ctx, h.conn, h.cuentasgrupos, req)
	}
}

func (h *Handler) Confirmar(ctx context.Context, req ConfirmarReq) (err error) {
	return Confirmar(ctx, h.conn, h.cuentasgrupos, h.opsHandler, h.compReader, h.compNumerador, req)
}

func (h *Handler) Delete(ctx context.Context, req DeleteReq) (err error) {
	return Delete(ctx, h.conn, h.opsHandler, req)
}
