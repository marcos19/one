package autoaplicador

import (
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
)

type Partida struct {
	PersonaID     uuid.UUID
	PersonaNombre string
	Cantidad      int
	Monto         dec.D2
}
