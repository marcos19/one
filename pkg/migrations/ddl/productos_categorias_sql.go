package ddl

func ProductosCategorias() Tabla {

	return Tabla{
		Nombre:    "productos_categorias",
		DependeDe: "",
		SQL: `
CREATE TABLE productos_categorias (
id                  SERIAL PRIMARY KEY,
comitente			INT NOT NULL REFERENCES comitentes,
nombre              TEXT,
full_path STRING,
imputable 			BOOL,
padre_id 			INT,
familia INT REFERENCES centros,
facetas				JSONB,
		
created_at          TIMESTAMP WITH TIME ZONE,
updated_at          TIMESTAMP WITH TIME ZONE
);
		`,
	}
}
