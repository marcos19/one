package ddl

func EmpresasKeys() Tabla {

	return Tabla{
		Nombre:    "empresas_keys",
		DependeDe: "",
		SQL: `
CREATE TABLE empresas_keys (

comitente INT NOT NULL REFERENCES comitentes,
empresa INT NOT NULL REFERENCES empresas,
key BYTES,
created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
CONSTRAINT "primary" PRIMARY KEY (
comitente, empresa
) 
);
		`,
	}
}
