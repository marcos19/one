package ddl

func CuentasAsociadas() Tabla {

	return Tabla{
		Nombre:    "cuentas_asociadas",
		DependeDe: "",
		SQL: `

CREATE TABLE cuentas_asociadas (
comitente INT REFERENCES comitentes,
cuenta INT,
persona UUID REFERENCES personas,
sucursal INT NOT NULL,
PRIMARY KEY (comitente, cuenta, persona, sucursal)
);
		`,
	}
}
