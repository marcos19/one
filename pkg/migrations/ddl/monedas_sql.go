package ddl

func Monedas() Tabla {

	return Tabla{
		Nombre:    "monedas",
		DependeDe: "",
		SQL: `
		CREATE TABLE monedas (
			id                  TEXT PRIMARY KEY,
			comitente 			INT NOT NULL REFERENCES comitentes,
			nombre_singular     TEXT,
			nombre_plural       TEXT,
			simbolo             TEXT
		);
		
	
		`,
	}
}

// INSERT INTO monedas (id, comitente, nombre_singular, nombre_plural, simbolo) VALUES
// 	('ARS', 1, 'Peso argentino', 'Pesos argentinos', '$'),
// 	('USD', 1, 'Dólar estadounidense', 'Dólares estadounidenses', 'U$S')
// 	;
