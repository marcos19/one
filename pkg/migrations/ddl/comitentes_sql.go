package ddl

func Comitentes() Tabla {

	return Tabla{
		Nombre:    "comitentes",
		DependeDe: "",
		SQL: `

			CREATE SEQUENCE comitentes_seq START 10;	
			CREATE TABLE IF NOT EXISTS comitentes (
				id              INT PRIMARY KEY DEFAULT nextval('comitentes_seq'),
				nombre          TEXT,
				created_at      TIMESTAMP WITH TIME ZONE, 
				updated_at      TIMESTAMP WITH TIME ZONE

			);`,
	}
}
