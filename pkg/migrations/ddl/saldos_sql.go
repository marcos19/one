package ddl

func Saldos() Tabla {

	return Tabla{
		Nombre:    "Saldos",
		DependeDe: "",
		SQL: `

CREATE TABLE saldos(

comitente INT,
cuenta INT,
centro int ,
persona BYTES,
sucursal int,
caja int,
producto BYTES,
sku BYTES,
deposito int,
moneda INT,
partida_aplicada BYTES,
contrato INT,
empresa INT,

monto int NOT NULL,
cantidad int NOT NULL,
monto_orig int NOT NULL,

CONSTRAINT "primary" PRIMARY KEY (
comitente, cuenta, centro, persona, sucursal, caja, producto, deposito, sku, partida_aplicada, moneda, contrato, empresa
) 
);

`,
	}
}

// CREATE TABLE saldos (

// comitente INT,
// cuenta INT,
// centro int ,
// persona BYTES,
// sucursal int,
// caja int,
// producto BYTES,
// sku BYTES,
// deposito int,
// unicidad BYTES,
// partida_aplicada BYTES,
// contrato INT,
// empresa INT,

// monto int,
// cantidad int,

// CONSTRAINT "primary" PRIMARY KEY (
// comitente, cuenta, centro, persona, sucursal, caja, producto, deposito, sku, unicidad, partida_aplicada, contrato, empresa
// )
// );
