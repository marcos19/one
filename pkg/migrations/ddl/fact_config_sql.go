package ddl

func FactConfig() Tabla {

	return Tabla{
		Nombre:    "fact_config",
		DependeDe: "",
		SQL: `
CREATE TABLE fact_config(

id SERIAL PRIMARY KEY,
comitente INT NOT NULL REFERENCES comitentes,
empresa INT NOT NULL REFERENCES empresas,
nombre STRING,
tipo_op INT NOT NULL,
grupo STRING, 
detalle STRING,
sugiere_fecha_actual BOOL,
condicion_pago STRING,
permite_productos BOOL,
productos_valorizados BOOL,
productos_se STRING,
precios_desde STRING,
permite_modificar_precios STRING,
permite_productos_por_cuenta_terceros BOOL,
permite_conceptos BOOL,
permite_partidas_directas BOOL,
permite_valores BOOL,
emite_recibo BOOL,
cuentas_anticipo INT[],
unicidades_que_se_crean INT[],
diferencia_maxima_redondeo INT,
imputaciones JSONB,
signo_renglon_producto INT,
signo_renglon_valor INT,
hace_asiento_costo BOOL,
cuenta_renglon_genera_aplicacion BOOL,
cuenta_patrimonial_genera_aplicacion BOOL,
es_nota_de_credito BOOL,
categorias_ignoradas_en_factura INT[],

inactiva BOOL NOT NULL
);

`}
}
