package ddl

func Productos() Tabla {

	return Tabla{
		Nombre:    "productos",
		DependeDe: "",
		SQL: `
CREATE TABLE productos (
id                  UUID PRIMARY KEY,
comitente			INT NOT NULL REFERENCES comitentes,
codigo_alfanumerico TEXT,
nombre              TEXT,
nombre_impresion    TEXT,
categoria           INT,
descripcion         TEXT,
es_un_combo BOOL,
detalles_combo JSONB,
inventariable       BOOL,
cuantificable       BOOL,
tipo_um             TEXT,
um                  TEXT,
envase              TEXT,
relaciones_um_fija  BOOL,
relaciones_um       JSONB,
facetas JSONB,
atts_generales      JSONB,
tiene_variantes BOOL,
variantes JSONB,
disponible_para_venta BOOL,
precio_final BOOL,
alicuota_iva INT,
alicuota_cero_venta BOOL,

trabaja_con_unicidades BOOL,
clase_unicidad INT REFERENCES unicidades_def,

activo              BOOL,
apertura            JSONB,

created_at          TIMESTAMP WITH TIME ZONE,
updated_at          TIMESTAMP WITH TIME ZONE,
es_estandar         BOOL,
estandar_id         INT,
cruces_proveedores  JSONB
);
`,
	}
}
