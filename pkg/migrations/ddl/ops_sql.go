package ddl

func Ops() Tabla {

	return Tabla{
		Nombre:    "ops",
		DependeDe: "skus",
		SQL: `
CREATE TABLE ops (
	id 
			UUID PRIMARY KEY NOT NULL,
	comitente
			INT NOT NULL REFERENCES comitentes,
 	empresa
	 		INT NOT NULL REFERENCES empresas,
	fecha
	    	DATE NOT NULL,
 	fecha_contable
	    	TIMESTAMP WITH TIME ZONE NOT NULL,
	programa
			TEXT NOT NULL,
 	tran_id             UUID NOT NULL,
   tran_def   INT NOT NULL,

   comp_item    INT,
   comp_id     INT NOT NULL REFERENCES comps,
   comp_nombre  STRING,
   punto_de_venta      INT,
   n_comp              INT NOT NULL,
   n_comp_str        TEXT,

   persona             UUID REFERENCES personas,
   persona_nombre STRING,
   sucursal            INT REFERENCES sucursales,
   
   
   total_comp          INT,
   detalle    TEXT,

   tipo_moneda STRING,
   tipo_cambio INT,
   tipo_cotizacion STRING,

   impresion           JSONB,
   
   fecha_original   DATE NOT NULL,

   tipo_identificacion INT,
   n_identificacion INT,
   condicion_fiscal INT,
   afip_comprobante_id INT,
   afip_comprobante_nombre STRING,
   cae INT,
   cae_vto DATE,
   bar_code STRING,
   afip_fields JSONB,
   qr STRING,
   
   totales_conceptos JSONB,

   created_at          TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
   updated_at          TIMESTAMP WITH TIME ZONE,
   usuario             TEXT NOT NULL
  );
  `,
	}
}

// CREATE INDEX idx_ops_fecha_contable ON ops (fecha);
// CREATE INDEX idx_ops_tran_id ON ops (tran_id);