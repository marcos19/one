package ddl

func Personas() Tabla {

	return Tabla{
		Nombre:    "personas",
		DependeDe: "",
		SQL: `
		CREATE TABLE personas (
			id                  UUID PRIMARY KEY,
			comitente 			INT NOT NULL REFERENCES comitentes (id),
			nombre              TEXT NOT NULL,
			nombre_fantasia     TEXT,   
			tipo                TEXT,
			cuit                BIGINT,
			tipo_cuit           INT,
			dni 				INT,
			tipo_dni            INT,
		
			calle 				TEXT,
			numero				TEXT,
			piso				TEXT,
			depto				TEXT,
			ciudad_id			INT,
			ciudad_cp			INT,
			ciudad_nombre		TEXT,
			provincia_id		INT,
			provincia_nombre	TEXT,
			pais 				STRING,

			domicilios 			JSONB,
			emails              JSONB,
			telefonos           JSONB,
		
			condicion_iva 		INT,
			condicion_gcias     TEXT,
			condicion_mt        TEXT, 
		
			observaciones       TEXT,
		
			activo              BOOL DEFAULT TRUE,
			motivo_inactividad  TEXT,
			usa_sucursales      BOOL,
		
			atts                JSONB,

			cuentas_asociadas JSONB,
			saldo_contable INT,
			saldo_financiero INT,
			
			created_at          TIMESTAMP WITH TIME ZONE,
			updated_at          TIMESTAMP WITH TIME ZONE
		);
		`,
	}
}
