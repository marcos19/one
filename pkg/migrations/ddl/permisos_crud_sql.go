package ddl

func PermisosCrud() Tabla {

	return Tabla{
		Nombre:    "permisos_crud",
		DependeDe: "",
		SQL: `
CREATE TABLE permisos_crud (

id UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
usuario TEXT,
grupo_usuarios INT,
programa TEXT,
empresa INT,
c BOOL,
r BOOL,
u BOOL,
d BOOL

);
 `,
	}
}
