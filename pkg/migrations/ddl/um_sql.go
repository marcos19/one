package ddl

func Ums() Tabla {

	return Tabla{
		Nombre:    "ums",
		DependeDe: "",
		SQL: `
		CREATE TABLE ums (
			id              TEXT PRIMARY KEY,
			nombre          TEXT,
			nombre_plural   TEXT,
			tipo            TEXT,
			base            TEXT,
			factor          FLOAT
		);
		
		INSERT INTO ums (id, nombre, nombre_plural, tipo, base, factor) VALUES
		('u', 'Unidad', 'Unidades', 'Cantidad', 'u', 1),
		
		('t', 'Tonelada', 'Toneladas', 'Peso', 'gr', 1000000),
		('kg', 'Kilogramo', 'Kilogramos', 'Peso', 'gr', 1000),
		('gr', 'Gramo', 'Gramos', 'Peso', 'gr', 1),
		('mg', 'Miligramo', 'Miligramos', 'Peso', 'gr', 0.001)
		
		--('km', 'Kilómetro', 'Kilómetros', 'Distancia'),
		--('hm', 'Hectómetro', 'Hectómetros', 'Distancia'),
		--('dam', 'Decámetro', 'Decámetros', 'Distancia'),
		--('m', 'Metro', 'Metros', 'Distancia'),
		--('dm', 'Decímetro', 'Decímetros', 'Distancia'),
		--('cm', 'Centímetro', 'Centímetros', 'Distancia'),
		--('mm', 'Milímetro', 'Milímetros', 'Distancia'),
		
		--('km2', 'Kilómetro cuadrado', 'Kilómetros', 'Superficie'),
		--('ha', 'Hectárea', 'Hectareas', 'Superficie'),
		--('dam2', 'Decámetro cuadrado', 'Decámetros cuadrados', 'Superficie'),
		--('m2', 'Metro cuadrado', 'Metros cuadrados', 'Superficie'),
		--('dm2', 'Decímetro cuadrado', 'Decímetros cuadrados', 'Superficie'),
		--('cm2', 'Centímetro cuadrado', 'Centímetros cuadrados', 'Superficie'),
		--('mm2', 'Milímetro cuadrado', 'Milímetros cuadrados', 'Superficie'),
		
		--('m3', 'Metro cúbico', 'Metros cúbicos', 'Volumen'),
		--('dm3', 'Decímetro cúbico', 'Decímetros cúbicos', 'Volumen'),
		--('cm3', 'Centímetro cúbico', 'Centímetros cúbicos', 'Volumen'),
		--('mm3', 'Milímetro cúbico', 'Milímetros cúbicos', 'Volumen'),
		--('L', 'Litro', 'Litros', 'Volumen'),
		--('ml', 'Mililitro', 'Mililitros', 'Volumen')
		;
		`,
	}
}
