package ddl

func FactEsquemas() Tabla {

	return Tabla{
		Nombre:    "fact_esquemas",
		DependeDe: "",
		SQL: `
CREATE TABLE fact_esquemas (

id SERIAL PRIMARY KEY,
comitente INT NOT NULL REFERENCES comitentes,
empresa INT NOT NULL REFERENCES empresas,

config INT NOT NULL,
grupo STRING NOT NULL,
comprobantes JSONB,
cae STRING,
permisos JSONB,
cajas INT[],
valores INT[],
depositos INT[],
condiciones INT[],
partidas_directas_config JSONB,

nombre STRING NOT NULL,
detalle STRING
);
`,
	}
}
