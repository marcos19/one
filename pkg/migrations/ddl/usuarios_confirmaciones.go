package ddl

func UsuariosConfirmaciones() Tabla {

	return Tabla{
		Nombre:    "usuarios_confirmaciones",
		DependeDe: "",
		SQL: `
		CREATE TABLE usuarios_confirmaciones	 (
			id          					UUID PRIMARY KEY,
			user_id 						TEXT NOT NULL,
			created_at 						TIMESTAMP WITH TIME ZONE,
			motivo							TEXT,
			confirmada	  					BOOL,
			fecha_confirmacion				TIMESTAMP WITH TIME ZONE
			
		);`,
	}
}
