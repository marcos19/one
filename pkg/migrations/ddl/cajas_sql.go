package ddl

func Cajas() Tabla {

	return Tabla{
		Nombre:    "cajas",
		DependeDe: "",
		SQL: `
CREATE TABLE cajas(

id SERIAL PRIMARY KEY,
comitente INT NOT NULL REFERENCES comitentes,
nombre TEXT,
acepta_saldo_negativo BOOL,
otras_cuentas INT8[] NULL,
created_at TIMESTAMPTZ,
updated_at TIMESTAMPTZ
);
		`,
	}
}

// INSERT
// INTO cajas
// (id, comitente, nombre ) VALUES
// (1, 1, 'Tesorería'),
// (2, 1, 'Mostrador');
