package ddl

func Conceptos() Tabla {

	return Tabla{
		Nombre:    "conceptos",
		DependeDe: "",
		SQL: `
		CREATE TABLE conceptos (
			id                      SERIAL PRIMARY KEY,
			nombre                  TEXT NOT NULL,
			nombre_impresion        TEXT NOT NULL,
			familia                 TEXT NOT NULL,
			concepto_de_iva 		TEXT,
			genera_concepto         INT NOT NULL,
			modo_calculo            TEXT,
			minimo_no_imponible     INT,
			deduccion               INT,
			porcentaje              INT,
			monto_fijo              INT,
			monto_maximo            INT,
			monto_minimo            INT,
			created_at              TIMESTAMP WITH TIME ZONE,
			updated_at              TIMESTAMP WITH TIME ZONE
		);
		
		`,
	}
}

// INSERT INTO conceptos
//  (id,
//  nombre,
//  nombre_impresion,
//  familia,
//  concepto_de_iva,
//  genera_concepto,
//  modo_calculo,
//  minimo_no_imponible,
//  deduccion,
//  porcentaje,
//  monto_fijo,
//  monto_maximo,
//  monto_minimo,
//  created_at,
//  updated_at) VALUES

//  (1, 1, 'Base IVA DF 0%', 'Neto 0%', 'IVA', 'Neto', 0, '', 0, 0, 0, 0, 0, 0, now(), now()),
//  (2, 1, 'Base IVA DF 2,5%', 'Neto 2%', 'IVA','Neto', 0, '',0, 0,  25000,0, 0, 0, now(), now()),
//  (5, 1, 'Base IVA DF 5%', 'Neto 5%', 'IVA','Neto', 0, '',	0, 0,  50000,0, 0, 0, now(), now()),
//  (10, 1, 'Base IVA DF 10,5%', 'Neto 10,5%', 'IVA','Neto', 0, '',  0, 0, 105000,0, 0, 0, now(), now()),
//  (21, 1, 'Base IVA DF 21%', 'Neto 21%', 'IVA','Neto', 0, '',0,  0, 210000, 0, 0, 0, now(), now()),
//  (27, 1, 'Base IVA DF 27%', 'Neto 27%', 'IVA','Neto', 0, '',0,  0,  270000,0, 0, 0, now(), now()),

//  (101, 1, 'IVA DF 0%', 'IVA 0%', 'IVA', 'IVA',  0, '',0, 0, 0, 0, 0, 0, now(), now()),
//  (102, 1, 'IVA DF 2,5%', 'IVA 2%', 'IVA', 'IVA', 0, '',0, 0,  25000, 0, 0, 0, now(), now()),
//  (105, 1, 'IVA DF 5%', 'IVA 5%', 'IVA',  'IVA', 0, '',0, 0,  50000, 0, 0, 0, now(), now()),
//  (110, 1, 'IVA DF 10,5%', 'IVA 10,5%', 'IVA',  'IVA', 0, '',0, 0,  105000, 0, 0, 0, now(), now()),
//  (121, 1, 'IVA DF 21%', 'IVA 21%', 'IVA',  'IVA', 0, '',0, 0, 210000, 0,  0, 0, now(), now()),
//  (127, 1, 'IVA DF 27%', 'IVA 27%', 'IVA',  'IVA', 0, '',0, 0,  270000,  0, 0, 0, now(), now()),

//  (201, 1, 'Base IVA CF 0%', 'Neto 0%', 'IVA','Neto', 0, '',0, 0, 0, 0, 0, 0, now(), now()),
//  (202, 1, 'Base IVA CF 2,5%', 'Neto 2%', 'IVA','Neto', 0, '', 0, 0,  25000, 0, 0, 0, now(), now()),
//  (205, 1, 'Base IVA CF 5%', 'Neto 5%', 'IVA','Neto', 0, '', 0, 0,  50000, 0, 0, 0, now(), now()),
//  (210, 1, 'Base IVA CF 10,5%', 'Neto 10,5%', 'IVA', 'Neto',0, '', 0, 0,  105000, 0, 0, 0, now(), now()),
//  (221, 1, 'Base IVA CF 21%', 'Neto 21%', 'IVA', 'Neto',0, '',0,  0,   210000, 0, 0, 0, now(), now()),
//  (227, 1, 'Base IVA CF 27%', 'Neto 27%', 'IVA', 'Neto',0, '',0, 0,  270000, 0, 0, 0, now(), now()),

//  (301, 1, 'IVA CF 0%', 'IVA 0%', 'IVA', 'IVA',0, '', 0, 0, 0, 0, 0, 0, now(), now()),
//  (302, 1, 'IVA CF 2,5%', 'IVA 2%', 'IVA', 'IVA',0,'', 0, 0,  25000,0, 0, 0, now(), now()),
//  (305, 1, 'IVA CF 5%', 'IVA 5%', 'IVA','IVA', 0, '', 0, 0,  50000, 0, 0, 0, now(), now()),
//  (310, 1, 'IVA CF 10,5%', 'IVA 10,5%', 'IVA', 'IVA', 0,'', 0, 0,  105000,  0, 0, 0, now(), now()),
//  (321, 1, 'IVA CF 21%', 'IVA 21%', 'IVA', 'IVA',0, '', 0, 0, 210000,  0, 0, 0, now(), now()),
//  (327, 1, 'IVA CF 27%', 'IVA 27%', 'IVA','IVA', 0,  '',0, 0,  270000,0, 0, 0, now(), now()),

//  (450, 1, 'No Gravado en IVA', 'No Gravado', 'IVA', 'No Gravado', 0, '',0, 0, null, 0, 0, 0, now(), now()),
//  (451, 1, 'Exento de IVA', 'IVA Exento', 'IVA', 'Exento', 0,'', 0,  0,  null, 0, 0, 0, now(), now()),
//  (460, 1, 'Percepción de IVA', 'Percepción IVA', 'IVA', 'Percepciones', 0,'', 0,  0,  null, 0, 0, 0, now(), now()),
//  (461, 1, 'Impuestos Internos', 'Impuestos internos', 'IVA', 'Otros Impuestos', 0,'', 0,  0,  null, 0, 0, 0, now(), now()),
//  (462, 1, 'ITC', 'ITC', 'IVA', 'Otros Impuestos', 0,'', 0,  0,  null, 0, 0, 0, now(), now()),
//  (463, 1, 'Enard', 'Enard', 'IVA', 'Otros Impuestos', 0,'', 0,  0,  null, 0, 0, 0, now(), now()),
//  (464, 1, 'Sellados provinciales', 'Impuestos internos', 'IVA', 'Otros Impuestos', 0,'', 0,  0,  null, 0, 0, 0, now(), now()),
//  (465, 1, 'Impuesto inmobiliario', 'Impuestos internos', 'IVA', 'Otros Impuestos', 0,'', 0,  0,  null, 0, 0, 0, now(), now()),
//  (469, 1, 'Otros impuestos', 'Impuestos internos', 'IVA', 'Otros Impuestos', 0,'', 0,  0,  null, 0, 0, 0, now(), now()),

//   (470, 1, 'Percepción Ingresos Brutos Santa Fe', 'Percepción ISIB Sta Fe', 'IVA', 'Percepciones', 0,'', 0,  0,  null, 0, 0, 0, now(), now()),
//   (471, 1, 'Percepción Ingresos Brutos Buenos Aires', 'Percepción ISIB Bs As', 'IVA', 'Percepciones', 0,'', 0,  0,  null, 0, 0, 0, now(), now()),
//   (472, 1, 'Percepción Ingresos Brutos Entre Ríos', 'Percepción ISIB Entre Ríos', 'IVA', 'Percepciones', 0,'', 0,  0,  null, 0, 0, 0, now(), now()),
//   (473, 1, 'Percepción Ingresos Brutos Córdoba', 'Percepción ISIB Cdba', 'IVA', 'Percepciones', 0,'', 0,  0,  null, 0, 0, 0, now(), now())
//  ;
