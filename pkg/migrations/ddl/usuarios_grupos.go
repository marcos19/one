package ddl

func UsuariosGrupos() Tabla {

	return Tabla{
		Nombre:    "usuarios_grupos",
		DependeDe: "",
		SQL: `
CREATE TABLE usuarios_grupos (
id SERIAL PRIMARY KEY,
comitente INT NOT NULL,
nombre STRING NOT NULL,
detalle STRING NOT NULL,
usuarios STRING[],

created_at TIMESTAMP WITH TIME ZONE,
updated_at TIMESTAMP WITH TIME ZONE
);
`,
	}
}
