package ddl

func Contratos() Tabla {

	return Tabla{
		Nombre:    "contratos",
		DependeDe: "",
		SQL: `
		CREATE TABLE contratos(
			id                  SERIAL PRIMARY KEY,
			tipo                TEXT,
			nombre              TEXT,
			created_at          TIMESTAMP WITH TIME ZONE,
			updated_at          TIMESTAMP WITH TIME ZONE
			
		);`,
	}
}
