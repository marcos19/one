package ddl

func Condiciones() Tabla {

	return Tabla{
		Nombre:    "condiciones",
		DependeDe: "",
		SQL: `
CREATE TABLE condiciones (

id SERIAL PRIMARY KEY,
comitente INT NOT NULL REFERENCES comitentes,
nombre TEXT,
modalidad STRING,
cantidad_cuotas INT,
cuotas JSONB,
valida_desde DATE,
valida_hasta DATE,
created_at TIMESTAMP WITH TIME ZONE, 
updated_at TIMESTAMP WITH TIME ZONE

);
`,
	}
}
