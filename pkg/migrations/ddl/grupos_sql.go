package ddl

func Grupos() Tabla {

	return Tabla{
		Nombre:    "grupos",
		DependeDe: "",
		SQL: `
		CREATE TABLE grupos (
			id              	SERIAL PRIMARY KEY,
			de 					TEXT,
			nombre	 			TEXT,
			ints				INTEGER[],
			uuids				UUID[],
			strings 			TEXT[]
		);
		`,
	}
}
