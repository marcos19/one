package ddl

func Fact() Tabla {

	return Tabla{
		Nombre:    "fact",
		DependeDe: "",
		SQL: `
CREATE TABLE fact(

id UUID PRIMARY KEY,
comitente INT NOT NULL references comitentes,
empresa INT NOT NULL references empresas,
config INT NOT NULL references fact_config,
esquema INT NOT NULL references fact_esquemas,
fecha DATE NOT NULL,
persona JSONB,
condicion_pago INT,
detalle STRING,
productos JSONB,
partidas_directas JSONB,
valores JSONB,
caja INT,

comp_original INT,
fecha_original DATE,
punto_de_venta INT,
n_comp INT,

totales_conceptos JSONB,
vencimientos JSONB,
vencimientos_pago JSONB,

aplicaciones JSONB, 
aplicaciones_esta_factura JSONB,
usuario STRING,
created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);

`}
}
