package ddl

func Sucursales() Tabla {

	return Tabla{
		Nombre: "sucursales",
		SQL: `
CREATE TABLE sucursales (
id              SERIAL PRIMARY KEY,
persona_id UUID REFERENCES personas,
comitente INT NOT NULL REFERENCES comitentes,
n_sucursal   INT,
nombre       TEXT,
calle   TEXT,
numero   TEXT,
piso   TEXT,
depto   TEXT,
ciudad_id  INT,
ciudad_cp  INT,
ciudad_nombre STRING,
provincia_id  INT,
provincia_nombre STRING,
pais STRING,

domicilios    JSONB,
emails              JSONB,
telefonos           JSONB,
atts                JSONB,

cuentas_asociadas JSONB,

activo              BOOL DEFAULT TRUE,
motivo_inactividad  TEXT,

created_at      TIMESTAMP WITH TIME ZONE,
updated_at      TIMESTAMP WITH TIME ZONE
  );
  `,
	}
}
