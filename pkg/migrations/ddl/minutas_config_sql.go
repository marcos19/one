package ddl

func MinutasConfig() Tabla {

	return Tabla{
		Nombre:    "minutas_config",
		DependeDe: "",
		SQL: `
CREATE TABLE minutas_config (

id SERIAL PRIMARY KEY,
comitente INT NOT NULL REFERENCES comitentes,
empresa INT NOT NULL REFERENCES empresas,
nombre STRING NOT NULL,
detalle STRING,
created_at TIMESTAMP WITH TIME ZONE,
updated_at TIMESTAMP WITH TIME ZONE
);

`,
	}
}
