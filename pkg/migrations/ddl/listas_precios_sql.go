package ddl

func ListasDePrecios() Tabla {

	return Tabla{
		Nombre:    "listas_precios",
		DependeDe: "",
		SQL: `
CREATE TABLE listas_precios (

id SERIAL PRIMARY KEY,
comitente INT NOT NULL REFERENCES comitentes,
nombre TEXT,
moneda STRING,
tipo STRING,
lista_base INT,
porcentaje INT,
valida_desde DATE,
valida_hasta DATE,

created_at          TIMESTAMP WITH TIME ZONE,
updated_at          TIMESTAMP WITH TIME ZONE
);

`,
	}
}
