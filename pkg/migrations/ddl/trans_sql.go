package ddl

func Trans() Tabla {

	return Tabla{
		Nombre:    "trans",
		DependeDe: "",
		SQL: `
CREATE TABLE trans (
id SERIAL PRIMARY KEY,
comitente INT NOT NULL REFERENCES comitentes,
empresa INT NOT NULL REFERENCES empresas,
programa STRING NOT NULL,
nombre STRING NOT NULL,
detalle STRING,
fact JSONB
);
`,
		Data: `
			
		`,
	}
}
