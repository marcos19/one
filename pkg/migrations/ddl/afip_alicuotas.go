package ddl

func AfipAlicuotas() Tabla {

	return Tabla{
		Nombre:    "afip_alicuotas",
		DependeDe: "",
		SQL: `
			CREATE TABLE  afip_alicuotas (
				id              INT PRIMARY KEY,
				nombre          TEXT,
				porcentaje		INT
			);
INSERT INTO afip_alicuotas (id, nombre, porcentaje) VALUES 
(1, 'No gravado', 0),
(2, 'Exento', 0),
(3, '0%', 0),
(9, '2,50%', 25000 ),
(8, '5%', 50000),
(4, '10,50%' , 105000),
(5, '21%', 210000),
(6, '27%', 270000);
`}
}
