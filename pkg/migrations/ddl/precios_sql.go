package ddl

func Precios() Tabla {

	return Tabla{
		Nombre:    "precios",
		DependeDe: "",
		SQL: `

CREATE TABLE  precios(

comitente INT,
sku UUID ,
empresa INT,
lista INT,

precio_neto INT,
precio_final INT,
precio_fecha DATE,

valido_desde DATE,
valido_hasta DATE,
created_at TIMESTAMP WITH TIME ZONE,
updated_at TIMESTAMP WITH TIME ZONE,

PRIMARY KEY (comitente, sku, empresa, lista)
);

`,
	}
}
