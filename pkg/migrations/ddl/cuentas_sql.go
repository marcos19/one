package ddl

func Cuentas() Tabla {

	return Tabla{
		Nombre:    "cuentas",
		DependeDe: "",
		SQL: `
CREATE TABLE IF NOT EXISTS cuentas (
      id SERIAL PRIMARY KEY,
      comitente INT NOT NULL REFERENCES comitentes,
      codigo TEXT NOT NULL,
      orden INT8 NULL,
      nombre TEXT NOT NULL,
      apertura_persona BOOL NOT NULL DEFAULT false,
      apertura_persona_restrictiva BOOL NOT NULL DEFAULT false,
      apertura_producto BOOL NOT NULL DEFAULT false,
      apertura_caja BOOL NOT NULL DEFAULT false,
      apertura_deposito BOOL NOT NULL DEFAULT false,
      apertura_contrato BOOL NOT NULL DEFAULT false,
      apertura_unicidad BOOL NOT NULL DEFAULT false,
      apertura_centro BOOL NOT NULL DEFAULT false,
      tipo_unicidad INT8 NULL,
      centros_permitidos INT8[] NULL,
      tipo_cuenta STRING NULL,
      ajusta_por_inflacion BOOL NULL,
      funcion STRING NULL,
      medio_de_pago BOOL NULL,
      alias STRING NULL,
      monetaria BOOL NULL,
      moneda_extranjera BOOL NULL,
      es_sistemica BOOL NULL,
      usa_aplicaciones BOOL NULL,
      padre_id INT8 NULL,
      descripcion STRING NULL,
      concepto_iva_compras STRING NULL,
      centros_grupos INT8[] NULL,
      concepto_producto INT8 NULL,
	  rubro_bsuso INT8 NULL,
      created_at TIMESTAMPTZ NULL,
      updated_at TIMESTAMPTZ NULL
);
		`,
	}
}
