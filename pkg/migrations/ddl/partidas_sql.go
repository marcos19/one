package ddl

func Partidas() Tabla {

	return Tabla{
		Nombre:    "partidas",
		DependeDe: "ops",
		SQL: `
  CREATE TABLE partidas(
   id                  UUID PRIMARY KEY,
   comitente INT REFERENCES comitentes,

   fecha_contable      DATE NOT NULL,
   fecha_financiera    DATE NOT NULL,
   time_stamp_contable TIMESTAMP WITH TIME ZONE NOT NULL,
   op_id               UUID NOT NULL REFERENCES ops,

   empresa             INT NOT NULL REFERENCES empresas,

   tran_def INT,
   comp_def INT REFERENCES comps,
   
   created_at          TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
   updated_at          TIMESTAMP WITH TIME ZONE,
   usuario             TEXT NOT NULL,
   
   pata STRING,


   comp_nombre  STRING,
   n_comp_str STRING,

   ---------------------------------------------------------------------------
   -- Propios de la partida
   cuenta INT NOT NULL REFERENCES cuentas,
   cuenta_nombre TEXT NOT NULL,
   centro              INT REFERENCES centros,
   persona             UUID REFERENCES personas,
   sucursal            INT REFERENCES sucursales,
   caja     INT REFERENCES cajas, 
   producto            UUID REFERENCES productos,
   sku                 UUID REFERENCES skus,
   deposito   INT REFERENCES depositos, 
   unicidad            UUID REFERENCES unicidades,
   unicidad_tipo       INT,
   contrato            INT REFERENCES contratos,
   concepto_iva INT,
   alicuota_iva INT,
  
   monto                   INT,
   moneda                  TEXT,
   monto_moneda_original   INT,
   tc                      INT,
  
   um             TEXT,
   cantidad                INT,
  
   partida_aplicada        UUID,
  
   detalle     TEXT,

   tipo_asiento INT
  );
  `,
	}
}

// Indices:
//
// CREATE INDEX idx_fecha_contable ON partidas (fecha_contable);
// CREATE INDEX idx_op_id ON partidas (op_id);
