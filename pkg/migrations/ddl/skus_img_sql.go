package ddl

func SkuImg() Tabla {

	return Tabla{
		Nombre:    "skus_img",
		DependeDe: "",
		SQL: `
		CREATE TABLE skus_img (
			id                  UUID PRIMARY KEY NOT NULL,
			comitente			INT NOT NULL REFERENCES comitentes,
			producto_id			UUID REFERENCES productos,
			sku_id				UUID REFERENCES skus,
					
			created_at          TIMESTAMP WITH TIME ZONE,
			updated_at          TIMESTAMP WITH TIME ZONE
		
		);
		`,
	}
}
