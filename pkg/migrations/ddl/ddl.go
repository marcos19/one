package ddl

type Tabla struct {
	Nombre    string
	SQL       string
	Data      string
	DependeDe string
}
