package ddl

func AfipCertificados() Tabla {

	return Tabla{
		Nombre:    "afip_certificados",
		DependeDe: "",
		SQL: `
CREATE TABLE afip_certificados(
id              SERIAL PRIMARY KEY,
comitente INT NOT NULL REFERENCES comitentes,
empresa INT NOT NULL REFERENCES empresas,
alias TEXT,
desde TIMESTAMP WITH TIME ZONE,
hasta TIMESTAMP WITH TIME ZONE,
certificado BYTES,
created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);
`,
	}
}
