package ddl

func Provincias() Tabla {

	return Tabla{
		Nombre:    "provincias",
		DependeDe: "",
		SQL: `
		CREATE TABLE provincias (
			id          int PRIMARY KEY,
			nombre      TEXT
		);
		INSERT INTO provincias (id, nombre) VALUES
		
			(901, 'Ciudad de Buenos Aires'),
			(902, 'Provincia de Buenos Aires'),
			(903, 'Catamarca'),
			(904, 'Córdoba'),
			(905, 'Corrientes'),
			(906, 'Chaco'),
			(907, 'Chubut'),
			(908, 'Entre Ríos'),
			(909, 'Formosa'),
			(910, 'Jujuy'),
			(911, 'La Pampa'),
			(912, 'La Rioja'),
			(913, 'Mendonza'),
			(914, 'Misiones'),
			(915, 'Neuquén'),
			(916, 'Río Negro'),
			(917, 'Salta'),
			(918, 'San Juan'),
			(919, 'San Luis'),
			(920, 'Santa Cruz'),
			(921, 'Santa Fe'),
			(922, 'Santiago del Estero'),
			(923, 'Tierra del Fuego'),
			(924, 'Tucumán');
		`,
	}
}
