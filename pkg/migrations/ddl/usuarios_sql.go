package ddl

func Usuarios() Tabla {

	return Tabla{
		Nombre:    "usuarios",
		DependeDe: "",
		SQL: `
CREATE TABLE usuarios (
id TEXT PRIMARY KEY,
email TEXT UNIQUE,
nombre TEXT NOT NULL,
apellido TEXT,
dni INT,
pie_mail STRING,
comitentes INT[],
hash TEXT,
blanquear_proximo_ingreso BOOL,
estado TEXT,
ultima_actualizacion_contraseña TIMESTAMP WITH TIME ZONE,

administrador BOOL,
inactivo BOOL,

created_at TIMESTAMP WITH TIME ZONE,
updated_at TIMESTAMP WITH TIME ZONE
);
`,
	}
}
