package ddl

func Depositos() Tabla {

	return Tabla{
		Nombre:    "depositos",
		DependeDe: "",
		SQL: `
		CREATE TABLE depositos(
			id              SERIAL PRIMARY KEY,
			nombre          TEXT,
			disponible_para_venta BOOL,
			acepta_stock_negativo BOOL,
			valido_desde    DATE,
			valido_hasta    DATE,
			created_at      TIMESTAMP WITH TIME ZONE, 
			updated_at      TIMESTAMP WITH TIME ZONE

		);
		`,
	}
}

// INSERT
// INTO depositos
// (id, comitente, nombre ) VALUES
// (1, 1, 'Principal'),
// (2, 1,  'Secundario');
