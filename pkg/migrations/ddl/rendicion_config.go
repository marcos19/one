package ddl

func RendicionConfig() Tabla {

	return Tabla{
		Nombre:    "rendicion_config",
		DependeDe: "",
		SQL: `
CREATE TABLE rendiciones_config(

id SERIAL PRIMARY KEY,
comitente INT NOT NULL references comitentes,
empresa INT NOT NULL references empresas,
nombre STRING,
comp INT NOT NULL REFERENCES comps, 
cajas_desde INT[],
cajas_hacia INT[],
cuentas INT[],

usuario STRING,
created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
updated_at TIMESTAMP WITH TIME ZONE 
);

`}
}
