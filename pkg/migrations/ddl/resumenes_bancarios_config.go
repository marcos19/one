package ddl

func ResumenesBancariosConfig() Tabla {

	return Tabla{
		Nombre:    "rendicion",
		DependeDe: "",
		SQL: `
CREATE TABLE resumenes_bancarios_config(

id SERIAL PRIMARY KEY,
comitente INT NOT NULL references comitentes,
empresa INT NOT NULL references empresas,

nombre STRING,
persona UUID,
sucursal INT,
descripcion STRING,
csv_config JSONB,
imputaciones JSONB,

comp_gastos INT,
comp_movs INT,
cuenta_contable_gastos INT,
cuenta_contable_movs INT,

updated_at TIMESTAMP WITH TIME ZONE,
created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);

`}
}
