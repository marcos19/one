package ddl

func Unicidades() Tabla {

	return Tabla{
		Nombre:    "unicidades",
		DependeDe: "",
		SQL: `
CREATE TABLE unicidades (
id UUID PRIMARY KEY,
comitente INT NOT NULL REFERENCES comitentes,
definicion INT NOT NULL REFERENCES unicidades_def,
atts_indexados JSONB,
atts JSONB,
created_at TIMESTAMP WITH TIME ZONE,
updated_at TIMESTAMP WITH TIME ZONE
);
`,
	}
}
