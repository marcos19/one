package ddl

func MinutasEsquemas() Tabla {

	return Tabla{
		Nombre:    "minutas_esquemas",
		DependeDe: "",
		SQL: `
CREATE TABLE minutas_esquemas (

id SERIAL PRIMARY KEY,
comitente INT NOT NULL REFERENCES comitentes,
empresa INT NOT NULL REFERENCES empresas,

nombre STRING NOT NULL,
detalle STRING,

config INT NOT NULL,
comp INT NOT NULL, 
created_at TIMESTAMP WITH TIME ZONE,
updated_at TIMESTAMP WITH TIME ZONE
);
`,
	}
}
