package ddl

func Skus() Tabla {

	return Tabla{
		Nombre:    "skus",
		DependeDe: "productos",
		SQL: `
		CREATE TABLE skus (
			id                  UUID PRIMARY KEY,
			producto            UUID REFERENCES productos NOT NULL,
			comitente			INT NOT NULL REFERENCES comitentes,
			codigo				TEXT,
			nombre              TEXT,
			nombre_impresion    TEXT,
			categoria           INT,
			bar_code			STRING,
			inventariable       BOOL,
			cuantificable       BOOL,
			tipo_um             TEXT,
			um                  TEXT,
			envase              TEXT,
			relaciones_um_fija  BOOL,
			relaciones_um       JSONB,
			atts                JSONB,
			activo              BOOL,
		
			created_at          TIMESTAMP WITH TIME ZONE,
			updated_at          TIMESTAMP WITH TIME ZONE,
		
			atts_especificos    JSONB,
		
			es_estandar         BOOL,
			estandar_id         INT,  
			cruces_proveedores  JSONB
		);
		`,
	}
}
