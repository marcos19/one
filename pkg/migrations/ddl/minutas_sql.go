package ddl

func Minutas() Tabla {

	return Tabla{
		Nombre:    "minutas",
		DependeDe: "",
		SQL: `
CREATE TABLE minutas (

id UUID PRIMARY KEY NOT NULL,
comitente INT NOT NULL REFERENCES comitentes,
empresa INT NOT NULL REFERENCES empresas,
config INT NOT NULL REFERENCES minutas_config, 
esquema INT NOT NULL REFERENCES minutas_esquemas,
comp INT NOT NULL,
fecha DATE NOT NULL,
punto_de_venta INT NOT NULL,
n_comp INT NOT NULL,
detalle STRING,
partidas JSONB,
created_at TIMESTAMP WITH TIME ZONE,
updated_at TIMESTAMP WITH TIME ZONE,
usuario STRING
);
`,
	}
}
