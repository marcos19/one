package ddl

func Comps() Tabla {

	return Tabla{
		Nombre:    "comps",
		DependeDe: "",
		SQL: `
CREATE TABLE comps (
id SERIAL PRIMARY KEY,
comitente INT NOT NULL REFERENCES comitentes,
empresa INT NOT NULL REFERENCES empresas,
tipo STRING NOT NULL, 
nombre STRING NOT NULL,
detalle STRING,
punto_de_venta INT,
tipo_emision STRING,
tipo_numeracion STRING,
tipo_anulacion STRING,
formato_numeracion STRING,
ultimo_numero_usado INT,
emisor INT NOT NULL DEFAULT 0,
imprime_datos_cajero BOOL,
calcula_iva BOOL,
discrimina_iva BOOL,
letra STRING,
va_al_libro_de_iva BOOL,
libro_iva STRING,
comprobante_fijo BOOL,
originales_aceptados INT[],

margen_izq INT,
margen_der INT,
margen_sup INT,
margen_inf INT,
altura_cabecera INT,
altura_persona INT,
altura_pie INT,
imprime_logo BOOL,
imprime_bordes BOOL,
imprime_nombre_usuario BOOL,
imprime_ts BOOL,

productos_nombre_impresion BOOL,
productos_ocultar_codigo BOOL,
productos_ocultar_cantidades BOOL,
punto_de_venta_obligatorio STRING,
inactivo BOOL,
compra_o_venta STRING,
fecha_desde DATE,
fecha_hasta DATE,
mail_template STRING,

created_at TIMESTAMP WITH TIME ZONE,
updated_at TIMESTAMP WITH TIME ZONE
);

		`,
	}
}

// abreviatura STRING,
// 	nombre_impresion STRING,
// 	descripcion STRING,
// 	punto_de_venta_fijo BOOL,
// 	punto_de_venta INT,
// 	numerar_por STRING,
// 	es_propio BOOL,
// 	signo STRING,
// 	tipo_anulacion STRING,

// 	valido_desde DATE,
// 	valido_hasta DATE,
// 	created_at TIMESTAMPTZ,
// 	updated_at TIMESTAMPTZ,
// 	created_by STRING,
// 	updated_by STRING,

// 	margen_superior INT,
// 	margen_inferior INT,
// 	margen_izquierdo INT,
// 	margen_derecho INT,
// 	tamaño_cabecera INT,
// 	tamaño_detalle_cliente INT,
// 	tamaño_pie INT,
// 	tamaño_totalizado INT,
// 	hoja_ancho INT,
// 	hoja_alto INT,
// 	cantidad_copias INT,
// 	cantidad_copias_hoja INT,
// 	se_imprime_por_default BOOL,
// 	es_fiscal BOOL,
// 	originales_aceptados INT[],
// 	letra STRING,
// 	condiciones_fiscales_aceptadas STRING[]
