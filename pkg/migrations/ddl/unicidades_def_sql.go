package ddl

func UnicidadesDefiniciones() Tabla {

	return Tabla{
		Nombre:    "unicidades_def",
		DependeDe: "",
		SQL: `
CREATE TABLE unicidades_def (
id SERIAL PRIMARY KEY,
comitente INT REFERENCES comitentes,
nombre STRING,
nombre_singular STRING,
descripcion STRING,
tipo STRING,
predefinida STRING,
genero STRING,
atts JSONB,
atts_indexados JSONB,
estados JSONB, 
activo BOOL,
created_at TIMESTAMP WITH TIME ZONE,
updated_at TIMESTAMP WITH TIME ZONE
);

CREATE INDEX ON unicidades_def USING GIN (atts_indexados);
		`,
	}
}
