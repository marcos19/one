package ddl

func AfipIdentificaciones() Tabla {

	return Tabla{
		Nombre:    "afip_identificaciones",
		DependeDe: "",
		SQL: `
CREATE TABLE afip_identificaciones (
id INT PRIMARY KEY,
nombre TEXT
);

INSERT INTO afip_identificaciones (id, nombre) VALUES 
(80, 'CUIT'),
(86, 'CUIL'),
(87, 'CDI'),
(96, 'DNI'),
(99, 'No identificado')
;
`}
}
