package ddl

func InflacionIndices() Tabla {

	return Tabla{
		Nombre:    "indices_inflacion",
		DependeDe: "",
		SQL: `
CREATE TABLE indices_inflacion (
comitente INT NOT NULL REFERENCES comitentes,
indice TEXT,
mes DATE,
coeficiente FLOAT,
created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW(),

CONSTRAINT "primary" PRIMARY KEY (
comitente, indice, mes
) 
)
;

;
		`,
	}
}

// INSERT INTO indices_inflacion (comitente, indice, mes, coeficiente) VALUES
// (1, 'IPC', '2019-05-01', 219.5691),
// (1, 'IPC', '2019-06-01', 225.537),
// (1, 'IPC', '2019-07-01', 230.4940),
// (1, 'IPC', '2019-08-01', 239.6077),
// (1, 'IPC', '2019-09-01', 253.7102),
// (1, 'IPC', '2019-10-01', 262.0661),
// (1, 'IPC', '2019-11-01', 273.2158),
// (1, 'IPC', '2019-12-01', 283.4442),
// (1, 'IPC', '2020-01-01', 289.2899),
// (1, 'IPC', '2020-02-01', 295.6660),
// (1, 'IPC', '2020-03-01', 305.5515),
// (1, 'IPC', '2020-04-01', 310.1243)
