package ddl

func PermisosLookup() Tabla {

	return Tabla{
		Nombre:    "permisos_lookup",
		DependeDe: "",
		SQL: `
		CREATE TABLE permisos_lookup (
			id              	UUID PRIMARY KEY NOT NULL DEFAULT gen_random_uuid(),
			usuario 			TEXT,
			grupo_usuarios		INT,
			para				TEXT,
			int					INT,
			string				TEXT
		);
		 `,
		Data: `
		INSERT INTO permisos_lookup (usuario, programa, grupo_usuarios, para, int, string) VALUES (
		'test', 'balance', null, 'Empresas', 1, null), (
		'test', 'balance', null, 'Empresas', 2, null)
		
		;`,
	}
}
