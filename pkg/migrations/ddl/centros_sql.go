package ddl

func Centros() Tabla {

	return Tabla{
		Nombre:    "centros",
		DependeDe: "",
		SQL: `
			CREATE TABLE centros (
				id              SERIAL PRIMARY KEY,
				comitente 		INT NOT NULL REFERENCES comitentes,
				nombre          TEXT,
				es_familia_de_productos BOOL, 
				valido_desde 	DATE,
				valido_hasta 	DATE,
				created_at      TIMESTAMP WITH TIME ZONE, 
				updated_at      TIMESTAMP WITH TIME ZONE
			);`,
	}
}
