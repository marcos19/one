package ddl

func Domicilios() Tabla {

	return Tabla{
		Nombre:    "domicilios",
		DependeDe: "personas",
		SQL: `
		CREATE TABLE domicilios(
			id              UUID,
			comitente       INT NOT NULL REFERENCES comitentes,
			persona_id		UUID REFERENCES personas,
			tipo			TEXT,
			calle			TEXT,
			numero			TEXT,
			piso			TEXT,
			depto			TEXT,
			ciudad_id		INT,
			ciudad_cp		INT,
			ciudad_nombre	TEXT,
			provincia_id	INT,
			provincia_nombre TEXT,
			created_at      TIMESTAMP WITH TIME ZONE,
			updated_at      TIMESTAMP WITH TIME ZONE
		);
		`,
	}
}
