package ddl

func Preciosprov() Tabla {

	return Tabla{
		Nombre:    "preciosprov",
		DependeDe: "",
		SQL: `
		CREATE TABLE preciosprov (
			id                      SERIAL PRIMARY KEY,
			comintente				INT NOT NULL REFERENCES comitentes,
			fecha                   DATE,
			proveedor_id            INT,
			proveedor_nombre        TEXT,
			producto_id             TEXT,
			producto_nombre         TEXT,
			condicion_pago          TEXT,
			precio_neto             INT,
			precio_con_iva          INT,
			created_at              TIMESTAMP WITH TIME ZONE,
			updated_at              TIMESTAMP WITH TIME ZONE,
			activo                  BOOL
		);
		`,
	}
}
