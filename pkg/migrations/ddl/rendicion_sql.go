package ddl

func Rendicion() Tabla {

	return Tabla{
		Nombre:    "rendiciones",
		DependeDe: "",
		SQL: `
CREATE TABLE rendiciones (

id UUID PRIMARY KEY,
comitente INT NOT NULL references comitentes,
empresa INT NOT NULL references empresas,
config INT NOT NULL references rendiciones_config,
fecha DATE NOT NULL,
detalle STRING,
detalle_manual STRING NOT NULL DEFAULT '',
valores JSONB,
caja_desde INT,
caja_desde_nombre STRING,
caja_hacia INT,
caja_hacia_nombre STRING,

monto INT, 

usuario STRING,
created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);

`}
}
