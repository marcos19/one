package ddl

func ResumenesBancarios() Tabla {

	return Tabla{
		Nombre:    "resumenes_bancarios",
		DependeDe: "",
		SQL: `
CREATE TABLE resumenes_bancarios(

id UUID PRIMARY KEY,
comitente INT NOT NULL references comitentes,
empresa INT NOT NULL references empresas,
config INT NOT NULL references resumenes_bancarios_config,
desde DATE NOT NULL,
hasta DATE NOT NULL,
persona UUID NOT NULL,
persona_nombre STRING NOT NULL,
sucursal INT NOT NULL,
sucursal_nombre STRING NOT NULL,
partidas JSONB,
imputaciones JSONB,

usuario STRING,
created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);

`}
}
