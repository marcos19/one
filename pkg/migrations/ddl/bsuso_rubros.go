package ddl

func BsUsoRubros() Tabla {

	return Tabla{
		Nombre:    "bsuso_rubros",
		DependeDe: "",
		SQL: `
			CREATE TABLE bsuso_rubros (
				id              SERIAL PRIMARY KEY,
				comitente		INT NOT NULL REFERENCES comitentes,
				nombre          TEXT,
				cta_patrimonial INT,
				cta_amort_acum  INT,
				cta_resultado   INT,
				 
				created_at      TIMESTAMP WITH TIME ZONE, 
				updated_at      TIMESTAMP WITH TIME ZONE
			);`,
	}
}
