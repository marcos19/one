// Package migrations da acceso a base de datos y permite generar una base de datos desde cero.
//
// # Conexiones a DB
//
// Cada paquete de testing llama a migrations para pedir una conexión. El mecanismo de
// determinación de parámetros es el siguiente:
//
// 1) Si se ingresan datos de conexión => Uso esos.
//
// 2) Si se ingresan datos vaciós 	=> Uso los de las variables os.Env()
//
// 3) Sino uso los valores puestos como constantes en este package.
package migrations

import (
	"database/sql"
	"fmt"
	"log"
	"sync"

	"bitbucket.org/marcos19/one/pkg/migrations/ddl"
	"github.com/cockroachdb/errors"
)

// CreateDBs crea varias bases de datos en forma concurrente.
func CreateDBs(host, pass string, dbNames, users []string, port int, ssl bool, conDatos bool) error {

	if len(dbNames) != len(users) {
		return errors.New("Debe haber la misma cantidad de usuarios que de base de datos")
	}

	wg := &sync.WaitGroup{}
	wg.Add(len(dbNames))
	// errChan := make(chan error)
	for i := range dbNames {
		// dbName := dbNames[i]
		// user := users[i]
		// go func(db, u string) {
		err := migrarDB(host, dbNames[i], users[i], pass, port, ssl, conDatos)
		if err != nil {
			return err
		}
		// wg.Done()
		// }(dbName, user)
	}

	// for e := range errChan {
	// 	return e
	// }

	// wg.Wait()
	return nil
}

// migrate lee todos los archivos de texto y ejecuta los statements
func migrarDB(host, dbName, user, pass string, port int, ssl bool, conDatos bool) (err error) {

	// Conecto base de datos
	var mode string
	if ssl {
		mode = "enable"
	} else {
		mode = "disable"
	}
	if user == "" {
		user = "root"
	}

	cn := fmt.Sprintf("postgresql://%v:%v@%v:%v/%v?sslmode=%v", user, pass, host, port, dbName, mode)
	log.Println("Iniciando migrate en:", cn)
	db, err := sql.Open("postgres", cn)
	if err != nil {
		return err
	}

	log.Println("Borrando base de datos:", dbName, "...")
	_, err = db.Exec("DROP DATABASE " + dbName + ";")
	if err != nil {
		log.Println("Base de datos", dbName, "no se pudo borrar:", err)
	} else {
		log.Println("Base de datos", dbName, "borrada")
	}

	// Uso el usuario root, no hace falta
	// _, err = db.Exec("CREATE USER IF NOT EXISTS " + user + ";")
	// if err != nil {
	// 	return errors.Wrapf(err, "al crear usuario '"+user+"'")
	// }

	log.Println("Creando base base de datos", dbName)
	_, err = db.Exec("CREATE DATABASE " + dbName + ";" + "USE " + dbName + ";")
	if err != nil {
		return errors.Wrapf(err, "al crear base de datos. SQL: %v", "CREATE DATABASE "+dbName+";"+"USE "+dbName+";")
	}

	// Uso el usuario root, no hace falta
	// _, err = db.Exec("GRANT ALL ON DATABASE " + dbName + " TO " + user + ";")
	// if err != nil {
	// 	return errors.Wrapf(err, "al otorgar permisos al usuario")
	// }
	log.Println("Base de datos creada. Insertando tablas...")

	// Hago el listado de tablas a insertar
	tablas := []ddl.Tabla{
		ddl.Comitentes(),
		ddl.Empresas(),
		ddl.EmpresasKeys(),
		ddl.Comps(),
		ddl.AfipComps(),
		ddl.AfipAlicuotas(),
		ddl.AfipIdentificaciones(),
		ddl.AfipCertificados(),
		ddl.BcraBancos(),
		ddl.Usuarios(),
		ddl.InflacionIndices(),
		ddl.UsuariosConfirmaciones(),
		ddl.BsUsoRubros(),
		ddl.Condiciones(),
		ddl.FactConfig(),
		ddl.FactEsquemas(),
		ddl.Fact(),
		ddl.RendicionConfig(),
		ddl.Rendicion(),
		ddl.MinutasConfig(),
		ddl.MinutasEsquemas(),
		ddl.Minutas(),
		ddl.ResumenesBancariosConfig(),
		ddl.ResumenesBancarios(),
		ddl.PermisosCrud(),
		ddl.Ums(),
		ddl.PermisosLookup(),
		ddl.Provincias(),
		ddl.Ciudades(),
		ddl.Cajas(),
		ddl.Centros(),
		ddl.Conceptos(),
		ddl.Contratos(),
		ddl.Cuentas(),
		ddl.Depositos(),
		ddl.Grupos(),
		ddl.Monedas(),
		ddl.Personas(),
		ddl.Sucursales(),
		ddl.CuentasAsociadas(),
		ddl.Preciosprov(),
		ddl.UnicidadesDefiniciones(),
		ddl.Unicidades(),
		ddl.Productos(),
		ddl.ProductosCategorias(),
		ddl.Domicilios(),
		ddl.Skus(),
		ddl.ListasDePrecios(),
		ddl.Precios(),
		ddl.Ops(),
		ddl.SkuImg(),
		ddl.Partidas(),
		ddl.Saldos(),
	}

	// Ejecuto las sentencias SQL
	for _, tabla := range tablas {
		log.Println("creando tabla", tabla.Nombre)
		_, err = db.Exec(tabla.SQL)
		if err != nil {
			return errors.Wrapf(err, "insertando tabla %v", tabla.Nombre)
		}
	}

	return nil
}
