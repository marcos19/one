package comitentes

import (
	"context"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/jackc/pgx/v4"
)

func CreateComitente(ctx context.Context, tx pgx.Tx, req *Comitente) (err error) {
	query := `INSERT INTO comitentes (nombre, created_at) VALUES ($1, $2) RETURNING id`

	id := 0
	err = tx.QueryRow(ctx, query, req.Nombre, time.Now()).Scan(&id)
	if err != nil {
		return deferror.DB(err, "inserting")
	}

	req.ID = new(int)
	*req.ID = id
	return
}
