package comitentes

import (
	"context"
	"errors"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Handler struct {
	conn *pgxpool.Pool
}

type ReaderOne interface {
	ReadOne(ctx context.Context, id int) (Comitente, error)
}

func NewHandler(conn *pgxpool.Pool) (h *Handler, err error) {

	if conn == nil {
		return h, errors.New("DB no puede ser nil")
	}
	return &Handler{conn}, nil
}

func (h *Handler) Create(ctx context.Context, req *Comitente) (err error) {
	if req.Nombre == "" {
		return deferror.Validation("No se ingresó nombre de comitente")
	}

	query := `INSERT INTO comitentes (nombre, created_at) VALUES ($1, $2)`
	_, err = h.conn.Exec(ctx, query, req.Nombre, time.Now())
	if err != nil {
		return deferror.DB(err, "insertando")
	}

	return
}

func (h *Handler) ReadOne(ctx context.Context, id int) (out Comitente, err error) {
	if id == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}

	query := `SELECT id, nombre, created_at, updated_at, cuenta_corriente FROM comitentes WHERE id = $1`
	err = h.conn.QueryRow(ctx, query, id).Scan(
		&out.ID, &out.Nombre, &out.CreatedAt, &out.UpdatedAt, &out.CuentaCorriente)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}

	return
}
