package comitentes

import (
	"time"
)

// Comitente se utiliza para separar un cliente de otro, ya que todos
// los clientes usan la misma base de datos.
// Cada usuario pertenece a un comitente.
//
// La facturación se la voy a hacer al comitente.
type Comitente struct {
	ID     *int
	Nombre string

	// Es el grupo de cuentas que toma por defecto en resumen cuenta personas.
	CuentaCorriente *int `json:",string"`

	CreatedAt *time.Time
	UpdatedAt *time.Time
}
