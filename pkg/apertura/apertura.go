package apertura

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/minuta"
	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

type Partida struct {
	Cuenta int
	Centro *int
	Monto  dec.D2
}

type VistaPreviaReq struct {
	Comitente int
	Usuario   string
	Empresa   int `json:",string"`
	Fecha     fecha.Fecha
}

func traerSaldos(ctx context.Context, db *pgxpool.Pool, req VistaPreviaReq) (out []Partida, err error) {

	if req.Empresa == 0 {
		return out, deferror.Validation("no se ingresó empresa")
	}

	opID := uuid.UUID{}
	{ // Determino el asiento de cierre
		query := `SELECT op_id
			FROM partidas
			WHERE
				comitente=$1 AND
				empresa=$2 AND
				fecha_contable < $3 AND 
				tipo_asiento = $4
			ORDER BY fecha_contable DESC
			LIMIT 1;
	`
		err = db.QueryRow(ctx, query, req.Comitente, req.Empresa, req.Fecha, ops.Cierre).Scan(&opID)
		if err != nil {
			return out, errors.Wrap(err, "querying")
		}
	}

	// Traigo sus partidas
	query := `SELECT cuenta, centro, monto FROM partidas where op_id = $1 AND comitente = $2`
	rows, err := db.Query(ctx, query, opID, req.Comitente)
	if err != nil {
		return out, errors.Wrap(err, "querying")
	}
	defer rows.Close()
	for rows.Next() {
		v := Partida{}
		monto := 0
		err = rows.Scan(&v.Cuenta, &v.Centro, &monto)
		if err != nil {
			return out, errors.Wrap(err, "scanning")
		}
		v.Monto = -dec.D2(monto)
		out = append(out, v)
	}

	if out == nil {
		out = []Partida{}
	}
	return
}

type ConfirmarReq struct {
	VistaPreviaReq
	Config  int `json:",string"`
	Esquema int `json:",string"`
}

func (h *Handler) crearMinuta(ctx context.Context, req ConfirmarReq) (out minuta.Minuta, err error) {
	// Valido
	if req.Config == 0 {
		return out, deferror.Validation("no se ingresó config ID")
	}
	if req.Esquema == 0 {
		return out, deferror.Validation("no se ingresó esquema ID")
	}

	ss, err := traerSaldos(ctx, h.db, req.VistaPreviaReq)
	if err != nil {
		return out, errors.Wrap(err, "determinado saldos")
	}

	out = minuta.Minuta{
		Config:    req.Config,
		Esquema:   req.Esquema,
		Fecha:     req.Fecha,
		Comitente: req.Comitente,
		Empresa:   req.Empresa,
		Detalle:   "Asiento de apertura",
	}
	out.Usuario = new(string)
	*out.Usuario = req.Usuario

	sum := dec.D2(0)
	for _, v := range ss {
		p := minuta.Partida{
			Cuenta: v.Cuenta,
		}
		if v.Monto > 0 {
			p.Debe = v.Monto
		} else {
			p.Haber = -v.Monto
		}
		if v.Centro != nil {
			p.Centro = new(int)
			*p.Centro = *v.Centro
		}
		sum += v.Monto
		out.Partidas = append(out.Partidas, p)
	}
	return
}
