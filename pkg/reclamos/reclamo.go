package reclamos

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Reclamo struct {
	ID        uuid.UUID
	Comitente int
	TS        time.Time
	Persona   uuid.UUID
	Sucursal  *int `json:",string"`
	Tipo      int  `json:",string"`
	Usuario   string
	Detalle   string
}

func Create(ctx context.Context, conn *pgxpool.Pool, r *Reclamo) (err error) {
	if r.Comitente == 0 {
		return deferror.ComitenteIndefinido()
	}
	if r.Persona == uuid.Nil {
		return deferror.Validation("persona indefinida")
	}
	if r.Tipo == 0 {
		return deferror.Validation("tipo de reclamo indefinido")
	}
	if r.Usuario == "" {
		return deferror.Validation("usuario indefinido")
	}
	if r.Detalle == "" {
		return deferror.Validation("no se ingresó ningún detalle")
	}

	const q = `INSERT INTO reclamos
(comitente, ts, persona, sucursal, tipo, usuario, detalle) VALUES
($1,$2,$3,$4,$5,$6,$7)
`
	r.TS = time.Now()
	_, err = conn.Exec(ctx, q, r.Comitente, r.TS, r.Persona, r.Sucursal, r.Tipo, r.Usuario, r.Detalle)
	if err != nil {
		return fmt.Errorf("insertando reclamo: %v", err)
	}

	return
}

type ReadManyReq struct {
	Comitente int
	Persona   uuid.UUID
}
type ReadManyResp struct {
	Reclamo
	Dias       int
	TipoNombre string
}

func ReadMany(ctx context.Context, conn *pgxpool.Pool, req ReadManyReq) (out []ReadManyResp, err error) {
	if req.Comitente == 0 {
		return out, deferror.ComitenteIndefinido()
	}

	ww := []string{"reclamos.comitente = $1"}
	pp := []any{req.Comitente}

	if req.Persona != uuid.Nil {
		pp = append(pp, req.Persona)
		ww = append(ww, fmt.Sprintf("persona = $%v", len(pp)))
	}
	where := ""
	if len(ww) > 0 {
		where = "WHERE " + strings.Join(ww, " AND ")
	}

	q := fmt.Sprintf(`SELECT reclamos.id, reclamos.comitente, ts, persona, sucursal, 
	tipo, reclamos_tipos.nombre, usuario, reclamos.detalle 
FROM reclamos
INNER JOIN reclamos_tipos ON reclamos_tipos.id = reclamos.tipo
%v
ORDER BY ts
`, where)
	rows, err := conn.Query(ctx, q, pp...)
	if err != nil {
		return out, fmt.Errorf("querying: %w", err)
	}

	for rows.Next() {
		r := ReadManyResp{}
		err = rows.Scan(&r.ID, &r.Comitente, &r.TS, &r.Persona, &r.Sucursal,
			&r.Tipo, &r.TipoNombre, &r.Usuario, &r.Detalle)
		if err != nil {
			return out, fmt.Errorf("scanning: %w", err)
		}
		r.Dias = fecha.NewFechaFromTime(time.Now()).Menos(fecha.NewFechaFromTime(r.TS))
		out = append(out, r)
	}

	return
}

/*
CREATE TABLE reclamos_tipos (
	id SERIAL PRIMARY KEY,
	comitente INT NOT NULL REFERENCES comitentes,
	nombre STRING
);
CREATE TABLE reclamos (
	id UUID NOT NULL DEFAULT gen_random_uuid(),
	comitente INT NOT NULL REFERENCES comitentes,
	ts TIMESTAMPTZ,
	usuario STRING,
	persona UUID,
	sucursal INT,
	tipo INT REFERENCES reclamos_tipos,
	detalle STRING
);

*/
