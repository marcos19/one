package tipos

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Tipo struct {
	ID     int `json:",string"`
	Nombre string
}

type Handler struct {
	conn *pgxpool.Pool
}

func NewHandler(db *pgxpool.Pool) (out *Handler, err error) {
	if db == nil {
		return out, fmt.Errorf("conn was nil")
	}
	out = &Handler{db}
	return
}

func (h *Handler) ReadMany(ctx context.Context, comitente int) (out []Tipo, err error) {
	out, err = ReadMany(ctx, h.conn, comitente)
	if err != nil {
		return out, fmt.Errorf("buscando tipos de reclamos: %w", err)
	}
	return
}

func ReadMany(ctx context.Context, conn *pgxpool.Pool, comitente int) (out []Tipo, err error) {
	if comitente == 0 {
		return out, deferror.ComitenteIndefinido()
	}

	ww := []string{"comitente = $1"}
	pp := []any{comitente}
	where := ""
	if len(ww) > 0 {
		where = "WHERE " + strings.Join(ww, " AND ")
	}

	q := fmt.Sprintf(`SELECT id, nombre
FROM reclamos_tipos
%v
ORDER BY nombre DESC
`, where)
	rows, err := conn.Query(ctx, q, pp...)
	if err != nil {
		return out, fmt.Errorf("querying: %w", err)
	}

	for rows.Next() {
		r := Tipo{}
		err = rows.Scan(&r.ID, &r.Nombre)
		if err != nil {
			return out, fmt.Errorf("scanning: %w", err)
		}
		out = append(out, r)
	}

	return
}
