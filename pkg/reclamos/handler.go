package reclamos

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
)

type Handler struct {
	conn *pgxpool.Pool
}

func NewHandler(db *pgxpool.Pool) (out *Handler, err error) {
	if db == nil {
		return out, fmt.Errorf("conn was nil")
	}
	out = &Handler{db}
	return
}

func (h *Handler) ReadMany(ctx context.Context, req ReadManyReq) (out []ReadManyResp, err error) {
	out, err = ReadMany(ctx, h.conn, req)
	if err != nil {
		return out, fmt.Errorf("buscando reclamos: %w", err)
	}
	return
}

func (h *Handler) Create(ctx context.Context, req *Reclamo) (err error) {
	err = Create(ctx, h.conn, req)
	if err != nil {
		return fmt.Errorf("creando reclamo: %w", err)
	}
	return
}
