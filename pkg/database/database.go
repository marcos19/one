package database

import (
	"fmt"
	"os"
	"strconv"
)

type ConnectionString struct {
	Host     string
	User     string
	Pass     string
	Port     int
	DB       string
	SSL      bool
	CertRoot string // ca.crt
	Cert     string // client.root.crt
	Key      string // client.root.key
}

func (cn ConnectionString) String() string {

	ssl := ""
	if cn.SSL {
		ssl = "verify-full"
	} else {
		ssl = "disable"
	}
	cnStr := fmt.Sprintf("postgresql://%v:%v@%v:%v/%v?sslmode=%v",
		cn.User, cn.Pass, cn.Host, cn.Port, cn.DB, ssl)
	if cn.SSL {
		cnStr += fmt.Sprintf("&sslrootcert=%v&sslcert=%v&sslkey=%v", cn.CertRoot, cn.Cert, cn.Key)
	}
	return cnStr
}

// Devuelve el string para conectarse a la base de datos de prueba
func TestDBConnectionString() string {

	// Default values
	cn := ConnectionString{
		Host: "localhost",
		Port: 26257,
		User: "root",
	}

	// Port
	port := os.Getenv("COCKROACH_PORT")
	if port != "" {
		portInt, _ := strconv.Atoi(port)
		if portInt != 0 {
			cn.Port = portInt
		}
	}

	// Host
	host, ok := os.LookupEnv("COCKROACH_HOST")
	if ok {
		cn.Host = host
	}

	// User
	user, ok := os.LookupEnv("COCKROACH_USER")
	if ok {
		cn.User = user
	}

	return cn.String()
}
