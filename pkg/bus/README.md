# Bus

Typed events.

```go

	// pkg users
	type UserCreated struct {
		ID int
		Name string
	}

	// pkg app
	func sayHi(e interface{}) {
		u := e.(UserCreated)
		fmt.Printf("Hi %v")
	}

	// pkg security
	police := NewPolice()
	func advertise(e interface{}) {
		u := e.(UserCreated)
		ok := police.CheckAntecedents(u.ID)
		if ok {
			fmt.Println("It's okey")
		} else {
			fmt.Println("Fuckoff")
		}
	}

	bus := NewBus()
	bus.On(UserCreated{}, sayHi)
	bus.On(UserCreated{}, checkAntecedents)

	bus.Emit(UserCreated{ID: 234324, "Marcos"})
	bus.Emit(UserCreated{ID: 94854, "Peter"})
```
