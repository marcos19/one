package bus

import (
	"fmt"
	"reflect"
	"strings"
)

type Bus struct {
	funcMap map[string][]func(interface{})
}

func NewBus() *Bus {
	b := &Bus{}
	b.funcMap = map[string][]func(interface{}){}
	return b
}

func (b *Bus) On(e interface{}, f func(interface{})) {

	k := hash(e)

	// Agrego la función a un map
	anterior := b.funcMap[k]
	nuevo := append(anterior, f)
	b.funcMap[k] = nuevo
}

func hash(e interface{}) string {

	// Creo un key en base al tipo de evento
	tipo := reflect.TypeOf(e)
	pkg := tipo.PkgPath()
	tipoStr := strings.Split(tipo.String(), ".")[1]
	key := fmt.Sprintf("%v.%v", pkg, tipoStr)
	return key

}

func (b *Bus) Emit(event interface{}) {
	k := hash(event)
	jobs := b.funcMap[k]

	for _, job := range jobs {
		go job(event)
	}
}
