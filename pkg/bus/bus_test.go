package bus

import (
	"fmt"
	"testing"
	"time"
)

func TestOn(t *testing.T) {

	bus := NewBus()

	type NewUser struct {
		Name string
	}

	type NewPlace struct {
		Where string
	}

	sayNewUser := func(e interface{}) {
		// time.Sleep(2 * time.Second)

		user := e.(NewUser)
		fmt.Printf("Ey! %v\n", user.Name)
	}
	pleaseNewUser := func(e interface{}) {
		// time.Sleep(2 * time.Second)

		user := e.(NewUser)
		fmt.Printf("Pleased to meet you %v\n", user.Name)
	}
	goodByeNewUser := func(e interface{}) {
		// time.Sleep(2 * time.Second)

		user := e.(NewUser)
		fmt.Printf("Goodbye %v\n", user.Name)
	}

	inspectPlace := func(e interface{}) {
		fmt.Println("this is a nice place")
	}

	bus.On(NewUser{}, sayNewUser)
	bus.On(NewUser{}, pleaseNewUser)
	bus.On(NewUser{}, goodByeNewUser)
	bus.On(NewPlace{}, inspectPlace)

	bus.Emit(NewUser{"Marcos"})
	bus.Emit(NewUser{"Pedro"})
	bus.Emit(NewPlace{"Los Molles"})
	time.Sleep(5 * time.Second)
}
