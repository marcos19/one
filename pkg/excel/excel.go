package excel

import (
	"github.com/cockroachdb/errors"
	"github.com/xuri/excelize/v2"
)

type Estilos struct {
	Dinero int
	Fecha  int
	Header int
}

// Devuelve un Excel con los estilos predefinidos. De esta manera
// todos los informes salen iguales.
func New() (f *excelize.File, e Estilos, err error) {

	f = excelize.NewFile()
	f.SetDefaultFont("Calibri")

	// Formato fecha
	fechaString := "dd/mm/yyyy"
	e.Fecha, err = f.NewStyle(&excelize.Style{CustomNumFmt: &fechaString})
	if err != nil {
		return f, e, errors.Wrap(err, "creando estilo de fecha")
	}

	// Formato dinero
	dineroString := `_(* #,##0.00_);[Red]_(* -#,##0.00;_(* "-"??_);_(@_)`
	e.Dinero, err = f.NewStyle(&excelize.Style{CustomNumFmt: &dineroString})
	if err != nil {
		return f, e, errors.Wrap(err, "creando estilo de dinero")
	}

	// Formato column header
	e.Header, err = f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Bold: true,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			Vertical:   "center",
			WrapText:   true,
		},
		Border: []excelize.Border{
			{Style: 1, Type: "thin", Color: "#000000"},
		},
	})
	return
}
