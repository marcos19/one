package productos

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/productos/categorias"
	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
)

type CreateReq struct {
	Comitente int
	Producto  Producto
	// Variantes Variantes
}

func (h *Handler) Create(ctx context.Context, req *CreateReq) (id uuid.UUID, err error) {

	// Valido
	if req.Producto.Nombre == "" {
		return id, deferror.Validation("Debe ingresar un nombre para el producto")
	}
	if req.Producto.ID == uuid.Nil {
		req.Producto.ID, err = uuid.NewV4()
		if err != nil {
			return id, err
		}
	}
	if req.Producto.Categoria == 0 {
		return id, deferror.Validation("Debe definir una categoría para el producto")

	}
	if req.Producto.AlicuotaIVA == 0 {
		return id, deferror.Validation("Debe definir una alícuota de IVA para el producto")
	}
	if req.Producto.Cuantificable {
		if req.Producto.UM == nil {
			return id, deferror.Validation("Si se marca como Cuantificable debe definir unidad de medida")
		} else {
			if *req.Producto.UM == "" {
				return id, deferror.Validation("Si se marca como Cuantificable debe definir unidad de medida")
			}
		}
	}

	req.Producto.Comitente = req.Comitente

	// Inicio transacción
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return id, deferror.DB(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)

	{ // Códificacion
		msk, ultimo, catID, err := h.determinarMascara(ctx, req.Comitente, req.Producto.Categoria)
		if err != nil {
			return id, errors.Wrap(err, "codificando producto")
		}
		req.Producto.CodigoAlfanumerico, err = categorias.LlenarMascara(msk, ultimo+1)
		if err != nil {
			return id, errors.Wrap(err, "llenando máscara")
		}

		// Aumento numerador categoría
		err = h.categorias.AumentarNumeracion(ctx, tx, categorias.ReadOneReq{Comitente: req.Comitente, ID: catID})
		if err != nil {
			return id, err
		}
	}

	// Creo
	err = CreateProducto(ctx, tx, req.Producto)
	if err != nil {
		return id, errors.Wrap(err, "creando producto")
	}

	// Confirmo transacción
	err = tx.Commit(ctx)
	if err != nil {
		return id, deferror.DB(err, "confirmando transacción")
	}

	return req.Producto.ID, nil
}

func (h *Handler) determinarMascara(ctx context.Context, comitente int, categoriaID int) (
	msk string,
	ultimo int,
	catID int,
	err error,
) {

	cat, err := h.categorias.ReadOne(ctx, categorias.ReadOneReq{
		Comitente: comitente,
		ID:        categoriaID,
	})
	if err != nil {
		return msk, ultimo, catID, errors.Wrap(err, "buscando categoría de producto")
	}

	// Había máscara?
	if cat.Mascara != "" {
		return cat.Mascara, cat.Ultimo, *cat.ID, nil
	}

	if cat.PadreID == nil {
		return msk, ultimo, catID, errors.Errorf("la categoría %v no tiene definida máscara de codificación", cat.Nombre)
	}

	// Busco en categorías superiores
	catID = *cat.ID
	for {
		sup, err := h.categorias.CategoriaSuperior(ctx, categorias.ReadOneReq{
			Comitente: comitente,
			ID:        catID,
		})
		if err != nil {
			return msk, ultimo, catID, errors.Wrap(err, "buscando categoría superior")
		}
		if sup == nil {
			return msk, ultimo, catID, errors.Errorf("la categoría %v no tiene definida máscara de codificación, ni niguna de su superiores")
		}
		if sup.Mascara != "" {
			return sup.Mascara, sup.Ultimo, catID, nil
		}
		catID = *sup.ID
	}
}

type ReadOneReq struct {
	ID        uuid.UUID
	SKU       uuid.UUID // Pido por SKU y me devuelve el producto completo
	Comitente int
}

// HandleBuscarPorID devuelve el producto solicitado.
func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (out Producto, err error) {

	if req.ID == uuid.Nil && req.SKU == uuid.Nil {
		return out, errors.Wrap(err, "no se ingresó ID de producto ni de SKU")
	}

	if req.SKU != uuid.Nil {
		err = h.conn.QueryRow(ctx, "SELECT producto FROM skus WHERE id=$1", req.SKU).Scan(&req.ID)
		if err != nil {
			return out, deferror.DB(err, "buscando ID de producto")
		}
	}

	// Busco en cache
	val, estaba := h.cache.Get(hashUUID("Producto", req.Comitente, req.ID))
	if estaba {
		out, ok := val.(Producto)
		if ok && out.Comitente == req.Comitente {
			return out, nil
		}
	}

	// Busco en DB
	out, err = ReadOneProducto(ctx, h.conn, req)
	if err != nil {
		return out, errors.Wrap(err, "buscando producto")
	}

	// Lo guardo en cache
	h.cache.Set(hashUUID("Producto", req.Comitente, req.ID), out, 0)
	return
}

// HandleBuscar devuelve todos los productos que hay cargados.
func (h *Handler) ReadMany(ctx context.Context, req ReadManyReq) (out []ProductoView, err error) {

	out, err = ReadManyProducto(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando productos")
	}

	return
}

// HandleModificar
func (h *Handler) Update(ctx context.Context, req Producto) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se definió ID de comitente")
	}
	if req.Nombre == "" {
		return deferror.Validation("Debe definir un nombre para el producto")
	}
	if req.Categoria == 0 {
		return deferror.Validation("Debe definir una categoría para el producto")
	}
	if req.AlicuotaIVA == 0 {
		return deferror.Validation("Debe definir una alícuota de IVA para el producto")
	}
	if req.Cuantificable {
		if req.UM == nil {
			return deferror.Validation("Si se marca como Cuantificable debe definir unidad de medida")
		} else {
			if *req.UM == "" {
				return deferror.Validation("Si se marca como Cuantificable debe definir unidad de medida")
			}
		}
	}

	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return deferror.DB(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)

	// Busco que exista
	anterior, err := ReadOneProducto(ctx, h.conn, ReadOneReq{Comitente: req.Comitente, ID: req.ID})
	if err != nil {
		return deferror.DB(err, "buscando producto anterior")
	}
	// No permito modificar el código alfanumérico si ya está usado
	if anterior.CodigoAlfanumerico != req.CodigoAlfanumerico {
		// Significa que cambió el codigo. Corroboro que no esté usado en la contabilidad
		// TODO
		return deferror.Validation("No se puede modificar el código alfanumérico")
	}

	// Modifico producto
	err = UpdateProducto(ctx, tx, req)
	if err != nil {
		return deferror.DB(err, "persistiendo modificaciones de producto")
	}

	// Busco los SKU de este producto
	ss, err := ReadManySKU(ctx, h.conn, ReadManySKUReq{Comitente: req.Comitente, ID: req.ID})
	if err != nil {
		return errors.Wrap(err, "leyendo SKUs")
	}

	// Pego las modificaciones heredadas del producto
	for i := range ss {
		ss[i].Nombre = req.Nombre
		// Le agrego los atributos al nombre de la variante
		if req.TieneVariantes {
			for _, v := range req.Apertura {
				attStr, ok := ss[i].Atts[v].(string)
				if !ok {
					return errors.Errorf("no se pudo convertir a string el atributo de la variante. Era: %v, dentro de: %v", v, req.Apertura)
				}
				ss[i].Nombre += " " + attStr
			}
		} else {
			ss[i].Codigo = req.CodigoAlfanumerico
		}
		ss[i].NombreImpresion = req.NombreImpresion
		ss[i].Categoria = req.Categoria
		ss[i].Inventariable = req.Inventariable
		ss[i].Cuantificable = req.Cuantificable
		ss[i].TipoUM = req.TipoUM
		ss[i].UM = req.UM
		ss[i].Envase = req.Envase
		ss[i].Comitente = req.Comitente
		ss[i].Activo = req.Activo

		err = UpdateSKU(ctx, tx, &ss[i])
		if err != nil {
			return deferror.DB(err, "modificando SKU")
		}
	}

	// Confirmo transacción
	err = tx.Commit(ctx)
	if err != nil {
		return deferror.DB(err, "confirmando transacción")
	}

	h.cache.Del(hashUUID("Producto", req.Comitente, req.ID))
	return
}

type DeleteReq struct {
	ID        uuid.UUID
	Comitente int
}

// HandleEliminar elimina el producto
func (h *Handler) Delete(ctx context.Context, req DeleteReq) (err error) {

	// Valido
	if req.ID == uuid.Nil {
		return deferror.Validation("no se ingresó el ID")
	}

	// Inicio transacción
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return deferror.DB(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)

	// Está usado en la contabilidad?
	count := 0
	err = tx.QueryRow(ctx, "SELECT COUNT(producto) FROM partidas WHERE comitente=$1 AND producto=$2", req.Comitente, req.ID).
		Scan(&count)
	if err != nil {
		return deferror.DB(err, "determinando si el producto se encontraba usado")
	}
	if count > 0 {
		return deferror.Validationf("No se puede borrar porque el producto ya ha sido usado %v veces", count)
	}

	// Borro precios
	_, err = tx.Exec(ctx, "DELETE FROM precios WHERE comitente=$1 AND sku IN (SELECT id FROM skus WHERE producto=$2)", req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "borrando tabla precios")
	}

	// Borro los SKU
	_, err = tx.Exec(ctx, "DELETE FROM skus WHERE comitente=$1 AND producto=$2", req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "borrando tabla skus")
	}

	// Borro producto
	_, err = tx.Exec(ctx, "DELETE FROM productos WHERE comitente=$1 AND id=$2", req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "borrando tabla producto")
	}

	h.cache.Del(hashUUID("Producto", req.Comitente, req.ID))

	// Confirmo transacción
	err = tx.Commit(ctx)
	if err != nil {
		return deferror.DB(err, "confirmando transacción")
	}

	return
}

func arrayUUID(ids []uuid.UUID) string {
	idsStr := []string{}
	for _, v := range ids {
		idsStr = append(idsStr, fmt.Sprintf("'%v'", v.String()))
	}
	return strings.Join(idsStr, ", ")
}
