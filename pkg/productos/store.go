package productos

import (
	"fmt"

	"github.com/gofrs/uuid"
)

func (h *Handler) DelSKU(comitente int, id uuid.UUID) {
	h.cache.Del(hashUUID("SKU", comitente, id))
}

func (h *Handler) DelProducto(comitente int, id uuid.UUID) {
	h.cache.Del(hashUUID("Producto", comitente, id))
}

func hashUUID(tipo string, comitente int, id uuid.UUID) string {
	return fmt.Sprintf("%v/%v/%x", tipo, comitente, id)
}
