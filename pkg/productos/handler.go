package productos

import (
	"context"
	"encoding/json"
	"net/http"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/productos/categorias"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/um"
	"github.com/dgraph-io/ristretto"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler es la estructura que recibe los request y emite los responses
type Handler struct {
	conn                   *pgxpool.Pool
	cache                  *ristretto.Cache
	TamañoMaximoDeImagenes int
	categorias             *categorias.Handler
}

// NewHandler instancia el handler de productos.
func NewHandler(c HandlerConfig) (h *Handler, err error) {
	if c.Conn == nil {
		return h, errors.New("Conn no puede ser nil")
	}
	if c.Cache == nil {
		return h, errors.New("Cache no puede ser nil")
	}
	if c.CategoriasHandler == nil {
		return h, errors.New("CategoriasHandler no puede ser nil")
	}
	h = &Handler{
		conn:                   c.Conn,
		cache:                  c.Cache,
		TamañoMaximoDeImagenes: c.TamañoMaximoDeImagenes,
		categorias:             c.CategoriasHandler,
	}

	return
}

type Getter interface {
	ReadOne(ctx context.Context, req ReadOneReq) (out Producto, err error)
}

// HandlerOptions contiene los datos que usa el handler de imagenes.
type HandlerConfig struct {
	Conn                   *pgxpool.Pool
	Cache                  *ristretto.Cache
	TamañoMaximoDeImagenes int
	CategoriasHandler      *categorias.Handler
}

func CrearDummySKU(p Producto) SKU {

	// Si el producto no tiene variantes, le creo un sku dummy
	v := SKU{
		Producto:        p.ID,
		Codigo:          p.CodigoAlfanumerico,
		Categoria:       p.Categoria,
		Cuantificable:   p.Cuantificable,
		Comitente:       p.Comitente,
		Envase:          p.Envase,
		Inventariable:   p.Inventariable,
		Nombre:          p.Nombre,
		NombreImpresion: p.NombreImpresion,
		TipoUM:          p.TipoUM,
		UM:              p.UM,
	}
	v.ID, _ = uuid.NewV4()
	return v
}

// HandleUnidadesDeMedida cambia los datos de una categoría de producto.
func (h *Handler) HandleUnidadesDeMedida() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		uu := um.Medidas()
		out := []um.UM{}
		for _, v := range uu {
			out = append(out, v)
		}
		err := json.NewEncoder(w).Encode(&out)
		if err != nil {
			deferror.Frontend(errors.Wrap(err, "codificando JSON"), w, r)
			return
		}

	}
}
