// Package parser sirve para levanter un excel con los productos y persistirlo
// en la base de datos.
package parser

// import (
// 	"encoding/json"
// 	"fmt"
// 	"log"
// 	"strconv"
// 	"strings"

// 	"bitbucket.org/marcos19/one/pkg/productos"
// 	"bitbucket.org/marcos19/one/pkg/um"

// 	"github.com/cockroachdb/errors"
// 	"github.com/jinzhu/gorm"
// 	"github.com/xuri/excelize"
// )

// var unidades map[string]um.UM

// func ParseXLSX(filePath string, db *gorm.DB) (prods []productos.Producto, err error) {
// 	xlsx, err := excelize.OpenFile(filePath)
// 	if err != nil {
// 		return prods, errors.Wrap(err, "Intentando leer "+filePath)
// 	}

// 	hoja := xlsx.GetSheetName(1)
// 	log.Printf("Leyendo hoja %v\n", hoja)

// 	rows := xlsx.GetRows(hoja)
// 	log.Printf("Cantidad de renglones: %v\n", len(rows))
// 	log.Printf("Rows: %v\n", rows)
// 	for i, row := range rows {
// 		if i == 0 {
// 			continue
// 		}

// 		prod, err := parseRow(row, db)
// 		if err != nil {
// 			return prods, errors.Wrap(err, fmt.Sprintf("Leyendo la fila número %v", i+1))
// 		}

// 		prods = append(prods, prod)
// 	}

// 	return
// }

// var cantidadCampos = 18

// func printArray(arr []string) (out string) {

// 	for i, v := range arr {
// 		out += fmt.Sprintf("\n%v - %v", i, v)
// 	}
// 	return
// }
// func parseRow(row []string, db *gorm.DB) (p productos.Producto, err error) {
// 	if len(row) != cantidadCampos {
// 		for i := cantidadCampos; i < len(row); i++ {
// 			if row[i] != "" {
// 				return p, errors.Errorf("La fila debería tener %v campos. Tiene %v. Items: %v",
// 					cantidadCampos, len(row), printArray(row))
// 			}
// 		}
// 	}

// 	if row[0] != "" {
// 		id, err := strconv.Atoi(row[0])
// 		if err != nil {
// 			return p, errors.Errorf("El código de cuenta debe ser un número")
// 		}
// 		p.ID = id
// 	}

// 	p.Nombre = row[1]
// 	p.NombreImpresion = row[2]

// 	cat, err := strconv.Atoi(row[3])
// 	if err != nil {
// 		return p, errors.Errorf("La categoría debe ser un número válido")
// 	}
// 	p.Categoria = cat

// 	p.Descripcion = row[4]
// 	p.Inventariable, err = parseBool(row[5])
// 	if err != nil {
// 		return p, errors.Errorf("Leyendo columna Inventariable")
// 	}

// 	p.Cuantificable, err = parseBool(row[6])
// 	if err != nil {
// 		return p, errors.Errorf("Leyendo columna Cuantificable")
// 	}

// 	// Tipo de medida
// 	switch row[7] {
// 	case "Peso", "Cantidad", "Distancia", "Volumen", "Superficie":
// 		p.TipoUM = row[7]
// 	case "":
// 		return p, errors.Errorf("El tipo de medida no puede estar vacío")
// 	default:
// 		return p, errors.Errorf("El tipo de medida '%v' no está permitido", row[7])
// 	}

// 	// Unidad de medida
// 	switch row[8] {
// 	case "":
// 		if p.Cuantificable == true {
// 			return p, errors.Errorf("El producto %v está definido como cuantificable pero no se le definió unidad de medida", p.Nombre)
// 		}
// 	default:
// 		if (p.Cuantificable == false) && (row[8] != "") {
// 			return p, errors.Errorf("El producto %v está definido como no cuantificable pero se le definió unidad de medida", p.Nombre)
// 		}
// 		ums := um.Medidas()
// 		if err != nil {
// 			return p, err
// 		}
// 		_, ok := ums[row[8]]
// 		if !ok {
// 			return p, errors.Errorf("La unidad de medida %v no existe en la base de datos.", row[8])
// 		}
// 		p.UM = row[8]
// 	}

// 	p.Envase = row[9]
// 	p.Activo, err = parseBool(row[13])
// 	if err != nil {
// 		return p, errors.Wrap(err, "En la columna Activo")
// 	}

// 	if row[11] != "" {
// 		rel := productos.RelacionesUM{}
// 		err = json.NewDecoder(strings.NewReader(row[11])).Decode(&rel)
// 		if err != nil {
// 			return p, errors.Wrap(err, "Cuando se decodificaba JSON de RelacionesUM")
// 		}
// 		p.RelacionesUM = rel
// 		p.RelacionesUMFija = true
// 	}

// 	p.EsEstandar, err = parseBool(row[14])
// 	if err != nil {
// 		return p, errors.Wrap(err, "En la columna 'Es Estandar'")
// 	}

// 	if row[15] != "" {
// 		p.EstandarID, err = strconv.Atoi(row[15])
// 		if err != nil {
// 			return p, errors.Wrap(err, "En la columna 'Producto estandar ID")
// 		}
// 	}

// 	if row[16] != "" {
// 		cruce := productos.CrucesConProveedores{}
// 		err = json.NewDecoder(strings.NewReader(row[16])).Decode(&cruce)
// 		if err != nil {
// 			return p, errors.Wrap(err, "Cuando se decodificaba JSON de Cruce Proveedores")
// 		}
// 		p.CrucesConProveedores = cruce
// 	}
// 	return
// }

// func parseBool(texto string) (valor bool, err error) {
// 	switch texto {
// 	case "true", "1", "si", "SI", "Si":
// 		return true, nil
// 	case "false", "0", "no", "NO", "No":
// 		return false, nil
// 	default:
// 		return false, errors.Errorf("No se pudo parsear %v como un booleano", texto)
// 	}
// }
