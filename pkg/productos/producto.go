// Package productos contiene las definiciones de productos, precios, listas de precios,
// condiciones de pago, promociones.
package productos

import (
	"database/sql/driver"
	"encoding/json"
	"time"

	"bitbucket.org/marcos19/one/pkg/iva"
	"github.com/gofrs/uuid"

	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/um"
)

// Producto es una estructura que guarda los datos de un producto y los
// atributos por los que arma las variantes.
// Tras bamabalinas genera registros en la tabla de SKUs y los mantiene
// sincronizados cada vez que se produzca un cambio en el Producto.
type Producto struct {
	ID                 uuid.UUID
	Comitente          int
	CodigoAlfanumerico string
	Nombre             string
	NombreImpresion    string

	// se usa para definir las cuentas contables
	Categoria int `json:",string"`

	// Un html
	Descripcion string

	// El combo permite vender varios productos juntos. Está pensado
	// Para el caso de dos o más productos que se facturan juntos, pero
	// que no pierden su individualidad (ej revisión técnica y oblea)
	// En la factura van a aparecer separados y cada uno con su precio.
	// Estos combos tienen un precio. Si la sumatoria de sus componentes
	// no da igual que el precio total, hay uno (o varios) productos que
	// ajustarán sus precios. En el caso de las revisiones, la oblea tiene
	// precio fijo. Si se está sacando un producto que se vende mucho con
	// otros dos que no, tiene sentido que el costo del descuento lo asuma
	// el producto que no sale.
	EsUnCombo     bool
	DetallesCombo DetallesCombo

	////////////////////////////////////////////////////
	// Estas opciones pueden ser pisadas por los SKU
	////////////////////////////////////////////////////
	// Si es true hace controles de stock
	Inventariable bool
	// Si es true exige cantidades.
	Cuantificable bool
	TipoUM        string  // Peso
	UM            *string // kg

	DisponibleParaVenta bool

	// Si es true significa que cuando se cargue el precio
	// el mismo será el precio final con impuestos incluidos.
	PrecioFinal bool

	AlicuotaIVA iva.Alicuota `json:",string"`
	// Si es true se deberá cargar una alícuota para venta
	// Está pensado para aquellos productos que cuando son venta a
	// Consumidor final salen a alícuota cero.
	AlicuotaCeroVenta bool

	// Cuando un producto se lleva en cantidades, cada uno va a tener el
	// dato del envase (caja, botella, bolsa, etc), pero contablemente
	// va a ir en la columna de cantidad con la UM = "u"
	Envase string

	// Un bidón viene de 25 litros siempre. Pero una torta puede ser de 800 gr
	// o 2300 gr. Si esta propiedad es false, se le exigirá al usuario al momento
	// de realizar la operación completar la equivalencia.
	RelacionesUMFija bool
	RelacionesUM     RelacionesUM

	// Las facetas son los atributos definidos en la categoría del producto.
	// Son atributos indexados.
	Facetas AttMap

	// Son todos los atributos del producto, que son comunes a todos los SKU
	// que conetiene. Ej: {Fabricante: Apple, Procedencia: China, ...}
	AttsGenerales Atts

	Activo bool

	TieneVariantes bool
	//	["Capacidad", "Color"]
	Apertura Aperturas

	TrabajaConUnicidades bool
	ClaseUnicidad        *int `json:",string"`

	CreatedAt time.Time
	UpdatedAt time.Time

	CrucesConProveedores CrucesConProveedores
}

// TableName devuelve el nombre de la tabla en la base de datos
func (p Producto) TableName() string {
	return "productos"
}

type DetallesCombo []DetalleCombo

type DetalleCombo struct {
	SkuID            uuid.UUID
	MostrarEnFactura bool
	// El combo va a tener un precio que no va a coincidir con el de
	// la suma de cada producto. Cuando se fije el precio de un combo,
	// la venta la voy a contabilizar al precio de cada producto,
	// la diferencia vendrá a este item (puede ser positiva o negativa)
	VariableAjustePrecio bool
}

// Value cumple con la interface SQL
func (p DetallesCombo) Value() (driver.Value, error) {
	return json.Marshal(p)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (p *DetallesCombo) Scan(src interface{}) error {
	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New("Type assertion error .([]byte)")
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, p)
	if err != nil {
		return errors.Wrap(err, "Unmarshalling")
	}

	return nil
}

// CrucesConProveedores dice cómo lo llaman mis proveedores a este producto.
// Sirve para poder cruzar mis productos con las listas de precios de ellos.
type CrucesConProveedores []CruceProveedor

// CruceProveedor identifica que este producto, el proveedor X lo tiene
// con el código A-34235, lo llama "Har xkg 000 EstCont."
//
// Defino acá también la unidad de medida de la lista de precios.
// Por ejemplo, tengo el producto "Almendgras", el cual llevo en gramos.
// Si un proveedor las vende de a 100 g y el otro de a kilo, esa relación
// queda grabada acá.
type CruceProveedor struct {
	ProveedorID     int     `json:"proveedor_id"`
	ProveedorNombre string  `json:"proveedor_nombre"`
	ProductoID      string  `json:"id"`
	Nombre          string  `json:"nombre"`
	UM              string  `json:"um"`
	Cantidad        float64 `json:"cantidad"`
}

// Value cumple con la interface SQL
func (a CrucesConProveedores) Value() (driver.Value, error) {
	return json.Marshal(a)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (a *CrucesConProveedores) Scan(src interface{}) error {
	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New("Type assertion error .([]byte)")
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, a)
	if err != nil {
		return errors.Wrap(err, "Unmarshalling")
	}

	return nil
}

type Aperturas []string

// Value cumple con la interface SQL
func (a Aperturas) Value() (driver.Value, error) {
	return json.Marshal(a)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (a *Aperturas) Scan(src interface{}) error {
	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New("Type assertion error .([]byte)")
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, a)
	if err != nil {
		return errors.Wrap(err, "Unmarshalling")
	}

	return nil
}

type Att struct {
	Key    string
	String *string `json:",omitempty"`
	Bool   *bool   `json:",omitempty"`
	Number *dec.D2 `json:",omitempty"`
}

// Atts es un map de key valor para atributos personalizados
// Esta estructura está pensada para satisfacer la búsqueda en la base de datos.
// Voy a usar otro artefacto para darle la jerarquía que quiero y mostrarlos
// como me convenga.
type Atts []Att

// Value cumple con la interface SQL
func (a Atts) Value() (driver.Value, error) {
	return json.Marshal(a)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (a *Atts) Scan(src interface{}) error {
	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New("Type assertion error .([]byte)")
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, a)
	if err != nil {
		return errors.Wrap(err, "Unmarshalling")
	}

	return nil
}

// RelacionesUM es un map que guarda las distintas relaciones de unidad de medida.
// Por ejemplo, un producto que se maneja en Bidones, se le puede agregar un
// item que se llame: Peso: 25
type RelacionesUM []um.RelacionUM

// Value cumple con la interface SQL
func (a RelacionesUM) Value() (driver.Value, error) {
	return json.Marshal(a)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (a *RelacionesUM) Scan(src interface{}) error {
	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New("Type assertion error .([]byte)")
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, a)
	if err != nil {
		return errors.Wrap(err, "Unmarshalling")
	}

	return nil
}
