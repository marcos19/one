package productos

import (
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
)

// ProductoView es un resumen del catálogo de productos que incluye además
// de los datos del producto, saldo de stocks, precio, promociones, etc.
// Sirve para hacer las queries de manera eficiente y para mostrar los
// resultados de búsqueda sin tener que tocar otra tabla.
//
// Cuando hago una búsqueda, necesito que
// - Me traiga los resultados agrupados por producto (no todas las variantes)
// -
//
// Es acutalizada cuando:
// - Se modifique el nombre o algún atributo indexado.
// - Se modifique stock.
// - Modifique el precio.
type ProductoView struct {
	ID                 uuid.UUID // El ID del SKU
	Nombre             string
	CodigoAlfanumerico string
	Categoria          int `json:",string"`
	Thumb              string

	// Estos son los campos que van a llegar al front-end
	Precio        dec.D4
	CantidadStock dec.D4
	UM            string
}

func (p ProductoView) TableName() string {
	return "productos"
}

// Así lo persisto. Un producto con 20 variantes y 3 listas de precios y 100 depositos
type ProductoRegistro struct {
	ID        uuid.UUID // El ID del producto
	Nombre    string    // El nombre del producto
	Thumb     string    //
	Categoria int

	// Los atributos indexados para este producto (los definidos en la categoría)
	Atts AttMap

	// TODO: Si quiero zapatos talle 42, no quiero ver ningún zapato que no tenga el talle 42.

	// // Dentro de un mismo producto puedo tener variantes con distintos
	// // precios y a su vez, distintas listas de precios para cada variante.
	// // Cuando haga la búsqueda quiero que me diga desde $999 hasta $1432
	// // Este dato se guarda cada vez que se actualiza el precio de un producto.
	// PreciosRangoDesde dec.D2
	// PreciosRangoHasta dec.D2
}

// StockPorDeposito es el listado de stock que hay en cada depósito para
// este producto.
type StockPorDeposito map[int]Stock

// Stock dice la cantidad que hay de un producto en un depósito determinado.
type Stock struct {
	ID       int
	Nombre   int
	Cantidad dec.D2
}

type Precio struct {
	Deposito      int
	ListaDePrecio int
	Variante      int
}
