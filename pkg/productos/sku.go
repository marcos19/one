package productos

import (
	"database/sql/driver"
	"encoding/json"
	"time"

	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
)

// SKU Define un producto que se compra o vende.
//
// Las variantes de productos te permiten (Source: Facebook for business):
// - Ayudar a las personas a encontrar otros colores, estilos, tamaños o
// diseños de un producto en particular.
// - Evitar hacer retargeting a una persona que ya tiene un producto
// similar, porque es poco probable que lo compre. Por ejemplo, si una
// persona vio varios colores del mismo zapato, pero eligió comprar solo
// un par de zapatos negros, es poco probable que compre el modelo marrón
// en el próximo mes.
// - Evitar mostrar varios productos similares uno al lado del otro en tus
// anuncios y experiencias comerciales para que las personas puedan ver
// una variedad más amplia. En los anuncios dinámicos, los anuncios de
// colección, las tiendas de páginas y las compras en Instagram, optamos
// por mostrar solo una variante de cada grupo de productos según su
// relevancia o popularidad. Cuando las personas seleccionan el producto
// para obtener más información, pueden ver las demás variantes.
type SKU struct {
	ID        uuid.UUID
	Producto  uuid.UUID
	Comitente int `json:",string"`

	// El producto al que perteneces
	Codigo          string
	Nombre          string
	NombreImpresion string
	BarCode         string
	Categoria       int `json:",string"`

	// Si es true hace controles de stock
	Inventariable bool

	// Si es true exige cantidades.
	Cuantificable bool
	TipoUM        string  // Peso
	UM            *string // Kg
	Envase        string

	// Un bidón viene de 25 litros siempre. Pero una torta puede ser de 800 gr
	// o 2300 gr. Si esta propiedad es false, se le exigirá al usuario al momento
	// de realizar la operación completar la equivalencia.
	RelacionesUMFija bool
	RelacionesUM     RelacionesUM

	// Cuando seleccione un producto, por ejemplo mermelada Sancor, voy a buscar
	// dentro de todos los SKU que pertenecen a ese producto, todos los keys
	// dispobinbles. Tendría entonces por ejemplo, [Sabor: string, Light: bool,
	// Peso (mg): int]. Entonces el usuario dice quiero filtrar por peso, y ahí
	// busca para cada SKU cuales son los valores que hay para el atributo Peso.
	Atts   AttMap
	Activo bool

	CreatedAt time.Time
	UpdatedAt time.Time

	// EsEstandar         bool
	// ProductoEstandarID int
}

// AttMap son los atributos que se guardan como key vale. No importa el orden,
// son mejores para los queries.
type AttMap map[string]interface{}

// Value cumple con la interface SQL
func (a AttMap) Value() (driver.Value, error) {
	return json.Marshal(a)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (a *AttMap) Scan(src interface{}) error {
	if src == nil {
		return nil
	}
	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.Errorf("Type assertion error. Expected: []byte, got: %T", src)
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, a)
	if err != nil {
		return errors.Wrap(err, "Unmarshalling")
	}

	return nil
}

// Variantes la uso en los request.
type Variantes []SKU

// Value cumple con la interface SQL
func (a Variantes) Value() (driver.Value, error) {
	return json.Marshal(a)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (a *Variantes) Scan(src interface{}) error {
	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New("Type assertion error .([]byte)")
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, a)
	if err != nil {
		return errors.Wrap(err, "Unmarshalling")
	}

	return nil
}
