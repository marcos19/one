package productos

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

func CreateProducto(ctx context.Context, tx pgx.Tx, v Producto) (err error) {

	// Verifico código alfanumérico no exista
	count := 0
	err = tx.QueryRow(ctx, "SELECT COUNT(id) FROM productos WHERE comitente=$1 AND codigo_alfanumerico=$2",
		v.Comitente, v.CodigoAlfanumerico).Scan(&count)
	if err != nil {
		return deferror.DB(err, "verificando codigo_alfanumerico")
	}
	if count > 0 {
		return deferror.Validation("Ya existe un producto con el código " + v.CodigoAlfanumerico)
	}

	query := `INSERT INTO productos (
	id,
	comitente, 
	codigo_alfanumerico,
	nombre,
	nombre_impresion,
	categoria,
	descripcion,
	es_un_combo,
	detalles_combo,
	inventariable,
	cuantificable,
	tipo_um,
	um,
	envase,
	relaciones_um_fija,
	relaciones_um,
	facetas, 
	atts_generales,
	tiene_variantes,
	disponible_para_venta,
	precio_final,
	alicuota_iva,
	alicuota_cero_venta,
	trabaja_con_unicidades,
	clase_unicidad,
	activo,
	apertura,
	created_at,
	updated_at,
	cruces_proveedores
	) VALUES (
		$1, $2, $3, $4, $5, $6, $7, $8, $9, $10,
		$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,
		$21,$22,$23,$24,$25,$26,$27,$28,$29,$30
	)`

	_, err = tx.Exec(ctx, query,
		v.ID,
		v.Comitente,
		v.CodigoAlfanumerico,
		v.Nombre,
		v.NombreImpresion,
		v.Categoria,
		v.Descripcion,
		v.EsUnCombo,
		v.DetallesCombo,
		v.Inventariable,
		v.Cuantificable,
		v.TipoUM,
		v.UM,
		v.Envase,
		v.RelacionesUMFija,
		v.RelacionesUM,
		v.Facetas,
		v.AttsGenerales,
		v.TieneVariantes,
		v.DisponibleParaVenta,
		v.PrecioFinal,
		v.AlicuotaIVA,
		v.AlicuotaCeroVenta,
		v.TrabajaConUnicidades,
		v.ClaseUnicidad,
		v.Activo,
		v.Apertura,
		v.CreatedAt,
		v.UpdatedAt,
		v.CrucesConProveedores,
	)
	if err != nil {
		return deferror.DB(err, "inserting")
	}

	// Si no hay variantes, inserto dummy en tabla skus
	if !v.TieneVariantes {
		dummy := CrearDummySKU(v)
		err = CreateSKU(ctx, tx, &dummy)
		if err != nil {
			return deferror.DB(err, "persistiendo dummy SKU")
		}
	}
	return
}

func CreateSKU(ctx context.Context, tx pgx.Tx, v *SKU) (err error) {

	query := `INSERT INTO skus (
		id,
		producto,
		comitente,
		codigo,
		nombre, 
		nombre_impresion,
		categoria,
		bar_code,
		inventariable,
		cuantificable,
		tipo_um,
		um,
		envase,
		relaciones_um_fija,
		relaciones_um,
		atts,
		activo,
		created_at,
		updated_at,
		atts_especificos
	) VALUES (
		$1, $2, $3, $4, $5, $6, $7, $8, $9, $10,
		$11,$12,$13,$14,$15,$16,$17,$18,$19,$20
	);`

	_, err = tx.Exec(ctx, query, v.ID, v.Producto, v.Comitente, v.Codigo, v.Nombre, v.NombreImpresion,
		v.Categoria, v.BarCode, v.Inventariable, v.Cuantificable, v.TipoUM, v.UM, v.Envase,
		v.RelacionesUMFija, v.RelacionesUM, v.Atts, v.Activo, v.CreatedAt, v.UpdatedAt, v.Atts,
	)
	if err != nil {
		return deferror.DB(err, "inserting")
	}
	return
}

// TODO: columnas a borrar de SKU: es_estandar, estandar_id, cruces_proveedores

func ReadOneProducto(ctx context.Context, conn *pgxpool.Pool, req ReadOneReq) (v Producto, err error) {

	query := `SELECT
	id,
	comitente, 
	codigo_alfanumerico,
	nombre,
	nombre_impresion,
	categoria,
	descripcion,
	es_un_combo,
	detalles_combo,
	inventariable,
	cuantificable,
	tipo_um,
	um,
	envase,
	relaciones_um_fija,
	relaciones_um,
	facetas, 
	atts_generales,
	tiene_variantes,
	disponible_para_venta,
	precio_final,
	alicuota_iva,
	alicuota_cero_venta,
	trabaja_con_unicidades,
	clase_unicidad,
	activo,
	apertura,
	created_at,
	updated_at,
	cruces_proveedores
FROM productos
WHERE comitente=$1 AND id=$2;	`

	err = conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(
		&v.ID, &v.Comitente, &v.CodigoAlfanumerico, &v.Nombre, &v.NombreImpresion, &v.Categoria, &v.Descripcion, &v.EsUnCombo,
		&v.DetallesCombo, &v.Inventariable, &v.Cuantificable, &v.TipoUM, &v.UM, &v.Envase, &v.RelacionesUMFija, &v.RelacionesUM,
		&v.Facetas, &v.AttsGenerales, &v.TieneVariantes, &v.DisponibleParaVenta, &v.PrecioFinal, &v.AlicuotaIVA,
		&v.AlicuotaCeroVenta, &v.TrabajaConUnicidades, &v.ClaseUnicidad, &v.Activo, &v.Apertura, &v.CreatedAt, &v.UpdatedAt,
		&v.CrucesConProveedores)
	if err != nil {
		return v, deferror.DB(err, "querying/scanning")
	}
	return
}

// TODO: Borrar columnas es_estandar, estandar_id, variantes

type ReadManyReq struct {
	IDs       []uuid.UUID
	Texto     string
	Categoria int `json:",string"`
	// Deposito                 int `json:",string"`
	// ListaDePrecio            int `json:",string"`
	UnicidadDef              int `json:",string"`
	MostrarInactivos         bool
	SoloDisponiblesParaVenta bool
	CantidadPorPagina        int
	Pagina                   int
	Comitente                int
}

func ReadManyProducto(ctx context.Context, conn *pgxpool.Pool, req ReadManyReq) (out []ProductoView, err error) {
	out = []ProductoView{}

	if req.Comitente == 0 {
		return out, errors.New("no se ingresó el ID de comitente")
	}

	ww := []string{"comitente = $1"}
	pp := []interface{}{req.Comitente}

	// Por IDs
	if len(req.IDs) > 0 {
		ww = append(ww, fmt.Sprintf("id IN (%v)", arrayUUID(req.IDs)))
	}

	// Texto
	if req.Texto != "" {
		pp = append(pp, "%"+req.Texto+"%")
		ww = append(ww, fmt.Sprintf("nombre ILIKE $%v", len(pp)))
	}

	if req.Categoria != 0 {
		pp = append(pp, req.Categoria)
		ww = append(ww, fmt.Sprintf("categoria=$%v", len(pp)))
	}
	if req.UnicidadDef != 0 {
		pp = append(pp, req.UnicidadDef)
		ww = append(ww, fmt.Sprintf("clase_unicidad=$%v", len(pp)))
	}
	if !req.MostrarInactivos {
		ww = append(ww, "activo=true")
	}
	if req.SoloDisponiblesParaVenta {
		ww = append(ww, "disponible_para_venta = true")
	}
	where := "WHERE " + strings.Join(ww, " AND ")

	limit := ""
	if req.CantidadPorPagina != 0 {
		limit = fmt.Sprintf(" LIMIT %v", req.CantidadPorPagina)
	}

	offset := ""
	if req.CantidadPorPagina != 0 {
		limit = fmt.Sprintf(" OFFSET %v", req.Pagina)
	}

	query := fmt.Sprintf(`SELECT id, nombre, codigo_alfanumerico, categoria
	FROM productos
	%v ORDER BY nombre%v%v`, where, limit, offset)

	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		v := ProductoView{}
		err = rows.Scan(
			&v.ID, &v.Nombre, &v.CodigoAlfanumerico, &v.Categoria)
		if err != nil {
			return out, deferror.DB(err, "scanning")
		}
		out = append(out, v)
	}
	return
}

func UpdateProducto(ctx context.Context, tx pgx.Tx, v Producto) (err error) {
	query := `
UPDATE productos 
SET 
nombre=$3,
nombre_impresion=$4,
categoria=$5,
descripcion=$6,
es_un_combo=$7,
detalles_combo=$8,
inventariable=$9,
cuantificable=$10,
tipo_um=$11,
um=$12,
envase=$13,
relaciones_um_fija=$14,
relaciones_um=$15,
facetas=$16, 
atts_generales=$17,
tiene_variantes=$18,
disponible_para_venta=$19,
precio_final=$20,
alicuota_iva=$21,
alicuota_cero_venta=$22,
trabaja_con_unicidades=$23,
clase_unicidad=$24,
activo=$25,
apertura=$26,
created_at=$27,
updated_at=$28,
cruces_proveedores=$29
WHERE comitente=$1 AND id=$2`

	_, err = tx.Exec(ctx, query,
		v.Comitente,
		v.ID,
		v.Nombre,
		v.NombreImpresion,
		v.Categoria,
		v.Descripcion,
		v.EsUnCombo,
		v.DetallesCombo,
		v.Inventariable,
		v.Cuantificable,
		v.TipoUM,
		v.UM,
		v.Envase,
		v.RelacionesUMFija,
		v.RelacionesUM,
		v.Facetas,
		v.AttsGenerales,
		v.TieneVariantes,
		v.DisponibleParaVenta,
		v.PrecioFinal,
		v.AlicuotaIVA,
		v.AlicuotaCeroVenta,
		v.TrabajaConUnicidades,
		v.ClaseUnicidad,
		v.Activo,
		v.Apertura,
		v.CreatedAt,
		v.UpdatedAt,
		v.CrucesConProveedores,
	)
	if err != nil {
		return deferror.DB(err, "updating")
	}
	return
}

func UpdateSKU(ctx context.Context, tx pgx.Tx, v *SKU) (err error) {
	query := `
UPDATE skus
SET 
		codigo=$3,
		nombre=$4, 
		nombre_impresion=$5,
		categoria=$6,
		bar_code=$7,
		inventariable=$8,
		cuantificable=$9,
		tipo_um=$10,
		um=$11,
		envase=$12,
		relaciones_um_fija=$13,
		relaciones_um=$14,
		atts=$15,
		activo=$16,
		created_at=$17,
		updated_at=$18,
		atts_especificos=$19
WHERE comitente=$1 AND id=$2`

	_, err = tx.Exec(ctx, query, v.Comitente, v.ID,
		v.Codigo,
		v.Nombre,
		v.NombreImpresion,
		v.Categoria,
		v.BarCode,
		v.Inventariable,
		v.Cuantificable,
		v.TipoUM,
		v.UM,
		v.Envase,
		v.RelacionesUMFija,
		v.RelacionesUM,
		v.Atts,
		v.Activo,
		v.CreatedAt,
		v.UpdatedAt,
		v.Atts,
	)
	if err != nil {
		return deferror.DB(err, "updating")
	}
	return
}

func ReadOneSKU(ctx context.Context, conn *pgxpool.Pool, req ReadOneReq) (v SKU, err error) {

	// Valido
	if req.Comitente == 0 {
		return v, errors.New("no se ingresó ID de comitente")
	}
	if req.ID == uuid.Nil {
		return v, errors.New("no se ingresó ID de comitente")
	}

	query := `SELECT
		id,
		producto,
		comitente,
		codigo,
		nombre, 
		nombre_impresion,
		categoria,
		bar_code,
		inventariable,
		cuantificable,
		tipo_um,
		um,
		envase,
		relaciones_um_fija,
		relaciones_um,
		atts,
		activo,
		created_at,
		updated_at,
		atts_especificos
	FROM skus
	WHERE comitente=$1 AND id=$2`

	err = conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(
		&v.ID, &v.Producto, &v.Comitente, &v.Codigo, &v.Nombre, &v.NombreImpresion,
		&v.Categoria, &v.BarCode, &v.Inventariable, &v.Cuantificable, &v.TipoUM, &v.UM,
		&v.Envase, &v.RelacionesUMFija, &v.RelacionesUM, &v.Atts, &v.Activo, &v.CreatedAt,
		&v.UpdatedAt, &v.Atts)
	if err != nil {
		return v, deferror.DB(err, "querying/scanning")
	}
	return
}

type ReadManySKUReq struct {
	ID        uuid.UUID   // ID de PRODUCTO
	IDs       []uuid.UUID // ID de SKU
	Comitente int
}

func ReadManySKU(ctx context.Context, conn *pgxpool.Pool, req ReadManySKUReq) (out []SKU, err error) {

	out = []SKU{}

	// Valido
	if req.Comitente == 0 {
		return out, errors.New("no se ingresó ID de comitente")
	}
	if req.ID != uuid.Nil && len(req.IDs) > 0 {
		return out, errors.New("se debe buscar por ID o por IDs, no por ambos")
	}
	if req.ID == uuid.Nil && len(req.IDs) == 0 {
		return out, errors.New("no se ingresó ID ni IDs")
	}
	if req.ID != uuid.Nil && len(req.IDs) > 0 {
		return out, errors.New("se debe buscar por ID o por IDs, no por ambos")
	}
	if req.ID == uuid.Nil && len(req.IDs) == 0 {
		return out, errors.New("no se ingresó ID ni IDs")
	}

	ww := []string{"comitente = $1"}
	pp := []interface{}{req.Comitente}

	if req.ID != uuid.Nil {
		pp = append(pp, req.ID)
		ww = append(ww, fmt.Sprintf("producto = $%v", len(pp)))
	}
	if len(req.IDs) > 0 {
		ww = append(ww, fmt.Sprintf("id IN (%v)", arrayUUID(req.IDs)))
	}
	where := "WHERE " + strings.Join(ww, " AND ")
	query := fmt.Sprintf(`SELECT
		id,
		producto,
		comitente,
		codigo,
		nombre, 
		nombre_impresion,
		categoria,
		bar_code,
		inventariable,
		cuantificable,
		tipo_um,
		um,
		envase,
		relaciones_um_fija,
		relaciones_um,
		atts,
		activo,
		created_at,
		updated_at,
		atts_especificos
	FROM skus
	%v
	`, where)

	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		v := SKU{}
		err = rows.Scan(
			&v.ID,
			&v.Producto,
			&v.Comitente,
			&v.Codigo,
			&v.Nombre,
			&v.NombreImpresion,
			&v.Categoria,
			&v.BarCode,
			&v.Inventariable,
			&v.Cuantificable,
			&v.TipoUM,
			&v.UM,
			&v.Envase,
			&v.RelacionesUMFija,
			&v.RelacionesUM,
			&v.Atts,
			&v.Activo,
			&v.CreatedAt,
			&v.UpdatedAt,
			&v.Atts)
		if err != nil {
			return out, deferror.DB(err, "escaneando")
		}
		out = append(out, v)
	}
	return
}
