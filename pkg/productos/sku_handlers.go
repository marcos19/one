package productos

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
)

type CreateSKUReq struct {
	SKU       SKU
	Comitente int
}

func (h *Handler) CreateSKU(ctx context.Context, req CreateSKUReq) (vv []SKU, err error) {

	// Valido
	if req.SKU.Producto == uuid.Nil {
		return vv, deferror.Validation("no se incluyó el ID de producto")
	}

	// Comienzo transacción
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return vv, deferror.DB(err, "inciando transacción")
	}
	defer tx.Rollback(ctx)

	// Busco producto
	prod, err := ReadOneProducto(ctx, h.conn, ReadOneReq{Comitente: req.Comitente, ID: req.SKU.Producto})
	if err != nil {
		return vv, deferror.DB(err, "buscando el producto al que pertenece la variante")
	}

	// Completo datos
	req.SKU.ID, err = uuid.NewV4()
	if err != nil {
		tx.Rollback(ctx)
		return
	}
	req.SKU.Comitente = req.Comitente
	req.SKU.Producto = prod.ID
	req.SKU.Nombre = prod.Nombre
	req.SKU.NombreImpresion = prod.NombreImpresion

	// Le agrego los atributos al nombre de la variante
	for _, v := range prod.Apertura {
		attStr, ok := req.SKU.Atts[v].(string)
		if !ok {
			return vv, errors.Errorf("no se pudo convertir a string el atributo de la variante")
		}
		req.SKU.Nombre += " " + attStr
	}

	// Inserto el SKU
	err = CreateSKU(ctx, tx, &req.SKU)
	if err != nil {
		return vv, deferror.DB(err, "persistiendo SKU")
	}

	// Confirmo transacción
	err = tx.Commit(ctx)
	if err != nil {
		return vv, deferror.DB(err, "confirmando transacción")
	}

	// Devuelvo el nuevo listado de variantes
	vv, err = ReadManySKU(ctx, h.conn, ReadManySKUReq{Comitente: req.Comitente, ID: req.SKU.Producto})
	if err != nil {
		return vv, deferror.DB(err, "buscando listado de nuevas variantes")
	}

	return
}

// HandleSKUs devuelve todos los SKUS ue hay para el producto.
func (h *Handler) ReadManySKU(ctx context.Context, req ReadManySKUReq) (out []SKU, err error) {

	out, err = ReadManySKU(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando SKUs")
	}

	return
}

// HandleModificarSKU modifica un SKU y los devuelve todos.
func (h *Handler) UpdateSKU(ctx context.Context, req SKU) (err error) {

	if req.ID == uuid.Nil {
		return deferror.Validation("no se incluyó el ID de variante a modificar")
	}
	if req.Producto == uuid.Nil {
		return deferror.Validation("no se incluyó el ID del producto a modificar")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó el ID de comitente")
	}

	// Corroboro que exista el producto
	prod, err := h.ReadOne(ctx, ReadOneReq{Comitente: req.Comitente, ID: req.Producto})
	if err != nil {
		return errors.Wrap(err, "buscando producto al que pertenece la variante")
	}

	req.Producto = prod.ID
	req.Nombre = prod.Nombre
	req.NombreImpresion = prod.NombreImpresion

	// Le agrego los atributos al nombre de la variante
	for _, v := range prod.Apertura {
		attStr, ok := req.Atts[v].(string)
		if !ok {
			return errors.Errorf("no se pudo convertir a string el atributo de la variante")
		}
		req.Nombre += " " + attStr
	}

	// Invalido cache
	h.cache.Del(hashUUID("SKU", req.Comitente, req.ID))

	// Inicio transacción
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return deferror.DB(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)

	// Persisto modificación
	err = UpdateSKU(ctx, tx, &req)
	if err != nil {
		return deferror.DB(err, "persistiendo variante")
	}

	err = tx.Commit(ctx)
	if err != nil {
		return deferror.DB(err, "confirmando transacción")
	}

	return
}

type DeleteSKUReq struct {
	ID        uuid.UUID
	Producto  uuid.UUID
	Comitente int
}

func (h *Handler) DeleteSKU(ctx context.Context, req DeleteSKUReq) (err error) {

	// Valido
	if req.ID == uuid.Nil {
		return deferror.Validation("no se ingresó ID de SKU")
	}
	if req.Producto == uuid.Nil {
		return deferror.Validation("no se ingresó ID de producto")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return deferror.DB(err, "iniciando transacción")
	}

	// Está usada en la contabilidad?
	count := 0
	err = tx.
		QueryRow(ctx, "SELECT COUNT(sku) FROM partidas WHERE comitente=$1 AND sku=$2", req.Comitente, req.ID).
		Scan(&count)
	if err != nil {
		return deferror.DB(err, "determinando si SKU se encontraba usada")
	}
	if count > 0 {
		return deferror.Validationf("No se puede borrar porque la variante ya ha sido usada %v veces", count)
	}

	// Borro precios
	_, err = tx.Exec(ctx, "DELETE FROM precios WHERE comitente=$1 AND sku=$2", req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "borrando tabla precios")
	}

	// Borro SKU
	_, err = tx.Exec(ctx, "DELETE FROM skus WHERE comitente=$1 AND producto=$2 AND id=$3", req.Comitente, req.Producto, req.ID)
	if err != nil {
		return deferror.DB(err, "borrando tabla skus")
	}

	// Invalido cache
	h.cache.Del(hashUUID("SKU", req.Comitente, req.ID))

	// Confirmo transacción
	err = tx.Commit(ctx)
	if err != nil {
		return deferror.DB(err, "confirmando transacción")
	}

	return
}
