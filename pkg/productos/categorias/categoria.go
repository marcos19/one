package categorias

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"github.com/cockroachdb/errors"
	"github.com/rs/zerolog/log"
)

// Categoria represta una categoria para agrupar productos.
// Funciona similar a las cuentas contables, generan un arbol.
// Hay imputables y no imputables.
// Las uso ademas de para la navegación, definen una serie de atributos
// que son comunes a los productos que le pertenecen.
type Categoria struct {
	ID        *int `json:",string"`
	Comitente int
	Nombre    string
	Imputable bool
	PadreID   *int `json:",string"`
	FullPath  string
	Familia   *int `json:",string"`
	// Son los atributos que me importan para la categoría.
	// Ej: si estoy en la categoría televisores, quiero filtrar por pulgadas
	// y por marca. Al definirlos en la categoría, cada vez que doy de alta
	// un TV me pone estos atributos para completar.
	Facetas Facetas

	// Letras para numerar productos
	Mascara string

	// Numerador para los nuevos productos que se creen
	Ultimo int

	CreatedAt *time.Time
	UpdatedAt *time.Time
}

func (c Categoria) TableName() string {
	return "productos_categorias"
}

type Facetas []Faceta

type Faceta struct {
	Nombre string // Ej: Marca, Pulgadas
	Tipo   string // bool, string, int, d4, date
}

func LlenarMascara(msk string, ultimo int) (out string, err error) {

	// Vacía? devuelvo el número
	if msk == "" {
		return strconv.Itoa(ultimo), nil
	}

	// Sólo numerales?
	if len(msk) > 0 {
		for _, v := range msk {
			if v != rune('#') {
				break
			}
		}
		// Completo con ceros adelante
		out, err := llenarCeros(ultimo, len(msk))
		if err != nil {
			return out, errors.Wrapf(err, "llenando máscara %v", msk)
		}
	}

	desde := -1
	hasta := -1

	for i, v := range msk {
		if v == '#' {
			if desde < 0 {
				desde = i
				continue
			}
		}
		if v != '#' && desde >= 0 {
			// acá termina
			hasta = i - 1
			break
		}
	}

	if hasta < 0 {
		hasta = len(msk) - 1
	}
	log.Debug().Msgf("%v - Desde: %v, Hasta: %v", msk, desde, hasta)
	if hasta < desde {
		return out, errors.Errorf("no se pudo determinar donde empiezan y terminan los números")
	}

	// Completo con ceros adelante
	nums, err := llenarCeros(ultimo, hasta-desde+1)
	if err != nil {
		return out, errors.Wrapf(err, "llenando máscara %v", msk)
	}
	out = msk[:desde] + nums + msk[hasta+1:]
	return
}

// Pone ceros a la izquierda
func llenarCeros(n int, ceros int) (out string, err error) {
	if n < 0 {
		return out, errors.Errorf("el número no puede ser negativo")
	}
	out = fmt.Sprint(n)
	if len(out) > ceros {
		return out, errors.Errorf("El número %v es mayor a la cantidad de ceros disponibles (%v)", n, ceros)
	}
	for faltan := ceros - len(out); faltan > 0; faltan-- {
		out = "0" + out
	}

	return
}

// Value cumple con la interface SQL
func (f Facetas) Value() (driver.Value, error) {
	return json.Marshal(f)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (f *Facetas) Scan(src interface{}) error {
	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New("Type assertion error .([]byte)")
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, f)
	if err != nil {
		return errors.Wrap(err, "Unmarshalling")
	}

	return nil
}
