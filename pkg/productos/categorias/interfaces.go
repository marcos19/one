package categorias

import "context"

type Getter interface {
	ReadOne(ReadOneReq) (Categoria, error)
}
type SuperiorGetter interface {
	CategoriaSuperior(ctx context.Context, req ReadOneReq) (out *Categoria, err error)
}
type CentroGetter interface {
	Centro(ctx context.Context, req ReadOneReq) (out int, err error)
}

type EventBus interface {
	CambióCategoriaProducto(crud string, cat Categoria)
}
