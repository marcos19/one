package categorias

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestLlenarCeros(t *testing.T) {
	{
		_, err := llenarCeros(10, 1)
		require.NotNil(t, err)
	}
	{
		out, err := llenarCeros(10, 2)
		require.Nil(t, err)
		assert.Equal(t, "10", out)
	}
	{
		out, err := llenarCeros(10, 3)
		require.Nil(t, err)
		assert.Equal(t, "010", out)
	}
	{
		out, err := llenarCeros(10, 4)
		require.Nil(t, err)
		assert.Equal(t, "0010", out)
	}
	{
		_, err := llenarCeros(-10, 4)
		require.NotNil(t, err)
	}
}

func TestLlenarMascara(t *testing.T) {

	{
		out, err := LlenarMascara("", 1)
		require.Nil(t, err)
		assert.Equal(t, "1", out)
	}
	{
		out, err := LlenarMascara("##", 1)
		require.Nil(t, err)
		assert.Equal(t, "01", out)
	}
	{
		_, err := LlenarMascara("##", 120)
		require.NotNil(t, err)
	}
	{
		out, err := LlenarMascara("CMB###", 1)
		require.Nil(t, err)
		assert.Equal(t, "CMB001", out)
	}
	{
		out, err := LlenarMascara("CMB-###/F", 1)
		require.Nil(t, err)
		assert.Equal(t, "CMB-001/F", out)
	}
}
