package categorias

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/dgraph-io/ristretto"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler es la estructura que recibe los request y emite los responses
type Handler struct {
	conn *pgxpool.Pool
	// eventbus EventBus
	cache *ristretto.Cache
}

func NewHandler(args *HandlerArgs) (h *Handler, err error) {
	if args.Conn == nil {
		return h, errors.New("DB no puede ser nil")
	}
	if args.Cache == nil {
		return h, errors.New("Cache no puede ser nil")
	}

	h = &Handler{
		conn: args.Conn,
		// eventbus: args.EventBus,
		cache: args.Cache,
	}

	return
}

type HandlerArgs struct {
	Conn *pgxpool.Pool
	// EventBus EventBus
	Cache *ristretto.Cache
}

func (h *Handler) CategoriasParaStore(ctx context.Context, comitente int) (cc []Categoria, err error) {

	cc = []Categoria{}

	query := `SELECT id, comitente, nombre, full_path, imputable, padre_id, familia, facetas FROM productos_categorias WHERE comitente=$1`
	rows, err := h.conn.Query(ctx, query, comitente)
	if err != nil {
		return cc, deferror.DB(err, "buscando categorías")
	}
	defer rows.Close()

	for rows.Next() {
		c := Categoria{}
		err = rows.Scan(&c.ID, &c.Comitente, &c.Nombre, &c.FullPath, &c.Imputable, &c.PadreID, &c.Familia, &c.Facetas)
		if err != nil {
			return cc, deferror.DB(err, "escaneando")
		}
		cc = append(cc, c)
	}
	return
}

type CategoriaConCantidad struct {
	ID       int `json:",string"`
	Nombre   string
	Cantidad int
	Facetas  Facetas
}

// HandleCategoriasConCantidad devuelve el listado de categorias con la cantidad de productos que tiene cada una.
func (h *Handler) ReadCategoriasConCantidad(ctx context.Context, comitente int) (out []CategoriaConCantidad, err error) {

	query := `
	SELECT skus.categoria AS id, productos_categorias.nombre AS nombre, COUNT(skus.categoria) as cantidad
	FROM skus 
	LEFT JOIN productos_categorias ON productos_categorias.id = skus.categoria
	WHERE productos_categorias.comitente=$1 AND activo=true
	GROUP BY skus.categoria, productos_categorias.nombre
	ORDER BY productos_categorias.nombre
	`
	rows, err := h.conn.Query(ctx, query, comitente)
	if err != nil {
		return out, deferror.DB(err, "buscando categorias")
	}
	defer rows.Close()

	for rows.Next() {
		c := CategoriaConCantidad{}
		err = rows.Scan(&c.ID, &c.Nombre, &c.Cantidad)
		if err != nil {
			return out, deferror.DB(err, "scanning")
		}
		out = append(out, c)
	}
	// err = h.db.Table("skus").
	// 	Select("skus.categoria AS id, productos_categorias.nombre AS nombre, COUNT(skus.categoria) as cantidad").
	// 	Where("productos_categorias.comitente = ? AND activo = true", comitente).
	// 	Group("skus.categoria, productos_categorias.nombre").
	// 	Joins("LEFT JOIN productos_categorias ON productos_categorias.id = skus.categoria").
	// 	Order("productos_categorias.nombre").
	// 	Scan(&out).Error
	return

}

type ReadManyReq struct {
	Tipo      string // 'imputables' o 'no imputables'
	NoMostrar int    `json:",string"` // Cuando estoy editando una categoría, no quiero que aparezca como padre también y se pueda autoreferenciar
	Comitente int
}

// HandleCategorias devuelve las categorías solicitadas
func (h *Handler) ReadMany(ctx context.Context, req ReadManyReq) (out []Categoria, err error) {

	out = []Categoria{}

	if req.Comitente == 0 {
		return out, errors.Errorf("no se determinó comitente")
	}
	ww := []string{"comitente=$1"}
	pp := []interface{}{req.Comitente}

	switch req.Tipo {
	case "no imputables":
		ww = append(ww, "imputable = false")
	case "imputables":
		ww = append(ww, "imputable = true")
	}

	if req.NoMostrar != 0 {
		pp = append(pp, req.NoMostrar)
		ww = append(ww, fmt.Sprintf("id <> $%v", len(pp)))
	}
	where := "WHERE " + strings.Join(ww, " AND ")
	query := fmt.Sprintf(
		`SELECT id, comitente, nombre, full_path, imputable, padre_id, familia, mascara, facetas FROM productos_categorias %v`,
		where)

	rows, err := h.conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		c := Categoria{}
		err = rows.Scan(&c.ID, &c.Comitente, &c.Nombre, &c.FullPath, &c.Imputable, &c.PadreID, &c.Familia, &c.Mascara, &c.Facetas)
		if err != nil {
			return out, deferror.DB(err, "scanning")
		}
		out = append(out, c)
	}
	return
}

func (h *Handler) Create(ctx context.Context, req *Categoria) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}

	{ // Hay alguna con ese nombre?
		count := 0
		err = h.conn.QueryRow(ctx, "SELECT COUNT(id) FROM productos_categorias WHERE comitente=$1 AND nombre=$2",
			req.Comitente, req.Nombre).Scan(&count)
		if err != nil {
			return deferror.DB(err, "determinando si había otra categoría con el mismo nombre")
		}
		if count > 0 {
			return deferror.Validation("Ya existe otra categoría con ese nombre")
		}
	}

	// Inicio transacción
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return deferror.DB(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)

	// Inserto
	query := `INSERT INTO productos_categorias (comitente, nombre, full_path, imputable, padre_id, familia, mascara, ultimo, facetas, created_at) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)`
	_, err = tx.Exec(ctx, query, req.Comitente, req.Nombre, req.FullPath, req.Imputable, req.PadreID, req.Familia, req.Mascara, 0, req.Facetas, time.Now())
	if err != nil {
		return deferror.DB(err, "persistiendo categoría")
	}

	// Imprimo los full path de cada categoría
	err = h.categoriasArmarFullPath(ctx, req.Comitente, tx)
	if err != nil {
		return errors.Wrap(err, "armando full paths")
	}

	// Grabo las familias
	err = h.fijarFamilias(ctx, req.Comitente, tx)
	if err != nil {
		return errors.Wrap(err, "fijando familias a los hijos")
	}

	// Confirmo transacción
	err = tx.Commit(ctx)
	if err != nil {
		return deferror.DB(err, "confirmando transacción")
	}

	// h.eventbus.CambióCategoriaProducto("creó", *req)

	return

}

type ReadOneReq struct {
	ID        int `json:",string"`
	Comitente int
}

// GetCategoria devuelve un producto contable por su ID.
// En el key del cache solo guardo el UUID.
// Corroboro que pertenezca al comitente antes de devolverlo.
func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (out Categoria, err error) {

	val, ok := h.cache.Get(hashInt(req.Comitente, req.ID))
	if ok {
		out, ok = val.(Categoria)
		if ok && out.Comitente == req.Comitente {
			return
		}

	} else {

		query := `SELECT id, comitente, nombre, full_path, imputable, padre_id, familia, mascara, ultimo, facetas, created_at, updated_at FROM productos_categorias WHERE comitente=$1 AND id=$2`
		// No estaba en cache, lo busco en la base de datos
		err = h.conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(
			&out.ID, &out.Comitente, &out.Nombre, &out.FullPath, &out.Imputable, &out.PadreID, &out.Familia, &out.Mascara, &out.Ultimo, &out.Facetas, &out.CreatedAt, &out.UpdatedAt,
		)
		if err != nil {
			return out, deferror.DBf(err, "buscando categoría ID=%v", req.ID)
		}

		h.cache.Set(hashInt(req.Comitente, req.ID), out, 0)
	}

	return
}

func (h *Handler) AumentarNumeracion(ctx context.Context, tx pgx.Tx, req ReadOneReq) (err error) {

	res, err := tx.Exec(ctx, "UPDATE productos_categorias SET ultimo = ultimo +1 WHERE comitente=$1 AND id=$2",
		req.Comitente, req.ID)
	if err != nil {
		return errors.Wrap(err, "actualizando numerador")
	}
	if res.RowsAffected() == 0 {
		return errors.Errorf("no se actualizó ninguna categoría")
	}
	// Borro del cache
	h.cache.Del(hashInt(req.Comitente, req.ID))
	return

}
func (h *Handler) Update(ctx context.Context, req Categoria) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}
	if req.ID == nil {
		return deferror.Validation("no se ingresó ID de categoría")
	}

	// Inicio transacción
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return deferror.DB(err, "inciando transacción")
	}

	{ // Hay alguna con ese nombre?
		count := 0
		err = tx.QueryRow(ctx, "SELECT COUNT(id) FROM productos_categorias WHERE comitente=$1 AND nombre=$2 AND id<>$3", req.Comitente, req.Nombre, req.ID).Scan(&count)
		if err != nil {
			return deferror.DB(err, "corroborando si existía categoría con ese mismo nombre")
		}
		if count > 0 {
			return deferror.Validation("Ya existe una categoría con el mismo nombre.")
		}
	}

	// Borro del cache
	h.cache.Del(hashInt(req.Comitente, *req.ID))

	// Update
	query := `UPDATE productos_categorias SET nombre=$3, full_path=$4, imputable=$5, padre_id=$6, familia=$7, mascara=$8, facetas=$9, updated_at=$10 WHERE comitente=$1 AND id=$2`
	_, err = tx.Exec(ctx, query, req.Comitente, req.ID, req.Nombre, req.FullPath, req.Imputable, req.PadreID, req.Familia, req.Mascara, req.Facetas, time.Now())
	if err != nil {
		return deferror.DB(err, "updating")
	}

	// Imprimo los full path de cada categoría
	err = h.categoriasArmarFullPath(ctx, req.Comitente, tx)
	if err != nil {
		return errors.Wrap(err, "armando full paths")
	}

	// Grabo las familias
	err = h.fijarFamilias(ctx, req.Comitente, tx)
	if err != nil {
		return errors.Wrap(err, "fijando familias a los hijos")
	}

	err = tx.Commit(ctx)
	if err != nil {
		return deferror.DB(err, "confirmando tranasacción")
	}

	// h.eventbus.CambióCategoriaProducto("creó", req)

	return

}

type DeleteReq struct {
	ID        int `json:",string"`
	Comitente int
}

func (h *Handler) Delete(ctx context.Context, req DeleteReq) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}
	if req.ID == 0 {
		return deferror.Validation("no se ingresó ID de categoría")
	}

	// Corroboro que no tenga hijas
	count := 0
	err = h.conn.QueryRow(ctx, "SELECT COUNT(id) FROM productos_categorias WHERE comitente=$1 AND padre_id=$2 ", req.Comitente, req.ID).Scan(&count)
	if err != nil {
		return errors.Wrap(err, "buscando si tenía hijos")
	}
	if count > 0 {
		return deferror.Validationf("No se puede borrar porque la categoría tiene %v subcategorias", count)
	}

	// Busco anterior
	_, err = h.ReadOne(ctx, ReadOneReq(req))
	if err != nil {
		return errors.Wrap(err, "buscando categoría")
	}

	// Borro del cache
	h.cache.Del(hashInt(req.Comitente, req.ID))

	query := `DELETE FROM productos_categorias WHERE comitente=$1 AND id=$2`
	_, err = h.conn.Exec(ctx, query, req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "borrando")
	}

	// h.eventbus.CambióCategoriaProducto("creó", ant)

	return

}

// Busca centro de imputación para la categoría o sus superiores hasta que
// encuentre alguna. Si no encuentra devuelve error.
func (h *Handler) Centro(ctx context.Context, req ReadOneReq) (centro int, err error) {

	// Valido
	if req.Comitente == 0 {
		return centro, deferror.Validation("no se definió ID de comitente")
	}
	if req.ID == 0 {
		return centro, deferror.Validation("no se definió ID de categoría")
	}

	id := req.ID
	count := 0

	// Busco categoría y sus superiores
	for {
		count++
		if count > 20 {
			return centro, errors.Errorf("se alcanzó máximo de iteraciones")
		}
		cat, err := h.ReadOne(ctx, ReadOneReq{
			Comitente: req.Comitente,
			ID:        id,
		})
		if err != nil {
			return centro, errors.Wrapf(err, "buscando categoria %v", req.ID)
		}
		if cat.Familia != nil {
			if *cat.Familia != 0 {
				return *cat.Familia, nil
			}
		}
		if cat.PadreID == nil {
			return 0, errors.Errorf("no se pudo determinar centro de imputación para la categoría %v", req.ID)
		}

		id = *cat.PadreID
	}
}

// Si devuelve nil es porque es una categoría root.
func (h *Handler) CategoriaSuperior(ctx context.Context, req ReadOneReq) (out *Categoria, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se definió ID de comitente")
	}
	if req.ID == 0 {
		return out, deferror.Validation("no se definió ID de categoría")
	}

	// Busco
	cat, err := h.ReadOne(ctx, req)
	if err != nil {
		return out, errors.Wrapf(err, "buscando categoria %v", req.ID)
	}

	if cat.PadreID == nil {
		return nil, nil
	}

	// Busco cuenta padre
	// Busco
	padre, err := h.ReadOne(ctx, ReadOneReq{
		ID:        *cat.PadreID,
		Comitente: req.Comitente,
	})
	if err != nil {
		return out, errors.Wrapf(err, "buscando categoria padre %v", req.ID)
	}

	return &padre, nil

}

// Las familias de productos (las que uso para contabilizar), se pueden
// definir a cualquier nivel del arbol de categorías.
// Todas las categorías y productos que estén debajo, pertenecen a la misma
// familia.
// Las categorías imputables tienen definida una familia, de esta manera
// cuando intente buscar la familia de un producto no tengo que recurrir
// hacia arriba.
// Cada vez que haya una modificación en las categorías, las regrabo a todas.
func (h *Handler) fijarFamilias(ctx context.Context, comitente int, tx pgx.Tx) (err error) {
	cc := []Categoria{}
	query := `SELECT id, familia FROM productos_categorias WHERE comitente=$1 AND padre_id IS NULL`
	rows, err := tx.Query(ctx, query, comitente)
	if err != nil {
		return deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		c := Categoria{}
		err = rows.Scan(&c.ID, &c.Familia)
		if err != nil {
			return deferror.DB(err, "escaneando")
		}
		cc = append(cc, c)
	}

	for _, v := range cc {
		err = h.fijarCategoriasHijos(ctx, comitente, *v.ID, v.Familia, tx)
		if err != nil {
			return errors.Wrapf(err, "fijando familias de categoría %v", v.ID)
		}
	}

	return
}

func (h *Handler) fijarCategoriasHijos(ctx context.Context, comitente, hijosDe int, familia *int, tx pgx.Tx) (err error) {

	cc := []Categoria{}

	// Busco la que son hijas de esta y hago lo mismo
	query := "SELECT id, padre_id, familia FROM productos_categorias WHERE comitente=$1 AND padre_id=$2"
	rows, err := tx.Query(ctx, query, comitente, hijosDe)
	if err != nil {
		return deferror.DB(err, "querying")
	}
	defer rows.Close()
	for rows.Next() {
		c := Categoria{}
		err = rows.Scan(&c.ID, &c.PadreID, &c.Familia)
		if err != nil {
			return deferror.DB(err, "escaneando")
		}
		cc = append(cc, c)
	}

	for _, v := range cc {
		if familia != nil {
			// Cambio la familia de esta categoría
			_, err = tx.Exec(ctx, "UPDATE productos_categorias SET familia=$1 WHERE id=$2", *familia, v.ID)
			if err != nil {
				return deferror.DB(err, "updating")
			}
		}
		if familia == nil && v.Familia != nil {
			// Si padre no tenía familia determinada pero este sí.
			familia = new(int)
			*familia = *v.Familia
		}
		err = h.fijarCategoriasHijos(ctx, comitente, *v.ID, familia, tx)
		if err != nil {
			return errors.Wrapf(err, "fijando familias de categoría %v", v.ID)
		}
	}

	return
}

func (h *Handler) categoriasArmarFullPath(ctx context.Context, comitente int, tx pgx.Tx) (err error) {
	cc := []Categoria{}
	rows, err := tx.Query(ctx, "SELECT id, padre_id, nombre FROM productos_categorias WHERE comitente=$1", comitente)
	if err != nil {
		return errors.Wrap(err, "buscando categorías")
	}
	defer rows.Close()

	for rows.Next() {
		c := Categoria{}
		err = rows.Scan(&c.ID, &c.PadreID, &c.Nombre)
		if err != nil {
			return deferror.DB(err, "escaneando")
		}
		cc = append(cc, c)
	}

	for i, c := range cc {
		fullPath := strings.TrimRight(fullPath(c, cc), " / ")
		if fullPath != c.FullPath {
			cc[i].FullPath = fullPath
			query := `UPDATE productos_categorias SET full_path = $1 WHERE id=$2 AND comitente=$3`
			_, err := tx.Exec(ctx, query, fullPath, c.ID, comitente)
			if err != nil {
				return deferror.DB(err, "fijando full path")
			}
		}
	}

	return
}

func fullPath(c Categoria, cc []Categoria) string {
	if c.PadreID == nil {
		return c.Nombre + " / "
	}

	// tiene un padre. Lo busco
	for _, v := range cc {
		if *v.ID == *c.PadreID {
			return fullPath(v, cc) + c.Nombre + " / "
		}
	}
	panic(fmt.Sprint("no deberiamos haber llegado acá", c))
}

func (h *Handler) Del(comitente int, id int) {
	h.cache.Del(hashInt(comitente, id))
}

func hashInt(comitente int, id int) string {
	return fmt.Sprintf("%v/%v/%x", "Categoría", comitente, id)
}
