package productos

import "context"

type ReaderOne interface {
	ReadOne(context.Context, ReadOneReq) (Producto, error)
}
