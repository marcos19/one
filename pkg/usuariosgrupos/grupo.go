package usuariosgrupos

import "time"

type Grupo struct {
	ID        int `json:",string,omitempty"`
	Comitente int
	Nombre    string
	Detalle   string
	Usuarios  []string
	CreatedAt *time.Time
	UpdatedAt *time.Time
}

/*

CREATE TABLE usuarios_grupos (
id SERIAL PRIMARY KEY,
comitente INT NOT NULL REFERENCES comitentes,
nombre STRING NOT NULL,
detalle STRING NOT NULL,
usuarios STRING[],
created_at TIMESTAMP WITH TIME ZONE NOT NULL,
updated_at TIMESTAMP WITH TIME ZONE
)


*/
