package db

import (
	"context"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/usuariosgrupos"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

func Create(ctx context.Context, conn *pgxpool.Pool, req usuariosgrupos.Grupo) (out int, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no ingresó el ID de comitente")
	}

	query := `INSERT INTO usuarios_grupos (comitente, nombre, detalle, usuarios, created_at) 
	VALUES ($1,$2,$3,$4,$5)
	RETURNING id`

	// Persisto
	err = conn.
		QueryRow(ctx, query, req.Comitente, req.Nombre, req.Detalle, req.Usuarios, time.Now()).
		Scan(&out)
	if err != nil {
		return out, errors.Wrap(err, "insertando")
	}

	return
}

func ReadOne(ctx context.Context, conn *pgxpool.Pool, req usuariosgrupos.ReadOneReq) (out usuariosgrupos.Grupo, err error) {

	// Valido
	if req.ID == 0 {
		return out, deferror.Validation("no se ingresó ID a buscar")
	}
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó comitente a buscar")
	}

	query := `SELECT id, comitente, nombre, detalle, usuarios, created_at, updated_at FROM usuarios_grupos WHERE comitente=$1 AND id=$2`

	// Busco
	err = conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(
		&out.ID, &out.Comitente, &out.Nombre, &out.Detalle, &out.Usuarios, &out.CreatedAt, &out.UpdatedAt,
	)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning grupo")
	}

	return
}

func ReadMany(ctx context.Context, conn *pgxpool.Pool, req usuariosgrupos.ReadManyReq) (out []usuariosgrupos.Grupo, err error) {

	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó comitente a buscar")
	}

	out = []usuariosgrupos.Grupo{}
	query := `SELECT id, comitente, nombre, detalle, usuarios, created_at, updated_at FROM usuarios_grupos WHERE comitente=$1`

	rows, err := conn.Query(ctx, query, req.Comitente)
	if err != nil {
		return out, errors.Wrap(err, "querying grupos")
	}
	defer rows.Close()

	for rows.Next() {
		v := usuariosgrupos.Grupo{}
		err = rows.Scan(
			&v.ID, &v.Comitente, &v.Nombre, &v.Detalle, &v.Usuarios, &v.CreatedAt, &v.UpdatedAt,
		)
		if err != nil {
			return out, deferror.DB(err, "escaneando")
		}
		out = append(out, v)
	}
	return
}
func Update(ctx context.Context, conn *pgxpool.Pool, req usuariosgrupos.Grupo) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no ingresó el ID de comitente ")
	}

	query := `UPDATE usuarios_grupos SET nombre=$3, detalle=$4, usuarios=$5, updated_at=$6 WHERE comitente=$1 AND id=$2`
	// Persisto
	res, err := conn.Exec(ctx, query, req.Comitente, req.ID, req.Nombre, req.Detalle, req.Usuarios, time.Now())
	if err != nil {
		return deferror.DB(err, "modificando grupo")
	}

	if res.RowsAffected() == 0 {
		return errors.Errorf("no se modificó ningún grupo")
	}
	return
}

func Delete(ctx context.Context, conn *pgxpool.Pool, req usuariosgrupos.ReadOneReq) (err error) {

	// Valido
	if req.ID == 0 {
		return deferror.Validation("no se ingresó ID a buscar")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó comitente a buscar")
	}

	// Busco
	res, err := conn.Exec(ctx, "DELETE FROM usuarios_grupos WHERE comitente=$1 AND id=$2", req.Comitente, req.ID)
	if err != nil {
		return errors.Wrap(err, "deleting")
	}

	if res.RowsAffected() == 0 {
		return errors.Errorf("no se borró ningún registro")
	}
	return
}
