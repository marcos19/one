package dbtest

import (
	"context"
	"testing"

	"bitbucket.org/marcos19/one/pkg/database"
	"bitbucket.org/marcos19/one/pkg/usuariosgrupos"
	"bitbucket.org/marcos19/one/pkg/usuariosgrupos/internal/db"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDB(t *testing.T) {
	ctx := context.Background()

	cn := database.TestDBConnectionString()

	cfg, err := pgxpool.ParseConfig(cn)
	require.Nil(t, err)
	conn, err := pgxpool.ConnectConfig(context.Background(), cfg)
	require.Nil(t, err)

	// Preparo base de datos
	err = Prepare(ctx, conn)
	require.Nil(t, err)

	id := 0
	t.Run("CREATE", func(t *testing.T) {
		id, err = db.Create(ctx, conn, grupoValido())
		require.Nil(t, err)
		assert.NotEqual(t, 0, id)
	})

	t.Run("READ", func(t *testing.T) {
		expected := grupoValido()
		expected.ID = id
		e, err := db.ReadOne(ctx, conn, usuariosgrupos.ReadOneReq{
			Comitente: 1,
			ID:        id,
		})
		require.Nil(t, err)
		assert.NotEqual(t, 0, e.ID)
		assert.NotNil(t, 0, e.CreatedAt)
		assert.Nil(t, e.UpdatedAt)
		e.CreatedAt = nil
		e.ID = id
		assert.Equal(t, expected, e)
	})

	t.Run("UPDATE", func(t *testing.T) {
		expected := grupoValidoModificado()
		expected.ID = id
		err = db.Update(ctx, conn, expected)
		require.Nil(t, err)

		leido, err := db.ReadOne(ctx, conn, usuariosgrupos.ReadOneReq{
			Comitente: 1,
			ID:        id,
		})
		require.Nil(t, err)
		assert.NotNil(t, leido.UpdatedAt)
		assert.NotNil(t, leido.CreatedAt)
		leido.CreatedAt = nil
		leido.UpdatedAt = nil
		assert.Equal(t, expected, leido)
	})
	t.Run("DELETE", func(t *testing.T) {
		err := db.Delete(ctx, conn, usuariosgrupos.ReadOneReq{
			Comitente: 1,
			ID:        id,
		})
		require.Nil(t, err)

		_, err = db.ReadOne(ctx, conn, usuariosgrupos.ReadOneReq{
			Comitente: 1,
			ID:        id,
		})
		require.NotNil(t, err)
	})

}

func grupoValido() usuariosgrupos.Grupo {
	return usuariosgrupos.Grupo{
		Comitente: 1,
		Nombre:    "Nombre",
		Detalle:   "Detalle",
		Usuarios:  []string{"user1", "user2"},
	}
}

func grupoValidoModificado() usuariosgrupos.Grupo {
	return usuariosgrupos.Grupo{
		Comitente: 1,
		Nombre:    "Nombre2",
		Detalle:   "Detalle2",
		Usuarios:  []string{"user3"},
	}
}
