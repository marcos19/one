package handler

import (
	"context"
	"sort"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/usuariosgrupos"
	"bitbucket.org/marcos19/one/pkg/usuariosgrupos/internal/db"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

// Handler es el encargado de recibir los http requests y emitir los responses.
type Handler struct {
	conn *pgxpool.Pool
}

// NewHandler incializa un Handler.
func NewHandler(conn *pgxpool.Pool) (h *Handler, err error) {

	// Valido
	if conn == nil {
		return h, errors.New("Conn no puede ser nil")
	}

	h = &Handler{
		conn: conn,
	}

	return
}

func (h *Handler) Create(ctx context.Context, req usuariosgrupos.Grupo) (out int, err error) {

	// Valido
	if req.Nombre == "" {
		return out, deferror.Validation("Debe ingresar un nombre para el grupo")
	}
	if len(req.Usuarios) == 0 {
		return out, errors.Errorf("no se ingresó usuario")
	}
	sort.Strings(req.Usuarios)

	return db.Create(ctx, h.conn, req)

	// h.eventbus.CambióCuentasGrupos("creó", *req)

}

func (h *Handler) Update(ctx context.Context, req usuariosgrupos.Grupo) (err error) {

	// Valido
	if req.Nombre == "" {
		return deferror.Validation("Debe ingresar un nombre para el grupo")
	}
	if len(req.Usuarios) == 0 {
		return deferror.Validation("Debe ingresar algún usuario")
	}

	sort.Strings(req.Usuarios)

	return db.Update(ctx, h.conn, req)
}

func (h *Handler) ReadOne(ctx context.Context, req usuariosgrupos.ReadOneReq) (out usuariosgrupos.Grupo, err error) {

	return db.ReadOne(ctx, h.conn, req)
}

func (h *Handler) ReadMany(ctx context.Context, req usuariosgrupos.ReadManyReq) (out []usuariosgrupos.Grupo, err error) {
	return db.ReadMany(ctx, h.conn, req)
}

func (h *Handler) Delete(ctx context.Context, req usuariosgrupos.ReadOneReq) (err error) {
	return db.Delete(ctx, h.conn, req)
}
