package usuariosgrupos

import "context"

type ReaderOne interface {
	ReadOne(context.Context, ReadOneReq) (Grupo, error)
}
type ReaderMany interface {
	ReadMany(context.Context, ReadManyReq) ([]Grupo, error)
}
type Creater interface {
	Create(ctx context.Context, c Grupo) (id int, err error)
}
type Updater interface {
	Update(ctx context.Context, c Grupo) (err error)
}
type Deleter interface {
	Delete(ctx context.Context, r ReadOneReq) (err error)
}
type ReadOneReq struct {
	Comitente int
	ID        int `json:",string"`
}

type ReadManyReq struct {
	Comitente int
}
