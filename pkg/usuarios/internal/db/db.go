package db

import (
	"context"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/usuarios"
	uuid "github.com/jackc/pgtype/ext/gofrs-uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

func ReadOne(ctx context.Context, conn *pgxpool.Pool, id string) (u usuarios.Usuario, err error) {
	query := `
SELECT id, email, nombre, apellido, dni, pie_mail, comitentes, hash, 
	blanquear_proximo_ingreso, estado, ultima_actualizacion_contraseña,
	administrador, inactivo, created_at, updated_at
FROM usuarios
WHERE id = $1
;`

	err = conn.QueryRow(ctx, query, id).Scan(
		&u.ID,
		&u.Email,
		&u.Nombre,
		&u.Apellido,
		&u.DNI,
		&u.PieMail,
		&u.Comitentes,
		&u.Hash,
		&u.BlanquearProximoIngreso,
		&u.Estado,
		&u.UltimaActualizacionContraseña,
		&u.Administrador,
		&u.Inactivo,
		&u.CreatedAt,
		&u.UpdatedAt,
	)
	if err != nil {
		return u, deferror.DB(err, "querying row")
	}

	return
}

func Create(ctx context.Context, conn *pgxpool.Pool, u usuarios.Usuario) (err error) {
	query := `
INSERT INTO usuarios (
	id, email, nombre, apellido, dni, pie_mail, comitentes, hash, 
	blanquear_proximo_ingreso, estado, ultima_actualizacion_contraseña,
	administrador, inactivo, created_at
) 
VALUES (
	$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,now()
);`

	_, err = conn.Exec(ctx, query,
		u.ID,
		u.Email,
		u.Nombre,
		u.Apellido,
		u.DNI,
		u.PieMail,
		u.Comitentes,
		u.Hash,
		u.BlanquearProximoIngreso,
		u.Estado,
		u.UltimaActualizacionContraseña,
		u.Administrador,
		u.Inactivo,
	)
	if err != nil {
		return deferror.DB(err, "insertando")
	}

	return
}

func ReadMany(ctx context.Context, conn *pgxpool.Pool, req usuarios.ReadManyReq) (out []usuarios.Usuario, err error) {

	query := `SELECT id, email, nombre, apellido, inactivo, created_at, updated_at
FROM usuarios
WHERE comitentes @> ARRAY[$1::INT8]`

	rows, err := conn.Query(ctx, query, req.Comitente)
	if err != nil {
		return out, errors.Wrap(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		r := usuarios.Usuario{}
		err = rows.Scan(&r.ID, &r.Email, &r.Nombre, &r.Apellido, &r.Inactivo, &r.CreatedAt, &r.UpdatedAt)
		if err != nil {
			return out, deferror.DB(err, "scanning")
		}
		out = append(out, r)
	}
	return
}

func Update(ctx context.Context, conn *pgxpool.Pool, req usuarios.Usuario) error {
	query := `UPDATE usuarios
SET nombre=$2, apellido=$3, email=$4, dni=$5, pie_mail=$6, administrador=$7, 
	inactivo=$8, updated_at=now()
WHERE id=$1`
	//AND comitentes @> ARRAY[%v]
	res, err := conn.Exec(ctx, query, req.ID, req.Nombre, req.Apellido,
		req.Email, req.DNI, req.PieMail, req.Administrador, req.Inactivo,
	)
	if err != nil {
		return errors.Wrap(err, "updating")
	}
	if res.RowsAffected() == 0 {
		return errors.Errorf("no se modificó ningún registro")
	}

	return nil
}

type CambiarPasswordReq struct {
	ID             string
	Hash           string
	BlanquearLuego bool
}

func CambiarPassword(ctx context.Context, conn *pgxpool.Pool, req CambiarPasswordReq) error {

	query := `UPDATE usuarios
SET hash=$2, ultima_actualizacion_contraseña=now(),
blanquear_proximo_ingreso=false
WHERE id=$1;`

	res, err := conn.Exec(ctx, query, req.ID, req.Hash)
	if err != nil {
		return errors.Wrap(err, "updating")
	}
	if res.RowsAffected() == 0 {
		return errors.Errorf("no se modificó ningún registro")
	}

	return nil
}

func CreateUserConfirmation(ctx context.Context, conn *pgxpool.Pool, req usuarios.Confirmacion) (err error) {

	tx, err := conn.Begin(ctx)
	if err != nil {
		return deferror.DB(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)

	err = createUserConfirmationTx(ctx, tx, req)
	if err != nil {
		return err
	}

	err = tx.Commit(ctx)
	if err != nil {
		return deferror.DB(err, "confirmando transacción")
	}

	return
}

func createUserConfirmationTx(ctx context.Context, tx pgx.Tx, req usuarios.Confirmacion) (err error) {
	query := `INSERT INTO usuarios_confirmaciones (id, user_id, created_at, motivo)
		VALUES (
		$1,$2,$3,$4
	)
	`
	_, err = tx.Exec(ctx, query, req.ID, req.UserID, time.Now(), req.Motivo)
	if err != nil {
		return deferror.DB(err, "inserting")
	}
	return
}

func readUserConfirmation(ctx context.Context, conn *pgxpool.Pool, id uuid.UUID, motivo string) (out usuarios.Confirmacion, err error) {

	query := `SELECT id, user_id, created_at, motivo, confirmada, fecha_confirmacion 
	FROM usuarios_confirmaciones 
	WHERE id=$1 AND motivo=$2`

	err = conn.QueryRow(ctx, query, id, motivo).Scan(out.ID, out.UserID, out.CreatedAt, out.Motivo, out.Confirmada, out.FechaConfirmacion)
	if err != nil {
		return out, errors.Wrap(err, "querying/scanning")
	}
	return
}

func confirmarUsuario(ctx context.Context, conn *pgxpool.Pool, conf usuarios.Confirmacion) (err error) {

	tx, err := conn.Begin(ctx)
	if err != nil {
		return deferror.DB(err, "inicando transacción")
	}
	defer tx.Rollback(ctx)

	// Modifico la confirmación
	query := `UPDATE usuarios_confirmaciones SET confirmada=$1, fecha_confirmacion=$2 
	WHERE id = $3`

	_, err = tx.Exec(ctx, query, true, time.Now())
	if err != nil {
		return errors.Wrap(err, "updating")
	}

	// Modifico estado en Usuario
	_, err = tx.Exec(ctx, "UPDATE usuarios SET estado=$1 WHERE id = $2", usuarios.EstadoConfirmado, conf.UserID)
	if err != nil {
		return errors.Wrap(err, "updating")
	}

	err = tx.Commit(ctx)
	if err != nil {
		return deferror.DB(err, "confirmando transacción")
	}
	return
}
