package dbtest

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/migrations/ddl"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

func Prepare(ctx context.Context, conn *pgxpool.Pool) error {

	{ // Borro base de datos
		q := `DROP DATABASE IF EXISTS test_usuarios;`
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "borro base de datos")
		}
	}
	{ // Creo base de datos
		q := `CREATE DATABASE test_usuarios; USE test_usuarios;`
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando base de datos")
		}
	}

	{ // Creo table de usuarios
		q := ddl.Usuarios().SQL
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla usuarios")
		}
	}

	return nil
}
