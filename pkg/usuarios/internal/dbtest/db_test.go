package dbtest

import (
	"context"
	"testing"
	"time"

	"bitbucket.org/marcos19/one/pkg/database"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"bitbucket.org/marcos19/one/pkg/usuarios"
	"bitbucket.org/marcos19/one/pkg/usuarios/internal/db"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDB(t *testing.T) {
	ctx := context.Background()

	cn := database.TestDBConnectionString()

	cfg, err := pgxpool.ParseConfig(cn)
	require.Nil(t, err)
	conn, err := pgxpool.ConnectConfig(context.Background(), cfg)
	require.Nil(t, err)

	// Preparo base de datos
	err = Prepare(ctx, conn)
	require.Nil(t, err)

	t.Run("CREATE", func(t *testing.T) {
		err = db.Create(ctx, conn, usuarioValido())
		require.Nil(t, err)
	})

	t.Run("READ", func(t *testing.T) {
		expected := usuarioValido()
		leido, err := db.ReadOne(ctx, conn, expected.ID)
		require.Nil(t, err)
		assert.NotNil(t, 0, leido.CreatedAt)
		leido.UltimaActualizacionContraseña = leido.UltimaActualizacionContraseña.UTC()
		expected.UltimaActualizacionContraseña = expected.UltimaActualizacionContraseña.UTC()
		leido.CreatedAt = nil
		leido.UpdatedAt = nil
		assert.Nil(t, leido.UpdatedAt)
		assert.Equal(t, expected, leido)
	})

	t.Run("UPDATE", func(t *testing.T) {
		mod := usuarioTotalmenteModificado()
		err = db.Update(ctx, conn, mod)
		require.Nil(t, err)

		leido, err := db.ReadOne(ctx, conn, mod.ID)
		require.Nil(t, err)

		leido.UltimaActualizacionContraseña = leido.UltimaActualizacionContraseña.UTC()
		mod.UltimaActualizacionContraseña = mod.UltimaActualizacionContraseña.UTC()
		assert.NotEqual(t, mod.UpdatedAt, leido.UpdatedAt, "expected: %v, got: %v", mod.UpdatedAt, leido.UpdatedAt)
		leido.CreatedAt = nil
		leido.UpdatedAt = nil
		assert.Equal(t, usuarioModificadoEsperado(), leido)
	})
}

func usuarioValido() usuarios.Usuario {
	email := "name@something.com"
	hoy := time.Date(2022, 12, 12, 0, 0, 0, 0, time.UTC)
	return usuarios.Usuario{
		ID:                            "ID",
		Nombre:                        "Nombre",
		Email:                         &email,
		Apellido:                      "Apellido",
		DNI:                           1111,
		PieMail:                       "Pie mail",
		Comitentes:                    tipos.GrupoInts{1, 2, 3},
		Hash:                          "hash",
		BlanquearProximoIngreso:       true,
		Estado:                        "Estado",
		UltimaActualizacionContraseña: hoy,
		Administrador:                 true,
		Inactivo:                      true,
	}
}

// Tiene los cambios que deben ser persistidos
func usuarioModificadoEsperado() usuarios.Usuario {
	email := "name2@something.com"
	u := usuarioValido()
	u.ID = "ID"
	u.Nombre = "Nombre2"
	u.Email = &email
	u.Apellido = "Apellido2"
	u.DNI = 11112
	u.PieMail = "Pie mail2"
	u.Administrador = false
	u.Inactivo = false
	return u
}

func usuarioTotalmenteModificado() usuarios.Usuario {
	hoy := time.Date(2023, 12, 12, 0, 0, 0, 0, time.UTC)
	u := usuarioModificadoEsperado()
	u.Comitentes = tipos.GrupoInts{1, 2, 5}
	u.Hash = "hash2"
	u.BlanquearProximoIngreso = false
	u.Estado = "Estado2"
	u.UltimaActualizacionContraseña = hoy

	return u
}
