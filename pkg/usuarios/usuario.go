package usuarios

import (
	"context"
	"time"

	"bitbucket.org/marcos19/one/pkg/tipos"
)

const (
	EstadoPendienteConfirmación = "Pendiente de confirmación"
	EstadoConfirmado            = "Confirmado"
)

// Usuario es cada usuario que ingresará al sistema
type Usuario struct {
	ID                            string
	Email                         *string
	Nombre                        string
	Apellido                      string
	DNI                           int
	PieMail                       string
	Comitentes                    tipos.GrupoInts
	Hash                          string
	BlanquearProximoIngreso       bool
	Estado                        string
	UltimaActualizacionContraseña time.Time

	Administrador bool
	Inactivo      bool

	CreatedAt *time.Time
	UpdatedAt *time.Time
}

type userType string

var userKey = userType("user")

func SetUserInContext(ctx context.Context, user string) context.Context {
	return context.WithValue(ctx, userKey, user)
}

func GetUserFromContext(ctx context.Context) *string {
	val, ok := ctx.Value(userKey).(string)
	if !ok {
		return nil
	}
	return &val
}
