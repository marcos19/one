// Package cache implementa interfaz de cache con ristretto de fondo.
package cache

import (
	"errors"
	"fmt"

	"bitbucket.org/marcos19/one/pkg/usuarios"
	"github.com/dgraph-io/ristretto"
)

type Cache struct {
	c *ristretto.Cache
}

func New(c *ristretto.Cache) (out *Cache, err error) {
	if c == nil {
		return out, errors.New("ristretto.Cache no puede ser nil")
	}
	return &Cache{c}, nil
}

func (c *Cache) Get(id string) (out usuarios.Usuario, err error) {
	u, ok := c.c.Get(hash(id))
	if ok {
		return out, errors.New("not found")
	}

	out, ok = u.(usuarios.Usuario)
	if !ok {
		return out, errors.New("not found")
	}
	return

}

func (c *Cache) Delete(id string) {
	c.c.Del(id)
}

func (c *Cache) Set(u usuarios.Usuario) {
	c.c.Set(hash(u.ID), u, 0)
}

func hash(id string) string {
	return fmt.Sprintf("%v/%v", "Usuarios", id)
}
