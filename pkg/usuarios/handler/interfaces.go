package handler

import "bitbucket.org/marcos19/one/pkg/usuarios"

type Cache interface {
	Get(id string) (usuarios.Usuario, error)
	Set(usuarios.Usuario)
	Delete(id string)
}
