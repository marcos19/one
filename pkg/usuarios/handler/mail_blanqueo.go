package handler

import (
	"bytes"
	"html/template"

	"github.com/cockroachdb/errors"
	"github.com/go-mail/mail"
)

type MailBlanqueo struct {
	From string
	To   string

	NombreUsuario    string
	LinkConfirmacion string
}

// Generar crea el mail para enviar con el mail.Dialer
func (m MailBlanqueo) Generar() (msg *mail.Message, err error) {

	if m.From == "" {
		return msg, errors.New("no se configuró el campo MailFrom")
	}
	if m.To == "" {
		return msg, errors.New("no se configuró el campo MailTo")
	}
	msg = mail.NewMessage()
	msg.SetHeader("From", m.From)
	msg.SetHeader("To", m.To)

	msg.SetHeader("Subject", "Blanqueo de contraseña")

	body, err := m.bodyString()
	if err != nil {
		return msg, errors.Wrap(err, "generando mail de blanqueo de contraseña")
	}
	msg.SetBody("text/html", body)

	return
}

// BodyString devuelve el string que se pone en el body del mail
func (m MailBlanqueo) bodyString() (html string, err error) {

	if m.NombreUsuario == "" {
		return html, errors.New("no se definió nombre de usuario")
	}
	if m.LinkConfirmacion == "" {
		return html, errors.New("no se definió link de confirmación")
	}

	body := `
	<!DOCTYPE html>
<html>

<head>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet">
    <style>
        body {
            background-color: lightblue;
        }

        * {
            font-family: "Roboto", sans-serif;
            font-weight: 300;
        }

        div#main {
            max-width: 700px;
            background-color: white;
            margin: auto;
            padding: 80px;
            height: 95%;
        }
		p#rechazar{
			padding-top: 4em;
			font-size: 80%;
			text-align: center;
		}
    </style>
</head>

<body>
    <div id='main'>
        <p> Hola {{ .Nombre }}</p>
        <p>
            Para finalizar con el blanqueo de contraseña, haz clic <a href="{{.LinkConfirmacion}}">AQUÍ</a>.
        </p>

        <p id="rechazar">
            Si tú no has realizado la solicitud de blanqueo de contraseña haz clic   aquí.
        </p>
    </div>

    <br>
</body>
</html>
	`

	tpl, err := template.New("conf").Parse(body)
	if err != nil {
		return body, errors.Wrap(err, "parseando template de confirmación de usuario")
	}

	datos := struct {
		Nombre           string
		LinkConfirmacion string
	}{
		Nombre:           m.NombreUsuario,
		LinkConfirmacion: m.LinkConfirmacion,
	}

	out := &bytes.Buffer{}
	err = tpl.Execute(out, datos)
	if err != nil {
		return html, errors.Wrap(err, "ejecutando template")
	}

	html = out.String()

	return
}
