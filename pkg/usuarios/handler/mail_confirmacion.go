package handler

import (
	"bytes"
	"html/template"

	"github.com/cockroachdb/errors"
	"github.com/go-mail/mail"
)

// MailConfirmacion es el mail que se envía una vez que se dio de alta
// un usuario para corroborar que el mail es correcto.
type MailConfirmacion struct {
	From string
	To   string

	NombreUsuario    string
	LinkConfirmacion string
}

func (m MailConfirmacion) Generar() (msg *mail.Message, err error) {

	// Valido
	if m.From == "" {
		return msg, errors.New("no se configuró el campo MailFrom")
	}
	if m.To == "" {
		return msg, errors.New("no se configuró el campo MailTo")
	}

	// Creo el mail
	msg = mail.NewMessage()
	msg.SetHeader("From", m.From)
	msg.SetHeader("To", m.To)

	msg.SetHeader("Subject", "Blanqueo de contraseña")

	html, err := m.bodyString()
	if err != nil {
		return msg, errors.Wrap(err, "generando mail confirmación de usuario")
	}
	msg.SetBody("text/html", html)

	return

}

func (m MailConfirmacion) bodyString() (html string, err error) {
	body := `
 <!DOCTYPE html>
<html>

<head>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet">
    <style>
        body {
            background-color: lightblue;
        }

        * {
            font-family: "Roboto", sans-serif;
            font-weight: 300;
        }

        div#main {
            max-width: 700px;
            background-color: white;
            margin: auto;
            padding: 80px;
            height: 95%;
        }

        p#rechazar {
			padding-top: 4em;
			font-size: 80%;
			text-align: center;
		}
    </style>
</head>

<body>
    <div id='main'>
        <p> Hola {{ .Nombre }} gracias por sumarte a ADA!</p>
        <p>
            Para confirmar tu alta como usuario, haz clic <a href='{{.LinkConfirmacion}}'>AQUÍ</a>.
        </p>

        <p id="rechazar">
            Si tú no has realizado la solicitud haz clic aquí.
        </p>
    </div>

    <br>
</body>
</html>
	`
	tpl, err := template.New("conf").Parse(body)
	if err != nil {
		return html, errors.Wrap(err, "parseando template")
	}

	datos := struct {
		Nombre           string
		LinkConfirmacion string
	}{
		Nombre:           m.NombreUsuario,
		LinkConfirmacion: m.LinkConfirmacion,
	}

	out := &bytes.Buffer{}
	err = tpl.Execute(out, datos)
	if err != nil {
		return html, errors.Wrap(err, "ejecutando template")
	}

	html = out.String()
	return
}
