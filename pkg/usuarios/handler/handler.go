package handler

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/usuarios"
	"bitbucket.org/marcos19/one/pkg/usuarios/internal/db"
	"github.com/crosslogic/niler"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

type Handler struct {
	conn        *pgxpool.Pool
	cache       Cache
	PassValidez time.Duration

	// dialer                *mail.Dialer
	// MailFrom              string // Sweet
	// PassMaxLength         int
	// PassMinLength         int
	// LinkConfirmarBlanqueo string
}

func NewHandler(conn *pgxpool.Pool, cache Cache) (*Handler, error) {

	if niler.IsNil(conn) {
		return nil, errors.Errorf("conn no puede ser nil")
	}
	if niler.IsNil(cache) {
		return nil, errors.Errorf("cache no puede ser nil")
	}

	// Datos por defecto PASSWORD
	// h.PassMaxLength = 40
	// h.PassMinLength = 6
	return &Handler{
		conn:        conn,
		cache:       cache,
		PassValidez: 30 * time.Hour * 24,
	}, nil
}

// Verifica que:
// 1) usuario y contraseña coincidan,
// 2) usuario esté confirmado,
// 3) no tenga puesta señal de BlanquearProximoIngreso
// 4) password no tenga fecha vencida,
//
// Si alguno de los controles anteriores falla, devuelve un error distinto.
func (h *Handler) Login(ctx context.Context, req usuarios.LoginReq) (err error) {
	usuario := usuarios.Usuario{}

	// Busco user
	u, err := h.ReadOne(ctx, req.UserID)
	if err != nil {
		return usuarios.ErrContraseñaIncorrecta{}
	}

	// Coincide password?
	if u.Hash != calcularHash(req.Pass) {
		return usuarios.ErrContraseñaIncorrecta{}
	}

	// Está confirmado
	if u.Estado == usuarios.EstadoPendienteConfirmación {
		return usuarios.ErrUsuarioNoConfirmado{}
	}

	// Tiene señal de cambiar proximo ingreso?
	if u.BlanquearProximoIngreso {
		return usuarios.ErrCorrespondeCambiar{}
	}

	// La password tiene la fecha vencida?
	vencimiento := usuario.UltimaActualizacionContraseña.Add(h.PassValidez)
	if vencimiento.After(time.Now()) {
		return nil
	}

	return nil
}

func (h *Handler) IsAdmin(ctx context.Context, id string) (bool, error) {
	u, err := h.ReadOne(ctx, id)
	if err != nil {
		return false, errors.Wrap(err, "buscando usuario")
	}
	return u.Administrador, nil
}

func (h *Handler) Create(ctx context.Context, u usuarios.Usuario) (err error) {

	// Valido TODO
	if u.ID == "" {
		return deferror.Validation("no se ingresó ID")
	}
	if u.Nombre == "" {
		return deferror.Validation("no se ingresó nombre")
	}
	if u.Apellido == "" {
		return deferror.Validation("no se ingresó apellido")
	}
	if u.Email != nil {
		if *u.Email == "" {
			u.Email = nil
		}
	}

	// Por defecto user == contraseña
	u.Hash = calcularHash(u.ID)
	u.BlanquearProximoIngreso = true

	// Inserto
	err = db.Create(ctx, h.conn, u)
	if err != nil {
		return err
	}

	// Fijo en cache
	h.cache.Set(u)

	return
}

func (h *Handler) ReadOne(ctx context.Context, id string) (out usuarios.Usuario, err error) {

	// Está en cache?
	out, err = h.cache.Get(id)
	if niler.IsNil(err) {
		return
	}

	// Busco en DB
	out, err = db.ReadOne(ctx, h.conn, id)
	if err != nil {
		return out, err
	}

	// Fijo cache
	h.cache.Set(out)

	return
}

func (h *Handler) ReadMany(ctx context.Context, req usuarios.ReadManyReq) (out []usuarios.Usuario, err error) {

	if req.Comitente == 0 {
		return out, errors.Errorf("no se ingresó ID de comitente")
	}

	// Busco en DB
	return db.ReadMany(ctx, h.conn, req)

}

func (h *Handler) Update(ctx context.Context, u usuarios.Usuario) (err error) {

	if u.ID == "" {
		return errors.Errorf("no se ingresó ID de usuario")
	}
	if u.Nombre == "" {
		return errors.Errorf("no se ingresó nombre de usuario")
	}

	// Borro de cache
	h.cache.Delete(u.ID)

	// Busco en DB
	return db.Update(ctx, h.conn, u)

}

func (h *Handler) CambiarPassword(ctx context.Context, req usuarios.CambiarPasswordReq) (err error) {
	if req.UserID == "" {
		return errors.Errorf("no se ingresó nombre de usuario")
	}
	if req.Anterior == "" {
		return errors.Errorf("no se ingresó contraseña anterior")
	}
	if req.Pass == "" {
		return errors.Errorf("no se ingresó contraseña (primera vez)")
	}
	if req.Pass2 == "" {
		return errors.Errorf("no se ingresó contraseña (segunda vez)")
	}
	usuario := usuarios.Usuario{}

	// Busco user
	usuario, err = h.ReadOne(ctx, req.UserID)
	if err != nil {
		return errors.Wrap(err, "buscando usuario")
	}

	// Coincide pass anterior?
	hashAnterior := calcularHash(req.Anterior)
	if usuario.Hash != hashAnterior {
		return usuarios.ErrContraseñaIncorrecta{}
	}

	// Coincide pass anterior?
	if req.Pass != req.Pass2 {
		return errors.Errorf("las contraseñas no coinciden")
	}

	// Todo ok, persisto
	nr := db.CambiarPasswordReq{}
	nr.ID = req.UserID
	nr.Hash = calcularHash(req.Pass)

	h.cache.Delete(usuario.ID)

	err = db.CambiarPassword(ctx, h.conn, nr)
	if err != nil {
		return err
	}
	return nil
}

// calcularHash genera un hash en base al string del password.
func calcularHash(password string) (hash string) {
	enBytes := sha256.Sum256([]byte(password))
	return hex.EncodeToString(enBytes[:])
}

func (h *Handler) ComitentesDisponibles(ctx context.Context, userID string) (out []usuarios.ComitentesDisponiblesResp, err error) {

	// Busco usuario
	user, err := h.ReadOne(ctx, userID)
	if err != nil {
		return out, deferror.DB(err, "determinando comitente de usuario")
	}

	// Busco comitentes
	query := fmt.Sprintf("SELECT id, nombre FROM comitentes WHERE id in %v ORDER BY nombre", user.Comitentes.ParaClausulaIn())
	rows, err := h.conn.Query(ctx, query)
	if err != nil {
		return out, errors.Wrap(err, "buscando comitentes")
	}
	defer rows.Close()

	// Escaneo
	for rows.Next() {
		c := usuarios.ComitentesDisponiblesResp{}
		err = rows.Scan(&c.ID, &c.Nombre)
		if err != nil {
			return nil, errors.Wrap(err, "escaneando comitentes")
		}
		out = append(out, c)
	}

	return
}
