package usuarios

import (
	"context"
)

type ReaderOne interface {
	ReadOne(ctx context.Context, id string) (Usuario, error)
}
type ReaderMany interface {
	ReadMany(context.Context, ReadManyReq) ([]Usuario, error)
}
type Creater interface {
	Create(context.Context, Usuario) error
}
type Updater interface {
	Update(context.Context, Usuario) error
}
type Patcher interface {
	CambiarPassword(context.Context, CambiarPasswordReq) error
}

type ComitentesGetter interface {
	ComitentesDisponibles(ctx context.Context, userID string) (out []ComitentesDisponiblesResp, err error)
}

type Loginer interface {
	Login(context.Context, LoginReq) error
}

type ReadManyReq struct {
	Comitente int
}

// type ReadManyResp struct {
// 	ID        int
// 	Email     string
// 	Nombre    string
// 	Apellido  string
// 	Inactivo  bool
// 	CreatedAt *time.Time
// 	UpdatedAt *time.Time
// }

type CambiarPasswordReq struct {
	UserID   string
	Anterior string
	Pass     string
	Pass2    string
}

type LoginReq struct {
	UserID string
	Pass   string
}
type ComitentesDisponiblesResp struct {
	ID     int `json:",string"`
	Nombre string
}
