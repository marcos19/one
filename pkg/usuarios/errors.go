package usuarios

type FrontendErr interface {
	// Usa el frontend para actuar en consecuencia
	Frontend() string

	// Human readable response
	Error() string
}

type ErrContraseñaIncorrecta struct{}

func (e ErrContraseñaIncorrecta) Error() string {
	return "usuario o contraseña incorrecta"
}

// En base a este string, el frontend sabe que hacer con el error
func (e ErrContraseñaIncorrecta) Frontend() string {
	return "credenciales inválidas"
}

type ErrUsuarioNoConfirmado struct{}

func (e ErrUsuarioNoConfirmado) Error() string {
	return "el usuario aún no ha sido confirmado"
}

// En base a este string, el frontend sabe que hacer con el error
func (e ErrUsuarioNoConfirmado) Frontend() string {
	return "Usuario no confirmado"
}

type ErrCorrespondeCambiar struct{}

func (e ErrCorrespondeCambiar) Error() string {
	return "corresponde cambiar contraseña"
}

// En base a este string, el frontend sabe que hacer con el error
func (e ErrCorrespondeCambiar) Frontend() string {
	return "corresponde cambiar contraseña"
}
