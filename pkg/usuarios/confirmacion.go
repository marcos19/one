package usuarios

import (
	"time"

	"github.com/gofrs/uuid"
)

const (
	MotivoCreacion = "Creación"
	MotivoBlanqueo = "Blanqueo"
)

type Confirmacion struct {
	ID                uuid.UUID
	UserID            string
	CreatedAt         time.Time
	Motivo            string
	Confirmada        bool
	FechaConfirmacion time.Time
}

// TableName devuelve el nombre de la tabla en la base de datos
func (u *Confirmacion) TableName() string {
	return "usuarios_confirmaciones"
}
