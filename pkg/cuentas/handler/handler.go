package handler

import (
	"context"
	"fmt"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/cuentas/internal/db"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/dgraph-io/ristretto"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Programa es el nombre que se identifica el programa de cuentas
const ProgramaNombre = "cuentas"

// Handler es el encargado de recibir los http requests y emitir los responses.
type Handler struct {
	conn  *pgxpool.Pool
	cache *ristretto.Cache

	// Si es true, omite el uso del cache
	UsarCache bool
}

type HandlerConfig struct {
	Conn  *pgxpool.Pool
	Cache *ristretto.Cache
}

// NewHandler incializa un Handler.
func NewHandler(config HandlerConfig) (h *Handler, err error) {

	// Valido
	if config.Conn == nil {
		return h, errors.New("Conn no puede ser nil")
	}
	if config.Cache == nil {
		return h, errors.New("Cache no puede ser nil")
	}

	h = &Handler{
		conn:  config.Conn,
		cache: config.Cache,
	}

	return
}

// HandleCreate persiste la cuenta ingresada en el JSON.
// (primero la valida).
func (h *Handler) Create(ctx context.Context, req cuentas.Cuenta) (id int, err error) {

	// Valido
	if req.Comitente == 0 {
		return id, deferror.Validation("no ingresó el ID de comitente ")
	}
	if req.Nombre == "" {
		return id, deferror.Validation("debe ingresar un nombre para la cuenta")
	}
	if req.TipoCuenta == cuentas.TipoCuentaContable("") {
		return id, deferror.Validation("debe ingresar el tipo de cuenta contable")
	}
	if req.PadreID == nil && req.TipoCuenta != cuentas.TipoCuentaAgrupadora {
		return id, deferror.Validation("Si la cuenta se define como imputable deberá elegir una cuenta no imputable que la englobe.\nPor ejemplo: Caja debe pertenecer a Activo")
	}

	{ // Existía?
		count := 0
		query := `SELECT COUNT(id) FROM cuentas WHERE comitente=$1 AND codigo=$2`
		err = h.conn.QueryRow(ctx, query, req.Comitente, req.Codigo).Scan(&count)
		if err != nil {
			return id, deferror.DB(err, "determinando si existía cuenta con ese código")
		}
		if count > 0 {
			return id, deferror.Validation("Ya existía una cuenta cón el código ingresado")
		}
	}

	// Todo OK persisto
	id, err = db.Create(ctx, h.conn, req)
	if err != nil {
		return id, deferror.DB(err, "creando cuenta")
	}

	return
}

// FuncionesContables devuelve la cuenta con el ID ingresado
func (h *Handler) FuncionesContables() (ff []cuentas.Funcion) {

	ff = []cuentas.Funcion{
		cuentas.BsUsoValorOrigen,
		cuentas.BsUsoAmortAcum,
		cuentas.MercaderiaPropiaEnDepositosPropios,
		cuentas.MercaderiaPropiaEnDepositosDeTerceros,
		cuentas.MercaderiaDeTercerosEnDepositosPropios,
		cuentas.Proveedores,
		cuentas.FacturasARecibir,
		cuentas.MercaderiaARecibir,
		cuentas.DeudoresPorVentas,
		cuentas.MercaderiaAFacturar,
		cuentas.MercaderiaAEntregar,
		cuentas.Ventas,
		cuentas.VentasACostear,
		cuentas.Costo,
		cuentas.IVACreditoFiscal,
		cuentas.IVADebitoFiscal,
		cuentas.MedioDePago,
		cuentas.Recpam,
		cuentas.CuentaBancaria,
		cuentas.CuentaBancariaConciliar,
	}

	return
}

func (h *Handler) ReadOne(ctx context.Context, req cuentas.ReadOneReq) (out cuentas.Cuenta, err error) {

	// Valido
	if req.ID == 0 {
		return out, deferror.Validation("Debe ingresar un ID para la cuenta")
	}
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}

	// Busco en cache
	val, estaba := h.cache.Get(hashInt(req))
	if estaba {
		out, ok := val.(cuentas.Cuenta)
		if ok && out.Comitente == req.Comitente {
			return out, nil
		}
	}

	// No estaba en cache
	out, err = db.ReadOne(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DBf(err, "buscando cuenta ID=%v", req.ID)
	}

	// Fijo en cache
	h.cache.Set(hashInt(req), out, 0)

	return
}

// TODO: eliminar esta funcion
func (h *Handler) PorFuncionContable(ctx context.Context, comitente int, f cuentas.Funcion) (out []cuentas.Cuenta, err error) {

	// Valido
	if comitente == 0 {
		return out, deferror.Validation("no se definió comitente")
	}
	if f == "" {
		return out, deferror.Validation("no se definió función a buscar")
	}

	// Busco
	out, err = db.ReadMany(ctx, h.conn, cuentas.ReadManyReq{Comitente: comitente, Funcion: f})
	if err != nil {
		return out, deferror.DBf(err, "buscando cuenta con función %v", f)
	}

	return
}

func (h *Handler) ReadMany(ctx context.Context, req cuentas.ReadManyReq) (out []cuentas.Cuenta, err error) {

	// Busco
	out, err = db.ReadMany(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando cuentas")
	}

	return
}

// HandleUpdate recibe un JSON con los datos modificados de la cuenta
// y persiste los cambios basados en el ID del objeto.
func (h *Handler) Update(ctx context.Context, req cuentas.Cuenta) (err error) {

	// Valido
	if req.ID == 0 {
		return deferror.Validation("no se ingresó ID de cuenta a modificar")
	}
	if req.Nombre == "" {
		return deferror.Validation("Debe ingresar un nombre para la cuenta")
	}
	if req.TipoCuenta == cuentas.TipoCuentaContable("") {
		return deferror.Validation("Debe ingresar el tipo de cuenta contable")
	}
	if req.PadreID == nil && req.TipoCuenta != cuentas.TipoCuentaAgrupadora {
		return deferror.Validation("Si la cuenta se define como imputable deberá elegir una cuenta no imputable que la englobe.\nPor ejemplo: Caja debe pertenecer a Activo")
	}

	{ // Existía?
		count := 0
		query := `SELECT COUNT(id) FROM cuentas WHERE comitente=$1 AND codigo=$2 AND id<>$3`
		err = h.conn.QueryRow(ctx, query, req.Comitente, req.Codigo, req.ID).Scan(&count)
		if err != nil {
			return deferror.DB(err, "determinando si existía cuenta con ese código")
		}
		if count > 0 {
			return deferror.Validation("Ya existía una cuenta cón el código ingresado")
		}
	}

	// Borro del cache
	h.cache.Del(hashInt(cuentas.ReadOneReq{Comitente: req.Comitente, ID: req.ID}))

	// Persisto
	err = db.Update(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "modificando cuenta")
	}

	return
}

func (h *Handler) Delete(ctx context.Context, req cuentas.DeleteReq) (err error) {

	// Chequeo que no haya sido usado nunca en la contabilidad
	// TODO
	// if h.autorizador.EstaUsadaCuenta(req.ID) == true {
	// 	errors.HTTP(w, errors.E(op, "no se puede borrar la cuenta porque la misma ya ha sido usada"), errors.Validacion)
	// 	return
	// }

	h.cache.Del(hashInt(cuentas.ReadOneReq(req)))

	// Borro
	err = db.Delete(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "eliminando cuenta")
	}

	return
}

func hashInt(req cuentas.ReadOneReq) string {
	return fmt.Sprintf("%v/%v/%v", "Cuenta", req.Comitente, req.ID)
}
