package dbtest

import (
	"context"
	"testing"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/cuentas/internal/db"
	"bitbucket.org/marcos19/one/pkg/database"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDB(t *testing.T) {
	ctx := context.Background()

	cn := database.TestDBConnectionString()

	cfg, err := pgxpool.ParseConfig(cn)
	require.Nil(t, err)
	conn, err := pgxpool.ConnectConfig(context.Background(), cfg)
	require.Nil(t, err)

	// Preparo base de datos
	err = Prepare(ctx, conn)
	require.Nil(t, err)

	id := 0
	t.Run("CREATE", func(t *testing.T) {
		id, err = db.Create(ctx, conn, ctaValida())
		require.Nil(t, err)
		assert.NotEqual(t, 0, id)
	})

	t.Run("READ", func(t *testing.T) {
		expected := ctaValida()
		expected.ID = id
		e, err := db.ReadOne(ctx, conn, cuentas.ReadOneReq{
			Comitente: 1,
			ID:        id,
		})
		require.Nil(t, err)
		assert.NotEqual(t, 0, e.ID)
		assert.NotNil(t, 0, e.CreatedAt)
		assert.Nil(t, e.UpdatedAt)
		e.CreatedAt = nil
		e.ID = id
		assert.Equal(t, expected, e)
	})

	t.Run("UPDATE", func(t *testing.T) {
		expected := ctaValidaModificada()
		expected.ID = id
		err = db.Update(ctx, conn, expected)
		require.Nil(t, err)

		leido, err := db.ReadOne(ctx, conn, cuentas.ReadOneReq{
			Comitente: 1,
			ID:        id,
		})
		require.Nil(t, err)
		assert.NotNil(t, leido.UpdatedAt)
		assert.NotNil(t, leido.CreatedAt)
		leido.CreatedAt = nil
		leido.UpdatedAt = nil
		assert.Equal(t, expected, leido)
	})
	t.Run("DELETE sin partida", func(t *testing.T) {
		err := db.Delete(ctx, conn, cuentas.DeleteReq{
			Comitente: 1,
			ID:        id,
		})
		require.Nil(t, err)

		_, err = db.ReadOne(ctx, conn, cuentas.ReadOneReq{
			Comitente: 1,
			ID:        id,
		})
		require.NotNil(t, err)
	})

	t.Run("DELETE con partida", func(t *testing.T) {

		// Creo nueva
		idConMov, err := db.Create(ctx, conn, ctaValida())
		require.Nil(t, err)
		assert.NotEqual(t, 0, id)

		// Borro partidas anteriores
		_, err = conn.Exec(ctx, "DELETE FROM partidas;")
		require.Nil(t, err)

		// Le agrego movimiento
		_, err = conn.Exec(ctx, "INSERT INTO partidas (id, comitente, cuenta) VALUES (1,1,$1)", idConMov)
		require.Nil(t, err)

		// No debe dejar borrar
		err = db.Delete(ctx, conn, cuentas.DeleteReq{
			Comitente: 1,
			ID:        idConMov,
		})
		require.NotNil(t, err)

		_, err = db.ReadOne(ctx, conn, cuentas.ReadOneReq{
			Comitente: 1,
			ID:        idConMov,
		})

		// Tiene que seguir estando
		require.Nil(t, err)

	})
}

func ctaValida() cuentas.Cuenta {
	tipoUnicidad := 3
	padreID := 43
	conceptoProd := 412
	rubroBsUso := 9
	return cuentas.Cuenta{
		Comitente:                  1,
		Codigo:                     "Codigo",
		Orden:                      1,
		Nombre:                     "Nombre",
		AperturaPersona:            true,
		AperturaPersonaRestrictiva: true,
		AperturaProducto:           true,
		AperturaCaja:               true,
		AperturaDeposito:           true,
		AperturaContrato:           true,
		AperturaUnicidad:           true,
		AperturaCentro:             true,
		TipoUnicidad:               &tipoUnicidad,
		CentrosPermitidos:          tipos.GrupoInts{1, 2, 7},
		TipoCuenta:                 "TipoCuenta",
		AjustaPorInflacion:         true,
		Funcion:                    "Funcion",
		MedioDePago:                true,
		Alias:                      "Alias",
		Monetaria:                  true,
		MonedaExtranjera:           true,
		EsSistemica:                true,
		UsaAplicaciones:            true,
		PadreID:                    &padreID,
		Descripcion:                "Descripcion",
		ConceptoIvaCompras:         "ConceptoIvaCompras",
		CentrosGrupos:              tipos.GrupoInts{4, 5, 6},
		ConceptoProducto:           &conceptoProd,
		RubroBsUso:                 &rubroBsUso,
	}
}

func ctaValidaModificada() cuentas.Cuenta {
	tipoUnicidad := 4
	padreID := 44
	conceptoProd := 413
	rubroBsUso := 10
	return cuentas.Cuenta{
		Comitente:                  1,
		Codigo:                     "Codigo2",
		Orden:                      2,
		Nombre:                     "Nombr2",
		AperturaPersona:            false,
		AperturaPersonaRestrictiva: false,
		AperturaProducto:           false,
		AperturaCaja:               false,
		AperturaDeposito:           false,
		AperturaContrato:           false,
		AperturaUnicidad:           false,
		AperturaCentro:             false,
		TipoUnicidad:               &tipoUnicidad,
		CentrosPermitidos:          tipos.GrupoInts{2},
		TipoCuenta:                 "TipoCuenta2",
		AjustaPorInflacion:         false,
		Funcion:                    "Funcion2",
		MedioDePago:                false,
		Alias:                      "Alias2",
		Monetaria:                  false,
		MonedaExtranjera:           false,
		EsSistemica:                false,
		UsaAplicaciones:            false,
		PadreID:                    &padreID,
		Descripcion:                "Descripcion2",
		ConceptoIvaCompras:         "ConceptoIvaCompras2",
		CentrosGrupos:              tipos.GrupoInts{4, 590945, 6},
		ConceptoProducto:           &conceptoProd,
		RubroBsUso:                 &rubroBsUso,
	}
}
