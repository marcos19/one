package db

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/jackc/pgx/v4/pgxpool"
)

func Create(ctx context.Context, conn *pgxpool.Pool, v cuentas.Cuenta) (out int, err error) {

	if v.Comitente == 0 {
		return out, errors.New("no se ingresó ID de cuenta")
	}
	if v.Codigo == "" {
		return out, deferror.Validation("no se ingresó código de cuenta")
	}

	v.CreatedAt = new(time.Time)
	*v.CreatedAt = time.Now()

	query := `INSERT INTO cuentas (
comitente,
codigo,
orden,
nombre,
apertura_persona,
apertura_persona_restrictiva,
apertura_producto,
apertura_caja,
apertura_deposito,
apertura_contrato,
apertura_unicidad,
tipo_unicidad,
apertura_centro,
centros_permitidos,
tipo_cuenta,
ajusta_por_inflacion,
funcion,
medio_de_pago,
alias,
monetaria, 
moneda_extranjera,
es_sistemica,
usa_aplicaciones,
padre_id,
descripcion,
concepto_iva_compras,
created_at,
centros_grupos,
concepto_producto,
rubro_bsuso
) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$30)
RETURNING id
	`

	err = conn.QueryRow(ctx, query,
		&v.Comitente,
		&v.Codigo,
		&v.Orden,
		&v.Nombre,
		&v.AperturaPersona,
		&v.AperturaPersonaRestrictiva,
		&v.AperturaProducto,
		&v.AperturaCaja,
		&v.AperturaDeposito,
		&v.AperturaContrato,
		&v.AperturaUnicidad,
		&v.TipoUnicidad,
		&v.AperturaCentro,
		&v.CentrosPermitidos,
		&v.TipoCuenta,
		&v.AjustaPorInflacion,
		&v.Funcion,
		&v.MedioDePago,
		&v.Alias,
		&v.Monetaria,
		&v.MonedaExtranjera,
		&v.EsSistemica,
		&v.UsaAplicaciones,
		&v.PadreID,
		&v.Descripcion,
		&v.ConceptoIvaCompras,
		&v.CreatedAt,
		&v.CentrosGrupos,
		&v.ConceptoProducto,
		&v.RubroBsUso,
	).Scan(&out)
	if err != nil {
		return out, deferror.DB(err, "inserting")
	}
	return
}

func ReadOne(ctx context.Context, conn *pgxpool.Pool, req cuentas.ReadOneReq) (out cuentas.Cuenta, err error) {

	// Valido
	if req.ID == 0 {
		return out, deferror.Validation("Debe ingresar un ID para la cuenta")
	}
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}

	query := `SELECT 
id,
comitente,
codigo,
orden,
nombre,
apertura_persona,
apertura_persona_restrictiva,
apertura_producto,
apertura_caja,
apertura_deposito,
apertura_contrato,
apertura_unicidad,
tipo_unicidad,
apertura_centro,
centros_permitidos,
tipo_cuenta,
ajusta_por_inflacion,
funcion,
medio_de_pago,
alias,
monetaria, 
moneda_extranjera,
es_sistemica,
usa_aplicaciones,
padre_id,
descripcion,
concepto_iva_compras,
created_at,
updated_at,
centros_grupos,
concepto_producto,
rubro_bsuso
	FROM cuentas 
	WHERE comitente=$1 AND id=$2`

	err = conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(
		&out.ID,
		&out.Comitente,
		&out.Codigo,
		&out.Orden,
		&out.Nombre,
		&out.AperturaPersona,
		&out.AperturaPersonaRestrictiva,
		&out.AperturaProducto,
		&out.AperturaCaja,
		&out.AperturaDeposito,
		&out.AperturaContrato,
		&out.AperturaUnicidad,
		&out.TipoUnicidad,
		&out.AperturaCentro,
		&out.CentrosPermitidos,
		&out.TipoCuenta,
		&out.AjustaPorInflacion,
		&out.Funcion,
		&out.MedioDePago,
		&out.Alias,
		&out.Monetaria,
		&out.MonedaExtranjera,
		&out.EsSistemica,
		&out.UsaAplicaciones,
		&out.PadreID,
		&out.Descripcion,
		&out.ConceptoIvaCompras,
		&out.CreatedAt,
		&out.UpdatedAt,
		&out.CentrosGrupos,
		&out.ConceptoProducto,
		&out.RubroBsUso,
	)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}
	return
}

func ReadMany(ctx context.Context, conn *pgxpool.Pool, req cuentas.ReadManyReq) (out []cuentas.Cuenta, err error) {
	out = []cuentas.Cuenta{}
	if req.Comitente == 0 {
		return out, errors.New("no se ingresó ID de comitente")
	}

	pp := []interface{}{req.Comitente}
	ww := []string{"comitente=$1"}

	// Si estoy buscando por tipo de cuenta
	if req.Tipo != "" {
		if req.Tipo == "Imputables" {
			ww = append(ww, "tipo_cuenta <> 'Agrupadora'")
		}
		if req.Tipo == "Agrupadoras" {
			ww = append(ww, "tipo_cuenta = 'Agrupadora'")
		}
	}
	if req.Funcion != "" {
		pp = append(pp, req.Funcion)
		ww = append(ww, fmt.Sprintf("funcion=$%v", len(pp)))
	}
	if req.Nombre != "" {
		pp = append(pp, "%"+req.Nombre+"%")
		ww = append(ww, fmt.Sprintf("nombre ILIKE $%v", len(pp)))
	}

	where := "WHERE " + strings.Join(ww, " AND ")
	query := fmt.Sprintf(`
SELECT 
id,
comitente,
codigo,
orden,
nombre,
apertura_persona,
apertura_persona_restrictiva,
apertura_producto,
apertura_caja,
apertura_deposito,
apertura_contrato,
apertura_unicidad,
tipo_unicidad,
apertura_centro,
centros_permitidos,
tipo_cuenta,
ajusta_por_inflacion,
funcion,
medio_de_pago,
alias,
monetaria, 
moneda_extranjera,
es_sistemica,
usa_aplicaciones,
padre_id,
descripcion,
concepto_iva_compras,
created_at,
updated_at,
centros_grupos,
concepto_producto,
rubro_bsuso
FROM cuentas 
	%v
	`, where)

	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		v := cuentas.Cuenta{}
		err = rows.Scan(
			&v.ID,
			&v.Comitente,
			&v.Codigo,
			&v.Orden,
			&v.Nombre,
			&v.AperturaPersona,
			&v.AperturaPersonaRestrictiva,
			&v.AperturaProducto,
			&v.AperturaCaja,
			&v.AperturaDeposito,
			&v.AperturaContrato,
			&v.AperturaUnicidad,
			&v.TipoUnicidad,
			&v.AperturaCentro,
			&v.CentrosPermitidos,
			&v.TipoCuenta,
			&v.AjustaPorInflacion,
			&v.Funcion,
			&v.MedioDePago,
			&v.Alias,
			&v.Monetaria,
			&v.MonedaExtranjera,
			&v.EsSistemica,
			&v.UsaAplicaciones,
			&v.PadreID,
			&v.Descripcion,
			&v.ConceptoIvaCompras,
			&v.CreatedAt,
			&v.UpdatedAt,
			&v.CentrosGrupos,
			&v.ConceptoProducto,
			&v.RubroBsUso,
		)
		if err != nil {
			return out, deferror.DB(err, "escaneando")
		}
		out = append(out, v)
	}
	return
}

func Update(ctx context.Context, conn *pgxpool.Pool, v cuentas.Cuenta) (err error) {

	if v.Comitente == 0 {
		return errors.New("no se ingresó ID de cuenta")
	}
	if v.Codigo == "" {
		return deferror.Validation("no se ingresó código de cuenta")
	}

	v.CreatedAt = new(time.Time)
	*v.CreatedAt = time.Now()

	query := `UPDATE cuentas SET 
codigo=$3,
orden=$4,
nombre=$5,
apertura_persona=$6,
apertura_persona_restrictiva=$7,
apertura_producto=$8,
apertura_caja=$9,
apertura_deposito=$10,
apertura_contrato=$11,
apertura_unicidad=$12,
tipo_unicidad=$13,
apertura_centro=$14,
centros_permitidos=$15,
tipo_cuenta=$16,
ajusta_por_inflacion=$17,
funcion=$18,
medio_de_pago=$19,
alias=$20,
monetaria=$21, 
moneda_extranjera=$22,
es_sistemica=$23,
usa_aplicaciones=$24,
padre_id=$25,
descripcion=$26,
concepto_iva_compras=$27,
updated_at=$28,
centros_grupos=$29,
concepto_producto=$30,
rubro_bsuso=$31
WHERE comitente=$1 AND id=$2`

	_, err = conn.Exec(ctx, query,
		v.Comitente,
		v.ID,
		v.Codigo,
		v.Orden,
		v.Nombre,
		v.AperturaPersona,
		v.AperturaPersonaRestrictiva,
		v.AperturaProducto,
		v.AperturaCaja,
		v.AperturaDeposito,
		v.AperturaContrato,
		v.AperturaUnicidad,
		v.TipoUnicidad,
		v.AperturaCentro,
		v.CentrosPermitidos,
		v.TipoCuenta,
		v.AjustaPorInflacion,
		v.Funcion,
		v.MedioDePago,
		v.Alias,
		v.Monetaria,
		v.MonedaExtranjera,
		v.EsSistemica,
		v.UsaAplicaciones,
		v.PadreID,
		v.Descripcion,
		v.ConceptoIvaCompras,
		v.CreatedAt,
		v.CentrosGrupos,
		v.ConceptoProducto,
		v.RubroBsUso,
	)
	if err != nil {
		return deferror.DB(err, "updating")
	}
	return
}

func Delete(ctx context.Context, conn *pgxpool.Pool, req cuentas.DeleteReq) (err error) {
	// Valido
	if req.ID == 0 {
		return deferror.Validation("no se ingresó ID de cuenta a borrar")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}

	{ // Está usada en la contabilidad?
		count := 0
		query := `SELECT COUNT(cuenta) FROM partidas WHERE comitente=$1 AND cuenta=$2`
		err = conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(&count)
		if err != nil {
			return deferror.DB(err, "determinando si la cuenta estaba usada")
		}
		if count > 0 {
			return deferror.Validationf("No se puede borrar porque la cuenta fue usada %v veces en la contabilidad", count)
		}
	}
	_, err = conn.Exec(ctx, "DELETE FROM cuentas WHERE comitente=$1 AND id=$2", req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "borrando")
	}
	return

}
