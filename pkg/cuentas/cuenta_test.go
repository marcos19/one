package cuentas

import (
	"testing"

	"github.com/davecgh/go-spew/spew"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestMakeTree(t *testing.T) {
	t.Run("usual", func(t *testing.T) { // Usual case
		c1 := Cuenta{ID: 1}
		c2 := Cuenta{ID: 11, PadreID: &c1.ID}
		c3 := Cuenta{ID: 111, PadreID: &c2.ID}

		root, err := MakeTree([]Cuenta{c1, c2, c3})
		require.Nil(t, err)

		require.NotNil(t, root)
		assert.Nil(t, root.Cuenta)
		require.Len(t, root.Children, 1)
		grouper := root.Children[0]
		assert.Equal(t, 1, grouper.Cuenta.ID)

		require.Len(t, grouper.Children, 1)
		child1 := grouper.Children[0]
		assert.Equal(t, 11, child1.Cuenta.ID)

		require.Len(t, child1.Children, 1)
		child2 := child1.Children[0]
		assert.Equal(t, 111, child2.Cuenta.ID)
	})

	t.Run("circular", func(t *testing.T) { // Circular references
		c1 := Cuenta{ID: 1}
		c2 := Cuenta{ID: 11}
		c3 := Cuenta{ID: 111, PadreID: &c2.ID}
		c2.PadreID = &c3.ID

		root, err := MakeTree([]Cuenta{c1, c2, c3})
		require.NotNil(t, err)
		assert.ErrorIs(t, err, ErrReferenciaCircular)
		require.Nil(t, root)
	})

	t.Run("no root", func(t *testing.T) {
		c2 := Cuenta{ID: 11}
		c3 := Cuenta{ID: 111, PadreID: &c2.ID}
		c2.PadreID = &c3.ID

		root, err := MakeTree([]Cuenta{c2, c3})
		spew.Dump(root)
		require.NotNil(t, err)
		assert.ErrorIs(t, err, ErrNingunaRaiz)
		require.Nil(t, root)
	})
}
