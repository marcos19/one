package cuentas

import "context"

type ReaderOne interface {
	ReadOne(context.Context, ReadOneReq) (Cuenta, error)
}
type ReaderMany interface {
	ReadMany(context.Context, ReadManyReq) ([]Cuenta, error)
}

type Creater interface {
	Create(context.Context, Cuenta) (id int, err error)
}

type Updater interface {
	Update(context.Context, Cuenta) error
}

type Deleter interface {
	Delete(context.Context, DeleteReq) error
}

type PorFuncionGetter interface {
	PorFuncionContable(ctx context.Context, comitente int, f Funcion) ([]Cuenta, error)
}

type FuncionesGetter interface {
	FuncionesContables() []Funcion
}

type ReadOneReq struct {
	ID        int `json:",string"`
	Comitente int
}
type DeleteReq struct {
	ID        int `json:",string"`
	Comitente int
}
type ReadManyReq struct {
	Comitente int
	Tipo      string
	Funcion   Funcion
	Nombre    string
}
