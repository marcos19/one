// Package cuentas define las Cuentas Contables.
package cuentas

import (
	"time"

	"bitbucket.org/marcos19/one/pkg/tipos"
	"golang.org/x/text/collate"
	"golang.org/x/text/language"
)

// Cuenta define la cuenta contable tal cual va en su tabla con toda su funcionalidad
type Cuenta struct {
	ID        int `json:",string"`
	Comitente int
	// El usuario se maneja con esta.
	Codigo string
	Nombre string

	// Si se usa, la partida tiene que aclarar quien es la persona.
	AperturaPersona bool

	// Para determinar la naturaleza de la cuenta. Por ejemplo: cuenta
	// corriente bancaria la quiero usar con determinadas personas nomás (los
	// bancos). Entonces cuando tenga que elegir una persona para hacer un
	// depósito bancario, me traiga únicamente los bancos. No es el mismo
	// comportamiento el deseado para Clientes, ya que lo habitual es que
	// todos sean clientes.
	AperturaPersonaRestrictiva bool

	// Bienes de Cambio, Costo, Ventas, etc. Hay que aclarar cuál es el producto.
	AperturaProducto bool

	// Si hay varias cajas para una misma cuenta contable
	AperturaCaja bool

	// Si hay varios depositos para una cuenta contable
	AperturaDeposito bool

	AperturaContrato bool
	// Hay una tabla de unicidades que va a incluye los cheques,
	// obleas, etc. Cada movimiento contable tiene que ir referenciado a una
	// de estas entidades.
	AperturaUnicidad bool

	// Cuando por ejemplo elijo "Cheque Propio", la UI me va a pedir que elija
	// a que unicidad me estoy refiriendo. Entonces, cheques propios me va a
	// referenciar a las unicidades definidas como ChequePropio, "Obleas en
	// tenencia" me va a referenciar a "Obleas"
	TipoUnicidad *int `json:",string"`

	// Si es true, hay que elegir el centro de imputación para cada movimiento.
	AperturaCentro    bool
	CentrosPermitidos tipos.GrupoInts
	CentrosGrupos     tipos.GrupoInts

	// Necesito saber si es patrimonial o de resultado para el asiento de
	// refundición y cierre.
	TipoCuenta TipoCuentaContable

	// Para las cuentas de resultado que las ajusto por mes
	AjustaPorInflacion bool

	// Esta señal es usada para determinar si corresponde hacer ajuste por inflación.
	Monetaria bool

	// Si es false significa a que a cada movimiento hay que especificarle la moneda
	MonedaExtranjera bool

	// Si es una cuenta de orden, tendrá que definir su contracuenta.
	// ContraCuenta int

	// Un bloqueo especial para disminuir las posibilidades de error.
	// Por ejemplo la cuenta IVA CF nunca podría tener el concepto percepción
	// ingresos brutos.
	ConceptosHabilitadosIds map[string][]int

	// Evita que la puede seleccionar manualmente el usuario
	EsSistemica bool

	// Funcion le dice al sistema que función comple esta cuenta (Bien de Uso,
	// Amort Acum, Bien de cambio, etc)
	Funcion Funcion

	RubroBsUso *int

	// Es el concepto definido en package opprod.
	ConceptoProducto *int `json:",string"`

	MedioDePago bool
	// Define la contracuenta predefinida. Por ejemplo cuando selecciono en la
	// factura de compra la cuenta contable "Electricidad"
	// ContraCuentas tipos.GrupoInts

	// Es el nombre que se va a mostrar en los recibos. Ejemplo la cuenta:
	// 'Cupones  de Tarjetas a Cobrar' es más legible para el usuario si
	// se llama 'Tarjeta de crédito'
	Alias string

	// Cuando es true va a pegar el campo idPartidaAplicada
	UsaAplicaciones bool

	// Para armar el arbol de jerarquía
	PadreID *int `json:",string"`
	Orden   int

	Descripcion string

	// Sirve para poder agrupar las compras en servicios, bienes, locaciones, etc
	ConceptoIvaCompras string

	Inactiva bool

	CreatedAt *time.Time
	UpdatedAt *time.Time
}

func (c Cuenta) GetID() int {
	return c.ID
}
func (c Cuenta) GetFather() *int {
	return c.PadreID
}

var col = collate.New(language.Spanish)

func (c *Cuenta) Sort(other *Cuenta) int {
	switch {
	case c.Orden == other.Orden:
		return col.CompareString(c.Nombre, other.Nombre)

	case c.Orden < other.Orden:
		return -1
	}

	return 1
}

// TipoCuentaContable determina si se trata de una cuenta, patrimonial,
// de resultado, de orde o agrupadora.
type TipoCuentaContable string

const (
	TipoCuentaPatrimonial = "Patrimonial"
	TipoCuentaDeResultado = "Resultado"
	TipoCuentaDeOrden     = "Orden"
	TipoCuentaAgrupadora  = "Agrupadora"
)

func (c *Cuenta) TableName() string {
	return "cuentas"
}

type Funcion string

// Función que tiene la cuenta contable dentro de la construcción del asiento de una operación.
const (
	// La cuenta de stock propio
	MercaderiaPropiaEnDepositosPropios     = Funcion("Mercadería propia en depósitos propios")
	MercaderiaPropiaEnDepositosDeTerceros  = Funcion("Mercadería propia en depósitos de terceros")
	MercaderiaDeTercerosEnDepositosPropios = Funcion("Mercadería de terceros en depósitos propios")
	Proveedores                            = Funcion("Proveedores")
	FacturasARecibir                       = Funcion("Facturas a recibir")
	MercaderiaARecibir                     = Funcion("Mercadería a recibir")

	DeudoresPorVentas   = Funcion("Deudores por ventas")
	MercaderiaAFacturar = Funcion("Mercadería facturar")
	MercaderiaAEntregar = Funcion("Mercadería a entregar")

	Ventas         = Funcion("Ventas")
	VentasACostear = Funcion("Ventas a costear")

	Costo = Funcion("Costo")

	IVACreditoFiscal = Funcion("IVA CF")
	IVADebitoFiscal  = Funcion("IVA DF")
	MedioDePago      = Funcion("Medio de pago")

	Recpam                  = Funcion("RECPAM")
	CuentaBancaria          = Funcion("Cuenta bancaria")
	CuentaBancariaConciliar = Funcion("Cuenta bancaria (a conciliar)")

	BsUsoValorOrigen = Funcion("Bienes de uso - Valor origen")
	BsUsoAmortAcum   = Funcion("Bienes de uso - Amortización acumulada")
)

func (f Funcion) AbrePorPersona() bool {
	switch f {
	case
		MercaderiaPropiaEnDepositosDeTerceros,
		MercaderiaDeTercerosEnDepositosPropios,
		Proveedores,
		FacturasARecibir,
		DeudoresPorVentas,
		MercaderiaAFacturar,
		MercaderiaAEntregar,
		Ventas,
		VentasACostear,
		Costo:
		return true
	}

	return false
}
