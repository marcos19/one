package aplicador

import (
	"sort"

	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/rs/zerolog/log"
)

// Este algoritmo tilda la mayor cantidad de partidas para que
// el total de ese monto.
// Devuelve con los mismos signos que
func calzar(pp []agg, aplicacion dec.D2) (out []agg, err error) {

	// Las ordeno por monto
	sort.Slice(pp, func(i, j int) bool {
		return pp[i].Monto < pp[j].Monto
	})

	// Total
	log.Debug().Msgf("Total a aplicar: %v", aplicacion)
	aplicado := dec.D2(0)
	for _, v := range pp {
		aplicado += v.Monto
		out = append(out, v)
	}

	iGrande := len(out) - 1
	iChico := 0
	count := 0

	for {
		// fmt.Println("Count", count)
		count++
		if count > len(pp) {
			return out, errors.Errorf("superadas iteraciones máximas %v", count)
		}

		switch {

		case aplicado > aplicacion:

			// Tengo que empezar a achicar
			masGrande := out[iGrande].Monto
			if masGrande < 0 {
				return out, errors.Errorf("no se puede calzar aplicaciones")
			}

			if masGrande < aplicado-aplicacion {
				// fmt.Println("poniendo en cero másGrande", iGrande, masGrande)
				aplicado -= masGrande
				out[iGrande].Monto = 0
			} else {
				// Parcializo este
				// fmt.Println("En iGrande=", iGrande, "monto pasa de", out[iGrande].Monto, " a ", out[iGrande].Monto-(aplicado-aplicacion))
				out[iGrande].Monto = out[iGrande].Monto - (aplicado - aplicacion)
				return
			}
			iGrande--

		case aplicado < aplicacion:

			// Empiezo a restar los más chicos
			masChico := pp[iChico].Monto
			if masChico > 0 {
				return out, errors.Errorf("no se puede calzar aplicaciones")
			}

			if masChico > aplicado-aplicacion {
				// fmt.Println("poniendo en cero masChico", iChico, masChico)
				aplicado -= masChico
				out[iChico].Monto = 0
			} else {
				// Parcializo este
				// fmt.Println("En iChico=", iChico, "monto pasa de", out[iChico].Monto, " a ", out[iChico].Monto+aplicacion)
				out[iChico].Monto = out[iChico].Monto - (aplicado - aplicacion)
				return
			}
			iChico++

		default:
			// Da cero, return
			return
		}
	}

}
