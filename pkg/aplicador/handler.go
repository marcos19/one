package aplicador

import (
	"context"
	"fmt"
	"sort"
	"strings"

	"bitbucket.org/marcos19/one/pkg/aplicador/config"
	"bitbucket.org/marcos19/one/pkg/cuentasgrupos"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/personas"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/crosslogic/niler"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/rs/zerolog/log"
)

const Programa = "aplicador"

// Handler da acceso al a todos los routes de personas.
type Handler struct {
	conn            *pgxpool.Pool
	config          *config.Handler
	grupos          *cuentasgrupos.Handler
	opInserter      ops.Inserter
	opDeleter       ops.Deleter
	personasHandler personas.ReaderOne
}

func NewHandler(
	conn *pgxpool.Pool,
	config *config.Handler,
	grupos *cuentasgrupos.Handler,
	opsHandler OpsHandler,
	personasHandler personas.ReaderOne,
) (h *Handler, err error) {

	if conn == nil {
		return h, errors.New("conn no puede ser nil")
	}
	if config == nil {
		return h, errors.New("config no puede ser nil")
	}
	if grupos == nil {
		return h, errors.New("grupos no puede ser nil")
	}
	if niler.IsNil(opsHandler) {
		return h, errors.New("ops.Inserter no puede ser nil")
	}
	if niler.IsNil(personasHandler) {
		return h, errors.New("personas no puede ser nil")
	}

	h = &Handler{
		conn:            conn,
		config:          config,
		grupos:          grupos,
		opInserter:      opsHandler,
		opDeleter:       opsHandler,
		personasHandler: personasHandler,
	}

	return
}

type DisponiblesReq struct {
	Comitente int
	Config    int `json:",string"`
	Empresa   int `json:",string"`
	Persona   uuid.UUID
	Sucursal  int `json:",string"`
	Desde     fecha.Fecha
	Hasta     fecha.Fecha
	Cuentas   tipos.GrupoInts
}

type DisponiblesResponse struct {
	OpID           uuid.UUID
	PartidasID     tipos.GrupoUUID
	FechaContable  fecha.Fecha
	FechaVto       fecha.Fecha
	CompNombre     string
	NCompStr       string
	MontoOriginal  dec.D2
	Saldo          dec.D2
	Detalle        *string
	DetallePartida *string
}

func (h *Handler) Disponibles(ctx context.Context, req DisponiblesReq) (out []DisponiblesResponse, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó comitente")
	}
	if req.Empresa == 0 {
		return out, deferror.Validation("no se ingresó empresa")
	}
	if req.Config == 0 {
		return out, deferror.Validation("no se ingresó config")
	}

	ww := []string{}
	pp := []interface{}{}

	{ // Comitente
		ww = append(ww, "comitente = $1")
		pp = append(pp, req.Comitente)
	}

	{ // Empresa
		ww = append(ww, "empresa = $2")
		pp = append(pp, req.Empresa)
	}

	// Persona
	if req.Persona != uuid.Nil {
		ww = append(ww, fmt.Sprintf("persona = %v", uuidtoBytes(req.Persona)))
	}

	// Sucursal
	if req.Sucursal != 0 {
		ww = append(ww, fmt.Sprintf("sucursal = %v", req.Sucursal))
	}

	// Traigo config
	cfg, err := h.config.ReadOne(ctx, config.ReadOneReq{
		Comitente: req.Comitente,
		ID:        req.Config,
	})
	if err != nil {
		return out, errors.Wrap(err, "buscando config")
	}
	if req.Persona == uuid.Nil && cfg.PorPersona {
		return out, deferror.Validation("no se ingresó persona")
	}

	// Filtro de cuentas
	if len(req.Cuentas) > 0 {
		// Si el frontend está pidiendo alguna cuenta en particular, considero esas.
		ww = append(ww, fmt.Sprintf("cuenta IN %v", req.Cuentas.ParaClausulaIn()))

	} else {
		// Tomo todas las cuentas determinadas por la config
		cc := cfg.Cuentas
		for _, v := range cfg.GruposCuentas {
			// Busco las cuentas del grupo
			g, err := h.grupos.ReadOne(ctx, cuentasgrupos.ReadOneReq{
				Comitente: req.Comitente,
				ID:        v,
			})
			if err != nil {
				return nil, errors.Wrapf(err, "buscando grupo %v", v)
			}
			cc = append(cc, g.Cuentas...)
		}
		ww = append(ww, fmt.Sprintf("cuenta IN %v", cc.ParaClausulaIn()))
	}

	ids := []string{}
	saldos := map[uuid.UUID]dec.D2{}

	// Busco IDS dipobibles en la tabla de saldos
	{
		where := strings.Join(ww, " AND ")
		query := fmt.Sprintf(`
			SELECT partida_aplicada, monto
			FROM saldos 
			WHERE %v
	`, where)

		rows, err := h.conn.Query(ctx, query, pp...)
		if err != nil {
			return nil, deferror.DB(err, "buscando ID de partidas disponibles")
		}
		defer rows.Close()
		for rows.Next() {
			id := []byte{}
			saldo := dec.D2(0)
			err = rows.Scan(&id, &saldo)
			if err != nil {
				return nil, deferror.DB(err, "esacaneando ID")
			}
			UUID, err := uuid.FromBytes(id)
			if err != nil {
				return nil, errors.Wrap(err, "creando UUID")
			}
			str := fmt.Sprintf("'%v'", UUID.String())
			saldos[UUID] = saldo
			ids = append(ids, str)
		}
	}

	whereClauses := []string{
		fmt.Sprintf("partidas.comitente = %v", req.Comitente),
		fmt.Sprintf("partidas.id IN (%v)", strings.Join(ids, ",")),
	}
	// if req.Desde != 0 {
	// 	whereClauses = append(whereClauses, fmt.Sprintf("fecha_financiera >= '%v'", req.Desde.JSONString()))
	// }
	// if req.Hasta != 0 {
	// 	whereClauses = append(whereClauses, fmt.Sprintf("fecha_financiera <= '%v'", req.Hasta.JSONString()))
	// }
	where := strings.Join(whereClauses, " AND ")

	// Busco datos de las partidas
	query := fmt.Sprintf(`
	SELECT partidas.op_id, partidas.id, partidas.fecha_contable, partidas.fecha_financiera, ops.comp_nombre, ops.n_comp_str, partidas.monto, ops.detalle, partidas.detalle
	FROM partidas
	INNER JOIN ops ON ops.id = partidas.op_id
	WHERE %v
	ORDER BY partidas.fecha_contable, partidas.created_at
	;`, where)

	// GROUP BY partidas.op_id, partidas.id, partidas.fecha_contable, partidas.fecha_financiera, ops.comp_nombre, ops.n_comp_str
	// ORDER BY partidas.fecha_financiera

	if cfg.AgruparPartidas {
		type key struct {
			OpID          uuid.UUID
			FechaContable fecha.Fecha
			FechaVto      fecha.Fecha
			CompNombre    string
			NCompStr      string
			Detalle       string
		}
		type value struct {
			MontoOriginal dec.D2
			Saldo         dec.D2
			PartidasID    tipos.GrupoUUID
		}

		agg := map[key]value{}
		// Busco datos partidas partidas
		rows, err := h.conn.Query(ctx, query)
		if err != nil {
			return nil, deferror.DB(err, "realizando query")
		}
		defer rows.Close()
		for rows.Next() {
			k := key{}
			monto := dec.D2(0)
			partidaID := uuid.UUID{}
			temp := new(string)
			detalle := new(string)
			err = rows.Scan(&k.OpID, &partidaID, &k.FechaContable, &k.FechaVto, &k.CompNombre, &k.NCompStr, &monto, &detalle, &temp)
			if err != nil {
				return nil, errors.Wrap(err, "escaneando row agrupada")
			}
			if detalle != nil {
				k.Detalle = *detalle
			}
			anterior := agg[k]
			anterior.PartidasID = append(anterior.PartidasID, partidaID)

			saldoDePartida := saldos[partidaID]
			anterior.Saldo += saldoDePartida
			anterior.MontoOriginal += monto
			agg[k] = anterior
		}
		for k, v := range agg {
			p := DisponiblesResponse{
				OpID:          k.OpID,
				CompNombre:    k.CompNombre,
				FechaContable: k.FechaContable,
				FechaVto:      k.FechaVto,
				NCompStr:      k.NCompStr,
				MontoOriginal: v.MontoOriginal,
				Saldo:         v.Saldo,
				PartidasID:    v.PartidasID,
			}
			p.Detalle = new(string)
			*p.Detalle = k.Detalle

			out = append(out, p)
		}
	} else {
		// Si no agrupa partidas
		rows, err := h.conn.Query(ctx, query)
		if err != nil {
			return nil, deferror.DB(err, "realizando query")
		}
		defer rows.Close()
		for rows.Next() {
			k := DisponiblesResponse{}
			partidaID := uuid.UUID{}
			err = rows.Scan(&k.OpID, &partidaID, &k.FechaContable, &k.FechaVto, &k.CompNombre, &k.NCompStr, &k.MontoOriginal, &k.Detalle, &k.DetallePartida)
			if err != nil {
				return nil, errors.Wrap(err, "escaneando row")
			}

			// Pongo el ID de partida
			k.PartidasID = append(k.PartidasID, partidaID)

			k.Saldo = saldos[partidaID]
			out = append(out, k)
		}
	}

	// Controlo que no no haya ninguna borrada
	partidasEncontradas := map[uuid.UUID]struct{}{}
	for _, v := range out {
		for _, w := range v.PartidasID {
			partidasEncontradas[w] = struct{}{}
		}
	}

	for k, v := range saldos {
		_, estaba := partidasEncontradas[k]
		if !estaba {
			out = append(out, DisponiblesResponse{PartidasID: tipos.GrupoUUID{k}, Saldo: v})
		}
	}

	// Está dentro del período solicitado?
	filtradas := []DisponiblesResponse{}
	for _, v := range out {

		if req.Desde != 0 {
			if cfg.PorFechaVto {
				if v.FechaVto < req.Desde {
					continue
				}
			} else {
				if v.FechaContable < req.Desde {
					continue
				}
			}
		}
		if req.Hasta != 0 {
			if cfg.PorFechaVto {
				if v.FechaVto > req.Hasta {
					continue
				}
			} else {
				if v.FechaContable > req.Hasta {
					continue
				}
			}
		}
		filtradas = append(filtradas, v)
	}
	out = filtradas

	// Ordeno por fecha de vencimiento
	sort.Slice(out, func(i, j int) bool {
		return out[i].FechaVto < out[j].FechaVto
	})
	return
}

func (h *Handler) Create(ctx context.Context, req Aplicacion) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se definió comitente")
	}
	if req.Empresa == 0 {
		return deferror.Validation("no se definió empresa")
	}
	if req.Config == 0 {
		return deferror.Validation("no se definió fecha")
	}
	if req.Fecha == 0 {
		return deferror.Validation("no se definió fecha")
	}
	if len(req.Partidas) == 0 {
		return deferror.Validation("no se ingresaron partidas")
	}
	req.ID, _ = uuid.NewV1()

	// Tengo los partidasID agrupadas por op => Aplano la lista
	ids := tipos.GrupoUUID{}
	for _, v := range req.Partidas {
		ids = append(ids, v.PartidasID...)
	}

	// Traigo config
	cfg, err := h.config.ReadOne(ctx, config.ReadOneReq{
		Comitente: req.Comitente,
		ID:        req.Config,
	})
	if err != nil {
		return errors.Wrap(err, "trayendo config")
	}

	// Inicio tx
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return deferror.DB(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)

	// Traigo saldos
	saldos, err := traerSaldos(ctx, tx, ids)
	if err != nil {
		return errors.Wrap(err, "buscando partidas por ID")
	}

	// Creo op
	op := ops.Op{}
	op.ID, _ = uuid.NewV1()
	op.TranID = req.ID
	op.Fecha = req.Fecha
	op.Comitente = req.Comitente
	op.Empresa = req.Empresa
	op.FechaOriginal = req.Fecha
	op.CompID = cfg.Comp
	if req.Persona != nil {
		op.Persona = new(uuid.UUID)
		*op.Persona = *req.Persona
		per, err := h.personasHandler.ReadOne(ctx, personas.ReadOneReq{Comitente: req.Comitente, ID: *req.Persona})
		if err != nil {
			return errors.Wrap(err, "buscando persona")
		}
		op.PersonaNombre = per.Nombre
	}
	if req.Sucursal != nil {
		op.Sucursal = new(int)
		*op.Sucursal = *req.Sucursal
	}
	op.Programa = Programa
	op.TotalComp = new(dec.D2)
	for i, v := range req.Partidas {

		// Creo slice con las partidas
		pp := []agg{}
		for _, w := range v.PartidasID {
			pp = append(pp, saldos[w])
		}

		calzadas, err := calzar(pp, v.Aplicacion)
		if err != nil {
			return errors.Wrapf(err, "calzando partidas renglon %v", i)
		}
		acum := dec.D2(0)

		// Creo partidas
		for _, w := range calzadas {
			p, err := crearPartidaContable(w)
			if err != nil {
				return errors.Wrap(err, "creando partida contable")
			}
			op.PartidasContables = append(op.PartidasContables, p)

			acum += w.Monto
			if w.Monto > 0 {
				*op.TotalComp += w.Monto
			}
		}
	}

	log.Debug().Msg("persistirAplicacion")
	// Inserto aplicacion
	err = persistirAplicacion(ctx, tx, &req)
	if err != nil {
		return err
	}

	log.Debug().Msg("InsertOps")
	// Inserto ops
	err = h.opInserter.Insert(ctx, tx, []*ops.Op{&op}, nil)
	if err != nil {
		return errors.Wrap(err, "ops")
	}

	// Confirmo
	log.Debug().Msg("confirmando")
	err = tx.Commit(ctx)
	if err != nil {
		return errors.Wrap(err, "confirmando transacción")
	}
	return
}

func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (out Aplicacion, err error) {

	// Busco
	out, err = readOne(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando apli")
	}

	return
}

func (h *Handler) Delete(ctx context.Context, req ReadOneReq) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó comitente")
	}
	if req.ID == uuid.Nil {
		return deferror.Validation("no se ingresó ID")
	}

	// Busco op id
	id := uuid.UUID{}
	err = h.conn.QueryRow(ctx, "SELECT id FROM ops WHERE tran_id = $1", req.ID).Scan(&id)
	if err != nil {
		return errors.Wrap(err, "buscando ID de op")
	}

	// Inicio transacción
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return errors.Wrap(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)

	err = h.opDeleter.Delete(ctx, tx, req.Comitente, []uuid.UUID{id})
	if err != nil {
		return deferror.DB(err, "borrando op")
	}

	{ // Borro rendición
		query := `DELETE FROM apli WHERE id = $1`
		_, err := tx.Exec(ctx, query, req.ID)
		if err != nil {
			return errors.Wrap(err, "borrando aplicación")
		}
	}

	// Confirmo transacción
	err = tx.Commit(ctx)
	if err != nil {
		return errors.Wrap(err, "confirmando transacción")
	}

	return
}

// En base al saldo crea la partida contable de aplicación
func crearPartidaContable(w agg) (p ops.Partida, err error) {

	// Creo partida contable
	p.ID, _ = uuid.NewV1()
	p.Cuenta = w.Cuenta

	// Centro
	if w.Centro != 0 {
		p.Centro = new(int)
		*p.Centro = w.Centro
	}

	// Persona
	if w.Persona != uuid.Nil {
		p.Persona = new(uuid.UUID)
		*p.Persona = w.Persona
	}

	// Sucursal
	if w.Sucursal != 0 {
		p.Sucursal = new(int)
		*p.Sucursal = w.Sucursal
	}

	// Sucursal
	if w.Caja != 0 {
		p.Caja = new(int)
		*p.Caja = w.Caja
	}

	// Producto
	if w.Producto != uuid.Nil {
		p.Producto = new(uuid.UUID)
		*p.Producto = w.Producto
	}

	// SKU
	if w.SKU != uuid.Nil {
		p.SKU = new(uuid.UUID)
		*p.SKU = w.SKU
	}

	// Deposito
	if w.Deposito != 0 {
		p.Deposito = new(int)
		*p.Deposito = w.Deposito
	}

	// Unicidad
	if w.Unicidad != uuid.Nil {
		p.Unicidad = new(uuid.UUID)
		*p.Unicidad = w.Unicidad
	}

	// Partida aplicada
	if w.PartidaAplicada != uuid.Nil {
		p.PartidaAplicada = new(uuid.UUID)
		*p.PartidaAplicada = w.PartidaAplicada
	}

	// Contrato
	if w.Contrato != 0 {
		p.Contrato = new(int)
		*p.Contrato = w.Contrato
	}

	// Empresa
	if w.Empresa == 0 {
		return p, deferror.Validation("empresa no puede ser cero")
	}

	p.Monto = -w.Monto
	return
}

type agg struct {
	Comitente       int
	Cuenta          int
	Centro          int
	Persona         uuid.UUID
	Sucursal        int
	Caja            int
	Producto        uuid.UUID
	SKU             uuid.UUID
	Deposito        int
	Unicidad        uuid.UUID
	PartidaAplicada uuid.UUID
	Contrato        int
	Empresa         int
	Monto           dec.D2
}

func uuidtoBytes(in uuid.UUID) string {
	if in == uuid.Nil {
		return "x''"
	} else {
		return fmt.Sprintf("x'%x'", in)
	}
}
func uuidsToBytes(in []uuid.UUID) string {
	oo := []string{}
	for _, v := range in {
		oo = append(oo, fmt.Sprintf("x'%x'", v))
	}

	return fmt.Sprintf("(%v)", strings.Join(oo, ", "))
}
