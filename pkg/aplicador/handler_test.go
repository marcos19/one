package aplicador

import (
	"context"
	"testing"

	"bitbucket.org/marcos19/one/pkg/aplicador/config"
	"bitbucket.org/marcos19/one/pkg/cuentasgrupos"
	opshandler "bitbucket.org/marcos19/one/pkg/ops/handler"
	"bitbucket.org/marcos19/one/pkg/personas"
	"bitbucket.org/marcos19/one/pkg/personas/handler"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/stretchr/testify/require"
)

func TestNewHandler(t *testing.T) {
	oph := opshandler.Handler{}
	con := config.Handler{}
	gr := cuentasgrupos.Handler{}
	conn := pgxpool.Pool{}

	{ // Untyped nil
		_, err := NewHandler(&conn, &con, &gr, &oph, nil)
		require.NotNil(t, err)
	}

	{ // Typed nil
		var pers *handler.Handler
		_, err := NewHandler(&conn, &con, &gr, &oph, pers)
		require.NotNil(t, err)
	}

	{ // Struct
		var pers mockPers
		_, err := NewHandler(&conn, &con, &gr, &oph, pers)
		require.Nil(t, err)
	}
}

type mockPers struct{}

func (m mockPers) ReadOne(context.Context, personas.ReadOneReq) (personas.Persona, error) {
	return personas.Persona{}, nil
}
