package aplicador

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type ReadOneReq struct {
	Comitente int
	ID        uuid.UUID
}

func readOne(ctx context.Context, conn *pgxpool.Pool, req ReadOneReq) (out Aplicacion, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó comitente")
	}
	if req.ID == uuid.Nil {
		return out, deferror.Validation("no se ingresó ID")
	}

	// Busco
	query := `SELECT id, comitente, empresa, config, fecha, partidas, usuario, created_at, persona, sucursal FROM apli WHERE comitente=$1 AND id=$2`
	err = conn.
		QueryRow(ctx, query, req.Comitente, req.ID).
		Scan(&out.ID, &out.Comitente, &out.Empresa, &out.Config, &out.Fecha, &out.Partidas, &out.Usuario, &out.CreatedAt, &out.Persona, &out.Sucursal)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}

	detOps, err := traerDetallesOps(ctx, conn, &out)
	if err != nil {
		return out, fmt.Errorf("trayendo detalles ops: %w", err)
	}
	detParts, err := traerDetallesPartidas(ctx, conn, &out)
	if err != nil {
		return out, fmt.Errorf("trayendo detalles partidas: %w", err)
	}
	for i := range out.Partidas {
		dd := []string{}
		detOp := detOps[out.Partidas[i].OpID]
		if detOp != "" {
			dd = append(dd, detOp)
		}

		for _, v := range out.Partidas[i].PartidasID {

			detPart := detParts[v]
			if detPart != "" {
				dd = append(dd, detPart)
			}
		}
		out.Partidas[i].Detalle = strings.Join(dd, " | ")
	}

	return
}

func traerDetallesOps(ctx context.Context, conn *pgxpool.Pool, aa *Aplicacion) (out map[uuid.UUID]string, err error) {

	out = map[uuid.UUID]string{}
	for _, v := range aa.Partidas {
		out[v.OpID] = ""
	}

	q := fmt.Sprintf(`SELECT id, detalle FROM ops WHERE id IN (%v)`, arrayUUID(out))
	rows, err := conn.Query(ctx, q)
	if err != nil {
		return out, fmt.Errorf("querying: %w", err)
	}
	for rows.Next() {
		det := ""
		id := uuid.UUID{}
		err = rows.Scan(&id, &det)
		if err != nil {
			return out, fmt.Errorf("scanning: %w", err)
		}
		out[id] = det
	}
	return
}

func traerDetallesPartidas(ctx context.Context, conn *pgxpool.Pool, aa *Aplicacion) (out map[uuid.UUID]string, err error) {

	out = map[uuid.UUID]string{}
	for _, v := range aa.Partidas {
		for _, j := range v.PartidasID {
			out[j] = ""
		}
	}

	q := fmt.Sprintf(`SELECT id, detalle FROM partidas WHERE id IN (%v)`, arrayUUID(out))
	rows, err := conn.Query(ctx, q)
	if err != nil {
		return out, fmt.Errorf("querying: %w", err)
	}

	for rows.Next() {
		var det *string
		id := uuid.UUID{}
		err = rows.Scan(&id, &det)
		if err != nil {
			return out, fmt.Errorf("scanning: %w", err)
		}
		if det == nil {
			continue
		}
		out[id] = *det
	}
	return
}

func arrayUUID(ids map[uuid.UUID]string) string {
	ii := make([]uuid.UUID, len(ids))
	i := 0
	for k := range ids {
		ii[i] = k
		i++
	}
	idsStr := []string{}
	for k := range ids {
		idsStr = append(idsStr, fmt.Sprintf("'%v'", k.String()))
	}
	return strings.Join(idsStr, ", ")
}

func persistirAplicacion(ctx context.Context, tx pgx.Tx, a *Aplicacion) (err error) {

	query := `INSERT INTO apli (
		id,
		comitente, 
		empresa,
		config,
		fecha,
		partidas, 
		usuario,
		created_at,
		persona,
		sucursal
	) VALUES (
		$1, $2, $3, $4, $5, $6, $7, $8, $9, $10
	);`

	_, err = tx.Exec(ctx, query, a.ID, a.Comitente, a.Empresa, a.Config, a.Fecha, a.Partidas, a.Usuario, time.Now(), a.Persona, a.Sucursal)
	if err != nil {
		return deferror.DB(err, "persistiendo apli")
	}

	return
}

// Lee de la tablas de partidas, las partidas que voy a dar vuelta
func traerSaldos(ctx context.Context, tx pgx.Tx, ids tipos.GrupoUUID) (out map[uuid.UUID]agg, err error) {
	out = map[uuid.UUID]agg{}
	query := fmt.Sprintf(`
	SELECT
		comitente,
		cuenta,
		centro,
		persona, 
		sucursal,
		caja,
		producto,
		sku,
		deposito,
		unicidad,
		partida_aplicada,
		contrato,
		empresa,
		sum(monto)
	FROM partidas
	WHERE partida_aplicada IN %v
	GROUP BY
		comitente,
		cuenta,
		centro,
		persona, 
		sucursal,
		caja,
		producto,
		sku,
		deposito,
		unicidad,
		partida_aplicada,
		contrato,
		empresa
	;`, uuidsToBytes(ids))

	rows, err := tx.Query(ctx, query)
	if err != nil {
		return out, deferror.DB(err, "ejectuando query SELECT para determinar saldos")
	}
	defer rows.Close()

	// Escaneo cada saldo
	for rows.Next() {
		k := agg{}
		centro := new(int)
		persona := &uuid.UUID{}
		sucursal := new(int)
		caja := new(int)
		producto := &uuid.UUID{}
		sku := &uuid.UUID{}
		deposito := new(int)
		unicidad := &uuid.UUID{}
		partidaAplicada := &uuid.UUID{}
		contrato := new(int)
		monto := new(int64)

		err = rows.Scan(
			&k.Comitente,
			&k.Cuenta,
			&centro,
			&persona,
			&sucursal,
			&caja,
			&producto,
			&sku,
			&deposito,
			&unicidad,
			&partidaAplicada,
			&contrato,
			&k.Empresa,
			&monto,
		)
		if err != nil {
			return out, deferror.DB(err, "escaneando row")
		}
		if centro != nil {
			k.Centro = *centro
		}
		if persona != nil {
			k.Persona = *persona
		}
		if sucursal != nil {
			k.Sucursal = *sucursal
		}
		if caja != nil {
			k.Caja = *caja
		}
		if producto != nil {
			k.Producto = *producto
		}
		if sku != nil {
			k.SKU = *sku
		}
		if deposito != nil {
			k.Deposito = *deposito
		}
		if unicidad != nil {
			k.Unicidad = *unicidad
		}
		if partidaAplicada != nil {
			k.PartidaAplicada = *partidaAplicada
		}
		if contrato != nil {
			k.Contrato = *contrato
		}
		if monto != nil {
			k.Monto = dec.D2(*monto)
		}
		out[*partidaAplicada] = k
	}
	return
}
