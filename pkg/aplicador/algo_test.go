package aplicador

import (
	"testing"

	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
)

func TestCalzar(t *testing.T) {

	// Se calzan todos entre sí
	{
		pp := []agg{
			{
				PartidaAplicada: uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e129"),
				Monto:           dec.NewD2(100),
			},
			{
				PartidaAplicada: uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e130"),
				Monto:           dec.NewD2(-100),
			},
			{
				PartidaAplicada: uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e131"),
				Monto:           dec.NewD2(50),
			},
			{
				PartidaAplicada: uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e132"),
				Monto:           dec.NewD2(-18),
			},
			{
				PartidaAplicada: uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e133"),
				Monto:           dec.NewD2(-10),
			},
			{
				PartidaAplicada: uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e134"),
				Monto:           dec.NewD2(-22),
			},
		}

		out, err := calzar(pp, dec.NewD2(0))
		assert.Nil(t, err)
		assert.Len(t, out, 6)

		sum := dec.D2(0)
		for _, v := range out {
			sum += v.Monto
			// fmt.Println(i, v.Monto)
		}
		assert.Equal(t, sum, dec.NewD2(0))
	}

	// Parcial, debería lograrse
	{
		pp := []agg{
			{
				PartidaAplicada: uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e129"),
				Monto:           dec.NewD2(100),
			},
			{
				PartidaAplicada: uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e130"),
				Monto:           dec.NewD2(-100),
			},
			{
				PartidaAplicada: uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e131"),
				Monto:           dec.NewD2(50),
			},
			{
				PartidaAplicada: uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e132"),
				Monto:           dec.NewD2(-18),
			},
			{
				PartidaAplicada: uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e133"),
				Monto:           dec.NewD2(-10),
			},
			{
				PartidaAplicada: uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e134"),
				Monto:           dec.NewD2(-22),
			},
		}

		out, err := calzar(pp, dec.NewD2(10))
		assert.Nil(t, err)
		assert.Len(t, out, 6)

		sum := dec.D2(0)
		for _, v := range out {
			sum += v.Monto
			// fmt.Println(i, v.Monto)
		}

		out, err = calzar(pp, dec.NewD2(-10))
		assert.Nil(t, err)
		assert.Len(t, out, 6)

		sum = dec.D2(-10)
		for _, v := range out {
			sum += v.Monto
		}
	}

	// iGrande
	{
		pp := []agg{
			{
				PartidaAplicada: uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e129"),
				Monto:           dec.NewD2(700),
			},
			{
				PartidaAplicada: uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e130"),
				Monto:           dec.NewD2(700),
			},
			{
				PartidaAplicada: uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e131"),
				Monto:           dec.NewD2(9800),
			},
			{
				PartidaAplicada: uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e132"),
				Monto:           dec.NewD2(9800.01),
			},
		}

		out, err := calzar(pp, dec.NewD2(21000))
		assert.Nil(t, err)
		assert.Len(t, out, 4)

		sum := dec.D2(0)
		for _, v := range out {
			sum += v.Monto
			// fmt.Println(i, v.Monto)
		}
		assert.Equal(t, sum, dec.NewD2(21000))
	}
}
