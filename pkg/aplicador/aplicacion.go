package aplicador

import (
	"database/sql/driver"
	"encoding/json"
	"time"

	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
)

// Aplicacion
type Aplicacion struct {
	ID        uuid.UUID
	Comitente int
	Empresa   int `json:",string"`
	Config    int `json:",string"`
	Fecha     fecha.Fecha
	Persona   *uuid.UUID
	Sucursal  *int `json:",string"`
	Partidas  Partidas
	Usuario   string
	CreatedAt *time.Time
}

func (a *Aplicacion) TableName() string {
	return "apli"
}

type Partida struct {

	// Puede que una op genere varias partidas patrimoniales. Para el usuario
	// es más razonable que seleccione el comprobante completo y el programa
	// se encargue de ver como parte el monto en las cuentas.
	OpID          uuid.UUID
	PartidasID    tipos.GrupoUUID
	FechaContable fecha.Fecha
	FechaVto      fecha.Fecha
	CompNombre    string
	NCompStr      string
	MontoOriginal dec.D2
	Saldo         dec.D2
	Grupo         int
	Aplicacion    dec.D2
	Detalle       string `json:",omitempty"`
}
type Partidas []Partida

// Value cumple con la interface SQL
func (p Partidas) Value() (driver.Value, error) {
	return json.Marshal(p)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (p *Partidas) Scan(src interface{}) error {
	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New("Type assertion error .([]byte)")
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, p)
	if err != nil {
		return errors.Wrap(err, "Unmarshalling")
	}

	return nil
}

/*
CREATE TABLE apli (
	id UUID PRIMARY KEY,
	comitente INT REFERENCES comitentes,
	empresa INT REFERENCES empresas,
	config INT REFERENCES apli_configs,
	fecha DATE,
	partidas JSONB,
	usuario STRING,
	created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW()
)
*/
