package config

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/jackc/pgx/v4/pgxpool"
)

func create(ctx context.Context, conn *pgxpool.Pool, req Config) (err error) {
	query := `INSERT INTO apli_configs (comitente, empresa, nombre, 
		cuentas, grupos_cuentas, comp, sugerir_fecha, agrupar_partidas, por_persona, por_fecha_vto)
		VALUES ($1, $2, $3,$4,$5,$6,$7,$8,$9,$10)`
	_, err = conn.Exec(ctx, query, req.Comitente, req.Empresa, req.Nombre,
		req.Cuentas, req.GruposCuentas, req.Comp, req.SugerirFecha, req.AgruparPartidas,
		req.PorPersona, req.PorFechaVto)
	if err != nil {
		return deferror.DB(err, "inserting")
	}
	return
}

func readOne(ctx context.Context, conn *pgxpool.Pool, req ReadOneReq) (out Config, err error) {

	query := `SELECT id, comitente, empresa, nombre, cuentas, grupos_cuentas, comp, sugerir_fecha, agrupar_partidas, por_persona, por_fecha_vto
		FROM apli_configs
		WHERE comitente=$1 AND id=$2
	`
	err = conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(&out.ID, &out.Comitente, &out.Empresa,
		&out.Nombre, &out.Cuentas, &out.GruposCuentas, &out.Comp, &out.SugerirFecha,
		&out.AgruparPartidas, &out.PorPersona, &out.PorFechaVto)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}
	return
}

type ReadManyReq struct {
	Comitente int `json:",string"`
	Empresa   int `json:",string"`
}

func readMany(ctx context.Context, conn *pgxpool.Pool, req ReadManyReq) (out []Config, err error) {
	out = []Config{}

	query := `SELECT  id, comitente, empresa, nombre, cuentas, grupos_cuentas, comp, sugerir_fecha, agrupar_partidas, por_persona, por_fecha_vto
	FROM apli_configs
	WHERE comitente=$1 AND empresa=$2
	ORDER BY nombre`

	rows, err := conn.Query(ctx, query, req.Comitente, req.Empresa)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		v := Config{}
		err = rows.Scan(&v.ID, &v.Comitente, &v.Empresa, &v.Nombre, &v.Cuentas, &v.GruposCuentas, &v.Comp, &v.SugerirFecha, &v.AgruparPartidas, &v.PorPersona, &v.PorFechaVto)
		if err != nil {
			return out, deferror.DB(err, "escaneando")
		}
		out = append(out, v)
	}
	return
}

func update(ctx context.Context, conn *pgxpool.Pool, req Config) (err error) {
	query := `UPDATE apli_configs 
	SET nombre=$3, cuentas=$4, grupos_cuentas=$5, comp=$6, sugerir_fecha=$7, agrupar_partidas=$8, por_persona=$9, por_fecha_vto=$10
	WHERE comitente=$1 AND id=$2`

	res, err := conn.Exec(ctx, query, req.Comitente, req.ID,
		req.Nombre, req.Cuentas, req.GruposCuentas, req.Comp, req.SugerirFecha, req.AgruparPartidas,
		req.PorPersona, req.PorFechaVto)
	if err != nil {
		return deferror.DB(err, "updating")
	}
	if res.RowsAffected() == 0 {
		return errors.New("no se modificó ningún registro")
	}

	return
}

func delete(ctx context.Context, conn *pgxpool.Pool, req ReadOneReq) (err error) {

	{ // Estaba usada?
		count := 0
		err = conn.QueryRow(ctx, "SELECT COUNT(id) FROM apli WHERE comitente=$1 AND config=$2",
			req.Comitente, req.ID).Scan(&count)
		if err != nil {
			return deferror.DB(err, "verificado si había sido utilizada")
		}
		if count > 0 {
			return deferror.Validationf("No se puede borrar la configuración porque la misma fue usada %v veces", count)
		}
	}

	res, err := conn.Exec(ctx, "DELETE FROM apli_configs WHERE comitente=$1 AND id=$2", req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "borrando")
	}
	if res.RowsAffected() == 0 {
		return errors.New("no se borró ningún registro")
	}

	return
}
