package config

import "bitbucket.org/marcos19/one/pkg/tipos"

type Config struct {
	ID              int `json:",string"`
	Comitente       int `json:",string"`
	Empresa         int `json:",string"`
	Nombre          string
	Comp            int `json:",string"`
	GruposCuentas   tipos.GrupoInts
	Cuentas         tipos.GrupoInts
	SugerirFecha    bool
	AgruparPartidas bool

	// Si es true usa fecha vto, sino contable.
	PorFechaVto bool

	// Si es true, el menú solicita que se seleccione una persona para traer las
	// partidas disponibles.
	PorPersona bool
}

func (c *Config) TableName() string {
	return "apli_configs"
}

/*
CREATE TABLE apli_configs (
	id SERIAL PRIMARY KEY,
	comitente INT NOT NULL REFERENCES comitentes,
	empresa INT NOT NULL REFERENCES empresas,
	nombre STRING,
	grupo_cuentas INT[],
	cuentas INT[]
);
*/
