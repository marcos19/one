package config

import (
	"context"
	"strings"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler da acceso al a todos los routes de personas.
type Handler struct {
	conn *pgxpool.Pool
}

func NewHandler(c HandlerArgs) (h *Handler, err error) {

	if c.Conn == nil {
		return h, errors.New("Conn no puede ser nil")
	}

	h = &Handler{
		conn: c.Conn,
	}
	return
}

type HandlerArgs struct {
	Conn *pgxpool.Pool
}

func (h *Handler) Create(ctx context.Context, req Config) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se definió comitente")
	}
	if req.Empresa == 0 {
		return deferror.Validation("no se definió empresa")
	}
	req.Nombre = strings.Trim(req.Nombre, " ")
	if req.Nombre == "" {
		return deferror.Validation("no se definió empresa")
	}

	// Inserto
	err = create(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "insertando config")
	}
	return
}

func (h *Handler) ReadMany(ctx context.Context, req ReadManyReq) (out []Config, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se definió comitente")
	}
	if req.Empresa == 0 {
		return out, deferror.Validation("no se definió empresa")
	}

	// Busco
	out, err = readMany(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando configs")
	}
	return
}

type ReadOneReq struct {
	Comitente int `json:",string"`
	ID        int `json:",string"`
}

func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (out Config, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se definió comitente")
	}
	if req.ID == 0 {
		return out, deferror.Validation("no se definió ID a buscar")
	}

	// Busco
	out, err = readOne(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "insertando config")
	}
	return
}

func (h *Handler) Update(ctx context.Context, req Config) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se definió comitente")
	}
	if req.ID == 0 {
		return deferror.Validation("no se definió ID a buscar")
	}

	// Busco si estaba
	err = update(ctx, h.conn, req)
	if err != nil {
		return errors.Wrap(err, "modificando config")
	}

	return
}

func (h *Handler) Delete(ctx context.Context, req ReadOneReq) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se definió comitente")
	}
	if req.ID == 0 {
		return deferror.Validation("no se definió ID a eliminar")
	}

	// Busco si estaba
	err = delete(ctx, h.conn, req)
	if err != nil {
		return deferror.Validation("buscando config a modificar")
	}

	return
}
