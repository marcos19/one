package vectorial

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFit(t *testing.T) {

	{ // Square
		div := Rect{Width: 1, Height: 1}
		t.Run("square equal", func(t *testing.T) {
			img := Fit(Rect{Width: 1, Height: 1}, div)
			assert.Equal(t, 1., img.Width)
			assert.Equal(t, 1., img.Height)
		})
		t.Run("square (grow)", func(t *testing.T) {
			img := Fit(Rect{Width: 0.5, Height: 0.5}, div)
			assert.Equal(t, 1., img.Width)
			assert.Equal(t, 1., img.Height)
		})
		t.Run("square (shrink)", func(t *testing.T) {
			img := Fit(Rect{Width: 2, Height: 2}, div)
			assert.Equal(t, 1., img.Width)
			assert.Equal(t, 1., img.Height)
		})
	}

	{ // Rect
		div := Rect{Width: 1, Height: 2}
		t.Run("rect (equal)", func(t *testing.T) {
			img := Fit(Rect{Width: 1, Height: 2}, div)
			assert.Equal(t, 1., img.Width)
			assert.Equal(t, 2., img.Height)
		})
		t.Run("rect (shrink same ratio)", func(t *testing.T) {
			img := Fit(Rect{Width: 2, Height: 4}, div)
			assert.Equal(t, 1., img.Width)
			assert.Equal(t, 2., img.Height)
		})
		t.Run("rect (grow same ratio)", func(t *testing.T) {
			img := Fit(Rect{Width: 0.5, Height: 1}, div)
			assert.Equal(t, 1., img.Width)
			assert.Equal(t, 2., img.Height)
		})
		t.Run("rect (shrink different ratio)", func(t *testing.T) {
			img := Fit(Rect{Width: 2, Height: 2}, div)
			assert.Equal(t, 1., img.Width)
			assert.Equal(t, 1., img.Height)
		})
		t.Run("rect (grow different ratio)", func(t *testing.T) {
			img := Fit(Rect{Width: 0.5, Height: 0.5}, div)
			assert.Equal(t, 1., img.Width)
			assert.Equal(t, 1., img.Height)
		})
	}

}
