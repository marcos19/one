package vectorial

type Rect struct {
	X      float64
	Y      float64
	Width  float64
	Height float64
}

// Increase or decrease inner size to fit inside outer. Relation kept.
func Fit(inner, outer Rect) (out Rect) {

	h := outer.Height
	w := h * inner.Width / inner.Height

	if w > outer.Width {
		w = outer.Width
		h = w / inner.Width * inner.Height
	}

	out = inner
	out.Height = h
	out.Width = w

	return
}

// Centers horizontally.
func AlignCenter(inner, outer Rect) (out Rect) {

	out = inner
	out.X = (outer.Width-inner.Width)/2 + outer.X
	out.Y = (outer.Height-inner.Height)/2 + outer.Y

	return
}

// Centers horizontally.
func AlignRight(inner, outer Rect) (out Rect) {

	out = inner
	out.X = outer.Width - inner.Width + outer.X
	return
}

// Centers vertically.
func AlignVerticalCenter(inner, outer Rect) (out Rect) {

	out = inner
	out.Y = (outer.Height-inner.Height)/2 + outer.Y
	return
}
