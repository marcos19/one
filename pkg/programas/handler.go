package programas

import (
	"encoding/json"
	"net/http"
	"path"
	"strings"

	"bitbucket.org/marcos19/one/pkg/deferror"
)

// Handler es el encargado de recibir los http requests y emitir los responses.
type Handler struct {
}

// NewHandler instancia el handler de fórmulas.
func NewHandler() (h *Handler) {
	h = &Handler{}
	return h
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string

	// Saco lo que viene después
	head, r.URL.Path = shiftPath(r.URL.Path)
	head = strings.TrimLeft(head, "/")

	switch head {
	case "many":
		HandleMany()(w, r)

	default:
		deferror.Frontend(deferror.NotFound(head), w, r)
	}
}

// HandleMany devuelve el listado de todos los programas
func HandleMany() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		// Devuelvo JSON
		err := json.NewEncoder(w).Encode(&Programas)
		if err != nil {
			deferror.Frontend(err, w, r)
			return
		}
	}
}

// ShiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}
