// Package programas define los programas que trabajan sobre las operaciones.
// Cuando se quiere profundizar sobre una determinada operación que está
// registrada, podría ser una minuta, factura de venta, etc.
// En cada operación hay un campo que define cual fue el programa que la creó.
//
package programas

const (
	// Minuta es el programa con el que se registran las minutas
	Minuta = "Minuta"
	// FacturaDeVenta es el programa con el que se persisten las facturas de venta.
	FacturaDeVenta = "Factura de venta"
)

// Programas es un listado de todos los programas con los que trabaja la aplicación.
var Programas = []string{
	Minuta,
	FacturaDeVenta,
}
