package config

import "context"

type ReaderOne interface {
	ReadOne(context.Context, ReadOneReq) (Config, error)
}
