package config

import (
	"context"
	"fmt"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/productos"
	"bitbucket.org/marcos19/one/pkg/productos/categorias"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/niler"
	"github.com/dgraph-io/ristretto"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/rs/zerolog/log"
)

type Handler struct {
	conn              *pgxpool.Pool
	cache             *ristretto.Cache
	cuentasHandler    cuentas.ReaderOne
	productosHandler  *productos.Handler
	categoriasHandler *categorias.Handler
	permisoDeleter    Deleter
}

// Borra los permisos de una determinada config
type Deleter interface {
	DeletePermisosDeConfig(ctx context.Context, tx pgx.Tx, comitente int, id int) error
}

func NewHandler(c HandlerArgs) (h *Handler, err error) {
	if c.Conn == nil {
		return h, errors.New("Conn no puede ser nil")
	}
	if c.Cache == nil {
		return h, errors.New("Cache no puede ser nil")
	}
	if c.ProductosHandler == nil {
		return h, errors.New("ProductosHandler no puede ser nil")
	}
	if c.CategoriasHandler == nil {
		return h, errors.New("CategoriasHandler no puede ser nil")
	}
	if niler.IsNil(c.CuentasHandler) {
		return h, errors.New("cuentas.ReaderOne no puede ser nil")
	}
	if niler.IsNil(c.PermisoDeleter) {
		return h, errors.New("Deleter no puede ser nil")
	}

	h = &Handler{
		conn:              c.Conn,
		cache:             c.Cache,
		cuentasHandler:    c.CuentasHandler,
		productosHandler:  c.ProductosHandler,
		categoriasHandler: c.CategoriasHandler,
		permisoDeleter:    c.PermisoDeleter,
	}

	return
}

type HandlerArgs struct {
	Conn              *pgxpool.Pool
	Cache             *ristretto.Cache
	CuentasHandler    cuentas.ReaderOne
	ProductosHandler  *productos.Handler
	CategoriasHandler *categorias.Handler
	PermisoDeleter    Deleter
}

type ReadOneReq struct {
	Comitente int
	ID        int `json:",string"`
}

func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (out Config, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se determinó comitente")
	}
	if req.ID == 0 {
		return out, deferror.Validation("no se determinó ID")
	}

	// Busco en cache
	val, estaba := h.cache.Get(hashInt(req))
	if estaba {
		out, ok := val.(Config)
		if ok && out.Comitente == req.Comitente {
			log.Debug().Str("tipo", "fact_config").Msg("devolviendo de cache")
			return out, nil
		}
	}

	// No estaba en cache
	out, err = readOne(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando config por ID")
	}

	if out.ReingresoPagoCuentasPermitidas == nil {
		out.ReingresoPagoCuentasPermitidas = tipos.GrupoInts{}
	}

	// Guardo en cache
	h.cache.Set(hashInt(req), out, 0)

	return
}

type ReadManyReq struct {
	Comitente int
	Empresa   int `json:",string"`
	Tipo      int `json:",string"`
	Grupo     string
	Activas   string // ["", "todas", "inactivas"]
	User      string
}

func (h *Handler) ReadMany(ctx context.Context, req ReadManyReq) (out []Config, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, errors.New("no se determinó comitente")
	}

	// Busco
	out, err = readMany(ctx, h.conn, req)
	if err != nil {
		errors.Wrap(err, "buscando configs de programa factura")
		return
	}

	return
}

func (h *Handler) Lookup(ctx context.Context, req ReadManyReq, user string) (out []Config, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, errors.New("no se determinó comitente")
	}

	req.User = user

	// Busco
	out, err = readMany(ctx, h.conn, req)
	if err != nil {
		errors.Wrap(err, "buscando configs de programa factura")
		return
	}

	return
}

func (h *Handler) Create(ctx context.Context, req Config) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se determinó comitente")
	}
	if req.Nombre == "" {
		return deferror.Validation("no se ingresó nombre")
	}
	if !req.GeneraReingreso {
		req.ReingresoEmpresa = nil
		req.ReingresoCuentaIVA = nil
		req.ReingresoCuentaPatrimonial = nil
		req.ReingresoCuentaReingreso = nil
	}

	// Persisto
	err = create(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "insertando config")
	}
	return
}

func (h *Handler) Update(ctx context.Context, req Config) (err error) {

	// Valido
	if req.Comitente == 0 {
		return errors.New("no se determinó comitente")
	}
	if req.ID == 0 {
		return errors.New("no se determinó ID")
	}

	// Por las dudas lo borro antes de modificar
	h.cache.Del(hashInt(ReadOneReq{Comitente: req.Comitente, ID: req.ID}))

	// Hago update
	err = update(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "modificando transacción")
	}
	return
}

func (h *Handler) Delete(ctx context.Context, req ReadOneReq) (err error) {

	// Valido
	if req.Comitente == 0 {
		return errors.New("no se determinó comitente")
	}
	if req.ID == 0 {
		return errors.New("no se determinó ID")
	}

	h.cache.Del(hashInt(ReadOneReq{Comitente: req.Comitente, ID: req.ID}))

	// Borro
	err = delete(ctx, h.conn, req, h.permisoDeleter)
	if err != nil {
		return deferror.DB(err, "borrando config")
	}
	return
}

type DupReq struct {
	Comitente    int `json:",string"`
	ID           int `json:",string"`
	NuevaEmpresa int `json:",string"`
}

func (h *Handler) Duplicar(ctx context.Context, req DupReq) (err error) {

	// Valido
	if req.Comitente == 0 {
		return errors.New("no se determinó comitente")
	}
	if req.ID == 0 {
		return errors.New("no se determinó ID")
	}
	if req.NuevaEmpresa == 0 {
		return errors.New("no se determinó nueva empresa")
	}

	// Busco
	prev, err := h.ReadOne(ctx, ReadOneReq{Comitente: req.Comitente, ID: req.ID})
	if err != nil {
		return fmt.Errorf("buscando config para copiar: %w", err)
	}
	prev.Empresa = req.NuevaEmpresa

	// Creo nueva
	err = h.Create(ctx, prev)
	if err != nil {
		return err
	}
	return
}

func hashInt(req ReadOneReq) string {
	return fmt.Sprintf("%v/%v/%v", "FactConfig", req.Comitente, req.ID)
}
