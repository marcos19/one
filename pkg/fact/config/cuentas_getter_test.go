package config

import (
	"context"
	"testing"

	"bitbucket.org/marcos19/one/pkg/fact/internal/opprod"
	"bitbucket.org/marcos19/one/pkg/productos"
	"bitbucket.org/marcos19/one/pkg/productos/categorias"
	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDeterminarImputacion(t *testing.T) {

	imps := []ImputacionContable{
		{
			Proveedores:                2000,
			ProductosCompradosARecibir: 2001,
			IVA:                        2300,
		},
	}

	prodGetter := prodGetter{}
	catGetter := catGetter{}
	out, err := DeterminarImputacion(
		context.Background(),
		1,
		uuid.FromStringOrNil("ac99ef64-e470-4ce5-b613-29d027c20174"),
		imps,
		&prodGetter,
		&catGetter,
		opprod.ProductosCompradosARecibir,
	)

	require.Nil(t, err)
	assert.Equal(t, 2001, out)

}

type prodGetter struct{}

func (p *prodGetter) ReadOne(ctx context.Context, req productos.ReadOneReq) (out productos.Producto, err error) {
	if req.ID == uuid.FromStringOrNil("ac99ef64-e470-4ce5-b613-29d027c20174") {
		out = productos.Producto{
			ID:        req.ID,
			Nombre:    "Mocking product",
			Categoria: 500,
		}
		return
	}
	return out, errors.Errorf("no product found")
}

type catGetter struct{}

func (c *catGetter) CategoriaSuperior(ctx context.Context, req categorias.ReadOneReq) (out *categorias.Categoria, err error) {
	cats := []categorias.Categoria{}

	{
		id := 500
		padre := 50
		sofas := categorias.Categoria{
			ID:      &id,
			Nombre:  "Sofas",
			PadreID: &padre,
		}
		cats = append(cats, sofas)
	}
	{
		id := 50
		sofas := categorias.Categoria{
			ID:     &id,
			Nombre: "Furnitures",
		}
		cats = append(cats, sofas)
	}

	switch req.ID {
	case 500:
		return &cats[1], nil

	case 50:
		return nil, nil
	}
	return out, errors.Errorf("no category found for ID %v", req.ID)
}
