package config

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/fact/permisos"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	"github.com/jackc/pgx/v4/pgxpool"
)

func readOne(ctx context.Context, conn *pgxpool.Pool, req ReadOneReq) (out Config, err error) {

	query := `SELECT 
	id, 
	comitente, 
	nombre, 
	empresa, 
	tipo_op,
	detalle,
	sugiere_fecha_actual,
	condicion_pago,
	permite_productos,
	productos_valorizados,
	productos_se,
	productos_se_aplican_a_cuentas,
	usa_lista_de_precios,
	trabaja_con_precio_final,
	precios_desde,
	permite_modificar_precios,
	permite_productos_por_cuenta_terceros,
	permite_conceptos,
	permite_partidas_directas,
	permite_valores,
	buscador_personas_extendido,
	emite_recibo,
	cuentas_anticipo,
	unicidades_que_se_crean,
	diferencia_maxima_redondeo,
	imputaciones,
	signo_renglon_producto,
	signo_renglon_valor,
	no_hace_asiento,
	hace_asiento_estandar,
	hace_asiento_costo,
	cuenta_renglon_genera_aplicacion,
	cuenta_patrimonial_genera_aplicacion,
	es_nota_de_credito,
	categorias_ignoradas_en_factura,
	inactiva,
	grupo,
	cuenta_redondeo,
	mostrar_saldo,
	grupo_cuentas_saldo,
	controla_limite_credito,
	permite_aplicar,
	detalle_obligatorio_en_partidas,
	copia_persona_de_cabecera,
	genera_reingreso,
	reingreso_empresa,
	reingreso_cuenta_iva,
	reingreso_cuenta_reingreso,
	reingreso_cuenta_patrimonial,
	muestra_depositos,
	opprod,
	especificar_unicidades,
	grupo_cuentas_aplicar_recibo,
	permite_imputacion_diferida,
	permite_detalle_productos,
	totaliza_desde_partidas_fact,
	permite_precio_cero,
	genera_reingreso_pago,
	reingreso_pago_cuentas_permitidas,
	reingreso_obligatorio,
	reingreso_pago_obligatorio
	FROM fact_config
	WHERE comitente=$1 AND id=$2`

	err = conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(
		&out.ID,
		&out.Comitente,
		&out.Nombre,
		&out.Empresa,
		&out.TipoOp,
		&out.Detalle,
		&out.SugiereFechaActual,
		&out.CondicionPago,
		&out.PermiteProductos,
		&out.ProductosValorizados,
		&out.ProductosSe,
		&out.ProductosSeAplicanACuentas,
		&out.UsaListaDePrecios,
		&out.TrabajaConPrecioFinal,
		&out.PreciosDesde,
		&out.PermiteModificarPrecios,
		&out.PermiteProductosPorCuentaTerceros,
		&out.PermiteConceptos,
		&out.PermitePartidasDirectas,
		&out.PermiteValores,
		&out.BuscadorPersonasExtendido,
		&out.EmiteRecibo,
		&out.CuentasAnticipo,
		&out.UnicidadesQueSeCrean,
		&out.DiferenciaMaximaRedondeo,
		&out.Imputaciones,
		&out.SignoRenglonProducto,
		&out.SignoRenglonValor,
		&out.NoHaceAsiento,
		&out.HaceAsientoEstandar,
		&out.HaceAsientoCosto,
		&out.CuentaRenglonGeneraAplicacion,
		&out.CuentaPatrimonialGeneraAplicacion,
		&out.EsNotaDeCredito,
		&out.CategoriasIgnoradasEnFactura,
		&out.Inactiva,
		&out.Grupo,
		&out.CuentaRedondeo,
		&out.MostrarSaldo,
		&out.GrupoCuentasSaldo,
		&out.ControlaLimiteCredito,
		&out.PermiteAplicar,
		&out.DetalleObligatorioEnPartidas,
		&out.CopiaPersonaDeCabecera,
		&out.GeneraReingreso,
		&out.ReingresoEmpresa,
		&out.ReingresoCuentaIVA,
		&out.ReingresoCuentaReingreso,
		&out.ReingresoCuentaPatrimonial,
		&out.MuestraDepositos,
		&out.OpProd,
		&out.EspecificarUnicidades,
		&out.GrupoCuentasAplicarRecibo,
		&out.PermiteImputacionDiferida,
		&out.PermiteDetalleProductos,
		&out.TotalizaDesdePartidasFact,
		&out.PermitePrecioCero,
		&out.GeneraReingresoPago,
		&out.ReingresoPagoCuentasPermitidas,
		&out.ReingresoObligatorio,
		&out.ReingresoPagoObligatorio,
	)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}

	out.Permisos, err = permisos.PorConfig(ctx, conn, permisos.PorConfigReq{
		Comitente: req.Comitente,
		ID:        req.ID,
	})
	if err != nil {
		return out, errors.Wrap(err, "buscando permisos de la config")
	}
	if out.Permisos == nil {
		out.Permisos = []permisos.Permiso{}
	}
	return
}

// Si se ingresa usuario, trae sólo las config que el usuario tiene habilitadas para create
func readMany(ctx context.Context, conn *pgxpool.Pool, req ReadManyReq) (out []Config, err error) {
	out = []Config{}

	if req.Comitente == 0 {
		return out, errors.New("no se determinó comitente")
	}

	ww := []string{"comitente=$1"}
	pp := []interface{}{req.Comitente}

	if req.Empresa != 0 {
		pp = append(pp, req.Empresa)
		ww = append(ww, fmt.Sprintf("empresa=$%v", len(pp)))
	}
	if req.Tipo != 0 {
		pp = append(pp, req.Tipo)
		ww = append(ww, fmt.Sprintf("tipo_op=$%v", len(pp)))
	}
	if req.Grupo != "" {
		pp = append(pp, req.Grupo)
		ww = append(ww, fmt.Sprintf("grupo=$%v", len(pp)))
	}

	// Si tiene user es porque estoy haciendo un lookup
	if req.User != "" {
		pp = append(pp, req.User)
		ww = append(ww, fmt.Sprintf(`
		id IN (
			SELECT config FROM fact_permisos WHERE (
				usuario=$%v OR 
				(usuario IS NULL AND grupo_usuario IS NULL) OR 
				grupo_usuario IN (
					SELECT id
					FROM usuarios_grupos
					WHERE comitente=$%v AND usuarios @> ARRAY[$%v]
				)
			) AND "create"=true
		)`, len(pp), 1, len(pp)))
	}

	switch req.Activas {
	case "":
		ww = append(ww, "inactiva=false")
	case "inactivas":
		ww = append(ww, "inactiva=true")
	}
	where := "WHERE " + strings.Join(ww, " AND ")

	query := fmt.Sprintf(`SELECT 
	id, 
	comitente, 
	nombre, 
	empresa, 
	tipo_op,
	detalle,
	sugiere_fecha_actual,
	condicion_pago,
	permite_productos,
	productos_valorizados,
	productos_se,
	productos_se_aplican_a_cuentas,
	usa_lista_de_precios,
	trabaja_con_precio_final,
	precios_desde,
	permite_modificar_precios,
	permite_productos_por_cuenta_terceros,
	permite_conceptos,
	permite_partidas_directas,
	permite_valores,
	buscador_personas_extendido,
	emite_recibo,
	cuentas_anticipo,
	unicidades_que_se_crean,
	diferencia_maxima_redondeo,
	imputaciones,
	signo_renglon_producto,
	signo_renglon_valor,
	no_hace_asiento,
	hace_asiento_estandar,
	hace_asiento_costo,
	cuenta_renglon_genera_aplicacion,
	cuenta_patrimonial_genera_aplicacion,
	es_nota_de_credito,
	categorias_ignoradas_en_factura,
	inactiva,
	grupo,
	cuenta_redondeo,
	mostrar_saldo,
	grupo_cuentas_saldo,
	controla_limite_credito,
	permite_aplicar,
	detalle_obligatorio_en_partidas,
	copia_persona_de_cabecera,
	genera_reingreso,
	reingreso_empresa,
	reingreso_cuenta_iva,
	reingreso_cuenta_reingreso,
	reingreso_cuenta_patrimonial,
	muestra_depositos, 
	opprod,
	especificar_unicidades,
	grupo_cuentas_aplicar_recibo,
	permite_imputacion_diferida,
	permite_detalle_productos,
	totaliza_desde_partidas_fact,
	permite_precio_cero,
	genera_reingreso_pago,
	reingreso_pago_cuentas_permitidas,
	reingreso_obligatorio,
	reingreso_pago_obligatorio
FROM fact_config
%v 
ORDER BY nombre`, where)

	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()
	for rows.Next() {
		v := Config{}
		err = rows.Scan(
			&v.ID,
			&v.Comitente,
			&v.Nombre,
			&v.Empresa,
			&v.TipoOp,
			&v.Detalle,
			&v.SugiereFechaActual,
			&v.CondicionPago,
			&v.PermiteProductos,
			&v.ProductosValorizados,
			&v.ProductosSe,
			&v.ProductosSeAplicanACuentas,
			&v.UsaListaDePrecios,
			&v.TrabajaConPrecioFinal,
			&v.PreciosDesde,
			&v.PermiteModificarPrecios,
			&v.PermiteProductosPorCuentaTerceros,
			&v.PermiteConceptos,
			&v.PermitePartidasDirectas,
			&v.PermiteValores,
			&v.BuscadorPersonasExtendido,
			&v.EmiteRecibo,
			&v.CuentasAnticipo,
			&v.UnicidadesQueSeCrean,
			&v.DiferenciaMaximaRedondeo,
			&v.Imputaciones,
			&v.SignoRenglonProducto,
			&v.SignoRenglonValor,
			&v.NoHaceAsiento,
			&v.HaceAsientoEstandar,
			&v.HaceAsientoCosto,
			&v.CuentaRenglonGeneraAplicacion,
			&v.CuentaPatrimonialGeneraAplicacion,
			&v.EsNotaDeCredito,
			&v.CategoriasIgnoradasEnFactura,
			&v.Inactiva,
			&v.Grupo,
			&v.CuentaRedondeo,
			&v.MostrarSaldo,
			&v.GrupoCuentasSaldo,
			&v.ControlaLimiteCredito,
			&v.PermiteAplicar,
			&v.DetalleObligatorioEnPartidas,
			&v.CopiaPersonaDeCabecera,
			&v.GeneraReingreso,
			&v.ReingresoEmpresa,
			&v.ReingresoCuentaIVA,
			&v.ReingresoCuentaReingreso,
			&v.ReingresoCuentaPatrimonial,
			&v.MuestraDepositos,
			&v.OpProd,
			&v.EspecificarUnicidades,
			&v.GrupoCuentasAplicarRecibo,
			&v.PermiteImputacionDiferida,
			&v.PermiteDetalleProductos,
			&v.TotalizaDesdePartidasFact,
			&v.PermitePrecioCero,
			&v.GeneraReingresoPago,
			&v.ReingresoPagoCuentasPermitidas,
			&v.ReingresoObligatorio,
			&v.ReingresoPagoObligatorio,
		)
		if err != nil {
			return out, deferror.DB(err, "escaneando")
		}
		if v.ReingresoPagoCuentasPermitidas == nil {
			v.ReingresoPagoCuentasPermitidas = tipos.GrupoInts{}
		}
		out = append(out, v)
	}
	return
}

func create(ctx context.Context, conn *pgxpool.Pool, req Config) (err error) {

	query := `INSERT INTO fact_config (
	comitente, 
	nombre, 
	empresa, 
	tipo_op,
	detalle,
	sugiere_fecha_actual,
	condicion_pago,
	permite_productos,
	productos_valorizados,
	productos_se,
	productos_se_aplican_a_cuentas,
	usa_lista_de_precios,
	trabaja_con_precio_final,
	precios_desde,
	permite_modificar_precios,
	permite_productos_por_cuenta_terceros,
	permite_conceptos,
	permite_partidas_directas,
	permite_valores,
	buscador_personas_extendido,
	emite_recibo,
	cuentas_anticipo,
	unicidades_que_se_crean,
	diferencia_maxima_redondeo,
	imputaciones,
	signo_renglon_producto,
	signo_renglon_valor,
	no_hace_asiento,
	hace_asiento_estandar,
	hace_asiento_costo,
	cuenta_renglon_genera_aplicacion,
	cuenta_patrimonial_genera_aplicacion,
	es_nota_de_credito,
	categorias_ignoradas_en_factura,
	inactiva,
	grupo,
	cuenta_redondeo,
	mostrar_saldo,
	grupo_cuentas_saldo,
	controla_limite_credito,
	permite_aplicar,
	detalle_obligatorio_en_partidas,
	copia_persona_de_cabecera,
	genera_reingreso,
	reingreso_empresa,
	reingreso_cuenta_iva,
	reingreso_cuenta_reingreso,
	reingreso_cuenta_patrimonial,
	created_at,
	muestra_depositos,
	opprod,
	especificar_unicidades,
	grupo_cuentas_aplicar_recibo,
	permite_imputacion_diferida,
	permite_detalle_productos,
	totaliza_desde_partidas_fact,
	permite_precio_cero,
	genera_reingreso_pago,
	reingreso_pago_cuentas_permitidas,
	reingreso_obligatorio,
	reingreso_pago_obligatorio
	)
VALUES (
	$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,
	$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,
	$21,$22,$23,$24,$25,$26,$27,$28,$29,$30,
	$31,$32,$33,$34,$35,$36,$37,$38,$39,$40,
	$41,$42,$43,$44,$45,$46,$47,$48,$49,$50,
	$51,$52,$53,$54,$55,$56,$57,$58,$59,$60,
	$61)
`

	_, err = conn.Exec(ctx, query,
		req.Comitente,
		req.Nombre,
		req.Empresa,
		req.TipoOp,
		req.Detalle,
		req.SugiereFechaActual,
		req.CondicionPago,
		req.PermiteProductos,
		req.ProductosValorizados,
		req.ProductosSe,
		req.ProductosSeAplicanACuentas,
		req.UsaListaDePrecios,
		req.TrabajaConPrecioFinal,
		req.PreciosDesde,
		req.PermiteModificarPrecios,
		req.PermiteProductosPorCuentaTerceros,
		req.PermiteConceptos,
		req.PermitePartidasDirectas,
		req.PermiteValores,
		req.BuscadorPersonasExtendido,
		req.EmiteRecibo,
		req.CuentasAnticipo,
		req.UnicidadesQueSeCrean,
		req.DiferenciaMaximaRedondeo,
		req.Imputaciones,
		req.SignoRenglonProducto,
		req.SignoRenglonValor,
		req.NoHaceAsiento,
		req.HaceAsientoEstandar,
		req.HaceAsientoCosto,
		req.CuentaRenglonGeneraAplicacion,
		req.CuentaPatrimonialGeneraAplicacion,
		req.EsNotaDeCredito,
		req.CategoriasIgnoradasEnFactura,
		req.Inactiva,
		req.Grupo,
		req.CuentaRedondeo,
		req.MostrarSaldo,
		req.GrupoCuentasSaldo,
		req.ControlaLimiteCredito,
		req.PermiteAplicar,
		req.DetalleObligatorioEnPartidas,
		req.CopiaPersonaDeCabecera,
		req.GeneraReingreso,
		req.ReingresoEmpresa,
		req.ReingresoCuentaIVA,
		req.ReingresoCuentaReingreso,
		req.ReingresoCuentaPatrimonial,
		time.Now(),
		req.MuestraDepositos,
		req.OpProd,
		req.EspecificarUnicidades,
		req.GrupoCuentasAplicarRecibo,
		req.PermiteImputacionDiferida,
		req.PermiteDetalleProductos,
		req.TotalizaDesdePartidasFact,
		req.PermitePrecioCero,
		req.GeneraReingresoPago,
		req.ReingresoPagoCuentasPermitidas,
		req.ReingresoObligatorio,
		req.ReingresoPagoObligatorio,
	)
	if err != nil {
		return deferror.DB(err, "inserting")
	}
	return
}

func update(ctx context.Context, conn *pgxpool.Pool, req Config) (err error) {

	query := `UPDATE fact_config SET
	nombre=$3, 
	empresa=$4, 
	tipo_op=$5,
	detalle=$6,
	sugiere_fecha_actual=$7,
	condicion_pago=$8,
	permite_productos=$9,
	productos_valorizados=$10,
	productos_se=$11,
	precios_desde=$12,
	permite_modificar_precios=$13,
	permite_productos_por_cuenta_terceros=$14,
	permite_conceptos=$15,
	permite_partidas_directas=$16,
	permite_valores=$17,
	buscador_personas_extendido=$18,
	emite_recibo=$19,
	cuentas_anticipo=$20,
	unicidades_que_se_crean=$21,
	diferencia_maxima_redondeo=$22,
	imputaciones=$23,
	signo_renglon_producto=$24,
	signo_renglon_valor=$25,
	no_hace_asiento=$26,
	hace_asiento_costo=$27,
	cuenta_renglon_genera_aplicacion=$28,
	cuenta_patrimonial_genera_aplicacion=$29,
	es_nota_de_credito=$30,
	categorias_ignoradas_en_factura=$31,
	inactiva=$32,
	grupo=$33,
	cuenta_redondeo=$34,
	mostrar_saldo=$35,
	grupo_cuentas_saldo=$36,
	permite_aplicar=$37,
	detalle_obligatorio_en_partidas=$38,
	copia_persona_de_cabecera=$39,
	genera_reingreso=$40,
	reingreso_empresa=$41,
	reingreso_cuenta_iva=$42,
	reingreso_cuenta_reingreso=$43,
	reingreso_cuenta_patrimonial=$44,
	updated_at=$45,
	usa_lista_de_precios=$46,
	trabaja_con_precio_final=$47,
	productos_se_aplican_a_cuentas=$48,
	hace_asiento_estandar=$49,
	muestra_depositos=$50,
	opprod=$51,
	especificar_unicidades=$52,
	grupo_cuentas_aplicar_recibo=$53,
	permite_imputacion_diferida=$54,
	permite_detalle_productos=$55,
	totaliza_desde_partidas_fact=$56,
	permite_precio_cero=$57,
	genera_reingreso_pago=$58,
	reingreso_pago_cuentas_permitidas=$59,
	reingreso_obligatorio=$60,
	reingreso_pago_obligatorio=$61,
	controla_limite_credito=$62

WHERE comitente=$1 AND id=$2
`

	_, err = conn.Exec(ctx, query, req.Comitente, req.ID,
		req.Nombre,
		req.Empresa,
		req.TipoOp,
		req.Detalle,
		req.SugiereFechaActual,
		req.CondicionPago,
		req.PermiteProductos,
		req.ProductosValorizados,
		req.ProductosSe,
		req.PreciosDesde,
		req.PermiteModificarPrecios,
		req.PermiteProductosPorCuentaTerceros,
		req.PermiteConceptos,
		req.PermitePartidasDirectas,
		req.PermiteValores,
		req.BuscadorPersonasExtendido,
		req.EmiteRecibo,
		req.CuentasAnticipo,
		req.UnicidadesQueSeCrean,
		req.DiferenciaMaximaRedondeo,
		req.Imputaciones,
		req.SignoRenglonProducto,
		req.SignoRenglonValor,
		req.NoHaceAsiento,
		req.HaceAsientoCosto,
		req.CuentaRenglonGeneraAplicacion,
		req.CuentaPatrimonialGeneraAplicacion,
		req.EsNotaDeCredito,
		req.CategoriasIgnoradasEnFactura,
		req.Inactiva,
		req.Grupo,
		req.CuentaRedondeo,
		req.MostrarSaldo,
		req.GrupoCuentasSaldo,
		req.PermiteAplicar,
		req.DetalleObligatorioEnPartidas,
		req.CopiaPersonaDeCabecera,
		req.GeneraReingreso,
		req.ReingresoEmpresa,
		req.ReingresoCuentaIVA,
		req.ReingresoCuentaReingreso,
		req.ReingresoCuentaPatrimonial,
		time.Now(),
		req.UsaListaDePrecios,
		req.TrabajaConPrecioFinal,
		req.ProductosSeAplicanACuentas,
		req.HaceAsientoEstandar,
		req.MuestraDepositos,
		req.OpProd,
		req.EspecificarUnicidades,
		req.GrupoCuentasAplicarRecibo,
		req.PermiteImputacionDiferida,
		req.PermiteDetalleProductos,
		req.TotalizaDesdePartidasFact,
		req.PermitePrecioCero,
		req.GeneraReingresoPago,
		req.ReingresoPagoCuentasPermitidas,
		req.ReingresoObligatorio,
		req.ReingresoPagoObligatorio,
		req.ControlaLimiteCredito,
	)
	if err != nil {
		return deferror.DB(err, "updating")
	}
	return
}

func delete(ctx context.Context, conn *pgxpool.Pool, req ReadOneReq, perm Deleter) (err error) {
	tx, err := conn.Begin(ctx)
	if err != nil {
		return errors.Wrap(err, "iniciando tx")
	}
	defer tx.Rollback(ctx)

	{ // Está usado en FACT?
		count := 0
		err = tx.QueryRow(ctx, "SELECT COUNT(id) FROM fact WHERE comitente=$1 AND config=$2",
			req.Comitente, req.ID).Scan(&count)
		if err != nil {
			return deferror.DB(err, "determinando si la config fue usada")
		}
		if count > 0 {
			return deferror.Validationf("no se puede borrar porque esta configuración ya fue usada %v veces", count)
		}
	}
	{ // Está usado en ESQUEMAS?
		count := 0
		err = tx.QueryRow(ctx, "SELECT COUNT(id) FROM fact_esquemas WHERE comitente=$1 AND config=$2",
			req.Comitente, req.ID).Scan(&count)
		if err != nil {
			return deferror.DB(err, "determinando si la config tiene algún esquema")
		}
		if count > 0 {
			return deferror.Validationf("no se puede borrar porque esta configuración tiene %v esquemas asociados. Borre primero los esquemas", count)
		}
	}

	// Borro fact_permisos
	err = perm.DeletePermisosDeConfig(ctx, tx, req.Comitente, req.ID)
	if err != nil {
		return errors.Wrap(err, "borrando permisos de config")
	}

	// Borro
	res, err := tx.Exec(ctx,
		"DELETE FROM fact_config WHERE comitente=$1 AND id=$2", req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "deleting")
	}
	if res.RowsAffected() == 0 {
		return errors.New("no se borró ningún registro")
	}

	// Confirmo TX
	err = tx.Commit(ctx)
	if err != nil {
		return errors.Wrap(err, "confirmando transacción")
	}

	return
}
