package config

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/fact/internal/opprod"
	"bitbucket.org/marcos19/one/pkg/productos"
	"bitbucket.org/marcos19/one/pkg/productos/categorias"
	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
)

// Esta struct en base al producto y a la config, determina
// a que cuenta corresponde el renglón del asiento.
type CuentasGetter struct {
	cfg       Config
	comitente int
	h         *Handler
}

func NewCuentasGetter(ctx context.Context, comitente int, cfg int, h *Handler) (out *CuentasGetter, err error) {
	out = &CuentasGetter{}
	out.cfg, err = h.ReadOne(ctx, ReadOneReq{Comitente: comitente, ID: cfg})
	if err != nil {
		return out, errors.Wrap(err, "buscando config")
	}
	out.comitente = comitente
	out.h = h
	return
}

// Wrapper
func (g *CuentasGetter) Cuenta(ctx context.Context, comitente, id int) (cta cuentas.Cuenta, err error) {
	return g.h.cuentasHandler.ReadOne(ctx, cuentas.ReadOneReq{Comitente: comitente, ID: id})
}

// Busca la planilla de imputación para este producto y config.
// Sube en la jerarquía de categorías hasta encontrar una que tenga imputación
func DeterminarImputacion(
	ctx context.Context,
	comitente int,
	producto uuid.UUID,
	imputaciones []ImputacionContable,
	productoGetter productos.Getter,
	categoriaGetter categorias.SuperiorGetter,
	opprodCuenta int,
) (out int, err error) {

	if producto == uuid.Nil {
		return out, errors.Errorf("no se ingresó producto ID")
	}

	// Busco producto
	prod, err := productoGetter.ReadOne(ctx, productos.ReadOneReq{
		Comitente: comitente,
		ID:        producto,
	})
	if err != nil {
		return out, errors.Wrapf(err, "buscando producto '%v'", producto)
	}
	if prod.Categoria == 0 {
		return out, errors.Errorf("el producto %v (%v) no tiene definida categoria", prod.Nombre, prod.CodigoAlfanumerico)
	}
	// Tiene la config imputaciones para esta categoría?
	for _, v := range imputaciones {
		if v.Categoria == prod.Categoria {
			out = mapping(v, opprodCuenta)
			if out != 0 {
				return out, nil
			}
		}
	}

	// No hay imputación para esta categoría.
	// Alguna categoría superior tiene imputación?
	id := prod.Categoria
	nombreCategoria := ""
	count := 0
	for {
		count++
		if count > 20 {
			return out, errors.Errorf("se alcanzó máximo de iteraciones")
		}
		sup, err := categoriaGetter.CategoriaSuperior(ctx, categorias.ReadOneReq{
			ID:        id,
			Comitente: comitente,
		})
		if err != nil {
			return out, errors.Wrapf(err, "buscando categoría superior de %v", id)
		}
		if sup == nil {
			// Llegué a root pero no tengo imputación todavía
			break
		} else {
			id = *sup.ID
		}
		if id == prod.Categoria {
			nombreCategoria = sup.Nombre
		}

		// Tiene la config imputaciones para esta categoría?
		for _, v := range imputaciones {
			if v.Categoria == *sup.ID {
				out = mapping(v, opprodCuenta)
				if out != 0 {
					return out, nil
				}
			}
		}
	}

	// Hay imputaciones sin categoría para esta config?
	for _, v := range imputaciones {
		if v.Categoria == 0 {
			out = mapping(v, opprodCuenta)
			if out != 0 {
				return out, nil
			}
		}
	}

	return out, errors.Errorf("la config no define ninguna cuenta contable para imputar a concepto '%v' (ni para la categoría '%v', ni ninguna de sus superiores)", opprod.NombreCuenta(opprodCuenta), nombreCategoria)
}

// Para cada cuenta de opprod devuelve la cuenta establecida en la config.
func mapping(imp ImputacionContable, id int) int {
	switch id {

	case opprod.ProductosSolicitadosARecibir:
		return imp.ProductosSolicitadosARecibir

	case opprod.ProductosSolicitados:
		return imp.ProductosSolicitados

	case opprod.ProductosSolicitadosCancelados:
		return imp.ProductosSolicitadosCancelados

	case opprod.ProductosSolicitadosRecibidos:
		return imp.ProductosSolicitadosRecibidos

	case opprod.ProductosSolicitadosFacturados:
		return imp.ProductosSolicitadosFacturados

	case opprod.FacturasARecibir:
		return imp.FacturasARecibir

	case opprod.ProductosCompradosARecibir:
		return imp.ProductosCompradosARecibir

	case opprod.NotasDeCreditoARecibir:
		return imp.NotasDeCreditoARecibir

	// Venta
	case opprod.PedidosDeClientesAEntregar:
		return imp.PedidosDeClientesAEntregar

	case opprod.PedidosDeClientes:
		return imp.PedidosDeClientes

	case opprod.FacturasAEmitir:
		return imp.FacturasAEmitir

	case opprod.ProductosFacturadosAEntregar:
		return imp.ProductosFacturadosAEntregar

	case opprod.PedidosDeClientesEntregados:
		return imp.PedidosDeClientesEntregados

	case opprod.PedidosDeClientesCancelados:
		return imp.PedidosDeClientesCancelados

	case opprod.PedidosDeClientesFacturados:
		return imp.PedidosDeClientesFacturados

	// Generales
	case opprod.Productos:
		return imp.Productos

	case opprod.Proveedores:
		return imp.Proveedores

	case opprod.IVA:
		return imp.IVA

	case opprod.Clientes:
		return imp.Clientes

	case opprod.Ventas:
		return imp.Ventas
	}

	return 0
}
