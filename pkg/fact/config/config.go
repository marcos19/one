package config

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"reflect"

	"bitbucket.org/marcos19/one/pkg/fact/permisos"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
)

// Config contiene toda la configuración de una transaccion
// del prgrama fact.
type Config struct {
	ID        int `json:",string"`
	Comitente int `json:",string"`
	Empresa   int `json:",string"`

	Nombre string
	TipoOp TipoOp `json:",string"`
	OpProd *int   `json:",string"`
	Grupo  string

	Detalle string

	// Generales
	//
	// Si es false, le obligo al usuario a ingresarla manualmente.
	SugiereFechaActual bool

	// Obligatoria, opcional o sin condición
	// Opcional: sirve si se trata de un remito por ejemplo, y el operador
	// 		     no sabe como deberá ser facturado.
	// Sin condición: no se para cuando se podría utilizar
	CondicionPago string

	////////////////////////////////////////////////////////////////////////
	// FACTURA DE PRODUCTOS
	////////////////////////////////////////////////////////////////////////

	// El front-end muestra toda la parte de productos
	PermiteProductos bool

	// Si es true, al usuario le aparece una caja de texto donde puede ingresar
	// un detalle adicional al del nombre del producto.
	PermiteDetalleProductos bool

	// Si es false, se contabilizarán unicamente unidades, no montos.
	ProductosValorizados bool

	// Si los productos se pueden elegir libremente del catálogo,
	// se tienen que aplicar o cualquiera de las dos cosas.
	ProductosSe OperacionConProductos

	// Si el producto se tiene (o puede) aplicar, buscará partidas disponibles
	// en estas cuentas contables.
	ProductosSeAplicanACuentas tipos.GrupoInts

	// Si estoy facturando algo remitido con anterioridad,
	// si quiero que tome los precios originales o los actuales.
	PreciosDesde PreciosDesde

	// Para compras y ordenes de compra probablemente no. El usuario ingresa a mano.
	UsaListaDePrecios bool

	// Cuando recibo o costeo bienes.
	MuestraDepositos bool

	// Determina como se muestran y seleccionan los precios
	TrabajaConPrecioFinal bool

	// El usuario podrá modificar los precios manualmente.
	PermiteModificarPrecios bool

	// Para ordenes de compra sin precio
	PermitePrecioCero bool

	PermiteProductosPorCuentaTerceros bool

	// Son los que se calculan sobre los productos (seguro, flete, etc)
	PermiteConceptos bool

	// Para aquellos comprobantes como ordenes de compra o facturas proforma
	// quiero que el comprobante diga neto e IVA independientemente del asiento.
	// Si este campo es true, lo va a calcular desde fact.PartidasProducto y
	// fact.PartidasDirectas
	TotalizaDesdePartidasFact bool

	////////////////////////////////////////////////////////////////////////
	// FACTURA DE CUENTAS CONTABLES
	////////////////////////////////////////////////////////////////////////
	// Esta sería para la factura de compra
	// Cuando alguien me factura productos y otros conceptos, estos otros
	// conceptos que para mi no son productos, ni son un costo necesario de
	// este, puedo directamente incluirlo como cuenta contable.
	PermitePartidasDirectas      bool
	DetalleObligatorioEnPartidas bool

	// Cuando por ejemplo se quiere elegir la cuenta
	// "Facturas a Emitir" para que el usuario no elija otro,
	// directamente lo selecciona.
	CopiaPersonaDeCabecera bool

	////////////////////////////////////////////////////////////////////////
	// RECIBO
	////////////////////////////////////////////////////////////////////////

	// Si es true el front-end muesta los valores para agregar.
	// Si el pago cancela la factura, sale como de contado detallando
	// el modo de pago en la factura. Sino emite un recibo a parte.
	// Quien determina si la operación deberá ser de contado es la
	// condición de venta.
	PermiteValores bool

	// En el frontend cuando selecciono un valor que tiene apertura por persona
	// (generalmente un banco), suelen ser pocos las opciones así que muestro
	// un select para elegirlo. Si esta variable es true, abre un buscador.
	BuscadorPersonasExtendido bool

	// Si es true, en la parte del recibo, buscará las partidas que hay pendientes
	// para aplicar
	PermiteAplicar bool

	// Si la operación es de contado, pero lo hace por ejemplo con un cheque
	// superior a la factura,
	EmiteRecibo bool

	// Son las cuentas a la que se imputa el pago cuando no hay aplicación
	// de comprobantes.
	CuentasAnticipo tipos.GrupoInts

	// Los IDs que estén en esta lista, cuando sean seleccionados se
	// abrirá un cuadro para cargar los datos. Sino abrirá un listado
	// para seleccionarlas.
	UnicidadesQueSeCrean tipos.GrupoInts

	// Generalmente cuando se selecciona un producto que abre por unicidades,
	// se espera que el usuario especifique la unicidad.
	// Hay casos en los que me interesa elegir el producto, pero sin especificar
	// unicidad, por ejemplo en una orden de compra. Si este campo es false
	// no va a pedir la especificación.
	EspecificarUnicidades bool

	DiferenciaMaximaRedondeo dec.D2
	CuentaRedondeo           int `json:",string"`

	// Sólo traerá para aplicar las cuentas que pertenezcan a este grupo.
	GrupoCuentasAplicarRecibo *int `json:",string"`

	// Monto y cantidades de las partidas generadas por renglones
	// van por este signo
	SignoRenglonProducto int
	SignoRenglonValor    int

	// Usada para presupuestos o facturas proforma
	NoHaceAsiento bool

	// Asiento de costo
	HaceAsientoEstandar bool
	HaceAsientoCosto    bool

	CuentaRenglonGeneraAplicacion     bool
	CuentaPatrimonialGeneraAplicacion bool

	EsNotaDeCredito bool

	Imputaciones []ImputacionContable

	// Para operación de reingreso
	// Genera una op en una segunda empresa con el asiento invertido.
	GeneraReingreso            bool
	ReingresoObligatorio       bool
	ReingresoEmpresa           *int `json:",string"`
	ReingresoCuentaIVA         *int `json:",string"`
	ReingresoCuentaReingreso   *int `json:",string"`
	ReingresoCuentaPatrimonial *int `json:",string"`

	GeneraReingresoPago            bool
	ReingresoPagoObligatorio       bool
	ReingresoPagoCuentasPermitidas tipos.GrupoInts

	// Si es true muestra el campo para elegir la fecha.
	PermiteImputacionDiferida bool

	// Para determinar el saldo de esta persona consultar este grupo
	MostrarSaldo          bool
	GrupoCuentasSaldo     *int `json:",string"`
	ControlaLimiteCredito bool

	// Cuando se facture un producto de esta categoría, el mismo
	// será ignorado en la impresión de la factura, pero se contabilizará.
	// Usado para las obleas facturas por la cámara
	CategoriasIgnoradasEnFactura tipos.GrupoInts

	Inactiva bool

	Permisos []permisos.Permiso

	GruposAutorizados   tipos.GrupoInts
	PersonasAutorizadas tipos.GrupoInts
}

func (Config) TableName() string {
	return "fact_config"
}

// Value cumple con la interface SQL
func (t Config) Value() (driver.Value, error) {
	return json.Marshal(t)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (t *Config) Scan(src interface{}) error {
	if src == nil {
		*t = Config{}
		return nil
	}

	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New(fmt.Sprint(
			"Type assertion error .([]byte). Era: ",
			reflect.TypeOf(src),
		))
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, t)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("Unmarshalling los siguientes bytes: '%s'", source))
	}

	return nil
}

type OperacionConProductos string

const (
	OperacionProductoSeAplican     = OperacionConProductos("Se aplican")
	OperacionProductoSeSeleccionan = OperacionConProductos("Se seleccionan")
	OperacionProductoCualquiera    = OperacionConProductos("Ambas")
)

type PreciosDesde string

const (
	PreciosDesdeApliacion = PreciosDesde("Aplicación")
	PreciosDesdeLista     = PreciosDesde("Lista de precios actual")
)

// Opción que puede tomar el campo CondicionDePago de Fact
const (
	CondicionPagoObligatoria  = "Obligatoria"
	CondicionPagoOpcional     = "Opcional"
	CondicionPagoSinCondicion = "Sin condición"
)

// type AplicacionesModalidad string

// const (
// 	// El pago irá a la cuenta de anticipos
// 	aplicacionesNoAplica = AplicacionesModalidad("No aplica")

// 	// El pago debe ser aplicado completamente
// 	aplicacionesCompleta = AplicacionesModalidad("Aplicación completa")

// 	// El pago debe ser aplicado completamente
// 	aplicacionesOpcional = AplicacionesModalidad("Aplicación opcional")
// )
