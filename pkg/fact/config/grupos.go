package config

// GrupoConfig es la separación que uso para determinar en que lugar
// del menú va cada transacción.
type GrupoConfig struct {
	ID             string
	NombreSingular string
	Genero         string
}

// Grupos (son para agrupar transacciones por)
var (
	facturasDeVenta = GrupoConfig{
		ID:             "Facturas de venta",
		NombreSingular: "Factura de venta",
		Genero:         "Femenino",
	}

	notasDeCreditoDeVenta = GrupoConfig{
		ID:             "Notas de crédito de venta",
		NombreSingular: "Nota de crédito de venta",
		Genero:         "Femenino",
	}
	notasDePedido = GrupoConfig{
		ID:             "Notas de pedido",
		NombreSingular: "Nota de pedido",
		Genero:         "Femenino",
	}
	facturasDeCompra = GrupoConfig{
		ID:             "Facturas de compra",
		NombreSingular: "Factura de compra",
		Genero:         "Femenino",
	}
	notasDeCreditoDeCompra = GrupoConfig{
		ID:             "Notas de crédito de compra",
		NombreSingular: "Nota de crédito de compra",
		Genero:         "Femenino",
	}
	presupuesto = GrupoConfig{
		ID:             "Presupuestos",
		NombreSingular: "Presupuesto",
		Genero:         "Masculino",
	}
	remitosDeVenta = GrupoConfig{
		ID:             "Remitos de venta",
		NombreSingular: "Remito de venta",
		Genero:         "Masculino",
	}
	remitosDeCompra = GrupoConfig{
		ID:             "Remitos de compra",
		NombreSingular: "Remito de compra",
		Genero:         "Masculino",
	}
	ordenesDeCompra = GrupoConfig{
		ID:             "Órdenes de compra",
		NombreSingular: "Órden de compra",
		Genero:         "Femenino",
	}
	recibos = GrupoConfig{
		ID:             "Recibos",
		NombreSingular: "Recibo",
		Genero:         "Masculino",
	}

	ordenesDePago = GrupoConfig{
		ID:             "Ordenes de pago",
		NombreSingular: "Orden de pago",
		Genero:         "Femenino",
	}
	depositosBancarios = GrupoConfig{
		ID:             "Depósitos bancarios",
		NombreSingular: "Depósito bancario",
		Genero:         "Masculino",
	}
	rechazoDeCheques = GrupoConfig{
		ID:             "Rechazos de cheques de terceros",
		NombreSingular: "Rechazo de cheque",
		Genero:         "Masculino",
	}
	debitoDeCheques = GrupoConfig{
		ID:             "Débito cheques",
		NombreSingular: "Débito cheque",
		Genero:         "Masculino",
	}
)

// // En base al ID del grupo, devuelve el listado de TipoOp que corresponden
// // al grupo.
// func mapGrupo(nombreGrupo string) (tipos []TipoOp, err error) {

// 	switch nombreGrupo {
// 	case "Facturas de venta":
// 		tipos = append(tipos,
// 			FacturaVentaAnticipada,
// 			FacturaVentaDirecta,
// 			FacturaVentaSobreRemito,
// 			FacturaVentaFinanciera,
// 		)
// 	case "Facturas de compra":
// 		tipos = append(tipos,
// 			FacturaCompraAnticipada,
// 			FacturaCompraDirecta,
// 			FacturaCompraSobreRemito,
// 			FacturaCompraFinanciera,
// 		)
// 	case "Remitos de venta":
// 		tipos = append(tipos,
// 			RemitoVenta,
// 			RemitoVentaAnticipada,
// 		)
// 	case "Remitos de compra":
// 		tipos = append(tipos,
// 			RemitoCompra,
// 			RemitoCompraAnticipada,
// 		)
// 	case "Recibos":
// 		tipos = append(tipos,
// 			Recibo,
// 		)
// 	case "Recibos devolución":
// 		tipos = append(tipos,
// 			ReciboDevolucion,
// 		)
// 	case "Ordenes de pago":
// 		tipos = append(tipos,
// 			OrdenPago,
// 		)
// 	case "Ordenes de pago devolución":
// 		tipos = append(tipos,
// 			OrdenPagoDevolucion,
// 		)
// 	case "Depositos bancarios":
// 		tipos = append(tipos,
// 			DepositoBancario,
// 		)
// 	default:
// 		return tipos, errors.Wrapf(err, "no se encontró el grupo %v", nombreGrupo)
// 	}
// 	return
// }

// GruposConfig son los items que habrá en el menú, todos usan el
// módulo fact.
var GruposConfig = []GrupoConfig{
	facturasDeVenta,
	notasDeCreditoDeVenta,
	notasDePedido,
	ordenesDeCompra,
	facturasDeCompra,
	notasDeCreditoDeCompra,
	presupuesto,
	remitosDeVenta,
	remitosDeCompra,
	recibos,
	ordenesDePago,
	depositosBancarios,
	rechazoDeCheques,
	debitoDeCheques,
}
