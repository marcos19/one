package config

import (
	"bitbucket.org/marcos19/one/pkg/tipos"
)

type ImputacionContable struct {
	// CuentaRenglon              int `json:",string"`
	// CuentaPatrimonial          int `json:",string"`
	// CuentaCosto                int `json:",string"`
	// CuentaBsCambio             int `json:",string"`
	// CuentaPendienteRenglon     int `json:",string"`
	// CuentaPendientePatrimonial int `json:",string"`

	Categoria int `json:",string"`

	ProductosSolicitadosARecibir   int `json:",string"`
	ProductosSolicitados           int `json:",string"`
	ProductosSolicitadosCancelados int `json:",string"`
	ProductosSolicitadosRecibidos  int `json:",string"`
	ProductosSolicitadosFacturados int `json:",string"`
	FacturasARecibir               int `json:",string"`
	ProductosCompradosARecibir     int `json:",string"`
	NotasDeCreditoARecibir         int `json:",string"`

	PedidosDeClientesAEntregar   int `json:",string"`
	PedidosDeClientes            int `json:",string"`
	FacturasAEmitir              int `json:",string"`
	ProductosFacturadosAEntregar int `json:",string"`
	PedidosDeClientesEntregados  int `json:",string"`
	PedidosDeClientesCancelados  int `json:",string"`
	PedidosDeClientesFacturados  int `json:",string"`

	Productos    int `json:",string"`
	Proveedores  int `json:",string"`
	IVA          int `json:",string"`
	Diferimiento int `json:",string"`
	Clientes     int `json:",string"`
	Ventas       int `json:",string"`

	CuentaSaldoUnicidad   int `json:",string"`
	CuentasSaldosUnicidad tipos.GrupoInts
}

const (
	// SignoFactura es el signo que tienen los renglones
	// en una factura de venta.
	SignoFacturaVenta = -1

	// SignoFacturaCompra es el signo que tienen los renglones
	// en una factura de venta.
	SignoFacturaCompra = 1

	// SignoNC es el signo que tienen los renglones
	// de productos en una nota de crédito de venta.
	SignoNC = 1
	// SignoCobro es el signo que tienen los renglones de valores
	// en un recibo de cobro
	SignoCobro = 1
	// SignoPago es el signo que tienen los renglones de valores
	// en una orden de pago
	SignoPago = -1
)

// TipoOp son FacturaDirecta, Remito, FacturaAnticipada, etc.
type TipoOp int

// Los tipos de comprobantes ya los predefino sistemáticamente
const (
	FacturaVentaDirecta     = TipoOp(1)
	NCVentaDirecta          = TipoOp(1001)
	FacturaVentaAnticipada  = TipoOp(2)
	FacturaVentaSobreRemito = TipoOp(3)
	FacturaVentaFinanciera  = TipoOp(4)
	NCVentaFinanciera       = TipoOp(1001)

	NCCompraFinanciera     = TipoOp(110)
	RemitoVentaAnticipada  = TipoOp(402)
	RemitoVenta            = TipoOp(403)
	RemitoCompraAnticipada = TipoOp(502)
	Recibo                 = TipoOp(601)
	ReciboDevolucion       = TipoOp(602)
	OrdenPago              = TipoOp(701)
	OrdenPagoDevolucion    = TipoOp(702)
	DepositoBancario       = TipoOp(710)

	AjusteStockPositivo = TipoOp(801)
	AjusteStockNegativo = TipoOp(802)
)
