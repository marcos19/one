package types

// // TotalConcepto representa cada uno de los renglones de abajo de un comprobante,
// // donde se suman los conceptos impositivos (Neto, IVA, Percepcion, Etc)
// type TotalConcepto struct {
// 	Tipo     iva.Concepto // Base imponible, IVA, Percepción, OtrosImpuestos
// 	Alicuota iva.Alicuota
// 	Nombre   string // Nombre concatenado que se imprime
// 	Monto    dec.D2
// }

// type TotalesConceptos []TotalConcepto

// func (p TotalesConceptos) Total() (sum dec.D2) {
// 	for _, v := range p {
// 		sum += v.Monto
// 	}
// 	return
// }

// // Value cumple con la interface SQL
// func (p TotalesConceptos) Value() (driver.Value, error) {
// 	return json.Marshal(p)
// }

// // Scan implementa la interface SQL. Es para pegar los datos de la base
// // de datos en una struct.
// func (p *TotalesConceptos) Scan(src interface{}) error {
// 	if src == nil {
// 		*p = TotalesConceptos{}
// 		return nil
// 	}

// 	// Chequeo que el campo sea []byte
// 	source, ok := src.([]byte)
// 	if !ok {
// 		return errors.New(fmt.Sprint(
// 			"Type assertion error .([]byte). Era: ",
// 			reflect.TypeOf(src),
// 		))
// 	}

// 	// Chequeo que se condiga con la struct
// 	err := json.Unmarshal(source, p)
// 	if err != nil {
// 		return errors.Wrap(err, fmt.Sprintf("Unmarshalling los siguientes bytes: '%s'", source))
// 	}

// 	return nil
// }
