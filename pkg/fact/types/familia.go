package types

import (
	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/deferror"
)

// ParametrosContables tiene como finalidad el número de cuenta contable
// que este comitente tiene determinado para realizar la funcióñ solicitada,
// Por ejemplo que se quiere saber cuál es la cuenta de ventas, se ingresa
// "Ventas" y devolverá el ID de la cuenta.
type ParametrosContables interface {
	Cuenta(FuncionContable) (int, error)
}

// FuncionContable representa una función sistémica que tiene una cuenta
// contable, por ejemplo ventas o Mercadería en stock.
type FuncionContable string

// Es la función que tiene la cuenta contable dentro de la construcción del asiento de una operación.
const (
	// La cuenta de stock propio
	MercaderiaPropiaEnDepositosPropios     = FuncionContable(cuentas.MercaderiaPropiaEnDepositosPropios)
	MercaderiaPropiaEnDepositosDeTerceros  = FuncionContable(cuentas.MercaderiaPropiaEnDepositosDeTerceros)
	MercaderiaDeTercerosEnDepositosPropios = FuncionContable(cuentas.MercaderiaDeTercerosEnDepositosPropios)
	Proveedores                            = FuncionContable(cuentas.Proveedores)
	FacturasARecibir                       = FuncionContable(cuentas.FacturasARecibir)
	MercaderiaARecibir                     = FuncionContable(cuentas.MercaderiaARecibir)

	DeudoresPorVentas   = FuncionContable(cuentas.DeudoresPorVentas)
	MercaderiaAFacturar = FuncionContable(cuentas.MercaderiaAFacturar)
	MercaderiaAEntregar = FuncionContable(cuentas.MercaderiaAEntregar)

	Ventas         = FuncionContable(cuentas.Ventas)
	VentasACostear = FuncionContable(cuentas.VentasACostear)

	Costo = FuncionContable(cuentas.Costo)

	IVACreditoFiscal = FuncionContable(cuentas.IVACreditoFiscal)
	IVADebitoFiscal  = FuncionContable(cuentas.IVADebitoFiscal)

	IVAEnFacturaDeVenta      = "IVA en factura de venta"
	IVAEnNotaCreditoDeVenta  = "IVA en nota de crédito de venta"
	IVAEnFacturaDeCompra     = "IVA en factura de compra"
	IVAEnNotaCreditoDeCompra = "IVA en nota de crédito de compra"

	MedioDePago = "Medio de pago"
)

type ParametrosPorDefecto struct {
	cc map[FuncionContable]int
}

// UsaAplicaciones determina si las cuentas intermedias
// como mercadería a facturar, ventas a costear, etc
// usan aplicaciones. Las crean o aplican a otras ya existentes.
func (c FuncionContable) UsaAplicaciones() bool {
	switch c {
	case MercaderiaAFacturar, VentasACostear, MercaderiaAEntregar:
		return true
	}
	return false
}

func (p *ParametrosPorDefecto) Cuenta(f FuncionContable) (cta int, err error) {

	switch f {
	case IVAEnFacturaDeCompra, IVAEnNotaCreditoDeCompra:
		f = IVACreditoFiscal
	case IVAEnFacturaDeVenta, IVAEnNotaCreditoDeVenta:
		f = IVADebitoFiscal
	}

	cta, estaba := p.cc[f]
	if !estaba {
		return cta, deferror.Validationf("buscando cuenta contable para función %v", f)
	}
	return
}
