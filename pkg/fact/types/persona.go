package types

import (
	"database/sql/driver"
	"encoding/json"

	"bitbucket.org/marcos19/one/pkg/afip/afipmodels"
	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
)

// Persona es la struct que va en fact.
// Guardo todos los datos de la persona seleccionada.
type Persona struct {
	ID                       uuid.UUID
	Sucursal                 *int `json:",string,omitempty"`
	Nombre                   string
	NombreSucursal           string                  `json:",omitempty"`
	Domicilio                string                  `json:",omitempty"`
	TipoIdentificacionFiscal int                     `json:",string,omitempty"`
	NIdentificacionFiscal    int                     `json:",string,omitempty"`
	CondicionFiscal          afipmodels.CondicionIVA `json:",string,omitempty"`
}

// Value cumple con la interface SQL
func (p Persona) Value() (driver.Value, error) {
	return json.Marshal(p)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (p *Persona) Scan(src interface{}) error {
	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New("Type assertion error .([]byte)")
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, p)
	if err != nil {
		return errors.Wrap(err, "Unmarshalling")
	}

	return nil
}
