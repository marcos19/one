package types

import (
	"bitbucket.org/marcos19/one/pkg/iva"
	"bitbucket.org/marcos19/one/pkg/unicidades"
	"github.com/crosslogic/dec"
	uuid "github.com/gofrs/uuid"
)

type PartidaProducto struct {
	Producto          uuid.UUID // OK
	SKU               uuid.UUID
	Codigo            string `json:",omitempty"`
	ProductoNombre    string `json:",omitempty"`
	ProductoImpresion string `json:",omitempty"` // El campo que se imprime
	ProductoDetalle   string `json:",omitempty"`
	UM                string `json:",omitempty"`
	UMNombre          string `json:",omitempty"`
	Cantidad          dec.D4 `json:",omitempty"`

	PrecioUnitario      dec.D4 `json:",omitempty"` // $100 // OK
	PorcentajeRecargo   dec.D4 `json:",omitempty"` // 0.1
	RecargoUnitario     dec.D4 `json:",omitempty"` //  100000 => $  10
	PrecioUnitarioFinal dec.D4 `json:",omitempty"` // 1100000 => $ 110 // OK

	// No incluye IVA ni otros impuestos
	MontoTotal dec.D4 `json:",omitempty"` // 8800000 => $ 800

	AlicuotaIVA iva.Alicuota `json:",string,omitempty"` // OK

	// Unicidades seleccionadas (ya existían de antes)
	Unicidades []UnicidadDetalle `json:",omitempty"`

	// Unicidades que se dan de alta con esta partida
	// (ej: recepción de obleas)
	UnicidadesNuevas []unicidades.Unicidad

	// En una misma factura podría tener productos que se vendan en
	// U$S y en pesos
	//	TipoMoneda string

	// Podría elegir varias listas de precios en una misma factura
	ListaPrecios int `json:",string,omitempty"` // OK

	// Hace referencia a una promoción que define como deben calcularse los descuentos
	//	Promocion int

	// Aplicaciones para partidas de productos
	//	Aplicaciones Aplicaciones

	// Luego de procesar la factura, si la categoría del producto está en
	// IgnorarCategoriasEnFactura, le pongo esta señal
	CuentaDeTerceros bool `json:",omitempty"`

	Centro int `json:",omitempty"`
}

type UnicidadDetalle struct {
	ID      uuid.UUID
	Detalle string
}
