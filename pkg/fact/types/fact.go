package types

import (
	"sort"
	"time"

	"bitbucket.org/marcos19/one/pkg/iva"
	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
)

const PkgID = "fact"

// Fact es la estructura de datos que uso en el front end.
// Se graba prácticamente igual en la base de datos.
// Las ops referencian a este registro.
type Fact struct {
	ID        uuid.UUID
	Empresa   int `json:",string"`
	Comitente int `json:"-"`

	Config  int `json:",string"`
	Esquema int `json:",string"`
	Fecha   fecha.Fecha

	Persona          Persona
	CondicionPago    *int              `json:",string"`
	Detalle          string            `json:",omitempty"`
	Productos        []PartidaProducto `json:",omitempty"`
	PartidasDirectas []PartidaDirecta  `json:",omitempty"`
	Valores          []PartidaValores  `json:",omitempty"`

	// Hay valores, pero no coinciden con la factura
	TieneRecibo *bool

	// Hay un pago que cancela el total de la factura.
	// Se imprime en el mismo comprobante
	EsContado *bool

	// Las aplicaciones a operaciones anteriores
	Aplicaciones []Aplicacion `json:",omitempty"`

	// Las aplicaciones que se generan y se aplican dentro de esta
	// misma fact
	AplicacionesEstaFactura []Aplicacion `json:",omitempty"`

	AplicacionesProductos []AplicacionProducto
	Deposito              *int `json:",string,omitempty"`

	// La parte del recibo que no va aplicada a ningún comprobante
	// va aimputada a esta cuenta
	AnticipoCuenta *int    `json:",string,omitempty"`
	AnticipoCentro *int    `json:",string,omitempty"`
	AnticipoMonto  *dec.D2 `json:",omitempty"`

	ReingresoMonto *dec.D2 `json:",omitempty"`

	// Si se ingresa una cuenta de pago, los saldos de deuda generado en la Persona
	// serán trasladados
	ReingresoPagoCuenta   *int       `json:",string,omitempty"`
	ReingresoPagoPersona  *uuid.UUID `json:",string,omitempty"`
	ReingresoPagoSucursal *int       `json:",string,omitempty"`
	ReingresoPagoCaja     *int       `json:",string,omitempty"`

	// Calculados
	// TotalesConceptos TotalesConceptos
	Vencimientos Vencimientos

	// Si es true significa que el usuario ingresó los vencimientos
	// manualmente y estos no deben ser recalculados.
	VencimientosManuales bool `json:",omitempty"`
	VencimientosPago     Vencimientos

	// Si una factura se pasa en abril pero el gasto correspondía a marzo
	// se puede completar estos campos y se genera un diferimiento.
	// La factura queda en la fecha contable que se puso, pero se
	// crea otra op con estas fechas.
	// La config define la cuenta de diferimiento.
	ImputacionDesde *fecha.Fecha `json:",omitempty"`
	// ImputacionHasta *fecha.Fecha `json:",omitempty"`

	CreatedAt time.Time

	// Luego de procesada la operación, estos son las structs que se persisten
	Factura       *ops.Op
	Reingreso     *ops.Op
	Recibo        *ops.Op
	Anulacion     *ops.Op
	Diferimientos []ops.Op

	// // Para mandar al frontend
	// Unicidades []unicidades.Unicidad
}

func (f Fact) TableName() string {
	return "fact"
}

func (f *Fact) Ops() (out []*ops.Op) {
	if f.Factura != nil {
		out = append(out, f.Factura)
	}
	if f.Recibo != nil {
		out = append(out, f.Recibo)
	}
	if f.Reingreso != nil {
		out = append(out, f.Reingreso)
	}
	for i := range f.Diferimientos {
		out = append(out, &f.Diferimientos[i])
	}
	return
}

// Pata determina la función de la partida contable dentro del asiento.
const (
	PataProducto            = "Renglon"
	PataProductoNoFacturado = "Renglon no facturado"
	PataPartidaDirecta      = "Directa"
	PataValor               = "Valor"
	PataIVA                 = "IVA"
	PataFacturaPatrimonial  = "Patrimonial factura"
	PataReciboPatrimonial   = "Patrimonial recibo"
	PataPendienteFlujo      = "Pendiente flujo"
	PataPendienteStock      = "Pendiente stock"

	// Es el IVA ganado por la operación de reingreso
	PataReingreso = "Reingreso"

	// En una factura de contado, las partidas que matan PataPatrimonialFactura y
	// PataPatrimonialRecibo
	PataAplicacion = "Patrimonial Aplicación"
	PataRedondeo   = "Redondeo"
)

// Caclula los totales de neto, iva y demás conceptos que van en una factura.
// Lo hace a partir de fact (no del asiento contable)
//
// Sirve para facturas proforma o ordenes de compra
func (f *Fact) Totalizar() (out []ops.TotalConcepto, err error) {

	type key struct {
		concepto iva.Concepto
		alic     iva.Alicuota
	}
	sum := map[key]dec.D4{}

	// Partidas productos
	for _, v := range f.Productos {
		switch v.AlicuotaIVA {
		case iva.IVAExento:
			sum[key{concepto: iva.Exento, alic: v.AlicuotaIVA}] += v.MontoTotal

		case iva.IVANoGravado:
			sum[key{concepto: iva.NoGravado, alic: v.AlicuotaIVA}] += v.MontoTotal

		default:
			// Neto
			sum[key{concepto: iva.BaseImponible, alic: v.AlicuotaIVA}] += v.MontoTotal

			// IVA
			porc, _ := v.AlicuotaIVA.Porcentaje()
			if err != nil {
				return out, errors.Wrap(err, "determinando porcentaje")
			}
			sum[key{concepto: iva.IVA, alic: v.AlicuotaIVA}] += dec.NewD4(v.MontoTotal.Float() * porc)
		}
	}

	// Partidas directas
	for _, v := range f.PartidasDirectas {
		switch v.AlicuotaIVA {
		case iva.IVAExento:
			sum[key{concepto: iva.Exento, alic: v.AlicuotaIVA}] += v.Monto

		case iva.IVANoGravado:
			sum[key{concepto: iva.NoGravado, alic: v.AlicuotaIVA}] += v.Monto

		default:
			// Neto
			sum[key{concepto: iva.BaseImponible, alic: v.AlicuotaIVA}] += v.Monto

			// IVA
			porc, err := v.AlicuotaIVA.Porcentaje()
			if err != nil {
				return out, errors.Wrap(err, "determinando porcentaje")
			}
			sum[key{concepto: iva.IVA, alic: v.AlicuotaIVA}] += dec.NewD4(v.Monto.Float() * porc)
		}
	}

	for k, v := range sum {
		c := ops.TotalConcepto{
			Concepto: k.concepto,
			Alic:     k.alic,
			Nombre:   iva.NombreCombinado(k.concepto, k.alic),
			Monto:    dec.NewD2(v.Float()),
		}
		out = append(out, c)
	}

	// Los ordeno
	sort.Slice(out, func(i, j int) bool {
		if out[i].Concepto != out[j].Concepto {
			return out[i].Concepto < out[j].Concepto
		}
		return out[i].Alic < out[j].Alic
	})

	return
}

// El campo pata me define la naturaleza de la partida.
// A los efectos de IVA, puede que haya varias patas que sean base imponible.
func PataBaseImponibleIVA(pata string) bool {
	switch pata {

	case PataProducto, PataPartidaDirecta:
		return true
	}
	return false
}

// Item me permite determinar para una Fact, cual op es la factura y cual el recibo.
const (
	ItemFactura                 = 1
	ItemRecibo                  = 2
	ItemAnulacion               = 3
	ItemReingreso               = 4
	ItemDiferimiento            = 5
	ItemDiferimientoCancelacion = 6

	// Los resúmenes que se pasan de las tarjetas bancarias
	// ItemTarjetaBancaria = 3
)
