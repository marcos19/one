package types

import (
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	uuid "github.com/gofrs/uuid"
)

// Aplicacion es el registro que se muestra al hacer un comprobante
// al momento de seleccionar a que partida de desea aplicar.
// Este es el dato estático. La operación se encargará al momento de grabar
// que las partidas que aparecen acá sean válidas.
//
// Hay dos tipos de aplicaciones: las de RECIBOS y las de PRODUCTOS.
// Las principal diferencia es que en las aplicaciones de recibos todos
// los datos necesarios para contabilizar están en el recibo.
// Cuando se aplican productos puede que el precio sea otro, es decir
// necesito la aplicación y la partida.
// Incluso puede que quiera aplicar a dos partidas de distintos remitos,
// pero querer mostrar la totalidad de productos en un solo renglón.
//
// En los recibos por ejemplo, el procedimiento es ver el listado de partidas
// disponibles y seleccionar de ahí.
// En una factura de venta que aplica a remitos, puede quiera cambiar los precios
// que figuraban en el remito. Es decir que necesito MÁS datos que los
// obtenidos de la aplicación.
//
// No graba cuenta contable ni ningún campo de especificación porque todos esos
// los copiará de la partida original.
//
// Cuando esté aplicando a un producto en dólares una factura que aplica
// a un remito, va a aplicar cantidades, va a aplicar dólares, y va a aplicar pesos.
// Por eso necesito los tres campos.
// Cuando aplico a productos la aplicación de pesos es proporcional a la cantidad.
// Si la partida tenía 100 camisas por $100.000 y aplico 20 camisas, el monto
// aplicado va a ser de $20.000. Si se vende a otro precio el programa determina
// a que cuenta va la diferencia.

// Del frontend 'MontoAplicado' llega siempre en signo positivo.
type Aplicacion struct {
	// El ID de la partida aplicada
	ID *uuid.UUID `json:",omitempty"`

	// Datos del comprobante al que está aplicando este
	FechaContable   fecha.Fecha `json:",omitempty"`
	FechaFinanciera fecha.Fecha `json:",omitempty"` // la fecha de la partida, el comprobante no tiene fecha financiera.
	TipoComp        string      `json:",omitempty"`
	NComp           string      `json:",omitempty"`
	Detalle         *string     `json:",omitempty"`

	Cuenta int  `json:",string,omitempty"`
	Centro *int `json:",string,omitempty"`

	// El MontoAplicado es el monto que se va a contabilizar en monto
	MontoOriginal  *dec.D2 // Me interesa para las cuentas corrientes nomás
	MontoPendiente dec.D2

	Dias          int    `json:",omitempty"`
	MontoAplicado dec.D2 `json:",omitempty"` // Sin signo, dato entrado desde el frontend
	Aplicacion    dec.D2 `json:",omitempty"` // Con el signo
}
