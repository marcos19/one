package types

import (
	"bitbucket.org/marcos19/one/pkg/iva"
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
)

// PartidaDirecta define cada uno de los renglones de un comprobante.
// Es la estructura tal cual es persistida en la base de datos.
// Todos los datos que se almacenan acá los son los elegidos por el usuario
// (o sugeridos por la UI). De esta manera cada vez que se grabe, todos los
// procesos correrán sobre estos datos y el resultado será idempotente.
// Se plantea la disyuntiva sobre que conviene:
//
// 1) Utilizar una estructura genérica para todos los comprobantes
// espejandola en una tabla en la base de datos; o bien
//
// 2) Que cada comprobante tenga la estructura que le corresponda y
// los guarde en un campo JSON.
//
// Luego de un profundo análisis se decidió que lo más práctico para el
// mantenimiento es utilizar esta estructura como un conducto genérico,
// y que cada package especializado use los campos a su antojo.
type PartidaDirecta struct {
	CuentaID     int `json:",string"`
	CuentaNombre string
	Centro       int          `json:",omitempty,string"`
	Concepto     iva.Concepto `json:",string"`
	Persona      *uuid.UUID   `json:",omitempty"`
	Sucursal     int          `json:",string,omitempty"`
	AlicuotaIVA  iva.Alicuota `json:",string"`
	// Persona          *int
	// Sucursal         *int
	Detalle string `json:",omitempty"`
	Monto   dec.D4

	// Son los a los que esta gravado el producto [IVA21, ITC]
	// Si el comprobante exige elegir conceptos.

	// En una misma factura podría tener productos que se vendan en
	// U$S y en pesos
	TipoMoneda string `json:",omitempty"`

	// La cuenta patrimonial de la persona a la que irán todos los otros conceptos
	CuentaPatrimonial int `json:",string"`
	CentroPatrimonial int `json:",omitempty,string"`

	// Cuando cargo una entidad, aca van todos los datos. Para modificaciones
	// voy a cargar un comprobante posterior con los datos corregidos.
	// Entidades []entidades.Entidad

}
