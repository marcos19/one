package types

import (
	"bitbucket.org/marcos19/one/pkg/iva"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
)

// Dato que queda grabado en la fact.
// Tiene todos los datos para poder mostrar e imprimir comprobante.
type AplicacionProducto struct {
	PartidaAplicada   uuid.UUID
	Producto          uuid.UUID
	SKU               uuid.UUID
	Cuenta            int `json:",string"`
	Codigo            string
	UM                string `json:",omitempty"`
	Envase            string `json:",omitempty"`
	Nombre            string
	Monto             dec.D2
	Cantidad          dec.D4
	AlicuotaIVA       *iva.Alicuota `json:",omitempty"`
	Centro            int           `json:",omitempty,string"`
	PorCuentaTerceros bool          `json:",omitempty"`
	Fecha             fecha.Fecha   // Fecha del comp al que aplica
	CompNombre        string
	NCompStr          string
}
