package types

import (
	"bitbucket.org/marcos19/one/pkg/unicidades"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	uuid "github.com/gofrs/uuid"
)

type PartidaValores struct {
	Cuenta  int    `json:",string"`
	Alias   string // Cheque de tercero
	Detalle string `json:",omitempty"` // BANCO MACRO N 4354345

	Caja     *int       `json:",string,omitempty"`
	Persona  *uuid.UUID `json:",omitempty"`        // Transferencia por ejemplo, Banco Credicoop
	Sucursal *int       `json:",string,omitempty"` // Cuenta 32443-2
	Centro   *int       `json:",string,omitempty"` // Cuenta 32443-2

	// Si es una unicidad nueva
	UnicidadNueva *unicidades.Unicidad `json:",omitempty"`

	// Si estoy referenciando una unicidad
	TipoUnicidad *int       `json:",string,omitempty"`
	UnicidadRef  *uuid.UUID `json:",omitempty"`

	Monto               dec.D2
	Vencimiento         fecha.Fecha `json:",omitempty"`
	Moneda              *int        `json:",string,omitempty"`
	MontoMonedaOriginal *dec.D4     `json:",omitempty"`
	TC                  *dec.D4     `json:",omitempty"`
}
