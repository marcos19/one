package types

import (
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
)

// Vencimiento es cada uno de los renglones.
type Vencimiento struct {
	Fecha fecha.Fecha
	Dias  int
	Monto dec.D2
}

type Vencimientos []Vencimiento

func (p Vencimientos) TotalEnMonedaContable() (sum dec.D2) {
	for _, v := range p {
		sum += v.Monto
	}
	return
}
