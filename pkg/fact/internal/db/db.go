package db

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

// Busca en la base de datos el registro de la tabla 'fact'.
func ReadOne(ctx context.Context, conn *pgxpool.Pool, comitente int, id uuid.UUID) (out types.Fact, err error) {
	query := `
		SELECT id, comitente, empresa, config, esquema, fecha, persona, condicion_pago,
			detalle, productos, partidas_directas, valores,	 vencimientos,
			vencimientos_pago, aplicaciones, aplicaciones_esta_factura, 
			created_at, es_contado, tiene_recibo, reingreso_monto, deposito, aplicaciones_productos,
			imputacion_desde, 
			reingreso_pago_cuenta, reingreso_pago_persona, reingreso_pago_sucursal, reingreso_pago_caja
		FROM fact
		WHERE comitente=$1 AND id=$2;
	`
	err = conn.QueryRow(ctx, query, comitente, id).Scan(
		&out.ID,
		&out.Comitente,
		&out.Empresa,
		&out.Config,
		&out.Esquema,
		&out.Fecha,
		&out.Persona,
		&out.CondicionPago,
		&out.Detalle,
		&out.Productos,
		&out.PartidasDirectas,
		&out.Valores,
		&out.Vencimientos,
		&out.VencimientosPago,
		&out.Aplicaciones,
		&out.AplicacionesEstaFactura,
		&out.CreatedAt,
		&out.EsContado,
		&out.TieneRecibo,
		&out.ReingresoMonto,
		&out.Deposito,
		&out.AplicacionesProductos,
		&out.ImputacionDesde,
		&out.ReingresoPagoCuenta,
		&out.ReingresoPagoPersona,
		&out.ReingresoPagoSucursal,
		&out.ReingresoPagoCaja,
	)
	if err != nil {
		return out, errors.Wrap(err, "querying fact")
	}
	return
}

// Inserta un registro en la tabla 'fact'.
func Create(ctx context.Context, f *types.Fact, tx pgx.Tx) (err error) {
	insertQuery := `
INSERT INTO fact
(
id, 
comitente, 
empresa, 
config, 
esquema,
fecha,
persona,
condicion_pago,
detalle,
productos,
partidas_directas,
valores,
vencimientos,
vencimientos_pago,
aplicaciones,
aplicaciones_esta_factura,
tiene_recibo,
es_contado,
reingreso_monto,
created_at,
aplicaciones_productos,
deposito,
imputacion_desde,
reingreso_pago_cuenta,
reingreso_pago_persona,
reingreso_pago_sucursal,
reingreso_pago_caja
) VALUES (
$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27
);
	`
	_, err = tx.Exec(ctx, insertQuery,
		f.ID,
		f.Comitente,
		f.Empresa,
		f.Config,
		f.Esquema,
		f.Fecha,
		f.Persona,
		f.CondicionPago,
		f.Detalle,
		f.Productos,
		f.PartidasDirectas,
		f.Valores,
		f.Vencimientos,
		f.VencimientosPago,
		f.Aplicaciones,
		f.AplicacionesEstaFactura,
		f.TieneRecibo,
		f.EsContado,
		f.ReingresoMonto,
		f.CreatedAt,
		f.AplicacionesProductos,
		f.Deposito,
		f.ImputacionDesde,
		f.ReingresoPagoCuenta,
		f.ReingresoPagoPersona,
		f.ReingresoPagoSucursal,
		f.ReingresoPagoCaja,
	)

	if err != nil {
		return deferror.DB(err, "insertando en tabla fact")
	}
	return
}
