package lookups

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/afip"
	"bitbucket.org/marcos19/one/pkg/afip/afipmodels"
	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/condiciones"
	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"bitbucket.org/marcos19/one/pkg/fact/config"
	"bitbucket.org/marcos19/one/pkg/fact/esquemas"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/personas"
	"bitbucket.org/marcos19/one/pkg/productos"
	"bitbucket.org/marcos19/one/pkg/productos/categorias"
	"bitbucket.org/marcos19/one/pkg/unicidadesdef"
	"bitbucket.org/marcos19/one/pkg/usuarios"
	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/rs/zerolog/log"
)

// lookups contiene todos los datos que se necesitan para procesar una fact.
type Lookups struct {
	Empresa               empresas.Empresa
	Config                config.Config
	Esquema               esquemas.Esquema
	CompFactura           *comps.Comp
	CompRecibo            *comps.Comp
	Condicion             *condiciones.Condicion
	CompAFIP              *afipmodels.ComprobanteAFIP
	Persona               personas.Persona
	PersonaIdentificacion afipmodels.Identificacion
	// cuentas                map[int]cuentas.Cuenta
	Centros                map[int]int
	Personas               map[uuid.UUID]personas.Persona
	Productos              map[uuid.UUID]productos.Producto
	Categorias             map[int]categorias.Categoria
	SKUs                   map[uuid.UUID]productos.SKU
	UnicidadesDef          map[int]unicidadesdef.Def
	AplicacionesAnteriores map[uuid.UUID]ops.Partida // Para recibos
	Usuario                usuarios.Usuario
}

// func (l *Lookups) Cuenta(id int) (cta cuentas.Cuenta, err error) {
// 	cta, ok := l.cuentas[id]
// 	if !ok {
// 		return cta, errors.Errorf("cuenta %v no encontrada en lookup", id)
// 	}
// 	return
// }

func (l *Lookups) CuentaIVA() (id int, err error) {

	// Hay imputaciones sin categoría para esta config?
	for _, v := range l.Config.Imputaciones {
		if v.Categoria == 0 {
			if v.IVA != 0 {
				return v.IVA, nil
			}
		}
	}
	return id, errors.Errorf("no se determinó cuenta IVA general")
}

// Llena una struct con todos los lookups que se necesitan para procesar una fact
func New(ctx context.Context, conn *pgxpool.Pool, f *types.Fact, usuario string,
	empresasHandler empresas.ReaderOne,
	usuariosReader usuarios.ReaderOne,
	configHandler *config.Handler,
	esquemasHandler *esquemas.Handler,
	compHandler comps.ReaderOne,
	afipHandler *afip.Handler,
	personasGetter personas.ReaderOne,
	condicionesHandler *condiciones.Handler,
	cuentasGetter cuentas.ReaderOne,
	productosGetter *productos.Handler,
	unicidadesdefHandler *unicidadesdef.Handler,
	categoriasHandler *categorias.Handler,
	opsHandler ops.ReaderPartidas,

) (out Lookups, err error) {

	// out.cuentas = map[int]cuentas.Cuenta{}
	out.Centros = map[int]int{}
	out.Personas = map[uuid.UUID]personas.Persona{}
	out.Productos = map[uuid.UUID]productos.Producto{}
	out.Categorias = map[int]categorias.Categoria{}
	out.UnicidadesDef = map[int]unicidadesdef.Def{}
	out.AplicacionesAnteriores = map[uuid.UUID]ops.Partida{}

	// Empresa
	out.Empresa, err = empresasHandler.ReadOne(ctx, empresas.ReadOneReq{Comitente: f.Comitente, ID: f.Empresa})
	if err != nil {
		return out, errors.Wrap(err, "buscando empresa")
	}

	// usuario
	out.Usuario, err = usuariosReader.ReadOne(ctx, usuario)
	if err != nil {
		return out, errors.Wrap(err, "buscando usuario")
	}

	// Config
	log.Debug().Msgf("Buscando config...")
	cfgFiltro := config.ReadOneReq{ID: f.Config, Comitente: f.Comitente}
	out.Config, err = configHandler.ReadOne(ctx, cfgFiltro)
	if err != nil {
		return out, errors.Wrap(err, "buscando config")
	}
	if out.Config.Empresa != f.Empresa {
		return out, errors.Errorf("la configuración seleccionada %v no pertenece a la empresa de la operación %v", out.Config.ID, f.Empresa)
	}

	// Esquema
	log.Debug().Msgf("Buscando esquemas...")
	out.Esquema, err = esquemasHandler.ReadOne(ctx, esquemas.ReadOneReq{Comitente: f.Comitente, ID: f.Esquema})
	if err != nil {
		return out, errors.Wrap(err, "buscando esquema")
	}
	if out.Esquema.Empresa != f.Empresa {
		return out, errors.Errorf("el esquema seleccionado %v no pertenece a la empresa de la operación %v", out.Esquema.ID, f.Empresa)
	}

	{ // Comp factura
		c, estaba := out.Esquema.Comprobantes["Factura"]
		if estaba {
			comp, err := compHandler.ReadOne(ctx, comps.ReadOneReq{Comitente: f.Comitente, ID: int(c)})
			if err != nil {
				return out, errors.Wrap(err, "buscando comp Factura")
			}
			out.CompFactura = new(comps.Comp)
			*out.CompFactura = comp
			if out.CompFactura.Empresa != f.Empresa {
				return out, errors.Errorf("el comprobante %v no pertenece a la empresa de la operación %v", out.CompFactura.ID, f.Empresa)
			}

			if f.Factura != nil {
				if f.Factura.AFIPComprobanteID != nil {
					if *f.Factura.AFIPComprobanteID != 0 {
						af, err := afipHandler.Comp(ctx, *f.Factura.AFIPComprobanteID)
						if err != nil {
							return out, errors.Wrap(err, "buscando comprobante original AFIP")
						}
						out.CompAFIP = &af
					}
				}
			}
		}
	}

	{ // Comp recibo
		c, estaba := out.Esquema.Comprobantes["Recibo"]
		if estaba {
			comp, err := compHandler.ReadOne(ctx, comps.ReadOneReq{Comitente: f.Comitente, ID: int(c)})
			if err != nil {
				return out, errors.Wrap(err, "buscando comp Recibo")
			}
			out.CompRecibo = new(comps.Comp)
			*out.CompRecibo = comp
			if out.CompRecibo.Empresa != f.Empresa {
				return out, errors.Errorf("el comprobante %v no pertenece a la empresa de la operación %v", out.CompFactura.ID, f.Empresa)
			}
		}
	}

	// Persona
	out.Persona, err = personasGetter.ReadOne(ctx, personas.ReadOneReq{
		Comitente: f.Comitente,
		ID:        f.Persona.ID,
	})
	if err != nil {
		return out, errors.Wrap(err, "buscando persona")
	}

	{ // Personas de valores
		pm := map[uuid.UUID]struct{}{}
		for _, v := range f.Valores {
			if v.Persona != nil {
				pm[*v.Persona] = struct{}{}
			}
		}
		for k := range pm {
			p, err := personasGetter.ReadOne(ctx, personas.ReadOneReq{
				Comitente: f.Comitente,
				ID:        k,
			})
			if err != nil {
				return out, errors.Wrap(err, "buscando persona")
			}
			out.Personas[k] = p
		}
	}
	// Identificacion fiscal
	out.PersonaIdentificacion, err = afipHandler.IdentificacionPorID(ctx, f.Persona.TipoIdentificacionFiscal)
	if err != nil {
		return out, errors.Wrapf(err, "buscando identificación ID=%v", f.Persona.TipoIdentificacionFiscal)
	}

	// Condiciones
	if f.CondicionPago != nil {
		cond, err := condicionesHandler.ReadOne(ctx,
			condiciones.ReadOneReq{
				Comitente: f.Comitente,
				ID:        *f.CondicionPago,
			},
		)
		if err != nil {
			return out, errors.Wrap(err, "buscando condición de pago")
		}
		out.Condicion = &cond
	}

	// Cuentas partidas directas
	// err = traerCuentasPartidasDirectas(ctx, &out, f.Comitente, f.PartidasDirectas, cuentasGetter)
	// if err != nil {
	// 	return out, errors.Wrap(err, "trayendo productos con sus cuentas")
	// }

	// Traigo productos (los uso en aplicaciones productos)
	err = traerProductos(ctx, &out, f.Comitente, productosGetter, unicidadesdefHandler, categoriasHandler, f.Productos)
	if err != nil {
		return out, errors.Wrap(err, "trayendo productos con sus cuentas")
	}

	// Traigo categorias de productos
	out.Categorias, err = traerCategorias(ctx, f.Comitente, out.Productos, categoriasHandler)
	if err != nil {
		return out, errors.Wrap(err, "trayendo productos con sus cuentas")
	}

	// Traigo SKUs (los uso en aplicaciones productos)
	err = traerSKUs(ctx, &out, f.Comitente, f.Config, productosGetter, conn, f.Productos, f.AplicacionesProductos)
	if err != nil {
		return out, errors.Wrap(err, "trayendo SKUs y sus cuentas")
	}

	// Traigo aplicaciones anteriores
	err = traerAplicacionesAnteriores(ctx, opsHandler, &out, f.Comitente, f.Aplicaciones)
	if err != nil {
		return out, errors.Wrap(err, "trayendo aplicaciones anteriores")
	}

	// Traigo las cuentas posta
	ids := map[int]struct{}{}
	for _, v := range f.Valores {
		ids[v.Cuenta] = struct{}{}
		if v.UnicidadNueva != nil {

			// Busco unicidad def
			def, err := unicidadesdefHandler.ReadOne(ctx, unicidadesdef.ReadOneReq{
				Comitente: f.Comitente,
				ID:        v.UnicidadNueva.Definicion,
			})
			if err != nil {
				return out, errors.Wrapf(err, "buscando definición de unicidad %v", v.UnicidadNueva.Definicion)
			}
			out.UnicidadesDef[v.UnicidadNueva.Definicion] = def
		}
	}
	ivaID, _ := out.CuentaIVA()
	if ivaID != 0 {
		ids[ivaID] = struct{}{}
	}
	// for k := range ids {
	// 	// Cuenta renglón
	// 	cta, err := cuentasGetter.ReadOne(ctx, cuentas.ReadOneReq{Comitente: f.Comitente, ID: k})
	// 	if err != nil {
	// 		return out, errors.Wrapf(err, "buscando cuenta %v", k)
	// 	}
	// 	out.cuentas[k] = cta
	// }

	return
}

// func traerCuentasPartidasDirectas(
// 	ctx context.Context,
// 	lu *Lookups,
// 	comitente int,
// 	pp []types.PartidaDirecta,
// 	cuentasStore cuentas.Getter,
// ) (err error) {

// 	for _, v := range pp {
// 		// Cuenta renglón
// 		cta, err := cuentasStore.ReadOne(ctx, cuentas.ReadOneReq{Comitente: comitente, ID: v.CuentaID})
// 		if err != nil {
// 			return errors.Wrapf(err, "buscando cuenta %v", v.CuentaID)
// 		}
// 		lu.cuentas[v.CuentaID] = cta

// 		// Cuenta patrimonial
// 		ctaPat, err := cuentasStore.ReadOne(ctx, cuentas.ReadOneReq{Comitente: comitente, ID: v.CuentaPatrimonial})
// 		if err != nil {
// 			return errors.Wrapf(err, "buscando cuenta patrimonial=%v", v.CuentaPatrimonial)
// 		}
// 		lu.cuentas[v.CuentaPatrimonial] = ctaPat
// 	}
// 	return
// }

func traerProductos(
	ctx context.Context,
	lu *Lookups,
	comitente int,
	productosGetter productos.Getter,
	unicidadesdefHandler *unicidadesdef.Handler,
	categoriasHandler *categorias.Handler,
	pp []types.PartidaProducto,
) (err error) {

	for i, v := range pp {

		// Producto
		prod, err := productosGetter.ReadOne(ctx, productos.ReadOneReq{
			Comitente: comitente,
			ID:        v.Producto,
		})
		if err != nil {
			return errors.Wrapf(err, "buscando producto renglón %v", i+1)
		}
		lu.Productos[v.Producto] = prod

		// Unicidad
		if prod.TrabajaConUnicidades {
			if prod.ClaseUnicidad == nil {
				return errors.Errorf("el producto %v tiene definido que trabaja con unicidades, pero no tiene determinado el tipo")
			}
			clase := *prod.ClaseUnicidad
			// Busco unicidad def
			def, err := unicidadesdefHandler.ReadOne(ctx, unicidadesdef.ReadOneReq{
				Comitente: comitente,
				ID:        clase,
			})
			if err != nil {
				return errors.Wrapf(err, "buscando definición de unicidad %v", *prod.ClaseUnicidad)
			}
			lu.UnicidadesDef[clase] = def
		}

		// Centro de imputación
		centro, err := categoriasHandler.Centro(ctx, categorias.ReadOneReq{
			Comitente: comitente,
			ID:        prod.Categoria,
		})
		if err != nil {
			return errors.Wrapf(err, "determinando centro de costo de cuenta patrimonial con apertura por producto")
		}
		lu.Centros[prod.Categoria] = centro

	}
	return
}

func traerCategorias(
	ctx context.Context,
	comitente int,
	pp map[uuid.UUID]productos.Producto,
	catHandler *categorias.Handler,
) (map[int]categorias.Categoria, error) {

	// Determino IDs
	cats := map[int]struct{}{}
	for _, v := range pp {
		cats[v.Categoria] = struct{}{}
	}
	if len(cats) == 0 {
		return nil, nil
	}

	// Busco en DB
	found, err := catHandler.ReadMany(ctx, categorias.ReadManyReq{
		Comitente: comitente,
		Tipo:      "imputables",
	})
	if err != nil {
		return nil, errors.Wrap(err, "buscando categorías de productos")
	}

	// Hago map
	out := map[int]categorias.Categoria{}
	for _, v := range found {
		out[*v.ID] = v
	}

	return out, nil
}

func traerSKUs(ctx context.Context,
	lu *Lookups,
	comitente, cfg int,
	productosGetter productos.Getter,
	conn *pgxpool.Pool,
	pp []types.PartidaProducto,
	aa []types.AplicacionProducto,
) (err error) {

	ids := map[uuid.UUID]struct{}{}
	for _, v := range aa {
		ids[v.SKU] = struct{}{}
	}
	for _, v := range pp {
		ids[v.SKU] = struct{}{}
	}

	skuIds := []uuid.UUID{}
	for k := range ids {
		skuIds = append(skuIds, k)
	}

	if len(skuIds) > 0 {
		skus, err := productos.ReadManySKU(ctx, conn, productos.ReadManySKUReq{Comitente: comitente, IDs: skuIds})
		if err != nil {
			return errors.Wrap(err, "buscando SKUs de productos aplicados")
		}
		lu.SKUs = map[uuid.UUID]productos.SKU{}
		for i, v := range skus {
			lu.SKUs[v.ID] = v
			log.Debug().Interface("productoID", v.ID).Str("nombre", v.Nombre).Msg("Fijando SKU en lu")
			// Traigo el producto
			prod, err := productosGetter.ReadOne(ctx, productos.ReadOneReq{
				Comitente: comitente,
				ID:        v.Producto,
			})
			if err != nil {
				return errors.Wrapf(err, "buscando producto de aplicación SKU renglón %v", i+1)
			}
			lu.Productos[prod.ID] = prod

			// 			{ // Cuenta renglón

			// 				// Determino cuenta
			// 				ctaID, err := h.configHandler.CuentaRenglon(ctx, comitente, cfg, v.Producto)
			// 				if err != nil {
			// 					return errors.Wrap(err, "determinando cuenta contable de producto renglon")
			// 				}
			// 				lu.cuentasProductoRenglon[v.Producto] = ctaID
			// 			}

			// 			// Cuentas IVA
			// 			if lu.CompFactura.CalculaIVA {
			// 				// Busco cuenta de IVA
			// 				cta, err := h.configHandler.CuentaIVA(ctx, comitente, cfg, v.Producto)
			// 				if err == nil {
			// 					// Que me de error al momento de buscar la cuenta
			// 					lu.cuentasProductosIVA[v.Producto] = cta
			// 				}
			// 			}

			// 			{ // Cuenta patrimonial

			// 				// Determino cuenta
			// 				ctaID, err := h.configHandler.CuentaPatrimonial(ctx, comitente, cfg, v.Producto)
			// 				if err != nil {
			// 					return errors.Wrap(err, "determinando cuenta patrimonial de producto")
			// 				}
			// 				lu.cuentasProductosPatrimonial[v.Producto] = ctaID
			// 			}
		}
	}
	return
}

func traerAplicacionesAnteriores(ctx context.Context, opsH ops.ReaderPartidas, lu *Lookups, comitente int, aa []types.Aplicacion) (err error) {

	ids := []uuid.UUID{}
	for _, v := range aa {
		if v.ID == nil {
			return errors.Errorf("partida aplicada anterior no tenía definida ID")
		}
		ids = append(ids, *v.ID)
	}
	if len(ids) > 0 {
		pp, err := opsH.Partidas(ctx,
			ops.PartidasReq{
				Comitente: comitente,
				IDs:       ids,
			})
		if err != nil {
			return errors.Wrap(err, "buscando partidas de aplicaciones")
		}

		// Las pongo en un map
		for _, v := range pp {
			lu.AplicacionesAnteriores[v.ID] = v
		}
	}
	return
}
