package diferimiento

import (
	"context"
	"fmt"
	"math"

	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
)

func CrearOp(
	ctx context.Context,
	f *types.Fact,
	ctaDif int,
	compID int,
	compCancelacionID int,
) (out []ops.Op, err error) {

	out = []ops.Op{}
	if f.ImputacionDesde == nil {
		if *f.ImputacionDesde == 0 {
			return
		}
	}
	apliID, err := uuid.NewV1()
	if err != nil {
		return out, err
	}

	{ // First op
		op := ops.Op{
			ID:            apliID,
			Comitente:     f.Comitente,
			Empresa:       f.Empresa,
			TranID:        f.ID,
			Fecha:         *f.ImputacionDesde,
			FechaOriginal: *f.ImputacionDesde,
			CompID:        compID,
			Programa:      f.Factura.Programa,
			Persona:       f.Factura.Persona,
			PersonaNombre: f.Factura.PersonaNombre,
			TranDef:       f.Config,
			Detalle:       fmt.Sprintf("%v %v desde %v a %v", f.Factura.CompNombre, f.Factura.NCompStr, f.Factura.Fecha, f.ImputacionDesde),
			Usuario:       f.Factura.Usuario,
		}
		op.CompItem = new(int)
		*op.CompItem = types.ItemDiferimiento

		if f.Factura.Sucursal != nil {
			op.Sucursal = new(int)
			*op.Sucursal = *f.Factura.Sucursal
		}

		sum := dec.D2(0)

		// Partidas de gasto
		for _, v := range f.Factura.PartidasContables {
			switch v.Pata {
			case types.PataProducto, types.PataProductoNoFacturado, types.PataPartidaDirecta:
				v.ID, err = uuid.NewV1()
				if err != nil {
					return
				}
				if v.PartidaAplicada != nil {
					// Esto no debería pasar, lo pongo por las dudas
					return out, errors.Errorf("la partida que se intenta difererir utiliza aplicaciones")
				}
				v.AlicuotaIVA = nil
				v.ConceptoIVA = nil
				sum += v.Monto
				op.PartidasContables = append(op.PartidasContables, v)
			}
		}

		// Partida patrimonial
		p := ops.Partida{
			ID:     apliID,
			Cuenta: ctaDif,
			Monto:  -sum,
		}

		// Persona
		p.Persona = new(uuid.UUID)
		*p.Persona = f.Persona.ID
		if op.Sucursal != nil {
			p.Sucursal = new(int)
			*p.Sucursal = *op.Sucursal
		}

		// Partida aplicada (crea nueva partida)
		p.PartidaAplicada = new(uuid.UUID)
		*p.PartidaAplicada = apliID

		op.TotalComp = new(dec.D2)
		*op.TotalComp = dec.NewD2(math.Abs(p.Monto.Float()))
		op.PartidasContables = append(op.PartidasContables, p)

		out = append(out, op)
	}

	{ // Second op
		op := ops.Op{
			Comitente:     f.Comitente,
			Empresa:       f.Empresa,
			TranID:        f.ID,
			Fecha:         f.Factura.Fecha,
			FechaOriginal: f.Factura.Fecha,
			CompID:        compCancelacionID,
			Programa:      f.Factura.Programa,
			Persona:       f.Factura.Persona,
			PersonaNombre: f.Factura.PersonaNombre,
			TranDef:       f.Config,
			Detalle:       fmt.Sprintf("%v %v desde %v a %v", f.Factura.CompNombre, f.Factura.NCompStr, f.Factura.Fecha, f.ImputacionDesde),
			Usuario:       f.Factura.Usuario,
		}

		op.CompItem = new(int)
		*op.CompItem = types.ItemDiferimientoCancelacion

		if f.Factura.Sucursal != nil {
			op.Sucursal = new(int)
			*op.Sucursal = *f.Factura.Sucursal
		}

		op.ID, err = uuid.NewV1()
		if err != nil {
			return
		}

		sum := dec.D2(0)

		// Partidas de gasto
		for _, v := range f.Factura.PartidasContables {
			switch v.Pata {
			case types.PataProducto, types.PataProductoNoFacturado, types.PataPartidaDirecta:
				v.ID, err = uuid.NewV1()
				if err != nil {
					return
				}
				if v.PartidaAplicada != nil {
					// Esto no debería pasar, lo pongo por las dudas
					return out, errors.Errorf("la partida que se intenta difererir utiliza aplicaciones")
				}
				v.AlicuotaIVA = nil
				v.ConceptoIVA = nil
				v.Monto = -v.Monto
				sum += v.Monto
				op.PartidasContables = append(op.PartidasContables, v)
			}
		}

		// Partida patrimonial
		p := ops.Partida{
			Cuenta: ctaDif,
			Monto:  -sum,
		}
		p.ID, err = uuid.NewV1()
		if err != nil {
			return out, err
		}

		// Persona
		p.Persona = new(uuid.UUID)
		*p.Persona = f.Persona.ID
		if op.Sucursal != nil {
			p.Sucursal = new(int)
			*p.Sucursal = *op.Sucursal
		}

		// Partida aplicada (aplica a la otra op)
		p.PartidaAplicada = new(uuid.UUID)
		*p.PartidaAplicada = apliID

		op.TotalComp = new(dec.D2)
		*op.TotalComp = dec.NewD2(math.Abs(p.Monto.Float()))
		op.PartidasContables = append(op.PartidasContables, p)
		out = append(out, op)
	}

	return
}
