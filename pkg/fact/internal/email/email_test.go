package email

import (
	"context"
	"io"
	"testing"
	"time"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/emails"
	"bitbucket.org/marcos19/one/pkg/emailsender"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"bitbucket.org/marcos19/one/pkg/personas"
	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestEnviarComprobantes(t *testing.T) {
	const comitente = 1

	t.Run("should work", func(t *testing.T) {
		err := EnviarComprobantes(
			context.Background(),
			comitente,
			oneData,
			mockPersona{},
			mockComp{tpl: "body del mail"},
			mockEmpresa{},
			mockSender{},
			true,
		)
		assert.Nil(t, err, "%v", err)
	})

	t.Run("fails persona", func(t *testing.T) {
		err := EnviarComprobantes(
			context.Background(),
			comitente,
			oneData,
			mockPersona{err: errors.Errorf("test error")},
			mockComp{tpl: "body del mail"},
			mockEmpresa{},
			mockSender{},
			false,
		)
		assert.NotNil(t, err)
	})

	t.Run("fails template", func(t *testing.T) {
		err := EnviarComprobantes(
			context.Background(),
			comitente,
			oneData,
			mockPersona{},
			mockComp{},
			mockEmpresa{},
			mockSender{},
			false,
		)
		assert.NotNil(t, err)
	})

	t.Run("fails comp", func(t *testing.T) {
		err := EnviarComprobantes(
			context.Background(),
			comitente,
			oneData,
			mockPersona{},
			mockComp{err: errors.Errorf("test error")},
			mockEmpresa{},
			mockSender{},
			false,
		)
		assert.NotNil(t, err)
	})

	t.Run("fails empresa", func(t *testing.T) {
		err := EnviarComprobantes(
			context.Background(),
			comitente,
			oneData,
			mockPersona{},
			mockComp{tpl: "body del mail"},
			mockEmpresa{err: errors.Errorf("test error")},
			mockSender{},
			false,
		)
		assert.NotNil(t, err)
	})

	t.Run("fails sender", func(t *testing.T) {
		err := EnviarComprobantes(
			context.Background(),
			comitente,
			oneData,
			mockPersona{},
			mockComp{tpl: "body del mail"},
			mockEmpresa{},
			mockSender{err: errors.Errorf("test error")},
			false,
		)
		assert.NotNil(t, err)
	})

	t.Run("timeouts", func(t *testing.T) {
		ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond*500)
		defer cancel()

		err := EnviarComprobantes(
			ctx,
			comitente,
			oneData,
			mockPersona{},
			mockComp{tpl: "body del mail"},
			mockEmpresa{},
			mockSender{timeToSendMail: time.Second},
			false,
		)
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "deadline")
	})
}

type mockSender struct {
	err            error
	timeToSendMail time.Duration
}

func (m mockSender) Send(context.Context, emailsender.Req) error {
	if m.err != nil {
		return m.err
	}
	time.Sleep(m.timeToSendMail)
	return nil
}
func (m mockSender) SetConfig(host string, port int, user, pass string) {}

type mockPersona struct {
	err error
}

func (m mockPersona) ReadOne(context.Context, personas.ReadOneReq) (out personas.Persona, err error) {
	if m.err != nil {
		err = m.err
		return
	}
	out.Emails = []emails.Email{
		{
			Email: "test@test.com",
		},
	}
	return
}

type mockEmpresa struct {
	err error
	tpl string
}

func (m mockEmpresa) ReadOne(ctx context.Context, req empresas.ReadOneReq) (out empresas.Empresa, err error) {
	if m.err != nil {
		return out, m.err
	}

	return empresas.Empresa{MailTemplate: m.tpl}, nil
}

type mockComp struct {
	err error
	tpl string
}

func (m mockComp) ReadOne(ctx context.Context, req comps.ReadOneReq) (out comps.Comp, err error) {
	if m.err != nil {
		return out, m.err
	}

	return comps.Comp{MailTemplate: m.tpl}, nil
}
func TestEnviarComprobantesMismaEmpresa(t *testing.T) {

	t.Run("missing data", func(t *testing.T) {
		err := enviarComprobantesMismaEmpresa(
			context.Background(),
			nil,
			&mockTo{q: 1},
			"someone",
			"Something",
			&mockBody{},
			&mockCred{},
			false,
		)
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "data")
	})
	t.Run("missing from", func(t *testing.T) {
		err := enviarComprobantesMismaEmpresa(
			context.Background(),
			oneData,
			&mockTo{q: 1},
			"",
			"Something",
			&mockBody{},
			&mockCred{},
			false,
		)
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "from")
	})
	t.Run("missing subject", func(t *testing.T) {
		err := enviarComprobantesMismaEmpresa(
			context.Background(),
			oneData,
			&mockTo{q: 1},
			"someone",
			"",
			&mockBody{},
			&mockCred{},
			false,
		)
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "asunto")
	})
	t.Run("missing outputer", func(t *testing.T) {
		err := enviarComprobantesMismaEmpresa(
			context.Background(),
			oneDataMissingOutputer,
			&mockTo{q: 1},
			"someone",
			"Something",
			&mockBody{},
			&mockCred{},
			false,
		)
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "outputer")
	})
	t.Run("empty filename", func(t *testing.T) {
		err := enviarComprobantesMismaEmpresa(
			context.Background(),
			oneDataMissingFileName,
			&mockTo{q: 1},
			"someone",
			"Something",
			&mockBody{},
			&mockCred{},
			false,
		)
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "nombre")
	})

	t.Run("missing senderTo", func(t *testing.T) {
		err := enviarComprobantesMismaEmpresa(
			context.Background(),
			oneData,
			nil, //		&mockSender{q: 1},
			"someone",
			"Something",
			&mockBody{},
			&mockCred{},
			false,
		)
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "send")
	})

	t.Run("missing body", func(t *testing.T) {
		err := enviarComprobantesMismaEmpresa(
			context.Background(),
			oneData,
			&mockTo{q: 1},
			"someone",
			"Something",
			nil, // &mockBody{},
			&mockCred{},
			false,
		)
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "bodier")
	})

	t.Run("missing cred", func(t *testing.T) {
		err := enviarComprobantesMismaEmpresa(
			context.Background(),
			oneData,
			&mockTo{q: 1},
			"someone",
			"Something",
			&mockBody{},
			nil, // &mockCred{},
			false,
		)
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "sender")
	})

	t.Run("sin direcciones", func(t *testing.T) {
		err := enviarComprobantesMismaEmpresa(
			context.Background(),
			oneData,
			&mockTo{q: 0},
			"someone",
			"Something",
			&mockBody{},
			&mockCred{},
			false,
		)
		require.Nil(t, err)
	})
	t.Run("sin direcciones y must", func(t *testing.T) {
		err := enviarComprobantesMismaEmpresa(
			context.Background(),
			oneData,
			&mockTo{q: 0},
			"someone",
			"Something",
			&mockBody{},
			&mockCred{},
			true,
		)
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "direcc")
	})

	t.Run("sender error", func(t *testing.T) {
		err := enviarComprobantesMismaEmpresa(
			context.Background(),
			oneData,
			&mockTo{q: -1},
			"someone",
			"Something",
			&mockBody{},
			&mockCred{},
			false,
		)
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "direcc")
	})

	t.Run("bodier error", func(t *testing.T) {
		err := enviarComprobantesMismaEmpresa(
			context.Background(),
			oneData,
			&mockTo{q: 1},
			"someone",
			"Something",
			&mockBody{err: errors.Errorf("test error")},
			&mockCred{},
			false,
		)
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "cuerpo")
	})

	t.Run("falla servidor mail", func(t *testing.T) {
		err := enviarComprobantesMismaEmpresa(
			context.Background(),
			oneData,
			&mockTo{q: 1},
			"someone",
			"Something",
			&mockBody{},
			&mockCred{err: errors.Errorf("test error")},
			false,
		)
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "sender")
	})
}

var oneData = []CompData{
	{
		ID:            uuid.FromStringOrNil("acebdd10-6804-11ed-aa69-afe0ebd48898"),
		Empresa:       1,
		Persona:       uuid.FromStringOrNil("acebdd10-6804-11ed-aa69-afe0ebd48898"),
		Outputer:      mockOutput{},
		CompNombre:    "Factura A 1",
		PersonaNombre: "Pedro",
		CompID:        1,
	},
}
var oneDataMissingFileName = []CompData{
	{
		ID:         uuid.FromStringOrNil("acebdd10-6804-11ed-aa69-afe0ebd48898"),
		Empresa:    1,
		Persona:    uuid.FromStringOrNil("acebdd10-6804-11ed-aa69-afe0ebd48898"),
		Outputer:   mockOutput{},
		CompNombre: "",
	},
}
var oneDataMissingOutputer = []CompData{
	{
		ID:      uuid.FromStringOrNil("acebdd10-6804-11ed-aa69-afe0ebd48898"),
		Empresa: 1,
		Persona: uuid.FromStringOrNil("acebdd10-6804-11ed-aa69-afe0ebd48898"),
	},
}

var twoData = []CompData{
	{
		ID:         uuid.FromStringOrNil("acebdd10-6804-11ed-aa69-afe0ebd48898"),
		Empresa:    1,
		Persona:    uuid.FromStringOrNil("acebdd10-6804-11ed-aa69-afe0ebd48898"),
		CompNombre: "Factura A 1",
	},
	{
		ID:         uuid.FromStringOrNil("acebdd10-6804-11ed-aa69-afe0ebd48898"),
		Empresa:    1,
		Persona:    uuid.FromStringOrNil("acebdd10-6804-11ed-aa69-afe0ebd48898"),
		CompNombre: "Factura A 2",
	},
}

type mockOutput struct {
}

func (m mockOutput) Output(w io.Writer) error {
	return nil
}

type mockData struct{ cantidad int }

func (m *mockData) Get(context.Context) (out []CompData, err error) {
	switch m.cantidad {
	case 0:
		return nil, nil
	case 1:
		return []CompData{
			{
				ID:      uuid.FromStringOrNil("acebdd10-6804-11ed-aa69-afe0ebd48898"),
				Empresa: 1,
				Persona: uuid.FromStringOrNil("acebdd10-6804-11ed-aa69-afe0ebd48898"),
			},
		}, nil

	case 2:
		return []CompData{
			{
				ID:      uuid.FromStringOrNil("acebdd10-6804-11ed-aa69-afe0ebd48898"),
				Empresa: 1,
				Persona: uuid.FromStringOrNil("acebdd10-6804-11ed-aa69-afe0ebd48898"),
			},
			{
				ID:      uuid.FromStringOrNil("acebdd10-6804-11ed-aa69-afe0ebd48898"),
				Empresa: 1,
				Persona: uuid.FromStringOrNil("acebdd10-6804-11ed-aa69-afe0ebd48898"),
			},
		}, nil
	}
	return out, errors.Errorf("cantidad no disponible")
}

type mockTo struct{ q int }

func (m *mockTo) Adresses(ctx context.Context) (out []string, err error) {

	switch m.q {
	case 0:
		return
	case 1:
		return []string{"test@test.com"}, nil
	case 2:
		return []string{"test@test.com", "test2@test.com"}, nil
	}
	return out, errors.Errorf("test error")
}

type mockBody struct{ err error }

func (m *mockBody) Body(context.Context) (out string, err error) {

	if m.err != nil {
		return out, m.err
	}
	return "Body del mail", nil
}

type mockCred struct{ err error }

func (m *mockCred) Send(context.Context, emailsender.Req) (err error) {
	return m.err
}
