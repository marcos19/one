// Procesos:
// - En base a las OpID generar los PDF.
// - Determinar la empresa para saber que datos de mail Usuario
// - Determinar las cuentas a las que enviaré el mail.
// - Determinar cuerpo del mail
package email

import (
	"context"
	"fmt"
	"sync"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/emailsender"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"bitbucket.org/marcos19/one/pkg/fact/internal/email/body"
	"bitbucket.org/marcos19/one/pkg/fact/internal/email/body/tplget"
	"bitbucket.org/marcos19/one/pkg/fact/internal/email/cred"
	"bitbucket.org/marcos19/one/pkg/fact/internal/email/sender"
	"bitbucket.org/marcos19/one/pkg/fact/internal/email/to"
	"bitbucket.org/marcos19/one/pkg/personas"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/niler"
	"github.com/davecgh/go-spew/spew"
	"github.com/gofrs/uuid"
	"github.com/rs/zerolog/log"
)

var _ Bodier = new(body.UnComprobante)

// var _ SenderNotConfigured = new(sender.Sender)
var _ SenderTo = new(to.PersonaAdress)
var _ body.TemplateGetter = new(tplget.TemplateDBGet)

// - mustEnviar: si no encuentra dirección y este campo es true devuelve error.
func EnviarComprobantes(
	ctx context.Context,
	comitente int,
	data []CompData,
	personaReader personas.ReaderOne,
	compReader comps.ReaderOne,
	empresasReader empresas.ReaderOne,
	isender SenderNotConfigured,
	mustEnviar bool,
) (err error) {

	// Valido
	if len(data) == 0 {
		return errors.Errorf("no se ingresaron comprobantes")
	}
	if len(data) > 1 {
		return errors.Errorf("multi archivo no implementado")
	}
	type key struct {
		empresa  int
		comp     int
		persona  uuid.UUID
		sucursal int
	}
	keys := map[key][]CompData{}
	for _, v := range data {
		key := key{
			empresa:  v.Empresa,
			comp:     v.CompID,
			persona:  v.Persona,
			sucursal: v.Sucursal,
		}
		prev := keys[key]
		prev = append(prev, v)
		keys[key] = prev
	}

	// Si hay de varias empresas o tipos de comp debería analizar cada UnComprobante
	// agruparlos
	errChan := make(chan error)
	done := make(chan struct{})
	wg := sync.WaitGroup{}

	go func() {
		wg.Add(len(keys))
		for k, v := range keys {

			go func(
				ctx context.Context,
				data []CompData,
				empresa int,
				compID int,
				// persona uuid.UUID,
				// sucursal int,
				mustEnviar bool,
			) {
				senderTo := to.NewPersonaAdress(
					comitente,
					data[0].Persona,
					data[0].Sucursal,
					personaReader,
				)
				if len(data) == 1 {
					compNombre := fmt.Sprintf("%v %v", data[0].CompNombre, data[0].NCompStr)
					tpl := tplget.NewTemplateDBGet(
						comitente,
						empresa,
						compID,
						compReader,
						empresasReader,
					)
					emp, err := empresasReader.ReadOne(ctx, empresas.ReadOneReq{Comitente: comitente, ID: empresa})
					if err != nil {
						errChan <- errors.Wrap(err, "buscando empresa")
					}
					// Who is mail for?
					from := fmt.Sprintf("%v <%v>", emp.ImpresionNombreFantasia, emp.MailUserName)

					// Get body for mail
					bodier := body.NewUnComprobante(compNombre, data[0].PersonaNombre, tpl)

					// Get credentials for email server
					cr := cred.New(comitente, empresa, empresasReader)

					// Create configured mail sender
					sen := sender.New(cr, isender)

					// Send
					err = enviarComprobantesMismaEmpresa(
						ctx, data, senderTo, from, compNombre, bodier, sen, mustEnviar,
					)
					if err != nil {
						errChan <- err
						return
					}
					wg.Done()
				}
			}(ctx, v, k.empresa, k.comp, mustEnviar)

		}
		wg.Wait()
		done <- struct{}{}
	}()

	select {
	case err := <-errChan:
		return err
	case <-done:
		return nil
	case <-ctx.Done():
		return ctx.Err()
	}
}

// Todos los archivos van en un mismo mail.
func enviarComprobantesMismaEmpresa(
	ctx context.Context,
	data []CompData,
	senderTo SenderTo,
	from string,
	subject string,
	bodier Bodier,
	sender configuredSender,
	mustEnviar bool,
) (err error) {

	// Valido
	if len(data) == 0 {
		return errors.Errorf("no se ingresó data")
	}
	if niler.IsNil(senderTo) {
		return errors.Errorf("senderTo no ingresado")
	}
	if niler.IsNil(bodier) {
		return errors.Errorf("bodier no ingresado")
	}
	if niler.IsNil(sender) {
		return errors.Errorf("sender no ingresado")
	}
	if from == "" {
		return errors.Errorf("no se definió from")
	}
	if subject == "" {
		return errors.Errorf("no se definió asunto")
	}

	// Estamos ok
	req := emailsender.Req{
		From:    from,
		Subject: subject,
	}

	// To
	req.To, err = senderTo.Adresses(ctx)
	if err != nil {
		return errors.Wrap(err, "determinando dirección de mail a enviar")
	}
	if len(req.To) == 0 {
		if mustEnviar {
			return errors.Errorf("no se pudo encontrar ninguna dirección para enviar")
		} else {
			return nil
		}
	}

	// Body
	req.Body, err = bodier.Body(ctx)
	if err != nil {
		return errors.Wrap(err, "generando cuerpo del mensaje")
	}

	for i, v := range data {
		if niler.IsNil(v.Outputer) {
			return errors.Errorf("outputer vacío")
		}
		if v.CompNombre == "" {
			return errors.Errorf("adjunto %v no tenía nombre", i+1)
		}
		req.Attachments = append(req.Attachments, v.Outputer)
		fileName := fmt.Sprintf("%v.%v", v.CompNombre, v.Extension)
		req.AttachmentsNames = append(req.AttachmentsNames, fileName)
	}

	// Envío
	spew.Dump(req)
	err = sender.Send(ctx, req)
	if err != nil {
		log.Error().
			Interface("to", req.To).
			Str("from", req.From).
			Str("subject", req.Subject).
			Msg("enviando mail")
		return errors.Wrap(err, "calling emailsender")
	}
	log.Debug().
		Interface("to", req.To).
		Msg("mail enviado")
	return
}

// func PreviewComprobantes(
// 	ctx context.Context,
// 	comitente int,
// 	data []CompData,
// 	personaReader personas.ReaderOne,
// 	compReader comps.ReaderOne,
// 	empresasReader empresas.ReaderOne,
// 	isender SenderNotConfigured,
// 	mustEnviar bool,
// ) (out emailsender.Req, err error) {

// 	// Valido
// 	if len(data) == 0 {
// 		return out, errors.Errorf("no se ingresaron comprobantes")
// 	}
// 	if len(data) > 1 {
// 		return out, errors.Errorf("multi archivo no implementado")
// 	}
// 	type key struct {
// 		empresa  int
// 		comp     int
// 		persona  uuid.UUID
// 		sucursal int
// 	}
// 	keys := map[key][]CompData{}
// 	for _, v := range data {
// 		key := key{
// 			empresa:  v.Empresa,
// 			comp:     v.CompID,
// 			persona:  v.Persona,
// 			sucursal: v.Sucursal,
// 		}
// 		prev := keys[key]
// 		prev = append(prev, v)
// 		keys[key] = prev
// 	}

// 	// Si hay de varias empresas o tipos de comp debería analizar cada UnComprobante
// 	// agruparlos
// 	if len(keys) > 1 {
// 		return out, errors.Errorf("más de un mail")
// 	}

// 	for k := range keys {

// 		senderTo := to.NewPersonaAdress(
// 			comitente,
// 			data[0].Persona,
// 			data[0].Sucursal,
// 			personaReader,
// 		)
// 		if len(data) == 1 {
// 			compNombre := fmt.Sprintf("%v %v", data[0].CompNombre, data[0].NCompStr)
// 			tpl := tplget.NewTemplateDBGet(
// 				comitente,
// 				k.empresa,
// 				k.comp,
// 				compReader,
// 				empresasReader,
// 			)
// 			var emp empresas.Empresa
// 			emp, err = empresasReader.ReadOne(ctx, empresas.ReadOneReq{Comitente: comitente, ID: k.empresa})
// 			if err != nil {
// 				return out, errors.Wrap(err, "buscando empresa")
// 			}
// 			// Who is mail for?
// 			from := fmt.Sprintf("%v <%v>", emp.ImpresionNombreFantasia, emp.MailUserName)

// 			// Get body for mail
// 			bodier := body.NewUnComprobante(compNombre, data[0].PersonaNombre, tpl)

// 			// Get credentials for email server
// 			cr := cred.New(comitente, k.empresa, empresasReader)

// 			// Create configured mail sender
// 			sen := sender.New(cr, isender)

// 			// Send
// 			// output = append(output,
// 			out, err = previewComprobantesMismaEmpresa(
// 				ctx, data, senderTo, from, compNombre, bodier, sen, mustEnviar,
// 			)
// 			if err != nil {
// 				return
// 			}
// 		}
// 	}
// 	return

// }

// // Una vez que el usuario seleccionó dirección de destino y completó
// // subject y body, con este método lo envío
// func PreviewComprobantesEnviar(
// 	ctx context.Context,
// 	comitente int,
// 	data []CompData,
// 	to string,
// 	subject string,
// 	body string,
// 	compReader comps.ReaderOne,
// 	empresasReader empresas.ReaderOne,
// 	isender SenderNotConfigured,
// 	mustEnviar bool,
// ) (out emailsender.Req, err error) {

// 	// Valido
// 	if len(data) == 0 {
// 		return out, errors.Errorf("no se ingresaron comprobantes")
// 	}
// 	if len(data) > 1 {
// 		return out, errors.Errorf("multi archivo no implementado")
// 	}
// 	type key struct {
// 		empresa  int
// 		comp     int
// 		persona  uuid.UUID
// 		sucursal int
// 	}
// 	keys := map[key][]CompData{}
// 	for _, v := range data {
// 		key := key{
// 			empresa:  v.Empresa,
// 			comp:     v.CompID,
// 			persona:  v.Persona,
// 			sucursal: v.Sucursal,
// 		}
// 		prev := keys[key]
// 		prev = append(prev, v)
// 		keys[key] = prev
// 	}

// 	// Si hay de varias empresas o tipos de comp debería analizar cada UnComprobante
// 	// agruparlos
// 	if len(keys) > 1 {
// 		return out, errors.Errorf("más de un mail")
// 	}

// 	for k := range keys {

// 		if len(data) == 1 {
// 			compNombre := fmt.Sprintf("%v %v", data[0].CompNombre, data[0].NCompStr)
// 			var emp empresas.Empresa
// 			emp, err = empresasReader.ReadOne(ctx, empresas.ReadOneReq{Comitente: comitente, ID: k.empresa})
// 			if err != nil {
// 				return out, errors.Wrap(err, "buscando empresa")
// 			}
// 			// Who is mail for?
// 			from := fmt.Sprintf("%v <%v>", emp.ImpresionNombreFantasia, emp.MailUserName)

// 			// Get credentials for email server
// 			cr := cred.New(comitente, k.empresa, empresasReader)

// 			// Create configured mail sender
// 			sen := sender.New(cr, isender)

// 			// Send
// 			// output = append(output,
// 			out, err = previewComprobantesMismaEmpresa(
// 				ctx, data, to, from, compNombre, bodier, sen, mustEnviar,
// 			)
// 			if err != nil {
// 				return
// 			}
// 		}
// 	}
// 	return

// }
