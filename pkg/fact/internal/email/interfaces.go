package email

import (
	"context"
	"io"

	"bitbucket.org/marcos19/one/pkg/emailsender"
	"github.com/gofrs/uuid"
)

// Determina a qué direcciones de correo corresponde enviar el mail.
type SenderTo interface {
	Adresses(ctx context.Context) ([]string, error)
}

// Determina el body que corresponde pegarle al mail.
type Bodier interface {
	Body(ctx context.Context) (string, error)
}

// Determina el template que se va a usar para generar el body
type templateGetter interface {
	Get(ctx context.Context) (string, error)
}

// Un cliente de correo que todavía no tiene configuradas las credenciales
type SenderNotConfigured interface {
	Send(ctx context.Context, req emailsender.Req) error
	SetConfig(
		host string,
		port int,
		user string,
		pass string,
	)
}

type configuredSender interface {
	Send(ctx context.Context, req emailsender.Req) error
}
type Outputer interface {
	Output(w io.Writer) error
}
type CompData struct {
	ID            uuid.UUID
	Empresa       int
	Persona       uuid.UUID
	PersonaNombre string
	Sucursal      int
	CompNombre    string
	NCompStr      string
	FileName      string
	Outputer      Outputer
	CompID        int
	NComp         int
	PuntoVenta    int
	Extension     string
}

// Devuelve los datos de los comprobantes a imprimir.
// Ademas le pone un nombre al archivo.
type CompDataGetter interface {
	Get(ctx context.Context) ([]CompData, error)
}
