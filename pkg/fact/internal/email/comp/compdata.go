package comp

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/fact/internal/email"
	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type CompDataGet struct {
	conn      *pgxpool.Pool
	comitente int
	ids       []uuid.UUID
}

func NewCompDataGet(conn *pgxpool.Pool, comitente int, opIDs []uuid.UUID) *CompDataGet {
	return &CompDataGet{
		conn:      conn,
		comitente: comitente,
		ids:       opIDs,
	}

}

func (c *CompDataGet) Get(ctx context.Context) (out []email.CompData, err error) {
	if c.conn == nil {
		return nil, errors.Errorf("conn no ingresada")
	}
	if c.comitente == 0 {
		return nil, deferror.ComitenteIndefinido()
	}
	if len(c.ids) == 0 {
		return nil, errors.Errorf("no se ingresaron IDs")
	}

	// Busco por OP id
	query := fmt.Sprintf(`
			SELECT id, empresa, persona, persona_nombre, comp_nombre, n_comp_str, comp_id, punto_de_venta, n_comp
			FROM ops 
			WHERE 
				id IN (%v) AND 
				comitente = %v;`, arrayUUID(c.ids), c.comitente)
	rows, err := c.conn.Query(ctx, query)
	if err != nil {
		return nil, errors.Wrap(err, "buscando ids")
	}
	defer rows.Close()

	// Escaneo
	for rows.Next() {
		o := email.CompData{}
		err = rows.Scan(&o.ID, &o.Empresa, &o.Persona, &o.PersonaNombre, &o.CompNombre, &o.NCompStr, &o.CompID, &o.PuntoVenta, &o.NComp)
		if err != nil {
			return out, errors.Wrap(err, "escaneando ids")
		}
		out = append(out, o)
	}

	return
}
func arrayUUID(ids []uuid.UUID) string {
	idsStr := []string{}
	for _, v := range ids {
		idsStr = append(idsStr, fmt.Sprintf("'%v'", v.String()))
	}
	return strings.Join(idsStr, ", ")
}
