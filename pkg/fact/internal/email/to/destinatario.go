package to

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/personas"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/niler"
	"github.com/gofrs/uuid"
)

type PersonaAdress struct {
	r         personas.ReaderOne
	comitente int
	persona   uuid.UUID
	sucursal  int
}

func NewPersonaAdress(comitente int, persona uuid.UUID, sucursal int, r personas.ReaderOne) *PersonaAdress {

	return &PersonaAdress{
		r:         r,
		comitente: comitente,
		persona:   persona,
		sucursal:  sucursal,
	}
}

func (p *PersonaAdress) Adresses(ctx context.Context) (out []string, err error) {
	if niler.IsNil(p.r) {
		return nil, errors.Errorf("no se ingresó persona reader")
	}
	if p.comitente == 0 {
		return nil, deferror.ComitenteIndefinido()
	}
	if p.persona == uuid.Nil {
		return nil, errors.Errorf("persona indefinida")
	}

	// Busco persona
	per, err := p.r.ReadOne(ctx, personas.ReadOneReq{
		Comitente: p.comitente,
		ID:        p.persona,
	})
	if err != nil {
		return nil, errors.Wrap(err, "buscando persona")
	}

	if !per.UsaSucursales {
		for _, v := range per.Emails {
			if v.NoEnviarAutomaticamente {
				continue
			}
			out = append(out, v.Email)
		}
		return
	}

	// Si usa sucursales, tomo mail de la sucursal
	if p.sucursal != 0 {
		for _, v := range per.Sucursales {
			if *v.ID != p.sucursal {
				continue
			}
			for _, em := range v.Emails {
				if em.NoEnviarAutomaticamente {
					continue
				}
				out = append(out, em.Email)
			}
		}
		if len(out) > 0 {
			return
		}
	}

	// La persona usa sucursales, pero no se encontró ninguna sucursal con mail.
	// Eligo el mail de la persona
	for _, v := range per.Emails {
		if v.NoEnviarAutomaticamente {
			continue
		}
		out = append(out, v.Email)
	}
	return
}
