package to

import (
	"context"
	"testing"

	"bitbucket.org/marcos19/one/pkg/emails"
	"bitbucket.org/marcos19/one/pkg/personas"
	"bitbucket.org/marcos19/one/pkg/sucursales"
	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestPersonaAdress(t *testing.T) {

	t.Run("sin reader", func(t *testing.T) {
		s := PersonaAdress{
			r:         nil,
			comitente: 1,
			persona:   uuid.FromStringOrNil("b5ff6188-c747-491b-813f-80435edbecbe"),
			sucursal:  0,
		}
		_, err := s.Adresses(context.Background())
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "reader")
	})
	t.Run("sin comitente", func(t *testing.T) {
		s := PersonaAdress{
			r:         mockPers{},
			comitente: 0,
			persona:   uuid.FromStringOrNil("b5ff6188-c747-491b-813f-80435edbecbe"),
			sucursal:  0,
		}
		_, err := s.Adresses(context.Background())
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "comitente")
	})
	t.Run("sin persona", func(t *testing.T) {
		s := PersonaAdress{
			r:         mockPers{},
			comitente: 1,
			persona:   uuid.Nil,
			sucursal:  0,
		}
		_, err := s.Adresses(context.Background())
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "persona")
	})
	t.Run("no encuentra persona", func(t *testing.T) {
		s := PersonaAdress{
			r:         mockPersErr{},
			comitente: 1,
			persona:   uuid.FromStringOrNil("b5ff6188-c747-491b-813f-80435edbecbe"),
			sucursal:  0,
		}
		_, err := s.Adresses(context.Background())
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "testErr")
	})

	t.Run("persona sin mail", func(t *testing.T) {
		s := PersonaAdress{
			r:         mockPers{},
			comitente: 1,
			persona:   uuid.FromStringOrNil("b5ff6188-c747-491b-813f-80435edbecbe"),
			sucursal:  0,
		}
		emails, err := s.Adresses(context.Background())
		assert.Nil(t, err)
		assert.Len(t, emails, 0)
	})
	t.Run("persona con mail", func(t *testing.T) {
		s := PersonaAdress{
			r:         mockPersConMail{},
			comitente: 1,
			persona:   uuid.FromStringOrNil("b5ff6188-c747-491b-813f-80435edbecbe"),
			sucursal:  0,
		}
		emails, err := s.Adresses(context.Background())
		assert.Nil(t, err)
		require.Len(t, emails, 1)
		assert.True(t, len(emails[0]) > 0)
	})

	t.Run("persona con sucursales", func(t *testing.T) {
		s := PersonaAdress{
			r:         mockPersConSucursalMail{},
			comitente: 1,
			persona:   uuid.FromStringOrNil("b5ff6188-c747-491b-813f-80435edbecbe"),
			sucursal:  3,
		}
		emails, err := s.Adresses(context.Background())
		assert.Nil(t, err)
		require.Len(t, emails, 1)
		assert.Equal(t, "sucursal@test.com", emails[0])
	})

	t.Run("persona con sucursales (con mail)", func(t *testing.T) {
		// No encuentra mail sucursal, devuelve persona
		s := PersonaAdress{
			r:         mockPersConSucursalMail{},
			comitente: 1,
			persona:   uuid.FromStringOrNil("b5ff6188-c747-491b-813f-80435edbecbe"),
			sucursal:  2,
		}
		emails, err := s.Adresses(context.Background())
		assert.Nil(t, err)
		require.Len(t, emails, 1)
		assert.Equal(t, "persona@test.com", emails[0])
	})

	t.Run("persona con sucursales (sin mail)", func(t *testing.T) {
		// Encuentra sucursal pero no tiene mail, devuelve persona
		s := PersonaAdress{
			r:         mockPersConSucursalSinMail{},
			comitente: 1,
			persona:   uuid.FromStringOrNil("b5ff6188-c747-491b-813f-80435edbecbe"),
			sucursal:  3,
		}
		emails, err := s.Adresses(context.Background())
		assert.Nil(t, err)
		require.Len(t, emails, 1)
		assert.Equal(t, "persona@test.com", emails[0])
	})

	t.Run("persona con mail marcado como no enviar", func(t *testing.T) {
		// Encuentra sucursal con mail pero está marcado no enviar, devuelve persona
		s := PersonaAdress{
			r:         mockPersConMailNoEnviar{},
			comitente: 1,
			persona:   uuid.FromStringOrNil("b5ff6188-c747-491b-813f-80435edbecbe"),
			sucursal:  0,
		}
		emails, err := s.Adresses(context.Background())
		assert.Nil(t, err)
		assert.Len(t, emails, 0)
	})

	t.Run("persona con sucursales (sin mail)", func(t *testing.T) {
		// Encuentra sucursal con mail pero está marcado no enviar, devuelve persona
		s := PersonaAdress{
			r:         mockPersConSucursalMailNoEnviar{},
			comitente: 1,
			persona:   uuid.FromStringOrNil("b5ff6188-c747-491b-813f-80435edbecbe"),
			sucursal:  3,
		}
		emails, err := s.Adresses(context.Background())
		assert.Nil(t, err)
		require.Len(t, emails, 1)
		assert.Equal(t, "persona@test.com", emails[0])
	})
}

type mockPers struct{}

func (m mockPers) ReadOne(context.Context, personas.ReadOneReq) (personas.Persona, error) {
	return personas.Persona{}, nil
}

type mockPersConMail struct{}

func (m mockPersConMail) ReadOne(context.Context, personas.ReadOneReq) (personas.Persona, error) {
	return personas.Persona{Emails: []emails.Email{
		{Email: "test@test.com"},
	}}, nil
}

type mockPersConMailNoEnviar struct{}

func (m mockPersConMailNoEnviar) ReadOne(context.Context, personas.ReadOneReq) (personas.Persona, error) {
	return personas.Persona{Emails: []emails.Email{
		{
			Email:                   "test@test.com",
			NoEnviarAutomaticamente: true,
		},
	}}, nil
}

type mockPersConSucursalMail struct{}

func (m mockPersConSucursalMail) ReadOne(context.Context, personas.ReadOneReq) (personas.Persona, error) {
	id := 3
	return personas.Persona{
		UsaSucursales: true,
		Emails: []emails.Email{
			{
				Email: "persona@test.com",
			},
		},
		Sucursales: []sucursales.Sucursal{
			{
				ID: &id,
				Emails: []emails.Email{
					{
						Email: "sucursal@test.com",
					},
				},
			},
		},
	}, nil
}

type mockPersConSucursalMailNoEnviar struct{}

func (m mockPersConSucursalMailNoEnviar) ReadOne(context.Context, personas.ReadOneReq) (personas.Persona, error) {
	id := 3
	return personas.Persona{
		UsaSucursales: true,
		Emails: []emails.Email{
			{
				Email: "persona@test.com",
			},
		},
		Sucursales: []sucursales.Sucursal{
			{
				ID: &id,
				Emails: []emails.Email{
					{
						Email:                   "sucursal@test.com",
						NoEnviarAutomaticamente: true,
					},
				},
			},
		},
	}, nil
}

type mockPersConSucursalSinMail struct{}

func (m mockPersConSucursalSinMail) ReadOne(context.Context, personas.ReadOneReq) (personas.Persona, error) {
	id := 3
	return personas.Persona{
		UsaSucursales: true,
		Emails: []emails.Email{
			{
				Email: "persona@test.com",
			},
		},
		Sucursales: []sucursales.Sucursal{
			{
				ID: &id,
			},
		},
	}, nil
}

type mockPersErr struct{}

func (m mockPersErr) ReadOne(context.Context, personas.ReadOneReq) (personas.Persona, error) {
	return personas.Persona{}, errors.Errorf("testErr")
}
