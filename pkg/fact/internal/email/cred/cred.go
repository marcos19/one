package cred

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"github.com/pkg/errors"
)

type Credentials struct {
	r         empresas.ReaderOne
	comitente int
	empresa   int
}

func New(
	comitente int,
	empresa int,
	r empresas.ReaderOne,
) *Credentials {
	return &Credentials{
		r:         r,
		comitente: comitente,
		empresa:   empresa,
	}
}

func (c *Credentials) Get(ctx context.Context) (host string, port int, user, pass string, err error) {

	if c.empresa == 0 {
		err = deferror.EmpresaIndefinida()
		return
	}
	if c.comitente == 0 {
		err = deferror.ComitenteIndefinido()
		return
	}
	// Busco empresa
	emp, err := c.r.ReadOne(ctx, empresas.ReadOneReq{
		Comitente: c.comitente,
		ID:        c.empresa,
	})
	if err != nil {
		err = errors.Wrap(err, "buscando empresa")
		return
	}

	host = emp.MailHost
	port = emp.MailPort
	user = emp.MailUserName
	pass = emp.MailPassword

	return
}
