package cred

import (
	"context"
	"testing"

	"bitbucket.org/marcos19/one/pkg/empresas"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
)

func TestCred(t *testing.T) {

	{
		c := New(1, 1, &mockEmp{errors.Errorf("test err")})
		host, port, user, pass, err := c.Get(context.Background())
		assert.NotNil(t, err)
		assert.Equal(t, host, "")
		assert.Equal(t, port, 0)
		assert.Equal(t, user, "")
		assert.Equal(t, pass, "")
	}

	{
		c := New(1, 1, &mockEmp{})
		host, port, user, pass, err := c.Get(context.Background())
		assert.Nil(t, err)
		assert.Equal(t, host, "host")
		assert.Equal(t, port, 100)
		assert.Equal(t, user, "user")
		assert.Equal(t, pass, "pass")
	}
}

type mockEmp struct{ err error }

func (m *mockEmp) ReadOne(context.Context, empresas.ReadOneReq) (out empresas.Empresa, err error) {
	if m.err != nil {
		err = m.err
		return
	}
	out.MailHost = "host"
	out.MailPort = 100
	out.MailUserName = "user"
	out.MailPassword = "pass"
	return
}
