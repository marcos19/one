package sender

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/emailsender"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/niler"
)

// Devuelve las credenciales que tiene registrada la empresa
type Sender struct {
	credentials Credentialer
	sender      SenderNotConfigured
}

func New(
	credentials Credentialer,
	sender SenderNotConfigured,
) (out *Sender) {

	out = &Sender{
		credentials: credentials,
		sender:      sender,
	}
	return
}

func (s *Sender) Send(ctx context.Context, req emailsender.Req) (err error) {
	if niler.IsNil(s.credentials) {
		return errors.Errorf("no se ingresó CredentialGetter")
	}
	if niler.IsNil(s.sender) {
		return errors.Errorf("no se ingresó Sender")
	}
	host, port, user, pass, err := s.credentials.Get(ctx)
	if err != nil {
		return errors.Wrap(err, "buscando empresa")
	}
	s.sender.SetConfig(host, port, user, pass)
	return s.sender.Send(ctx, req)

}
