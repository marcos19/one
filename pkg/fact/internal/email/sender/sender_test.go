package sender

import (
	"context"
	"testing"

	"bitbucket.org/marcos19/one/pkg/emailsender"
	"github.com/cockroachdb/errors"
	"github.com/stretchr/testify/assert"
)

func TestSender(t *testing.T) {

	req := emailsender.Req{}
	{ // Ok
		cr := credMock{}
		se := sendMock{}
		c := New(cr, se)
		err := c.Send(context.Background(), req)
		assert.Nil(t, err)
	}
	{ // Error credenciales
		cr := credMock{err: errors.Errorf("error credenciales")}
		se := sendMock{}
		c := New(cr, se)
		err := c.Send(context.Background(), req)
		assert.NotNil(t, err)
	}
	{ // Error sender
		cr := credMock{}
		se := sendMock{err: errors.Errorf("error credenciales")}
		c := New(cr, se)
		err := c.Send(context.Background(), req)
		assert.NotNil(t, err)
	}
}

type credMock struct{ err error }

func (e credMock) Get(context.Context) (host string, port int, user, pass string, err error) {
	if e.err != nil {
		err = e.err
		return
	}
	return "host", 133, "user", "pass", nil
}

type sendMock struct{ err error }

func (e sendMock) Send(context.Context, emailsender.Req) error {
	return e.err
}
func (e sendMock) SetConfig(string, int, string, string) {

}
