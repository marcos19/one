package sender

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/emailsender"
)

// // Devuelve los datos para conectar al servidor de mail
// type CredentialGetter interface {
// 	Get(context.Context) (host string, port int, user, pass string, err error)
// }

type SenderNotConfigured interface {
	Send(
		ctx context.Context, req emailsender.Req) error
	SetConfig(
		host string,
		port int,
		user string,
		pass string,
	)
}

// Devuelve las credenciales para poder conectarse a un mail server.
type Credentialer interface {
	Get(context.Context) (host string, port int, user, pass string, err error)
}
