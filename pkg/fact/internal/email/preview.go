package email

import (
	"context"
	"fmt"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/emails"
	"bitbucket.org/marcos19/one/pkg/emailsender"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"bitbucket.org/marcos19/one/pkg/fact/internal/email/body"
	"bitbucket.org/marcos19/one/pkg/fact/internal/email/body/tplget"
	"bitbucket.org/marcos19/one/pkg/fact/internal/email/cred"
	"bitbucket.org/marcos19/one/pkg/fact/internal/email/sender"
	"bitbucket.org/marcos19/one/pkg/fact/internal/email/to"
	"bitbucket.org/marcos19/one/pkg/personas"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/niler"
	"github.com/gofrs/uuid"
)

type MailComprobantePreviewReq struct {
	Comitente  int
	UserID     string
	OpsID      []uuid.UUID
	PartidasID []uuid.UUID
}

type MailComprobantePreviewResponse struct {
	Comitente     int
	ClienteID     uuid.UUID
	ClienteNombre string
	Empresa       int `json:",string"`
	Body          string
	Comprobantes  []string
	Emails        emails.Emails
}

// Devuelve todos los datos para que el usuario pueda modificarlos
// y enviar el mail.
func PreviewComprobantes(
	ctx context.Context,
	comitente int,
	data []CompData,
	personaReader personas.ReaderOne,
	compReader comps.ReaderOne,
	empresasReader empresas.ReaderOne,
	isender SenderNotConfigured,
	mustEnviar bool,
) (out emailsender.Req, err error) {

	// Valido
	if len(data) == 0 {
		return out, errors.Errorf("no se ingresaron comprobantes")
	}
	if len(data) > 1 {
		return out, errors.Errorf("multi archivo no implementado")
	}
	type key struct {
		empresa  int
		comp     int
		persona  uuid.UUID
		sucursal int
	}
	keys := map[key][]CompData{}
	for _, v := range data {
		key := key{
			empresa:  v.Empresa,
			comp:     v.CompID,
			persona:  v.Persona,
			sucursal: v.Sucursal,
		}
		prev := keys[key]
		prev = append(prev, v)
		keys[key] = prev
	}

	// Si hay de varias empresas o tipos de comp debería analizar cada UnComprobante
	// agruparlos
	if len(keys) > 1 {
		return out, errors.Errorf("más de un mail")
	}

	for k := range keys {

		senderTo := to.NewPersonaAdress(
			comitente,
			data[0].Persona,
			data[0].Sucursal,
			personaReader,
		)
		if len(data) == 1 {
			compNombre := fmt.Sprintf("%v %v", data[0].CompNombre, data[0].NCompStr)
			tpl := tplget.NewTemplateDBGet(
				comitente,
				k.empresa,
				k.comp,
				compReader,
				empresasReader,
			)
			var emp empresas.Empresa
			emp, err = empresasReader.ReadOne(ctx, empresas.ReadOneReq{Comitente: comitente, ID: k.empresa})
			if err != nil {
				return out, errors.Wrap(err, "buscando empresa")
			}
			// Who is mail for?
			from := fmt.Sprintf("%v <%v>", emp.ImpresionNombreFantasia, emp.MailUserName)

			// Get body for mail
			bodier := body.NewUnComprobante(compNombre, data[0].PersonaNombre, tpl)

			// Get credentials for email server
			cr := cred.New(comitente, k.empresa, empresasReader)

			// Create configured mail sender
			sen := sender.New(cr, isender)

			// Send
			// output = append(output,
			out, err = previewComprobantesMismaEmpresa(
				ctx, data, senderTo, from, compNombre, bodier, sen, mustEnviar,
			)
			if err != nil {
				return
			}
		}
	}
	return
}

type EnviarReq struct {
	Comitente int
	IDs       []uuid.UUID
	To        []string
	Subject   string
	BodyStr   string

	Data []CompData
}

func (e *EnviarReq) Body(context.Context) (string, error) {
	return e.BodyStr, nil
}
func (e *EnviarReq) Adresses(context.Context) ([]string, error) {
	if len(e.To) == 0 {
		return nil, errors.Errorf("no se designaron destinatarios")
	}
	return e.To, nil
}

// Una vez que el usuario seleccionó dirección de destino y completó
// subject y body, con este método lo envío
func PreviewComprobantesConfirmar(
	ctx context.Context,
	req EnviarReq,
	compReader comps.ReaderOne,
	empresasReader empresas.ReaderOne,
	isender SenderNotConfigured,
) (err error) {

	// Valido
	if len(req.Data) == 0 {
		return errors.Errorf("no se ingresaron comprobantes")
	}
	if len(req.Data) > 1 {
		return errors.Errorf("multi archivo no implementado")
	}
	type key struct {
		empresa  int
		comp     int
		persona  uuid.UUID
		sucursal int
	}
	keys := map[key][]CompData{}
	for _, v := range req.Data {
		key := key{
			empresa:  v.Empresa,
			comp:     v.CompID,
			persona:  v.Persona,
			sucursal: v.Sucursal,
		}
		prev := keys[key]
		prev = append(prev, v)
		keys[key] = prev
	}

	// Si hay de varias empresas o tipos de comp debería analizar cada UnComprobante
	// agruparlos
	if len(keys) > 1 {
		return errors.Errorf("más de un mail")
	}

	for k := range keys {

		if len(req.Data) == 1 {
			// compNombre := fmt.Sprintf("%v %v", req.Data[0].CompNombre, req.Data[0].NCompStr)
			var emp empresas.Empresa
			emp, err = empresasReader.ReadOne(ctx, empresas.ReadOneReq{
				Comitente: req.Comitente,
				ID:        k.empresa,
			})
			if err != nil {
				return errors.Wrap(err, "buscando empresa")
			}

			// Who is mail from?
			from := fmt.Sprintf("%v <%v>", emp.ImpresionNombreFantasia, emp.MailUserName)

			// Get credentials for email server
			cr := cred.New(req.Comitente, k.empresa, empresasReader)

			// Create configured mail sender
			sen := sender.New(cr, isender)

			// Send
			// output = append(output,
			err = enviarComprobantesMismaEmpresa(
				ctx,
				req.Data,
				&req,
				from,
				req.Subject,
				&req,
				sen,
				true,
			)
			if err != nil {
				return
			}
		}
	}
	return
}

// Todos los archivos van en un mismo mail.
func previewComprobantesMismaEmpresa(
	ctx context.Context,
	data []CompData,
	senderTo SenderTo,
	from string,
	subject string,
	bodier Bodier,
	sender configuredSender,
	mustEnviar bool,
) (out emailsender.Req, err error) {

	// Valido
	if len(data) == 0 {
		return out, errors.Errorf("no se ingresó data")
	}
	if niler.IsNil(senderTo) {
		return out, errors.Errorf("senderTo no ingresado")
	}
	if niler.IsNil(bodier) {
		return out, errors.Errorf("bodier no ingresado")
	}
	if niler.IsNil(sender) {
		return out, errors.Errorf("sender no ingresado")
	}
	if from == "" {
		return out, errors.Errorf("no se definió from")
	}
	if subject == "" {
		return out, errors.Errorf("no se definió asunto")
	}

	// Estamos ok
	out = emailsender.Req{
		From:    from,
		Subject: subject,
	}

	// To
	out.To, err = senderTo.Adresses(ctx)
	if err != nil {
		return out, errors.Wrap(err, "determinando dirección de mail a enviar")
	}

	for i, v := range data {
		if niler.IsNil(v.Outputer) {
			return out, errors.Errorf("outputer vacío")
		}
		if v.CompNombre == "" {
			return out, errors.Errorf("adjunto %v no tenía nombre", i+1)
		}
		// out.Attachments = append(out.Attachments, v.Outputer)
		fileName := fmt.Sprintf("%v.%v", v.CompNombre, v.Extension)
		out.AttachmentsNames = append(out.AttachmentsNames, fileName)
	}
	return
}
