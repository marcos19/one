package body

import (
	"context"
	"testing"

	"github.com/cockroachdb/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestBody(t *testing.T) {

	// f, err := os.Open("./testdata/body.html")
	// require.NotNil(t, err)

	t.Run("sin datos", func(t *testing.T) { // Sin datos
		uc := UnComprobante{}
		_, err := uc.Body(context.Background())
		assert.NotNil(t, err)
	})

	t.Run("sin persona", func(t *testing.T) { // Sin persona
		uc := UnComprobante{
			comprobante:    "aa",
			personaNombre:  "",
			templateGetter: tplMock{},
		}
		_, err := uc.Body(context.Background())
		assert.NotNil(t, err)
		assert.Contains(t, err.Error(), "persona")
	})

	t.Run("sin comprobante", func(t *testing.T) { // Sin comprobante
		uc := UnComprobante{
			comprobante:    "",
			personaNombre:  "aa",
			templateGetter: tplMock{},
		}
		_, err := uc.Body(context.Background())
		assert.NotNil(t, err)
		assert.Contains(t, err.Error(), "comprobante")
	})

	t.Run("sin template", func(t *testing.T) { // Sin template
		uc := UnComprobante{
			comprobante:    "ss",
			personaNombre:  "aa",
			templateGetter: nil,
		}
		_, err := uc.Body(context.Background())
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "template")
	})

	t.Run("template getter da error", func(t *testing.T) {
		uc := UnComprobante{
			comprobante:    "ss",
			personaNombre:  "aa",
			templateGetter: tplMockErr{},
		}
		_, err := uc.Body(context.Background())
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "obteniendo")
	})

	t.Run("Template no compila", func(t *testing.T) {
		uc := UnComprobante{
			comprobante:    "ss",
			personaNombre:  "aa",
			templateGetter: tplMockErrParse{},
		}
		_, err := uc.Body(context.Background())
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "parse")
	})

	t.Run("Template no ejecuta", func(t *testing.T) {
		uc := UnComprobante{
			comprobante:    "ss",
			personaNombre:  "aa",
			templateGetter: tplMockErrExecute{},
		}
		_, err := uc.Body(context.Background())
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "ejecutando")
	})
}

type tplMock struct{}

func (t tplMock) Get(context.Context) (string, error) {
	return `{{.Comprobante}} de {{.PersonaNombre}}`, nil
}

// No consigue obtener template
type tplMockErr struct{}

func (t tplMockErr) Get(context.Context) (string, error) {
	return "", errors.Errorf("testError")
}

// No consigue parsear (error sintaxis)
type tplMockErrParse struct{}

func (t tplMockErrParse) Get(context.Context) (string, error) {
	return "no hay item {{.Falopa !+ tararira}}", nil
}

// No consigue ejecutar (variable no existe)
type tplMockErrExecute struct{}

func (t tplMockErrExecute) Get(context.Context) (string, error) {
	return "no hay item {{.Falopa }}", nil
}
