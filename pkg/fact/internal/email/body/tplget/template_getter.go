package tplget

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/niler"
)

type TemplateDBGet struct {
	comitente     int
	empresa       int
	compID        int
	compGetter    comps.ReaderOne
	empresaGetter empresas.ReaderOne
}

func NewTemplateDBGet(comitente, empresa, compID int,
	comp comps.ReaderOne, emp empresas.ReaderOne,
) *TemplateDBGet {
	return &TemplateDBGet{
		comitente:     comitente,
		empresa:       empresa,
		compID:        compID,
		compGetter:    comp,
		empresaGetter: emp,
	}
}

func (t *TemplateDBGet) Get(ctx context.Context) (string, error) {

	// Valido
	if t.comitente == 0 {
		return "", deferror.ComitenteIndefinido()
	}
	if t.empresa == 0 {
		return "", deferror.EmpresaIndefinida()
	}
	if t.compID == 0 {
		return "", errors.Errorf("no se ingresó compID")
	}
	if niler.IsNil(t.compGetter) {
		return "", errors.Errorf("compGetter era nil")
	}
	if niler.IsNil(t.empresaGetter) {
		return "", errors.Errorf("empresaGetter era nil")
	}

	{ // Tiene template el comp?
		comp, err := t.compGetter.ReadOne(ctx, comps.ReadOneReq{
			Comitente: t.comitente,
			ID:        t.compID,
		})
		if err != nil {
			return "", errors.Wrapf(err, "buscando comp %v", t.compID)
		}
		if comp.MailTemplate != "" {
			return comp.MailTemplate, nil
		}
	}

	{ // El comp no tiene. Tiene template la empresa?
		emp, err := t.empresaGetter.ReadOne(ctx, empresas.ReadOneReq{
			Comitente: t.comitente,
			ID:        t.empresa,
		})
		if err != nil {
			return "", errors.Wrapf(err, "buscando empresa %v", t.empresa)
		}
		if emp.MailTemplate != "" {
			return emp.MailTemplate, nil
		}
	}

	return "", errors.Errorf("no existe plantilla de mail para el comprobante, ni para la empresa")
}
