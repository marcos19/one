package tplget

import (
	"context"
	"testing"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"github.com/cockroachdb/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestTemplateGetter(t *testing.T) {
	t.Run("sin comp reader", func(t *testing.T) {
		tpl := NewTemplateDBGet(1, 1, 1, nil, mockEmpresa{})
		_, err := tpl.Get(context.Background())
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "comp")
	})

	t.Run("sin empresa reader", func(t *testing.T) {
		tpl := NewTemplateDBGet(1, 1, 1, mockComp{}, nil)
		_, err := tpl.Get(context.Background())
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "empresa")
	})
	t.Run("sin empresa", func(t *testing.T) {
		tpl := NewTemplateDBGet(1, 0, 1, mockComp{}, mockEmpresa{})
		_, err := tpl.Get(context.Background())
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "empresa")
	})
	t.Run("sin comitente", func(t *testing.T) {
		tpl := NewTemplateDBGet(0, 1, 1, mockComp{}, mockEmpresa{})
		_, err := tpl.Get(context.Background())
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "comitente")
	})
	t.Run("sin comp", func(t *testing.T) {
		tpl := NewTemplateDBGet(1, 1, 0, mockComp{}, mockEmpresa{})
		_, err := tpl.Get(context.Background())
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "comp")
	})

	t.Run("falla empresa reader", func(t *testing.T) {
		tpl := NewTemplateDBGet(1, 1, 1,
			mockComp{},
			mockEmpresa{err: errors.Errorf("test err empresa")})
		_, err := tpl.Get(context.Background())
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "test err empresa")
	})

	t.Run("falla comp reader", func(t *testing.T) {
		tpl := NewTemplateDBGet(1, 1, 1,
			mockComp{err: errors.Errorf("test err comp")},
			mockEmpresa{})
		_, err := tpl.Get(context.Background())
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "test err comp")
	})

	t.Run("devuelve comp template", func(t *testing.T) {
		tpl := NewTemplateDBGet(1, 1, 1,
			mockComp{tpl: "comp template"},
			mockEmpresa{tpl: "empresa template"})
		out, err := tpl.Get(context.Background())
		require.Nil(t, err)
		assert.Equal(t, "comp template", out)
	})
	t.Run("devuelve empresa template", func(t *testing.T) {
		tpl := NewTemplateDBGet(1, 1, 1,
			mockComp{},
			mockEmpresa{tpl: "empresa template"})
		out, err := tpl.Get(context.Background())
		require.Nil(t, err)
		assert.Equal(t, "empresa template", out)
	})
	t.Run("no hay template", func(t *testing.T) {
		tpl := NewTemplateDBGet(1, 1, 1,
			mockComp{},
			mockEmpresa{})
		_, err := tpl.Get(context.Background())
		require.NotNil(t, err)
		assert.Contains(t, err.Error(), "plantilla")
	})

}

type mockEmpresa struct {
	err error
	tpl string
}

func (m mockEmpresa) ReadOne(ctx context.Context, req empresas.ReadOneReq) (out empresas.Empresa, err error) {
	if m.err != nil {
		return out, m.err
	}

	return empresas.Empresa{MailTemplate: m.tpl}, nil
}

type mockComp struct {
	err error
	tpl string
}

func (m mockComp) ReadOne(ctx context.Context, req comps.ReadOneReq) (out comps.Comp, err error) {
	if m.err != nil {
		return out, m.err
	}

	return comps.Comp{MailTemplate: m.tpl}, nil
}
