// Implementación de Bodier, genera el body que le corresponde
// a un mail.
package body

import (
	"bytes"
	"context"
	"html/template"

	"github.com/cockroachdb/errors"
	"github.com/crosslogic/niler"
)

// Devuelve un html/template
type TemplateGetter interface {
	Get(ctx context.Context) (string, error)
}

type UnComprobante struct {
	comprobante    string
	personaNombre  string
	templateGetter TemplateGetter
}

func NewUnComprobante(comp, pers string, tpl TemplateGetter) *UnComprobante {
	return &UnComprobante{
		comprobante:    comp,
		personaNombre:  pers,
		templateGetter: tpl,
	}
}
func (b *UnComprobante) Body(ctx context.Context) (out string, err error) {

	if b.personaNombre == "" {
		return out, errors.Errorf("no se ingresó nombre persona")
	}
	if b.comprobante == "" {
		return out, errors.Errorf("no se ingresaron nombres de comprobantes")
	}
	if niler.IsNil(b.templateGetter) {
		return out, errors.Errorf("no se ingresó template getter")
	}

	// get template
	templateStr, err := b.templateGetter.Get(ctx)
	if err != nil {
		return out, errors.Wrap(err, "obteniendo template")
	}

	// Body
	tmpl, err := template.New("msg").Parse(templateStr)
	if err != nil {
		return out, errors.Wrap(err, "parseando HTML template")
	}

	data := struct {
		PersonaNombre string
		Comprobante   string
	}{
		PersonaNombre: b.personaNombre,
		Comprobante:   b.comprobante,
	}

	by := []byte{}
	buf := bytes.NewBuffer(by)

	// Ejecuto
	err = tmpl.Execute(buf, data)
	if err != nil {
		return out, errors.Wrap(err, "ejecutando HTML template")
	}
	out = buf.String()
	return
}

// func comprobanteBody(
// 	oo []compParaMail,
// 	user sesiones.UsuarioDTO,
// 	cliente personas.Persona,
// 	emp empresas.Empresa,
// ) (out string, err error) {

// 	// Body
// 	tmpl, err := template.New("msg").Parse(comprobanteHTML)
// 	if err != nil {
// 		return out, errors.Wrap(err, "parseando HTML template")
// 	}
// 	names := []string{}
// 	for _, v := range oo {
// 		names = append(names, fmt.Sprintf("%v %v", v.compNombre, v.nCompStr))
// 	}

// 	userPie := []string{}
// 	if user.PieMail != nil {
// 		userPie = strings.Split(*user.PieMail, "\n")
// 	}
// 	buf := new(bytes.Buffer)
// 	data := struct {
// 		ClienteNombre  string
// 		Intro          string
// 		Comprobante    string
// 		NombreFirmante string
// 		DatosFirmante  []string
// 		EmpresaNombre  string
// 		DatosEmpresa   []string
// 	}{
// 		ClienteNombre:  cliente.Nombre,
// 		NombreFirmante: fmt.Sprintf("%v %v", user.Nombre, user.Apellido),
// 		Comprobante:    names[0],
// 		DatosFirmante:  userPie,
// 		EmpresaNombre:  emp.Nombre,
// 		DatosEmpresa:   strings.Split(emp.PieMail, "\n"),
// 	}

// 	// Ejecuto
// 	err = tmpl.Execute(buf, data)
// 	if err != nil {
// 		return out, errors.Wrap(err, "ejecutando HTML template")
// 	}
// 	out = buf.String()
// 	return

// }

// const comprobanteHTML = `

//     <div style="font-family: Segoe UI, Tahoma, Geneva, Verdana, sans-serif">
//       <p>Hola {{ .ClienteNombre }}, te adjuntamos el comprobante <strong>{{ .Comprobante }} </strong></p>
// 	<p>Saludos</p>

//       <div >
//         <div style="font-weight: bolder">
//           {{ .NombreFirmante}}
//         </div>
//         {{ range .DatosFirmante }}<div style="font-size: 90%" > {{ . }}</div> {{ end }}
// 		<br/>
//         <div style="font-weight: bolder">
//           {{ .EmpresaNombre}}
//         </div>
//         {{ range .DatosEmpresa }} <div style="font-size: 90%"> {{ . }} </div> {{ end }}
//       </div>
//     </div>

// `
