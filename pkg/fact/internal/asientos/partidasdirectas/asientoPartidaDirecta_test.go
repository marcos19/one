package partidasdirectas

import (
	"context"
	"testing"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/fact/config"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/iva"
	"github.com/crosslogic/dec"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestAsientoPartidaDirecta(t *testing.T) {

	require := require.New(t)
	assert := assert.New(t)
	per := types.Persona{}
	ventasID := 9100
	ventasNombre := "Ventas"
	monto := dec.NewD4(10)
	clientes := 1200

	t.Run("Sin IVA", func(t *testing.T) {
		pp := []types.PartidaDirecta{
			{
				CuentaID:          ventasID,
				CuentaNombre:      ventasNombre,
				Monto:             monto,
				CuentaPatrimonial: clientes,
			},
		}
		cfg := config.Config{
			SignoRenglonProducto: 1,
		}
		cmp := comps.Comp{}
		oo, err := GenerarPartidas(context.Background(), 1, per, pp, cfg, cmp, ctaGetterMock{})
		require.Nil(err)
		require.Len(oo, 2)
		assert.Equal(ventasID, oo[0].Cuenta)
		assert.Equal(types.PataPartidaDirecta, oo[0].Pata)
		assert.Equal(dec.NewD2(monto.Float()), oo[0].Monto)
		assert.Equal(clientes, oo[1].Cuenta)
		assert.Equal(types.PataFacturaPatrimonial, oo[1].Pata)
		assert.Equal(dec.NewD2(-monto.Float()), oo[1].Monto)
	})

	t.Run("Con IVA", func(t *testing.T) {
		pp := []types.PartidaDirecta{
			{
				CuentaID:          ventasID,
				CuentaNombre:      ventasNombre,
				Monto:             monto,
				CuentaPatrimonial: clientes,
			},
		}
		cfg := config.Config{
			SignoRenglonProducto: 1,
			Imputaciones: []config.ImputacionContable{
				{
					IVA: 2200,
				},
			},
		}
		cmp := comps.Comp{CalculaIVA: true}
		_, err := GenerarPartidas(context.Background(), 1, per, pp, cfg, cmp, ctaGetterMock{})
		require.NotNil(err)
		pp[0].AlicuotaIVA = iva.IVA21
		pp[0].Concepto = iva.BaseImponible

		// Pongo alicuota
		oo, err := GenerarPartidas(context.Background(), 1, per, pp, cfg, cmp, ctaGetterMock{})
		require.Nil(err)

		require.Len(oo, 3)

		assert.Equal(ventasID, oo[0].Cuenta)
		assert.Equal(types.PataPartidaDirecta, oo[0].Pata)
		assert.Equal(dec.NewD2(monto.Float()), oo[0].Monto)

		assert.Equal(2200, oo[1].Cuenta)
		assert.Equal(types.PataIVA, oo[1].Pata)
		assert.Equal(dec.NewD2(monto.Float()*0.21), oo[1].Monto)

		assert.Equal(clientes, oo[2].Cuenta)
		assert.Equal(types.PataFacturaPatrimonial, oo[2].Pata)
		assert.Equal(dec.NewD2(-monto.Float()*1.21), oo[2].Monto)
	})

	t.Run("Con IVA (dos partidas distintas alicuotas)", func(t *testing.T) {
		pp := []types.PartidaDirecta{
			{
				CuentaID:          ventasID,
				CuentaNombre:      ventasNombre,
				Monto:             monto,
				CuentaPatrimonial: clientes,
			},
			{
				CuentaID:          ventasID,
				CuentaNombre:      ventasNombre,
				Monto:             monto * 2,
				CuentaPatrimonial: clientes,
			},
		}
		cfg := config.Config{
			SignoRenglonProducto: 1,
			Imputaciones: []config.ImputacionContable{
				{
					IVA: 2200,
				},
			},
		}
		cmp := comps.Comp{
			CalculaIVA:     true,
			VaAlLibroDeIVA: true,
		}
		_, err := GenerarPartidas(context.Background(), 1, per, pp, cfg, cmp, ctaGetterMock{})
		require.NotNil(err)
		pp[0].AlicuotaIVA = iva.IVA21
		pp[0].Concepto = iva.BaseImponible
		pp[1].AlicuotaIVA = iva.IVA10yMedio
		pp[1].Concepto = iva.BaseImponible

		// Pongo alicuota
		oo, err := GenerarPartidas(context.Background(), 1, per, pp, cfg, cmp, ctaGetterMock{})
		require.Nil(err)

		require.Len(oo, 6)

		assert.Equal(ventasID, oo[0].Cuenta)
		assert.Equal(types.PataPartidaDirecta, oo[0].Pata)
		assert.Equal(dec.NewD2(monto.Float()), oo[0].Monto)
		assert.Equal(iva.BaseImponible, *oo[0].ConceptoIVA)
		assert.Equal(iva.IVA21, *oo[0].AlicuotaIVA)

		assert.Equal(2200, oo[1].Cuenta)
		assert.Equal(types.PataIVA, oo[1].Pata)
		assert.Equal(dec.NewD2(monto.Float()*0.21), oo[1].Monto)
		assert.Equal(iva.IVA, *oo[1].ConceptoIVA)
		assert.Equal(iva.IVA21, *oo[1].AlicuotaIVA)

		assert.Equal(clientes, oo[2].Cuenta)
		assert.Equal(types.PataFacturaPatrimonial, oo[2].Pata)
		assert.Equal(dec.NewD2(-monto.Float()*1.21), oo[2].Monto)
		assert.Nil(oo[2].ConceptoIVA)
		assert.Nil(oo[2].AlicuotaIVA)

		assert.Equal(ventasID, oo[3].Cuenta)
		assert.Equal(types.PataPartidaDirecta, oo[3].Pata)
		assert.Equal(dec.NewD2(monto.Float()*2), oo[3].Monto)
		assert.Equal(iva.BaseImponible, *oo[3].ConceptoIVA)
		assert.Equal(iva.IVA10yMedio, *oo[3].AlicuotaIVA)

		assert.Equal(2200, oo[4].Cuenta)
		assert.Equal(types.PataIVA, oo[4].Pata)
		assert.Equal(dec.NewD2(monto.Float()*2*0.105), oo[4].Monto)
		assert.Equal(iva.IVA, *oo[4].ConceptoIVA)
		assert.Equal(iva.IVA10yMedio, *oo[4].AlicuotaIVA)

		assert.Equal(clientes, oo[5].Cuenta)
		assert.Equal(types.PataFacturaPatrimonial, oo[5].Pata)
		assert.Equal(dec.NewD2(-monto.Float()*2*1.105), oo[5].Monto)
		assert.Nil(oo[5].ConceptoIVA)
		assert.Nil(oo[5].AlicuotaIVA)
	})
}

type ctaGetterMock struct{}

func (c ctaGetterMock) PorFuncionContable(ctx context.Context, comitente int, f cuentas.Funcion) ([]cuentas.Cuenta, error) {
	return nil, nil
}
func (c ctaGetterMock) ReadOne(ctx context.Context, req cuentas.ReadOneReq) (cuentas.Cuenta, error) {
	switch req.ID {
	case 9100:
		return cuentas.Cuenta{ID: req.ID, Nombre: "Ventas"}, nil
	case 1200:
		return cuentas.Cuenta{ID: req.ID, Nombre: "Clientes"}, nil
	case 2200:
		return cuentas.Cuenta{ID: req.ID, Nombre: "IVA DF"}, nil
	}
	panic("not implemented")
}
