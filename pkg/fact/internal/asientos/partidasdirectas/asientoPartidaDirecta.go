package partidasdirectas

import (
	"context"
	"strings"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/fact/config"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/iva"
	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
)

// Devuelve la cuenta de IVA
type CuentaIVAGetter interface {
	CuentaIVA() (id int, err error)
}

// Genera las partidas contables desde las partidas DIRECTAS
func GenerarPartidas(
	ctx context.Context,
	comitente int,
	personaCabecera types.Persona,
	pp []types.PartidaDirecta,
	cfg config.Config,
	comp comps.Comp,
	cuentaGetter cuentas.ReaderOne,
) (out []ops.Partida, err error) {

	for i, v := range pp {

		if cfg.DetalleObligatorioEnPartidas {
			if strings.Trim(v.Detalle, " ") == "" {
				return out, deferror.Validationf("debe ingresar un detalle en partida %v", i+1)
			}
		}

		montoNeto := 0.

		{ // Partida de RENGLÓN
			part := ops.Partida{}
			part.ID, _ = uuid.NewV1()
			cta, err := cuentaGetter.ReadOne(ctx, cuentas.ReadOneReq{Comitente: comitente, ID: v.CuentaID})
			if err != nil {
				return out, errors.Wrapf(err, "en cuenta renglón para partida %v", i+1)
			}
			part.Cuenta = cta.ID
			part.CuentaNombre = cta.Nombre
			part.Pata = types.PataPartidaDirecta

			montoNeto = v.Monto.Float() * float64(cfg.SignoRenglonProducto)
			part.Monto = dec.NewD2(montoNeto)

			// IVA
			if comp.VaAlLibroDeIVA {
				if v.Concepto == 0 {
					return out, deferror.Validationf("no se ingresó concepto en partida número %v", i+1)
				}
				if v.AlicuotaIVA == 0 {
					return out, deferror.Validationf("no se ingresó alícuota en partida número %v", i+1)
				}

				part.ConceptoIVA = new(iva.Concepto)
				*part.ConceptoIVA = v.Concepto

				part.AlicuotaIVA = new(iva.Alicuota)
				*part.AlicuotaIVA = v.AlicuotaIVA
			}

			if v.Centro != 0 {
				part.Centro = new(int)
				*part.Centro = v.Centro
			}

			// Copia persona de cabecera?
			if cfg.CopiaPersonaDeCabecera && cta.AperturaPersona {
				part.Persona = new(uuid.UUID)
				*part.Persona = personaCabecera.ID
				if personaCabecera.Sucursal != nil {
					part.Sucursal = new(int)
					*part.Sucursal = *personaCabecera.Sucursal
				}
			} else {
				if v.Persona != nil {
					if *v.Persona != uuid.Nil {
						part.Persona = new(uuid.UUID)
						*part.Persona = *v.Persona
					}
				}
				if v.Sucursal != 0 {
					part.Sucursal = new(int)
					*part.Sucursal = v.Sucursal
				}
			}
			if v.Detalle != "" {
				part.Detalle = new(string)
				*part.Detalle = v.Detalle
			}
			// if cta.UsaAplicaciones {
			// 	part.PartidaAplicada = new(uuid.UUID)
			// 	*part.PartidaAplicada = part.ID
			// }

			out = append(out, part)
		}

		// Partida de IVA
		montoIVA := dec.D2(0)
		if comp.CalculaIVA {

			// Calculo monto de IVA
			alic, err := v.AlicuotaIVA.Porcentaje()
			if err != nil {
				return out, errors.Wrap(err, "calculando partida contable IVA")
			}
			montoIVA = dec.NewD2(montoNeto * alic)

			if montoIVA != 0 {
				pIVA := ops.Partida{}
				pIVA.Pata = types.PataIVA
				for _, imp := range cfg.Imputaciones {
					if imp.Categoria == 0 && imp.IVA != 0 {
						pIVA.Cuenta = imp.IVA
						break
					}
				}
				if pIVA.Cuenta == 0 {
					return out, errors.Errorf("determinando cuenta de IVA partida %v", i+1)
				}
				pIVA.Monto = montoIVA
				pIVA.ConceptoIVA = new(iva.Concepto)
				*pIVA.ConceptoIVA = iva.IVA
				pIVA.AlicuotaIVA = new(iva.Alicuota)
				*pIVA.AlicuotaIVA = v.AlicuotaIVA
				out = append(out, pIVA)
			}
		}

		{ // Partida PATRIMONIAL
			if v.CuentaPatrimonial == 0 {
				return out, errors.Errorf("no se determinó cuenta patrimonial para partida %v de %v", v.CuentaNombre, v.Monto.String())
			}
			ctaPat, err := cuentaGetter.ReadOne(ctx, cuentas.ReadOneReq{
				Comitente: comitente,
				ID:        v.CuentaPatrimonial,
			})
			if err != nil {
				return out, errors.Errorf("cuenta patrimonial %v no ingresada", v.CuentaPatrimonial)
			}

			if ctaPat.AperturaCentro {
				if v.CentroPatrimonial == 0 {
					return out, deferror.Validation("no se ingresó centro para la cuenta patrimonial")
				}
			}
			part := ops.Partida{}
			// part.ID, _ = uuid.NewV1()
			part.Monto = -dec.NewD2(montoNeto) - montoIVA
			part.Cuenta = v.CuentaPatrimonial
			if v.CentroPatrimonial != 0 {
				part.Centro = new(int)
				*part.Centro = v.CentroPatrimonial
			}
			part.Pata = types.PataFacturaPatrimonial
			part.Persona = new(uuid.UUID)
			*part.Persona = personaCabecera.ID
			if personaCabecera.Sucursal != nil {
				part.Sucursal = personaCabecera.Sucursal
			}
			out = append(out, part)
		}
	}

	return
}
