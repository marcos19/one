package asientos

import (
	"context"
	"fmt"
	"sort"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/iva"
	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/rs/zerolog/log"
)

// Input es una estructura que sirve para calcular los asientos de productos.
type Input struct {
	Producto          uuid.UUID
	SKU               uuid.UUID
	Centro            int
	Cantidad          float64
	Codigo            string
	UM                string
	Detalle           string
	PartidaAplicada   uuid.UUID
	CuentaAplicada    *int
	ConceptoProducto  *int
	Neto              float64
	Unicidades        []Unicidad
	AlicuotaIVA       iva.Alicuota
	PorCuentaTerceros bool
}

type Unicidad struct {
	ID              uuid.UUID
	PartidaAplicada uuid.UUID
	DeUso           bool
	Tipo            int
	Detalle         string
	Monto           dec.D2 // Tengo que usar una por renglón no puede ser más precisa
}

// Agrupa las partidas que se coinciden en todos los campos de agrupación.
// Si la cuenta llega con campo PartidaAplicada
func AgruparPartidas(ctx context.Context, comitente int, pp []ops.Partida, r cuentas.ReaderOne) (out []ops.Partida, err error) {

	type key struct {
		ID              uuid.UUID
		FechaFinanciera fecha.Fecha
		Cuenta          int
		Pata            string
		Persona         uuid.UUID
		Sucursal        int
		Producto        uuid.UUID
		SKU             uuid.UUID
		Unicidad        uuid.UUID
		UM              string
		UnicidadTipo    int
		Centro          int
		Contrato        int
		Deposito        int
		Caja            int
		ConceptoIVA     iva.Concepto
		AlicuotaIVA     iva.Alicuota
		PartidaAplicada uuid.UUID
	}
	type val struct {
		monto    dec.D2
		cantidad dec.D4
	}

	m := map[key]val{}

	for _, v := range pp {
		// if v.Pata != pata {
		// 	out = append(out, v)
		// 	continue
		// }

		// Era partida patrimonial
		k := key{
			Cuenta:          v.Cuenta,
			Pata:            v.Pata,
			FechaFinanciera: v.FechaFinanciera,
		}

		k.ID = v.ID

		// Persona
		if v.Persona != nil {
			k.Persona = *v.Persona
		}

		// Sucursal
		if v.Sucursal != nil {
			k.Sucursal = *v.Sucursal
		}

		// Centro
		if v.Centro != nil {
			k.Centro = *v.Centro
		}

		// Contrato
		if v.Contrato != nil {
			k.Contrato = *v.Contrato
		}

		// Deposito
		if v.Deposito != nil {
			k.Deposito = *v.Deposito
		}

		// Caja
		if v.Caja != nil {
			k.Caja = *v.Caja
		}

		// Producto
		if v.Producto != nil {
			k.Producto = *v.Producto
		}

		// SKU
		if v.SKU != nil {
			k.SKU = *v.SKU
		}

		// UM
		if v.UM != nil {
			k.UM = *v.UM
		}

		// Unicidad
		if v.Unicidad != nil {
			k.Unicidad = *v.Unicidad
		}

		// Unicidad tipo
		if v.UnicidadTipo != nil {
			k.UnicidadTipo = *v.UnicidadTipo
		}

		// Concepto IVA
		if v.ConceptoIVA != nil {
			k.ConceptoIVA = *v.ConceptoIVA
		}

		// Alícuota
		if v.AlicuotaIVA != nil {
			k.AlicuotaIVA = *v.AlicuotaIVA
		}

		// Partida aplicada
		if v.PartidaAplicada != nil {
			k.PartidaAplicada = *v.PartidaAplicada
		}

		anterior := m[k]
		anterior.monto += v.Monto
		if v.Cantidad != nil {
			anterior.cantidad += *v.Cantidad
		}
		m[k] = anterior
	}

	for k, v := range m {
		p := ops.Partida{}
		if k.ID == uuid.Nil {
			p.ID, err = uuid.NewV1()
			if err != nil {
				return out, err
			}
			cta, err := r.ReadOne(ctx, cuentas.ReadOneReq{
				Comitente: comitente,
				ID:        k.Cuenta},
			)
			if err != nil {
				return out, errors.Wrap(err, "buscando cuenta contable")
			}
			if cta.UsaAplicaciones {
				p.PartidaAplicada = new(uuid.UUID)
				*p.PartidaAplicada = p.ID
			}
		} else {
			// Si viene con un ID significa que ID y PartidaAplicada tienen
			// un significado, no puedo pisarlas.
			p.ID = k.ID
		}

		p.FechaFinanciera = k.FechaFinanciera
		p.Cuenta = k.Cuenta
		p.Pata = k.Pata

		// if pata == types.PataFacturaPatrimonial {
		// 	p.PartidaAplicada = new(uuid.UUID)
		// 	*p.PartidaAplicada = p.ID
		// }

		if k.Persona != uuid.Nil {
			p.Persona = new(uuid.UUID)
			*p.Persona = k.Persona
		}
		if k.Sucursal != 0 {
			p.Sucursal = new(int)
			*p.Sucursal = k.Sucursal
		}
		if k.Centro != 0 {
			p.Centro = new(int)
			*p.Centro = k.Centro
		}
		if k.Contrato != 0 {
			p.Contrato = new(int)
			*p.Contrato = k.Contrato
		}
		if k.Deposito != 0 {
			p.Deposito = new(int)
			*p.Deposito = k.Deposito
		}
		if k.Caja != 0 {
			p.Caja = new(int)
			*p.Caja = k.Caja
		}
		if k.Producto != uuid.Nil {
			p.Producto = new(uuid.UUID)
			*p.Producto = k.Producto
		}
		if k.SKU != uuid.Nil {
			p.SKU = new(uuid.UUID)
			*p.SKU = k.SKU
		}
		if k.UM != "" {
			p.UM = new(string)
			*p.UM = k.UM
		}
		if k.Unicidad != uuid.Nil {
			p.Unicidad = new(uuid.UUID)
			*p.Unicidad = k.Unicidad
		}
		if k.UnicidadTipo != 0 {
			p.UnicidadTipo = new(int)
			*p.UnicidadTipo = k.UnicidadTipo
		}
		if k.ConceptoIVA != 0 {
			p.ConceptoIVA = new(iva.Concepto)
			*p.ConceptoIVA = k.ConceptoIVA
		}
		if k.AlicuotaIVA != 0 {
			p.AlicuotaIVA = new(iva.Alicuota)
			*p.AlicuotaIVA = k.AlicuotaIVA
		}
		if k.PartidaAplicada != uuid.Nil {
			p.PartidaAplicada = new(uuid.UUID)
			*p.PartidaAplicada = k.PartidaAplicada
		}

		p.Monto = v.monto
		if v.cantidad != 0 {
			p.Cantidad = new(dec.D4)
			*p.Cantidad = v.cantidad
		}
		out = append(out, p)
	}

	sort.Slice(out, func(i, j int) bool {
		if out[i].FechaFinanciera != out[j].FechaFinanciera {
			return out[i].FechaFinanciera < out[j].FechaFinanciera
		}
		if out[i].Monto != out[j].Monto {
			return out[i].Monto > out[j].Monto
		}
		return out[i].Cuenta < out[j].Cuenta
	})
	return
}

// Llena el campo PartidaAplicada para aquellas partidas que sean patrimoniales
// y cuyas cuentas estén tildadas como UsaAplicaciones
func LlenarPartidasPatrimoniales(ctx context.Context, comitente int, pp []ops.Partida, cg cuentas.ReaderOne) (err error) {
	for i, v := range pp {
		log.Debug().Msgf("%v cuenta: %v, era pata: %v", i, v.Cuenta, v.Pata)
		if v.Pata != types.PataFacturaPatrimonial &&
			v.Pata != types.PataPendienteStock &&
			v.Pata != types.PataPartidaDirecta {
			continue
		}

		// Si ya viene con partida aplicada es porque estoy aplicando a un
		// producto (ej: haciendo rececpión de una orden de compra)
		if v.PartidaAplicada != nil {
			continue
		}
		cta, err := cg.ReadOne(ctx, cuentas.ReadOneReq{
			Comitente: comitente,
			ID:        v.Cuenta,
		})
		if err != nil {
			return errors.Wrapf(err, "buscando cuenta %v", v.Cuenta)
		}
		if !cta.UsaAplicaciones {
			continue
		}

		// Debería llegar con un ID
		if v.ID == uuid.Nil {
			return errors.Errorf("partida con cuenta %v sin ID", v.Cuenta)
		}

		pp[i].PartidaAplicada = new(uuid.UUID)
		*pp[i].PartidaAplicada = pp[i].ID
	}

	return
}

// Toma las partidas patrimoniales de la factura y las parte de acuerdo a los
// vencimientos.
//
// Si existe más de un vencimiento, esta función hace una novación
// de partidas patrimoniales y salen con IDs distintos a los que entraron.
func PartirPartidasPatrimonialesPorVencimiento(pp []ops.Partida, vv []types.Vencimiento) (out []ops.Partida, err error) {

	if len(vv) == 0 {
		return pp, nil
	}

	pats := []ops.Partida{}

	sum := dec.D2(0)
	for _, v := range pp {
		if v.Pata == types.PataFacturaPatrimonial {
			pats = append(pats, v)
			sum += v.Monto
		} else {
			out = append(out, v)
		}
	}

	// Si hay un solo vencimiento no cambio IDs, solo fecha
	if len(vv) == 1 {
		if vv[0].Fecha == 0 {
			return out, fmt.Errorf("vencimiento no tenía fecha definida")
		}
		for _, v := range pats {
			v.FechaFinanciera = vv[0].Fecha
			out = append(out, v)
		}
		return
	}

	for _, pat := range pats {

		// no debería pasar nunca que entre con partida aplicada porque con
		// este proceso cambio los ID
		//if pat.PartidaAplicada != nil {
		//return out, errors.Errorf("partida patrimonial entró con ID de partida aplicada")
		//}
		acum := dec.D2(0)
		for i, venc := range vv {
			nueva := pat

			nueva.ID, err = uuid.NewV1()
			if err != nil {
				return out, err
			}
			nueva.PartidaAplicada = new(uuid.UUID)
			*nueva.PartidaAplicada = nueva.ID

			// Monto
			porc := venc.Monto.Float() / sum.Float()
			nueva.Monto = dec.NewD2(pat.Monto.Float() * porc)

			// Si hay diferencia de centavos por partir cuotas los agrego a la última.
			esUltimaCuota := len(vv) == i+1
			if esUltimaCuota {
				nueva.Monto = pat.Monto - acum
			}

			nueva.FechaFinanciera = venc.Fecha
			if len(vv) > 1 && len(pats) == 1 {
				nueva.Detalle = new(string)
				*nueva.Detalle = fmt.Sprintf("Cuota %v de %v", i+1, len(vv))
			}
			acum += nueva.Monto
			out = append(out, nueva)
		}
	}

	return
}

// Pega los campos de concepto de IVA y de AlicuotaIVA en las partidas contables
func LlenarCamposIVA(part *ops.Partida, alic iva.Alicuota, vaAlLibroIVA bool, empresaInscIVA bool) error {

	if !vaAlLibroIVA {
		return nil
	}

	switch part.Pata {

	case types.PataIVA:
		return llenarCamposIVAPataIVA(part, alic, vaAlLibroIVA)

	case types.PataProducto, types.PataPartidaDirecta:
		return llenarCamposIVAPataProducto(part, alic, empresaInscIVA)
	}

	return nil
}

func llenarCamposIVAPataIVA(p *ops.Partida, alic iva.Alicuota, empresaInscIVA bool) error {

	if alic == 0 {
		return errors.Errorf("partida pata IVA sin alicuota")
	}
	switch {
	case empresaInscIVA:

		// Este caso se daría para una factura en negro.
		// Quiero que calcule el IVA para que quede igual que el precio
		// en blanco, pero no es fiscal.
		p.ConceptoIVA = new(iva.Concepto)
		*p.ConceptoIVA = iva.IVA
		p.AlicuotaIVA = new(iva.Alicuota)
		*p.AlicuotaIVA = alic

	case !empresaInscIVA:
		// El comprobante esta tildado que va al libro de IVA
		// pero no está inscripta en IVA. No debería pasar
		return errors.Errorf("no debería haber partida de IVA en empresa no inscripta en IVA")
	}

	return nil
}

func llenarCamposIVAPataProducto(part *ops.Partida, alic iva.Alicuota, empresaInscIVA bool) error {

	switch {

	case !empresaInscIVA:
		// Si es una empresa monotributista o exenta pongo todo como no gravado.
		part.ConceptoIVA = new(iva.Concepto)
		*part.ConceptoIVA = iva.NoGravado

		part.AlicuotaIVA = new(iva.Alicuota)
		*part.AlicuotaIVA = iva.IVANoGravado

	case empresaInscIVA:

		if alic == 0 {
			return errors.Errorf("no se ingresó alícuota IVA")
		}

		part.AlicuotaIVA = new(iva.Alicuota)
		*part.AlicuotaIVA = alic

		part.ConceptoIVA = new(iva.Concepto)
		switch alic {

		case iva.IVAExento:
			*part.ConceptoIVA = iva.Exento

		case iva.IVANoGravado:
			*part.ConceptoIVA = iva.NoGravado

		default:
			*part.ConceptoIVA = iva.BaseImponible
		}
	}

	return nil
}
