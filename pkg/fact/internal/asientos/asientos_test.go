package asientos

import (
	"context"
	"testing"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/iva"
	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestAgruparPartidasPatrimoniales(t *testing.T) {
	ctx := context.Background()
	c := 1
	r := &mockCuentas{}

	{ // Una persona, dos partidas
		persona := uuid.FromStringOrNil("a18edb4d-8948-4d67-90b6-ed977be2debd")
		p1 := ops.Partida{
			Pata:    types.PataFacturaPatrimonial,
			Monto:   1000,
			Persona: &persona,
		}
		p2 := ops.Partida{
			Pata:    types.PataFacturaPatrimonial,
			Monto:   1000,
			Persona: &persona,
		}

		out, err := AgruparPartidas(ctx, c, []ops.Partida{p1, p2}, r)
		require.Nil(t, err)
		require.Len(t, out, 1)
		assert.Equal(t, dec.D2(2000), out[0].Monto)
	}

	{ // Una persona (dos sucursales), dos partidas
		persona := uuid.FromStringOrNil("a18edb4d-8948-4d67-90b6-ed977be2debd")
		sucursal1 := 1
		sucursal2 := 2
		p1 := ops.Partida{
			Pata:     types.PataFacturaPatrimonial,
			Monto:    1000,
			Persona:  &persona,
			Sucursal: &sucursal1,
		}
		p2 := ops.Partida{
			Pata:     types.PataFacturaPatrimonial,
			Monto:    1000,
			Persona:  &persona,
			Sucursal: &sucursal2,
		}

		out, err := AgruparPartidas(context.Background(), 1, []ops.Partida{p1, p2}, &mockCuentas{})
		require.Nil(t, err)
		require.Len(t, out, 2)
		assert.Equal(t, dec.D2(1000), out[0].Monto)
		assert.Equal(t, dec.D2(1000), out[1].Monto)
	}

	{ // Una persona, dos fechas de vencimiento
		persona := uuid.FromStringOrNil("a18edb4d-8948-4d67-90b6-ed977be2debd")
		p1 := ops.Partida{
			FechaFinanciera: 20220201,
			Pata:            types.PataFacturaPatrimonial,
			Monto:           1000,
			Persona:         &persona,
		}
		p2 := ops.Partida{
			FechaFinanciera: 20220101,
			Pata:            types.PataFacturaPatrimonial,
			Monto:           1000,
			Persona:         &persona,
		}

		out, err := AgruparPartidas(ctx, c, []ops.Partida{p1, p2}, r)
		require.Nil(t, err)
		require.Len(t, out, 2)
		assert.Equal(t, dec.D2(1000), out[0].Monto)
		assert.Equal(t, fecha.Fecha(20220101), out[0].FechaFinanciera)

		assert.Equal(t, dec.D2(1000), out[1].Monto)
		assert.Equal(t, fecha.Fecha(20220201), out[1].FechaFinanciera)
	}
	t.Run("con IDs", func(t *testing.T) {
		// No las debe agrupar si ya vienen con ID
		persona1 := uuid.FromStringOrNil("b20103d6-cbcb-43cd-a81f-b8ffdca44840")
		persona2 := uuid.FromStringOrNil("b20103d6-cbcb-43cd-a81f-b8ffdca44840")
		p1 := ops.Partida{
			ID:      uuid.FromStringOrNil("ded59da3-6e12-11ed-bb5f-ceb04c933589"),
			Pata:    types.PataFacturaPatrimonial,
			Persona: &persona1,
			Monto:   dec.NewD2(121),
		}
		p2 := ops.Partida{
			ID:      uuid.FromStringOrNil("ded59daa-6e12-11ed-bb5f-ceb04c933589"),
			Pata:    types.PataFacturaPatrimonial,
			Persona: &persona2,
			Monto:   dec.NewD2(-21),
		}
		out, err := AgruparPartidas(ctx, c, []ops.Partida{p1, p2}, r)
		require.Nil(t, err)
		require.Len(t, out, 2)
	})
}

func TestAgruparPartidasDirectas(t *testing.T) {

	ctx := context.Background()
	c := 1
	r := &mockCuentas{}
	ventas := 9200

	t.Run("Distintos centros", func(t *testing.T) {
		mayorista := 1
		minorista := 2
		p1 := ops.Partida{
			Pata:   types.PataPartidaDirecta,
			Cuenta: ventas,
			Centro: &mayorista,
			Monto:  1000,
		}
		p2 := ops.Partida{
			Pata:   types.PataPartidaDirecta,
			Cuenta: ventas,
			Centro: &minorista,
			Monto:  300,
		}

		out, err := AgruparPartidas(ctx, c, []ops.Partida{p1, p2}, r)
		require.Nil(t, err)
		require.Len(t, out, 2)
		c := 0
		for _, v := range out {
			if v.Monto == 1000 {
				c++
				assert.Equal(t, ventas, v.Cuenta)
				assert.Equal(t, mayorista, *v.Centro)
				assert.Equal(t, types.PataPartidaDirecta, v.Pata)
			}
			if v.Monto == 300 {
				c++
				assert.Equal(t, ventas, v.Cuenta)
				assert.Equal(t, minorista, *v.Centro)
				assert.Equal(t, types.PataPartidaDirecta, v.Pata)
			}
		}
		assert.Equal(t, 2, c)
	})

	t.Run("Mismas alicuotas IVA", func(t *testing.T) {
		conc := iva.BaseImponible
		al10 := iva.IVA10yMedio
		// al21 := iva.IVA21
		p1 := ops.Partida{
			Pata:        types.PataPartidaDirecta,
			Cuenta:      ventas,
			ConceptoIVA: &conc,
			AlicuotaIVA: &al10,
			Monto:       1000,
		}
		p2 := ops.Partida{
			Pata:        types.PataPartidaDirecta,
			Cuenta:      ventas,
			ConceptoIVA: &conc,
			AlicuotaIVA: &al10,
			Monto:       300,
		}

		out, err := AgruparPartidas(ctx, c, []ops.Partida{p1, p2}, r)
		require.Nil(t, err)
		require.Len(t, out, 1)
		assert.Equal(t, dec.D2(1300), out[0].Monto)
		assert.Equal(t, conc, *out[0].ConceptoIVA)
		assert.Equal(t, al10, *out[0].AlicuotaIVA)
	})

	t.Run("Distintas alicuotas IVA (base imponible)", func(t *testing.T) {
		conc := iva.BaseImponible
		al10 := iva.IVA10yMedio
		al21 := iva.IVA21
		p1 := ops.Partida{
			Pata:        types.PataPartidaDirecta,
			Cuenta:      ventas,
			ConceptoIVA: &conc,
			AlicuotaIVA: &al10,
			Monto:       1000,
		}
		p2 := ops.Partida{
			Pata:        types.PataPartidaDirecta,
			Cuenta:      ventas,
			ConceptoIVA: &conc,
			AlicuotaIVA: &al21,
			Monto:       300,
		}

		out, err := AgruparPartidas(ctx, c, []ops.Partida{p1, p2}, r)
		require.Nil(t, err)
		require.Len(t, out, 2)
		c := 0
		for i, v := range out {
			if v.Monto == 1000 {
				assert.Equal(t, conc, *out[i].ConceptoIVA)
				assert.Equal(t, al10, *out[i].AlicuotaIVA)
				c++
			}
			if v.Monto == 300 {
				assert.Equal(t, conc, *out[i].ConceptoIVA)
				assert.Equal(t, al21, *out[i].AlicuotaIVA)
				c++
			}
		}
		assert.Equal(t, c, 2)
	})
	t.Run("Distintas alicuotas IVA (iva)", func(t *testing.T) {
		conc := iva.IVA
		al10 := iva.IVA10yMedio
		al21 := iva.IVA21
		p1 := ops.Partida{
			Pata:        types.PataIVA,
			Cuenta:      ventas,
			ConceptoIVA: &conc,
			AlicuotaIVA: &al10,
			Monto:       1000,
		}
		p2 := ops.Partida{
			Pata:        types.PataIVA,
			Cuenta:      ventas,
			ConceptoIVA: &conc,
			AlicuotaIVA: &al21,
			Monto:       300,
		}

		out, err := AgruparPartidas(ctx, c, []ops.Partida{p1, p2}, r)
		require.Nil(t, err)
		require.Len(t, out, 2)
		c := 0
		for i, v := range out {
			if v.Monto == 1000 {
				assert.Equal(t, conc, *out[i].ConceptoIVA)
				assert.Equal(t, al10, *out[i].AlicuotaIVA)
				c++
			}
			if v.Monto == 300 {
				assert.Equal(t, conc, *out[i].ConceptoIVA)
				assert.Equal(t, al21, *out[i].AlicuotaIVA)
				c++
			}
		}
		assert.Equal(t, c, 2)
	})

}

func TestPartirPartidasPatrimonialesPorVencimiento(t *testing.T) {
	assert := assert.New(t)

	unaPartida := []ops.Partida{
		{
			ID:     uuid.FromStringOrNil("f81aa3fe-a212-42d2-9fb9-9947ef51c5e1"),
			Pata:   types.PataFacturaPatrimonial,
			Cuenta: 342,
			Monto:  dec.NewD2(100),
		},
	}

	dosPartidas := []ops.Partida{
		{
			ID:     uuid.FromStringOrNil("f81aa3fe-a212-42d2-9fb9-9947ef51c5e1"),
			Pata:   types.PataFacturaPatrimonial,
			Cuenta: 342,
			Monto:  dec.NewD2(80),
		},
		{
			ID:     uuid.FromStringOrNil("9c6e99a3-25c6-4fc1-9bc9-440a57714cdf"),
			Pata:   types.PataFacturaPatrimonial,
			Cuenta: 9098,
			Monto:  dec.NewD2(20),
		},
	}

	unVencimiento := []types.Vencimiento{
		{
			Fecha: fecha.Fecha(20210101),
		},
	}
	dosVencimientos := []types.Vencimiento{
		{
			Fecha: fecha.Fecha(20210101),
			Monto: dec.NewD2(50),
		},
		{
			Fecha: fecha.Fecha(20210201),
			Monto: dec.NewD2(50),
		},
	}
	tresVencimientos := []types.Vencimiento{
		{
			Fecha: fecha.Fecha(20210101),
			Monto: dec.NewD2(33.33),
		},
		{
			Fecha: fecha.Fecha(20210201),
			Monto: dec.NewD2(33.33),
		},
		{
			Fecha: fecha.Fecha(20210301),
			Monto: dec.NewD2(33.34),
		},
	}

	{ // Un vencimiento, una partida
		out, err := PartirPartidasPatrimonialesPorVencimiento(unaPartida, unVencimiento)
		require.Nil(t, err)
		assert.Len(out, 1)
		if len(out) == 1 {
			assert.Equal(out[0].Monto, dec.NewD2(100))
			assert.Equal(out[0].ID, unaPartida[0].ID)
		}
	}

	{ // Dos vencimiento, una partida
		out, err := PartirPartidasPatrimonialesPorVencimiento(unaPartida, dosVencimientos)
		require.Nil(t, err)
		assert.Len(out, 2)
		if len(out) == 2 {
			// Cuota 1
			assert.Equal(out[0].Monto, dec.NewD2(50))
			assert.Equal(out[0].FechaFinanciera, fecha.Fecha(20210101))
			assert.NotNil(out[0].Detalle)
			if out[0].Detalle != nil {
				assert.Equal(*out[0].Detalle, "Cuota 1 de 2")
			}
			assert.NotEqual(out[0].ID, unaPartida[0].ID)

			// Cuota 2
			assert.Equal(out[1].Monto, dec.NewD2(50))
			assert.Equal(out[1].FechaFinanciera, fecha.Fecha(20210201))
			assert.NotNil(out[1].Detalle)
			if out[0].Detalle != nil {
				assert.Equal(*out[1].Detalle, "Cuota 2 de 2")
			}
			assert.NotEqual(out[1].ID, unaPartida[0].ID)

			// IDs diferentes
			assert.NotEqual(out[0].ID, out[1].ID)

			assert.Equal(out[0].ID, *out[0].PartidaAplicada)
			assert.Equal(out[1].ID, *out[1].PartidaAplicada)
		}
	}
	{ // Tres vencimiento, una partida
		out, err := PartirPartidasPatrimonialesPorVencimiento(unaPartida, tresVencimientos)
		require.Nil(t, err)
		assert.Len(out, 3)
		if len(out) == 3 {
			// Cuota 1
			assert.Equal(out[0].Monto, dec.NewD2(33.33))
			assert.Equal(out[0].FechaFinanciera, fecha.Fecha(20210101))
			assert.NotNil(out[0].Detalle)
			if out[0].Detalle != nil {
				assert.Equal(*out[0].Detalle, "Cuota 1 de 3")
			}
			assert.NotEqual(out[0].ID, unaPartida[0].ID)

			// Cuota 2
			assert.Equal(out[1].Monto, dec.NewD2(33.33))
			assert.Equal(out[1].FechaFinanciera, fecha.Fecha(20210201))
			assert.NotNil(out[1].Detalle)
			if out[1].Detalle != nil {
				assert.Equal(*out[1].Detalle, "Cuota 2 de 3")
			}
			assert.NotEqual(out[1].ID, unaPartida[0].ID)

			// Cuota 3
			assert.Equal(out[2].Monto, dec.NewD2(33.34))
			assert.Equal(out[2].FechaFinanciera, fecha.Fecha(20210301))
			assert.NotNil(out[2].Detalle)
			if out[2].Detalle != nil {
				assert.Equal(*out[2].Detalle, "Cuota 3 de 3")
			}
			assert.NotEqual(out[2].ID, unaPartida[0].ID)

			assert.Equal(out[0].ID, *out[0].PartidaAplicada)
			assert.Equal(out[1].ID, *out[1].PartidaAplicada)
			assert.Equal(out[2].ID, *out[2].PartidaAplicada)
		}
	}

	{ // Un vencimiento, DOS partidas
		out, err := PartirPartidasPatrimonialesPorVencimiento(dosPartidas, unVencimiento)
		require.Nil(t, err)
		assert.Len(out, 2)
		if len(out) == 2 {

			// Cuota 1
			assert.Equal(out[0].Monto, dec.NewD2(80))
			assert.Equal(out[0].FechaFinanciera, fecha.Fecha(20210101))
			assert.Nil(out[0].Detalle)
			assert.Equal(out[0].ID, dosPartidas[0].ID)
			assert.Equal(out[1].ID, dosPartidas[1].ID)

			// Cuota 2
			assert.Equal(out[1].Monto, dec.NewD2(20))
			assert.Equal(out[1].FechaFinanciera, fecha.Fecha(20210101))
			assert.Nil(out[1].Detalle)
			assert.Equal(out[1].ID, dosPartidas[1].ID)
			assert.Equal(out[1].ID, dosPartidas[1].ID)

		}
	}

	{ // DOS vencimientos, DOS partidas
		out, err := PartirPartidasPatrimonialesPorVencimiento(dosPartidas, dosVencimientos)
		require.Nil(t, err)
		assert.Len(out, 4)
		if len(out) == 4 {

			// Cuota 1
			assert.Equal(out[0].Monto, dec.NewD2(40))
			assert.Equal(out[0].FechaFinanciera, fecha.Fecha(20210101))
			assert.Nil(out[0].Detalle)
			assert.NotEqual(out[0].ID, dosPartidas[0].ID)
			assert.NotEqual(out[0].ID, dosPartidas[1].ID)

			// Cuota 2
			assert.Equal(out[1].Monto, dec.NewD2(40))
			assert.Equal(out[1].FechaFinanciera, fecha.Fecha(20210201))
			assert.Nil(out[1].Detalle)
			assert.NotEqual(out[1].ID, dosPartidas[0].ID)
			assert.NotEqual(out[1].ID, dosPartidas[1].ID)

			// Cuota 2
			assert.Equal(out[2].Monto, dec.NewD2(10))
			assert.Equal(out[2].FechaFinanciera, fecha.Fecha(20210101))
			assert.Nil(out[2].Detalle)
			assert.NotEqual(out[2].ID, dosPartidas[0].ID)
			assert.NotEqual(out[2].ID, dosPartidas[1].ID)

			// Cuota 2
			assert.Equal(out[3].Monto, dec.NewD2(10))
			assert.Equal(out[3].FechaFinanciera, fecha.Fecha(20210201))
			assert.Nil(out[3].Detalle)
			assert.NotEqual(out[3].ID, dosPartidas[0].ID)
			assert.NotEqual(out[3].ID, dosPartidas[1].ID)

			assert.Equal(out[0].ID, *out[0].PartidaAplicada)
			assert.Equal(out[1].ID, *out[1].PartidaAplicada)
			assert.Equal(out[2].ID, *out[2].PartidaAplicada)
			assert.Equal(out[3].ID, out[3].PartidaAplicada)
		}
	}
}

func TestLlenarCamposIVA(t *testing.T) {
	type item struct {
		name             string
		empresaInscIVA   bool
		vaAlLibroIVA     bool
		p                ops.Partida
		alic             iva.Alicuota
		expectedConcepto *iva.Concepto
		expectedAlicuota *iva.Alicuota
		expectedError    bool
	}
	bi := iva.BaseImponible
	ng := iva.NoGravado
	iv := iva.IVA
	ex := iva.Exento
	iva21 := iva.IVA21
	ivaNg := iva.IVANoGravado
	ivaExento := iva.IVAExento

	table := []item{
		{
			name:           "Factura A (pataProducto)",
			empresaInscIVA: true,
			vaAlLibroIVA:   true,
			p: ops.Partida{
				Pata: types.PataProducto,
			},
			alic:             iva.IVA21,
			expectedConcepto: &bi,
			expectedAlicuota: &iva21,
			expectedError:    false,
		},
		{
			name:           "Factura A (pataIVA)",
			empresaInscIVA: true,
			vaAlLibroIVA:   true,
			p: ops.Partida{
				Pata: types.PataIVA,
			},
			alic:             iva.IVA21,
			expectedConcepto: &iv,
			expectedAlicuota: &iva21,
			expectedError:    false,
		},
		{
			name:           "Factura A (pataIVA) - sin alicuota",
			empresaInscIVA: true,
			vaAlLibroIVA:   true,
			p: ops.Partida{
				Pata: types.PataIVA,
			},
			alic:             0,
			expectedConcepto: nil,
			expectedAlicuota: nil,
			expectedError:    true,
		},
		{
			name:           "Factura A (pataProducto) - producto exento",
			empresaInscIVA: true,
			vaAlLibroIVA:   true,
			p: ops.Partida{
				Pata: types.PataProducto,
			},
			alic:             iva.IVAExento,
			expectedConcepto: &ex,
			expectedAlicuota: &ivaExento,
			expectedError:    false,
		},
		{
			name:           "Factura A (pataProducto) - Sin alicuota del frontend",
			empresaInscIVA: true,
			vaAlLibroIVA:   true,
			p: ops.Partida{
				Pata: types.PataProducto,
			},
			alic:             0,
			expectedConcepto: nil,
			expectedAlicuota: nil,
			expectedError:    true,
		},
		{
			name:           "Factura A - Producto no gravado (pataProducto)",
			empresaInscIVA: true,
			vaAlLibroIVA:   true,
			p: ops.Partida{
				Pata: types.PataProducto,
			},
			alic:             iva.IVANoGravado,
			expectedConcepto: &ng,
			expectedAlicuota: &ivaNg,
			expectedError:    false,
		},
		{
			name:           "Factura C",
			empresaInscIVA: false,
			vaAlLibroIVA:   true,
			p: ops.Partida{
				Pata: types.PataProducto,
			},
			alic:             iva.IVA21,
			expectedConcepto: &ng,
			expectedAlicuota: &ivaNg,
			expectedError:    false,
		},
		{
			name:           "Factura X (pataProducto)",
			empresaInscIVA: true,
			vaAlLibroIVA:   false,
			p: ops.Partida{
				Pata: types.PataProducto,
			},
			alic:             iva.IVA21,
			expectedConcepto: nil,
			expectedAlicuota: nil,
			expectedError:    false,
		},
		{
			name:           "Partida patrimonial",
			empresaInscIVA: true,
			vaAlLibroIVA:   true,
			p: ops.Partida{
				Pata: types.PataFacturaPatrimonial,
			},
			alic:             0,
			expectedConcepto: nil,
			expectedAlicuota: nil,
			expectedError:    false,
		},
	}

	for _, v := range table {
		t.Run(v.name, func(t *testing.T) {
			err := LlenarCamposIVA(&v.p, v.alic, v.vaAlLibroIVA, v.empresaInscIVA)

			// Chequeo error
			if v.expectedError {
				assert.NotNil(t, err)
			} else {
				assert.Nil(t, err)
			}

			if v.expectedConcepto == nil {
				assert.Nil(t, v.p.ConceptoIVA)
			} else {
				require.NotNil(t, v.p.ConceptoIVA)
				assert.Equal(t, *v.expectedConcepto, *v.p.ConceptoIVA)
			}
			if v.expectedAlicuota == nil {
				assert.Nil(t, v.p.AlicuotaIVA)
			} else {
				require.NotNil(t, v.p.AlicuotaIVA)
				assert.Equal(t, *v.expectedAlicuota, *v.p.AlicuotaIVA)
			}
		})
	}
}

type mockCuentas struct{}

func (m *mockCuentas) ReadOne(context.Context, cuentas.ReadOneReq) (out cuentas.Cuenta, err error) {
	return
}
