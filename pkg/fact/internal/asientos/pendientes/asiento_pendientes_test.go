package pendientes

import (
	"context"
	"testing"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/fact/config"
	"bitbucket.org/marcos19/one/pkg/fact/internal/opprod"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/productos"
	"bitbucket.org/marcos19/one/pkg/productos/categorias"
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestGenerarAsiento(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)
	Comitente := 1
	var Deposito *int
	persona := types.Persona{
		ID:     uuid.FromStringOrNil("f3008e46-b8f0-40d7-8b0a-9320d39a29ae"),
		Nombre: "Testing name",
	}

	input := Input{}
	input.Producto = uuid.FromStringOrNil("b19c8e5b-da35-407d-af94-0cb3ee7da859")
	input.SKU = uuid.FromStringOrNil("644f8033-9925-4259-a01c-00aace6b048b")
	input.Cantidad = 80
	input.UM = "l"
	Opprod := opprod.OrdenDeCompra{}
	imps := []config.ImputacionContable{
		{
			ProductosSolicitados:         10001,
			ProductosSolicitadosARecibir: 10002,
		},
	}

	out, err := GenerarAsiento(
		context.Background(),
		Comitente,
		input,
		persona,
		Deposito,
		prodGetterMock{},
		superiorGetterMock{},
		Opprod,
		imps,
		ctaGetterMock{})

	require.Nil(err)
	require.Len(out, 2)
	for _, v := range out {
		assert.NotNil(v.Producto)
		assert.NotNil(v.Persona)
		assert.NotNil(v.SKU)
		require.NotNil(v.Cantidad)
		require.NotNil(v.UM)
		assert.Equal(*v.UM, "l")
	}
	assert.Equal(10002, out[0].Cuenta)
	assert.Equal(dec.NewD4(80), *out[0].Cantidad)

	assert.Equal(10001, out[1].Cuenta)
	assert.Equal(dec.NewD4(-80), *out[1].Cantidad)
}

type ctaGetterMock struct{}

func (c ctaGetterMock) PorFuncionContable(ctx context.Context, comitente int, f cuentas.Funcion) ([]cuentas.Cuenta, error) {
	return nil, nil
}
func (c ctaGetterMock) ReadOne(ctx context.Context, req cuentas.ReadOneReq) (cuentas.Cuenta, error) {
	switch req.ID {
	case 10001:
		return cuentas.Cuenta{
			ID:               req.ID,
			Nombre:           "Productos solicitados",
			AperturaPersona:  true,
			AperturaProducto: true,
		}, nil
	case 10002:
		return cuentas.Cuenta{
			ID:               req.ID,
			AperturaPersona:  true,
			AperturaProducto: true,
			Nombre:           "Productos solicitados a recibir",
		}, nil
	}
	panic("not implemented")
}

type prodGetterMock struct{}

func (prodGetterMock) ReadOne(context.Context, productos.ReadOneReq) (out productos.Producto, err error) {
	l := "l"

	return productos.Producto{
		ID:            uuid.FromStringOrNil("0130e045-7310-455b-bad6-ba0a9aae30e9"),
		Nombre:        "Nafta super",
		Categoria:     123,
		UM:            &l,
		Cuantificable: true,
		Inventariable: true,
	}, nil
}

type superiorGetterMock struct{}

func (superiorGetterMock) CategoriaSuperior(ctx context.Context, req categorias.ReadOneReq) (out *categorias.Categoria, err error) {
	switch req.ID {
	case 123:
		out = new(categorias.Categoria)
		out.ID = new(int)
		*out.ID = 34
		out.Nombre = "Naftas"
		return

	case 124:
		out = new(categorias.Categoria)
		out.ID = new(int)
		*out.ID = 35
		out.Nombre = "Combustibles"
		return
	}
	return
}
