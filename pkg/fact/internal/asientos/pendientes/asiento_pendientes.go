package pendientes

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/fact/config"
	"bitbucket.org/marcos19/one/pkg/fact/internal/asientos"
	"bitbucket.org/marcos19/one/pkg/fact/internal/opprod"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/productos"
	"bitbucket.org/marcos19/one/pkg/productos/categorias"
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
)

type CuentaPendientesFlujoGetter interface {
	CuentaPendienteFlujo(ctx context.Context, productoID uuid.UUID) (id int, err error)
}
type CuentaPendientesStockGetter interface {
	CuentaPendientePatrimonial(ctx context.Context, productoID uuid.UUID) (id int, err error)
}

type Input struct {
	asientos.Input
}

// Tiene que determinar cuenta debe y haber.
func GenerarAsiento(
	ctx context.Context,
	comitente int,
	v Input,
	persona types.Persona,
	deposito *int,
	prodGetter productos.Getter,
	catGetter categorias.SuperiorGetter,
	tipo opprod.RegistradorPendientes,
	imps []config.ImputacionContable,
	cuentaGetter cuentas.ReaderOne,
) (out []ops.Partida, err error) {

	montoSum := 0.

	{ // Partida debe
		part := ops.Partida{}

		// Determino cuenta opprod
		ctaApli := 0
		if v.ConceptoProducto != nil {
			ctaApli = *v.ConceptoProducto
		}
		opprodCta, nova, err := tipo.CuentaDebePendientes(ctaApli)
		if err != nil {
			return out, errors.Wrapf(err, "determinando cuenta contable de producto: %v", v.Producto)
		}

		// Determino pata
		part.Pata, err = opprod.PataCuenta(opprodCta)
		if err != nil {
			return out, errors.Wrap(err, "calculando partida haber")
		}

		// Busco la cuenta contable que tiene esa función
		ctaID, err := config.DeterminarImputacion(
			ctx,
			comitente,
			v.Producto,
			imps,
			prodGetter,
			catGetter,
			opprodCta,
		)
		if err != nil {
			return out, errors.Wrapf(err, "mappeando %v a plan contable en producto %v", opprodCta, v.Producto)
		}
		part.Cuenta = ctaID
		log.Debug().Int("opprod", opprodCta).Int("ctaID", ctaID).Msg("Partida DEBE")

		cta, err := cuentaGetter.ReadOne(ctx, cuentas.ReadOneReq{Comitente: comitente, ID: ctaID})
		if err != nil {
			return out, errors.Wrapf(err, "buscando cuenta renglón %v", ctaID)
		}

		// Centro
		if v.Centro != 0 {
			part.Centro = new(int)
			*part.Centro = v.Centro
		}

		// Producto
		part.Producto = new(uuid.UUID)
		*part.Producto = v.Producto

		// Sku
		part.SKU = new(uuid.UUID)
		*part.SKU = v.SKU

		// Partida aplicada
		if cta.UsaAplicaciones {
			if nova {
				// En AgruparPartidas le creo un ID.
				// Si la cuenta usa aplicaciones, le va a pegar el mismo ID.
			} else {
				part.ID, err = uuid.NewV1()
				if err != nil {
					return out, err
				}
				part.PartidaAplicada = new(uuid.UUID)
				*part.PartidaAplicada = v.PartidaAplicada
			}
		}

		// Persona
		if cta.AperturaPersona {
			part.Persona = new(uuid.UUID)
			*part.Persona = persona.ID
		}

		// Deposito
		if cta.AperturaDeposito {
			if deposito == nil {
				return out, errors.Errorf("la cuenta %v abre por depósito pero no se ingresó ninguno", cta.Nombre)
			}
			part.Deposito = new(int)
			*part.Deposito = *deposito
		}

		// Monto
		montoSum += v.Neto
		part.Monto = dec.NewD2(montoSum)

		// Cantidad
		if v.Cantidad != 0 {
			part.Cantidad = new(dec.D4)
			*part.Cantidad = dec.NewD4(v.Cantidad)
		}

		// Unidad de medida
		if v.UM != "" {
			part.UM = new(string)
			*part.UM = v.UM
		}

		// Detalle
		part.Detalle = new(string)
		*part.Detalle = v.Detalle
		out = append(out, part)
	}

	{ // PARTIDA HABER

		p := ops.Partida{}

		// Determino cuenta opprod
		ctaApli := 0
		if v.ConceptoProducto != nil {
			ctaApli = *v.ConceptoProducto
		}
		opprodCta, nova, err := tipo.CuentaHaberPendientes(ctaApli)
		if err != nil {
			return out, errors.Wrapf(err, "determinando cuenta contable de asiento estándar, producto: %v", v.Producto)
		}

		// Determino pata
		p.Pata, err = opprod.PataCuenta(opprodCta)
		if err != nil {
			return out, errors.Wrap(err, "calculando partida haber")
		}

		// Busco la cuenta contable que tiene esa función
		ctaID, err := config.DeterminarImputacion(
			ctx,
			comitente,
			v.Producto,
			imps,
			prodGetter,
			catGetter,
			opprodCta,
		)
		if err != nil {
			return out, errors.Wrapf(err, "mappeando %v a plan contable en producto %v", opprodCta, v.Producto)
		}
		p.Cuenta = ctaID

		cta, err := cuentaGetter.ReadOne(ctx, cuentas.ReadOneReq{Comitente: comitente, ID: ctaID})
		if err != nil {
			return out, errors.Wrapf(err, "buscando cuenta %v", ctaID)
		}

		// Persona
		if persona.ID != uuid.Nil {
			p.Persona = new(uuid.UUID)
			*p.Persona = persona.ID
		}

		// Deposito
		if cta.AperturaDeposito {
			p.Deposito = new(int)
			*p.Deposito = *deposito
		}

		// Sucursal
		if persona.Sucursal != nil {
			p.Sucursal = new(int)
			*p.Sucursal = *persona.Sucursal
		}

		if cta.UsaAplicaciones {
			if nova {
				// En AgruparPartidas le creo un ID.
				// Si la cuenta usa aplicaciones, le va a pegar el mismo ID.
			} else {
				p.ID, err = uuid.NewV1()
				if err != nil {
					return out, err
				}
				p.PartidaAplicada = new(uuid.UUID)
				*p.PartidaAplicada = v.PartidaAplicada
			}
		}
		// Agrego datos adicionales
		if cta.AperturaProducto {
			// Producto
			p.Producto = new(uuid.UUID)
			*p.Producto = v.Producto

			// SKU
			p.SKU = new(uuid.UUID)
			*p.SKU = v.SKU

			// Cantidad
			p.Cantidad = new(dec.D4)
			*p.Cantidad = -dec.NewD4(v.Cantidad)

			// Unida de medida
			p.UM = new(string)
			*p.UM = v.UM
		}

		if cta.AperturaCentro {
			if v.Centro == 0 {
				return out, errors.Errorf("centro no definido")
			}
			p.Centro = new(int)
			*p.Centro = v.Centro
		}

		// Monto
		p.Monto = -dec.NewD2(montoSum)

		out = append(out, p)
	}
	return
}
