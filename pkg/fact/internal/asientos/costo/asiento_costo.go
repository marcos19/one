package asientos

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/fact/internal/asientos"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/gofrs/uuid"
)

// Incluye además el costo del producto.
type Input struct {
	asientos.Input

	Costo float64
}

type CuentaCostoGetter interface {
	CuentaPatrimonial(ctx context.Context, productoID uuid.UUID) (id int, err error)
}

type CuentaBsCambioGetter interface {
	CuentaPatrimonial(ctx context.Context, productoID uuid.UUID) (id int, err error)
}

// Tiene que determinar el costo de cada producto, cuenta costo y cuenta bien de cambio.
func GenerarAsiento(
	input []Input,
	persona types.Persona,
	deposito *int,
	costoGetter CuentaCostoGetter,
	bsCambioGetter CuentaBsCambioGetter,
) (pp []ops.Partida, err error) {

	return
}
