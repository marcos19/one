package estandar

import (
	"context"
	"testing"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/fact/config"
	"bitbucket.org/marcos19/one/pkg/fact/internal/asientos"
	"bitbucket.org/marcos19/one/pkg/fact/internal/opprod"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/iva"
	"bitbucket.org/marcos19/one/pkg/productos"
	"bitbucket.org/marcos19/one/pkg/productos/categorias"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/davecgh/go-spew/spew"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const Proveedores = 2000
const ProductosCompradosARecibir = 2001
const IVA = 2300
const Productos = 1300
const FacturasARecibir = 2400

func TestGenerarAsiento(t *testing.T) {
	require := require.New(t)
	assert := assert.New(t)

	pers := types.Persona{
		ID:     uuid.FromStringOrNil("27e05e54-c501-4229-92f5-a4afad4255fd"),
		Nombre: "Mocking persona",
	}
	var sinDeposito *int

	prodGetter := prodGetter{}
	catGetter := catGetter{}
	ctasGetter := ctasGetter{}

	imps := []config.ImputacionContable{
		{
			Proveedores:                Proveedores,
			ProductosCompradosARecibir: ProductosCompradosARecibir,
			IVA:                        IVA,
			Productos:                  Productos,
			FacturasARecibir:           FacturasARecibir,
		},
	}

	t.Run("Factura A", func(t *testing.T) { // Partida básica con IVA
		calculaIVA := true
		vaAlLibroIVA := true
		empresaInscriptaEnIVA := true
		ii := []Input{
			{
				Input: asientos.Input{
					Producto:    uuid.FromStringOrNil("ac99ef64-e470-4ce5-b613-29d027c20174"),
					SKU:         uuid.FromStringOrNil("6319e353-066e-4f1a-b437-d4cef55aac82"),
					Cantidad:    10,
					UM:          "unidades",
					Neto:        1000,
					AlicuotaIVA: iva.IVA21,
				},
			},
		}
		pp, err := GenerarAsiento(
			context.Background(),
			1,
			ii,
			pers,
			sinDeposito,
			&prodGetter,
			&catGetter,
			calculaIVA,
			vaAlLibroIVA,
			empresaInscriptaEnIVA,
			opprod.FacturaDeCompra{},
			imps,
			&ctasGetter,
			&unicIDmock{},
			[]int{},
		)
		require.Nil(err)
		assert.Len(pp, 3, "%v", spew.Sdump(pp))
		assert.Equal(2001, pp[0].Cuenta)
		assert.Equal(2300, pp[1].Cuenta)
		assert.Equal(2000, pp[2].Cuenta)

		assert.Equal(types.PataProducto, pp[0].Pata)
		assert.Equal(types.PataIVA, pp[1].Pata)
		assert.Equal(types.PataFacturaPatrimonial, pp[2].Pata)

		require.NotNil(pp[0].ConceptoIVA)
		assert.Equal(iva.BaseImponible, *pp[0].ConceptoIVA)
		require.NotNil(pp[1].ConceptoIVA)
		assert.Equal(iva.IVA, *pp[1].ConceptoIVA)
		assert.Nil(pp[2].ConceptoIVA)

		assert.Equal(dec.NewD2(1000), pp[0].Monto)
		assert.Equal(dec.NewD2(210), pp[1].Monto)
		assert.Equal(dec.NewD2(-1210), pp[2].Monto, "%v", spew.Sdump(pp))

		assert.Equal(dec.NewD4(10), *pp[0].Cantidad)
		assert.Nil(pp[1].Cantidad)
		assert.Nil(pp[2].Cantidad)

		assert.Equal("unidades", *pp[0].UM)
		assert.Nil(pp[1].UM)
		assert.Nil(pp[2].UM)

		assert.NotNil(pp[0].Persona)
		assert.Nil(pp[1].Persona)
		assert.NotNil(pp[2].Persona)

		assert.NotEqual(uuid.Nil, pp[0].Persona)
	})

	t.Run("Factura C", func(t *testing.T) { // Compra, comprobante fiscal que no genera IVA (Factura C)
		calculaIVA := false
		vaAlLibroIVA := true
		empresaInscriptaEnIVA := false
		ii := []Input{
			{
				Input: asientos.Input{
					Producto:    uuid.FromStringOrNil("ac99ef64-e470-4ce5-b613-29d027c20174"),
					SKU:         uuid.FromStringOrNil("6319e353-066e-4f1a-b437-d4cef55aac82"),
					Cantidad:    10,
					UM:          "unidades",
					Neto:        1000,
					AlicuotaIVA: iva.IVA21,
				},
			},
		}
		pp, err := GenerarAsiento(
			context.Background(),
			1,
			ii,
			pers,
			sinDeposito,
			&prodGetter,
			&catGetter,
			calculaIVA,
			vaAlLibroIVA,
			empresaInscriptaEnIVA,
			opprod.FacturaDeCompra{},
			imps,
			&ctasGetter,
			&unicIDmock{},
			[]int{},
		)
		require.Nil(err)
		require.Len(pp, 2, "%v", spew.Sdump(pp))
		assert.Equal(2001, pp[0].Cuenta)
		assert.Equal(2000, pp[1].Cuenta)

		assert.Equal(types.PataProducto, pp[0].Pata)
		assert.Equal(types.PataFacturaPatrimonial, pp[1].Pata)

		require.NotNil(pp[0].ConceptoIVA)
		assert.Equal(iva.NoGravado, *pp[0].ConceptoIVA)
		require.NotNil(pp[0].AlicuotaIVA)
		assert.Equal(iva.IVANoGravado, *pp[0].AlicuotaIVA)

		assert.Nil(pp[1].ConceptoIVA)
		assert.Nil(pp[1].AlicuotaIVA)

		assert.Equal(dec.NewD2(1000), pp[0].Monto)
		assert.Equal(dec.NewD2(-1000), pp[1].Monto)

		assert.Equal(dec.NewD4(10), *pp[0].Cantidad)
		assert.Nil(pp[1].Cantidad)

		assert.Equal("unidades", *pp[0].UM)
		assert.Nil(pp[1].UM)

		assert.NotNil(pp[0].Persona)
		assert.NotNil(pp[1].Persona)

		assert.NotEqual(uuid.Nil, pp[1].Persona)
	})

	t.Run("Remito - Producto con unicidades", func(t *testing.T) { // Partida básica con IVA
		calculaIVA := false
		vaAlLibroIVA := false
		empresaInscriptaEnIVA := true
		tipoUnic := 1
		ii := []Input{
			{
				Input: asientos.Input{
					Producto:    uuid.FromStringOrNil("ac99ef64-e470-4ce5-b613-29d027c20174"),
					SKU:         uuid.FromStringOrNil("6319e353-066e-4f1a-b437-d4cef55aac82"),
					Cantidad:    2,
					UM:          "unidades",
					Neto:        200,
					AlicuotaIVA: iva.IVA21,
					Unicidades: []asientos.Unicidad{
						{
							Tipo:    tipoUnic,
							Detalle: "Unic 1",
							Monto:   dec.NewD2(100),
						},
						{
							Tipo:    tipoUnic,
							Detalle: "Unic 2",
							Monto:   dec.NewD2(100),
						},
					},
				},
			},
		}

		// Debería pasar
		pp, err := GenerarAsiento(
			context.Background(),
			1,
			ii,
			pers,
			sinDeposito,
			&prodGetter,
			&catGetter,
			calculaIVA,
			vaAlLibroIVA,
			empresaInscriptaEnIVA,
			opprod.RemitoDeCompra{},
			imps,
			&ctasGetter,
			&unicIDmock{},
			[]int{1},
		)
		require.Nil(err)
		assert.Len(pp, 3, "%v", spew.Sdump(pp))
		for _, v := range pp {
			switch v.Cuenta {

			case Productos:
				assert.Equal(dec.NewD2(100), v.Monto)
				assert.NotNil(v.Producto)
				assert.NotNil(v.Unicidad)
				require.NotNil(v.PartidaAplicada)
				assert.Equal(v.ID, *v.PartidaAplicada)

			case FacturasARecibir:
				assert.Equal(dec.NewD2(-200), v.Monto)
			}
		}

		// Remito tiene unicidades que aplican a partidas ya existentes.
		// Si no se encuentran debería dar error
		pp, err = GenerarAsiento(
			context.Background(),
			1,
			ii,
			pers,
			sinDeposito,
			&prodGetter,
			&catGetter,
			calculaIVA,
			vaAlLibroIVA,
			empresaInscriptaEnIVA,
			opprod.RemitoDeCompra{},
			imps,
			&ctasGetter,
			&unicIDmock{err: true},
			[]int{},
		)
		require.NotNil(err)
		assert.Contains(err.Error(), "partida aplicada")

		// Remito tiene unicidades que aplican a partidas ya existentes.
		// UnicIDGetter las encuentra, debería estar OK
		pp, err = GenerarAsiento(
			context.Background(),
			1,
			ii,
			pers,
			sinDeposito,
			&prodGetter,
			&catGetter,
			calculaIVA,
			vaAlLibroIVA,
			empresaInscriptaEnIVA,
			opprod.RemitoDeCompra{},
			imps,
			&ctasGetter,
			&unicIDmock{err: false},
			[]int{},
		)
	})
}

type ctasGetter struct{}

func (c *ctasGetter) ReadOne(ctx context.Context, req cuentas.ReadOneReq) (out cuentas.Cuenta, err error) {
	switch req.ID {

	case Proveedores:
		return cuentas.Cuenta{
			ID:              req.ID,
			Comitente:       req.Comitente,
			Nombre:          "Cuentas por pagar",
			AperturaPersona: true,
			UsaAplicaciones: true,
		}, nil

	case ProductosCompradosARecibir:
		return cuentas.Cuenta{
			ID:               req.ID,
			Comitente:        req.Comitente,
			Nombre:           "Productos comprados pendientes de recibir",
			AperturaPersona:  true,
			AperturaProducto: true,
			UsaAplicaciones:  true,
		}, nil

	case IVA:
		return cuentas.Cuenta{
			ID:        req.ID,
			Comitente: req.Comitente,
			Nombre:    "IVA CF",
		}, nil

	case Productos:
		return cuentas.Cuenta{
			ID:               req.ID,
			Comitente:        req.Comitente,
			Nombre:           "Productos",
			AperturaProducto: true,
			UsaAplicaciones:  true,
		}, nil

	case FacturasARecibir:
		return cuentas.Cuenta{
			ID:               req.ID,
			Comitente:        req.Comitente,
			Nombre:           "Facturas a recibir",
			AperturaProducto: true,
			UsaAplicaciones:  true,
		}, nil
	}
	return out, errors.Errorf("cuenta %v no encontrada", req.ID)
}

type prodGetter struct{}

func (p *prodGetter) ReadOne(ctx context.Context, req productos.ReadOneReq) (out productos.Producto, err error) {
	if req.ID == uuid.FromStringOrNil("ac99ef64-e470-4ce5-b613-29d027c20174") {
		out = productos.Producto{
			ID:        req.ID,
			Nombre:    "Mocking product",
			Categoria: 500,
		}
		return
	}
	return out, errors.Errorf("no product found")
}

func (p *ctasGetter) PorFuncionContable(ctx context.Context, comitente int, f cuentas.Funcion) (cta []cuentas.Cuenta, err error) {
	return
}

type catGetter struct{}

func (c *catGetter) CategoriaSuperior(ctx context.Context, req categorias.ReadOneReq) (out *categorias.Categoria, err error) {
	cats := []categorias.Categoria{}

	{
		id := 500
		padre := 50
		sofas := categorias.Categoria{
			ID:      &id,
			Nombre:  "Sofas",
			PadreID: &padre,
		}
		cats = append(cats, sofas)
	}
	{
		id := 50
		sofas := categorias.Categoria{
			ID:     &id,
			Nombre: "Furnitures",
		}
		cats = append(cats, sofas)
	}

	switch req.ID {
	case 500:
		return &cats[1], nil

	case 50:
		return nil, nil
	}
	return out, errors.Errorf("no category found for ID %v", req.ID)
}

type unicIDmock struct {
	err bool
}

func (m *unicIDmock) GetPartidaAplicada(context.Context, int, uuid.UUID) (uuid.UUID, error) {
	if m.err {
		return uuid.UUID{}, errors.Errorf("unic id mock error")
	}
	return uuid.FromStringOrNil("ae7741a0-0aa0-11ec-ae9a-42010a8e0002"), nil
}
