// Package asientos genera las partidas contables correspondientes a los
// items de fact.Productos o de fact.AplicacionesProductos.
package estandar

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/fact/config"
	"bitbucket.org/marcos19/one/pkg/fact/internal/asientos"
	"bitbucket.org/marcos19/one/pkg/fact/internal/opprod"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/productos"
	"bitbucket.org/marcos19/one/pkg/productos/categorias"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
	"github.com/rs/zerolog/log"
)

type Input struct {
	asientos.Input
}

// Tiene que determinar cuentas para renglón, IVA y patrimonial.
func GenerarAsiento(
	ctx context.Context,
	comitente int,
	ii []Input,
	persona types.Persona,
	deposito *int,
	prodGetter productos.Getter,
	catGetter categorias.SuperiorGetter,
	calculaIVA bool,
	vaAlLibroIVA bool,
	empresaInscIVA bool,
	tipo opprod.RegistradorEstandar,
	imps []config.ImputacionContable,
	cuentaGetter cuentas.ReaderOne,
	unicIDgetter ops.UnicidadIDGetter,
	unicidadesQueSeCrean []int,
) (out []ops.Partida, err error) {

	if len(ii) == 0 {
		return
	}

	for _, v := range ii {

		signoIVA := 1.
		componentes := map[string][]ops.Partida{}

		{ // Partida renglón
			part := ops.Partida{}

			// Determino cuenta opprod
			ctaApli := 0
			if v.ConceptoProducto != nil {
				ctaApli = *v.ConceptoProducto
			}
			opprodCta, nova, err := tipo.CuentaDebe(ctaApli)
			if err != nil {
				return out, errors.Wrapf(err, "determinando cuenta contable de asiento estándar, producto: %v", v.Producto)
			}

			part.Pata, err = tipo.Pata(opprodCta)
			if err != nil {
				return out, errors.Wrap(err, "calculando partida haber")
			}
			if part.Pata == types.PataProducto && v.PorCuentaTerceros {
				part.Pata = types.PataProductoNoFacturado
			}

			// Busco la cuenta contable que tiene esa función
			ctaID, err := config.DeterminarImputacion(
				ctx,
				comitente,
				v.Producto,
				imps,
				prodGetter,
				catGetter,
				opprodCta,
			)
			log.Debug().Msgf("Para debe de asiento estándar usando opprodCta: %v; sacado de cuenta: %v", opprodCta, ctaApli)
			if err != nil {
				return out, errors.Wrapf(err, "cuenta debe: mappeando '%v' a plan contable en producto %v", opprod.NombreCuenta(opprodCta), v.Producto)
			}
			part.Cuenta = ctaID

			// Traigo cuenta contable
			cta, err := cuentaGetter.ReadOne(ctx, cuentas.ReadOneReq{Comitente: comitente, ID: ctaID})
			if err != nil {
				return out, errors.Wrapf(err, "buscando cuenta renglón %v", ctaID)
			}

			if vaAlLibroIVA {
				err = asientos.LlenarCamposIVA(&part, v.AlicuotaIVA, vaAlLibroIVA, empresaInscIVA)
				if err != nil {
					return out, errors.Wrapf(err, "calculando partida pata %v ", part.Pata)
				}
			}

			// Centro
			if cta.AperturaCentro {
				if v.Centro == 0 {
					return out, errors.Errorf("la cuenta %v exige apertura por centro", cta.Nombre)
				}
				part.Centro = new(int)
				*part.Centro = v.Centro
			}

			// Producto
			if cta.AperturaProducto {
				if v.Producto == uuid.Nil {
					return out, errors.Errorf("la cuenta %v exige apertura por producto", cta.Nombre)
				}
				part.Producto = new(uuid.UUID)
				*part.Producto = v.Producto

				// Sku
				if v.SKU == uuid.Nil {
					return out, errors.Errorf("la cuenta %v exige SKU", cta.Nombre)
				}
				part.SKU = new(uuid.UUID)
				*part.SKU = v.SKU

				// Cantidad
				if v.Cantidad != 0 {
					part.Cantidad = new(dec.D4)
					*part.Cantidad = dec.NewD4(v.Cantidad)
				}

				// Unidad de medida
				if v.UM != "" {
					part.UM = new(string)
					*part.UM = v.UM
				}
			}

			// Partida aplicada
			log.Debug().Msgf("La cuenta usada en HABER era %v y usaba aplicaciones: %v. Partida aplicada era: %v", cta.Nombre, cta.UsaAplicaciones, v.PartidaAplicada)
			if cta.UsaAplicaciones {
				if nova {
					// En AgruparPartidas le creo un ID.
					// Si la cuenta usa aplicaciones, le va a pegar el mismo ID.
				} else {
					part.ID, err = uuid.NewV1()
					if err != nil {
						return out, err
					}
					part.PartidaAplicada = new(uuid.UUID)
					*part.PartidaAplicada = v.PartidaAplicada
				}
			}

			// Persona
			if cta.AperturaPersona {
				part.Persona = new(uuid.UUID)
				*part.Persona = persona.ID
			}

			// Sucursal
			if persona.Sucursal != nil {
				part.Sucursal = new(int)
				*part.Sucursal = *persona.Sucursal
			}

			// Deposito
			if cta.AperturaDeposito {
				if deposito == nil {
					return out, errors.Errorf("la cuenta %v abre por depósito pero no se ingresó ninguno", cta.Nombre)
				}
				part.Deposito = new(int)
				*part.Deposito = *deposito
			}

			// Monto
			part.Monto = dec.NewD2(v.Neto)

			// Detalle
			part.Detalle = new(string)
			*part.Detalle = v.Detalle

			// Partidas unicidades
			if len(v.Unicidades) == 0 {
				componentes[part.Pata] = []ops.Partida{part}
			} else {
				// Hay unicidades
				if cta.AperturaUnicidad || part.Pata == types.PataProducto {
					pp := []ops.Partida{}
					if cta.UsaAplicaciones {
						pp, err = crearPartidasUnicidadUso(ctx, comitente, v, part, unicIDgetter, unicidadesQueSeCrean)
						if err != nil {
							return out, errors.Wrap(err, "creando partidas unicidad de uso")
						}
					} else {
						pp, err = crearPartidasUnicidadReferencial(v, part)
						if err != nil {
							return out, errors.Wrap(err, "creando partidas unicidad referencial")
						}
					}
					componentes[part.Pata] = append(componentes[part.Pata], pp...)
				} else {
					componentes[part.Pata] = append(componentes[part.Pata], part)
				}
			}
		}

		// Partida IVA
		if calculaIVA {

			// Calculo monto de IVA
			porcentajeIVA, err := v.AlicuotaIVA.Porcentaje()
			if err != nil {
				return out, errors.Wrapf(err, "determinando porcentaje de IVA de producto %v", v.Producto)
			}
			montoIVA := dec.NewD2(v.Neto * porcentajeIVA * signoIVA)

			if montoIVA != 0 {
				// Busco la cuenta contable que tiene esa función
				ctaID, err := config.DeterminarImputacion(
					ctx,
					comitente,
					v.Producto,
					imps,
					prodGetter,
					catGetter,
					opprod.IVA,
				)
				if err != nil {
					return out, errors.Wrapf(err, "buscando cuenta de IVA producto %v", v.Producto)
				}

				p := ops.Partida{}
				p.Cuenta = ctaID
				p.Pata = types.PataIVA
				err = asientos.LlenarCamposIVA(&p, v.AlicuotaIVA, vaAlLibroIVA, empresaInscIVA)
				if err != nil {
					return out, errors.Wrap(err, "creando partida pata IVA")
				}
				p.Monto = montoIVA
				componentes[types.PataIVA] = []ops.Partida{p}
			}
		}

		{ // PARTIDA HABER

			part := ops.Partida{}

			// Determino cuenta opprod
			ctaApli := 0
			if v.ConceptoProducto != nil {
				ctaApli = *v.ConceptoProducto
			}
			opprodCta, nova, err := tipo.CuentaHaber(ctaApli)
			if err != nil {
				return out, errors.Wrapf(err, "determinando cuenta contable de asiento estándar, producto: %v", v.Producto)
			}

			part.Pata, err = tipo.Pata(opprodCta)
			if err != nil {
				return out, errors.Wrap(err, "calculando partida haber")
			}
			if part.Pata == types.PataProducto && v.PorCuentaTerceros {
				part.Pata = types.PataProductoNoFacturado
			}

			if part.Pata == types.PataProducto {
				signoIVA *= -1
			}
			// Busco la cuenta contable que tiene esa función
			ctaID, err := config.DeterminarImputacion(
				ctx,
				comitente,
				v.Producto,
				imps,
				prodGetter,
				catGetter,
				opprodCta,
			)
			log.Debug().Msgf("Para haber de asiento estándar usando opprodCta: %v; sacado de cuenta: %v", opprodCta, ctaApli)
			if err != nil {
				return out, errors.Wrapf(err, "cuenta haber: mappeando %v a plan contable en producto %v", opprod.NombreCuenta(opprodCta), v.Producto)
			}
			part.Cuenta = ctaID

			// Traigo cuenta contable
			cta, err := cuentaGetter.ReadOne(ctx, cuentas.ReadOneReq{Comitente: comitente, ID: ctaID})
			if err != nil {
				return out, errors.Wrapf(err, "buscando cuenta %v", ctaID)
			}

			if vaAlLibroIVA {
				err = asientos.LlenarCamposIVA(&part, v.AlicuotaIVA, vaAlLibroIVA, empresaInscIVA)
				if err != nil {
					return out, errors.Wrapf(err, "calculando partida pata %v ", part.Pata)
				}
			}

			// Centro
			if cta.AperturaCentro {
				if v.Centro == 0 {
					return out, errors.Errorf("la cuenta %v exige apertura por centro", cta.Nombre)
				}
				part.Centro = new(int)
				*part.Centro = v.Centro
			}

			// Producto
			if cta.AperturaProducto {
				if v.Producto == uuid.Nil {
					return out, errors.Errorf("la cuenta %v exige apertura por producto", cta.Nombre)
				}
				part.Producto = new(uuid.UUID)
				*part.Producto = v.Producto

				// Sku
				if v.SKU == uuid.Nil {
					return out, errors.Errorf("la cuenta %v exige SKU", cta.Nombre)
				}
				part.SKU = new(uuid.UUID)
				*part.SKU = v.SKU

				// Cantidad
				if v.Cantidad != 0 {
					part.Cantidad = new(dec.D4)
					*part.Cantidad = -dec.NewD4(v.Cantidad)
				}

				// Unidad de medida
				if v.UM != "" {
					part.UM = new(string)
					*part.UM = v.UM
				}
			}

			// Partida aplicada
			log.Debug().Msgf("La cuenta usada en HABER era %v y usaba aplicaciones: %v. Partida aplicada era: %v", cta.Nombre, cta.UsaAplicaciones, v.PartidaAplicada)
			if cta.UsaAplicaciones {
				if nova {
					// En AgruparPartidas le creo un ID.
					// Si la cuenta usa aplicaciones, le va a pegar el mismo ID.
				} else {
					part.ID, err = uuid.NewV1()
					if err != nil {
						return out, err
					}
					part.PartidaAplicada = new(uuid.UUID)
					*part.PartidaAplicada = v.PartidaAplicada
				}
			}

			// Persona
			if cta.AperturaPersona {
				part.Persona = new(uuid.UUID)
				*part.Persona = persona.ID
			}

			// Sucursal
			if persona.Sucursal != nil {
				part.Sucursal = new(int)
				*part.Sucursal = *persona.Sucursal
			}

			// Deposito
			if cta.AperturaDeposito {
				if deposito == nil {
					return out, errors.Errorf("la cuenta %v abre por depósito pero no se ingresó ninguno", cta.Nombre)
				}
				part.Deposito = new(int)
				*part.Deposito = *deposito
			}

			// Monto
			part.Monto = -dec.NewD2(v.Neto)

			if cta.AperturaUnicidad && len(v.Unicidades) == 0 {
				return out, errors.Errorf("la cuenta '%v' abre por unicidad pero no se ingresó ninguna", cta.Nombre)
			}

			// Partidas unicidades
			if len(v.Unicidades) == 0 {
				componentes[part.Pata] = []ops.Partida{part}
			} else {
				// Hay unicidades
				if cta.AperturaUnicidad || part.Pata == types.PataProducto {

					pp := []ops.Partida{}
					if cta.UsaAplicaciones {
						pp, err = crearPartidasUnicidadUso(ctx, comitente, v, part, unicIDgetter, unicidadesQueSeCrean)
						if err != nil {
							return out, errors.Wrap(err, "creando partidas unicidad en haber")
						}
					} else {
						pp, err = crearPartidasUnicidadReferencial(v, part)
						if err != nil {
							return out, errors.Wrap(err, "creando partidas unicidad en haber")
						}
					}
					if err != nil {
						return out, errors.Wrap(err, "creando partidas unicidad en haber")
					}
					componentes[part.Pata] = append(componentes[part.Pata], pp...)
				} else {
					componentes[part.Pata] = append(componentes[part.Pata], part)
				}
			}
		}

		// Partidas de producto
		montoRenglon := dec.D2(0)
		cantidadRenglon := dec.D4(0)
		prods := []ops.Partida{}
		prods = append(prods, componentes[types.PataProducto]...)
		prods = append(prods, componentes[types.PataProductoNoFacturado]...)

		for _, v := range prods {
			// if v.Monto == 0 {
			// 	if v.Cantidad == nil {
			// 		continue
			// 	} else {
			// 		if *v.Cantidad == 0 {
			// 			continue
			// 		}
			// 	}
			// }
			montoRenglon += v.Monto
			if v.Cantidad != nil {
				cantidadRenglon += *v.Cantidad
			}
			out = append(out, v)
		}

		// Partidas de IVA
		montoIVA := dec.D2(0)
		for _, v := range componentes[types.PataIVA] {
			if signoIVA == -1 {
				v.Monto = -v.Monto
			}
			montoIVA += v.Monto
			out = append(out, v)
		}

		if len(componentes[types.PataIVA]) > 0 && len(componentes[types.PataProducto]) > 1 {
			return out, errors.Errorf("caso de varias unicidades por partida con IVA no implementado")
		}

		// Partidas patrimoniales
		for _, v := range componentes[types.PataFacturaPatrimonial] {
			v.Monto = -(montoRenglon + montoIVA)

			// Aunque sea cero, la dejo igual.
			// if v.Monto == 0 {
			// 	if v.Cantidad == nil {
			// 		continue
			// 	} else {
			// 		if *v.Cantidad == 0 {
			// 			continue
			// 		}
			// 	}
			// }
			out = append(out, v)
		}
	}
	return
}

// En base a al renglón de PartidaProductos, lo parte haciendo una partida contable
// para cada unicidad.
// Montos y cantidades serán iguales para todos.
func crearPartidasUnicidadUso(ctx context.Context,
	comitente int,
	v Input,
	part ops.Partida,
	unicIDgetter ops.UnicidadIDGetter,
	unicidadesQueSeCrean []int,
) (out []ops.Partida, err error) {

	cantidad := part.Cantidad.Float() / float64(len(v.Unicidades))
	monto := part.Monto.Float() / float64(len(v.Unicidades))

	for _, unicidad := range v.Unicidades {
		partUnic := part
		partUnic.ID, err = uuid.NewV1()
		if err != nil {
			return out, errors.Wrap(err, "creando ID de partida de unicidad")
		}
		partUnic.Unicidad = new(uuid.UUID)
		*partUnic.Unicidad = unicidad.ID

		partUnic.UnicidadTipo = new(int)
		*partUnic.UnicidadTipo = unicidad.Tipo

		partUnic.PartidaAplicada = new(uuid.UUID)

		// Esta operación crea una nueva unicidad?
		for _, v := range unicidadesQueSeCrean {
			if v == unicidad.Tipo {
				*partUnic.PartidaAplicada = partUnic.ID
			}
		}
		if *partUnic.PartidaAplicada == uuid.Nil {
			// Busco ID de la partida a la que estoy aplicando
			*partUnic.PartidaAplicada, err = unicIDgetter.GetPartidaAplicada(ctx, comitente, *partUnic.Unicidad)
			if err != nil {
				return out, errors.Wrapf(
					err, "buscando ID de partida aplicada para unicidad %v", partUnic.Unicidad,
				)
			}
		}

		if unicidad.Detalle != "" {
			part.Detalle = new(string)
			*part.Detalle = unicidad.Detalle
		}
		partUnic.Pata = part.Pata

		partUnic.Cantidad = new(dec.D4)
		*partUnic.Cantidad = dec.NewD4(cantidad)

		// Pego el monto unitario
		partUnic.Monto = dec.NewD2(monto)

		out = append(out, partUnic)
	}
	return
}

func crearPartidasUnicidadReferencial(v Input, part ops.Partida) (out []ops.Partida, err error) {

	cantidad := part.Cantidad.Float() / float64(len(v.Unicidades))
	monto := part.Monto.Float() / float64(len(v.Unicidades))

	for _, unicidad := range v.Unicidades {
		partUnic := part
		partUnic.ID, err = uuid.NewV1()
		if err != nil {
			return out, errors.Wrap(err, "creando ID de partida de unicidad")
		}
		partUnic.Unicidad = new(uuid.UUID)
		*partUnic.Unicidad = unicidad.ID

		partUnic.UnicidadTipo = new(int)
		*partUnic.UnicidadTipo = unicidad.Tipo

		if unicidad.Detalle != "" {
			part.Detalle = new(string)
			*part.Detalle = unicidad.Detalle
		}
		partUnic.Pata = part.Pata

		// Si tiene unicidad la cantidad tiene que ser 1
		partUnic.Cantidad = new(dec.D4)
		*partUnic.Cantidad = dec.NewD4(cantidad)

		// Pego el monto unitario
		partUnic.Monto = dec.NewD2(monto)
		out = append(out, partUnic)
	}
	return
}
