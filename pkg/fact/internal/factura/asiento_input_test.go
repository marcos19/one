package factura

import (
	"context"
	"testing"

	"bitbucket.org/marcos19/one/pkg/fact/types"
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestHacerAsientoInputDesdeProductos(t *testing.T) {
	require := require.New(t)
	assert := assert.New(t)
	pp := []types.PartidaProducto{
		{
			Producto:            uuid.FromStringOrNil("7b7ae421-2a33-4837-af24-059df128e663"),
			SKU:                 uuid.FromStringOrNil("7b7ae421-2a33-4837-af24-059df128e663"),
			Codigo:              "AA",
			ProductoNombre:      "Nombre",
			ProductoImpresion:   "NombrePrint",
			ProductoDetalle:     "Algun detale",
			UM:                  "q",
			UMNombre:            "unidades",
			Cantidad:            dec.NewD4(10),
			PrecioUnitario:      dec.NewD4(100),
			PorcentajeRecargo:   dec.NewD4(0.1),
			PrecioUnitarioFinal: dec.NewD4(110),
			MontoTotal:          dec.NewD4(10 * 100 * 1.1),
			ListaPrecios:        1,
			CuentaDeTerceros:    false,
			Centro:              2,
		},
	}

	out := hacerAsientoInputDesdeProductos(pp, 1)
	require.Len(out, len(pp))

	assert.Equal(pp[0].Producto, out[0].Producto)
	assert.Equal(pp[0].SKU, out[0].SKU)
	assert.Equal(pp[0].Codigo, out[0].Codigo)
	assert.Equal(pp[0].UM, out[0].UM)
	assert.Equal(pp[0].Cantidad.Float(), out[0].Cantidad)
	assert.Equal(10*100*1.1, out[0].Neto)
	assert.Equal(pp[0].ProductoDetalle, out[0].Detalle)
	assert.Equal(pp[0].CuentaDeTerceros, out[0].PorCuentaTerceros)
	assert.Equal(pp[0].Centro, out[0].Centro)
	assert.Equal(pp[0].AlicuotaIVA, out[0].AlicuotaIVA)
	assert.Nil(out[0].Unicidades)

	pp[0].Unicidades = []types.UnicidadDetalle{
		{
			ID:      uuid.FromStringOrNil("03d14fb6-918a-4255-8519-6156403e62bb"),
			Detalle: "Detalle de la unicidad",
		},
		{
			ID:      uuid.FromStringOrNil("03d14fb6-918a-4255-8519-6156403e62bb"),
			Detalle: "Detalle de la unicidad",
		},
		{
			ID:      uuid.FromStringOrNil("03d14fb6-918a-4255-8519-6156403e62bb"),
			Detalle: "Detalle de la unicidad",
		},
		{
			ID:      uuid.FromStringOrNil("03d14fb6-918a-4255-8519-6156403e62bb"),
			Detalle: "Detalle de la unicidad",
		},
		{
			ID:      uuid.FromStringOrNil("03d14fb6-918a-4255-8519-6156403e62bb"),
			Detalle: "Detalle de la unicidad",
		},
		{
			ID:      uuid.FromStringOrNil("03d14fb6-918a-4255-8519-6156403e62bb"),
			Detalle: "Detalle de la unicidad",
		},
		{
			ID:      uuid.FromStringOrNil("03d14fb6-918a-4255-8519-6156403e62bb"),
			Detalle: "Detalle de la unicidad",
		},
		{
			ID:      uuid.FromStringOrNil("03d14fb6-918a-4255-8519-6156403e62bb"),
			Detalle: "Detalle de la unicidad",
		},
		{
			ID:      uuid.FromStringOrNil("03d14fb6-918a-4255-8519-6156403e62bb"),
			Detalle: "Detalle de la unicidad",
		},
		{
			ID:      uuid.FromStringOrNil("03d14fb6-918a-4255-8519-6156403e62bb"),
			Detalle: "Detalle de la unicidad",
		},
	}
	out = hacerAsientoInputDesdeProductos(pp, 1)
	require.Len(out, len(pp))
	require.Len(out[0].Unicidades, 10)
	assert.Equal(uuid.FromStringOrNil("03d14fb6-918a-4255-8519-6156403e62bb"), out[0].Unicidades[0].ID)
	assert.Equal("Detalle de la unicidad", out[0].Unicidades[0].Detalle)
	assert.Equal(dec.NewD2(110), out[0].Unicidades[0].Monto)

}

func TestHacerAsientoDesdeAplicacionesProductos(t *testing.T) {

	// Productos solicitados a recibir
	const cta = 100

	{ // Sin partidas
		pp := []types.AplicacionProducto{}
		_, err := hacerAsientoInputDesdeAplicacionesProductos(context.Background(), pp, 1, ctaGetterMock{})
		assert.Nil(t, err)
	}
	{ // Partidas sin cuenta contable
		pp := []types.AplicacionProducto{
			{},
		}
		_, err := hacerAsientoInputDesdeAplicacionesProductos(context.Background(), pp, 1, ctaGetterMock{})
		assert.ErrorIs(t, err, ErrCuentaNoDefinida)
	}
	{ // Partida sin producto
		pp := []types.AplicacionProducto{
			{
				Cuenta: cta,
			},
		}
		_, err := hacerAsientoInputDesdeAplicacionesProductos(context.Background(), pp, 1, ctaGetterMock{})
		assert.ErrorIs(t, err, ErrProductoNoDefinido)
	}
	{ // Partida sin SKU
		pp := []types.AplicacionProducto{
			{
				Cuenta:   cta,
				Producto: uuid.FromStringOrNil("03d14fb6-918a-4255-8519-6156403e62bb"),
			},
		}
		_, err := hacerAsientoInputDesdeAplicacionesProductos(context.Background(), pp, 1, ctaGetterMock{})
		assert.ErrorIs(t, err, ErrSKUNoDefinido)
	}
	{ // Partida sin monto ni cantidad
		pp := []types.AplicacionProducto{
			{
				Cuenta:   cta,
				Producto: uuid.FromStringOrNil("03d14fb6-918a-4255-8519-6156403e62bb"),
				SKU:      uuid.FromStringOrNil("03d14fb6-918a-4255-8519-6156403e62bb"),
			},
		}
		_, err := hacerAsientoInputDesdeAplicacionesProductos(context.Background(), pp, 1, ctaGetterMock{})
		assert.ErrorIs(t, err, ErrNiMontoNiCantidad)
	}

	{ // Partida con cantidad pero sin UM
		pp := []types.AplicacionProducto{
			{
				Cuenta:   cta,
				Producto: uuid.FromStringOrNil("03d14fb6-918a-4255-8519-6156403e62bb"),
				SKU:      uuid.FromStringOrNil("03d14fb6-918a-4255-8519-6156403e62bb"),
				Cantidad: 23,
			},
		}
		_, err := hacerAsientoInputDesdeAplicacionesProductos(context.Background(), pp, 1, ctaGetterMock{})
		assert.ErrorIs(t, err, ErrFaltaUM)
	}

	{ // Falta partida aplicada
		pp := []types.AplicacionProducto{
			{
				Cuenta:   cta,
				Producto: uuid.FromStringOrNil("03d14fb6-918a-4255-8519-6156403e62bb"),
				SKU:      uuid.FromStringOrNil("03d14fb6-918a-4255-8519-6156403e62bb"),
				Cantidad: 23,
				Monto:    3423,
				UM:       "q",
			},
		}
		_, err := hacerAsientoInputDesdeAplicacionesProductos(context.Background(), pp, 1, ctaGetterMock{})
		assert.ErrorIs(t, err, ErrFaltaPartidaAplicada)
	}
	{ // Partida con monto sin cantidad
		pp := []types.AplicacionProducto{
			{
				Cuenta:          cta,
				Producto:        uuid.FromStringOrNil("03d14fb6-918a-4255-8519-6156403e62bb"),
				SKU:             uuid.FromStringOrNil("03d14fb6-918a-4255-8519-6156403e62bb"),
				Monto:           3423,
				PartidaAplicada: uuid.FromStringOrNil("03d14fb6-918a-4255-8519-6156403e62bb"),
			},
		}
		out, err := hacerAsientoInputDesdeAplicacionesProductos(context.Background(), pp, 1, ctaGetterMock{})
		assert.Nil(t, err)
		assert.Len(t, out, 1)
	}
}
