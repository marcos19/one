package factura

import (
	"context"
	"fmt"
	"sort"
	"strings"

	"bitbucket.org/marcos19/one/pkg/afip/afipmodels"
	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/condiciones"
	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"bitbucket.org/marcos19/one/pkg/fact/config"
	"bitbucket.org/marcos19/one/pkg/fact/internal/asientos"
	"bitbucket.org/marcos19/one/pkg/fact/internal/asientos/estandar"
	"bitbucket.org/marcos19/one/pkg/fact/internal/asientos/partidasdirectas"
	"bitbucket.org/marcos19/one/pkg/fact/internal/asientos/pendientes"
	"bitbucket.org/marcos19/one/pkg/fact/internal/lookups"
	"bitbucket.org/marcos19/one/pkg/fact/internal/opprod"
	"bitbucket.org/marcos19/one/pkg/fact/internal/redondeo"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/iva"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/productos"
	"bitbucket.org/marcos19/one/pkg/productos/categorias"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/crosslogic/cuits"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
)

func Procesar(ctx context.Context,
	f *types.Fact,
	lu *lookups.Lookups,
	categoriaGetter categorias.CentroGetter,
	categoriaSuperior categorias.SuperiorGetter,
	cuentasGetter cuentas.ReaderOne,
	productosGetter productos.Getter,
	conn *pgxpool.Pool,
	unicIDgetter ops.UnicidadIDGetter,
) (err error) {

	// Llenar datos de fact.Productos
	if len(f.Productos) > 0 {
		err = llenarDatosProductos(
			ctx,
			f.Productos,
			f.Comitente,
			lu.Config.CategoriasIgnoradasEnFactura,
			lu.CompFactura.CalculaIVA,
			lu.Productos,
			lu.SKUs,
			categoriaGetter,
		)
		if err != nil {
			return errors.Wrap(err, "llenando datos partidas productos")
		}
	}

	// Llenar datos de fact.AplicacionesProductos
	if len(f.AplicacionesProductos) > 0 {
		err = llenarDatosProductosAplicaciones(
			ctx,
			f.AplicacionesProductos,
			f.Comitente,
			lu.Config.CategoriasIgnoradasEnFactura,
			lu.CompFactura.CalculaIVA,
			lu.Productos,
			lu.SKUs,
			categoriaGetter,
		)
		if err != nil {
			return errors.Wrap(err, "llenando datos partidas aplicaciones productos")
		}
	}

	// Partidas de factura
	partidasFactura, err := generarPartidas(ctx, f, lu, cuentasGetter, productosGetter, categoriaSuperior, conn, unicIDgetter)
	if err != nil {
		return errors.Wrap(err, "generando partidas de factura")
	}

	hayFactura := len(partidasFactura) > 0
	if hayFactura {
		log.Debug().Msg("Había factura")

		// Genero op
		err = generarOpFactura(f, lu)
		if err != nil {
			return errors.Wrap(err, "generando factura")
		}

		// Pongo las partidas contables
		f.Factura.PartidasContables = partidasFactura

		// Pongo los totales de conceptos
		if lu.Config.TotalizaDesdePartidasFact {
			f.Factura.TotalesConceptos, err = f.Totalizar()
			if err != nil {
				return errors.Wrap(err, "totalizando conceptos desde fact")
			}
			f.Factura.TotalComp = new(dec.D2)
			*f.Factura.TotalComp = f.Factura.TotalesConceptos.Sum()
		} else {
			f.Factura.TotalesConceptos, err = CalcularConceptos(f.Factura.PartidasContables, lu.Config.SignoRenglonProducto)
			if err != nil {
				return errors.Wrap(err, "calculando totales conceptos")
			}
			f.Factura.TotalComp = new(dec.D2)
			*f.Factura.TotalComp = f.Factura.TotalesConceptos.Sum()
		}

		// Hay condición de pago? => Parto por vencimientos
		if f.CondicionPago != nil {
			if lu.Condicion == nil {
				return errors.Errorf("lu.Condición era nil")
			}

			if f.VencimientosManuales {
				// Puede modificar vencimientos?
				if !lu.Condicion.Modificable {
					return fmt.Errorf("la configuración no permite modificar vencimientos")
				}
				// Puede modificar vencimientos?
				if f.Recibo != nil && len(f.Vencimientos) > 1 {
					return fmt.Errorf("no se pueden ingresar más de un vencimiento si la operación incluye pago")
				}
				// Opprod permite?
				id := 0
				if lu.Config.OpProd != nil {
					id = *lu.Config.OpProd
				}
				if !opProdPermiteVencimientos(id) && len(f.Vencimientos) > 1 {
					return fmt.Errorf("no se permiten más de un vencimiento por el tipo de operación")
				}
			}

			// Calculo vencimientos?
			if !f.VencimientosManuales {
				var err error
				f.Vencimientos, err = calcularVencimientosFactura(f.Factura.Fecha, *f.Factura.TotalComp, *lu.Condicion)
				if err != nil {
					return errors.Wrap(err, "calculando los vencimientos del comprobante")
				}
			}
		}

		// Poner ID a las partidas aplicadas vacías
		err = asientos.LlenarPartidasPatrimoniales(ctx, f.Comitente, f.Factura.PartidasContables, cuentasGetter)
		if err != nil {
			return errors.Wrap(err, "llenando campo PartidaAplicada en partidas patrimoniales de factura")
		}

		// Está tildado que no haga asiento?
		if lu.Config.NoHaceAsiento {
			f.Factura.PartidasContables = []ops.Partida{}
		}

		// Parto las partidas contables por fecha de vencimiento
		f.Factura.PartidasContables, err = asientos.PartirPartidasPatrimonialesPorVencimiento(
			f.Factura.PartidasContables,
			f.Vencimientos,
		)
		if err != nil {
			return errors.Wrap(err, "partiendo partidas patrimoniales")
		}
		// f.Factura.AsciiTable(os.Stdout)
	} else {
		f.Factura = nil
	}

	return
}

// Con los datos de types.Fact completa los datos de types.Fact.Factura.
// No hace nada con las partidas.
func generarOpFactura(f *types.Fact, lu *lookups.Lookups) error {

	// Valido
	if f.CondicionPago == nil && lu.Config.CondicionPago == config.CondicionPagoObligatoria {
		return ErrFaltaCondicionPago
	}
	if lu.CompFactura == nil {
		return errors.Errorf("comp no ingresado en lookup")
	}

	if f.Factura == nil {
		f.Factura = new(ops.Op)
	}
	op := ops.Op{}
	op.CompItem = new(int)
	*op.CompItem = types.ItemFactura
	op.NComp = f.Factura.NComp
	op.PuntoDeVenta = f.Factura.PuntoDeVenta
	op.AFIPComprobanteID = f.Factura.AFIPComprobanteID
	op.AFIPComprobanteNombre = f.Factura.AFIPComprobanteNombre
	op.FechaOriginal = f.Factura.FechaOriginal

	op.ID, _ = uuid.NewV1()
	op.TranID = f.ID
	op.Comitente = f.Comitente
	op.Empresa = f.Empresa
	op.Fecha = f.Fecha
	op.Programa = types.PkgID
	op.TranDef = f.Config
	op.Detalle = f.Detalle
	op.Usuario = lu.Usuario.ID

	op.Impresion = map[string]interface{}{}

	{ // Comp
		comp := lu.CompFactura
		op.CompNombre = comp.Nombre
		op.CompID = comp.ID

		// Es propio, fecha de original es igual a emisión
		if comp.PuntoDeVenta != nil {
			op.FechaOriginal = op.Fecha
		}

		// Obligatoriedad punto de venta
		puntoVentaDigitadoPorUsuario := comp.PuntoDeVenta == nil
		if puntoVentaDigitadoPorUsuario && op.PuntoDeVenta == 0 {
			obligaComp := comp.PuntoDeVentaObligatorio == comps.ObligatorioSi
			obligaAfip := false
			if op.AFIPComprobanteID != nil && comp.PuntoDeVentaObligatorio == comps.ObligatorioOriginal {
				if lu.CompAFIP == nil {
					return ErrComprobanteAFIPNoEncontrado
				}
				obligaAfip = false
				if lu.CompAFIP.PuntoVentaObligatorio != nil {
					obligaAfip = *lu.CompAFIP.PuntoVentaObligatorio
				}
			}
			if obligaComp || obligaAfip {
				return ErrFaltaPuntoDeVenta
			}
		}

		// A
		op.Impresion = tipos.JSON{}
		op.Impresion["LetraComp"] = comp.Letra

		if comp.ComprobanteFijo {
			if len(comp.OriginalesAceptados) != 1 {
				return errors.Errorf("el comp %v está definido como ComprobanteFijo, pero hay %v originales", comp.Nombre, len(comp.OriginalesAceptados))
			}
			op.AFIPComprobanteID = new(int)
			*op.AFIPComprobanteID = comp.OriginalesAceptados[0]
		}
		// Comp AFIP
		if op.AFIPComprobanteID != nil {
			op.Impresion["CodigoComp"] = fmt.Sprintf("Cod. %v", *op.AFIPComprobanteID)
		}
		op.Impresion["DiscriminaIVA"] = comp.DiscriminaIVA
	}

	{
		e := lu.Empresa
		op.Impresion["EmpresaNombre"] = e.ImpresionNombre
		if e.ImpresionNombreFantasia != "" {
			op.Impresion["EmpresaNombreFantasia"] = e.ImpresionNombreFantasia
		}
		op.Impresion["HeaderRenglonesLeft"] = e.ImpresionRenglonesLeft
		op.Impresion["HeaderRenglonesRight"] = e.ImpresionRenglonesRight

	}

	{ // Datos de la persona

		// ID
		op.Persona = new(uuid.UUID)
		*op.Persona = f.Persona.ID

		if lu.Persona.UsaSucursales && f.Persona.Sucursal == nil {
			return ErrFaltaSucursal
		}
		if f.Persona.Sucursal != nil {
			op.Sucursal = new(int)
			*op.Sucursal = *f.Persona.Sucursal
		}

		if lu.CompFactura.VaAlLibroDeIVA {
			if f.Persona.CondicionFiscal == 0 {
				return ErrFaltaCondicionFiscal
			}
			op.CondicionFiscal = new(afipmodels.CondicionIVA)
			*op.CondicionFiscal = f.Persona.CondicionFiscal
		}
		f.Persona.TipoIdentificacionFiscal = lu.Persona.TipoIdentificacionFiscal()
		f.Persona.NIdentificacionFiscal = lu.Persona.NIdentificacionFiscal()

		if lu.CompFactura.VaAlLibroDeIVA {
			if f.Persona.TipoIdentificacionFiscal == 0 {
				return ErrFaltaTipoIdentificacion
			}
			if f.Persona.NIdentificacionFiscal == 0 {
				return ErrFaltaNIdentificacion
			}
			if f.Persona.TipoIdentificacionFiscal != 80 && lu.CompFactura.Letra == "A" {
				return ErrFaltaCUIT
			}
			op.TipoIdentificacion = new(int)
			*op.TipoIdentificacion = f.Persona.TipoIdentificacionFiscal

			op.NIdentificacion = new(int)
			*op.NIdentificacion = f.Persona.NIdentificacionFiscal
		}
		// Cuit válido?
		switch f.Persona.TipoIdentificacionFiscal {
		case 80, 86, 87:
			if !cuits.CUIT(f.Persona.NIdentificacionFiscal).Valid() {
				return errors.Wrapf(ErrCUITInvalido, "%v", f.Persona.NIdentificacionFiscal)
			}
		}

		domicilio := f.Persona.Domicilio
		if lu.Persona.UsaSucursales {
			f.Persona.NombreSucursal = ""
			for _, v := range lu.Persona.Sucursales {
				if *v.ID == *f.Persona.Sucursal {
					f.Persona.NombreSucursal += " - " + v.Nombre
					domicilio = v.DomicilioString()
					break
				}
			}
		}
		op.PersonaNombre = f.Persona.Nombre
		op.Impresion["PersonaCamposLeft"] = []string{
			fmt.Sprintf("%v: %v", lu.PersonaIdentificacion.Nombre, f.Persona.NIdentificacionFiscal),
			fmt.Sprintf("Domicilio: %v", domicilio),
		}

		derecha := []string{
			fmt.Sprintf("Condición frente a IVA: %v", f.Persona.CondicionFiscal.Nombre()),
		}

		if f.CondicionPago != nil {
			derecha = append(derecha, fmt.Sprintf("Condición de pago: %v", lu.Condicion.Nombre))
		}
		op.Impresion["PersonaCamposRight"] = derecha
	}

	f.Factura = &op

	return nil
}

// Calculo asientos sobre:
//
//   - Partidas directas
//   - Partidas productos
//   - Partidas aplicadas
//
// A su vez, partidas de productos y aplicadas pueden o no generar tres tipos
// de asientos:
//
//   - Estandar: venta
//   - Costo: todavía no implementado
//   - Pendientes:
//
// Asiento común puede llevar IVA, el de pendientes No.
// Asiento común no puede ser monto cero, el de pendientes sí.
// El de costo tiene que determinar un costo para cada producto.
func generarPartidas(
	ctx context.Context,
	f *types.Fact,
	lu *lookups.Lookups,
	cuentasStore cuentas.ReaderOne,
	productosGetter productos.Getter,
	categoriasHandler categorias.SuperiorGetter,
	conn *pgxpool.Pool,
	unicIDGetter ops.UnicidadIDGetter,
) (out []ops.Partida, err error) {

	// Hay partidas para asientos?
	if len(f.Productos) == 0 && len(f.AplicacionesProductos) == 0 && len(f.PartidasDirectas) == 0 {
		return []ops.Partida{}, nil
	}

	if len(f.PartidasDirectas) > 0 { // Asiento de partidas directas
		pp, err := partidasdirectas.GenerarPartidas(
			ctx,
			f.Comitente,
			f.Persona,
			f.PartidasDirectas,
			lu.Config,
			*lu.CompFactura,
			cuentasStore,
		)
		if err != nil {
			return out, errors.Wrap(err, "generando asiento de partidas directas")
		}
		out = append(out, pp...)
	}

	factTieneProductos := len(f.Productos) > 0 || len(f.AplicacionesProductos) > 0
	configEsOpProd := lu.Config.OpProd != nil

	if factTieneProductos && !configEsOpProd {
		// Asientos de productos
		return out, errors.Errorf("la configuración no tiene determinada comportamiento")
	}

	if factTieneProductos && configEsOpProd { // Asientos de productos
		log.Debug().Msgf("Tiene productos: %v; config es opprod: %v", factTieneProductos, configEsOpProd)
		opp, err := opprod.PorID(int(*lu.Config.OpProd))
		if err != nil {
			return out, errors.Wrap(err, "determinando tipo de opprod")
		}

		// fact.Productos
		inputs := hacerAsientoInputDesdeProductos(f.Productos, f.Comitente)
		if err != nil {
			return out, errors.Wrap(err, "haciendo asiento input desde productos")
		}
		// fact.AplicacionesProductos
		apl, err := hacerAsientoInputDesdeAplicacionesProductos(ctx,
			f.AplicacionesProductos,
			f.Comitente,
			cuentasStore,
		)
		if err != nil {
			return out, errors.Wrap(err, "generando preasientos aplicación productos")
		}
		// Junto ambas
		inputs = append(inputs, apl...)

		// Hace asiento estándar?
		est, ok := opp.(opprod.RegistradorEstandar)
		if ok {

			log.Debug().Msgf("Corresponde asiento estándar")
			// Le lleno datos de unicidades
			ii, err := preAsientoEstandar(
				ctx,
				inputs,
				f.Comitente,
				conn,
			)
			if err != nil {
				return out, errors.Wrap(err, "generando preasientos estándar")
			}

			empresaInscriptaEnIVA := lu.Empresa.IVA == empresas.Inscripto
			pp, err := estandar.GenerarAsiento(
				ctx,
				f.Comitente,
				ii,
				f.Persona,
				f.Deposito,
				productosGetter,
				categoriasHandler,
				lu.CompFactura.CalculaIVA,
				lu.CompFactura.VaAlLibroDeIVA,
				empresaInscriptaEnIVA,
				est,
				lu.Config.Imputaciones,
				cuentasStore,
				unicIDGetter,
				lu.Config.UnicidadesQueSeCrean,
			)
			if err != nil {
				return out, errors.Wrap(err, "generando asiento estándar")
			}
			out = append(out, pp...)

			// Redondeo de IVA
			calculaIVA := lu.CompFactura.CalculaIVA
			haceAsiento := !lu.Config.NoHaceAsiento
			if haceAsiento && calculaIVA {
				out, err = redondeo.RedondearIVA(f.Productos, f.AplicacionesProductos, f.PartidasDirectas, lu.Config.SignoRenglonProducto, out, lu.Config.CuentaRedondeo)
				if err != nil {
					return out, errors.Wrap(err, "calculando partida de rendondeo")
				}
			}
		}

		// Hace asiento de pendientes?
		pend, ok := opp.(opprod.RegistradorPendientes)
		if ok {
			// En principio hace asiento de pendientes.
			// Analizo a que cuentas está aplicando:
			log.Debug().Msgf("Corresponde asiento pendientes (en principio). Es %v", pend)
			ii, err := preAsientoPendientes(ctx, inputs, cuentasStore, f.Comitente)
			if err != nil {
				return out, errors.Wrap(err, "generando preasientos de pendientes")
			}

			for _, v := range ii {
				ctaAplicada := 0
				if v.ConceptoProducto != nil {
					ctaAplicada = *v.ConceptoProducto
				}
				hace, err := pend.HaceAsientoPendientes(ctaAplicada)
				if err != nil {
					return out, errors.Wrapf(err, "determinando si correspondía hacer asiento de pendientes sobre cuenta aplicada %v", ctaAplicada)
				}
				if !hace {
					log.Debug().Int("ctaAplicada", ctaAplicada).Msg("Por la cuenta a la que aplica, no corresponde asiento pendiente")
					continue
				}
				pp, err := pendientes.GenerarAsiento(
					ctx,
					f.Comitente,
					v,
					f.Persona,
					f.Deposito,
					productosGetter,
					categoriasHandler,
					pend,
					lu.Config.Imputaciones,
					cuentasStore,
				)
				if err != nil {
					return out, errors.Wrap(err, "generando asiento de pendientes")
				}

				out = append(out, pp...)
			}
		}

		// Hace asiento de costo?

	}

	// Agrupo partidas
	out, err = asientos.AgruparPartidas(ctx, f.Comitente, out, cuentasStore)
	if err != nil {
		return out, errors.Wrap(err, "agrupando partidas")
	}

	return
}

// Calculo asientos sobre:
//
//  - Partidas directas
//  - Partidas productos
//  - Partidas aplicadas
//
// A su vez, partidas de productos y aplicadas pueden o no generar tres tipos
// de asientos:
//
//  - Renglón y patriomonial: venta
//  - Costo
//  - Pendientes
//
// Común puede llevar IVA, el de pendientes No.
// Común no puede ser monto cero, el de pendientes sí.
// El de costo tiene que determinar un costo para cada producto.
// Se decide si corresponde o no cada tipo de asiento si tiene imputación
// func generarPartidas(f *types.Fact, lu *lookups) (out []ops.Partida, err error) {

// 	return
// }

func preAsientoEstandar(
	ctx context.Context,
	pp []asientos.Input,
	comitente int,
	conn *pgxpool.Pool,
) (out []estandar.Input, err error) {

	tiposUnicidades := map[uuid.UUID]int{}
	{ // Determino el tipo de unicidades
		ids := []uuid.UUID{}
		for _, p := range pp {
			for _, u := range p.Unicidades {
				if u.Tipo != 0 {
					continue
				}
				ids = append(ids, u.ID)
			}
		}
		if len(ids) > 0 {
			query := fmt.Sprintf(`SELECT id, definicion FROM unicidades WHERE comitente = $1 AND id IN (%v)`, arrayUUID(ids))
			rows, err := conn.Query(ctx, query, comitente)
			if err != nil {
				return out, errors.Wrap(err, "querying unicidades")
			}
			defer rows.Close()
			for rows.Next() {
				id := uuid.UUID{}
				tipo := 0
				err = rows.Scan(&id, &tipo)
				if err != nil {
					return out, errors.Wrap(err, "escaneando unicidad tipo")
				}
				tiposUnicidades[id] = tipo
			}
		}
	}

	for _, v := range pp {
		inp := estandar.Input{}
		inp.Input = v

		// Pego unicidades referenciadas
		for i := range v.Unicidades {
			if v.Unicidades[i].Tipo == 0 {
				// Sólo a unicidades existentes, si era nueva ya llega del frontend con ID
				v.Unicidades[i].Tipo = tiposUnicidades[v.Unicidades[i].ID]
			}
		}

		out = append(out, inp)
	}
	return
}

// Por ahora no hace nada
func preAsientoPendientes(ctx context.Context, pp []asientos.Input, ctaGetter cuentas.ReaderOne, comitente int) (out []pendientes.Input, err error) {

	for _, v := range pp {
		o := pendientes.Input{}
		o.Input = v

		// cta, err := ctaGetter.ReadOne(ctx, cuentas.ReadOneReq{Comitente: comitente, ID: *v.CuentaAplicada})
		// if err != nil {
		// 	return out, errors.Wrapf(err, "determinando concepto producto de cuenta %v", *v.CuentaAplicada)
		// }
		// if cta.ConceptoProducto == nil {
		// 	return out, errors.Errorf("no se pudo determinar concepto de producto de cuenta %v", cta.Nombre)
		// }
		// o.CuentaAplicada = new(int)
		// *o.CuentaAplicada = *cta.ConceptoProducto
		out = append(out, o)
	}
	return
}

// Completa datos de los items de pp. Modifica el slice.
func llenarDatosProductosAplicaciones(
	ctx context.Context,
	pp []types.AplicacionProducto,
	comitente int,
	categoriasIgnoradasEnFactura tipos.GrupoInts,
	conIVA bool,
	prods map[uuid.UUID]productos.Producto,
	skus map[uuid.UUID]productos.SKU,
	categoriasHandler categorias.CentroGetter,
) (err error) {

	for i, v := range pp {

		// Busco SKU
		sku, ok := skus[v.SKU]
		if !ok {
			return errors.Errorf("no se encontró SKU en lu")
		}
		pp[i].Producto = sku.Producto
		if sku.UM != nil {
			pp[i].UM = *sku.UM
		}

		// Busco producto
		prod, ok := prods[sku.Producto]
		if !ok {
			return errors.Errorf("no se encontró Producto en lu")
		}

		// Pego centro
		pp[i].Centro, err = categoriasHandler.Centro(ctx, categorias.ReadOneReq{
			Comitente: comitente,
			ID:        prod.Categoria,
		})
		if err != nil {
			return errors.Wrap(err, "determinando centro del producto")
		}

		// Pego datos de producto
		pp[i].Codigo = sku.Codigo
		if conIVA {
			pp[i].AlicuotaIVA = new(iva.Alicuota)
			*pp[i].AlicuotaIVA = prod.AlicuotaIVA
		}

		// Cuenta de terceros?
		for _, v := range categoriasIgnoradasEnFactura {
			if prod.Categoria == v {
				pp[i].PorCuentaTerceros = true
				break
			}
		}
	}
	return
}

// Completa datos de los items de pp. Modifica el slice.
func llenarDatosProductos(
	ctx context.Context,
	pp []types.PartidaProducto,
	comitente int,
	categoriasIgnoradasEnFactura tipos.GrupoInts,
	conIVA bool,
	prods map[uuid.UUID]productos.Producto,
	skus map[uuid.UUID]productos.SKU,
	categoriasHandler categorias.CentroGetter,
) (err error) {
	for i, v := range pp {

		// Busco SKU
		sku, ok := skus[v.SKU]
		if !ok {
			return errors.Errorf("no se encontró SKU en lu")
		}
		pp[i].Producto = sku.Producto
		if sku.UM != nil {
			pp[i].UM = *sku.UM
		}

		// Busco producto
		prod, ok := prods[sku.Producto]
		if !ok {
			return errors.Errorf("no se encontró Producto en lu")
		}

		// Pego centro
		pp[i].Centro, err = categoriasHandler.Centro(ctx, categorias.ReadOneReq{
			Comitente: comitente,
			ID:        prod.Categoria,
		})
		if err != nil {
			return errors.Wrap(err, "determinando centro del producto")
		}

		// Pego datos de producto
		pp[i].Codigo = sku.Codigo
		if conIVA {
			pp[i].AlicuotaIVA = prod.AlicuotaIVA
		}

		// Cuenta de terceros?
		for _, v := range categoriasIgnoradasEnFactura {
			if prod.Categoria == v {
				pp[i].CuentaDeTerceros = true
				break
			}
		}
	}
	return
}

func calcularVencimientosFactura(f fecha.Fecha, monto dec.D2, cond condiciones.Condicion) (vtos []types.Vencimiento, err error) {

	// Se entiende que es 100% de contado
	if len(cond.Cuotas) == 0 {

		vto := types.Vencimiento{}
		vto.Monto = monto
		vto.Fecha = f
		vtos = append(vtos, vto)
		return
	}

	// Si hay vencimientos definidos
	acum := dec.D2(0)
	for i, v := range cond.Cuotas {
		vto := types.Vencimiento{}
		vto.Monto = dec.NewD2(monto.Float() * v.Porcentaje / 100)
		esUltimaCuota := len(cond.Cuotas) == i+1
		if esUltimaCuota {
			vto.Monto = monto - acum
		}

		if v.FechaFija != nil {
			vto.Fecha = *v.FechaFija
			vto.Dias = f.Menos(vto.Fecha)
		} else {
			vto.Fecha = f.AgregarDias(v.Dias)
			vto.Dias = v.Dias
		}

		acum += vto.Monto
		vtos = append(vtos, vto)
	}

	return
}

// Caclula los totales de neto, iva y demás conceptos que van en una factura.
// Tiene que ser esPataQueSumaEnFactura
func CalcularConceptos(pp []ops.Partida, signo int) (out ops.TotalesConceptos, err error) {

	if signo == 0 {
		return out, errors.Errorf("no se definió signo operación")
	}
	type key struct {
		concepto iva.Concepto
		alicuota iva.Alicuota
	}

	m := map[key]dec.D2{}

	// TOTALES a partir de partidas
	sum := dec.D2(0)
	for _, v := range pp {
		if !esPataQueSumaEnFactura(v.Pata) {
			continue
		}
		key := key{}
		if v.ConceptoIVA != nil {
			key.concepto = *v.ConceptoIVA
		}
		if v.AlicuotaIVA != nil {
			key.alicuota = *v.AlicuotaIVA
		}
		sum += v.Monto
		m[key] += v.Monto
	}

	for k, v := range m {
		c := ops.TotalConcepto{
			Concepto: k.concepto,
			Alic:     k.alicuota,
			Nombre:   iva.NombreCombinado(k.concepto, k.alicuota),
			Monto:    v * dec.D2(signo),
		}
		out = append(out, c)
	}

	// Los ordeno
	sort.Slice(out, func(i, j int) bool {
		if out[i].Concepto != out[j].Concepto {
			return out[i].Concepto < out[j].Concepto
		}
		return out[i].Alic < out[j].Alic
	})

	return
}

func opProdPermiteVencimientos(id int) bool {
	switch id {
	case opprod.FacturaCompraDirectaID:
		return true
	case opprod.FacturaCompraID:
		return true
	case opprod.FacturaVentaID:
		return true
	case opprod.FacturaVentaDirectaID:
		return true
	}
	return false
}

// Para totalizar los conceptos que van en el cuadrito abajo, recorro
// las partidas contables. Esta función me dice aquellos conceptos que
// deben ir sumados.
func esPataQueSumaEnFactura(p string) bool {
	switch p {
	case types.PataProducto, types.PataPartidaDirecta, types.PataIVA, types.PataReingreso:
		return true
	}
	return false
}

func arrayUUID(ids []uuid.UUID) string {
	idsStr := []string{}
	for _, v := range ids {
		idsStr = append(idsStr, fmt.Sprintf("'%v'", v.String()))
	}
	return strings.Join(idsStr, ", ")
}
