package factura

import (
	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/pkg/errors"
)

// Errores que no deberían dar
var ErrComprobanteAFIPNoEncontrado = errors.Errorf("no se encontró comprobante AFIP en lookup")
var ErrCuentaNoDefinida = errors.Errorf("falta cuenta contable")
var ErrProductoNoDefinido = errors.Errorf("falta producto ID")
var ErrSKUNoDefinido = errors.Errorf("falta SKU")
var ErrNiMontoNiCantidad = errors.Errorf("no se definió monto ni cantidad")
var ErrFaltaUM = errors.Errorf("falta unidad de medida")
var ErrFaltaPartidaAplicada = errors.Errorf("no se definió el ID de la partida a aplicar")

// Validación para el usuario final
var ErrFaltaCondicionPago = deferror.Validation("No se definió condición de pago")
var ErrFaltaCondicionFiscal = deferror.Validation("No se definió condición fiscal de la persona")
var ErrFaltaComprobanteAFIP = deferror.Validation("No se determinó el código de comprobante AFIP")
var ErrFaltaPuntoDeVenta = deferror.Validation("No se determinó punto de venta")
var ErrFaltaSignoFactura = deferror.Validation("La configuración no tiene definida el signo que debe utilizarse")
var ErrFaltaSucursal = deferror.Validation("La persona utiliza sucursales, pero no se determinó ninguna")
var ErrFaltaTipoIdentificacion = deferror.Validation("El comprobante es fiscal pero no se definió el tipo de identificación de la persona")
var ErrFaltaNIdentificacion = deferror.Validation("El comprobante es fiscal pero no se definió el número de identificación de la persona")
var ErrFaltaCUIT = deferror.Validation("Para comprobantes 'A' es obligatorio el número de CUIT")
var ErrCUITInvalido = deferror.Validationf("El CUIT es inválido")
