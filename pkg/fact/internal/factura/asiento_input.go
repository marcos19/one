package factura

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/fact/internal/asientos"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
	"github.com/rs/zerolog/log"
)

// A partir de fact.Productos genera la struct asientos.Input.
func hacerAsientoInputDesdeProductos(
	pp []types.PartidaProducto,
	comitente int,
) (out []asientos.Input) {

	for _, v := range pp {
		inp := asientos.Input{
			Producto:          v.Producto,
			SKU:               v.SKU,
			Codigo:            v.Codigo,
			UM:                v.UM,
			Cantidad:          v.Cantidad.Float(),
			Neto:              v.MontoTotal.Float(),
			Detalle:           v.ProductoDetalle,
			PorCuentaTerceros: v.CuentaDeTerceros,
			Centro:            v.Centro,
			AlicuotaIVA:       v.AlicuotaIVA,
		}
		for _, unic := range v.Unicidades {
			u := asientos.Unicidad{
				ID:      unic.ID,
				Detalle: unic.Detalle,
				Monto:   dec.NewD2(v.MontoTotal.Float() / float64(len(v.Unicidades))),
			}

			inp.Unicidades = append(inp.Unicidades, u)
		}
		for _, unic := range v.UnicidadesNuevas {
			u := asientos.Unicidad{
				ID:              unic.ID,
				PartidaAplicada: unic.ID,
				Tipo:            unic.Definicion,
				Monto:           dec.NewD2(v.MontoTotal.Float() / float64(len(v.UnicidadesNuevas))),
			}
			inp.Unicidades = append(inp.Unicidades, u)
		}
		out = append(out, inp)
	}

	return
}

// A partir de fact.AplicacionesProductos genera la struct asientos.Input.
func hacerAsientoInputDesdeAplicacionesProductos(
	ctx context.Context,
	pp []types.AplicacionProducto,
	comitente int,
	ctasGetter cuentas.ReaderOne,
) (out []asientos.Input, err error) {

	for _, v := range pp {
		inp := asientos.Input{
			Producto:          v.Producto,
			SKU:               v.SKU,
			Codigo:            v.Codigo,
			UM:                v.UM,
			Cantidad:          v.Cantidad.Float(),
			Neto:              v.Monto.Float(),
			PorCuentaTerceros: v.PorCuentaTerceros,
			Centro:            v.Centro,
			PartidaAplicada:   v.PartidaAplicada,
		}

		// Valido
		if v.Cuenta == 0 {
			return out, errors.Wrap(ErrCuentaNoDefinida, "haciendo input de aplicación producto")
		}
		if v.Producto == uuid.Nil {
			return out, errors.Wrap(ErrProductoNoDefinido, "haciendo input de aplicación producto")
		}
		if v.SKU == uuid.Nil {
			return out, errors.Wrap(ErrSKUNoDefinido, "haciendo input de aplicación producto")
		}
		if v.Cantidad == 0 && v.Monto == 0 {
			return out, errors.Wrap(ErrNiMontoNiCantidad, "haciendo input de aplicación producto")
		}
		if v.Cantidad != 0 && v.UM == "" {
			return out, errors.Wrap(ErrFaltaUM, "haciendo input de aplicación producto")
		}
		if v.PartidaAplicada == uuid.Nil {
			return out, errors.Wrap(ErrFaltaPartidaAplicada, "haciendo input de aplicación producto")
		}

		{ // Concepto producto
			cta, err := ctasGetter.ReadOne(ctx, cuentas.ReadOneReq{Comitente: comitente, ID: v.Cuenta})
			if err != nil {
				return out, errors.Wrap(err, "buscando cuenta aplicada")
			}
			if cta.ConceptoProducto == nil {
				return out, errors.Errorf("cuenta contable %v no tiene definida concepto opprod", cta.Nombre)
			}
			if *cta.ConceptoProducto == 0 {
				return out, errors.Errorf("cuenta contable %v no tiene definida concepto opprod", cta.Nombre)
			}
			inp.ConceptoProducto = new(int)
			*inp.ConceptoProducto = *cta.ConceptoProducto
			log.Debug().Msgf("Haciendo input desde aplicaciones productos: a partir de cuenta %v definimos ConceptoProducto: %v", v.Cuenta, *inp.ConceptoProducto)
		}

		if v.AlicuotaIVA != nil {
			inp.AlicuotaIVA = *v.AlicuotaIVA
		}

		out = append(out, inp)
	}

	return
}
