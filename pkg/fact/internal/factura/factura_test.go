package factura

import (
	"context"
	"fmt"
	"testing"

	"bitbucket.org/marcos19/one/pkg/afip/afipmodels"
	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/condiciones"
	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"bitbucket.org/marcos19/one/pkg/fact/config"
	"bitbucket.org/marcos19/one/pkg/fact/internal/lookups"
	"bitbucket.org/marcos19/one/pkg/fact/internal/opprod"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/iva"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/personas"
	"bitbucket.org/marcos19/one/pkg/productos"
	"bitbucket.org/marcos19/one/pkg/productos/categorias"
	"bitbucket.org/marcos19/one/pkg/unicidadesdef"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/davecgh/go-spew/spew"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

// Tipos varios para probar:
// - Factura:
//	- Partidas productos
//  - Partidas aplicadas
//  - No hace asiento

func TestGenerarOpFactura(t *testing.T) {
	var err error
	// require := require.New(t)
	assert := assert.New(t)

	f := &types.Fact{}
	lu := &lookups.Lookups{}
	lu.Config.CondicionPago = config.CondicionPagoObligatoria

	// Falta comp en lookup
	err = generarOpFactura(f, lu)
	assert.NotNil(err)

	// Pongo comp
	lu.CompFactura = &comps.Comp{}

	// Falta condicion pago
	err = generarOpFactura(f, lu)
	assert.ErrorIs(err, ErrFaltaCondicionPago)

	// Falta condicion pago, pero no es obligatoria
	lu.Config.CondicionPago = config.CondicionPagoSinCondicion
	err = generarOpFactura(f, lu)
	assert.Nil(err)

	// Falta condicion pago, pero no es obligatoria
	lu.Config.CondicionPago = config.CondicionPagoOpcional
	err = generarOpFactura(f, lu)
	assert.Nil(err)

	{ // Con comprobante fiscal
		lu.CompFactura.VaAlLibroDeIVA = true
		err = generarOpFactura(f, lu)
		assert.ErrorIs(err, ErrFaltaCondicionFiscal)

		// Pongo condicion fiscal, falta numero id persona
		f.Persona.CondicionFiscal = afipmodels.CondicionResponsableInscripto
		err = generarOpFactura(f, lu)
		assert.NotErrorIs(err, ErrFaltaCondicionFiscal)
	}
	{ // Nümero identificacion persona
		err = generarOpFactura(f, lu)
		assert.ErrorIs(err, ErrFaltaNIdentificacion)
		lu.Persona.TipoDNI = new(int)
		*lu.Persona.TipoDNI = 96
		lu.Persona.DNI = 32889647
		err = generarOpFactura(f, lu)
		assert.NotErrorIs(err, ErrFaltaNIdentificacion)
	}

	{ // Debe exigir CUIT
		lu.CompFactura.Letra = "A"
		err = generarOpFactura(f, lu)
		require.NotNil(t, err)
		// pongo el cuit a la persona en el backend
		lu.Persona.CUIT = 20328896479
		lu.Persona.TipoCUIT = new(int)
		*lu.Persona.TipoCUIT = 80
		err = generarOpFactura(f, lu)
		assert.NotErrorIs(err, ErrFaltaCUIT)
	}
	{ // CUIT válido?
		lu.Persona.CUIT = 20328896477
		err = generarOpFactura(f, lu)
		assert.ErrorIs(err, ErrCUITInvalido)
		// lo corrijo
		lu.Persona.CUIT = 20328896479
		err = generarOpFactura(f, lu)
		assert.NotErrorIs(err, ErrCUITInvalido)
	}

	assert.Nil(err)

}

const CuentaClientes = 1200
const CuentaProductos = 1300
const CuentaVentas = 9001
const CuentaIVA = 2202

func TestGenerarPartidas(t *testing.T) {

	t.Run("Partida vacía", func(t *testing.T) { // Fact vacía, pasa de largo.
		f := &types.Fact{}
		lu := &lookups.Lookups{}
		_, err := generarPartidas(context.Background(), f, lu, ctaGetterMock{}, prodGetterMock{}, superiorGetterMock{}, nil, &unicIDmock{})
		assert.Nil(t, err)
	})

	t.Run("Partida producto. Con IVA, partida con monto cero", func(t *testing.T) {
		f := &types.Fact{}
		f.Comitente = 1
		f.Persona.ID = uuid.FromStringOrNil("f870d363-5181-487b-985d-a99a4f4b0b5d")

		// Partida producto válida.
		p := types.PartidaProducto{}
		p.Producto = uuid.FromStringOrNil("f870d363-5181-487b-984d-a99a4f4b0b33")
		p.SKU = uuid.FromStringOrNil("f870d363-5181-487b-984d-a99a4f4b0b34")
		p.Cantidad = dec.NewD4(1)
		p.AlicuotaIVA = iva.IVA21
		p.UM = "L"

		f.Productos = append(f.Productos, p)
		lu := &lookups.Lookups{}
		lu.Empresa.IVA = empresas.Inscripto
		lu.Config = config.Config{}
		lu.Config.OpProd = new(int)
		lu.Config.SignoRenglonProducto = -1

		lu.CompFactura = &comps.Comp{}
		lu.CompFactura.CalculaIVA = true
		lu.CompFactura.VaAlLibroDeIVA = true
		lu.CompFactura.ID = 87

		*lu.Config.OpProd = opprod.FacturaVentaDirectaID
		lu.Config.Imputaciones = append(lu.Config.Imputaciones,
			config.ImputacionContable{
				Clientes:  CuentaClientes,
				Productos: CuentaProductos,
				IVA:       CuentaIVA,
				Ventas:    CuentaVentas,
			},
		)

		lu.Categorias = map[int]categorias.Categoria{}
		{
			n := new(categorias.Categoria)
			n.ID = new(int)
			*n.ID = 34
			n.Nombre = "Naftas"
			lu.Categorias[34] = *n
		}
		{
			n := new(categorias.Categoria)
			n.ID = new(int)
			*n.ID = 35
			n.Nombre = "Combustibles"
			lu.Categorias[35] = *n
		}

		out, err := generarPartidas(
			context.Background(),
			f,
			lu,
			ctaGetterMock{},
			prodGetterMock{},
			superiorGetterMock{},
			nil,
			&unicIDmock{},
		)
		require.Nil(t, err)
		require.Len(t, out, 2, spew.Sdump(out))
	})

	t.Run("Partida producto. Con IVA, partida con monto", func(t *testing.T) {
		f := &types.Fact{}
		f.Comitente = 1
		f.Persona.ID = uuid.FromStringOrNil("f870d363-5181-487b-985d-a99a4f4b0b5d")

		// Partida producto válida.
		p := types.PartidaProducto{}
		p.Producto = uuid.FromStringOrNil("f870d363-5181-487b-984d-a99a4f4b0b33")
		p.SKU = uuid.FromStringOrNil("f870d363-5181-487b-984d-a99a4f4b0b34")
		p.Cantidad = dec.NewD4(1)
		p.AlicuotaIVA = iva.IVA21
		p.UM = "L"
		p.MontoTotal = dec.NewD4(500)

		f.Productos = append(f.Productos, p)
		lu := &lookups.Lookups{}
		lu.Empresa.IVA = empresas.Inscripto
		lu.Config = config.Config{}
		lu.Config.OpProd = new(int)
		lu.Config.SignoRenglonProducto = -1

		lu.CompFactura = &comps.Comp{}
		lu.CompFactura.CalculaIVA = true
		lu.CompFactura.VaAlLibroDeIVA = true
		lu.CompFactura.ID = 87

		*lu.Config.OpProd = opprod.FacturaVentaDirectaID
		lu.Config.Imputaciones = append(lu.Config.Imputaciones,
			config.ImputacionContable{
				Clientes:  CuentaClientes,
				Productos: CuentaProductos,
				IVA:       CuentaIVA,
				Ventas:    CuentaVentas,
			},
		)

		lu.Categorias = map[int]categorias.Categoria{}
		{
			n := new(categorias.Categoria)
			n.ID = new(int)
			*n.ID = 34
			n.Nombre = "Naftas"
			lu.Categorias[34] = *n
		}
		{
			n := new(categorias.Categoria)
			n.ID = new(int)
			*n.ID = 35
			n.Nombre = "Combustibles"
			lu.Categorias[35] = *n
		}

		out, err := generarPartidas(
			context.Background(),
			f,
			lu,
			ctaGetterMock{},
			prodGetterMock{},
			superiorGetterMock{},
			nil,
			&unicIDmock{},
		)
		require.Nil(t, err)
		require.Len(t, out, 3, spew.Sdump(out))

		{ // Partida renglón
			idx := 2
			assert.Equal(t, CuentaVentas, out[idx].Cuenta, spew.Sdump(out[idx]))
			assert.Equal(t, types.PataProducto, out[idx].Pata, spew.Sdump(out[idx]))
			require.NotNil(t, out[idx].Cantidad)
			assert.Equal(t, dec.NewD4(-1), *out[idx].Cantidad, spew.Sdump(out[idx]))
			assert.Equal(t, dec.NewD2(-500), out[idx].Monto, spew.Sdump(out[idx]))
		}
		{ // Partida IVA
			idx := 1
			assert.Equal(t, CuentaIVA, out[idx].Cuenta, spew.Sdump(out[idx]))
			assert.Equal(t, types.PataIVA, out[idx].Pata, spew.Sdump(out[idx]))
			assert.Nil(t, out[idx].Cantidad, spew.Sdump(out[idx]))
			assert.Equal(t, dec.NewD2(-105), out[idx].Monto, spew.Sdump(out[idx]))
		}
		{ // Partida Patrimonial
			idx := 0
			assert.Equal(t, CuentaClientes, out[idx].Cuenta, spew.Sdump(out[idx]))
			assert.Equal(t, types.PataFacturaPatrimonial, out[idx].Pata, spew.Sdump(out[idx]))
			assert.Nil(t, out[idx].Cantidad, spew.Sdump(out[idx]))
			assert.Equal(t, dec.NewD2(605), out[idx].Monto, spew.Sdump(out[idx]))
		}
	})
}

type prodGetterMock struct{}

func (prodGetterMock) ReadOne(context.Context, productos.ReadOneReq) (out productos.Producto, err error) {
	return productos.Producto{
		ID:        uuid.FromStringOrNil("0130e045-7310-455b-bad6-ba0a9aae30e9"),
		Nombre:    "Nafta super",
		Categoria: 123,
	}, nil
}

type superiorGetterMock struct{}

func (superiorGetterMock) CategoriaSuperior(ctx context.Context, req categorias.ReadOneReq) (out *categorias.Categoria, err error) {
	switch req.ID {
	case 123:
		out = new(categorias.Categoria)
		out.ID = new(int)
		*out.ID = 34
		out.Nombre = "Naftas"
		return

	case 124:
		out = new(categorias.Categoria)
		out.ID = new(int)
		*out.ID = 35
		out.Nombre = "Combustibles"
		return
	}
	return
}

func TestPartidasDirectas(t *testing.T) {
	// Conceptos
	var gravado = new(iva.Concepto)
	*gravado = 1
	var a105 = new(iva.Alicuota)
	*a105 = iva.IVA10yMedio
	var a21 = new(iva.Alicuota)
	*a21 = iva.IVA21

	f := types.Fact{}
	f.PartidasDirectas = []types.PartidaDirecta{
		{
			CuentaID:          2,
			CuentaPatrimonial: 10,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA21,
			Monto:             dec.NewD4(9752.0661),
		},
		{
			CuentaID:          2,
			CuentaPatrimonial: 10,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA21,
			Monto:             dec.NewD4(9752.0661),
		},
		{
			CuentaID:          2,
			CuentaPatrimonial: 10,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA10yMedio,
			Monto:             dec.NewD4(9752.0661),
		},
		{
			CuentaID:          1,
			CuentaPatrimonial: 11,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA21,
			Monto:             dec.NewD4(9752.0661),
		},
	}

	lu := lookups.Lookups{}
	// ctasGetter := ctaGetterMock{}
	lu.Centros = map[int]int{}
	lu.Personas = map[uuid.UUID]personas.Persona{}
	lu.Productos = map[uuid.UUID]productos.Producto{}
	lu.UnicidadesDef = map[int]unicidadesdef.Def{}

	lu.Config.SignoRenglonProducto = -1
	lu.Config.Imputaciones = []config.ImputacionContable{
		{
			IVA: 21,
		},
	}
	lu.CompFactura = new(comps.Comp)
	lu.CompFactura.CalculaIVA = true
	lu.CompFactura.DiscriminaIVA = true
	lu.CompFactura.VaAlLibroDeIVA = true
	lu.CompFactura.LibroIVA = "Compras"
	cond := afipmodels.CondicionResponsableInscripto
	lu.Persona.CUIT = 20328896479
	// lu.Persona.TipoCUIT =80

	f.Factura = &ops.Op{}
	f.Persona.CondicionFiscal = cond
	f.Persona.ID = uuid.FromStringOrNil("7259e4ea-a010-450d-bdc1-55835727774e")
	f.Persona.Sucursal = new(int)
	*f.Persona.Sucursal = 349058743
	f.Factura.NIdentificacion = &f.Persona.NIdentificacionFiscal
	f.Factura.TipoIdentificacion = &f.Persona.TipoIdentificacionFiscal
	// err := generarFactura(&f, &lu)
	// assert.Nil(t, err)

	assert.Equal(t, f.Ops()[0].TotalDebe(), -f.Ops()[0].TotalHaber())
}

func TestCalcularVencimientosFactura(t *testing.T) {

	assert := assert.New(t)

	cond := condiciones.Condicion{}
	cond.Cuotas = condiciones.Cuotas{
		{
			Dias:       30,
			Porcentaje: 33.333333333,
		},
		{
			Dias:       60,
			Porcentaje: 33.333333333,
		},
		{
			Dias:       90,
			Porcentaje: 33.333333333,
		},
	}
	out, err := calcularVencimientosFactura(20210101, dec.NewD2(100), cond)
	assert.Nil(err)
	assert.Len(out, 3)
	if len(out) == 3 {

		// Cuota 1
		assert.Equal(fecha.Fecha(20210131), out[0].Fecha)
		assert.Equal(dec.NewD2(33.33), out[0].Monto)

		// Cuota 2
		assert.Equal(fecha.Fecha(20210302), out[1].Fecha)
		assert.Equal(dec.NewD2(33.33), out[1].Monto)

		// Cuota 3
		assert.Equal(fecha.Fecha(20210401), out[2].Fecha)
		assert.Equal(dec.NewD2(33.34), out[2].Monto)
	}
}

func TestCalcularConceptos(t *testing.T) {

	assert := assert.New(t)
	require := require.New(t)

	bi := iva.BaseImponible
	iv := iva.IVA
	ng := iva.NoGravado

	alic21 := iva.IVA21
	alicng := iva.IVANoGravado

	{ // Con signo positivo
		pp := []ops.Partida{
			{
				ConceptoIVA: &bi,
				AlicuotaIVA: &alic21,
				Pata:        types.PataProducto,
				Monto:       dec.NewD2(-100),
			},
			{
				ConceptoIVA: &iv,
				AlicuotaIVA: &alic21,
				Pata:        types.PataIVA,
				Monto:       dec.NewD2(-21),
			},
			{
				ConceptoIVA: &ng,
				AlicuotaIVA: &alicng,
				Pata:        types.PataPartidaDirecta,
				Monto:       dec.NewD2(2),
			},
		}

		tt, err := CalcularConceptos(pp, config.SignoFacturaVenta)
		require.Nil(err)
		require.Len(tt, 3)
		assert.Equal(tt[0].Monto, dec.NewD2(100))
		assert.Equal(tt[1].Monto, dec.NewD2(21))
		assert.Equal(tt[2].Monto, dec.NewD2(-2))
	}

	{ // Con signo positivo
		pp := []ops.Partida{
			{
				ConceptoIVA: &bi,
				AlicuotaIVA: &alic21,
				Pata:        types.PataProducto,
				Monto:       dec.NewD2(100),
			},
			{
				ConceptoIVA: &iv,
				AlicuotaIVA: &alic21,
				Pata:        types.PataIVA,
				Monto:       dec.NewD2(21),
			},
			{
				ConceptoIVA: &ng,
				AlicuotaIVA: &alicng,
				Pata:        types.PataPartidaDirecta,
				Monto:       dec.NewD2(-2),
			},
		}

		tt, err := CalcularConceptos(pp, config.SignoFacturaCompra)
		require.Nil(err)
		require.Len(tt, 3)
		assert.Equal(tt[0].Monto, dec.NewD2(100))
		assert.Equal(tt[1].Monto, dec.NewD2(21))
		assert.Equal(tt[2].Monto, dec.NewD2(-2))
	}
}

type ctaGetterMock struct{}

func (c ctaGetterMock) PorFuncionContable(ctx context.Context, comitente int, f cuentas.Funcion) ([]cuentas.Cuenta, error) {
	return nil, nil
}
func (c ctaGetterMock) ReadOne(ctx context.Context, req cuentas.ReadOneReq) (cuentas.Cuenta, error) {
	switch req.ID {
	case 1:
		return cuentas.Cuenta{ID: req.ID, Nombre: "Gastos varios"}, nil
	case 2:
		return cuentas.Cuenta{ID: req.ID, Nombre: "Herramientas"}, nil
	case 10:
		return cuentas.Cuenta{ID: req.ID, Nombre: "Proveedores varios", UsaAplicaciones: true}, nil
	case 11:
		return cuentas.Cuenta{ID: req.ID, Nombre: "Proveedores bs uso", UsaAplicaciones: true}, nil
	case 21:
		return cuentas.Cuenta{ID: req.ID, Nombre: "IVA"}, nil

	case 100:
		conc := opprod.ProductosSolicitadosARecibir
		return cuentas.Cuenta{ID: 100, Nombre: "Productos", ConceptoProducto: &conc}, nil

	case CuentaIVA:
		return cuentas.Cuenta{ID: req.ID, Nombre: "IVA"}, nil
	case CuentaVentas:
		return cuentas.Cuenta{ID: req.ID, Nombre: "Ventas", AperturaProducto: true}, nil
	case CuentaClientes:
		return cuentas.Cuenta{ID: req.ID,
			Nombre:          "Clientes",
			UsaAplicaciones: true,
			AperturaPersona: true,
		}, nil
	case CuentaProductos:
		return cuentas.Cuenta{
			ID:               req.ID,
			Nombre:           "Productos",
			AperturaProducto: true,
		}, nil
	}
	panic(fmt.Sprintf("mock de cuenta %v not implemented", req.ID))
}

type unicIDmock struct {
	err bool
}

func (m *unicIDmock) GetPartidaAplicada(context.Context, int, uuid.UUID) (uuid.UUID, error) {
	if m.err {
		return uuid.UUID{}, errors.Errorf("unic id mock error")
	}
	return uuid.FromStringOrNil("ae7741a0-0aa0-11ec-ae9a-42010a8e0002"), nil
}
