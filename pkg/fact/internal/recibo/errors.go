package recibo

import (
	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
)

// Errores que no deberían dar
var ErrConfigNoDefinida = errors.Errorf("falta config ID")
var ErrEsquemaNoDefinido = errors.Errorf("falta esquema ID")
var ErrUsuarioNoDefinido = errors.Errorf("falta usuario ID")
var ErrNoHayPartidas = errors.Errorf("no se ingresaron partidas contables")
var ErrCompReciboNoDefinido = errors.Errorf("no se ingresó comp a utilizar en recibo")
var ErrFaltaCuentaContable = errors.Errorf("no se ingresó cuenta contable")
var ErrFaltaID = errors.Errorf("no se ingresó ID")

// Validación para el usuario final
var ErrFaltaFechaContable = deferror.Validation("No se determinó fecha de operación")
var ErrFaltaSignoValor = deferror.Validation("La configuración no tiene definida el signo que debe utilizarse")
var ErrFaltaSucursal = deferror.Validation("La persona utiliza sucursales, pero no se determinó ninguna")
var ErrFaltaMonto = deferror.Validationf("No se ingresó monto")
var ErrFaltaCaja = deferror.Validationf("No se ingresó caja")
var ErrFaltaPersona = deferror.Validationf("No se ingresó persona")
var ErrFaltaMontoOriginalYTC = deferror.Validationf("Si usa moneda extranjera debe definir tipo de cambio o el monto en moneda original")
var ErrFaltaMontoOriginal = deferror.Validationf("Monto moneda original no puede ser cero")
var ErrFaltaMoneda = deferror.Validationf("No se definió moneda")
var ErrFaltaTipoCambio = deferror.Validationf("No se definió tipo de cambio")
var ErrMontoNegativo = deferror.Validationf("Monto no puede ser negativo")
var ErrFaltaUnicidad = deferror.Validationf("No se definió unicidad")
