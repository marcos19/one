package recibo

import (
	"context"
	"testing"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/fact/internal/lookups"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/personas"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"bitbucket.org/marcos19/one/pkg/unicidades"
	"bitbucket.org/marcos19/one/pkg/unicidadesdef"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/davecgh/go-spew/spew"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestGenerarRecibo(t *testing.T) {
	// require := require.New(t)
	assert := assert.New(t)
	var err error
	f := &types.Fact{}
	lu := &lookups.Lookups{}
	pp := []ops.Partida{}
	err = GenerarOp(f, pp, lu)

	{ // ID
		prevErr := err
		assert.ErrorContains(err, "falta ID")
		// Corrijo
		f.ID, _ = uuid.NewV1()
		err = GenerarOp(f, pp, lu)
		// Tiene que dar otro error
		assert.NotErrorIs(err, prevErr)
	}

	{ // Comitente
		prevErr := err
		assert.ErrorIs(err, deferror.ComitenteIndefinido())
		// Corrijo
		f.Comitente = 1
		err = GenerarOp(f, pp, lu)
		// Tiene que dar otro error
		assert.NotErrorIs(err, prevErr, spew.Sdump("%v", f))
	}

	{ // Empresa
		assert.ErrorIs(err, deferror.EmpresaIndefinida())
		// Corrijo
		f.Empresa = 1
		err = GenerarOp(f, pp, lu)
		// Tiene que dar otro error
		assert.NotErrorIs(err, deferror.EmpresaIndefinida())
	}

	{ // Fecha
		prevErr := err
		assert.ErrorIs(err, ErrFaltaFechaContable)
		// Corrijo
		f.Fecha = 20220101
		err = GenerarOp(f, pp, lu)
		// Tiene que dar otro error
		assert.NotErrorIs(err, prevErr)
	}

	{ // Config
		prevErr := err
		assert.ErrorIs(err, ErrConfigNoDefinida)
		// Corrijo
		f.Config = 1
		err = GenerarOp(f, pp, lu)
		// Tiene que dar otro error
		assert.NotErrorIs(err, prevErr)
	}

	{ // Esquema
		prevErr := err
		assert.ErrorIs(err, ErrEsquemaNoDefinido)
		// Corrijo
		f.Esquema = 2
		err = GenerarOp(f, pp, lu)
		// Tiene que dar otro error
		assert.NotErrorIs(err, prevErr)
	}

	{ // Usuario
		prevErr := err
		assert.ErrorIs(err, ErrUsuarioNoDefinido)
		// Corrijo
		lu.Usuario.ID = "marcos"
		err = GenerarOp(f, pp, lu)
		// Tiene que dar otro error
		assert.NotErrorIs(err, prevErr)
	}

	{ // Partidas vacias
		prevErr := err
		assert.ErrorIs(err, ErrNoHayPartidas)
		// Corrijo
		pp = append(pp, ops.Partida{
			Cuenta: 1000,
			Monto:  100,
		})
		err = GenerarOp(f, pp, lu)
		// Tiene que dar otro error
		assert.NotErrorIs(err, prevErr)
	}

	{ // Comprobante recibo no está en lookup
		prevErr := err
		assert.ErrorIs(err, ErrCompReciboNoDefinido)
		lu.CompRecibo = &comps.Comp{ID: 1}
		err = GenerarOp(f, pp, lu)
		assert.NotErrorIs(err, prevErr)
	}

	{ // Falta signo
		prevErr := err
		assert.ErrorIs(err, ErrFaltaSignoValor)
		lu.Config.SignoRenglonValor = 1
		err = GenerarOp(f, pp, lu)
		assert.NotErrorIs(err, prevErr)
	}

	{ // Persona que usa sucursales
		lu.Persona.UsaSucursales = true
		err = GenerarOp(f, pp, lu)
		assert.ErrorIs(err, ErrFaltaSucursal)
		f.Persona.Sucursal = new(int)
		*f.Persona.Sucursal = 1
		err = GenerarOp(f, pp, lu)
		assert.NotErrorIs(err, ErrFaltaSucursal)
	}
}

func TestGenerarPartidasValoresRenglon(t *testing.T) {
	require := require.New(t)
	var err error
	var part ops.Partida
	assert := assert.New(t)
	v := types.PartidaValores{}
	fch := fecha.Fecha(20220101)
	p := &personas.Persona{}
	cta := cuentas.Cuenta{}
	ctx := context.Background()
	const comitente = 1

	{ // Partida vacía
		_, err = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.ErrorIs(err, ErrFaltaCuentaContable)
		v.Cuenta = 1
		_, err = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.NotErrorIs(err, ErrFaltaCuentaContable)
	}

	{ // Sin monto
		_, err = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.ErrorIs(err, ErrFaltaMonto)
		v.Monto = dec.NewD2(100)
		_, err = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.NotErrorIs(err, ErrFaltaMonto)
	}

	{ // Sin caja
		cta.AperturaCaja = true
		_, err = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.ErrorIs(err, ErrFaltaCaja)
		v.Caja = new(int)
		*v.Caja = 1
		part, err := generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.NotErrorIs(err, ErrFaltaCaja)
		require.NotNil(part.Caja)
		assert.Equal(1, *part.Caja)
	}

	{ // Detalle
		part, _ := generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.Nil(part.Detalle)
		v.Detalle = "Test"
		part, _ = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		require.NotNil(part.Detalle)
		assert.Equal(v.Detalle, *part.Detalle)
	}

	{ // Persona y sucursal
		cta.AperturaPersona = true
		_, err = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.ErrorIs(err, ErrFaltaPersona)
		v.Persona = new(uuid.UUID)
		*v.Persona = uuid.FromStringOrNil("cff7bffb-2628-4f33-8771-902ffe4f9972")
		part, err = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.NotErrorIs(err, ErrFaltaPersona)
		require.NotNil(part.Persona)
		assert.Equal(*v.Persona, *part.Persona)

		p.UsaSucursales = true
		_, err = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.ErrorIs(err, ErrFaltaSucursal)
		v.Sucursal = new(int)
		*v.Sucursal = 8
		part, err = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.NotErrorIs(err, ErrFaltaSucursal)
		require.NotNil(part.Sucursal)
		assert.Equal(8, *part.Sucursal)
	}

	{ // Monto moneda extranjera
		cta.MonedaExtranjera = true
		// Moneda
		part, err = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.ErrorIs(err, ErrFaltaMoneda)
		v.Moneda = new(int)
		*v.Moneda = 1
		part, err = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.NotErrorIs(err, ErrFaltaMoneda)

		// Pongo monto moneda original
		part, err = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.ErrorIs(err, ErrFaltaMontoOriginalYTC, "%v", spew.Sdump(v, cta))
		v.MontoMonedaOriginal = new(dec.D4)
		*v.MontoMonedaOriginal = dec.NewD4(1)
		part, err = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.NotErrorIs(err, ErrFaltaMontoOriginalYTC)

		// Pongo tipo de cambio
		v.MontoMonedaOriginal = nil
		part, err = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.ErrorIs(err, ErrFaltaMontoOriginalYTC, "%v", spew.Sdump(v, cta))
		v.TC = new(dec.D4)
		*v.TC = dec.NewD4(100)
		part, err = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.NotErrorIs(err, ErrFaltaMontoOriginalYTC)

		assert.Equal(dec.NewD2(100), part.Monto)
		require.NotNil(part.MontoMonedaOriginal)
		assert.Equal(dec.NewD4(1), *part.MontoMonedaOriginal)
		require.NotNil(part.TC)
		assert.Equal(dec.NewD4(100), *part.TC)
	}

	// Unicidades
	{
		cta.AperturaUnicidad = true
		cta.TipoUnicidad = new(int)
		*cta.TipoUnicidad = 2
		part, err = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.ErrorIs(err, ErrFaltaUnicidad, "%v", spew.Sdump(v, cta))

		// Pongo unicidad
		v.UnicidadNueva = &unicidades.Unicidad{}
		part, err = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.ErrorContains(err, "establecida")

		// Otro código
		v.UnicidadNueva.Definicion = 9
		part, err = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.ErrorContains(err, "establecida")

		v.UnicidadNueva.Definicion = 2
		part, err = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.ErrorIs(err, ErrFaltaID)

		v.UnicidadNueva.ID = uuid.FromStringOrNil("39c6cc30-e21b-4dea-af8b-9d691da0da1c")
		part, err = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.NotErrorIs(err, ErrFaltaID)

		// Unicidad ref
		v.UnicidadNueva = nil
		v.UnicidadRef = new(uuid.UUID)
		part, err = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.ErrorIs(err, ErrFaltaID)

		*v.UnicidadRef = uuid.FromStringOrNil("39c6cc30-e21b-4dea-af8b-9d691da0da1c")
		part, err = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{err: true})
		assert.ErrorContains(err, "determinando PartidaAplicada")

		part, err = generarPartidasValoresRenglon(ctx, comitente, v, fch, cta, 1, p, unicMock{})
		assert.Nil(err)
	}

}

func TestProcesarUnicidadDeValorNueva(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)

	{ // Nils
		vacio := procesarUnicidadDeValorNueva(nil, unicidadesdef.Def{}, fecha.Fecha(20210101))
		assert.NotNil(vacio)
	}

	{ // Nil
		v := types.PartidaValores{}
		sinUnicidadNueva := procesarUnicidadDeValorNueva(&v, unicidadesdef.Def{}, fecha.Fecha(20210101))
		assert.NotNil(sinUnicidadNueva)
	}

	{ // Montos distintos entre unicidad y total
		p, def := unicidadNuevaCorrecta()
		p.Monto = 200
		sinUnicidadNueva := procesarUnicidadDeValorNueva(&p, def, fecha.Fecha(20210101))
		assert.NotNil(sinUnicidadNueva)
	}
	{ // Definiciones no coincidentes
		p, def := unicidadNuevaCorrecta()
		p.UnicidadNueva.Definicion = 0
		out := procesarUnicidadDeValorNueva(&p, def, fecha.Fecha(20210101))
		assert.NotNil(out)
	}
	{ // Debería pasar
		p, def := unicidadNuevaCorrecta()
		err := procesarUnicidadDeValorNueva(&p, def, fecha.Fecha(20210101))
		require.Nil(err)
		assert.NotNil(p.UnicidadNueva.Comitente)
		assert.NotNil(p.UnicidadNueva.CreatedAt)
		assert.NotEqual(p.UnicidadNueva.ID, uuid.Nil)
		assert.Len(p.UnicidadNueva.Atts, 3) // Los dos atts + Dias
		assert.Len(p.UnicidadNueva.AttsIndexados, 1)
		assert.Equal(p.Vencimiento, fecha.Fecha(20211031))
		assert.Equal(p.Monto, dec.NewD2(100))

	}
}

func TestProcesarUnicidades(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)

	p1, def1 := unicidadNuevaCorrecta()
	p2, def2 := unicidadExistenteCorrecta()
	p3, def3 := unicidadNuevaCorrecta()

	{ // Vacío
		pp := []types.PartidaValores{}
		dd := map[int]unicidadesdef.Def{
			def1.ID: def1,
			def2.ID: def2,
			def3.ID: def3,
		}

		out, err := procesarUnicidades(pp, dd, fecha.Fecha(20211114))
		require.Nil(err)
		require.Len(out, 0)
	}

	{ // Tres partidas distintas
		pp := []types.PartidaValores{p1, p2, p3}
		dd := map[int]unicidadesdef.Def{
			def1.ID: def1,
			def2.ID: def2,
			def3.ID: def3,
		}

		out, err := procesarUnicidades(pp, dd, fecha.Fecha(20211114))
		require.Nil(err)
		require.Len(out, 3)

		assert.NotNil(out[0].UnicidadNueva)
		assert.Nil(out[0].UnicidadRef)
		assert.Nil(out[0].TipoUnicidad)

		assert.Nil(out[1].UnicidadNueva)
		assert.NotNil(out[1].UnicidadRef)
		assert.NotNil(out[1].TipoUnicidad)

		assert.NotNil(out[2].UnicidadNueva)
		assert.Nil(out[2].UnicidadRef)
		assert.Nil(out[2].TipoUnicidad)
	}

}

func unicidadExistenteCorrecta() (partidaValor types.PartidaValores, definicion unicidadesdef.Def) {

	partidaValor = types.PartidaValores{}
	partidaValor.Monto = dec.NewD2(300)
	partidaValor.UnicidadRef = new(uuid.UUID)
	*partidaValor.UnicidadRef = uuid.FromStringOrNil("36714b70-60c1-434c-b6dd-3dc4579a1d69")
	partidaValor.TipoUnicidad = new(int)
	*partidaValor.TipoUnicidad = 2

	definicion = unicidadesdef.Def{
		ID:        2,
		Comitente: 1,
		Nombre:    "Cheque tercero",
		Atts: []unicidadesdef.Att{
			{
				Nombre:  "Fecha diferida",
				Type:    unicidadesdef.TipoFecha,
				Funcion: unicidadesdef.FuncionFechaVto,
			},
			{
				Nombre:  "Monto",
				Type:    unicidadesdef.TipoD2,
				Funcion: unicidadesdef.FuncionMonto,
			},
		},
		AttsIndexados: []unicidadesdef.Att{
			{
				Nombre: "Número",
				Type:   unicidadesdef.TipoString,
			},
		},
	}
	return
}

func unicidadNuevaCorrecta() (partidaValor types.PartidaValores, definicion unicidadesdef.Def) {

	partidaValor = types.PartidaValores{}
	partidaValor.Monto = dec.NewD2(100)
	partidaValor.UnicidadNueva = &unicidades.Unicidad{
		Definicion: 1,
		AttsIndexados: tipos.JSON{
			"Número": "120",
		},
		Atts: tipos.JSON{
			"Fecha diferida": "2021-10-31",
			"Monto":          100.,
		},
	}

	definicion = unicidadesdef.Def{
		ID:        1,
		Comitente: 1,
		Nombre:    "Cheque tercero",
		Atts: []unicidadesdef.Att{
			{
				Nombre:  "Fecha diferida",
				Type:    unicidadesdef.TipoFecha,
				Funcion: unicidadesdef.FuncionFechaVto,
			},
			{
				Nombre:  "Monto",
				Type:    unicidadesdef.TipoD2,
				Funcion: unicidadesdef.FuncionMonto,
			},
		},
		AttsIndexados: []unicidadesdef.Att{
			{
				Nombre: "Número",
				Type:   unicidadesdef.TipoString,
			},
		},
	}
	return
}

func TestMappearPartidasYAplicacionesCompleta(t *testing.T) {

	aa := []types.Aplicacion{
		{
			FechaFinanciera: fecha.Fecha(20211114),
			MontoAplicado:   dec.NewD2(100),
		},
		{
			FechaFinanciera: fecha.Fecha(20211214),
			MontoAplicado:   dec.NewD2(100),
		},
	}

	pp := []ops.Partida{
		{
			FechaFinanciera: fecha.Fecha(20211114),
			Monto:           dec.NewD2(100),
		},
		{
			FechaFinanciera: fecha.Fecha(20211214),
			Monto:           dec.NewD2(100),
		},
	}

	out, err := mappearPartidasYAplicaciones(pp, aa)
	require.Nil(t, err)
	require.Len(t, out, 2, spew.Sdump(out))
	assert.Equal(t, out[0].Monto, dec.NewD2(-100))
	assert.Equal(t, out[1].Monto, dec.NewD2(-100))
}

func TestMappearPartidasYAplicacionesSignosErrados(t *testing.T) {

	aa := []types.Aplicacion{
		{
			FechaFinanciera: fecha.Fecha(20211114),
			MontoAplicado:   dec.NewD2(100),
		},
		{
			FechaFinanciera: fecha.Fecha(20211214),
			MontoAplicado:   dec.NewD2(100),
		},
	}

	pp := []ops.Partida{
		{
			FechaFinanciera: fecha.Fecha(20211114),
			Monto:           dec.NewD2(-100),
		},
		{
			FechaFinanciera: fecha.Fecha(20211214),
			Monto:           dec.NewD2(-100),
		},
	}

	out, err := mappearPartidasYAplicaciones(pp, aa)
	require.NotNil(t, err, spew.Sdump(out))
}

func TestMappearPartidasYAplicacionesVariasPartidas(t *testing.T) {

	aa := []types.Aplicacion{
		{
			FechaFinanciera: fecha.Fecha(20211114),
			MontoAplicado:   dec.NewD2(100),
		},
		{
			FechaFinanciera: fecha.Fecha(20211214),
			MontoAplicado:   dec.NewD2(100),
		},
	}

	pp := []ops.Partida{
		{
			FechaFinanciera: fecha.Fecha(20211114),
			Monto:           dec.NewD2(75),
			Cuenta:          1,
		},
		{
			FechaFinanciera: fecha.Fecha(20211114),
			Monto:           dec.NewD2(25),
			Cuenta:          2,
		},
		{
			FechaFinanciera: fecha.Fecha(20211214),
			Monto:           dec.NewD2(100),
		},
	}

	out, err := mappearPartidasYAplicaciones(pp, aa)
	require.Nil(t, err)
	require.Len(t, out, 3, spew.Sdump(out))
	assert.Equal(t, out[0].Monto, dec.NewD2(-75))
	assert.Equal(t, out[0].Cuenta, 1)
	assert.Equal(t, out[1].Monto, dec.NewD2(-25))
	assert.Equal(t, out[1].Cuenta, 2)
	assert.Equal(t, out[2].Monto, dec.NewD2(-100))
}

func TestMappearPartidasYAplicacionesVariasPartidasIncompleta(t *testing.T) {

	aa := []types.Aplicacion{
		{
			FechaFinanciera: fecha.Fecha(20211114),
			MontoAplicado:   dec.NewD2(80),
		},
		{
			FechaFinanciera: fecha.Fecha(20211214),
			MontoAplicado:   dec.NewD2(100),
		},
	}

	pp := []ops.Partida{
		{
			FechaFinanciera: fecha.Fecha(20211114),
			Monto:           dec.NewD2(75),
			Cuenta:          1,
		},
		{
			FechaFinanciera: fecha.Fecha(20211114),
			Monto:           dec.NewD2(5),
			Cuenta:          2,
		},
		{
			FechaFinanciera: fecha.Fecha(20211214),
			Monto:           dec.NewD2(100),
		},
	}

	out, err := mappearPartidasYAplicaciones(pp, aa)
	require.Nil(t, err)
	require.Len(t, out, 3, spew.Sdump(out))
	assert.Equal(t, out[0].Monto, dec.NewD2(-75))
	assert.Equal(t, out[0].Cuenta, 1)
	assert.Equal(t, out[1].Monto, dec.NewD2(-5))
	assert.Equal(t, out[1].Cuenta, 2)
	assert.Equal(t, out[2].Monto, dec.NewD2(-100))
}

func TestMappearPartidasYAplicacionesVariasPartidasExcesoAplicacion(t *testing.T) {

	aa := []types.Aplicacion{
		{
			FechaFinanciera: fecha.Fecha(20211114),
			MontoAplicado:   dec.NewD2(90),
		},
		{
			FechaFinanciera: fecha.Fecha(20211214),
			MontoAplicado:   dec.NewD2(100),
		},
	}

	pp := []ops.Partida{
		{
			FechaFinanciera: fecha.Fecha(20211114),
			Monto:           dec.NewD2(75),
			Cuenta:          1,
		},
		{
			FechaFinanciera: fecha.Fecha(20211114),
			Monto:           dec.NewD2(5),
			Cuenta:          2,
		},
		{
			FechaFinanciera: fecha.Fecha(20211214),
			Monto:           dec.NewD2(100),
		},
	}

	out, err := mappearPartidasYAplicaciones(pp, aa)
	require.NotNil(t, err, spew.Sdump(out))
}

func TestGenerarPartidasAnticipoVacia(t *testing.T) {

	pp := []ops.Partida{}
	persona := uuid.FromStringOrNil("8329f4c6-ccc7-4537-ab47-19fe8c8b14c0")
	out, err := generarPartidasAnticipo(pp, persona, nil, nil, nil, 20221001)
	require.Nil(t, err)
	assert.Len(t, out, 0)
}

// Debería generar una partida anticipo
func TestGenerarPartidasAnticipoSoloValores(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)
	pp := []ops.Partida{
		{
			Pata:            types.PataValor,
			FechaFinanciera: 20210101,
			Monto:           dec.NewD2(100),
		},
	}
	monto := dec.NewD2(-100)
	cuenta := 1
	persona := uuid.FromStringOrNil("8329f4c6-ccc7-4537-ab47-19fe8c8b14c0")
	out, err := generarPartidasAnticipo(pp, persona, nil, &cuenta, nil, 20220101)
	require.Nil(err)
	require.Len(out, 1)
	assert.Equal(out[0].Pata, types.PataReciboPatrimonial)
	assert.Equal(out[0].Cuenta, cuenta)
	assert.Equal(out[0].Monto, monto)
	assert.Equal(out[0].FechaFinanciera, fecha.Fecha(20210101))
}

func TestGenerarPartidasAnticipoAplicacionAnteriorParcial(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)
	pp := []ops.Partida{
		{
			Pata:            types.PataValor,
			FechaFinanciera: 20210101,
			Monto:           dec.NewD2(100),
		},
		{
			Pata:            types.PataAplicacion,
			FechaFinanciera: 20210101,
			Monto:           dec.NewD2(-50),
		},
	}
	monto := dec.NewD2(-50)
	cuenta := 1
	persona := uuid.FromStringOrNil("8329f4c6-ccc7-4537-ab47-19fe8c8b14c0")
	sucursal := 4
	out, err := generarPartidasAnticipo(pp, persona, &sucursal, &cuenta, nil, 20220101)
	require.Nil(err)

	// Debería generar una partida anticipo
	require.Len(out, 1)
	assert.Equal(out[0].Pata, types.PataReciboPatrimonial)
	assert.Equal(out[0].Cuenta, cuenta)
	assert.Equal(out[0].Monto, monto)
	assert.Equal(*out[0].Persona, persona)
	require.NotNil(out[0].Sucursal)
	assert.NotNil(*out[0].Sucursal, sucursal)
}

func TestGenerarPartidasAnticipoAplicacionAnteriorCompleta(t *testing.T) {
	require := require.New(t)
	pp := []ops.Partida{
		{
			Pata:            types.PataValor,
			FechaFinanciera: 20210101,
			Monto:           dec.NewD2(100),
		},
		{
			Pata:            types.PataAplicacion,
			FechaFinanciera: 20210101,
			Monto:           dec.NewD2(-100),
		},
	}
	persona := uuid.FromStringOrNil("8329f4c6-ccc7-4537-ab47-19fe8c8b14c0")
	{ // Monto nil y correcto
		cuenta := 1
		out, err := generarPartidasAnticipo(pp, persona, nil, &cuenta, nil, 20220101)
		require.Nil(err)
		require.Len(out, 0)
	}
	{ // Monto 0 y correcto
		cuenta := 1
		out, err := generarPartidasAnticipo(pp, persona, nil, &cuenta, nil, 20220101)
		require.Nil(err)
		require.Len(out, 0)
	}
}

func TestGenerarPartidasAnticipoAplicacionConFacturaContado(t *testing.T) {
	require := require.New(t)
	pp := []ops.Partida{
		{
			Pata:  types.PataProducto,
			Monto: dec.NewD2(-100),
		},
		{
			Pata:  types.PataIVA,
			Monto: dec.NewD2(-21),
		},
		{
			Pata:            types.PataFacturaPatrimonial,
			FechaFinanciera: 20210101,
			Monto:           dec.NewD2(121),
		},
		{
			Pata:            types.PataValor,
			FechaFinanciera: 20210201,
			Monto:           dec.NewD2(121),
		},
		{
			Pata:            types.PataAplicacion,
			FechaFinanciera: 20210201,
			Monto:           dec.NewD2(-121),
		},
	}
	cuenta := 1
	persona := uuid.FromStringOrNil("8329f4c6-ccc7-4537-ab47-19fe8c8b14c0")
	out, err := generarPartidasAnticipo(pp, persona, nil, &cuenta, nil, 20220101)
	require.Nil(err)
	require.Len(out, 0)
}

func TestGenerarPartidasAnticipoAplicacionConFacturaCobroExceso(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)
	pp := []ops.Partida{
		{
			Pata:  types.PataProducto,
			Monto: dec.NewD2(-100),
		},
		{
			Pata:  types.PataIVA,
			Monto: dec.NewD2(-21),
		},
		{
			Pata:            types.PataFacturaPatrimonial,
			FechaFinanciera: 20210101,
			Monto:           dec.NewD2(121),
		},
		{
			Pata:            types.PataValor,
			FechaFinanciera: 20210201,
			Monto:           dec.NewD2(150),
		},
		{
			Pata:            types.PataAplicacion,
			FechaFinanciera: 20210201,
			Monto:           dec.NewD2(-121),
		},
	}
	cuenta := 1
	persona := uuid.FromStringOrNil("8329f4c6-ccc7-4537-ab47-19fe8c8b14c0")
	out, err := generarPartidasAnticipo(pp, persona, nil, &cuenta, nil, 20220101)
	require.Nil(err)
	require.Len(out, 1)
	assert.Equal(out[0].Monto, dec.NewD2(-29))
	assert.Equal(out[0].Cuenta, 1)
	assert.Equal(*out[0].Persona, persona)
	var nilSuc *int
	assert.Equal(out[0].Sucursal, nilSuc)
	assert.Equal(out[0].Pata, types.PataReciboPatrimonial)
	assert.Equal(out[0].FechaFinanciera, fecha.Fecha(20210201))
}

func TestGenerarPartidasAnticipoAplicacionVariosVencimientos(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)
	pp := []ops.Partida{
		{
			Pata:            types.PataValor,
			FechaFinanciera: fecha.Fecha(20210101),
			Monto:           dec.NewD2(100),
		},
		{
			Pata:            types.PataValor,
			FechaFinanciera: fecha.Fecha(20210201),
			Monto:           dec.NewD2(21),
		},
	}
	cuenta := 1
	persona := uuid.FromStringOrNil("8329f4c6-ccc7-4537-ab47-19fe8c8b14c0")
	out, err := generarPartidasAnticipo(pp, persona, nil, &cuenta, nil, 20220101)
	require.Nil(err)
	require.Len(out, 2)

	var nilSuc *int

	// Primer vencimiento
	assert.Equal(out[0].Monto, dec.NewD2(-100))
	assert.Equal(out[0].Cuenta, 1)
	assert.Equal(*out[0].Persona, persona)
	assert.Equal(out[0].Sucursal, nilSuc)
	assert.Equal(out[0].Pata, types.PataReciboPatrimonial)
	assert.Equal(out[0].FechaFinanciera, fecha.Fecha(20210101))

	// Segundo vencimiento
	assert.Equal(out[1].Monto, dec.NewD2(-21))
	assert.Equal(out[1].Cuenta, 1)
	assert.Equal(*out[1].Persona, persona)
	assert.Equal(out[1].Sucursal, nilSuc)
	assert.Equal(out[1].Pata, types.PataReciboPatrimonial)
	assert.Equal(out[1].FechaFinanciera, fecha.Fecha(20210201))

}

func TestGenerarPartidasAnticipoAplicacionFacturaYVariosVencimientos(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)
	pp := []ops.Partida{
		{
			Pata:  types.PataProducto,
			Monto: dec.NewD2(-100),
		},
		{
			Pata:  types.PataIVA,
			Monto: dec.NewD2(-21),
		},
		{
			Pata:            types.PataFacturaPatrimonial,
			FechaFinanciera: fecha.Fecha(20201215),
			Monto:           dec.NewD2(60),
		},
		{
			Pata:            types.PataFacturaPatrimonial,
			FechaFinanciera: fecha.Fecha(20201231),
			Monto:           dec.NewD2(61),
		},
		{
			Pata:            types.PataValor,
			FechaFinanciera: fecha.Fecha(20210101),
			Monto:           dec.NewD2(100),
		},
		{
			Pata:            types.PataValor,
			FechaFinanciera: fecha.Fecha(20210201),
			Monto:           dec.NewD2(21),
		},
	}
	cuenta := 1
	persona := uuid.FromStringOrNil("8329f4c6-ccc7-4537-ab47-19fe8c8b14c0")
	out, err := generarPartidasAnticipo(pp, persona, nil, &cuenta, nil, 20201201)
	require.Nil(err)
	require.Len(out, 2)

	var nilSuc *int

	// Primer vencimiento
	assert.Equal(out[0].Monto, dec.NewD2(-100))
	assert.Equal(out[0].Cuenta, 1)
	assert.Equal(*out[0].Persona, persona)
	assert.Equal(out[0].Sucursal, nilSuc)
	assert.Equal(out[0].Pata, types.PataReciboPatrimonial)
	assert.Equal(out[0].FechaFinanciera, fecha.Fecha(20210101))

	// Segundo vencimiento
	assert.Equal(out[1].Monto, dec.NewD2(-21))
	assert.Equal(out[1].Cuenta, 1)
	assert.Equal(*out[1].Persona, persona)
	assert.Equal(out[1].Sucursal, nilSuc)
	assert.Equal(out[1].Pata, types.PataReciboPatrimonial)
	assert.Equal(out[1].FechaFinanciera, fecha.Fecha(20210201))
}

func TestCalcularVencimientosValoresVacío(t *testing.T) {
	require := require.New(t)
	vv := []types.PartidaValores{}
	out, err := calcularVencimientosValores(fecha.Fecha(20211115), vv)
	require.Nil(err)
	require.Len(out, 0)
}

func TestCalcularVencimientosValoresContado(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)
	vv := []types.PartidaValores{
		{
			Vencimiento: fecha.Fecha(20211115),
			Monto:       dec.NewD2(100),
		},
	}
	out, err := calcularVencimientosValores(fecha.Fecha(20211115), vv)
	require.Nil(err)
	require.Len(out, 1)
	assert.Equal(out[0].Fecha, fecha.Fecha(20211115))
	assert.Equal(out[0].Monto, dec.NewD2(100))
	assert.Equal(out[0].Dias, 0)
}

func TestCalcularVencimientosValoresVarios(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)
	vv := []types.PartidaValores{
		{
			Vencimiento: fecha.Fecha(20211119),
			Monto:       dec.NewD2(100),
		},
		{
			Vencimiento: fecha.Fecha(20211119),
			Monto:       dec.NewD2(100),
		},
		{
			Vencimiento: fecha.Fecha(20211130),
			Monto:       dec.NewD2(100),
		},
	}
	out, err := calcularVencimientosValores(fecha.Fecha(20211115), vv)
	require.Nil(err)
	require.Len(out, 2)

	// Cuota 1
	assert.Equal(out[0].Fecha, fecha.Fecha(20211119))
	assert.Equal(out[0].Monto, dec.NewD2(200))
	assert.Equal(out[0].Dias, 4)

	// Cuota 2
	assert.Equal(out[1].Fecha, fecha.Fecha(20211130))
	assert.Equal(out[1].Monto, dec.NewD2(100))
	assert.Equal(out[1].Dias, 15)
}

func TestGenerarPartidasAplicacionesAnteriores(t *testing.T) {
	id1 := uuid.FromStringOrNil("342f8b03-2fbe-4a8a-a2d5-17be4c46f83b")
	id2 := uuid.FromStringOrNil("342f8b03-2fbe-4a8a-a2d5-17be4c46f83c")

	{ // Aplicación OK
		vv := []types.Aplicacion{
			{
				ID:             &id1,
				MontoPendiente: 900,
				Cuenta:         100,
				MontoAplicado:  500,
			},
			{
				ID:             &id2,
				MontoPendiente: -200,
				Cuenta:         200,
				MontoAplicado:  200,
			},
		}
		anteriores := map[uuid.UUID]ops.Partida{
			id1: {
				ID:     id1,
				Cuenta: 100,
				Monto:  1000,
			},
			id2: {
				ID:     id2,
				Cuenta: 200,
				Monto:  -200,
			},
		}

		out, err := generarPartidasAplicacionesAnteriores(vv, 20220331, anteriores, -1)
		require.Nil(t, err)
		require.Len(t, out, 2)
		assert.Equal(t, dec.D2(-500), out[0].Monto)
		assert.Equal(t, dec.D2(200), out[1].Monto)
	}

	{ // Sobreaplicación
		vv := []types.Aplicacion{
			{
				ID:             &id1,
				MontoPendiente: 900,
				Cuenta:         100,
				MontoAplicado:  500,
			},
			{
				ID:             &id2,
				MontoPendiente: -200,
				Cuenta:         200,
				MontoAplicado:  300,
			},
		}
		anteriores := map[uuid.UUID]ops.Partida{
			id1: {
				ID:     id1,
				Cuenta: 100,
				Monto:  1000,
			},
			id2: {
				ID:     id2,
				Cuenta: 200,
				Monto:  -200,
			},
		}

		_, err := generarPartidasAplicacionesAnteriores(vv, 20220331, anteriores, -1)
		require.NotNil(t, err)
	}
}

type unicMock struct {
	err bool
	id  bool
}

func (m unicMock) GetPartidaAplicada(context.Context, int, uuid.UUID) (uuid.UUID, error) {
	if m.err {
		return uuid.Nil, errors.Errorf("mock err")
	}
	return uuid.FromStringOrNil("c7826796-bab9-41f0-b957-141e079ceaa2"), nil
}
