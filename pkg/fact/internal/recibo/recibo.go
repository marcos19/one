package recibo

import (
	"context"
	"fmt"
	"math"
	"sort"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/fact/config"
	"bitbucket.org/marcos19/one/pkg/fact/internal/lookups"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/personas"
	"bitbucket.org/marcos19/one/pkg/unicidades"
	"bitbucket.org/marcos19/one/pkg/unicidadesdef"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
)

// generarRecibo crea una op que contiene los valores.
func GenerarOp(f *types.Fact, pp []ops.Partida, lu *lookups.Lookups) (err error) {
	if f.ID == uuid.Nil {
		return errors.Errorf("falta ID de fact")
	}
	if f.Comitente == 0 {
		return deferror.ComitenteIndefinido()
	}
	if f.Empresa == 0 {
		return deferror.EmpresaIndefinida()
	}
	if f.Fecha == 0 {
		return ErrFaltaFechaContable
	}
	if f.Config == 0 {
		return ErrConfigNoDefinida
	}
	if f.Esquema == 0 {
		return ErrEsquemaNoDefinido
	}
	if lu.Usuario.ID == "" {
		return ErrUsuarioNoDefinido
	}
	if len(pp) == 0 {
		return ErrNoHayPartidas
	}
	if lu.CompRecibo == nil {
		return ErrCompReciboNoDefinido
	}

	op := ops.Op{}
	op.CompItem = new(int)
	*op.CompItem = types.ItemRecibo

	// Por ahora los quiero en orden, tengo el comitente para distribuir
	// la carga en el cluster
	op.ID, _ = uuid.NewV1()
	op.TranID = f.ID
	op.Comitente = f.Comitente
	op.Empresa = f.Empresa
	op.Fecha = f.Fecha
	op.Programa = types.PkgID
	op.TranDef = f.Config
	op.FechaOriginal = op.Fecha
	op.Usuario = lu.Usuario.ID
	op.PartidasContables = pp

	// Calculo vencimientos
	f.VencimientosPago, err = calcularVencimientosValores(f.Fecha, f.Valores)
	if err != nil {
		return errors.Wrap(err, "calculando vencimientos recibo")
	}

	op.TotalComp = new(dec.D2)
	*op.TotalComp = f.VencimientosPago.TotalEnMonedaContable()

	op.CompID = lu.CompRecibo.ID

	// Determino signos
	if lu.Config.SignoRenglonValor == 0 {
		return ErrFaltaSignoValor
	}

	// Impresión
	op.Impresion = map[string]interface{}{}

	{ // Datos de la empresa
		op.Impresion["EmpresaNombre"] = lu.Empresa.ImpresionNombre
		if lu.Empresa.ImpresionNombreFantasia != "" {
			op.Impresion["EmpresaNombreFantasia"] = lu.Empresa.ImpresionNombreFantasia
		}
		op.Impresion["HeaderRenglonesLeft"] = lu.Empresa.ImpresionRenglonesLeft
		op.Impresion["HeaderRenglonesRight"] = lu.Empresa.ImpresionRenglonesRight

	}

	{ // Datos del comp
		op.Impresion["LetraComp"] = lu.CompRecibo.Letra
		if op.AFIPComprobanteID != nil {
			op.Impresion["CodigoComp"] = fmt.Sprintf("Cod. %v", *op.AFIPComprobanteID)
		}

		// Cajero
		if lu.CompRecibo.ImprimeDatosCajero {
			if lu.Usuario.ID == "" {
				return errors.Wrap(err, "buscando datos de cajero")
			}
			op.Impresion["CajeroNombre"] = fmt.Sprintf("%v %v", lu.Usuario.Nombre, lu.Usuario.Apellido)
			op.Impresion["CajeroDNI"] = fmt.Sprintf("%v", lu.Usuario.DNI)
		}
	}
	{ // Datos de la persona
		// Identificacion fiscal
		domicilio := ""
		if lu.Persona.UsaSucursales {
			if f.Persona.Sucursal == nil {
				return ErrFaltaSucursal
			}
			for _, v := range lu.Persona.Sucursales {
				if *v.ID == *f.Persona.Sucursal {
					f.Persona.NombreSucursal = v.Nombre
					domicilio = v.DomicilioString()
					break
				}
			}
			op.Sucursal = new(int)
			*op.Sucursal = *f.Persona.Sucursal
		} else {
			domicilio = lu.Persona.DomicilioString()
		}
		op.Impresion["PersonaCamposLeft"] = []string{
			fmt.Sprintf("%v: %v", lu.Persona.TipoIdentificacionFiscal(), lu.Persona.NIdentificacionFiscal()),
			fmt.Sprintf("Domicilio: %v", domicilio),
		}

		derecha := []string{
			fmt.Sprintf("Condición frente a IVA: %v", f.Persona.CondicionFiscal.Nombre()),
		}

		op.Impresion["PersonaCamposRight"] = derecha
		op.Persona = new(uuid.UUID)
		*op.Persona = f.Persona.ID
		op.PersonaNombre = f.Persona.Nombre

		// Le pongo detalle a la op
		if f.Factura == nil {
			det := "Cobro"
			if lu.Config.SignoRenglonValor == config.SignoPago {
				det = "Pago"
			}
			det = fmt.Sprintf("%v a %v", det, op.PersonaNombre)
			aJuntar := []string{det}
			if f.Detalle != "" {
				aJuntar = append(aJuntar, f.Detalle)
			}
			op.Detalle = strings.Join(aJuntar, " - ")
		}
	}
	// if len(op.PartidasContables) == 0 && len(f.Aplicaciones) == 0 {
	// 	return errors.New("el recibo debería haber tenido partidas contables o aplicaciones")
	// }

	f.Recibo = &op
	return nil
}

func GenerarPartidas(
	ctx context.Context,
	f *types.Fact,
	lu *lookups.Lookups,
	ctasGetter cuentas.ReaderOne,
	unicGetter ops.UnicidadIDGetter) (out []ops.Partida, err error) {

	f.Valores, err = procesarUnicidades(f.Valores, lu.UnicidadesDef, f.Fecha)
	if err != nil {
		return out, errors.Wrap(err, "procesando unicidades")
	}

	{ // Partidas valores
		for i, v := range f.Valores {
			// Busco cuenta
			cta, err := ctasGetter.ReadOne(ctx, cuentas.ReadOneReq{Comitente: f.Comitente, ID: v.Cuenta})
			if err != nil {
				return out, errors.Wrapf(err, "buscando cuenta %v en lookup", v.Cuenta)
			}
			// Busco persona
			var pers *personas.Persona
			if v.Persona != nil {
				p, ok := lu.Personas[*v.Persona]
				if ok {
					pers = &p
				}
			}
			p, err := generarPartidasValoresRenglon(ctx, f.Comitente, v, f.Fecha, cta, lu.Config.SignoRenglonValor, pers, unicGetter)
			if err != nil {
				return out, errors.Wrapf(err, "generando partida contable de valor renglón %v", i+1)
			}
			out = append(out, p)
		}
	}

	{ // Partidas de aplicación
		pp, err := generarPartidasAplicacionesAnteriores(f.Aplicaciones, f.Fecha, lu.AplicacionesAnteriores, lu.Config.SignoRenglonValor)
		if err != nil {
			return out, errors.Wrap(err, "generando partidas de aplicaciones previas")
		}
		out = append(out, pp...)
	}

	{ // Partidas de aplicación este comprobante
		pp, err := generarPartidasAplicacionesEste(f.Aplicaciones, out)
		if err != nil {
			return out, errors.Wrap(err, "generando partidas de aplicacion de esta operación")
		}
		out = append(out, pp...)
	}

	{ // Partidas de anticipo
		pp, err := generarPartidasAnticipo(
			out,
			f.Persona.ID,
			f.Persona.Sucursal,
			f.AnticipoCuenta,
			f.AnticipoCentro,
			f.Fecha)
		if err != nil {
			return out, errors.Wrap(err, "generando partidas de anticipo")
		}
		out = append(out, pp...)
	}

	return
}

// Suma partidas patrimoniales de la factura, del recibo, las aplicaciones
// Si todas esas no se cancelan, significa que una parte va a anticipo.
func generarPartidasAnticipo(
	pp []ops.Partida,
	persona uuid.UUID,
	sucursal *int,
	ctaAnticipo *int,
	centro *int,
	fechaComp fecha.Fecha) (out []ops.Partida, err error) {

	sums := map[fecha.Fecha]dec.D2{}
	sum := dec.D2(0)

	// Determino monto
	for _, v := range pp {
		switch v.Pata {
		case
			types.PataValor,
			types.PataAplicacion:
			if v.FechaFinanciera == 0 {
				v.FechaFinanciera = fechaComp
				// return out, errors.Errorf("partida pata %v por %v tenía fecha financiera vacía", v.Pata, v.Monto)
			}
			sums[v.FechaFinanciera] += v.Monto
			sum += v.Monto
		}
	}

	// Se cancelan entre sí?
	if sum == 0 {
		return
	}

	// Valido
	if persona == uuid.Nil {
		return out, errors.Errorf("no se ingresó ID de persona")
	}
	if ctaAnticipo == nil {
		return out, errors.Errorf("correspondía anticipo pero no se determinó cuenta anticipo")
	}
	if *ctaAnticipo == 0 {
		return out, errors.Errorf("correspondía anticipo pero no se determinó cuenta anticipo")
	}
	// if monto == nil {
	// 	return out, errors.Errorf("correspondía anticipo pero no se determinó monto")
	// }
	// if *monto == 0 {
	// 	return out, errors.Errorf("correspondía anticipo pero no se determinó monto")
	// }
	// if sum != -*monto {
	// 	return out, errors.Errorf("con el monto anticipo ingresado no cierra el asiento")
	// }

	// Hago determinsta output
	type vto struct {
		vto fecha.Fecha
		mto dec.D2
	}
	vtos := []vto{}

	for k, v := range sums {
		v := vto{
			vto: k,
			mto: v,
		}
		vtos = append(vtos, v)
	}
	sort.Slice(vtos, func(i, j int) bool {
		return vtos[i].vto < vtos[j].vto
	})

	// Creo partidas
	for _, v := range vtos {
		p := ops.Partida{}
		p.ID, err = uuid.NewV1()
		if err != nil {
			return out, err
		}
		p.PartidaAplicada = new(uuid.UUID)
		*p.PartidaAplicada = p.ID
		p.Pata = types.PataReciboPatrimonial
		p.Cuenta = *ctaAnticipo
		p.Monto = -v.mto
		p.Persona = new(uuid.UUID)
		*p.Persona = persona
		p.FechaFinanciera = v.vto
		if sucursal != nil {
			p.Sucursal = new(int)
			*p.Sucursal = *sucursal
		}

		out = append(out, p)
	}
	return
}

// Las genera si el frontend lo dice
func generarPartidasAplicacionesEste(vv []types.Aplicacion, pp []ops.Partida) (out []ops.Partida, err error) {

	// Determino las partidas patrimoniales que generó la factura
	ppat := []ops.Partida{}
	for _, v := range pp {
		if v.Pata != types.PataFacturaPatrimonial {
			continue
		}
		ppat = append(ppat, v)
	}
	if len(ppat) == 0 {
		return
	}
	out, err = mappearPartidasYAplicaciones(ppat, vv)
	if err != nil {
		return out, errors.Wrap(err, "mappeando partidas y aplicaciones")
	}
	return
}

// Desde el frontend las aplicaciones esta factura no vienen con un ID
// o cuenta contable, solamente partidas por fecha de vencimiento.
// Tengo PartidasPatrimonialesFactura y PartidasAplicadas, necesito una
// función que genere ops.Partida
func mappearPartidasYAplicaciones(pp []ops.Partida, aa []types.Aplicacion) (out []ops.Partida, err error) {

	pendientes := map[fecha.Fecha]dec.D2{}
	for _, v := range aa {
		pendientes[v.FechaFinanciera] += v.MontoAplicado
	}

	// Los ordeno para que ouput sea determinista
	type vto struct {
		vto fecha.Fecha
		mto dec.D2
	}
	vtos := []vto{}
	for k, v := range pendientes {
		v := vto{
			vto: k,
			mto: v,
		}
		vtos = append(vtos, v)
	}
	sort.Slice(vtos, func(i, j int) bool {
		return vtos[i].vto < vtos[j].vto
	})

	for _, vto := range vtos {
		mtoPendiente := vto.mto
		for _, v := range pp {
			if v.FechaFinanciera != vto.vto {
				continue
			}
			if mtoPendiente >= v.Monto {
				// Creo partida con v.Monto
				p, err := generarPartidaAplicacion(v, vto.vto)
				if err != nil {
					return out, err
				}
				mtoPendiente -= v.Monto
				out = append(out, p)
				// Sigo loopeando hasta llenar el monto pendiente
			} else {
				// creo partida con mtoPendiente
				p, err := generarPartidaAplicacion(v, vto.vto)
				if err != nil {
					return out, err
				}
				p.Monto = -mtoPendiente
				mtoPendiente = 0
				out = append(out, p)
			}
			if mtoPendiente == 0 {
				break
			}
		}
		if mtoPendiente != 0 {
			return out, errors.Errorf("aplicaciones superan partidas disponibles por %v", mtoPendiente)
		}
	}

	return
}

func generarPartidaAplicacion(laAplicada ops.Partida, fchVto fecha.Fecha) (p ops.Partida, err error) {

	p.ID, err = uuid.NewV1()
	if err != nil {
		return p, err
	}

	p.PartidaAplicada = new(uuid.UUID)
	*p.PartidaAplicada = laAplicada.ID

	p.Pata = types.PataAplicacion
	p.Cuenta = laAplicada.Cuenta
	p.FechaFinanciera = fchVto

	// Centro
	if laAplicada.Centro != nil {
		p.Centro = new(int)
		*p.Centro = *laAplicada.Centro
	}

	// Caja
	if laAplicada.Caja != nil {
		p.Caja = new(int)
		*p.Caja = *laAplicada.Caja
	}

	// Persona
	if laAplicada.Persona != nil {
		p.Persona = new(uuid.UUID)
		*p.Persona = *laAplicada.Persona
	}

	// Sucursal
	if laAplicada.Sucursal != nil {
		p.Sucursal = new(int)
		*p.Sucursal = *laAplicada.Sucursal
	}

	// Deposito
	if laAplicada.Deposito != nil {
		p.Deposito = new(int)
		*p.Deposito = *laAplicada.Deposito
	}

	// Producto
	if laAplicada.Producto != nil {
		p.Producto = new(uuid.UUID)
		*p.Producto = *laAplicada.Producto
	}

	// Producto
	if laAplicada.SKU != nil {
		p.SKU = new(uuid.UUID)
		*p.SKU = *laAplicada.SKU
	}

	// Contrato
	if laAplicada.Contrato != nil {
		p.Contrato = new(int)
		*p.Contrato = *laAplicada.Contrato
	}

	p.Monto = -laAplicada.Monto

	return
}

// En base a las aplicaciones seleccionadas por el usuario en el frontend
// genera las partidas contables correspondientes.
func generarPartidasAplicacionesAnteriores(
	vv []types.Aplicacion,
	fchComp fecha.Fecha,
	anteriores map[uuid.UUID]ops.Partida,
	signoRenglonValor int,
) (out []ops.Partida, err error) {

	// Creo las partidas contables
	for _, v := range vv {

		if v.MontoAplicado == 0 {
			continue
		}
		laAplicada, ok := anteriores[*v.ID]
		if !ok {
			return out, errors.Errorf("no se ingresó la partida aplicacion anterior")
		}

		p := ops.Partida{}
		p.PartidaAplicada = new(uuid.UUID)
		*p.PartidaAplicada = laAplicada.ID
		p.Pata = types.PataAplicacion
		p.Cuenta = laAplicada.Cuenta
		p.FechaFinanciera = fchComp

		p.ID, err = uuid.NewV1()
		if err != nil {
			return out, err
		}

		// Centro
		if laAplicada.Centro != nil {
			p.Centro = new(int)
			*p.Centro = *laAplicada.Centro
		}

		// Caja
		if laAplicada.Caja != nil {
			p.Caja = new(int)
			*p.Caja = *laAplicada.Caja
		}

		// Persona
		if laAplicada.Persona != nil {
			p.Persona = new(uuid.UUID)
			*p.Persona = *laAplicada.Persona
		}

		// Sucursal
		if laAplicada.Sucursal != nil {
			p.Sucursal = new(int)
			*p.Sucursal = *laAplicada.Sucursal
		}

		// Deposito
		if laAplicada.Deposito != nil {
			p.Deposito = new(int)
			*p.Deposito = *laAplicada.Deposito
		}

		// Producto
		if laAplicada.Producto != nil {
			p.Producto = new(uuid.UUID)
			*p.Producto = *laAplicada.Producto
		}

		// Producto
		if laAplicada.SKU != nil {
			p.SKU = new(uuid.UUID)
			*p.SKU = *laAplicada.SKU
		}

		// Contrato
		if laAplicada.Contrato != nil {
			p.Contrato = new(int)
			*p.Contrato = *laAplicada.Contrato
		}

		// Corroboro límite
		apl := v.MontoAplicado.Float()
		disp := math.Abs(v.MontoPendiente.Float())
		if apl > disp {
			return out, errors.Errorf("el monto aplicado (%v) supera el disponible (%v) para aplicar en partida %v",
				dec.NewD2(apl), dec.NewD2(disp), laAplicada.ID)
		}

		// Está dentro del límite. Tengo que determinar el signo ya que el MontoAplicado
		// llega en valor absoluto.
		if v.MontoPendiente < 0 {
			// => Aplicación va en positivo
			p.Monto = dec.NewD2(math.Abs(v.MontoAplicado.Float()))
		} else {
			// => Aplicación va en negativo
			p.Monto = dec.NewD2(math.Abs(v.MontoAplicado.Float()) * -1.)
		}

		out = append(out, p)
	}
	return
}

// En base a una types.Partida valor, genera la partida contable correspondiente.
func generarPartidasValoresRenglon(
	ctx context.Context,
	comitente int,
	v types.PartidaValores,
	fchComp fecha.Fecha,
	cta cuentas.Cuenta,
	signoRenglonValor int,
	persona *personas.Persona,
	uGett ops.UnicidadIDGetter,
) (p ops.Partida, err error) {

	// Valido
	if comitente == 0 {
		return p, deferror.ComitenteIndefinido()
	}
	if v.Cuenta == 0 {
		return p, ErrFaltaCuentaContable
	}
	if v.Monto == 0 {
		return p, ErrFaltaMonto
	}

	p.ID, err = uuid.NewV1()
	if err != nil {
		return p, err
	}
	p.Pata = types.PataValor
	p.Cuenta = v.Cuenta

	p.Monto = v.Monto * dec.D2(signoRenglonValor)
	p.FechaFinanciera = fchComp

	if cta.AperturaCaja {
		if v.Caja == nil {
			return p, ErrFaltaCaja
		}
		p.Caja = new(int)
		*p.Caja = *v.Caja
	}

	if v.Detalle != "" {
		p.Detalle = new(string)
		*p.Detalle = v.Detalle
	}

	// Persona
	if cta.AperturaPersona {
		if v.Persona == nil {
			return p, ErrFaltaPersona
		}
		p.Persona = new(uuid.UUID)
		*p.Persona = *v.Persona
		if persona == nil {
			return p, errors.Errorf("no se ingresó datos de persona a la función")
		}
		if persona.UsaSucursales {
			if v.Sucursal == nil {
				return p, ErrFaltaSucursal
			}
			p.Sucursal = new(int)
			*p.Sucursal = *v.Sucursal
		}
	} else {
		if v.Persona != nil {
			return p, deferror.Validationf("la cuenta %v tiene una persona, pero la misma no abre por persona", cta.Nombre)
		}
	}

	if cta.UsaAplicaciones {
		// Aplica sobre sí misma
		p.PartidaAplicada = new(uuid.UUID)
		*p.PartidaAplicada = p.ID
	}

	if cta.AperturaCentro {
		if v.Centro == nil {
			return p, deferror.Validationf("debe ingresar un centro para la cuenta %v", cta.Nombre)
		}
		p.Centro = new(int)
		*p.Centro = *v.Centro
	}

	// Monenda extrajera
	if cta.MonedaExtranjera {
		if v.Monto < 0 {
			return p, ErrMontoNegativo
		}
		// Moneda
		if v.Moneda == nil {
			return p, ErrFaltaMoneda
		}
		if *v.Moneda == 0 {
			return p, ErrFaltaMoneda
		}
		p.Moneda = new(int)
		*p.Moneda = *v.Moneda

		//
		if v.MontoMonedaOriginal == nil && v.TC == nil {
			return p, ErrFaltaMontoOriginalYTC
		}

		// Calculo monto en moneda original
		if v.MontoMonedaOriginal == nil {
			if *v.TC <= 0 {
				return p, ErrFaltaTipoCambio
			}
			p.MontoMonedaOriginal = new(dec.D4)
			*p.MontoMonedaOriginal = dec.NewD4(v.Monto.Float() / (*v.TC).Float() * float64(signoRenglonValor))
			p.TC = new(dec.D4)
			*p.TC = *v.TC
		} else {
			if *v.MontoMonedaOriginal == 0 {
				return p, ErrFaltaMontoOriginal
			}
			p.TC = new(dec.D4)
			*p.TC = dec.NewD4(
				math.Abs(
					v.Monto.Float() / (*v.MontoMonedaOriginal).Float() * float64(signoRenglonValor),
				),
			)
			p.MontoMonedaOriginal = new(dec.D4)
			*p.MontoMonedaOriginal = dec.NewD4(v.MontoMonedaOriginal.Float() * float64(signoRenglonValor))
		}
	}

	if cta.AperturaUnicidad {
		if v.UnicidadNueva == nil && v.UnicidadRef == nil {
			return p, ErrFaltaUnicidad
		}
	}

	// Es unicidad nueva?
	if v.UnicidadNueva != nil {
		if cta.TipoUnicidad == nil {
			return p, errors.Errorf("cuenta no define tipo de unicidad")
		}
		if *cta.TipoUnicidad != v.UnicidadNueva.Definicion {
			return p, errors.Errorf(
				"la definición de unicidad ingresada (%v) no coincide con la definición establecida para la cuenta (%v)",
				v.UnicidadNueva.Definicion, cta.TipoUnicidad)
		}

		if v.UnicidadNueva.ID == uuid.Nil {
			return p, errors.Wrapf(ErrFaltaID, "en unicidad nueva")
		}
		p.UnicidadTipo = new(int)
		*p.UnicidadTipo = v.UnicidadNueva.Definicion

		p.Unicidad = new(uuid.UUID)
		*p.Unicidad = v.UnicidadNueva.ID

		p.PartidaAplicada = new(uuid.UUID)
		*p.PartidaAplicada = p.ID

		p.FechaFinanciera = v.Vencimiento

	}

	// Es unicidad referenciada?
	if v.UnicidadRef != nil {
		if *v.UnicidadRef == uuid.Nil {
			return p, errors.Wrap(ErrFaltaID, "en unicidad referenciada")
		}
		p.Unicidad = new(uuid.UUID)
		*p.Unicidad = *v.UnicidadRef

		apli, err := uGett.GetPartidaAplicada(ctx, comitente, *v.UnicidadRef)
		if err != nil {
			return p, errors.Wrapf(err, "determinando PartidaAplicada de %v %v", v.Alias, v.Detalle)
		}
		p.PartidaAplicada = &apli
		// TODO: Fecha vencimiento cheque referenciado
	}

	noHayUnicidad := v.UnicidadNueva == nil && v.UnicidadRef == nil
	if noHayUnicidad {
		p.FechaFinanciera = fchComp
	}

	return
}

// Para cada valor pone fecha vencimiento y monto.
func procesarUnicidades(vv []types.PartidaValores, defs map[int]unicidadesdef.Def, fchComp fecha.Fecha) (out []types.PartidaValores, err error) {

	if vv == nil {
		return
	}

	for i, v := range vv {

		noHayUnicidad := v.UnicidadNueva == nil && v.UnicidadRef == nil && v.TipoUnicidad == nil
		if noHayUnicidad {
			v.Vencimiento = fchComp
			out = append(out, v)
			continue
		}
		if v.UnicidadNueva != nil && v.UnicidadRef != nil {
			return out, errors.Errorf("en partida valor renglón %v había unicidad nueva y referenciada", i+1)
		}

		// Unicidad nueva?
		if v.UnicidadNueva != nil {
			if v.UnicidadNueva.Definicion == 0 {
				return out, errors.Errorf("en valores renglón %v unicidad nueva no contiene definición ID", i+1)
			}
			def, ok := defs[v.UnicidadNueva.Definicion]
			if !ok {
				return out, errors.Errorf("renglón %v no se encontró definición ID de unicidad nueva en lu", i+1)
			}
			err = procesarUnicidadDeValorNueva(&v, def, fchComp)
			if err != nil {
				return out, errors.Wrapf(err, "en valores renglon %v, procesando unicidad de valor nueva", i+1)
			}
			out = append(out, v)
			continue
		}

		{ // Unicidad referenciada
			if v.TipoUnicidad == nil {
				return out, errors.Wrapf(err, "no se ingresó unicidad definición ID", i+1)
			}
			if v.UnicidadRef == nil {
				return out, errors.Wrapf(err, "no se ingresó unicidad ID", i+1)
			}
			// TODO: ponerle fecha vencimiento de la unicidad original
			v.Vencimiento = fchComp
			out = append(out, v)
			continue
		}
	}

	return
}

// Valida los datos de la unicidad y pega en el renglón monto, fecha vencimiento y detalle
func procesarUnicidadDeValorNueva(v *types.PartidaValores, def unicidadesdef.Def, fchComp fecha.Fecha) (err error) {

	if v == nil {
		return errors.Errorf("PartidaValor era nil")
	}

	if v.UnicidadNueva == nil {
		return errors.Errorf("UnicidadNueva era nil")
	}
	if v.UnicidadNueva.Definicion == 0 {
		return deferror.Validationf("valor no definió tipo de unicidad")
	}
	if v.UnicidadNueva.Definicion != def.ID {
		return deferror.Validationf("las definiciones no coinciden")
	}

	// Datos unicidad
	v.UnicidadNueva.ID, err = uuid.NewV1()
	if err != nil {
		return err
	}
	v.UnicidadNueva.Comitente = def.Comitente
	v.UnicidadNueva.CreatedAt = new(time.Time)
	*v.UnicidadNueva.CreatedAt = time.Now()

	// Monto - busco el atr"2021-10-31"ibuto de unicidad
	attMonto := unicidades.AttMonto(def, v.UnicidadNueva)
	if attMonto == "" {
		// No hay atributo monto => El monto es el que ingresó el usuario
	} else {
		mto, err := v.UnicidadNueva.AttD2(attMonto)
		if err != nil {
			return err
		}

		// Debería haber venido bien del frontend, por las dudas controlo
		if mto != v.Monto {
			return errors.Errorf("el monto de la unicidad era %v, pero el del renglón era %v", mto, v.Monto)
		}
	}

	// ¿Hay atributo Fecha vencimiento?
	attFch := unicidades.AttFechaDiferida(def, v.UnicidadNueva)
	if attFch == "" {
		v.Vencimiento = fchComp
	} else {
		fchDiferida, err := v.UnicidadNueva.AttFecha(attFch)
		if err != nil {
			return err
		}
		if fchDiferida != 0 {
			if fchDiferida < fchComp {
				// Si esa fecha es anterior a la fecha del recibo => tomo la del recibo
				v.Vencimiento = fchComp
			} else {
				v.Vencimiento = fchDiferida
			}
			// TODO: Corroborar si sirve para algo este campo
			v.UnicidadNueva.Atts["Días"] = fchDiferida.Menos(fchComp)
		}
	}

	// TODO: verificar los campos obligatorios... por ahora
	// confío en el frontend
	// TODO: Detalle
	return
}

// En base a los valores agrupa los vencimientos por fecha
func calcularVencimientosValores(fchRecibo fecha.Fecha, vv []types.PartidaValores) (out types.Vencimientos, err error) {

	if fchRecibo == 0 {
		return out, errors.Errorf("la fecha de recibo no puede ser cero")
	}

	m := map[fecha.Fecha]types.Vencimiento{}
	for i, v := range vv {
		if v.Vencimiento == 0 {
			return out, errors.Errorf("renglón %v de valores no tenía definido fecha de vencimiento", i)
		}
		anterior := m[v.Vencimiento]
		anterior.Monto += dec.D2(v.Monto)
		anterior.Fecha = v.Vencimiento
		anterior.Dias = v.Vencimiento.Menos(fchRecibo)
		m[v.Vencimiento] = anterior
	}

	for _, v := range m {
		out = append(out, v)
	}

	sort.Slice(out, func(i, j int) bool {
		return out[i].Fecha < out[j].Fecha
	})
	return
}
