package reingresopago

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
)

// Datos sobre cuenta de pago a utilizar
type Req struct {
	Cuenta   int
	Centro   *int
	Caja     *int
	Persona  *uuid.UUID
	Sucursal *int
}

// Cuando se hace una fact con reingreso se genera una deuda en empresa 1
// y una contradeuda en empresa 2.
// Esta función genera 2 recibos que cancelan esa deuda.
// De esta manera todo el proceso queda cerrado con una sola operación.
// Es principalmente útil cuando los socios de una empresa traen facturas
// propias que pasan por la empresa y se quedan con el IVA (o parte)
//
// Pega las partidas a las ops, como si fuera una factura contado.
// Por ejemplo con una factura de compra:
//
//  1. Pago en Empresa1
//  2. Cobro en Empresa2
func AgregarReingresosPagos(
	ctx context.Context,
	comitente int,
	opOrig *ops.Op,
	opReing *ops.Op,
	req Req,
	ctasGetter cuentas.ReaderOne) (err error) {

	// Valido
	if opOrig == nil {
		return errors.Errorf("no se ingresó op original")
	}
	if opReing == nil {
		return errors.Errorf("no se ingresó op reingreso")
	}
	if req.Cuenta == 0 {
		return errors.Errorf("no se ingresó cuenta de pago")
	}
	if len(opOrig.PartidasContables) == 0 {
		return errors.Errorf("la op original no tiene partidas contables")
	}
	if len(opReing.PartidasContables) == 0 {
		return errors.Errorf("la op reingreso no tiene partidas contables")
	}

	{ // Genero partidas op original
		pp, err := generarPartidasPago(ctx, comitente, opOrig.PartidasContables, req.Cuenta, req.Centro, req.Caja, req.Persona, req.Sucursal, ctasGetter)
		if err != nil {
			return errors.Wrap(err, "generando reingreso pago en operación original")
		}
		opOrig.PartidasContables = append(opOrig.PartidasContables, pp...)
	}
	{ // Genero partidas op de la segunda empresa
		pp, err := generarPartidasPago(ctx, comitente, opReing.PartidasContables, req.Cuenta, req.Centro, req.Caja, req.Persona, req.Sucursal, ctasGetter)
		if err != nil {
			return errors.Wrap(err, "generando reingreso pago en operación original")
		}
		opReing.PartidasContables = append(opReing.PartidasContables, pp...)
	}

	return
}

func generarPartidasPago(
	ctx context.Context,
	comitente int,
	pp []ops.Partida,
	cuenta int,
	centro *int,
	caja *int,
	persona *uuid.UUID,
	sucursal *int,
	ctaGetter cuentas.ReaderOne) (out []ops.Partida, err error) {

	sumMonto := dec.D2(0)
	for _, v := range pp {
		if v.Pata != types.PataFacturaPatrimonial {
			continue
		}

		v.ID, err = uuid.NewV1()
		if err != nil {
			return
		}
		// Doy vuelta signo
		v.Monto = -v.Monto
		if v.Cantidad != nil {
			*v.Cantidad = -*v.Cantidad
		}
		sumMonto += v.Monto
		out = append(out, v)
	}
	if len(out) == 0 {
		return out, errors.Errorf("no se encontraron partidas para cancelar")
	}
	if sumMonto == 0 {
		return out, errors.Errorf("el monto a cancelar era cero")
	}

	// Partida pago
	p := ops.Partida{}
	p.ID, err = uuid.NewV1()
	if err != nil {
		return
	}

	// Controlo
	cta, err := ctaGetter.ReadOne(context.Background(), cuentas.ReadOneReq{
		Comitente: comitente,
		ID:        cuenta,
	})
	if err != nil {
		return out, errors.Wrap(err, "buscando cuenta de partida pago")
	}
	if cta.AperturaPersona {
		if persona == nil {
			return out, errors.Errorf("no se ingresó persona para cuenta '%v'", cta.Nombre)
		}
	}
	if cta.AperturaCentro {
		if centro == nil {
			return out, errors.Errorf("no se ingresó centro para cuenta '%v'", cta.Nombre)
		}
	}
	if cta.AperturaCaja {
		if caja == nil {
			return out, errors.Errorf("no se ingresó caja para cuenta '%v'", cta.Nombre)
		}
	}
	if cta.UsaAplicaciones {
		p.PartidaAplicada = new(uuid.UUID)
		*p.PartidaAplicada, err = uuid.NewV1()
		if err != nil {
			return out, err
		}
	}

	// Controlo cuenta pago
	p.Monto = -sumMonto
	p.Cuenta = cuenta
	if centro != nil {
		p.Centro = new(int)
		*p.Centro = *centro
	}
	if caja != nil {
		p.Caja = new(int)
		*p.Caja = *caja
	}
	if persona != nil {
		p.Persona = new(uuid.UUID)
		*p.Persona = *persona
	}
	if sucursal != nil {
		p.Sucursal = new(int)
		*p.Sucursal = *sucursal
	}
	p.Pata = types.PataReciboPatrimonial

	out = append(out, p)

	return
}
