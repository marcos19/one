package reingresopago

import (
	"context"
	"testing"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestReingresoPago(t *testing.T) {

	t.Run("Pago con efectivo", func(t *testing.T) {
		Caja := 1
		req := Req{
			Cuenta: 123,
			Caja:   &Caja,
		}

		o1 := ops.Op{PartidasContables: []ops.Partida{
			{
				Pata:   types.PataProducto,
				Cuenta: 9001,
				Monto:  100,
			},
			{
				Pata:   types.PataIVA,
				Cuenta: 2300,
				Monto:  21,
			},
			{
				Pata:   types.PataFacturaPatrimonial,
				Cuenta: 2200,
				Monto:  -121,
			},
		},
		}
		o2 := ops.Op{PartidasContables: []ops.Partida{
			{
				Pata:   types.PataProducto,
				Cuenta: 9001,
				Monto:  -100,
			},
			{
				Pata:   types.PataIVA,
				Cuenta: 2300,
				Monto:  -21,
			},
			{
				Pata:   types.PataReingreso,
				Cuenta: 9900,
				Monto:  21,
			},
			{
				Pata:   types.PataFacturaPatrimonial,
				Cuenta: 2200,
				Monto:  100,
			},
		}}

		err := AgregarReingresosPagos(context.Background(),
			1,
			&o1,
			&o2,
			req,
			ctaGetterMock{},
		)
		require.Nil(t, err)
		require.Len(t, o1.PartidasContables, 5)
		assert.Equal(t, dec.D2(121), o1.PartidasContables[3].Monto)
		assert.Equal(t, dec.D2(-121), o1.PartidasContables[4].Monto)
		assert.Nil(t, o1.PartidasContables[4].Persona)
		assert.Nil(t, o1.PartidasContables[4].Sucursal)
		assert.Nil(t, o1.PartidasContables[4].Centro)
		require.NotNil(t, o1.PartidasContables[4].Caja)
		assert.Equal(t, 1, *o1.PartidasContables[4].Caja)

		require.Len(t, o2.PartidasContables, 6)
		assert.Equal(t, dec.D2(-100), o2.PartidasContables[4].Monto)
		assert.Equal(t, dec.D2(100), o2.PartidasContables[5].Monto)
		assert.Nil(t, o2.PartidasContables[5].Persona)
		assert.Nil(t, o2.PartidasContables[5].Sucursal)
		assert.Nil(t, o2.PartidasContables[5].Centro)
		require.NotNil(t, o2.PartidasContables[5].Caja)
		assert.Equal(t, 1, *o2.PartidasContables[5].Caja)
	})

	t.Run("Pago con cuenta corriente", func(t *testing.T) {
		Persona := uuid.FromStringOrNil("6b07aef7-ca67-45d1-a4e0-2b78653da1a2")
		req := Req{
			Cuenta:  1210,
			Persona: &Persona,
		}

		o1 := ops.Op{PartidasContables: []ops.Partida{
			{
				Pata:   types.PataProducto,
				Cuenta: 9001,
				Monto:  100,
			},
			{
				Pata:   types.PataIVA,
				Cuenta: 2300,
				Monto:  21,
			},
			{
				Pata:   types.PataFacturaPatrimonial,
				Cuenta: 2200,
				Monto:  -121,
			},
		},
		}
		o2 := ops.Op{PartidasContables: []ops.Partida{
			{
				Pata:   types.PataProducto,
				Cuenta: 9001,
				Monto:  -100,
			},
			{
				Pata:   types.PataIVA,
				Cuenta: 2300,
				Monto:  -21,
			},
			{
				Pata:   types.PataReingreso,
				Cuenta: 9900,
				Monto:  21,
			},
			{
				Pata:   types.PataFacturaPatrimonial,
				Cuenta: 2200,
				Monto:  100,
			},
		}}

		err := AgregarReingresosPagos(context.Background(),
			1,
			&o1,
			&o2,
			req,
			ctaGetterMock{},
		)
		require.Nil(t, err)
		require.Len(t, o1.PartidasContables, 5)
		assert.Equal(t, dec.D2(121), o1.PartidasContables[3].Monto)
		assert.Equal(t, dec.D2(-121), o1.PartidasContables[4].Monto)
		require.NotNil(t, o1.PartidasContables[4].Persona)
		require.Equal(t, Persona, *o1.PartidasContables[4].Persona)
		assert.Nil(t, o1.PartidasContables[4].Sucursal)
		assert.Nil(t, o1.PartidasContables[4].Centro)
		assert.Nil(t, o1.PartidasContables[4].Caja)

		require.Len(t, o2.PartidasContables, 6)
		assert.Equal(t, dec.D2(-100), o2.PartidasContables[4].Monto)
		assert.Equal(t, dec.D2(100), o2.PartidasContables[5].Monto)
		require.NotNil(t, o2.PartidasContables[5].Persona)
		assert.Equal(t, Persona, *o2.PartidasContables[5].Persona)
		assert.Nil(t, o2.PartidasContables[5].Sucursal)
		assert.Nil(t, o2.PartidasContables[5].Centro)
		assert.Nil(t, o2.PartidasContables[5].Caja)
	})
}

type ctaGetterMock struct{}

func (c ctaGetterMock) PorFuncionContable(ctx context.Context, comitente int, f cuentas.Funcion) ([]cuentas.Cuenta, error) {
	return nil, nil
}
func (c ctaGetterMock) ReadOne(ctx context.Context, req cuentas.ReadOneReq) (cuentas.Cuenta, error) {
	switch req.ID {
	case 9001:
		return cuentas.Cuenta{
			ID:               req.ID,
			Nombre:           "Ventas",
			AperturaPersona:  true,
			AperturaProducto: true,
		}, nil
	case 2300:
		return cuentas.Cuenta{
			ID:     req.ID,
			Nombre: "IVA DF",
		}, nil
	case 9900:
		return cuentas.Cuenta{
			ID:     req.ID,
			Nombre: "IVA reingresado",
		}, nil
	case 2200:
		return cuentas.Cuenta{
			ID:     req.ID,
			Nombre: "Cuentas a cobrar",
		}, nil
	case 123:
		return cuentas.Cuenta{
			ID:           req.ID,
			Nombre:       "Caja",
			AperturaCaja: true,
		}, nil
	case 1210:
		return cuentas.Cuenta{
			ID:              req.ID,
			Nombre:          "Cuentas particulares socios",
			AperturaPersona: true,
		}, nil
	}
	panic(errors.Errorf("cuenta %v not implemented", req.ID))
}
