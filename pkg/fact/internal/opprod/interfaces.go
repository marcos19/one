package opprod

// RegistradorEstandar determina las cuentas que van al debe y al haber.
// Que una opción haga o no haga asiento estándar no depende de a qué
// esté aplicando. Por ejemplo una orden de compra nunca hace asiento
// estándar y por eso no implementa esta interface.
type RegistradorEstandar interface {
	CuentaDebe(ctaAplicada int) (out int, nova bool, err error)
	CuentaHaber(ctaAplicada int) (out int, nova bool, err error)
	Pata(ctaID int) (pata string, err error)
}

// RegistradorPendientes determina si corresponde hacer asiento pendientes,
// y las cuentas que debe utilizar.
type RegistradorPendientes interface {

	// Depende del tipo de opprod y de a qué está aplicando.
	HaceAsientoPendientes(ctaAplicada int) (out bool, err error)

	// Si se llama a este método con una cuenta que no HaceAsientoPendientes
	// devuelve error.
	CuentaDebePendientes(ctaAplicada int) (out int, nova bool, err error)

	// Si se llama a este método con una cuenta que no HaceAsientoPendientes
	// devuelve error.
	CuentaHaberPendientes(ctaAplicada int) (out int, nova bool, err error)
}
