package opprod

import (
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"github.com/pkg/errors"
)

// Factura que NO mueve stock
type FacturaDeVenta struct{}

var _ RegistradorEstandar = new(FacturaDeVenta)
var _ RegistradorPendientes = new(FacturaDeVenta)

func (o FacturaDeVenta) HaceAsientoEstandar(ctaProductoAplicado int) (out bool, err error) {
	return true, nil
}

func (o FacturaDeVenta) CuentaDebe(ctaProductoAplicado int) (out int, nova bool, err error) {
	return Clientes, true, nil
}

func (o FacturaDeVenta) CuentaHaber(ctaProductoAplicado int) (out int, nova bool, err error) {
	switch ctaProductoAplicado {

	case FacturasAEmitir:
		return FacturasAEmitir, false, nil

	case 0, PedidosDeClientesAEntregar:
		return ProductosFacturadosAEntregar, true, nil
	}
	return out, nova, errors.Errorf("no se pudo determinar CuentaDebe para 'Factura de Compra' que aplica a cuenta %v", ctaProductoAplicado)
}

func (o FacturaDeVenta) Pata(ctaID int) (out string, err error) {
	switch ctaID {
	case Clientes:
		return types.PataFacturaPatrimonial, nil

	case FacturasAEmitir, ProductosFacturadosAEntregar:
		return types.PataProducto, nil
	}
	return out, errors.Errorf("'%v' no tiene definida pata contable para '%v'", o, NombreCuenta(ctaID))
}

func (r FacturaDeVenta) String() string {
	return "Factura de venta"
}

func (o FacturaDeVenta) HaceAsientoPendientes(ctaProductoAplicado int) (out bool, err error) {
	switch ctaProductoAplicado {
	case 0:
		// No aplica a nada
		return false, nil
	}
	return true, nil
}

func (o FacturaDeVenta) CuentaDebePendientes(ctaProductoAplicado int) (out int, nova bool, err error) {
	switch ctaProductoAplicado {
	case 0:
		// Remito que NO aplica a una orden de compra => No toca pendientes
		return out, nova, errors.Errorf("factura de venta no hace pendientes cuando no aplica")

	case PedidosDeClientesAEntregar:
		return PedidosDeClientesAEntregar, false, nil
	}

	return out, nova, errors.Errorf("no se pudo determinar CuentaDebePendientes para Factura de Compra que aplica a cuenta %v", ctaProductoAplicado)
}

func (o FacturaDeVenta) CuentaHaberPendientes(ctaProductoAplicado int) (out int, nova bool, err error) {

	switch ctaProductoAplicado {

	case 0:
		// Remito que NO aplica a una orden de compra => No toca pendientes
		return out, nova, errors.Errorf("remito de compra no hace pendientes cuando no aplica")

	case PedidosDeClientesAEntregar:
		return PedidosDeClientesFacturados, false, nil
	}

	return out, nova, errors.Errorf("no se pudo determinar CuentaHaberPendientes para Factura de Compra que aplica a cuenta %v", ctaProductoAplicado)
}
