package opprod

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFacturaCompraDirecta(t *testing.T) {
	assert := assert.New(t)

	o := FacturaDeCompraDirecta{}
	var hace bool
	var err error
	var cta int
	var nova bool

	{ // Estandar
		hace, err = o.HaceAsientoEstandar(0)
		assert.Nil(err)
		assert.Equal(true, hace)

		hace, err = o.HaceAsientoEstandar(ProductosSolicitadosARecibir)
		assert.Nil(err)
		assert.Equal(true, hace)

		{ // No aplica a nada
			cta, nova, err = o.CuentaDebe(0)
			assert.Nil(err)
			assert.False(nova)
			assert.Equal(Productos, cta)

			cta, nova, err = o.CuentaHaber(0)
			assert.Nil(err)
			assert.True(nova)
			assert.Equal(Proveedores, cta)
		}

		{ // Aplica a orden de compra
			cta, nova, err = o.CuentaDebe(ProductosCompradosARecibir)
			assert.Nil(err)
			assert.False(nova)
			assert.Equal(Productos, cta)

			cta, nova, err = o.CuentaHaber(ProductosCompradosARecibir)
			assert.Nil(err)
			assert.True(nova)
			assert.Equal(Proveedores, cta)
		}
	}

	{ // Pendientes
		hace, err = o.HaceAsientoPendientes(0)
		assert.Nil(err)
		assert.Equal(false, hace)

		hace, err = o.HaceAsientoPendientes(ProductosSolicitadosARecibir)
		assert.Nil(err)
		assert.Equal(true, hace)

		cta, nova, err = o.CuentaDebePendientes(0)
		assert.NotNil(err)
		assert.False(nova)
		assert.Equal(0, cta)

		cta, nova, err = o.CuentaHaberPendientes(0)
		assert.NotNil(err)
		assert.False(nova)
		assert.Equal(0, cta)

		cta, nova, err = o.CuentaDebePendientes(ProductosSolicitadosARecibir)
		assert.Nil(err)
		assert.False(nova)
		assert.Equal(ProductosSolicitadosRecibidos, cta)

		cta, nova, err = o.CuentaHaberPendientes(ProductosSolicitadosARecibir)
		assert.Nil(err)
		assert.False(nova)
		assert.Equal(ProductosSolicitadosARecibir, cta)

		{ // Aplica a otra cosa (no debería)
			cta, nova, err = o.CuentaDebePendientes(999)
			assert.NotNil(err)
			assert.False(nova)
			assert.Equal(0, cta)

			cta, nova, err = o.CuentaHaberPendientes(999)
			assert.NotNil(err)
			assert.False(nova)
			assert.Equal(0, cta)
		}
	}
}
