graph TD
    A(Orden de compra) --> B{ }
    B --> C[Recepción]
    
    B --> G[Factura]
    G --> H(Recepción)
    B --> D(Factura directa)
    B --> E(Anulación)
    C --> F(Factura)
