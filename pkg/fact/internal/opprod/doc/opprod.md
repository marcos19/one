# Asientos ciclo de compras de productos

## Orden de compra

Hace un asiento de pendientes solamente

| Cuenta                                                | Debe | Haber |
| ----------------------------------------------------- | ---: | ----: |
| Productos solicitados a recibir _(orden patrimonial)_ |  100 |     0 |
| a Productos solicitados _(orden resultado)_           |    0 |   100 |

## Orden de compra anulación

Hace un asiento de pendientes solamente

| Cuenta                                                  | Debe | Haber |
| ------------------------------------------------------- | ---: | ----: |
| Productos solicitados cancelados _(orden resultado)_    |  100 |     0 |
| a Productos solicitados a recibir _(orden patrimonial)_ |    0 |   100 |

## Remito de compra

Si no aplica a nada:

| Cuenta                          | Debe | Haber |
| ------------------------------- | ---: | ----: |
| Productos _(activo)_            |  100 |     0 |
| a Facturas a recibir _(pasivo)_ |    0 |   100 |

Si aplica a una factura anticipada:

| Cuenta                                     | Debe | Haber |
| ------------------------------------------ | ---: | ----: |
| Productos _(activo)_                       |  100 |     0 |
| a Productos comprados a recibir _(pasivo)_ |    0 |   100 |

Si aplica a una orden de compra:

| Cuenta                                                | Debe | Haber |
| ----------------------------------------------------- | ---: | ----: |
| Productos _(activo)_                                  |  100 |     0 |
| a Facturas a recibir _(pasivo)_                       |    0 |   100 |
| Productos solicitados a recibir _(orden patrimonial)_ |  100 |     0 |
| a Productos solicitados recibidos _(orden resultado)_ |    0 |   100 |

## Remito de compra (anulación)

Si el proveedor todavía no lo facturó, aplica al remito original.

| Cuenta                        | Debe | Haber |
| ----------------------------- | ---: | ----: |
| Facturas a recibir _(pasivo)_ |  100 |     0 |
| a Productos _(activo)_        |    0 |   100 |

Si el proveedor ya lo facturó, hace una nota de crédito. No aplica a nada.

| Cuenta                        | Debe | Haber |
| ----------------------------- | ---: | ----: |
| Facturas a recibir _(pasivo)_ |  100 |     0 |
| a Productos _(activo)_        |    0 |   100 |

## Factura compra directa

No aplica a nada.

| Cuenta                   | Debe | Haber |
| ------------------------ | ---: | ----: |
| Productos _(activo)_     |  100 |     0 |
| IVA CF                   |   21 |     0 |
| a proveedores _(pasivo)_ |    0 |   121 |

## Factura compra (no directa)

Si aplica a un remito:

| Cuenta                        | Debe | Haber |
| ----------------------------- | ---: | ----: |
| Facturas a recibir _(pasivo)_ |  100 |     0 |
| IVA CF                        |   21 |     0 |
| a proveedores _(pasivo)_      |    0 |   121 |

Si aplica a una orden de compra:

| Cuenta                                   | Debe | Haber |
| ---------------------------------------- | ---: | ----: |
| Productos comprados a recibir _(activo)_ |  100 |     0 |
| IVA CF                                   |   21 |     0 |
| a proveedores _(pasivo)_                 |    0 |   121 |

| Cuenta                                                | Debe | Haber |
| ----------------------------------------------------- | ---: | ----: |
| Productos solicitados facturados _(orden resultado)_  |  100 |     0 |
| Productos solicitados a recibir _(orden patrimonial)_ |    0 |   121 |
