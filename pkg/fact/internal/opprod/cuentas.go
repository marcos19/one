// Package opprod permite determinar si una fact con productos:
//
//	1) Debe hacer asiento estándar;
//	2) Debe hacer asiento de pendientes;
//	3) Que cuéntas debe utilizar.
//
// Las cuentas contables son determinadas en base al tipo de opprod
// (remito, factura, etc) y a las cuentas de las partidas que está
// aplicando.
//
// Las cuentas que utiliza son internas de este package.
// La implementación debe mappear estas cuentas con las del plan
// contable del comitente.
package opprod

import (
	"sort"
	"strconv"

	"bitbucket.org/marcos19/one/pkg/fact/types"
	"github.com/pkg/errors"
)

// Exclusivas de COMPRAS
const (
	// El pendiente que genera la orden de compra
	ProductosSolicitadosARecibir = 100

	// La cuenta flujo que genera la orden de compra
	ProductosSolicitados = 101

	// La cuenta flujo que genera la anulación de orden de compra
	ProductosSolicitadosCancelados = 102

	// La cuenta flujo que genera la recepción de los productos de una orden de compra
	ProductosSolicitadosRecibidos = 103

	// Cuando recibo factura por productos solicitados, ProductosSolicitadosARecibir
	// desaparaece, el pendiente me queda en el asiento estándar como ProductosCompradosARecibir.
	// Esta es la cuenta flujo que uso para matar el ProductosSolicitadosARecibir
	ProductosSolicitadosFacturados = 104

	// El pendiente que genera un remito de compra (sin factura)
	FacturasARecibir = 105

	// El pendiente que genera una factura de compra (sin remito)
	//
	ProductosCompradosARecibir = 106

	NotasDeCreditoARecibir = 107
)

// Exclusivas de VENTAS
const (
	// El pendiente que genera la nota de pedido
	PedidosDeClientesAEntregar = 200

	// La cuenta flujo que genera la nota de pedido
	PedidosDeClientes = 201

	// El pendiente que genera un remito de venta (sin factura)
	FacturasAEmitir = 202

	// Cuenta de pasivo cuando se factura un producto que todavía no se entregó
	ProductosFacturadosAEntregar = 203

	// Cuenta flujo para matar PedidosDeClientesAEntregar
	PedidosDeClientesEntregados = 204

	// Cuenta flujo para matar PedidosDeClientesAEntregar cuando se hace una anulación de pedido
	PedidosDeClientesCancelados = 205

	// Cuenta flujo para matar PedidosDeClientesAEntregar
	PedidosDeClientesFacturados = 206

	// Cuando hago un remito sobre un producto que ya estaba facturado
	NotasDeCreditoAEmitir = 207
)

const (
	Productos   = 1
	Proveedores = 2
	IVA         = 3
	Ventas      = 4
	Clientes    = 5
)

type Concepto struct {
	ID          int `json:",string"`
	Nombre      string
	Ciclo       string
	Descripcion string `json:",omitempty"`
}

var conceptos []Concepto
var conceptosMap map[int]Concepto

func Conceptos() []Concepto {
	if len(conceptos) != 0 {
		return conceptos
	}

	conceptos = []Concepto{
		{
			ID:          ProductosSolicitadosARecibir,
			Nombre:      "Productos solicitados a recibir",
			Ciclo:       "Compras",
			Descripcion: "El pendiente que genera la orden de compra",
		},
		{
			ID:          ProductosSolicitados,
			Nombre:      "Productos solicitados",
			Ciclo:       "Compras",
			Descripcion: "La cuenta flujo que genera la orden de compra",
		},
		{
			ID:          ProductosSolicitadosCancelados,
			Nombre:      "Productos solicitados cancelados",
			Ciclo:       "Compras",
			Descripcion: "La cuenta flujo que genera la anulación de orden de compra",
		},
		{
			ID:          ProductosSolicitadosRecibidos,
			Nombre:      "Productos solicitados recibidos",
			Ciclo:       "Compras",
			Descripcion: "La cuenta flujo que genera la recepción de los productos de una orden de compra",
		},
		{
			ID:          ProductosSolicitadosFacturados,
			Nombre:      "Productos solicitados facturados",
			Ciclo:       "Compras",
			Descripcion: `Cuando recibo factura por productos solicitados, ProductosSolicitadosARecibir desaparaece, el pendiente me queda en el asiento estándar como ProductosCompradosARecibir. Esta es la cuenta flujo que uso para matar el ProductosSolicitadosARecibir`,
		},
		{
			ID:          FacturasARecibir,
			Nombre:      "Facturas a recibir",
			Ciclo:       "Compras",
			Descripcion: "El pendiente que genera un remito de compra (sin factura)",
		},
		{
			ID:          ProductosCompradosARecibir,
			Nombre:      "Productos comprados a recibir",
			Ciclo:       "Compras",
			Descripcion: "El pendiente que genera una factura de compra (sin remito)",
		},
		{
			ID:          NotasDeCreditoARecibir,
			Nombre:      "Notas de crédito a recibir",
			Ciclo:       "Compras",
			Descripcion: "Cuando se hace un remito devolución a un proveedor de algo que ya está facturado.",
		},
		{
			ID:     Proveedores,
			Nombre: "Proveedores",
			Ciclo:  "Compras",
		},
		{
			ID:     IVA,
			Nombre: "IVA",
		},
		{
			ID:     Productos,
			Nombre: "Productos",
		},
		{
			ID:     Ventas,
			Nombre: "Ventas",
		},
		{
			ID:     Clientes,
			Nombre: "Clientes",
		},

		{
			ID:          PedidosDeClientesAEntregar,
			Nombre:      "Pedidos de clientes a entregar",
			Ciclo:       "Ventas",
			Descripcion: "El pendiente que genera la nota de pedido.",
		},
		{
			ID:          PedidosDeClientes,
			Nombre:      "Pedidos de clientes",
			Ciclo:       "Ventas",
			Descripcion: "La cuenta flujo que genera la nota de pedido",
		},
		{
			ID:          FacturasAEmitir,
			Nombre:      "Facturas a emitir",
			Ciclo:       "Ventas",
			Descripcion: "El pendiente que genera un remito de venta (sin factura)",
		},
		{
			ID:          ProductosFacturadosAEntregar,
			Nombre:      "Productos facturados a entregar",
			Ciclo:       "Ventas",
			Descripcion: "Cuenta de pasivo cuando se factura un producto que todavía no se entregó",
		},
		{
			ID:          PedidosDeClientesEntregados,
			Nombre:      "Pedidos de clientes entregados",
			Ciclo:       "Ventas",
			Descripcion: "Cuenta de flujo que hace el remito venta para matar PedidosDeClientesAEntregar",
		},
		{
			ID:          PedidosDeClientesCancelados,
			Nombre:      "Pedidos de clientes cancelados",
			Ciclo:       "Ventas",
			Descripcion: "Cuenta flujo para matar PedidosDeClientesAEntregar cuando se hace una anulación de pedido",
		},
		{
			ID:          PedidosDeClientesFacturados,
			Nombre:      "Pedidos de clientes facturados",
			Ciclo:       "Ventas",
			Descripcion: "Cuenta de flujo que hace la factura anticipada para matar PedidosDeClientesAEntregar",
		},
		{
			ID:          NotasDeCreditoAEmitir,
			Nombre:      "Notas de crédito a emitir",
			Ciclo:       "Ventas",
			Descripcion: "Cuando un cliente me devuelve mercadería y la ingreso con remito",
		},
	}

	sort.Slice(conceptos, func(i, j int) bool {
		return conceptos[i].Nombre < conceptos[j].Nombre
	})

	conceptosMap = map[int]Concepto{}
	for _, v := range conceptos {
		conceptosMap[v.ID] = v
	}
	return conceptos
}

func NombreCuenta(id int) string {
	if conceptosMap == nil {
		Conceptos()
	}

	c, ok := conceptosMap[id]
	if !ok {
		return strconv.Itoa(id)
	}
	return c.Nombre
}

func PataCuenta(id int) (string, error) {

	switch id {

	case ProductosSolicitadosARecibir, PedidosDeClientesAEntregar:
		return types.PataPendienteStock, nil

	case
		ProductosSolicitados,
		ProductosSolicitadosCancelados,
		ProductosSolicitadosRecibidos,
		ProductosSolicitadosFacturados,
		PedidosDeClientes,
		PedidosDeClientesCancelados,
		PedidosDeClientesEntregados,
		PedidosDeClientesFacturados:
		return types.PataPendienteFlujo, nil
	}

	return "", errors.Errorf("cuenta opprod %v no tiene definida pata de asiento", id)
}
