package opprod

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFacturaCompra(t *testing.T) {
	assert := assert.New(t)

	o := FacturaDeCompra{}
	var hace bool
	var err error
	var cta int
	var nova bool

	{ // Estandar
		hace, err = o.HaceAsientoEstandar(0)
		assert.Nil(err)
		assert.Equal(true, hace)

		hace, err = o.HaceAsientoEstandar(ProductosSolicitadosARecibir)
		assert.Nil(err)
		assert.Equal(true, hace)

		{ // No aplica a nada
			cta, nova, err = o.CuentaDebe(0)
			assert.Nil(err)
			assert.True(nova)
			assert.Equal(ProductosCompradosARecibir, cta)

			cta, nova, err = o.CuentaHaber(0)
			assert.Nil(err)
			assert.True(nova)
			assert.Equal(Proveedores, cta)
		}

		{ // Aplica a orden de compra
			cta, nova, err = o.CuentaDebe(ProductosSolicitadosARecibir)
			assert.Nil(err)
			assert.True(nova)
			assert.Equal(ProductosCompradosARecibir, cta)

			cta, nova, err = o.CuentaHaber(ProductosSolicitadosARecibir)
			assert.Nil(err)
			assert.True(nova)
			assert.Equal(Proveedores, cta)
		}
		{ // Aplica a remito
			cta, nova, err = o.CuentaDebe(FacturasARecibir)
			assert.Nil(err)
			assert.False(nova)
			assert.Equal(FacturasARecibir, cta)

			cta, nova, err = o.CuentaHaber(FacturasARecibir)
			assert.Nil(err)
			assert.True(nova)
			assert.Equal(Proveedores, cta)
		}
	}

	{ // Pendientes
		hace, err = o.HaceAsientoPendientes(0)
		assert.Nil(err)
		assert.Equal(false, hace)

		hace, err = o.HaceAsientoPendientes(ProductosSolicitadosARecibir)
		assert.Nil(err)
		assert.Equal(true, hace)

		cta, nova, err = o.CuentaDebePendientes(0)
		assert.NotNil(err)
		assert.False(nova)
		assert.Equal(0, cta)

		cta, nova, err = o.CuentaHaberPendientes(0)
		assert.NotNil(err)
		assert.False(nova)
		assert.Equal(0, cta)

		// Estoy recibiendo factura de un producto que encargué pero que todavía no recibí
		//
		cta, nova, err = o.CuentaDebePendientes(ProductosSolicitadosARecibir)
		assert.Nil(err)
		assert.False(nova)
		assert.Equal(ProductosSolicitadosFacturados, cta)

		cta, nova, err = o.CuentaHaberPendientes(ProductosSolicitadosARecibir)
		assert.Nil(err)
		assert.False(nova)
		assert.Equal(ProductosSolicitadosARecibir, cta)
	}
}
