package opprod

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRemitoCompraAnulacion(t *testing.T) {
	assert := assert.New(t)

	o := RemitoDeCompraAnulacion{}
	var hace bool
	var err error
	var cta int
	var nova bool

	{ // Estandar
		hace, err = o.HaceAsientoEstandar(0)
		assert.Nil(err)
		assert.Equal(true, hace)

		hace, err = o.HaceAsientoEstandar(FacturasARecibir)
		assert.Nil(err)
		assert.Equal(true, hace)

		{ // Remito compra que no aplica a nada
			cta, nova, err = o.CuentaDebe(0)
			assert.Nil(err)
			assert.True(nova)
			assert.Equal(NotasDeCreditoARecibir, cta)

			cta, nova, err = o.CuentaHaber(0)
			assert.Nil(err)
			assert.False(nova)
			assert.Equal(Productos, cta)
		}

		{ // Remito sobre factura anticipada
			cta, nova, err = o.CuentaDebe(FacturasARecibir)
			assert.Nil(err)
			assert.False(nova)
			assert.Equal(FacturasARecibir, cta)

			cta, nova, err = o.CuentaHaber(FacturasARecibir)
			assert.Nil(err)
			assert.False(nova)
			assert.Equal(Productos, cta)
		}
	}

	{ // Pendientes
		// hace, err = o.HaceAsientoPendientes(0)
		// assert.Nil(err)
		// assert.Equal(false, hace)

		// hace, err = o.HaceAsientoPendientes(ProductosSolicitadosARecibir)
		// assert.Nil(err)
		// assert.Equal(true, hace)

		// cta, err = o.CuentaDebePendientes(0)
		// assert.NotNil(err)
		// assert.Equal(0, cta)

		// cta, err = o.CuentaHaberPendientes(0)
		// assert.NotNil(err)
		// assert.Equal(0, cta)

		// cta, err = o.CuentaDebePendientes(ProductosSolicitadosARecibir)
		// assert.Nil(err)
		// assert.Equal(ProductosSolicitadosRecibidos, cta)

		// cta, err = o.CuentaHaberPendientes(ProductosSolicitadosARecibir)
		// assert.Nil(err)
		// assert.Equal(ProductosSolicitadosARecibir, cta)
	}

}
