package opprod

import "github.com/pkg/errors"

type OrdenDeCompra struct{}

var _ RegistradorPendientes = new(OrdenDeCompra)

func (o OrdenDeCompra) HaceAsiento(ctaAplicada int) (out bool, err error) {
	if ctaAplicada == 0 {
		return false, nil
	}
	return false, errors.Errorf("orden de compra no debería aplicar a ninguna cuenta")
}
func (o OrdenDeCompra) HaceAsientoPendientes(ctaAplicada int) (out bool, err error) {
	if ctaAplicada == 0 {
		return true, nil
	}
	return out, errors.Errorf("orden de compra no debería aplicar a ninguna cuenta")
}

func (o OrdenDeCompra) CuentaDebePendientes(ctaAplicada int) (out int, nova bool, err error) {
	return ProductosSolicitadosARecibir, true, nil
}
func (o OrdenDeCompra) CuentaHaberPendientes(ctaAplicada int) (out int, nova bool, err error) {
	return ProductosSolicitados, false, nil
}
