package opprod

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFacturaVenta(t *testing.T) {
	assert := assert.New(t)

	o := FacturaDeVenta{}
	var hace bool
	var err error
	var cta int
	var nova bool

	{ // Estandar
		hace, err = o.HaceAsientoEstandar(0)
		assert.Nil(err)
		assert.Equal(true, hace)

		hace, err = o.HaceAsientoEstandar(PedidosDeClientesAEntregar)
		assert.Nil(err)
		assert.Equal(true, hace)

		hace, err = o.HaceAsientoEstandar(FacturasAEmitir)
		assert.Nil(err)
		assert.Equal(true, hace)

		{ // No aplica a nada
			cta, nova, err = o.CuentaDebe(0)
			assert.Nil(err)
			assert.True(nova)
			assert.Equal(Clientes, cta)

			cta, nova, err = o.CuentaHaber(0)
			assert.Nil(err)
			assert.True(nova)
			assert.Equal(ProductosFacturadosAEntregar, cta)
		}

		{ // Aplica a remito
			cta, nova, err = o.CuentaDebe(FacturasAEmitir)
			assert.Nil(err)
			assert.True(nova)
			assert.Equal(Clientes, cta)

			cta, nova, err = o.CuentaHaber(FacturasAEmitir)
			assert.Nil(err)
			assert.False(nova)
			assert.Equal(FacturasAEmitir, cta)
		}
		{ // Aplica a remito
			cta, nova, err = o.CuentaDebe(PedidosDeClientesAEntregar)
			assert.Nil(err)
			assert.True(nova)
			assert.Equal(Clientes, cta)

			cta, nova, err = o.CuentaHaber(PedidosDeClientesAEntregar)
			assert.Nil(err)
			assert.True(nova)
			assert.Equal(ProductosFacturadosAEntregar, cta)
		}
	}

	{ // Pendientes
		hace, err = o.HaceAsientoPendientes(0)
		assert.Nil(err)
		assert.Equal(false, hace)

		hace, err = o.HaceAsientoPendientes(FacturasAEmitir)
		assert.Nil(err)
		assert.Equal(true, hace)

		hace, err = o.HaceAsientoPendientes(PedidosDeClientesAEntregar)
		assert.Nil(err)
		assert.Equal(true, hace)

		_, _, err = o.CuentaDebePendientes(0)
		assert.NotNil(err)

		_, _, err = o.CuentaHaberPendientes(0)
		assert.NotNil(err)

		//
		cta, nova, err = o.CuentaDebePendientes(PedidosDeClientesAEntregar)
		assert.Nil(err)
		assert.False(nova)
		assert.Equal(PedidosDeClientesAEntregar, cta)

		cta, nova, err = o.CuentaHaberPendientes(PedidosDeClientesAEntregar)
		assert.Nil(err)
		assert.False(nova)
		assert.Equal(PedidosDeClientesFacturados, cta)
	}
}
