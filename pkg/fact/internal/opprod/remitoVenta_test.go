package opprod

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRemitoVenta(t *testing.T) {
	assert := assert.New(t)

	o := RemitoDeVenta{}
	var hace bool
	var err error
	var cta int
	var nova bool

	{ // Estandar
		hace, err = o.HaceAsientoEstandar(0)
		assert.Nil(err)
		assert.Equal(true, hace)

		hace, err = o.HaceAsientoEstandar(PedidosDeClientesAEntregar)
		assert.Nil(err)
		assert.Equal(true, hace)

		{ // Remito compra que no aplica a nada
			cta, nova, err = o.CuentaDebe(0)
			assert.Nil(err)
			assert.True(nova)
			assert.Equal(FacturasAEmitir, cta)

			cta, nova, err = o.CuentaHaber(0)
			assert.Nil(err)
			assert.False(nova)
			assert.Equal(Ventas, cta)
		}

		{ // Remito sobre factura anticipada
			cta, nova, err = o.CuentaDebe(ProductosFacturadosAEntregar)
			assert.Nil(err)
			assert.False(nova)
			assert.Equal(ProductosFacturadosAEntregar, cta)

			cta, nova, err = o.CuentaHaber(ProductosFacturadosAEntregar)
			assert.Nil(err)
			assert.False(nova)
			assert.Equal(Ventas, cta)
		}
	}

	{ // Pendientes
		hace, err = o.HaceAsientoPendientes(0)
		assert.Nil(err)
		assert.Equal(false, hace)

		hace, err = o.HaceAsientoPendientes(PedidosDeClientesAEntregar)
		assert.Nil(err)
		assert.Equal(true, hace)

		cta, nova, err = o.CuentaDebePendientes(0)
		assert.NotNil(err)
		assert.False(nova)
		assert.Equal(0, cta)

		cta, nova, err = o.CuentaHaberPendientes(0)
		assert.NotNil(err)
		assert.False(nova)
		assert.Equal(0, cta)

		cta, nova, err = o.CuentaDebePendientes(PedidosDeClientesAEntregar)
		assert.Nil(err)
		assert.False(nova)
		assert.Equal(PedidosDeClientesAEntregar, cta)

		cta, nova, err = o.CuentaHaberPendientes(PedidosDeClientesAEntregar)
		assert.Nil(err)
		assert.False(nova)
		assert.Equal(PedidosDeClientesEntregados, cta)
	}

}
