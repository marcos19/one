package opprod

import (
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"github.com/pkg/errors"
)

type FacturaDeCompraDirecta struct{}

var _ RegistradorEstandar = new(FacturaDeCompraDirecta)
var _ RegistradorPendientes = new(FacturaDeCompraDirecta)

func (o FacturaDeCompraDirecta) HaceAsientoEstandar(ctaProductoAplicado int) (out bool, err error) {
	return true, nil
}

func (o FacturaDeCompraDirecta) CuentaDebe(ctaProductoAplicado int) (out int, nova bool, err error) {
	return Productos, false, nil
}

func (o FacturaDeCompraDirecta) CuentaHaber(ctaProductoAplicado int) (out int, nova bool, err error) {
	return Proveedores, true, nil
}

func (o FacturaDeCompraDirecta) Pata(ctaID int) (out string, err error) {
	switch ctaID {
	case Productos:
		return types.PataProducto, nil
	case Proveedores:
		return types.PataFacturaPatrimonial, nil
	}
	return out, errors.Errorf("'%v' no tiene definida pata contable para '%v'", o, NombreCuenta(ctaID))
}

func (r FacturaDeCompraDirecta) String() string {
	return "Factura de compra directa"
}

func (o FacturaDeCompraDirecta) HaceAsientoPendientes(ctaProductoAplicado int) (out bool, err error) {
	switch ctaProductoAplicado {
	case 0:
		// No aplica a nada
		return false, nil
	}
	return true, nil
}

func (o FacturaDeCompraDirecta) CuentaDebePendientes(ctaProductoAplicado int) (out int, nova bool, err error) {
	switch ctaProductoAplicado {
	case 0:
		// Remito que NO aplica a una orden de compra => No toca pendientes
		return out, nova, errors.Errorf("remito de compra no hace pendientes cuando aplica a %v", ctaProductoAplicado)

	case ProductosSolicitadosARecibir:
		return ProductosSolicitadosRecibidos, false, nil
	}

	return out, nova, errors.Errorf("no se pudo determinar CuentaDebePendientes para '%v' que aplica a cuenta %v", o, NombreCuenta(ctaProductoAplicado))
}

func (o FacturaDeCompraDirecta) CuentaHaberPendientes(ctaProductoAplicado int) (out int, nova bool, err error) {

	switch ctaProductoAplicado {

	case 0:
		// Remito que NO aplica a una orden de compra => No toca pendientes
		return out, nova, errors.Errorf("remito de compra no hace pendientes cuando aplica a %v", NombreCuenta(ctaProductoAplicado))

	case ProductosSolicitadosARecibir:
		return ProductosSolicitadosARecibir, false, nil
	}

	return out, nova, errors.Errorf("no se pudo determinar CuentaHaberPendientes para Orden de Compra que aplica a cuenta %v", NombreCuenta(ctaProductoAplicado))
}
