package opprod

import "github.com/pkg/errors"

type OrdenDeCompraAnulacion struct{}

var _ RegistradorPendientes = new(OrdenDeCompraAnulacion)

func (o OrdenDeCompraAnulacion) HaceAsientoPendientes(ctaAplicada int) (out bool, err error) {
	if ctaAplicada == 0 {
		return false, errors.Errorf("anulación orden de compra debería estar aplicando")
	}
	return true, nil
}
func (o OrdenDeCompraAnulacion) CuentaDebePendientes(ctaAplicada int) (out int, nova bool, err error) {
	return ProductosSolicitadosCancelados, false, nil
}
func (o OrdenDeCompraAnulacion) CuentaHaberPendientes(ctaAplicada int) (out int, nova bool, err error) {
	return ProductosSolicitadosARecibir, false, nil
}
func (o OrdenDeCompraAnulacion) String() string {
	return "Orden de compra (anulación)"
}
