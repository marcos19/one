package opprod

import (
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"github.com/pkg/errors"
)

type RemitoDeVenta struct{}

var _ RegistradorEstandar = new(RemitoDeVenta)
var _ RegistradorPendientes = new(RemitoDeVenta)

func (o RemitoDeVenta) HaceAsientoEstandar(ctaProductoAplicado int) (out bool, err error) {
	return true, nil
}

func (o RemitoDeVenta) CuentaDebe(ctaProductoAplicado int) (out int, nova bool, err error) {
	switch ctaProductoAplicado {
	case 0:
		// No aplica a nada (remito sin orden de compra)
		return FacturasAEmitir, true, nil

	case ProductosFacturadosAEntregar:
		return ProductosFacturadosAEntregar, false, nil
	}
	return out, nova, errors.Errorf("determinando cuenta haber de RemitoDeVenta aplicando a %v", NombreCuenta(ctaProductoAplicado))
}

func (o RemitoDeVenta) CuentaHaber(ctaProductoAplicado int) (out int, nova bool, err error) {
	return Ventas, false, nil
}

func (o RemitoDeVenta) Pata(ctaID int) (out string, err error) {
	switch ctaID {
	case Ventas:
		return types.PataProducto, nil
	case FacturasAEmitir, ProductosFacturadosAEntregar:
		return types.PataFacturaPatrimonial, nil
	}
	return out, errors.Errorf("'%v' no tiene definida pata contable para '%v'", o, NombreCuenta(ctaID))
}

func (r RemitoDeVenta) String() string {
	return "Remito de venta"
}

func (o RemitoDeVenta) HaceAsientoPendientes(ctaProductoAplicado int) (out bool, err error) {
	switch ctaProductoAplicado {
	case 0:
		return false, nil
	}
	return true, nil
}

func (o RemitoDeVenta) CuentaDebePendientes(ctaProductoAplicado int) (out int, nova bool, err error) {
	switch ctaProductoAplicado {
	case 0:
		return out, nova, errors.Errorf("cuenta a aplicar no ingresada")

	case PedidosDeClientesAEntregar:
		return PedidosDeClientesAEntregar, false, nil
	}

	return out, nova, errors.Errorf("no se pudo determinar CuentaDebePendientes para Remito de Venta que aplica a cuenta %v", NombreCuenta(ctaProductoAplicado))

}

func (o RemitoDeVenta) CuentaHaberPendientes(ctaProductoAplicado int) (out int, nova bool, err error) {

	switch ctaProductoAplicado {

	case 0:
		// Remito que NO aplica a una orden de compra => No toca pendientes
		return out, nova, errors.Errorf("remito de compra no hace pendientes cuando aplica a %v", NombreCuenta(ctaProductoAplicado))

	case PedidosDeClientesAEntregar:
		return PedidosDeClientesEntregados, false, nil
	}

	return 0, nova, errors.Errorf("no se pudo determinar CuentaHaberPendientes para Remito de Venta que aplica a cuenta %v", NombreCuenta(ctaProductoAplicado))
}
