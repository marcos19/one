package opprod

import (
	"fmt"
	"sort"

	"github.com/pkg/errors"
)

const (
	FacturaCompraDirectaID   = 101
	FacturaCompraID          = 102
	OrdenDeCompraID          = 103
	OrdenDeCompraAnulacionID = 104
	RemitoCompraID           = 105
	RemitoCompraDevolucionID = 106

	FacturaVentaDirectaID   = 201
	FacturaVentaID          = 202
	NotaDePedidoID          = 203
	NotaDePedidoAnulacionID = 204
	RemitoVentaID           = 205
	RemitoVentaAnulacionID  = 206
)

func PorID(id int) (out any, err error) {

	// Ciclo compras
	switch id {

	case OrdenDeCompraID:
		return OrdenDeCompra{}, nil

	case OrdenDeCompraAnulacionID:
		return OrdenDeCompraAnulacion{}, nil

	case FacturaCompraID:
		return FacturaDeCompra{}, nil

	case FacturaCompraDirectaID:
		return FacturaDeCompraDirecta{}, nil

	case RemitoCompraID:
		return RemitoDeCompra{}, nil

	case RemitoCompraDevolucionID:
		return RemitoDeCompraAnulacion{}, nil
	}

	// Ciclo venta
	switch id {

	// TODO:
	// case NotaDePedidoID:
	// 	return NotaDePedido{}, nil

	// case NotaDePedidoAnulacionID:
	// 	return NotaDePedidoAnulacion{}, nil

	case FacturaVentaDirectaID:
		return FacturaDeVentaDirecta{}, nil

	case FacturaVentaID:
		return FacturaDeVenta{}, nil

	case RemitoVentaID:
		return RemitoDeVenta{}, nil

	case RemitoVentaAnulacionID:
		return RemitoDeVentaAnulacion{}, nil
	}

	return out, errors.Errorf("opprod %v no implementada", id)
}

type TipoOp struct {
	ID     int `json:",string"`
	Nombre string
	Ciclo  string
}

var tiposOps []TipoOp

func TiposOps() []TipoOp {
	if len(tiposOps) != 0 {
		return tiposOps
	}

	compras := []TipoOp{
		{
			ID:     FacturaCompraDirectaID,
			Nombre: "Factura de compra directa",
			Ciclo:  "Compras",
		},
		{
			ID:     FacturaCompraID,
			Nombre: "Factura de compra",
			Ciclo:  "Compras",
		},
		{
			ID:     OrdenDeCompraID,
			Nombre: "Orden de compra",
			Ciclo:  "Compras",
		},
		{
			ID:     OrdenDeCompraAnulacionID,
			Nombre: "Orden de compra (anulación)",
			Ciclo:  "Compras",
		},
		{
			ID:     RemitoCompraID,
			Nombre: "Remito de compra",
			Ciclo:  "Compras",
		},
		{
			ID:     RemitoCompraDevolucionID,
			Nombre: "Remito de compra (devolución a proveedor)",
			Ciclo:  "Compras",
		},
	}

	ventas := []TipoOp{
		{
			ID:     FacturaVentaDirectaID,
			Nombre: "Factura de venta directa",
			Ciclo:  "Ventas",
		},
		{
			ID:     FacturaVentaID,
			Nombre: "Factura de venta",
			Ciclo:  "Ventas",
		},
		{
			ID:     NotaDePedidoID,
			Nombre: "Nota de pedido",
			Ciclo:  "Ventas",
		},
		{
			ID:     NotaDePedidoAnulacionID,
			Nombre: "Nota de pedido (anulación)",
			Ciclo:  "Ventas",
		},
		{
			ID:     RemitoVentaID,
			Nombre: "Remito de venta",
			Ciclo:  "Ventas",
		},
		{
			ID:     RemitoVentaAnulacionID,
			Nombre: "Remito de venta (devolución de cliente)",
			Ciclo:  "Ventas",
		},
	}

	tiposOps = append(compras, ventas...)

	sort.Slice(tiposOps, func(i, j int) bool {
		nombreI := fmt.Sprintf("%v - %v", tiposOps[i].Ciclo, tiposOps[i].Nombre)
		nombreJ := fmt.Sprintf("%v - %v", tiposOps[j].Ciclo, tiposOps[j].Nombre)
		return nombreI < nombreJ
	})
	return tiposOps
}
