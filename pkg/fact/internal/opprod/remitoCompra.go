package opprod

import (
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"github.com/pkg/errors"
)

type RemitoDeCompra struct{}

var _ RegistradorEstandar = new(RemitoDeCompra)
var _ RegistradorPendientes = new(RemitoDeCompra)

func (o RemitoDeCompra) HaceAsientoEstandar(ctaProductoAplicado int) (out bool, err error) {
	return true, nil
}

func (o RemitoDeCompra) CuentaDebe(ctaProductoAplicado int) (out int, nova bool, err error) {
	return Productos, false, nil
}

func (o RemitoDeCompra) CuentaHaber(ctaProductoAplicado int) (out int, nova bool, err error) {
	switch ctaProductoAplicado {
	case 0:
		// No aplica a nada (remito sin orden de compra)
		return FacturasARecibir, true, nil

	case ProductosCompradosARecibir:
		// Estoy aplicando a factura anticipada
		return ProductosCompradosARecibir, false, nil

	case ProductosSolicitadosARecibir:
		// Estoy aplicando a orden de compra
		return FacturasARecibir, true, nil
	}
	return out, false, errors.Errorf("determinando cuenta haber de RemitoDeCompra aplicando a '%v'", NombreCuenta(ctaProductoAplicado))
}

func (o RemitoDeCompra) Pata(ctaID int) (out string, err error) {
	switch ctaID {
	case Productos:
		return types.PataProducto, nil
	case ProductosCompradosARecibir, FacturasARecibir:
		return types.PataFacturaPatrimonial, nil
	}
	return out, errors.Errorf("'%v' no tiene definida pata contable para '%v'", o, NombreCuenta(ctaID))
}

func (r RemitoDeCompra) String() string {
	return "Remito de compra"
}

func (o RemitoDeCompra) HaceAsientoPendientes(ctaProductoAplicado int) (out bool, err error) {
	switch ctaProductoAplicado {
	case 0, ProductosCompradosARecibir:
		// No aplica a nada
		return false, nil
	}
	return true, nil
}

func (o RemitoDeCompra) CuentaDebePendientes(ctaProductoAplicado int) (out int, nova bool, err error) {
	switch ctaProductoAplicado {
	case 0:
		// Remito que NO aplica a una orden de compra => No toca pendientes
		return out, false, errors.Errorf("remito de compra no hace pendientes cuando aplica a '%v'", NombreCuenta(ctaProductoAplicado))

	case ProductosSolicitadosARecibir:
		return ProductosSolicitadosRecibidos, false, nil
	}

	return 0, false, errors.Errorf("no se pudo determinar CuentaDebePendientes para '%v' que aplica a cuenta '%v'", o, NombreCuenta(ctaProductoAplicado))
}

func (o RemitoDeCompra) CuentaHaberPendientes(ctaProductoAplicado int) (out int, nova bool, err error) {

	switch ctaProductoAplicado {

	case 0:
		// Remito que NO aplica a una orden de compra => No toca pendientes
		return out, false, errors.Errorf("remito de compra no hace pendientes cuando aplica a '%v'", NombreCuenta(ctaProductoAplicado))

	case ProductosSolicitadosARecibir:
		return ProductosSolicitadosARecibir, false, nil
	}

	return 0, false, errors.Errorf("no se pudo determinar CuentaHaberPendientes para Orden de Compra que aplica a cuenta %v", NombreCuenta(ctaProductoAplicado))
}
