package opprod

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRemitoVentaAnulacion(t *testing.T) {
	assert := assert.New(t)

	o := RemitoDeVentaAnulacion{}
	var hace bool
	var err error
	var cta int
	var nova bool

	{ // Estandar
		hace, err = o.HaceAsientoEstandar(0)
		assert.Nil(err)
		assert.Equal(true, hace)

		hace, err = o.HaceAsientoEstandar(PedidosDeClientesAEntregar)
		assert.Nil(err)
		assert.Equal(true, hace)

		{ // Aplico a remito-factura
			cta, nova, err = o.CuentaDebe(0)
			assert.Nil(err)
			assert.False(nova)
			assert.Equal(Ventas, cta)

			cta, nova, err = o.CuentaHaber(0)
			assert.Nil(err)
			assert.True(nova)
			assert.Equal(NotasDeCreditoAEmitir, cta)
		}

		{ // Aplico a una entrega que todavía no está facturada
			cta, nova, err = o.CuentaDebe(FacturasAEmitir)
			assert.Nil(err)
			assert.False(nova)
			assert.Equal(Ventas, cta)

			cta, nova, err = o.CuentaHaber(FacturasAEmitir)
			assert.Nil(err)
			assert.False(nova)
			assert.Equal(FacturasAEmitir, cta)
		}
	}

}
