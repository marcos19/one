package opprod

import (
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"github.com/pkg/errors"
)

type FacturaDeVentaDirecta struct{}

var _ RegistradorEstandar = new(FacturaDeVentaDirecta)

// var _ RegistradorPendientes = new(FacturaDeVentaDirecta)

func (o FacturaDeVentaDirecta) HaceAsientoEstandar(ctaProductoAplicado int) (out bool, err error) {
	return true, nil
}

func (o FacturaDeVentaDirecta) CuentaDebe(ctaProductoAplicado int) (out int, nova bool, err error) {
	return Clientes, true, nil
}

func (o FacturaDeVentaDirecta) CuentaHaber(ctaProductoAplicado int) (out int, nova bool, err error) {
	return Ventas, false, nil
}

func (o FacturaDeVentaDirecta) Pata(ctaID int) (out string, err error) {
	switch ctaID {
	case Ventas:
		return types.PataProducto, nil
	case Clientes:
		return types.PataFacturaPatrimonial, nil
	}
	return out, errors.Errorf("'%v' no tiene definida pata contable para '%v'", o, NombreCuenta(ctaID))
}

func (o FacturaDeVentaDirecta) String() string {
	return "Factura de venta directa"
}

func (o FacturaDeVentaDirecta) HaceAsientoPendientes(ctaProductoAplicado int) (out bool, err error) {
	switch ctaProductoAplicado {
	case 0:
		// No aplica a nada
		return false, nil
	}
	return true, nil
}

func (o FacturaDeVentaDirecta) CuentaDebePendientes(ctaProductoAplicado int) (out int, nova bool, err error) {
	switch ctaProductoAplicado {
	case 0:
		// Factura que NO aplica a una orden de compra => No toca pendientes
		return out, nova, errors.Errorf("remito de compra no hace pendientes cuando aplica a %v", ctaProductoAplicado)

	case PedidosDeClientesAEntregar:
		return PedidosDeClientesAEntregar, false, nil
	}

	return out, nova, errors.Errorf("no se pudo determinar CuentaDebePendientes para Orden de Compra que aplica a cuenta %v", ctaProductoAplicado)
}

func (o FacturaDeVentaDirecta) CuentaHaberPendientes(ctaProductoAplicado int) (out int, nova bool, err error) {

	switch ctaProductoAplicado {

	case 0:
		// Remito que NO aplica a una orden de compra => No toca pendientes
		return out, nova, errors.Errorf("remito de compra no hace pendientes cuando aplica a %v", ctaProductoAplicado)

	case PedidosDeClientesAEntregar:
		return PedidosDeClientesEntregados, false, nil
	}

	return 0, nova, errors.Errorf("no se pudo determinar CuentaHaberPendientes para Orden de Compra que aplica a cuenta %v", ctaProductoAplicado)
}
