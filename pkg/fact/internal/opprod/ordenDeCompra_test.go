package opprod

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestOrdenDeCompra(t *testing.T) {
	assert := assert.New(t)

	o := OrdenDeCompra{}
	var err error
	var cta int
	var nova bool

	cta, nova, err = o.CuentaDebePendientes(0)
	assert.Nil(err)
	assert.True(nova)
	assert.Equal(ProductosSolicitadosARecibir, cta)

	cta, nova, err = o.CuentaHaberPendientes(0)
	assert.Nil(err)
	assert.False(nova)
	assert.Equal(ProductosSolicitados, cta)

}

func TestOrdenDeCompraAnulacion(t *testing.T) {
	assert := assert.New(t)

	o := OrdenDeCompraAnulacion{}
	var err error
	var cta int
	var nova bool

	cta, nova, err = o.CuentaDebePendientes(0)
	assert.Nil(err)
	assert.False(nova)
	assert.Equal(ProductosSolicitadosCancelados, cta)

	cta, nova, err = o.CuentaHaberPendientes(0)
	assert.Nil(err)
	assert.False(nova)
	assert.Equal(ProductosSolicitadosARecibir, cta)

}
