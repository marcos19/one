package opprod

import (
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"github.com/pkg/errors"
)

type RemitoDeVentaAnulacion struct{}

var _ RegistradorEstandar = new(RemitoDeVentaAnulacion)

// var _ RegistradorPendientes = new(RemitoDeVentaAnulacion)

func (o RemitoDeVentaAnulacion) HaceAsientoEstandar(ctaProductoAplicado int) (out bool, err error) {
	return true, nil
}

func (o RemitoDeVentaAnulacion) CuentaDebe(ctaProductoAplicado int) (out int, nova bool, err error) {
	return Ventas, false, nil
}

func (o RemitoDeVentaAnulacion) CuentaHaber(ctaProductoAplicado int) (out int, nova bool, err error) {
	switch ctaProductoAplicado {

	case 0:
		return NotasDeCreditoAEmitir, true, nil

	case FacturasAEmitir:
		return FacturasAEmitir, false, nil
	}
	return out, nova, errors.Errorf("determinando cuenta haber de RemitoDeVenta aplicando a %v", NombreCuenta(ctaProductoAplicado))
}

func (o RemitoDeVentaAnulacion) Pata(ctaID int) (out string, err error) {
	switch ctaID {
	case Ventas:
		return types.PataProducto, nil
	case NotasDeCreditoAEmitir, FacturasAEmitir:
		return types.PataFacturaPatrimonial, nil
	}
	return out, errors.Errorf("'%v' no tiene definida pata contable para '%v'", o, NombreCuenta(ctaID))
}

func (r RemitoDeVentaAnulacion) String() string {
	return "Remito de venta (devolución de clientes)"
}

// func (o RemitoDeVentaAnulacion) HaceAsientoPendientes(ctaProductoAplicado int) (out bool, err error) {
// 	switch ctaProductoAplicado {
// 	case 0:
// 		return false, nil
// 	}
// 	return true, nil
// }

// func (o RemitoDeVentaAnulacion) CuentaDebePendientes(ctaProductoAplicado int) (out int, err error) {
// 	switch ctaProductoAplicado {
// 	case 0:
// 		return out, errors.Errorf("cuenta a aplicar no ingresada")

// 	case PedidosDeClientesAEntregar:
// 		return PedidosDeClientesAEntregar, nil
// 	}

// 	return 0, errors.Errorf("no se pudo determinar CuentaDebePendientes para Remito de Venta que aplica a cuenta %v", NombreCuenta(ctaProductoAplicado))
// }

// func (o RemitoDeVentaAnulacion) CuentaHaberPendientes(ctaProductoAplicado int) (out int, err error) {

// 	switch ctaProductoAplicado {

// 	case 0:
// 		// Remito que NO aplica a una orden de compra => No toca pendientes
// 		return out, errors.Errorf("remito de compra no hace pendientes cuando aplica a %v", NombreCuenta(ctaProductoAplicado))

// 	case PedidosDeClientesAEntregar:
// 		return PedidosDeClientesEntregados, nil
// 	}

// 	return 0, errors.Errorf("no se pudo determinar CuentaHaberPendientes para Remito de Venta que aplica a cuenta %v", NombreCuenta(ctaProductoAplicado))
// }
