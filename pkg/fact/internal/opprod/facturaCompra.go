package opprod

import (
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"github.com/pkg/errors"
)

// Factura que NO mueve stock
type FacturaDeCompra struct{}

var _ RegistradorEstandar = new(FacturaDeCompra)
var _ RegistradorPendientes = new(FacturaDeCompra)

func (o FacturaDeCompra) HaceAsientoEstandar(ctaProductoAplicado int) (out bool, err error) {
	return true, nil
}

func (o FacturaDeCompra) CuentaDebe(ctaProductoAplicado int) (out int, nova bool, err error) {
	switch ctaProductoAplicado {

	case FacturasARecibir:
		return FacturasARecibir, false, nil

	case ProductosSolicitadosARecibir, 0:
		return ProductosCompradosARecibir, true, nil
	}
	return out, nova, errors.Errorf("no se pudo determinar CuentaDebe para 'Factura de Compra' que aplica a cuenta %v", NombreCuenta(ctaProductoAplicado))
}

func (o FacturaDeCompra) CuentaHaber(ctaProductoAplicado int) (out int, nova bool, err error) {
	return Proveedores, true, nil
}

func (o FacturaDeCompra) Pata(ctaID int) (out string, err error) {
	switch ctaID {
	case Proveedores:
		return types.PataFacturaPatrimonial, nil
	case FacturasARecibir, ProductosCompradosARecibir:
		return types.PataProducto, nil
	}
	return out, errors.Errorf("'%v' no tiene definida pata contable para '%v'", o, NombreCuenta(ctaID))
}

func (r FacturaDeCompra) String() string {
	return "Factura de compra"
}

func (o FacturaDeCompra) HaceAsientoPendientes(ctaProductoAplicado int) (out bool, err error) {
	switch ctaProductoAplicado {
	case 0, FacturasARecibir:
		// No aplica a nada
		return false, nil
	}
	return true, nil
}

func (o FacturaDeCompra) CuentaDebePendientes(ctaProductoAplicado int) (out int, nova bool, err error) {
	switch ctaProductoAplicado {
	case 0, FacturasARecibir:
		// Remito que NO aplica a una orden de compra => No toca pendientes

	case ProductosSolicitadosARecibir:
		return ProductosSolicitadosFacturados, false, nil
	}

	return out, nova, errors.Errorf("no se pudo determinar CuentaDebePendientes para Factura de Compra que aplica a cuenta %v", NombreCuenta(ctaProductoAplicado))
}

func (o FacturaDeCompra) CuentaHaberPendientes(ctaProductoAplicado int) (out int, nova bool, err error) {

	switch ctaProductoAplicado {

	case 0, FacturasARecibir:
		// Remito que NO aplica a una orden de compra => No toca pendientes
		return out, nova, errors.Errorf("remito de compra no hace pendientes cuando no aplica")

	case ProductosSolicitadosARecibir:
		return ProductosSolicitadosARecibir, false, nil
	}

	return out, nova, errors.Errorf("no se pudo determinar CuentaHaberPendientes para Factura de Compra que aplica a cuenta %v", NombreCuenta(ctaProductoAplicado))
}
