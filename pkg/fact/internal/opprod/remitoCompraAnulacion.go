package opprod

import (
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"github.com/pkg/errors"
)

type RemitoDeCompraAnulacion struct{}

var _ RegistradorEstandar = new(RemitoDeCompraAnulacion)

// var _ RegistradorPendientes = new(RemitoDeCompraAnulacion)

func (o RemitoDeCompraAnulacion) HaceAsientoEstandar(ctaProductoAplicado int) (out bool, err error) {
	return true, nil
}

func (o RemitoDeCompraAnulacion) CuentaDebe(ctaProductoAplicado int) (out int, nova bool, err error) {
	switch ctaProductoAplicado {

	case 0:
		return NotasDeCreditoARecibir, true, nil

	case FacturasARecibir:
		return FacturasARecibir, false, nil
	}

	return out, nova, errors.Errorf("no se pudo definir cuenta para aplicación a %v", NombreCuenta(ctaProductoAplicado))
}

func (o RemitoDeCompraAnulacion) CuentaHaber(ctaProductoAplicado int) (out int, nova bool, err error) {
	return Productos, false, nil
}

func (o RemitoDeCompraAnulacion) Pata(ctaID int) (out string, err error) {
	switch ctaID {
	case Productos:
		return types.PataProducto, nil
	case FacturasARecibir, NotasDeCreditoARecibir:
		return types.PataFacturaPatrimonial, nil
	}
	return out, errors.Errorf("'%v' no tiene definida pata contable para '%v'", o, NombreCuenta(ctaID))
}

func (r RemitoDeCompraAnulacion) String() string {
	return "Remito de compra (devolución a proveedores)"
}

// TODO: Si era un remito que aplicaba a una orden de compra debería volver
// a generar el pendiente de recibir.
// Tendría que manejarlo sepadarados [OC-REMITO-ANULACION, REMITO-ANULACION]
// Por ahora hago que la devolución termine con los pendientes.
// Corresponde al usuario hacer una nueva orden de compra (si corresponde)
// func (o RemitoDeCompraAnulacion) HaceAsientoPendientes(ctaProductoAplicado int) (out bool, err error) {
// 	switch ctaProductoAplicado {
// 	case 0:
// 		// No aplica a nada
// 		return false, nil
// 	}
// 	return true, nil
// }

// func (o RemitoDeCompraAnulacion) CuentaDebePendientes(ctaProductoAplicado int) (out int, err error) {
// 	switch ctaProductoAplicado {

// 	case 0:
// 		return out, errors.Errorf("RemitoDeCompraAnulacion debe aplicar una partida")

// 	case FacturasARecibir:
// 		return ProductosSolicitadosRecibidos, nil
// 	}

// 	return out, errors.Errorf("no se pudo definir cuenta para aplicación a %v", NombreCuenta(ctaProductoAplicado))
// }

// func (o RemitoDeCompraAnulacion) CuentaHaberPendientes(ctaProductoAplicado int) (out int, err error) {

// 	switch ctaProductoAplicado {

// 	case 0:
// 		return out, errors.Errorf("RemitoDeCompraAnulacion debe aplicar una partida")

// 	case ProductosSolicitadosARecibir:
// 		return ProductosSolicitadosARecibir, nil
// 	}

// 	return out, errors.Errorf("no se pudo definir cuenta para aplicación a %v", NombreCuenta(ctaProductoAplicado))

// }
