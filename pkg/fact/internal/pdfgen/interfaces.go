package pdfgen

import "io"

type Outputer interface {
	Output(w io.Writer) error
}
