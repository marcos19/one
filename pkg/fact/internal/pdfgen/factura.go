package pdfgen

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"bitbucket.org/marcos19/one/pkg/fact"
	"bitbucket.org/marcos19/one/pkg/fact/config"
	"bitbucket.org/marcos19/one/pkg/fact/internal/pdfgen/internal/pdf"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
)

// Pasa los datos de types.Fact => pdf.Fact
func facturaPDF(
	ctx context.Context,
	req ops.ReadOneReq,
	op *ops.Op,
	factReader fact.ReaderOne,
	configReader config.ReaderOne,
	compReader comps.ReaderOne,
	logoReader fact.LogoReader,
) (Outputer, error) {

	// Valido
	if req.ID == uuid.Nil {
		return nil, deferror.Validation("no se ingresó ID de op")
	}
	if req.Comitente == 0 {
		return nil, deferror.Validation("no se ingresó comitente")
	}
	if op == nil {
		return nil, errors.Errorf("op no ingresada")
	}
	if op.CompItem == nil {
		return nil, errors.Errorf("la op no tiene definido item comp")
	}
	if *op.CompItem != types.ItemFactura && *op.CompItem != types.ItemAnulacion {
		return nil, errors.Errorf("la op ingresada no es una factura ni anulación")
	}

	if op.CompItem == nil {
		if *op.CompItem != types.ItemRecibo {
			return nil, errors.Errorf("la op ingresada no es un recibo")
		}
	}

	// Busco Fact
	f, err := factReader.ReadOne(ctx, fact.ReadOneReq{
		Comitente: req.Comitente,
		ID:        op.TranID,
	})
	if err != nil {
		return nil, errors.Wrap(err, "buscando fact")
	}

	// Comp
	comp, err := compReader.ReadOne(ctx, comps.ReadOneReq{
		Comitente: req.Comitente,
		ID:        op.CompID,
	})
	if err != nil {
		return nil, errors.Wrap(err, "buscando comp")
	}

	// Config
	cfg, err := configReader.ReadOne(ctx, config.ReadOneReq{
		Comitente: req.Comitente,
		ID:        f.Config,
	})
	if err != nil {
		return nil, errors.Wrap(err, "buscando config")
	}

	// Creo PDF
	c := pdf.NewFactura(pdf.NewFormatoPaginaCompleta())

	// Para el caso de productos. Si hay partidas directas también.
	c.MuestraPrecios = cfg.ProductosValorizados

	// Formato determinado por el comp
	if comp.AlturaCabecera != 0 {
		c.HeaderHeight = comp.AlturaCabecera.Float()
	}
	if comp.AlturaPersona != 0 {
		c.PersonaHeight = comp.AlturaPersona.Float()
	}
	if comp.AlturaPie != 0 {
		c.FooterHeight = comp.AlturaPie.Float()
	}

	if comp.MargenIzq != 0 {
		c.MargenIzq = comp.MargenIzq.Float()
	}
	if comp.MargenDer != 0 {
		c.MargenDer = comp.MargenDer.Float()
	}
	if comp.MargenSup != 0 {
		c.MargenDer = comp.MargenSup.Float()
	}
	if comp.MargenInf != 0 {
		c.MargenDer = comp.MargenInf.Float()
	}
	if comp.ImprimeBordes != nil {
		c.Bordes = *comp.ImprimeBordes
	}

	// Por defecto si la empresa tiene logo, lo pego
	vaLogo := true
	if comp.ImprimeLogo != nil {
		vaLogo = *comp.ImprimeLogo
	}
	if vaLogo {
		logo, err := logoReader.ReadLogo(ctx, empresas.ReadLogoReq{
			Comitente: req.Comitente,
			Empresa:   op.Empresa,
		})
		if err != nil {
			return nil, errors.Wrap(err, "buscando logo")
		}
		c.Logo = logo
		c.LogoFormato = "png"
	}

	// Estos ya los tengo en los campos del comp:
	c.Fecha = op.Fecha
	c.NombreComp = fmt.Sprintf("%v %v", op.CompNombre, op.NCompStr)
	c.PersonaNombre = op.PersonaNombre
	c.SucursalNombre = f.Persona.NombreSucursal

	if op.TotalComp != nil {
		c.TotalComp = *op.TotalComp
	}

	if op.CAE != nil {
		c.CAE = fmt.Sprint(*op.CAE)
	}

	if op.CAEVto != nil {
		c.CAEVto = *op.CAEVto
	}

	if op.BarCode != nil {
		c.Barcode = *op.BarCode
	}
	if op.QR != nil {
		c.QR = *op.QR
	}
	if op.AFIPComprobanteID != nil {
		if *op.AFIPComprobanteID != 0 {
			c.CodigoComp = fmt.Sprint(*op.AFIPComprobanteID)
		} else {
			c.CodigoComp = "DOCUMENTO NO VÁLIDO COMO FACTURA"
		}
	} else {
		c.CodigoComp = "DOCUMENTO NO VÁLIDO COMO FACTURA"
	}

	if comp.TipoEmision == comps.TipoEmisionWSFEV1 {
		if op.CAE == nil {
			c.MarcaDeAgua = "PENDIENTE DE AUTORIZAR POR AFIP"
		} else {
			if *op.CAE == 0 {
				c.MarcaDeAgua = "PENDIENTE DE AUTORIZAR POR AFIP"
			}
		}
	}

	{ // Discrimina IVA
		v, ok := op.Impresion["DiscriminaIVA"]
		if !ok {
			return c, errors.Errorf("no se pudo determinar campo DiscriminaIVA")
		}
		c.DiscriminaIVA, ok = v.(bool)
		if !ok {
			return c, errors.Errorf("DiscriminaIVA no era un bool")
		}
	}

	// Calcula IVA?
	calculaIVA := false
	{
		v, ok := op.Impresion["CalculaIVA"]
		if ok {
			// La op dice el comportamiento => Tomo de acá
			calculaIVA, ok = v.(bool)
			if !ok {
				return c, errors.Errorf("DiscriminaIVA no era un bool")
			}
		} else {
			// la op no dice nada sobre el comportamiento frente a IVA,
			// lo saco del comp
			calculaIVA = comp.CalculaIVA
		}
	}

	// Estos los tengo en Fact:
	// Productos
	for _, p := range f.Productos {

		if p.CuentaDeTerceros {
			continue
		}

		precioUnitario := p.PrecioUnitario
		var montoTotal dec.D2

		montoTotal = dec.NewD2(p.MontoTotal.Float())
		if calculaIVA && !c.DiscriminaIVA {
			// Un RI que factura a un consumidor final
			// Incluyo IVA en el total del renglón
			porc, err := p.AlicuotaIVA.Porcentaje()
			if err != nil {
				return c, errors.Wrapf(err, "obteniendo porcentaje de IVA de alícuota %v", porc)
			}
			montoTotal = dec.NewD2(p.MontoTotal.Float() * (1 + porc))
		}

		bonif := ""
		if p.RecargoUnitario != 0 {
			bonif = p.RecargoUnitario.StringDos()
		}
		if p.PorcentajeRecargo != 0 {
			bonif = p.PorcentajeRecargo.String()
		}

		codigo := p.Codigo
		nombre := p.ProductoNombre
		um := p.UM
		cantidad := p.Cantidad.String()
		precUnitario := precioUnitario.String()

		// Datos que oculto si tiene otro nombre de impresión
		if p.ProductoImpresion != "" && comp.ProductosNombreImpresion {
			nombre = p.ProductoImpresion
		}
		if comp.ProductosOcultarCodigo {
			codigo = ""
		}
		if comp.ProductosOcultarCantidades {
			precUnitario = ""
			um = ""
			cantidad = ""
		}

		prod := pdf.Producto{
			Codigo:         codigo,
			Detalle:        nombre,
			Cantidad:       cantidad,
			UM:             um,
			PrecioUnitario: precUnitario,
			Bonificacion:   bonif,
			Subtotal:       montoTotal.String(),
			IVA:            p.AlicuotaIVA.String(),
		}

		if p.ProductoDetalle != "" {
			prod.Detalle += fmt.Sprintf(" | %v", p.ProductoDetalle)
		}

		// Detalle de unicidades
		unicidadesStr := ""
		if len(p.Unicidades) > 0 {
			unicidadesStr += " ("
			for i, v := range p.Unicidades {
				if i > 0 {
					unicidadesStr += "; "
				}
				unicidadesStr += v.Detalle
				if i > 9 {
					unicidadesStr += fmt.Sprintf("; ... %v más", len(p.Unicidades)-i-1)
					break
				}
			}
			unicidadesStr += ")"
			prod.Detalle += unicidadesStr
		}

		c.Productos = append(c.Productos, prod)
	}

	// Aplicaciones productos
	for _, p := range f.AplicacionesProductos {

		if p.PorCuentaTerceros {
			continue
		}

		precioUnitario := dec.NewD4(p.Monto.Float() / p.Cantidad.Float())
		montoTotal := dec.NewD2(p.Monto.Float())

		if !c.DiscriminaIVA {

			if err != nil {
				return c, errors.Wrapf(err, "determinando IVA de producto %v", p.Nombre)
			}

			montoTotal = dec.NewD2(precioUnitario.Float() * p.Cantidad.Float())
		}
		bonif := ""

		codigo := p.Codigo
		nombre := p.Nombre
		um := p.UM
		cantidad := p.Cantidad.String()
		precUnitario := precioUnitario.String()

		alic := ""
		if p.AlicuotaIVA != nil {
			alic = p.AlicuotaIVA.String()
		}
		prod := pdf.Producto{
			Codigo:         codigo,
			Detalle:        nombre,
			Cantidad:       cantidad,
			UM:             um,
			PrecioUnitario: precUnitario,
			Bonificacion:   bonif,
			Subtotal:       montoTotal.String(),
			IVA:            alic,
		}

		c.Productos = append(c.Productos, prod)
	}

	for _, v := range f.PartidasDirectas {

		c.MuestraPrecios = true
		part := pdf.PartidaDirecta{}
		part.Detalle = v.Detalle

		switch c.DiscriminaIVA {

		case true:
			part.Subtotal = dec.NewD2(v.Monto.Float()).String()
			part.IVA = v.AlicuotaIVA.String()

		case false:
			// Incluyo IVA en el total del renglón
			porc, err := v.AlicuotaIVA.Porcentaje()
			if err != nil {
				return c, errors.Wrapf(err, "obteniendo porcentaje de IVA de alícuota %v", porc)
			}
			part.Subtotal = dec.NewD2(v.Monto.Float() * (1 + porc)).String()
		}
		c.PartidasDirectas = append(c.PartidasDirectas, part)
	}
	for _, v := range op.TotalesConceptos {
		kv := pdf.KV{
			Key:   v.Nombre,
			Value: v.Monto,
		}
		c.ConceptosResumen = append(c.ConceptosResumen, kv)
	}

	// El resto son los que guardé en el campo Impresion
	{ // Nombre empresa
		nombre, ok := op.Impresion["EmpresaNombre"]
		if !ok {
			return c, errors.Errorf("no se pudo determinar campo NombreEmpresa")
		}
		c.EmpresaNombre, ok = nombre.(string)
		if !ok {
			return c, errors.Errorf("NombreEmpresa no era un string")
		}
	}
	{ // HeaderRenglonesLeft
		rr, ok := op.Impresion["HeaderRenglonesLeft"]
		if !ok {
			return c, errors.Errorf("no se pudo determinar campo HeaderRenglonesLeft")
		}
		str, ok := rr.(string)
		if !ok {
			return c, errors.Errorf("type assertion en HeaderRenglonesLeft")
		}
		c.HeaderRenglonesLeft = strings.Split(str, "\n")
	}
	{ // HeaderRenglonesRight
		rr, ok := op.Impresion["HeaderRenglonesRight"]
		if !ok {
			return c, errors.Errorf("no se pudo determinar campo HeaderRenglonesRight")
		}
		str, ok := rr.(string)
		if !ok {
			return c, errors.Errorf("type assertion en HeaderRenglonesLeft")
		}
		c.HeaderRenglonesRight = strings.Split(str, "\n")
	}
	{ // Letra comp
		v, ok := op.Impresion["LetraComp"]
		if !ok {
			return c, errors.Errorf("no se pudo determinar campo LetraComp")
		}
		c.Letra, ok = v.(string)
		if !ok {
			return c, errors.Errorf("LetraComp no era un string")
		}
	}
	// { // Codigo comp
	// 	v, ok := op.Impresion["CodigoComp"]
	// 	if !ok {
	// 		return c, errors.Errorf("no se pudo determinar campo CodigoComp")
	// 	}
	// 	c.CodigoComp, ok = v.(string)
	// 	if !ok {
	// 		return c, errors.Errorf("CodigoComp no era un string")
	// 	}
	// }
	{ // HeaderRenglonesLeft
		rr, ok := op.Impresion["PersonaCamposLeft"]
		if !ok {
			return c, errors.Errorf("no se pudo determinar campo PersonaCamposLeft")
		}
		inter, ok := rr.([]interface{})
		if !ok {
			return c, errors.Errorf("type assertion en PersonaCamposLeft. se esperaba []interface{}, se obtuvo %T",
				rr)
		}

		for _, v := range inter {
			c.PersonaCamposLeft = append(c.PersonaCamposLeft, v.(string))
		}
	}
	{ // HeaderRenglonesRight
		rr, ok := op.Impresion["PersonaCamposRight"]
		if !ok {
			return c, errors.Errorf("no se pudo determinar campo PersonaCamposRight")
		}
		inter, ok := rr.([]interface{})
		if !ok {
			return c, errors.Errorf(
				"type assertion en PersonaCamposLeft. se esperaba []interface{}, se obtuvo %T",
				rr)
		}

		for _, v := range inter {
			c.PersonaCamposRight = append(c.PersonaCamposRight, v.(string))
		}
	}

	if f.EsContado != nil {
		if *f.EsContado {
			for _, v := range f.Valores {
				kv := pdf.KV{
					Key:   v.Alias,
					Value: v.Monto,
				}
				if v.Detalle != "" {
					kv.Key += " - " + v.Detalle
				}
				c.Valores = append(c.Valores, kv)
			}
		}
	}
	// CajeroNombre
	// CajeroDNI

	// Cheques
	// for _, v := range f.Valores {
	// 	// TODO: hacer más sólido este criterio
	// 	val := pdf.MedioDePago{
	// 		Concepto: v.Alias,
	// 		Detalle:  v.Detalle,
	// 		Monto:    v.Monto,
	// 	}

	// }
	// Tarjetas
	// Retenciones
	// MediosVarios
	// Totales
	// Los llevo a pdf.Factura cada vez que imprima un comprobante.
	return c, nil
}
