package pdf

import (
	"io"
	"os"
	"testing"

	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/stretchr/testify/assert"
)

func TestRecibo(t *testing.T) {
	file, err := os.Create("./testdata/recibo.pdf")
	assert.Nil(t, err)

	f := NewRecibo()

	// Logo
	logo, err := os.Open("./testdata/vw.png")
	f.LogoFormato = "png"
	assert.Nil(t, err)
	f.Logo, err = io.ReadAll(logo)
	assert.Nil(t, err)

	f.CodigoComp = "DOCUMENTO NO VÁLIDO COMO FACTURA"
	f.EmpresaNombre = "Rafaela Revisión Técnica Vehicular SA"
	f.HeaderRenglonesLeft = []string{
		"Domicilio Taller: Ruta Provincial Nº 13 - KM 1.",
		"Domicilio Legal: Gral. Roca 657 - Las Rosas (2520) SF",
		"contacto@rrtvtecnica.com - 03401-449424 / 25",
	}
	f.HeaderRenglonesRight = []string{
		"Fecha Inicio Actividades: 01/11/2012",
		"CUIT: 30-71152196-4",
		"Ingresos Brutos: 121-016857-1",
	}
	f.Letra = "X"
	f.NombreComp = "Recibo 0004-00000324"
	f.Fecha = fecha.Fecha(20200621)

	f.PersonaNombre = "BORTOLUSSI MARCOS"
	f.PersonaCamposLeft = []string{
		"CUIT: 20-32889647-9",
		"Domicilio: Gral Roca 657 - LAS ROSAS (2520) Santa Fe",
	}
	f.PersonaCamposRight = []string{
		"Condición frente IVA: Responsable inscripto",
		"Condición de pago: Cuenta corriente",
	}

	f.Totales = []KV{
		{
			Key:   "ARS",
			Value: dec.NewD2(15783.43),
		},
		// {
		// 	Key:   "USD",
		// 	Value: dec.NewD2(1000),
		// },
	}

	f.Cheques = []Cheque{
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		}, {
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		}, {
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		}, {
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		}, {
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		}, {
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		}, {
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		}, {
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		}, {
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		}, {
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		}, {
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		}, {
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		}, {
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		}, {
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		}, {
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		}, {
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		}, {
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		}, {
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		}, {
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		}, {
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		}, {
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
		{
			Banco:         "BANCO DE LA NACIÓN ARGENTINA",
			FechaEmision:  fecha.Fecha(20200622),
			FechaDiferida: fecha.Fecha(20200722),
			Dias:          30,
			Monto:         dec.NewD2(18900),
			Numero:        43980,
		},
	}

	// f.= []Tarjeta{
	// 	{
	// 		Tarjeta: "VISA",
	// 		Tipo:    "Tarjeta de débito",
	// 		Cuotas:  6,
	// 		Cupon:   34,
	// 		Lote:    3,
	// 		Monto:   dec.NewD2(5500),
	// 	},
	// }

	f.Retenciones = []Retencion{
		{
			Impuesto: "Impuesto a las ganancias",
			Regimen:  "342 - Locaciones de servicios no realizados en relación de dependencia",
			Detalle:  "",
			Numero:   "0002-00000023",
			Monto:    dec.NewD2(345.43),
		},
		{
			Impuesto: "Ingresos brutos",
			Regimen:  "12",
			Detalle:  "Santa Fe",
			Numero:   "0002-00000023",
			Monto:    dec.NewD2(345.43),
		},
	}

	f.MediosVarios = []MedioDePago{
		{
			Concepto: "Efectivo",
			Detalle:  "Pesos",
			Monto:    dec.NewD2(7200),
		},
		{
			Concepto: "Cuenta Corriente ARS",
			Detalle:  "Banco Credicoop 265-1101-6",
			Monto:    dec.NewD2(25000),
		},
	}

	f.Aplicaciones = []Aplicacion{
		{
			FechaEmision:  20210508,
			FechaDiferida: 20210825,
			NombreComp:    "Factura A",
			NCompStr:      "0001-00005416",
			MontoAplicado: 51654789,
		},
		{
			FechaEmision:  20210508,
			FechaDiferida: 20210825,
			NombreComp:    "Factura A",
			NCompStr:      "0001-00005416",
			MontoAplicado: 51654789,
		},
		{
			FechaEmision:  20210508,
			FechaDiferida: 20210825,
			NombreComp:    "Factura A",
			NCompStr:      "0001-00005416",
			MontoAplicado: 51654789,
		},
	}
	// f.CajeroNombre = "Alejandro J. Cena"
	// f.CajeroDNI = "DNI 33.879.380"
	err = f.Output(file)
	assert.Nil(t, err)
}
