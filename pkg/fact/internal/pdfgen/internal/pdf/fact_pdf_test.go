package pdf

import (
	"io"
	"os"
	"testing"

	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/stretchr/testify/assert"
)

func TestFactura(t *testing.T) {

	f := NewFactura(NewFormatoPaginaCompleta())
	f.CodigoComp = "Cod. 1"
	f.EmpresaNombre = "Rafaela Revisión Técnica Vehicular SA"

	// Logo
	logo, err := os.Open("./testdata/vw.png")
	f.LogoFormato = "png"
	assert.Nil(t, err)
	f.Logo, err = io.ReadAll(logo)
	assert.Nil(t, err)

	f.HeaderRenglonesLeft = []string{
		"Domicilio Taller: Ruta Provincial Nº 13 - KM 1.",
		"Domicilio Legal: Gral. Roca 657 - Las Rosas (2520) SF",
		"contacto@rrtvtecnica.com - 03401-449424 / 25",
	}
	f.HeaderRenglonesRight = []string{
		"Fecha Inicio Actividades: 01/11/2012",
		"CUIT: 30-71152196-4",
		"Ingresos Brutos: 121-016857-1",
	}
	f.Letra = "A"
	f.NombreComp = "Factura A 0002-00543643"
	f.Fecha = fecha.Fecha(20200621)

	f.PersonaNombre = "BORTOLUSSI MARCOS"
	f.PersonaCamposLeft = []string{
		"CUIT: 20-32889647-9",
		"Domicilio: Gral Roca 657 - LAS ROSAS (2520) Santa Fe",
		"Condición frente IVA: Responsable inscripto",
	}
	f.PersonaCamposRight = []string{
		// "Condición de pago: Cuenta corriente",
	}
	f.CondicionPago = "Cuenta corriente - 6 cuotas. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
	f.Vencimientos = []KV{
		{Key: "21/06/2020", Value: dec.NewD2(1000)},
		{Key: "21/07/2020", Value: dec.NewD2(1000)},
		{Key: "21/08/2020", Value: dec.NewD2(1000)},
		{Key: "21/09/2020", Value: dec.NewD2(1000)},
		{Key: "21/10/2020", Value: dec.NewD2(1000)},
		{Key: "21/11/2020", Value: dec.NewD2(500)},
	}

	f.Valores = []KV{
		{Key: "Cheque BANCO NACIÓN N 1561651", Value: dec.NewD2(500)},
		{Key: "Cheque BANCO SANTANDER N 15", Value: dec.NewD2(500)},
		{Key: "Efectivo (ARS)", Value: dec.NewD2(2156.15)},
	}
	f.Productos = []Producto{
		{
			Codigo:         "ABC-3489",
			Detalle:        "Acoplado / Semirremolque - EQY820 OMBU 2004 PALLETERO",
			PrecioUnitario: "4.173,55",
			Cantidad:       "1",
			UM:             "unidad",
			Bonificacion:   "",
			IVA:            "21%",
			Subtotal:       "4.173,55",
		},
		{
			Codigo:         "ABC-3489",
			Detalle:        "Acoplado / Semirremolque - EQY820 OMBU 2004 PALLETERO",
			PrecioUnitario: "4.173,55",
			Cantidad:       "8.900",
			UM:             "kilogramos",
			Bonificacion:   "",
			IVA:            "21%",
			Subtotal:       "4.173,55",
		},
		{
			Codigo:         "ABC-3489",
			Detalle:        "Acoplado / Semirremolque - EQY820 OMBU 2004 PALLETERO ",
			PrecioUnitario: "184.173,55",
			Bonificacion:   "",
			IVA:            "21%",
			Subtotal:       "4.173,55",
		},
		{
			Codigo:         "ABC-3489",
			Detalle:        "Acoplado / Semirremolque - EQY820 OMBU 2004 PALLETERO",
			PrecioUnitario: "4.173,55",
			Bonificacion:   "12,34%",
			IVA:            "NG",
			Subtotal:       "2.454.173,55",
		},
		{
			Codigo:         "ABC-3489",
			Detalle:        "Acoplado / Semirremolque - EQY820 OMBU 2004 PALLETERO",
			PrecioUnitario: "4.173,55",
			Cantidad:       "1",
			UM:             "unidad",
			Bonificacion:   "",
			IVA:            "21%",
			Subtotal:       "4.173,55",
		},
		{
			Codigo:         "ABC-3489",
			Detalle:        "Acoplado / Semirremolque - EQY820 OMBU 2004 PALLETERO",
			PrecioUnitario: "4.173,55",
			Bonificacion:   "12,34%",
			IVA:            "NG",
			Subtotal:       "2.454.173,55",
		},
		{
			Codigo:         "ABC-3489",
			Detalle:        "Acoplado / Semirremolque - EQY820 OMBU 2004 PALLETERO",
			PrecioUnitario: "4.173,55",
			Cantidad:       "1",
			UM:             "unidad",
			Bonificacion:   "",
			IVA:            "21%",
			Subtotal:       "4.173,55",
		}, {
			Codigo:         "ABC-3489",
			Detalle:        "Acoplado / Semirremolque - EQY820 OMBU 2004 PALLETERO",
			PrecioUnitario: "4.173,55",
			Cantidad:       "8.900",
			UM:             "kilogramos",
			Bonificacion:   "",
			IVA:            "21%",
			Subtotal:       "4.173,55",
		},

		// {
		// 	Codigo:         "ABC-3489",
		// 	Detalle:        "Acoplado / Semirremolque - EQY820 OMBU 2004 PALLETERO ",
		// 	PrecioUnitario: "184.173,55",
		// 	Bonificacion:   "",
		// 	IVA:            "21%",
		// 	Subtotal:       "4.173,55",
		// },
		// {
		// 	Codigo:         "ABC-3489",
		// 	Detalle:        "Acoplado / Semirremolque - EQY820 OMBU 2004 PALLETERO",
		// 	PrecioUnitario: "4.173,55",
		// 	Bonificacion:   "12,34%",
		// 	IVA:            "NG",
		// 	Subtotal:       "2.454.173,55",
		// },
		// {
		// 	Codigo:         "ABC-3489",
		// 	Detalle:        "Acoplado / Semirremolque - EQY820 OMBU 2004 PALLETERO",
		// 	PrecioUnitario: "4.173,55",
		// 	Cantidad:       "1",
		// 	UM:             "unidad",
		// 	Bonificacion:   "",
		// 	IVA:            "21%",
		// 	Subtotal:       "4.173,55",
		// },
		// {
		// 	Codigo:         "ABC-3489",
		// 	Detalle:        "Acoplado / Semirremolque - EQY820 OMBU 2004 PALLETERO",
		// 	PrecioUnitario: "4.173,55",
		// 	Bonificacion:   "12,34%",
		// 	IVA:            "NG",
		// 	Subtotal:       "2.454.173,55",
		// },
		// {
		// 	Codigo:         "ABC-3489",
		// 	Detalle:        "Acoplado / Semirremolque - EQY820 OMBU 2004 PALLETERO",
		// 	PrecioUnitario: "4.173,55",
		// 	Cantidad:       "1",
		// 	UM:             "unidad",
		// 	Bonificacion:   "",
		// 	IVA:            "21%",
		// 	Subtotal:       "4.173,55",
		// },
	}

	f.ConceptosResumen = []KV{
		{
			Key:   "Neto gravado 21%",
			Value: dec.NewD2(4173.55),
		},
		{
			Key:   "IVA 21%",
			Value: dec.NewD2(876.45),
		},
		{
			Key:   "No gravado",
			Value: dec.NewD2(450),
		},
	}
	f.TotalComp = dec.NewD2(5500)

	f.CAE = "70254126852850"
	f.CAEVto = fecha.Fecha(20200629)
	// f.Barcode = "3071152196401000270254126852850202006294"
	f.QR = "https://www.afip.gob.ar/fe/qr/?p=eyJ2ZXIiOjEsImZlY2hhIjoiMjAyMS0wNC0wNSIsImN1aXQiOjMwNzExNTIxOTY0LCJwdG9WdGEiOjIsInRpcG9DbXAiOjMsIm5yb0NtcCI6NjQyLCJpbXBvcnRlIjoxOTAwMCwibW9uZWRhIjoiUEVTIiwiY3R6IjoxLCJ0aXBvRG9jUmVjIjo4MCwibnJvRG9jUmVjIjoyMDMyOTE2Nzk0MiwidGlwb0NvZEF1dCI6IkUiLCJjb2RBdXQiOjcxMTU0NTE5MjI2MTM2fQ=="
	f.CantidadPorHoja = 1
	f.Copias = 1
	f.DiscriminaIVA = true

	{ // Con logo
		file, err := os.Create("./testdata/factura_con_logo.pdf")
		assert.Nil(t, err)
		err = f.Output(file)
		assert.Nil(t, err)
	}

	{ // Sin logo
		file, err := os.Create("./testdata/factura.pdf")
		assert.Nil(t, err)
		f.Logo = []byte{}
		err = f.Output(file)
		assert.Nil(t, err)
	}

}
