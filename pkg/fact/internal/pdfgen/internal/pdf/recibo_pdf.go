package pdf

import (
	"bytes"
	"fmt"
	"io"
	"math"
	"strings"

	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/jung-kurt/gofpdf"
	"github.com/rs/zerolog/log"
)

type Recibo struct {
	fpdf *gofpdf.Fpdf
	tr   func(string) string

	// Header
	Logo          []byte
	LogoFormato   string
	EmpresaNombre string

	HeaderRenglonesLeft  []string
	HeaderRenglonesRight []string

	NombreComp string // Incluye nombre, punto de venta y número
	Letra      string
	CodigoComp string

	Fecha fecha.Fecha

	// Persona
	PersonaNombre      string
	SucursalNombre     string
	PersonaCamposLeft  []string
	PersonaCamposRight []string

	// Body
	Cheques []Cheque
	// Tarjetas     []Tarjeta
	Retenciones      []Retencion
	MediosVarios     []MedioDePago
	MonedaExtranjera []MonedaExtranjera

	Aplicaciones []Aplicacion

	// Resumen
	Totales      []KV
	CajeroNombre string
	CajeroDNI    string

	// Footer

	// Formato
	Copias int

	MargenIzq            float64
	MargenDer            float64
	MargenSup            float64
	MargenInf            float64
	MargenEntreSecciones float64
	PaddingLeft          float64

	HeaderHeight  float64
	PersonaHeight float64
	FooterHeight  float64

	Bordes  bool
	padding float64
}

type Cheque struct {
	Banco         string
	Numero        int
	FechaEmision  fecha.Fecha
	FechaDiferida fecha.Fecha
	Dias          int
	Monto         dec.D2
}

// type Tarjeta struct {
// 	Tipo    string
// 	Tarjeta string
// 	Lote    int
// 	Cupon   int
// 	Cuotas  int
// 	Monto   dec.D2
// }

type Retencion struct {
	Impuesto string
	Regimen  string
	Detalle  string
	Numero   string
	Monto    dec.D2
}

type MedioDePago struct {
	Concepto string
	Detalle  string
	Monto    dec.D2
}

type MonedaExtranjera struct {
	Concepto string
	Detalle  string
	Original dec.D4
	Moneda   string
	TC       dec.D4
	Monto    dec.D2
}

type Aplicacion struct {
	FechaEmision  fecha.Fecha
	FechaDiferida fecha.Fecha
	NombreComp    string
	NCompStr      string
	MontoAplicado dec.D2
}

func (c Recibo) AnchoImprimible() float64 {
	ancho, _ := c.fpdf.GetPageSize()
	return ancho - c.MargenIzq - c.MargenDer - c.padding*2
}

// NewRecibo crea un nuevo comprobante con los valores por defecto.
func NewRecibo() (c Recibo) {
	// Seteo margenes
	c.MargenIzq = 10
	c.MargenDer = 10
	c.MargenSup = 10
	c.MargenInf = 10
	c.MargenEntreSecciones = 0.6

	c.HeaderHeight = 44
	c.PersonaHeight = 18

	c.Bordes = true
	c.padding = A4Ancho * oro / 30
	c.Copias = 1
	c.fpdf = gofpdf.New("P", "mm", "A4", "")
	c.tr = c.fpdf.UnicodeTranslatorFromDescriptor("")

	return c
}

func (c Recibo) Output(w io.Writer) (err error) {
	p := c.fpdf
	top := 0.
	p.SetMargins(c.MargenIzq, c.MargenSup+c.HeaderHeight+c.PersonaHeight+c.MargenEntreSecciones*4, c.MargenDer)
	p.SetAutoPageBreak(true, c.MargenInf)

	// Header para cada página
	p.SetHeaderFunc(func() {
		// Cuadro cliente
		p.SetDrawColor(0, 0, 0)
		// p.SetMargins(c.MargenIzq, c.MargenSup, c.MargenDer)
		c.printHeader(top)
		c.printCuadroCliente(top)

	})
	// Determino la altura del footer (me hace crecer toda la sección)
	c.FooterHeight += c.padding*2 + 3*2 + float64(len(c.Totales)*5)
	if c.CajeroNombre != "" {
		c.FooterHeight += 15
	}
	// p.SetFooterFunc(fnc func())
	// Fuentes
	fontSize := 8.

	p.SetFont("Arial", "", fontSize)
	p.AddPage()

	// p.SetMargins(c.MargenIzq, c.MargenSup+c.HeaderHeight+c.PersonaHeight, c.MargenDer)
	c.printBody(top)
	c.printFooter(top)

	for i := 1; i <= c.fpdf.PageCount(); i++ {
		if c.Bordes {

			// Recuadro del body
			log.Debug().Msgf("Imprimiendo recuadro en hoja %v", i)
			p.SetPage(i)
			_, h := c.fpdf.GetPageSize()
			_, top, _, _ := p.GetMargins()
			log.Debug().Msgf("%v - top margin era %v", i, top)
			altura := h - top - c.MargenInf

			ultimaPagina := i == c.fpdf.PageCount()
			if ultimaPagina {
				log.Debug().Msgf("%v - ultima pagina", i)
				altura -= c.FooterHeight + c.MargenEntreSecciones*2
			}
			p.SetDrawColor(0, 0, 0)
			p.ClipRoundedRect(c.MargenIzq, top, c.AnchoImprimible()+c.padding*2, altura, 3, true)
			p.ClipEnd()

			pageText := fmt.Sprintf("Página %v de %v", i, c.fpdf.PageCount())

			p.SetFont("Arial", "", fontSize)
			if p.PageCount() != 1 {
				p.Text(c.MargenIzq+c.AnchoImprimible()-20+c.padding, h-c.MargenInf+4, c.tr(pageText))
			}
		}
	}

	err = p.Output(w)
	return

}

func (c *Recibo) printHeader(top float64) {
	// NombreEmpresa
	p := c.fpdf
	top += c.MargenSup
	offsetY := top
	// anchoCuadritoLetra := 10.0

	// difPorImg := 20.
	if len(c.Logo) > 0 {
		switch c.LogoFormato {
		case "png", "jpg":
			// Ok
		default:
			offsetY += c.HeaderHeight*(1-oro) + 1
			goto saltearLogo
		}
		buf := bytes.NewReader(c.Logo)
		p.RegisterImageOptionsReader("logo", gofpdf.ImageOptions{ImageType: c.LogoFormato}, buf)
		info := p.GetImageInfo("logo")
		if info == nil {
			log.Error().Msg("no se pudo encontrar información de imagen")
			goto saltearLogo
		}

		// Limito altura, mantengo proporcion original
		alto := 20.0
		offsetY += alto
		ancho := alto * info.Width() / info.Height()
		p.Image("logo", +c.MargenIzq+c.AnchoImprimible()/4-ancho/2,
			top+2, ancho, alto, false, "", 0, "")
		offsetY += 7
	} else {
		// Lo pongo a la misma altura que 'Factura A 0001'
		offsetY += c.HeaderHeight*(1-oro) + 1
	}

saltearLogo:

	// Nombre empresa
	p.SetLeftMargin(c.MargenIzq)
	p.SetY(offsetY)
	p.SetFont("Arial", "B", 10)
	p.CellFormat(c.AnchoImprimible()/2+c.padding, 1, c.tr(c.EmpresaNombre), "", 1, "C", false, 0, "")
	p.Ln(1)
	// p.WriteAligned(
	// 	c.AnchoImprimible()/2, // Width
	// 	0,                     // Line height
	// 	c.tr(c.EmpresaNombre), "C")

	// Renglones de la izquierda
	p.SetFont("Arial", "", 8)
	offsetY += 3
	p.SetY(offsetY)
	for _, v := range c.HeaderRenglonesLeft {
		p.SetX(c.MargenIzq)
		p.CellFormat(c.AnchoImprimible()/2+c.padding, 3, c.tr(v), "", 1, "C", false, 0, "")
		p.Ln(0)
	}

	// Factura A 0003-23244323
	p.SetXY(c.MargenIzq+c.AnchoImprimible()/2+c.padding, top+7+5)
	p.SetFont("Arial", "B", 12)
	p.CellFormat(c.AnchoImprimible()/2+c.padding, 1, c.tr(c.NombreComp), "", 1, "C", false, 0, "")

	// Fecha
	p.SetXY(c.MargenIzq+c.AnchoImprimible()/2+c.padding, top+7+5+5)
	p.SetFont("Arial", "", 9)
	fchStr := c.tr("Fecha emisión: " + c.Fecha.String())
	// p.Text(c.MargenIzq+c.AnchoImprimible()/2+15, top+20, fchStr)
	p.CellFormat(c.AnchoImprimible()/2+c.padding, 3, fchStr, "", 1, "C", false, 0, "")

	// Renglones de la derecha
	p.SetY(top + 7 + 20)
	p.SetFont("Arial", "", 8)
	for _, v := range c.HeaderRenglonesRight {
		// p.SetX(c.MargenIzq + c.AnchoImprimible()/2)
		p.SetX(c.AnchoImprimible()/2 + c.MargenIzq + c.padding)
		p.CellFormat(c.AnchoImprimible()/2+c.padding, 3, c.tr(v), "", 1, "C", false, 0, "")
		p.Ln(0)
	}

	// Cuadradito con letra A
	cuadSize := 10.0
	p.ClipRoundedRect(c.MargenIzq+c.AnchoImprimible()/2-cuadSize/2+c.padding, top+4, cuadSize, cuadSize, 1, true)
	p.ClipEnd()

	// Letra A
	p.SetFont("Arial", "B", 22)
	p.SetXY(c.MargenIzq, top+cuadSize/2+4.4)
	p.CellFormat(c.AnchoImprimible()+c.padding*2, 0, c.tr(c.Letra), "", 1, "C", false, 0, "")
	// p.Text(c.AnchoImprimible()+c.padding*2, top+11.5, c.Letra)

	// Cod. 1
	anchoCell := 30.
	p.SetFont("Arial", "", 7)
	p.SetXY(c.MargenIzq+c.AnchoImprimible()/2+c.padding-anchoCell/2, top+15)
	p.MultiCell(anchoCell, 3, c.tr(c.CodigoComp), "", "MC", false)
	// p.CellFormat(c.AnchoImprimible()+c.padding*2, 0, c.tr(c.CodigoComp), "", 1, "C", false, 0, "")
	// p.Text(c.MargenIzq+c.AnchoImprimible()/2-3.5, top+17, c.CodigoComp)

	// Cuadrado del header
	if c.Bordes {
		p.ClipRoundedRect(c.MargenIzq, top, c.AnchoImprimible()+c.padding*2, c.HeaderHeight, 3, true)
		p.ClipEnd()
	}

	p.SetY(c.MargenSup + c.HeaderHeight + c.PersonaHeight + c.MargenEntreSecciones*2 + c.padding)
}

// Dibuja el cuadro del cliente a la altura indicada
func (c *Recibo) printCuadroCliente(top float64) {
	p := c.fpdf
	offsetY := top + c.MargenSup + c.HeaderHeight + c.MargenEntreSecciones*2
	offsetX := c.MargenIzq

	// Cuadrado del header
	if c.Bordes {
		p.ClipRoundedRect(offsetX, offsetY, c.AnchoImprimible()+c.padding*2, c.PersonaHeight, 3, true)
		p.ClipEnd()
	}

	offsetY += 1.001
	p.SetFont("Arial", "B", 10)
	nombre := c.PersonaNombre
	if c.SucursalNombre != "" {
		nombre += " (" + c.SucursalNombre + ")"
	}
	p.Text(offsetX+5, offsetY+5, c.tr(nombre))

	p.SetFont("Arial", "", 8)

	offsetY += 10
	for i, v := range c.PersonaCamposLeft {
		p.Text(c.MargenIzq+5, offsetY+3*float64(i), c.tr(v))
	}
	for i, v := range c.PersonaCamposRight {
		p.Text(c.MargenIzq+c.AnchoImprimible()/2+15, offsetY+3*float64(i), c.tr(v))
	}

}
func colorRenglon(p *gofpdf.Fpdf) {
	p.SetFillColor(243, 243, 243)
}

func colorSubtotal(p *gofpdf.Fpdf) {
	p.SetFillColor(233, 233, 233)
}

// Dibuja el cuadro del footer
func (c *Recibo) printBody(top float64) {
	p := c.fpdf
	p.SetDashPattern([]float64{}, 0)

	offsetX := c.MargenIzq

	offsetX += c.padding
	// offsetY += c.padding
	// p.SetXY(offsetX, offsetY)

	headH := 6.
	rengH := 4.5
	subtituloH := 6.
	subtituloW := 30.
	distEntreCuadros := 5.
	anchoTabla := c.AnchoImprimible()
	sizeMonto := anchoTabla * 0.14 // Ancho fijo para todas las tablas
	sizeIt := anchoTabla * .03

	p.SetDrawColor(238, 238, 238)

	p.SetX(offsetX)
	log.Debug().Int("Cantidad medios varios", len(c.MediosVarios)).Msg("")
	// MEDIOS VARIOS
	if len(c.MediosVarios) > 0 {
		p.SetFillColor(230, 229, 255)

		p.SetFont("Arial", "B", 8)
		// p.CellFormat(c.AnchoImprimible()-6, 6, c.tr("Retenciones"), "TLR", 0, "MC", true, 0, "")
		// p.Ln(6)
		// offsetY += 6
		sizeCon := anchoTabla * 0.2
		sizeDet := anchoTabla - (sizeIt + sizeCon + sizeMonto)

		p.CellFormat(sizeIt, headH, c.tr(""), "LT", 0, "MC", false, 0, "")
		p.CellFormat(sizeCon, headH, c.tr("Concepto"), "T", 0, "ML", false, 0, "")
		p.CellFormat(sizeDet, headH, c.tr("Detalle"), "T", 0, "ML", false, 0, "")
		p.CellFormat(sizeMonto, headH, c.tr("Monto"), "RT", 0, "MC", false, 0, "")
		p.Ln(headH)

		p.SetFont("Arial", "", 8)
		pintar := false
		sum := dec.D2(0)
		for i, v := range c.MediosVarios {
			sum += v.Monto
			// pintar = !pintar
			bordes := ""
			if i == len(c.MediosVarios)-1 {
				bordes = "B"
			}
			colorRenglon(p)

			// Detalle
			yAnt := p.GetY()
			p.SetX(offsetX + sizeIt + sizeCon)
			p.MultiCell(sizeDet, rengH, c.tr(v.Detalle), bordes, "TL", pintar)
			yDesp := p.GetY()
			alturaDetalle := yDesp - yAnt
			p.SetXY(offsetX+sizeIt+sizeCon+sizeDet, yAnt)
			p.CellFormat(sizeMonto, alturaDetalle, c.tr(v.Monto.String()), "R"+bordes, 0, "TR", pintar, 0, "")

			p.SetXY(offsetX, yAnt)
			p.CellFormat(sizeIt, alturaDetalle, fmt.Sprint(i+1), "L"+bordes, 0, "TC", pintar, 0, "")
			p.CellFormat(sizeCon, alturaDetalle, c.tr(v.Concepto), bordes, 0, "TL", pintar, 0, "")
			p.SetY(yDesp)
		}
		if len(c.MediosVarios) > 1 {
			p.SetX(offsetX)
			p.SetFont("Arial", "B", 8)
			p.CellFormat(c.AnchoImprimible()-sizeMonto, headH, c.tr(""), "", 0, "MR", false, 0, "")
			colorSubtotal(p)
			p.CellFormat(sizeMonto, headH, c.tr(sum.String()), "", 0, "MR", true, 0, "")
			p.Ln(headH)
		}
		p.Ln(distEntreCuadros)
	}

	// MONEDA EXTRANJERA
	if len(c.MonedaExtranjera) > 0 {
		p.SetX(offsetX)
		p.SetFillColor(230, 229, 255)

		p.SetFont("Arial", "B", 8)
		// p.CellFormat(c.AnchoImprimible()-6, 6, c.tr("Retenciones"), "TLR", 0, "MC", true, 0, "")
		// p.Ln(6)
		// offsetY += 6
		sizeCon := anchoTabla * 0.3
		sizeOrig := 20.
		sizeTC := 20.
		sizeDet := anchoTabla*0.53 - sizeOrig - sizeTC

		p.SetX(offsetX)
		p.CellFormat(sizeIt, headH, c.tr(""), "LT", 0, "MC", false, 0, "")
		p.CellFormat(sizeCon, headH, c.tr("Concepto"), "T", 0, "ML", false, 0, "")
		p.CellFormat(sizeDet, headH, c.tr("Detalle"), "T", 0, "ML", false, 0, "")
		p.CellFormat(sizeOrig, headH, c.tr("Nominal"), "T", 0, "MR", false, 0, "")
		p.CellFormat(sizeTC, headH, c.tr("Tipo cambio"), "T", 0, "MR", false, 0, "")
		p.CellFormat(sizeMonto, headH, c.tr("Monto"), "RT", 0, "MC", false, 0, "")
		p.Ln(headH)

		p.SetFont("Arial", "", 8)
		pintar := false
		sum := dec.D2(0)
		for i, v := range c.MonedaExtranjera {
			sum += v.Monto
			// pintar = !pintar
			bordes := ""
			if i == len(c.MediosVarios)-1 {
				bordes = "B"
			}
			p.SetX(offsetX)
			colorRenglon(p)
			p.CellFormat(sizeIt, headH, fmt.Sprint(i+1), "L"+bordes, 0, "MC", pintar, 0, "")
			p.CellFormat(sizeCon, headH, c.tr(v.Concepto), bordes, 0, "ML", pintar, 0, "")
			p.CellFormat(sizeDet, headH, c.tr(v.Detalle), bordes, 0, "ML", pintar, 0, "")
			p.CellFormat(sizeOrig, headH, c.tr(v.Moneda+" "+v.Original.StringDos()), bordes, 0, "MR", pintar, 0, "")
			p.CellFormat(sizeTC, headH, c.tr(v.TC.String()), bordes, 0, "MR", pintar, 0, "")
			p.CellFormat(sizeMonto, headH, c.tr(v.Monto.String()), "R"+bordes, 0, "MR", pintar, 0, "")
			p.Ln(headH)
		}
		if len(c.MediosVarios) > 1 {
			p.SetX(offsetX)
			p.SetFont("Arial", "B", 8)
			p.CellFormat(c.AnchoImprimible()-sizeMonto, headH, c.tr(""), "", 0, "MR", false, 0, "")
			colorSubtotal(p)
			p.CellFormat(sizeMonto, headH, c.tr(sum.String()), "", 0, "MR", true, 0, "")
			p.Ln(headH)
		}
		p.Ln(distEntreCuadros)
	}

	// CHEQUES
	if len(c.Cheques) > 0 {

		p.SetX(offsetX)
		p.SetFillColor(249, 249, 249)
		sizeBco := anchoTabla * 0.37
		sizeN := anchoTabla * 0.12
		sizeEm := anchoTabla * 0.12
		sizeDif := anchoTabla * 0.12
		sizeDias := anchoTabla * 0.1

		p.SetFont("Arial", "B", 8)

		// p.SetFillColor(230, 229, 255)
		p.CellFormat(subtituloW, subtituloH, c.tr("Cheques"), "B", 0, "ML", true, 0, "")
		p.Ln(subtituloH)

		p.SetX(offsetX)
		p.CellFormat(sizeIt, headH, c.tr(""), "LT", 0, "MC", false, 0, "")
		p.CellFormat(sizeBco, headH, c.tr("Banco"), "T", 0, "MC", false, 0, "")
		p.CellFormat(sizeN, headH, c.tr("Numero"), "T", 0, "MR", false, 0, "")
		p.CellFormat(sizeEm, headH, c.tr("Fecha emisión"), "T", 0, "MC", false, 0, "")
		p.CellFormat(sizeDif, headH, c.tr("Fecha diferida"), "T", 0, "MC", false, 0, "")
		p.CellFormat(sizeDias, headH, c.tr("Días"), "T", 0, "MC", false, 0, "")
		p.CellFormat(sizeMonto, headH, c.tr("Monto"), "RT", 1, "MC", false, 0, "")

		p.SetX(offsetX)
		p.SetFont("Arial", "", 8)
		pintar := false
		sum := dec.D2(0)
		for i, v := range c.Cheques {
			sum += v.Monto
			pintar = !pintar
			bordes := ""
			if i == len(c.Cheques)-1 {
				bordes = "B"
			}
			p.SetX(offsetX)
			colorRenglon(p)
			p.CellFormat(sizeIt, rengH, fmt.Sprint(i+1), "L"+bordes, 0, "MC", pintar, 0, "")
			p.CellFormat(sizeBco, rengH, c.tr(v.Banco), bordes, 0, "M", pintar, 0, "")
			p.CellFormat(sizeN, rengH, c.tr(fmt.Sprint(v.Numero)), bordes, 0, "MR", pintar, 0, "")
			p.CellFormat(sizeEm, rengH, c.tr(v.FechaEmision.String()), bordes, 0, "MC", pintar, 0, "")
			p.CellFormat(sizeDif, rengH, c.tr(v.FechaDiferida.String()), bordes, 0, "MC", pintar, 0, "")
			p.CellFormat(sizeDias, rengH, c.tr(fmt.Sprint(v.Dias)), bordes, 0, "MC", pintar, 0, "")
			p.CellFormat(sizeMonto, rengH, c.tr(v.Monto.String()), "R"+bordes, 1, "MR", pintar, 0, "")
		}
		if len(c.Cheques) > 1 {
			p.SetX(offsetX)
			p.SetFont("Arial", "B", 8)
			p.CellFormat(c.AnchoImprimible()-sizeMonto, headH, c.tr("Total cheques"), "", 0, "MR", false, 0, "")
			colorSubtotal(p)
			p.CellFormat(sizeMonto, headH, c.tr(sum.String()), "", 0, "MR", true, 0, "")
		}
		p.Ln(distEntreCuadros)
	}

	// TARJETAS
	// if len(c.Tarjetas) > 0 {
	// 	p.SetX(offsetX)
	// 	p.SetFillColor(230, 229, 255)

	// 	p.SetFont("Arial", "B", 8)
	// 	p.CellFormat(subtituloW, subtituloH, c.tr("Tarjetas"), "B", 0, "ML", false, 0, "")
	// 	p.Ln(subtituloH)
	// 	sizeTipo := anchoTabla * 0.205
	// 	sizeTarj := anchoTabla * 0.205
	// 	sizeCuot := anchoTabla * 0.14
	// 	sizeCupon := anchoTabla * 0.14
	// 	sizeLote := anchoTabla * 0.14

	// 	p.SetX(offsetX)
	// 	p.CellFormat(sizeIt, headH, c.tr(""), "L", 0, "MC", false, 0, "")
	// 	p.CellFormat(sizeTipo, headH, c.tr("Tipo"), "", 0, "MC", false, 0, "")
	// 	p.CellFormat(sizeTarj, headH, c.tr("Tarjeta"), "", 0, "MC", false, 0, "")
	// 	p.CellFormat(sizeCupon, headH, c.tr("Cupon"), "", 0, "MR", false, 0, "")
	// 	p.CellFormat(sizeLote, headH, c.tr("Lote"), "", 0, "MR", false, 0, "")
	// 	p.CellFormat(sizeCuot, headH, c.tr("Cuotas"), "", 0, "MR", false, 0, "")
	// 	p.CellFormat(sizeMonto, headH, c.tr("Monto"), "R", 0, "MC", false, 0, "")
	// 	p.Ln(headH)

	// 	p.SetFont("Arial", "", 8)
	// 	p.SetFillColor(243, 243, 243)
	// 	pintar := false
	// 	sum := dec.D2(0)
	// 	for i, v := range c.Tarjetas {
	// 		sum += v.Monto
	// 		// pintar = !pintar
	// 		bordes := ""
	// 		if i == len(c.Tarjetas)-1 {
	// 			bordes = "B"
	// 		}
	// 		p.SetX(offsetX)
	// 		colorRenglon(p)
	// 		p.CellFormat(sizeIt, headH, fmt.Sprint(i+1), "L"+bordes, 0, "MC", pintar, 0, "")
	// 		p.CellFormat(sizeTipo, headH, c.tr(v.Tipo), bordes, 0, "MC", pintar, 0, "")
	// 		p.CellFormat(sizeTarj, headH, c.tr(v.Tarjeta), bordes, 0, "MC", pintar, 0, "")
	// 		p.CellFormat(sizeCupon, headH, c.tr(fmt.Sprint(v.Cupon)), bordes, 0, "MR", pintar, 0, "")
	// 		p.CellFormat(sizeLote, headH, c.tr(fmt.Sprint(v.Lote)), bordes, 0, "MR", pintar, 0, "")
	// 		p.CellFormat(sizeCuot, headH, c.tr(fmt.Sprint(v.Cuotas)), bordes, 0, "MR", pintar, 0, "")
	// 		p.CellFormat(sizeMonto, headH, c.tr(v.Monto.String()), "R"+bordes, 0, "MR", pintar, 0, "")
	// 		p.Ln(headH)
	// 	}

	// 	if len(c.Tarjetas) > 1 {
	// 		p.SetFont("Arial", "B", 8)
	// 		p.CellFormat(c.AnchoImprimible()-6/2-sizeMonto, headH, c.tr("Total tarjetas"), "", 0, "MR", false, 0, "")
	// 		colorSubtotal(p)
	// 		p.CellFormat(sizeMonto, headH, c.tr(sum.String()), "", 0, "MR", true, 0, "")
	// 	}
	// 	p.Ln(distEntreCuadros)
	// }

	// RETENCIONES
	if len(c.Retenciones) > 0 {
		p.SetX(offsetX)
		// p.SetFillColor(230, 229, 255)
		p.SetFillColor(249, 249, 249)

		p.SetFont("Arial", "B", 8)
		p.CellFormat(subtituloW, subtituloH, c.tr("Retenciones"), "B", 0, "ML", false, 0, "")
		p.Ln(subtituloH)
		sizeImp := anchoTabla * 0.2
		sizeReg := anchoTabla * 0.37
		sizeDet := anchoTabla * 0.18
		sizeNum := anchoTabla * 0.08

		p.SetX(offsetX)
		p.CellFormat(sizeIt, headH, c.tr(""), "L", 0, "MC", false, 0, "")
		p.CellFormat(sizeImp, headH, c.tr("Impuesto"), "T", 0, "ML", false, 0, "")
		p.CellFormat(sizeReg, headH, c.tr("Regimen"), "T", 0, "ML", false, 0, "")
		p.CellFormat(sizeDet, headH, c.tr("Detalle"), "T", 0, "ML", false, 0, "")
		p.CellFormat(sizeNum, headH, c.tr("Número comp"), "T", 0, "MR", false, 0, "")
		p.CellFormat(sizeMonto, headH, c.tr("Monto"), "RT", 0, "MC", false, 0, "")
		p.Ln(headH)

		p.SetFont("Arial", "", 8)
		p.SetFillColor(243, 243, 243)
		pintar := false
		sum := dec.D2(0)
		for i, v := range c.Retenciones {
			sum += v.Monto
			// pintar = !pintar
			bordes := ""
			if i == len(c.Retenciones)-1 {
				bordes = "B"
			}
			p.SetX(offsetX)
			colorRenglon(p)
			p.CellFormat(sizeIt, headH, fmt.Sprint(i+1), "L"+bordes, 0, "MC", pintar, 0, "")
			p.CellFormat(sizeImp, headH, c.tr(v.Impuesto), bordes, 0, "ML", pintar, 0, "")
			p.CellFormat(sizeReg, headH, c.tr(v.Regimen), bordes, 0, "ML", pintar, 0, "")
			p.CellFormat(sizeDet, headH, c.tr(v.Detalle), bordes, 0, "ML", pintar, 0, "")
			p.CellFormat(sizeNum, headH, c.tr(v.Numero), bordes, 0, "MR", pintar, 0, "")
			p.CellFormat(sizeMonto, headH, c.tr(v.Monto.String()), "R"+bordes, 0, "MR", pintar, 0, "")
			p.Ln(headH)
		}
		if len(c.Retenciones) > 1 {
			p.SetX(offsetX)
			p.SetFont("Arial", "B", 8)
			p.CellFormat(c.AnchoImprimible()-sizeMonto, headH, c.tr("Total retenciones"), "", 0, "MR", false, 0, "")
			colorSubtotal(p)
			p.CellFormat(sizeMonto, headH, c.tr(sum.String()), "", 0, "MR", true, 0, "")
		}

		p.Ln(distEntreCuadros)
	}

	// APLICACIONES
	if len(c.Aplicaciones) > 0 {
		p.SetX(offsetX)
		p.SetFillColor(249, 249, 249)
		sizeComp := anchoTabla * 0.37
		sizeN := anchoTabla * 0.12
		sizeEm := anchoTabla * 0.12
		sizeDif := anchoTabla * 0.12
		sizeDias := anchoTabla * 0.1

		p.SetFont("Arial", "B", 8)

		// p.SetFillColor(230, 229, 255)
		p.CellFormat(subtituloW, subtituloH, c.tr("Comprobantes aplicados"), "B", 0, "ML", true, 0, "")
		p.Ln(subtituloH)

		p.SetX(offsetX)
		p.CellFormat(sizeIt, headH, c.tr(""), "LT", 0, "MC", false, 0, "")
		p.CellFormat(sizeDif, headH, c.tr("Fecha diferida"), "T", 0, "MC", false, 0, "")
		p.CellFormat(sizeComp, headH, c.tr("Comprobante"), "T", 0, "MC", false, 0, "")
		p.CellFormat(sizeN, headH, c.tr("Número"), "T", 0, "MC", false, 0, "")
		p.CellFormat(sizeEm, headH, c.tr("Fecha emisión"), "T", 0, "MC", false, 0, "")
		p.CellFormat(sizeDias, headH, c.tr("Días"), "T", 0, "MC", false, 0, "")
		p.CellFormat(sizeMonto, headH, c.tr("Monto"), "RT", 1, "MC", false, 0, "")

		p.SetX(offsetX)
		p.SetFont("Arial", "", 8)
		pintar := false
		sum := dec.D2(0)
		for i, v := range c.Aplicaciones {
			sum += v.MontoAplicado
			pintar = !pintar
			bordes := ""
			if i == len(c.Cheques)-1 {
				bordes = "B"
			}
			p.SetX(offsetX)
			colorRenglon(p)
			p.CellFormat(sizeIt, rengH, fmt.Sprint(i+1), "L"+bordes, 0, "MC", pintar, 0, "")
			p.CellFormat(sizeDif, rengH, c.tr(v.FechaDiferida.String()), bordes, 0, "MC", pintar, 0, "")
			p.CellFormat(sizeComp, rengH, c.tr(v.NombreComp), bordes, 0, "MC", pintar, 0, "")
			p.CellFormat(sizeN, rengH, c.tr(fmt.Sprint(v.NCompStr)), bordes, 0, "MC", pintar, 0, "")
			p.CellFormat(sizeEm, rengH, c.tr(v.FechaEmision.String()), bordes, 0, "MC", pintar, 0, "")
			p.CellFormat(sizeDias, rengH, c.tr(""), bordes, 0, "MC", pintar, 0, "")
			p.CellFormat(sizeMonto, rengH, c.tr(v.MontoAplicado.String()), "R"+bordes, 1, "MR", pintar, 0, "")
		}

		if len(c.Aplicaciones) > 1 {
			p.SetX(offsetX)
			p.SetFont("Arial", "B", 8)
			p.CellFormat(c.AnchoImprimible()-sizeMonto, headH, c.tr("Total aplicaciones"), "", 0, "MR", false, 0, "")
			colorSubtotal(p)
			p.CellFormat(sizeMonto, headH, c.tr(sum.String()), "", 0, "MR", true, 0, "")
		}

		p.Ln(distEntreCuadros)
	}

}

// Dibuja  la sección footer
func (c *Recibo) printFooter(top float64) {
	p := c.fpdf
	_, h := c.fpdf.GetPageSize()

	offsetY := top + h - c.MargenInf - c.FooterHeight
	offsetX := c.MargenIzq
	p.SetDrawColor(0, 0, 0)
	// Recuadro
	if c.Bordes {
		p.ClipRoundedRect(offsetX, offsetY, c.AnchoImprimible()+c.padding*2, c.FooterHeight, 3, true)
		p.ClipEnd()
	}

	// Monto en texto
	vals := []string{}
	for _, v := range c.Totales {
		vals = append(vals, v.Value.EnPalabras(v.Key))
	}
	p.SetXY(offsetX+c.padding, offsetY+c.padding)
	p.MultiCell(c.AnchoImprimible(), 5, c.tr("SON: "+strings.Join(vals, "; ")), "", "", false)

	paddingCuadrito := 3.
	cuadritoH := paddingCuadrito*2 + float64(len(c.Totales)*5)
	offsetY = top + h - c.MargenInf - cuadritoH - c.padding

	// Firma cajero (va solo en recibos)
	if c.CajeroNombre != "" {
		p.SetXY(c.MargenIzq+90, offsetY+paddingCuadrito*2)
		p.SetDashPattern([]float64{1}, 2)
		p.MultiCell(40, 7, c.tr(fmt.Sprintf("%v", c.CajeroNombre)), "T", "MC", false)
	}

	cuadritoAncho := 20.
	p.SetFont("Arial", "B", 12)
	for _, v := range c.Totales {
		val := fmt.Sprintf("%v %v", c.tr(v.Key), c.tr(v.Value.String()))
		cuadritoAncho = math.Max(cuadritoAncho, p.GetStringWidth(val)+paddingCuadrito*2)
	}

	// Totales (ARS 20.0000)
	offsetX += c.MargenIzq + c.AnchoImprimible() - cuadritoAncho - c.padding

	// Cuadrito total
	p.ClipRoundedRect(
		offsetX,
		offsetY,
		cuadritoAncho,
		cuadritoH, 2, false)
	p.SetFillColor(204, 229, 255)
	p.Rect(
		offsetX,
		offsetY,
		cuadritoAncho, cuadritoH,
		"F")
	p.ClipEnd()

	for i, v := range c.Totales {
		val := fmt.Sprintf("%v  %v", c.tr(v.Key), c.tr(v.Value.String()))
		p.SetXY(offsetX-1.5, offsetY+paddingCuadrito+5*float64(i))
		p.CellFormat(cuadritoAncho, 5, val, "", 0, "R", false, 0, "")
	}
}
