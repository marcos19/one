package pdf

import (
	"bytes"
	"fmt"
	"io"
	"math"

	"github.com/boombuler/barcode/qr"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/jung-kurt/gofpdf"
	"github.com/jung-kurt/gofpdf/contrib/barcode"
	"github.com/rs/zerolog/log"
)

var oro = math.Sqrt(2.0) / 2.

const (
	A4Ancho = 210.0
	A4Alto  = 297.0
)

type Factura struct {
	fpdf *gofpdf.Fpdf
	tr   func(string) string

	// Header
	EmpresaNombre string
	Logo          []byte
	LogoFormato   string

	HeaderRenglonesLeft  []string
	HeaderRenglonesRight []string

	NombreComp string // Incluye nombre, punto de venta y número
	Letra      string
	CodigoComp string

	Fecha fecha.Fecha

	// Persona
	PersonaNombre      string
	SucursalNombre     string
	PersonaCamposLeft  []string
	PersonaCamposRight []string

	// Body
	Productos        []Producto
	PartidasDirectas []PartidaDirecta

	// Resumen
	ConceptosResumen []KV
	TotalComp        dec.D2

	// Footer
	CAE         string
	CAEVto      fecha.Fecha
	Barcode     string
	QR          string
	MarcaDeAgua string

	// Valores
	Valores []KV

	// Condición pago
	CondicionPago string
	Vencimientos  []KV

	// Formato
	CantidadPorHoja float64
	Copias          int

	DiscriminaIVA  bool
	MuestraPrecios bool

	MargenIzq            float64
	MargenDer            float64
	MargenSup            float64
	MargenInf            float64
	MargenEntreSecciones float64

	HeaderHeight  float64
	PersonaHeight float64
	FooterHeight  float64

	Bordes  bool
	padding float64
}

type KV struct {
	Key   string
	Value dec.D2
}

type Producto struct {
	Codigo         string
	Detalle        string
	Cantidad       string
	UM             string
	PrecioUnitario string
	Bonificacion   string
	IVA            string
	Subtotal       string
}
type PartidaDirecta struct {
	Detalle  string
	IVA      string
	Subtotal string
}

func (c Factura) AnchoImprimible() float64 {
	ancho, _ := c.fpdf.GetPageSize()
	return ancho - c.MargenIzq - c.MargenDer - c.padding*2
}

type FormatoArgs struct {
	CantidadPorHoja float64
	Copias          int

	DiscriminaIVA bool

	MargenIzq            float64
	MargenDer            float64
	MargenSup            float64
	MargenInf            float64
	MargenEntreSecciones float64

	HeaderHeight  float64
	PersonaHeight float64
	ResumenHeight float64
	FooterHeight  float64
}

func NewFormatoPaginaCompleta() (f FormatoArgs) {

	f.CantidadPorHoja = 1
	f.Copias = 1

	// Seteo margenes
	f.MargenIzq = A4Ancho * oro / 14
	f.MargenDer = A4Ancho * oro / 14
	f.MargenSup = A4Ancho * oro / 14
	f.MargenInf = A4Ancho * oro / 14
	f.MargenEntreSecciones = A4Ancho * oro / 200 //0.6

	// Secciones
	f.HeaderHeight = A4Alto * oro / 5
	f.PersonaHeight = A4Alto * oro / 8
	f.FooterHeight = A4Alto * oro / 7

	return
}

func NewFormatoDosPorPagina() (f FormatoArgs) {

	f.CantidadPorHoja = 2
	f.Copias = 1

	// Seteo margenes
	f.MargenIzq = 10
	f.MargenDer = 10
	f.MargenSup = 10
	f.MargenInf = 10
	f.MargenEntreSecciones = 0.6

	// Secciones
	f.HeaderHeight = 26
	f.PersonaHeight = 18
	f.ResumenHeight = 14
	f.FooterHeight = 17

	return
}

// NewFactura crea un nuevo comprobante con los valores por defecto.
func NewFactura(a FormatoArgs) (f Factura) {
	f.CantidadPorHoja = a.CantidadPorHoja
	f.Copias = a.Copias

	// Margenes
	f.MargenIzq = a.MargenIzq
	f.MargenDer = a.MargenDer
	f.MargenSup = a.MargenSup
	f.MargenInf = a.MargenInf
	f.MargenEntreSecciones = a.MargenEntreSecciones

	// Secciones
	f.HeaderHeight = a.HeaderHeight
	f.PersonaHeight = a.PersonaHeight
	f.FooterHeight = a.FooterHeight
	f.Bordes = true
	return f
}

// Output graba en el io.Writer la factura PDF
func (c Factura) Output(w io.Writer) (err error) {

	c.fpdf = gofpdf.New("P", "mm", "A4", "")
	c.tr = c.fpdf.UnicodeTranslatorFromDescriptor("")

	p := c.fpdf

	p.SetMargins(c.MargenIzq, c.MargenSup, c.MargenDer)
	p.SetAutoPageBreak(true, c.MargenInf)

	// Fuentes
	fontSize := 8.

	// Encabezado
	// p.SetHeaderFunc(func(c.printHeader)

	p.SetFont("Arial", "", fontSize)

	if c.Bordes {
		c.padding = A4Ancho * oro / 30
		c.MargenIzq -= 3
		c.MargenDer -= 3
		c.MargenInf -= 3
	}
	p.AddPage()

	top := 0.
	for i := 0; i < c.Copias; i++ {

		p.SetDrawColor(0, 0, 0)

		if math.Mod(float64(i), c.CantidadPorHoja) == 0 && i != 0 {
			top = 0
			p.AddPage()
		}
		if top > 0 {
			c.printLineaDeCorte(top)
		}
		c.printHeader(top)
		c.printCuadroCliente(top)
		c.printFooter(top)
		c.printBody(top)
		_, h := p.GetPageSize()
		nuevoI := math.Trunc(float64(i) / c.CantidadPorHoja)
		nuevoI = float64(i)/c.CantidadPorHoja - nuevoI
		top = h / c.CantidadPorHoja * float64(nuevoI+1)
	}

	// Corroboro que no haya habido un error
	err = p.Error()
	if err != nil {
		return errors.Wrap(err, "error durante la creación del PDF")
	}

	err = p.Output(w)
	if err != nil {
		return errors.Wrap(err, "exportando el PDF al responseWriter")
	}
	return
}

func (c *Factura) printLineaDeCorte(top float64) {
	p := c.fpdf
	p.SetDrawColor(192, 192, 192)
	p.SetDashPattern([]float64{2}, 1)
	ancho, _ := p.GetPageSize()
	p.Line(0, top, ancho, top)

	p.SetDrawColor(0, 0, 0)
	p.SetDashPattern([]float64{}, 0)
}

func (c *Factura) printHeader(top float64) {
	// NombreEmpresa
	p := c.fpdf
	top += c.MargenSup
	offsetY := top
	// anchoCuadritoLetra := 10.0

	// difPorImg := 20.
	if len(c.Logo) > 0 {
		switch c.LogoFormato {
		case "png", "jpg":
			// Ok
		default:
			offsetY += c.HeaderHeight*(1-oro) + 1
			goto saltearLogo
		}
		buf := bytes.NewReader(c.Logo)
		p.RegisterImageOptionsReader("logo", gofpdf.ImageOptions{ImageType: c.LogoFormato}, buf)
		info := p.GetImageInfo("logo")
		if info == nil {
			log.Error().Msg("no se pudo encontrar información de imagen")
			goto saltearLogo
		}

		// Limito altura, mantengo proporcion original
		alto := 20.0
		offsetY += alto
		ancho := alto * info.Width() / info.Height()
		p.Image("logo", +c.MargenIzq+c.AnchoImprimible()/4-ancho/2,
			top+2, ancho, alto, false, "", 0, "")
		offsetY += 7
	} else {
		// Lo pongo a la misma altura que 'Factura A 0001'
		offsetY += c.HeaderHeight*(1-oro) + 1
	}

saltearLogo:

	// Nombre empresa
	p.SetLeftMargin(c.MargenIzq)
	p.SetY(offsetY)
	p.SetFont("Arial", "B", 10)
	p.CellFormat(c.AnchoImprimible()/2+c.padding, 1, c.tr(c.EmpresaNombre), "", 1, "C", false, 0, "")
	p.Ln(1)
	// p.WriteAligned(
	// 	c.AnchoImprimible()/2, // Width
	// 	0,                     // Line height
	// 	c.tr(c.EmpresaNombre), "C")

	// Renglones de la izquierda
	p.SetFont("Arial", "", 8)
	offsetY += 3
	p.SetY(offsetY)
	for _, v := range c.HeaderRenglonesLeft {
		p.SetX(c.MargenIzq)
		p.CellFormat(c.AnchoImprimible()/2+c.padding, 3, c.tr(v), "", 1, "C", false, 0, "")
		p.Ln(0)
	}

	// Factura A 0003-23244323
	p.SetXY(c.MargenIzq+c.AnchoImprimible()/2+c.padding, top+7+5)
	p.SetFont("Arial", "B", 12)
	p.CellFormat(c.AnchoImprimible()/2+c.padding, 1, c.tr(c.NombreComp), "", 1, "C", false, 0, "")

	// Fecha
	p.SetXY(c.MargenIzq+c.AnchoImprimible()/2+c.padding, top+7+5+5)
	p.SetFont("Arial", "", 9)
	fchStr := c.tr("Fecha emisión: " + c.Fecha.String())
	// p.Text(c.MargenIzq+c.AnchoImprimible()/2+15, top+20, fchStr)
	p.CellFormat(c.AnchoImprimible()/2+c.padding, 3, fchStr, "", 1, "C", false, 0, "")

	// Renglones de la derecha
	p.SetY(top + 7 + 20)
	p.SetFont("Arial", "", 8)
	for _, v := range c.HeaderRenglonesRight {
		// p.SetX(c.MargenIzq + c.AnchoImprimible()/2)
		p.SetX(c.AnchoImprimible()/2 + c.MargenIzq + c.padding)
		p.CellFormat(c.AnchoImprimible()/2+c.padding, 3, c.tr(v), "", 1, "C", false, 0, "")
		p.Ln(0)
	}

	// Cuadradito con letra A
	cuadSize := 10.0
	p.ClipRoundedRect(c.MargenIzq+c.AnchoImprimible()/2-cuadSize/2+c.padding, top+4, cuadSize, cuadSize, 1, true)
	p.ClipEnd()

	// Letra A
	p.SetFont("Arial", "B", 22)
	p.SetXY(c.MargenIzq, top+cuadSize/2+4.4)
	p.CellFormat(c.AnchoImprimible()+c.padding*2, 0, c.tr(c.Letra), "", 1, "C", false, 0, "")
	// p.Text(c.AnchoImprimible()+c.padding*2, top+11.5, c.Letra)

	// Cod. 1
	p.SetFont("Arial", "", 7)
	p.SetXY(c.MargenIzq, top+16)
	p.CellFormat(c.AnchoImprimible()+c.padding*2, 0, c.tr(c.CodigoComp), "", 1, "C", false, 0, "")
	// p.Text(c.MargenIzq+c.AnchoImprimible()/2-3.5, top+17, c.CodigoComp)

	// Cuadrado del header
	if c.Bordes {
		p.ClipRoundedRect(c.MargenIzq, top, c.AnchoImprimible()+c.padding*2, c.HeaderHeight, 3, true)
		p.ClipEnd()
	}

}

// Dibuja el cuadro del cliente a la altura indicada
func (c *Factura) printCuadroCliente(top float64) {
	p := c.fpdf
	offsetY := top + c.MargenSup + c.HeaderHeight + c.MargenEntreSecciones*2
	offsetX := c.MargenIzq

	// Cuadrado del header
	if c.Bordes {
		p.ClipRoundedRect(offsetX, offsetY, c.AnchoImprimible()+c.padding*2, c.PersonaHeight, 3, true)
		p.ClipEnd()
	}

	offsetX += c.padding
	p.SetLeftMargin(offsetX)

	// Cliente
	offsetY += 2
	p.SetY(offsetY)
	p.SetFont("Arial", "B", 8)
	p.SetTextColor(128, 128, 128)
	p.CellFormat(c.AnchoImprimible(), 3, c.tr("Cliente"), "", 2, "L", false, 0, "")
	p.SetTextColor(0, 0, 0)

	// Nombre
	offsetY += 5
	p.SetFont("Arial", "B", 10)
	p.SetY(offsetY)
	nombre := c.PersonaNombre
	if c.SucursalNombre != "" {
		nombre += " (" + c.SucursalNombre + ")"
	}
	p.CellFormat(c.AnchoImprimible(), 3, c.tr(nombre), "", 2, "L", false, 0, "")
	p.Ln(1.125)

	p.SetFont("Arial", "", 8)

	alturaCampos := p.GetY()
	for _, v := range c.PersonaCamposLeft {
		p.CellFormat(c.AnchoImprimible()/2, 3, c.tr(v), "", 2, "L", false, 0, "")
	}

	p.SetXY(c.MargenIzq+c.AnchoImprimible()/2+c.padding*2, alturaCampos)
	for _, v := range c.PersonaCamposRight {
		p.CellFormat(c.AnchoImprimible()/2, 3, c.tr(v), "", 2, "L", false, 0, "")
	}

}
func (c *Factura) hayBonificacion() bool {
	for _, v := range c.Productos {
		if v.Bonificacion != "" {
			return true
		}
	}
	return false
}

// Dibuja el cuadro del footer
func (c *Factura) printBody(top float64) {
	p := c.fpdf
	offsetY := top + c.MargenSup + c.HeaderHeight + c.MargenEntreSecciones*4 + c.PersonaHeight
	_, h := c.fpdf.GetPageSize()
	bottom := top + h/c.CantidadPorHoja - c.MargenInf - c.FooterHeight - c.MargenEntreSecciones*3

	offsetX := c.MargenIzq

	// Recuadro
	if c.Bordes {
		p.ClipRoundedRect(offsetX, offsetY, c.AnchoImprimible()+c.padding*2, bottom-offsetY, 3, true)
		p.ClipEnd()
		p.SetLeftMargin(c.MargenIzq + 3)
	}

	offsetX += c.padding
	headH := 10.
	renglonH := 5.
	terminaProductosY := 0.

	if len(c.Productos) > 0 { // Productos
		p.SetDrawColor(238, 238, 238)
		p.SetXY(offsetX, offsetY)
		anchoTabla := c.AnchoImprimible()
		sizeCod := anchoTabla * 0.1
		sizeDet := anchoTabla * 0.46
		sizeQ := anchoTabla * 0.1
		sizeUni := anchoTabla * 0.1
		sizeBon := anchoTabla * 0.06
		sizeIVA := anchoTabla * 0.06
		sizeSub := anchoTabla * 0.12

		// Cabecera de productos
		p.SetDrawColor(192, 192, 192)
		p.SetFont("Arial", "B", 8)
		p.CellFormat(sizeCod, headH, c.tr("Código"), "", 0, "MC", false, 0, "")
		p.CellFormat(sizeDet, headH, c.tr("Detalle"), "", 0, "ML", false, 0, "")
		p.CellFormat(sizeQ, headH, c.tr("Cantidad"), "", 0, "MC", false, 0, "")

		x, y := p.GetXY()
		if c.MuestraPrecios {
			p.MultiCell(sizeUni, headH/2, c.tr("Precio unitario"), "", "MR", false)
			p.SetXY(x+sizeUni, y)
		}

		bonif := ""
		if c.hayBonificacion() {
			bonif = "Bonif"
		}
		p.CellFormat(sizeBon, headH, c.tr(bonif), "", 0, "MR", false, 0, "")

		iva := ""
		if c.DiscriminaIVA {
			iva = "IVA"
		}
		if c.MuestraPrecios {
			p.CellFormat(sizeIVA, headH, iva, "", 0, "MR", false, 0, "")
			p.CellFormat(sizeSub, headH, c.tr("Sub-total"), "", 0, "MR", false, 0, "")
		}
		p.Ln(headH)

		// Renglones producto
		p.SetX(offsetX)
		p.SetFont("Arial", "", 8)
		p.SetFillColor(243, 243, 243)
		pintar := false
		for i, v := range c.Productos {
			pintar = !pintar
			borde := ""
			if i == len(c.Productos)-1 {
				borde = ""
			}
			p.SetX(offsetX)
			initialY := p.GetY()
			p.CellFormat(sizeCod, renglonH, c.tr(v.Codigo), ""+borde, 0, "M", pintar, 0, "")
			p.MultiCell(sizeDet, renglonH, c.tr(v.Detalle), borde, "M", pintar)
			proximoLn := p.GetY() - initialY
			p.SetXY(offsetX+sizeCod+sizeDet, initialY)
			p.CellFormat(sizeQ/2, renglonH, c.tr(fmt.Sprintf("%v", v.Cantidad)), borde, 0, "MR", pintar, 0, "")
			p.CellFormat(sizeQ/2, renglonH, c.tr(fmt.Sprintf("%v", v.UM)), borde, 0, "ML", pintar, 0, "")
			if c.MuestraPrecios {
				p.CellFormat(sizeUni, renglonH, c.tr(v.PrecioUnitario), borde, 0, "MR", pintar, 0, "")
				p.CellFormat(sizeBon, renglonH, c.tr(v.Bonificacion), borde, 0, "MR", pintar, 0, "")
			}

			iva := ""
			if c.DiscriminaIVA {
				iva = c.tr(v.IVA)
			}
			if c.MuestraPrecios {
				p.CellFormat(sizeIVA, renglonH, iva, borde, 0, "MR", pintar, 0, "")
				p.CellFormat(sizeSub, renglonH, c.tr(v.Subtotal), ""+borde, -1, "MR", pintar, 0, "")
			}
			p.Ln(proximoLn)
		}
		terminaProductosY = p.GetY()
	}

	if len(c.PartidasDirectas) > 0 { //  Partidas directas
		// Cabecera
		p.SetDrawColor(192, 192, 192)
		p.SetFont("Arial", "B", 8)
		p.SetXY(offsetX, offsetY)

		sizeMonto := c.AnchoImprimible() * 0.12
		sizeIVA := c.AnchoImprimible() * 0.12
		sizeDet := c.AnchoImprimible() - sizeMonto
		if c.DiscriminaIVA {
			sizeDet -= sizeIVA
		}
		p.CellFormat(sizeDet, headH, c.tr("Detalle"), "", 0, "MC", false, 0, "")
		if c.DiscriminaIVA {
			p.CellFormat(sizeIVA, headH, c.tr("IVA"), "", 0, "MR", false, 0, "")
		}
		p.CellFormat(sizeMonto, headH, c.tr("Subtotal"), "", 0, "MR", false, 0, "")

		p.SetX(offsetX)
		p.SetFont("Arial", "", 8)
		p.SetFillColor(243, 243, 243)
		p.Ln(8)
		for i, v := range c.PartidasDirectas {
			borde := ""
			if i == len(c.Productos)-1 {
				borde = ""
			}
			p.SetX(offsetX)
			initialY := p.GetY()
			p.MultiCell(sizeDet, renglonH, c.tr(v.Detalle), borde, "M", false)
			proximoLn := p.GetY() - initialY
			p.SetXY(offsetX+sizeDet, initialY)
			if c.DiscriminaIVA {
				iva := c.tr(v.IVA)
				p.CellFormat(sizeIVA, renglonH, iva, borde, 0, "TR", false, 0, "")
			}
			p.CellFormat(sizeMonto, renglonH, c.tr(v.Subtotal), ""+borde, -1, "TR", false, 0, "")
			p.Ln(proximoLn)
		}
		terminaProductosY = p.GetY()
	}

	// Conceptos (Sólo lo muestro si es A)
	offsetX = c.MargenIzq + c.AnchoImprimible()/2 + c.padding
	p.SetX(offsetX)
	primerCampo := c.AnchoImprimible()/2 - 27
	segundCampo := 27.
	p.Ln(7)
	p.SetFont("Arial", "", 10)
	if c.DiscriminaIVA {
		for _, v := range c.ConceptosResumen {
			p.SetX(offsetX)
			p.CellFormat(primerCampo, 5, c.tr(v.Key), "", 0, "R", false, 0, "")
			p.CellFormat(segundCampo, 5, c.tr(v.Value.String()), "", 1, "R", false, 0, "")
		}
	}

	// Cuadrito total
	if c.MuestraPrecios {
		anchoPintado := 44.0
		p.ClipRoundedRect(
			c.MargenIzq+c.AnchoImprimible()+c.padding-anchoPintado,
			p.GetY(),
			anchoPintado,
			7, 2, false)
		p.SetFillColor(204, 229, 255)
		p.Rect(
			c.MargenIzq+c.AnchoImprimible()+c.padding-anchoPintado,
			p.GetY(),
			anchoPintado, 7,
			"F")
		p.ClipEnd()

		p.SetFont("Arial", "B", 10)
		p.SetXY(offsetX, p.GetY()+1.5)
		p.CellFormat(primerCampo, 4, c.tr("Total"), "", 0, "R", false, 0, "")
		p.CellFormat(segundCampo, 4, c.tr(c.TotalComp.String()), "", 1, "R", false, 0, "")
		p.Ln(10)
	}

	// Condición de pago
	offsetX = c.MargenIzq + c.padding
	c.fpdf.SetLeftMargin(offsetX)
	p.SetX(offsetX)
	p.SetY(terminaProductosY + 15)
	anchoCond := c.AnchoImprimible() / 2
	p.SetFont("Arial", "", 8)
	if c.CondicionPago != "" {
		p.SetX(offsetX)
		p.SetRightMargin(c.MargenIzq + c.AnchoImprimible()/2)
		p.Write(3.5, c.tr("Condición de pago: "+c.CondicionPago))
		// p.SubWrite(ht float64, str string, subFontSize float64, subOffset float64, link int, linkStr string)
		// p.CellFormat(0, 0, c.tr("Condición de pago: "+c.CondicionPago), "", 0, "M", false, 0, "")
		p.Ln(renglonH)
	}
	if len(c.Vencimientos) == 1 {
		p.SetX(offsetX)
		str := fmt.Sprintf("Este comprobante vence el %v", c.Vencimientos[0].Key)
		p.CellFormat(anchoCond, renglonH, c.tr(str), "", 0, "M", false, 0, "")
	}
	if len(c.Vencimientos) > 1 {
		p.CellFormat(anchoCond, renglonH, c.tr("Vencimientos:"), "", 0, "M", false, 0, "")
		p.Ln(renglonH)
	}
	for _, v := range c.Vencimientos {
		// str := fmt.Sprintf("%v: %v", v.Key, v.Value)
		p.SetX(offsetX + 5)
		p.CellFormat(20, 2.5, c.tr(v.Key+":"), "", 0, "ML", false, 0, "")
		p.CellFormat(20, 2.5, c.tr(v.Value.String()), "", 0, "R", false, 0, "")
		p.Ln(3.5)
	}

}

// Dibuja la sección Resumen
// func (c *Factura) printResumen(top float64) {
// p := c.fpdf

// _, h := c.fpdf.GetPageSize()
// offsetY := top + h/c.CantidadPorHoja - c.MargenInf - c.FooterHeight - c.MargenEntreSecciones*2 - c.ResumenHeight
// totalY := offsetY + c.ResumenHeight - 10
// offsetX := c.MargenIzq

// // Recuadro
// if c.Bordes {
// 	p.ClipRoundedRect(offsetX, offsetY, c.AnchoImprimible(), c.ResumenHeight, 3, true)
// 	p.ClipEnd()
// }

// }

// Dibuja  la sección footer
func (c *Factura) printFooter(top float64) {
	p := c.fpdf
	_, h := c.fpdf.GetPageSize()
	offsetY := top + h/c.CantidadPorHoja - c.MargenInf - c.FooterHeight
	offsetX := c.MargenIzq

	// Recuadro
	if c.Bordes {
		p.ClipRoundedRect(offsetX, offsetY, c.AnchoImprimible()+c.padding*2, c.FooterHeight, 3, true)
		p.ClipEnd()
	}

	// QR
	if c.QR != "" {
		qrSize := 24.0
		if c.Bordes {
			qrSize = c.FooterHeight - c.padding*2
		}
		key := barcode.RegisterQR(p, c.QR, qr.L, qr.Unicode)
		offsetY := h - c.MargenInf - qrSize - c.padding
		barcode.Barcode(p, key, offsetX+c.padding, offsetY, qrSize, qrSize, false)

		// CAE y Vto
		p.SetFont("Arial", "", 8)
		p.Text(c.MargenIzq+c.padding+qrSize+2, h-c.MargenInf-c.padding-3, c.tr("CAE: "+c.CAE))
		p.Text(c.MargenIzq+c.padding+qrSize+2, h-c.MargenInf-c.padding, c.tr("Vencimiento CAE: "+c.CAEVto.String()))
	}

	// Código de barras
	if c.Barcode != "" {
		key := barcode.RegisterTwoOfFive(p, c.Barcode, true)
		barcode.Barcode(p, key, offsetX+3, offsetY+3, 100, 7, false)
		p.SetFont("Arial", "", 7)
		p.Text(c.MargenIzq+26, top+h/c.CantidadPorHoja-c.MargenInf-2, c.Barcode)

		// CAE y Vto
		p.SetFont("Arial", "", 8)
		p.Text(c.MargenIzq+c.AnchoImprimible()-45, offsetY+6, c.tr("CAE: "+c.CAE))
		p.Text(c.MargenIzq+c.AnchoImprimible()-45, offsetY+9, c.tr("Vencimiento CAE: "+c.CAEVto.String()))
	}

	if c.MarcaDeAgua != "" {
		_, alto := p.GetPageSize()
		p.SetXY(c.MargenIzq+c.padding, alto/2-20)
		p.SetFont("Arial", "B", 35)
		p.SetAlpha(0.2, "")
		p.SetTextColor(255, 102, 102)
		p.WriteAligned(c.AnchoImprimible(), 20, c.MarcaDeAgua, "C")
		p.SetAlpha(1, "")
		p.SetTextColor(0, 0, 0)
	}
	// // Creo el código de barras
	// bar, err := twooffive.Encode(c.Barcode, true)
	// if err != nil {
	// 	panic(err)
	// }
	// // Lo agrando
	// bar, err = barcode.Scale(bar, 600, 30)
	// if err != nil {
	// 	panic(err)
	// }

	// buf := new(bytes.Buffer)
	// err = png.Encode(buf, bar)
	// if err != nil {
	// 	panic(err)
	// }

	// key := barcode.Register(bcode)
	// var width float64 = 100
	// var height float64 = 10.0
	// barcode.BarcodeUnscalable(pdf, key, 15, 15, &width, &height, false)

	// options := gofpdf.ImageOptions{
	// 	// ImageType: "PNG",
	// }
	// // info := p.RegisterImageOptionsReader("barcode", options, buf)
	// p.RegisterImageOptions("./barcode.png", options)
	// p.ImageOptions("./barcode.png", offsetX, offsetY, 60, 10, false, options, 0, "")
}
