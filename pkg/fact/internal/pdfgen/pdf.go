package pdfgen

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/fact"
	"bitbucket.org/marcos19/one/pkg/fact/config"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/monedas"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/unicidades"
	"github.com/pkg/errors"
)

func PDF(
	ctx context.Context,
	r fact.PDFReq,
	opReader ops.ReaderOne,
	factReader fact.ReaderOne,
	configReader config.ReaderOne,
	compReader comps.ReaderOne,
	logoReader fact.LogoReader,
	unicidadesReader unicidades.ReaderOne,
	monedaReader monedas.ReaderOne,
) (out Outputer, err error) {

	req := ops.ReadOneReq{
		Comitente: r.Comitente,
		ID:        r.OpID,
	}
	op, err := opReader.ReadOne(ctx, req)
	if err != nil {
		return out, errors.Wrap(err, "buscando op")
	}
	if op.Programa != types.PkgID {
		return out, errors.Errorf("la operación solicitada no corresponde al módulo fact")
	}

	switch *op.CompItem {

	// Factura
	case types.ItemFactura, types.ItemAnulacion:
		return facturaPDF(
			ctx,
			req,
			&op,
			factReader,
			configReader,
			compReader,
			logoReader,
		)

	// Recibo
	case types.ItemRecibo:
		return reciboPDF(
			ctx,
			req,
			&op,
			factReader,
			compReader,
			logoReader,
			unicidadesReader,
			monedaReader,
		)

	// Diferimiento
	default:
		return nil, errors.Errorf("comp item %v no tiene determinada impresión", *op.CompItem)
	}
}
