package pdfgen

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"bitbucket.org/marcos19/one/pkg/fact"
	"bitbucket.org/marcos19/one/pkg/fact/internal/pdfgen/internal/pdf"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/monedas"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/unicidades"
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
)

// Pasa los datos de types.Fact => pdf.Fact
func reciboPDF(
	ctx context.Context,
	req ops.ReadOneReq,
	op *ops.Op,
	factReader fact.ReaderOne,
	compReader comps.ReaderOne,
	logoReader fact.LogoReader,
	unicidadesReader unicidades.ReaderOne,
	monedaReader monedas.ReaderOne,
) (Outputer, error) {

	// Busco op
	if op == nil {
		return nil, errors.Errorf("op no ingresada")
	}
	if op.CompItem == nil {
		return nil, errors.Errorf("la op ingresada no tiene definida CompItem")
	}
	if *op.CompItem != types.ItemRecibo {
		return nil, errors.Errorf("la op ingresada no es un recibo")
	}

	// Busco fact
	f, err := factReader.ReadOne(ctx, fact.ReadOneReq{
		Comitente: req.Comitente,
		ID:        op.TranID,
	})
	if err != nil {
		return nil, errors.Wrap(err, "buscando fact")
	}

	// Busco comp
	comp, err := compReader.ReadOne(ctx, comps.ReadOneReq{
		Comitente: req.Comitente,
		ID:        op.CompID,
	})
	if err != nil {
		return nil, errors.Wrap(err, "")
	}

	// Determino cuál es la factura dentro de la transacción

	c := pdf.NewRecibo()
	// Estos ya los tengo en los campos del comp:
	c.Fecha = op.Fecha
	c.NombreComp = fmt.Sprintf("%v %v", op.CompNombre, op.NCompStr)
	c.PersonaNombre = op.PersonaNombre
	c.SucursalNombre = f.Persona.NombreSucursal

	if op.AFIPComprobanteID != nil {
		c.CodigoComp = fmt.Sprint(*op.AFIPComprobanteID)
	}

	// Por defecto si la empresa tiene logo, lo pego
	vaLogo := true
	if comp.ImprimeLogo != nil {
		vaLogo = *comp.ImprimeLogo
	}
	if vaLogo {
		logo, err := logoReader.ReadLogo(ctx, empresas.ReadLogoReq{
			Comitente: req.Comitente,
			Empresa:   op.Empresa,
		})
		if err != nil {
			return nil, errors.Wrap(err, "buscando logo")
		}
		c.Logo = logo
		c.LogoFormato = "png"
	}

	// El resto son los que guardé en el campo Impresion
	{ // Nombre empresa
		nombre, ok := op.Impresion["EmpresaNombre"]
		if !ok {
			return c, errors.Errorf("no se pudo determinar campo NombreEmpresa")
		}
		c.EmpresaNombre, ok = nombre.(string)
		if !ok {
			return c, errors.Errorf("NombreEmpresa no era un string")
		}
	}
	{ // HeaderRenglonesLeft
		rr, ok := op.Impresion["HeaderRenglonesLeft"]
		if !ok {
			return c, errors.Errorf("no se pudo determinar campo HeaderRenglonesLeft")
		}
		str, ok := rr.(string)
		if !ok {
			return c, errors.Errorf("type assertion en HeaderRenglonesLeft")
		}
		c.HeaderRenglonesLeft = strings.Split(str, "\n")
	}
	{ // HeaderRenglonesRight
		rr, ok := op.Impresion["HeaderRenglonesRight"]
		if !ok {
			return c, errors.Errorf("no se pudo determinar campo HeaderRenglonesRight")
		}
		str, ok := rr.(string)
		if !ok {
			return c, errors.Errorf("type assertion en HeaderRenglonesLeft")
		}
		c.HeaderRenglonesRight = strings.Split(str, "\n")
	}
	{ // Letra comp
		v, ok := op.Impresion["LetraComp"]
		if !ok {
			return c, errors.Errorf("no se pudo determinar campo LetraComp")
		}
		c.Letra, ok = v.(string)
		if !ok {
			return c, errors.Errorf("LetraComp no era un string")
		}
	}
	{ // Cajero nombre
		v, ok := op.Impresion["CajeroNombre"]
		if ok {
			c.CajeroNombre, ok = v.(string)
			if !ok {
				return c, errors.Errorf("CajeroNombre no era un string")
			}
		}
	}
	{ // Cajero DNI
		v, ok := op.Impresion["CajeroDNI"]
		if ok {
			c.CajeroDNI, ok = v.(string)
			if !ok {
				return c, errors.Errorf("CajeroDNI no era un string")
			}
		}
	}
	{ // HeaderRenglonesLeft
		rr, ok := op.Impresion["PersonaCamposLeft"]
		if !ok {
			return c, errors.Errorf("no se pudo determinar campo PersonaCamposLeft")
		}
		inter, ok := rr.([]interface{})
		if !ok {
			return c, errors.Errorf(
				"type assertion en PersonaCamposLeft. se esperaba []interface{}, se obtuvo %T",
				rr)
		}

		for _, v := range inter {
			c.PersonaCamposLeft = append(c.PersonaCamposLeft, v.(string))
		}
	}
	{ // HeaderRenglonesRight
		rr, ok := op.Impresion["PersonaCamposRight"]
		if !ok {
			return c, errors.Errorf("no se pudo determinar campo PersonaCamposRight")
		}
		inter, ok := rr.([]interface{})
		if !ok {
			return c, errors.Errorf(
				"type assertion en PersonaCamposLeft. se esperaba []interface{}, se obtuvo %T",
				rr)
		}

		for _, v := range inter {
			c.PersonaCamposRight = append(c.PersonaCamposRight, v.(string))
		}
	}

	// f.Empresa =
	// En el momento que registro el comprobante tengo que dejar pegados
	// los siguientes datos:

	// CajeroNombre
	// CajeroDNI

	// Cheques
	sum := dec.D2(0)
	for _, v := range f.Valores {
		sum += v.Monto

		switch {
		case v.Alias == "Cheques de terceros":
			ch := pdf.Cheque{
				Monto: v.Monto,
			}
			// Sea un cheque nuevo o referenciado ya está persistido.
			// Saco los datos de la tabla unicidades

			// Busco el cheque
			id := uuid.UUID{}
			if v.UnicidadRef != nil {
				id = *v.UnicidadRef
			}
			if v.UnicidadNueva != nil {
				id = v.UnicidadNueva.ID
			}
			unic, err := unicidadesReader.ReadOne(ctx, unicidades.ReadOneReq{Comitente: f.Comitente, ID: id})
			if err != nil {
				return pdf.Recibo{}, err
			}

			// Banco
			ch.Banco, err = unic.AttStr("Nombre Banco")
			if err != nil {
				ch.Banco, err = unic.AttStr("Banco")
				if err != nil {
					// return pdf.Recibo{}, errors.Wrap(err, "buscando atributo en cheque")
					ch.Banco = "Banco N/D"
				}
			}

			// Número
			ch.Numero, _ = unic.AttInt("Número")

			// Fecha diferida
			ch.FechaDiferida, _ = unic.AttFecha("Fecha diferida")

			// Fecha de emisión
			ch.FechaEmision, _ = unic.AttFecha("Fecha emisión")

			// Días
			if !op.Fecha.IsZero() && !ch.FechaDiferida.IsZero() {
				ch.Dias = op.Fecha.Menos(ch.FechaDiferida)
			}

			c.Cheques = append(c.Cheques, ch)
			continue

		// Moneda extranjera
		case v.Moneda != nil:

			val := pdf.MonedaExtranjera{
				Concepto: v.Alias,
				Detalle:  v.Detalle,
				Monto:    v.Monto,
			}
			if v.TC != nil {
				val.TC = *v.TC
			}
			if v.MontoMonedaOriginal != nil {
				val.Original = *v.MontoMonedaOriginal
			}

			if v.Moneda != nil {
				mon, err := monedaReader.ReadOne(ctx, monedas.ReadOneReq{
					Comitente: f.Comitente,
					ID:        *v.Moneda,
				})
				if err != nil {
					val.Moneda = "N/D"
				}
				val.Moneda = mon.Simbolo
			}

			c.MonedaExtranjera = append(c.MonedaExtranjera, val)

		default:
			// Si llegué acá es porque no hay una impresión específica
			val := pdf.MedioDePago{
				Concepto: v.Alias,
				Detalle:  v.Detalle,
				Monto:    v.Monto,
			}
			c.MediosVarios = append(c.MediosVarios, val)

		}
	}

	c.Totales = append(c.Totales, pdf.KV{Key: "ARS", Value: sum})

	for _, v := range f.Aplicaciones {
		apl := pdf.Aplicacion{
			FechaEmision:  v.FechaContable,
			FechaDiferida: v.FechaFinanciera,
			NombreComp:    v.TipoComp,
			NCompStr:      v.NComp,
			MontoAplicado: v.Aplicacion,
		}
		c.Aplicaciones = append(c.Aplicaciones, apl)
	}

	// Tarjetas
	// Retenciones
	// MediosVarios
	// Totales
	// Los llevo a pdf.Factura cada vez que imprima un comprobante.
	return c, nil
}
