package redondeo

import (
	"math"
	"sort"

	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/iva"
	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
)

// El IVA debería calcularlo renglón por renglón de la factura y al final sumarlo.
// Factura electrónica me impone un límite que se determina haciendo el neto
// total de la factura por la alícuota de IVA. Esto me gerera diferencias.
//
// Esta función ajusta los centavos de NETO e IVA en las partidas más grandes.
func RedondearIVA(
	prods []types.PartidaProducto,
	apProds []types.AplicacionProducto,
	dd []types.PartidaDirecta,
	signo int,
	pp []ops.Partida,
	cuentaRedondeo int) (out []ops.Partida, err error) {

	// Montos contables
	montoContableNeto, montoContableIVA, err := montosContables(pp)
	if err != nil {
		return out, errors.Wrap(err, "sumando montos contables")
	}

	// Montos netos
	montoTeoricoNeto, montoTeoricoIVA, montoTeoricoTotal, err := montosTeoricos(prods, apProds, dd, signo)
	if err != nil {
		return out, errors.Wrap(err, "calculando montos teóricos de neto e IVA")
	}

	// Calculo diferencias
	ΔNeto := diferenciasTeoricasContables(montoContableNeto, montoTeoricoNeto)
	ΔIVA := diferenciasTeoricasContables(montoContableIVA, montoTeoricoIVA)
	// fmt.Println("Delta IVA", ΔIVA)
	// fmt.Println("Delta Neto", ΔNeto)

	// Ajusto partidas de NETO
	out = ajustarDiferenciasRedondeoEnPartidas(pp, iva.BaseImponible, ΔNeto, signo)

	// Ajusto paridas de IVA
	out = ajustarDiferenciasRedondeoEnPartidas(out, iva.IVA, ΔIVA, signo)

	// Recalculo cuánto me da el total ahora
	nuevoMontoContableNeto, nuevoMontoContableIVA, err := montosContables(out)
	if err != nil {
		return out, errors.Wrap(err, "sumando montos contables segunda vez")
	}
	montoContableRenglones := sum(nuevoMontoContableIVA, nuevoMontoContableNeto)
	// fmt.Println("Nuevo monto contable", montoContableRenglones, "antes", sum(montoContableIVA, montoContableNeto))

	// Ajusto diferencias entre TOTAL teórico y total renglones
	ΔTotal := diferenciaPatrimonial(montoContableRenglones, montoTeoricoTotal)
	// fmt.Println("Teórico total", montoTeoricoTotal, "Contable renglones", montoContableRenglones, "delta a ajustar", ΔTotal)
	out, err = ajustarDiferenciasRedondeoEnPatrimonial(out, ΔTotal, signo, cuentaRedondeo)
	if err != nil {
		return out, errors.Wrap(err, "ajustando diferencia redondeo partida patrimonial")
	}

	// A esta altura coinciden los totales de los renglones con total teórico, el IVA y el neto.
	// Ajusto si tengo una diferencia partida doble es porque tengo que ajustar patrimonial.
	totalPatrimonial := totalPatrimonial(out)
	totalRenglones := totalRenglones(out)
	ajusteTeoricoContable := totalPatrimonial + totalRenglones
	out, err = ajustarDiferenciasRedondeoEnPatrimonial(out, ajusteTeoricoContable, signo, cuentaRedondeo)
	if err != nil {
		return out, errors.Wrap(err, "ajustando diferencia redondeo partida patrimonial")
	}

	out, err = ajustarPartidaDoble(out, cuentaRedondeo)
	if err != nil {
		return out, errors.Wrap(err, "ajustando partida doble por diferencia de centavos")
	}
	return
}

// Calcula netos e IVA con todos los decimales
func montosTeoricos(
	prods []types.PartidaProducto,
	apProds []types.AplicacionProducto,
	dd []types.PartidaDirecta, signo int) (

	deNeto map[iva.Alicuota]dec.D4,
	deIVA map[iva.Alicuota]dec.D4,
	deTotal dec.D4,
	err error) {

	if signo != 1 && signo != -1 {
		err = errors.Errorf("signo no puede ser %v", signo)
		return
	}
	// IVA teórico con todos los decimales
	deNeto = map[iva.Alicuota]dec.D4{}
	deIVA = map[iva.Alicuota]dec.D4{}

	// Sumo productos
	for i, v := range prods {
		if v.AlicuotaIVA == 0 {
			continue
		}
		porcentaje, err := v.AlicuotaIVA.Porcentaje()
		if err != nil {
			return deNeto, deIVA, deTotal, errors.Wrapf(err, "en renglón de producto %v", i+1)
		}
		neto := v.MontoTotal * dec.D4(signo)
		iva := dec.NewD4(v.MontoTotal.Float() * porcentaje * float64(signo))
		deNeto[v.AlicuotaIVA] += neto
		deIVA[v.AlicuotaIVA] += iva
		deTotal += neto + iva
	}

	// Sumo aplicaciones productos
	for i, v := range apProds {
		if v.AlicuotaIVA == nil {
			continue
		}
		porcentaje, err := v.AlicuotaIVA.Porcentaje()
		if err != nil {
			return deNeto, deIVA, deTotal, errors.Wrapf(err, "en renglón de producto %v", i+1)
		}
		neto := dec.NewD4(v.Monto.Float() * float64(signo))
		iva := dec.NewD4(v.Monto.Float() * porcentaje * float64(signo))
		deNeto[*v.AlicuotaIVA] += neto
		deIVA[*v.AlicuotaIVA] += iva
		deTotal += neto + iva
	}

	// Sumo partidas directas
	for i, v := range dd {
		if v.AlicuotaIVA == 0 {
			continue
		}
		porcentaje, err := v.AlicuotaIVA.Porcentaje()
		if err != nil {
			return deNeto, deIVA, deTotal, errors.Wrapf(err, "en renglón de partida directa %v", i+1)
		}
		neto := v.Monto * dec.D4(signo)
		iva := dec.NewD4(v.Monto.Float() * porcentaje * float64(signo))
		deNeto[v.AlicuotaIVA] += neto
		deIVA[v.AlicuotaIVA] += iva
		deTotal += neto + iva
	}

	return
}

// // Calcula netos e IVA con todos los decimales
// func montosTeoricosTotales(prods []types.PartidaProducto, dd []types.PartidaDirecta, signo int) (
// 	totales map[iva.Alicuota]dec.D4,
// 	err error) {

// 	// IVA teórico con todos los decimales
// 	totales = map[iva.Alicuota]dec.D4{}

// 	// Sumo productos
// 	for i, v := range prods {
// 		if v.AlicuotaIVA == 0 {
// 			continue
// 		}
// 		porcentaje, err := v.AlicuotaIVA.Porcentaje()
// 		if err != nil {
// 			return totales, errors.Wrapf(err, "en renglón de producto %v", i+1)
// 		}
// 		totales[v.AlicuotaIVA] += v.MontoTotal * dec.D4(signo)
// 		totales[v.AlicuotaIVA] += dec.NewD4(v.MontoTotal.Float() * porcentaje * float64(signo))
// 	}

// 	// Sumo partidas directas
// 	for i, v := range dd {
// 		if v.AlicuotaIVA == 0 {
// 			continue
// 		}
// 		porcentaje, err := v.AlicuotaIVA.Porcentaje()
// 		if err != nil {
// 			return totales, errors.Wrapf(err, "en renglón de partida directa %v", i+1)
// 		}
// 		totales[v.AlicuotaIVA] += v.Monto * dec.D4(signo)
// 		totales[v.AlicuotaIVA] += dec.NewD4(v.Monto.Float() * porcentaje * float64(signo))
// 	}

// 	return
// }

// Devueve el total de neto y total de IVA de un grupo de partidas.
func montosContables(pp []ops.Partida) (
	deNeto map[iva.Alicuota]dec.D2,
	deIVA map[iva.Alicuota]dec.D2,
	err error) {

	// Montos contable IVA
	deNeto = map[iva.Alicuota]dec.D2{}
	deIVA = map[iva.Alicuota]dec.D2{}

	for i, v := range pp {
		esBaseImponible := types.PataBaseImponibleIVA(v.Pata)
		if v.Pata != types.PataIVA && !esBaseImponible {
			continue
		}
		if v.ConceptoIVA == nil {
			return deNeto, deIVA, errors.Errorf("partida %v con pata=IVA pero sin concepto definido", i+1)
		}
		if v.AlicuotaIVA == nil {
			return deNeto, deIVA, errors.Errorf("partida %v con pata=IVA pero sin alícuota definida", i+1)
		}

		switch *v.ConceptoIVA {
		case iva.IVA:
			deIVA[*v.AlicuotaIVA] += v.Monto
		case iva.BaseImponible:
			deNeto[*v.AlicuotaIVA] += v.Monto
		}
	}

	return
}

func sum(mm ...map[iva.Alicuota]dec.D2) (out dec.D2) {

	for _, m := range mm {
		for _, v := range m {
			out += v
		}
	}
	return
}

func diferenciasTeoricasContables(
	cont map[iva.Alicuota]dec.D2,
	teor map[iva.Alicuota]dec.D4) (
	deltas map[iva.Alicuota]dec.D2,
) {

	deltas = map[iva.Alicuota]dec.D2{}
	for k, v := range teor {
		deltas[k] += dec.NewD2(v.Float())
	}

	for k, v := range cont {
		deltas[k] -= dec.NewD2(v.Float())
	}

	return
}

func diferenciaPatrimonial(
	cont dec.D2,
	teor dec.D4) (
	delta dec.D2) {

	delta += dec.NewD2(teor.Float())
	delta -= dec.NewD2(cont.Float())

	return
}

// Devuelve una nueva lista de partidas con los saldos ajustados
func ajustarDiferenciasRedondeoEnPartidas(
	pp []ops.Partida,
	conc iva.Concepto,
	Δ map[iva.Alicuota]dec.D2,
	signo int,
) (out []ops.Partida) {

	type PartidaConOrden struct {
		idx int
		ops.Partida
	}

	ordenadas := []PartidaConOrden{}
	for i, v := range pp {
		p := PartidaConOrden{
			idx:     i,
			Partida: v,
		}
		ordenadas = append(ordenadas, p)
		out = append(out, v)
	}

	for alic, dif := range Δ {
		if dif == 0 {
			continue
		}

		filtradas := []PartidaConOrden{}

		for _, part := range ordenadas {
			if part.ConceptoIVA == nil || part.AlicuotaIVA == nil {
				continue
			}
			if *part.ConceptoIVA == conc && *part.AlicuotaIVA == alic {
				filtradas = append(filtradas, part)
			}
		}

		// Las ordeno
		sort.Slice(filtradas, func(i, j int) bool {
			return filtradas[i].Monto*dec.D2(signo) > filtradas[j].Monto*dec.D2(signo)
		})

		if len(filtradas) == 0 {
			// Significa que hay una ajuste para una partida que no encontré.
			// Si los datos ingresados están bien no debería entrar nunca acá
			continue
		}

		// La primera debería ser la más grande
		idx := filtradas[0].idx
		out[idx].Monto += dif
	}

	return
}

// Si hubo una diferencia entre el monto total teórico y el monto contable téorico
// esta función ajusta la partida patrimonial para que coincidan.
func ajustarDiferenciasRedondeoEnPatrimonial(
	pp []ops.Partida,
	Δ dec.D2,
	signo int,
	cuentaAjusteCentavos int,
) (out []ops.Partida, err error) {

	if Δ == 0 {
		return pp, nil
	}
	type PartidaConOrden struct {
		idx int
		ops.Partida
	}

	ordenadas := []PartidaConOrden{}
	for i, v := range pp {
		p := PartidaConOrden{
			idx:     i,
			Partida: v,
		}
		ordenadas = append(ordenadas, p)
		out = append(out, v)
	}

	filtradas := []PartidaConOrden{}

	for _, part := range ordenadas {
		if part.Pata != types.PataFacturaPatrimonial {
			continue
		}
		filtradas = append(filtradas, part)
	}

	// Las ordeno
	sort.Slice(filtradas, func(i, j int) bool {
		return filtradas[i].Monto*dec.D2(signo) > filtradas[j].Monto*dec.D2(signo)
	})

	if len(filtradas) == 0 {
		// Significa que hay una ajuste para una partida que no encontré.
		// Si los datos ingresados están bien no debería entrar nunca acá
		return pp, errors.Errorf("no se pudo encontrar partida patrimonial factura")
	}

	// La primera debería ser la más grande
	idx := filtradas[0].idx
	out[idx].Monto -= Δ

	return
}

func totalPatrimonial(pp []ops.Partida) (sum dec.D2) {
	for _, v := range pp {
		if v.Pata == types.PataFacturaPatrimonial {
			sum += v.Monto
		}
	}
	return
}

func totalRenglones(pp []ops.Partida) (sum dec.D2) {
	for _, v := range pp {
		if esPataQueSumaEnPatrimonial(v.Pata) {
			sum += v.Monto
		}
	}
	return
}

func ajustarPartidaDoble(pp []ops.Partida, cuentaAjusteCentavos int) (out []ops.Partida, err error) {
	out = append(out, pp...)

	totalRenglones := totalRenglones(pp)
	totalPatrimoniales := totalPatrimonial(pp)

	dif := totalRenglones + totalPatrimoniales
	if dif == 0 {
		return
	}
	if math.Abs(dif.Float()) > 0.03 {
		return out, errors.Errorf("ajuste de centavos supera límite (%v)", dif)
	}

	if cuentaAjusteCentavos == 0 {
		return out, errors.Errorf("cuenta ajuste centavos no definida")
	}

	// Creo partida de ajuste de centavos
	p := ops.Partida{}
	p.ID, err = uuid.NewV1()
	if err != nil {
		return out, errors.Wrap(err, "generando uuid")
	}
	p.Monto = -dif
	p.Cuenta = cuentaAjusteCentavos
	out = append(out, p)
	return
}

// Para totalizar los conceptos que van a la cuenta del cliente
func esPataQueSumaEnPatrimonial(p string) bool {
	switch p {
	case types.PataProducto, types.PataPartidaDirecta, types.PataIVA, types.PataReingreso, types.PataProductoNoFacturado:
		return true
	}
	return false
}
