package redondeo

import (
	"context"
	"testing"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/fact/config"
	"bitbucket.org/marcos19/one/pkg/fact/internal/asientos"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/iva"
	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestRedondeoIVA(t *testing.T) {
	require := require.New(t)
	assert := assert.New(t)

	// Conceptos
	gravado := new(iva.Concepto)
	*gravado = iva.BaseImponible
	IVA := new(iva.Concepto)
	*IVA = iva.IVA
	var a105 = new(iva.Alicuota)
	*a105 = iva.IVA10yMedio
	var a21 = new(iva.Alicuota)
	*a21 = iva.IVA21

	// Cuentas
	ctaIVA := 21

	pp := []ops.Partida{}

	directas := []types.PartidaDirecta{
		{
			CuentaID:          2,
			CuentaPatrimonial: 10,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA21,
			Monto:             dec.NewD4(9752.0661),
		},
		{
			CuentaID:          2,
			CuentaPatrimonial: 10,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA21,
			Monto:             dec.NewD4(9752.8926),
		},
		{
			CuentaID:          2,
			CuentaPatrimonial: 10,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA10yMedio,
			Monto:             dec.NewD4(9752.0661),
		},
		{
			CuentaID:          1,
			CuentaPatrimonial: 11,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA21,
			Monto:             dec.NewD4(19668.5950),
		},
	}
	{ // Partida de IVA
		p1 := ops.Partida{}
		p1.Pata = types.PataIVA
		p1.Cuenta = ctaIVA
		p1.Monto = dec.NewD2(2047.93)
		p1.ConceptoIVA = IVA
		p1.AlicuotaIVA = a21

		p2 := ops.Partida{}
		p2.Pata = types.PataIVA
		p2.Cuenta = ctaIVA
		p2.Monto = dec.NewD2(2048.11)
		p2.ConceptoIVA = IVA
		p2.AlicuotaIVA = a21

		p3 := ops.Partida{}
		p3.Pata = types.PataIVA
		p3.Cuenta = ctaIVA
		p3.Monto = dec.NewD2(1023.97)
		p3.ConceptoIVA = IVA
		p3.AlicuotaIVA = a105

		p4 := ops.Partida{}
		p4.Pata = types.PataIVA
		p4.Cuenta = ctaIVA
		p4.Monto = dec.NewD2(4130.40)
		p4.ConceptoIVA = IVA
		p4.AlicuotaIVA = a21
		pp = append(pp, p1, p2, p3, p4)
	}

	{ // Partidas de neto
		p1 := ops.Partida{}
		p1.Pata = types.PataProducto
		p1.Cuenta = ctaIVA
		p1.Monto = dec.NewD2(9752.07)
		p1.ConceptoIVA = gravado
		p1.AlicuotaIVA = a21

		p2 := ops.Partida{}
		p2.Pata = types.PataProducto
		p2.Cuenta = ctaIVA
		p2.Monto = dec.NewD2(9752.89)
		p2.ConceptoIVA = gravado
		p2.AlicuotaIVA = a21

		p3 := ops.Partida{}
		p3.Pata = types.PataProducto
		p3.Cuenta = ctaIVA
		p3.Monto = dec.NewD2(9752.07)
		p3.ConceptoIVA = gravado
		p3.AlicuotaIVA = a105

		p4 := ops.Partida{}
		p4.Pata = types.PataProducto
		p4.Cuenta = ctaIVA
		p4.Monto = dec.NewD2(19668.60)
		p4.ConceptoIVA = gravado
		p4.AlicuotaIVA = a21

		pp = append(pp, p1, p2, p3, p4)
	}

	{ // Partida patrimonial
		p1 := ops.Partida{}
		p1.Pata = types.PataFacturaPatrimonial
		p1.Monto = dec.NewD2(-58176.04)
		pp = append(pp, p1)
	}

	signo := config.SignoFacturaCompra
	out, err := RedondearIVA(nil, nil, directas, signo, pp, 99)
	require.Nil(err)
	assert.Len(out, 9)
	// o := ops.Op{PartidasContables: out}
	// o.AsciiTable(os.Stdout)

	{ // Partida doble
		expected := dec.NewD2(0)
		result := totalDebeYHaber(out)
		assert.Equal(expected, result, "no coincide debe y haber (dif: %v) \n%v", result-expected)
	}
	{ // IVA 21
		expected := dec.NewD2(8226.44 + 0.01)
		result := totalIVA21(out)
		assert.Equal(expected, result, "IVA al 21 no dio, diferencia %v", result-expected)
	}
	{ // IVA 10.5
		expected := dec.NewD2(1023.97)
		result := totalIVA105(out)
		assert.Equal(expected, result, "IVA al 10 no dio, diferencia %v", result-expected)
	}
	{ // Neto 21
		expected := dec.NewD2(39173.56 - 0.01)
		result := totalNeto21(out)
		assert.Equal(expected, result, "Neto al 21 no dio, diferencia %v", result-expected)
	}
	{ // Neto 10.5
		expected := dec.NewD2(9752.07)
		result := totalNeto105(out)
		assert.Equal(expected, result, "Neto al 10 no dio, diferencia %v", result-expected)
	}
	{ // Patrimonial
		expected := dec.NewD2(-58176.04)
		result := totalPatrimonial(out)
		assert.Equal(expected, result, "Patrimonial no dio. Diferencia %v", result-expected)
	}
}

func TestRedondeoIVAVariosProductos(t *testing.T) {

	require := require.New(t)
	assert := assert.New(t)

	// Conceptos
	gravado := new(iva.Concepto)
	*gravado = iva.BaseImponible
	IVA := new(iva.Concepto)
	*IVA = iva.IVA
	var a105 = new(iva.Alicuota)
	*a105 = iva.IVA10yMedio
	var a21 = new(iva.Alicuota)
	*a21 = iva.IVA21

	// Cuentas
	ctaIVA := 21

	pp := []ops.Partida{}

	directas := []types.PartidaDirecta{
		{
			CuentaID:          2,
			CuentaPatrimonial: 10,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA21,
			Monto:             dec.NewD4(8787.8788),
		},
		{
			CuentaID:          2,
			CuentaPatrimonial: 10,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA21,
			Monto:             dec.NewD4(8787.8788),
		},
		{
			CuentaID:          2,
			CuentaPatrimonial: 10,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA21,
			Monto:             dec.NewD4(8787.8788),
		},
	}

	{ // Partida de IVA
		p1 := ops.Partida{}
		p1.Pata = types.PataIVA
		p1.Cuenta = ctaIVA
		p1.Monto = dec.NewD2(1845.45)
		p1.ConceptoIVA = IVA
		p1.AlicuotaIVA = a21

		p2 := p1
		p3 := p1
		pp = append(pp, p1, p2, p3)
	}

	{ // Partidas de neto
		p1 := ops.Partida{}
		p1.Pata = types.PataProducto
		p1.Cuenta = ctaIVA
		p1.Monto = dec.NewD2(8787.88)
		p1.ConceptoIVA = gravado
		p1.AlicuotaIVA = a21

		p2 := p1
		p3 := p1
		pp = append(pp, p1, p2, p3)
	}

	{ // Partida patrimonial
		p1 := ops.Partida{}
		p1.Pata = types.PataFacturaPatrimonial
		p1.Monto = dec.NewD2(-31899.99)
		pp = append(pp, p1)
	}

	signo := config.SignoFacturaCompra
	out, err := RedondearIVA(nil, nil, directas, signo, pp, 99)
	require.Nil(err)
	assert.Len(out, 7)

	// o := ops.Op{PartidasContables: out}
	// o.AsciiTable(os.Stdout)

	{ // Partida doble
		expected := dec.NewD2(0)
		result := totalDebeYHaber(out)
		assert.Equal(expected, result, "no coincide debe y haber (dif: %v) \n%v", result-expected)
	}
	{ // IVA 21
		expected := dec.NewD2(5536.36)
		result := totalIVA21(out)
		assert.Equal(expected, result, "IVA al 21 no dio, diferencia %v", result-expected)
	}
	{ // Neto 21
		expected := dec.NewD2(26363.64)
		result := totalNeto21(out)
		assert.Equal(expected, result, "Neto al 21 no dio, diferencia %v", result-expected)
	}
}

func TestRedondeoIVAVariosProductosSignoContrario(t *testing.T) {

	require := require.New(t)
	assert := assert.New(t)

	// Conceptos
	gravado := new(iva.Concepto)
	*gravado = iva.BaseImponible
	IVA := new(iva.Concepto)
	*IVA = iva.IVA
	var a105 = new(iva.Alicuota)
	*a105 = iva.IVA10yMedio
	var a21 = new(iva.Alicuota)
	*a21 = iva.IVA21

	// Cuentas
	ctaIVA := 21

	pp := []ops.Partida{}

	directas := []types.PartidaDirecta{
		{
			CuentaID:          2,
			CuentaPatrimonial: 10,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA21,
			Monto:             dec.NewD4(8787.8788),
		},
		{
			CuentaID:          2,
			CuentaPatrimonial: 10,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA21,
			Monto:             dec.NewD4(8787.8788),
		},
		{
			CuentaID:          2,
			CuentaPatrimonial: 10,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA21,
			Monto:             dec.NewD4(8787.8788),
		},
	}

	{ // Partida de IVA
		p1 := ops.Partida{}
		p1.Pata = types.PataIVA
		p1.Cuenta = ctaIVA
		p1.Monto = dec.NewD2(-1845.45)
		p1.ConceptoIVA = IVA
		p1.AlicuotaIVA = a21

		p2 := p1
		p3 := p1
		pp = append(pp, p1, p2, p3)
	}

	{ // Partidas de neto
		p1 := ops.Partida{}
		p1.Pata = types.PataProducto
		p1.Cuenta = ctaIVA
		p1.Monto = dec.NewD2(-8787.88)
		p1.ConceptoIVA = gravado
		p1.AlicuotaIVA = a21

		p2 := p1
		p3 := p1
		pp = append(pp, p1, p2, p3)
	}

	{ // Partida patrimonial
		p1 := ops.Partida{}
		p1.Pata = types.PataFacturaPatrimonial
		p1.Monto = dec.NewD2(31899.99)
		pp = append(pp, p1)
	}

	signo := config.SignoFacturaVenta
	out, err := RedondearIVA(nil, nil, directas, signo, pp, 99)
	require.Nil(err)
	assert.Len(out, 7)

	// o := ops.Op{PartidasContables: out}
	// o.AsciiTable(os.Stdout)

	{ // Partida doble
		expected := dec.NewD2(0)
		result := totalDebeYHaber(out)
		assert.Equal(expected, result, "no coincide debe y haber (dif: %v) \n%v", result-expected)
	}
	{ // IVA 21
		expected := dec.NewD2(-5536.36)
		result := totalIVA21(out)
		assert.Equal(expected, result, "IVA al 21 no dio, diferencia %v", result-expected)
	}
	{ // Neto 21
		expected := dec.NewD2(-26363.64)
		result := totalNeto21(out)
		assert.Equal(expected, result, "Neto al 21 no dio, diferencia %v", result-expected)
	}
}

func TestRedondeoIVAUnSoloProducto(t *testing.T) {
	require := require.New(t)
	assert := assert.New(t)
	// Conceptos
	gravado := new(iva.Concepto)
	*gravado = iva.BaseImponible
	IVA := new(iva.Concepto)
	*IVA = iva.IVA
	var a21 = new(iva.Alicuota)
	*a21 = iva.IVA21

	// Cuentas
	ctaIVA := 21

	pp := []ops.Partida{}

	productos := []types.PartidaProducto{
		{
			MontoTotal:  dec.NewD4(454.5455),
			AlicuotaIVA: iva.IVA21,
		},
	}

	{ // Partida de IVA
		p1 := ops.Partida{}
		p1.Pata = types.PataIVA
		p1.Cuenta = ctaIVA
		p1.Monto = dec.NewD2(95.45)
		p1.ConceptoIVA = IVA
		p1.AlicuotaIVA = a21
		pp = append(pp, p1)
	}

	{ // Partidas de neto
		p1 := ops.Partida{}
		p1.Pata = types.PataProducto
		p1.Cuenta = ctaIVA
		p1.Monto = dec.NewD2(454.55)
		p1.ConceptoIVA = gravado
		p1.AlicuotaIVA = a21

		pp = append(pp, p1)
	}

	{ // Partida patrimonial
		p1 := ops.Partida{}
		p1.Pata = types.PataFacturaPatrimonial
		p1.Monto = dec.NewD2(-550.00)
		pp = append(pp, p1)
	}

	signo := config.SignoFacturaCompra
	out, err := RedondearIVA(productos, nil, nil, signo, pp, 99)
	require.Nil(err)
	assert.Len(out, 3)

	{ // Partida doble
		expected := dec.NewD2(0)
		result := totalDebeYHaber(out)
		assert.Equal(expected, result, "no coincide debe y haber (dif: %v) \n%v", result-expected)
	}
	{ // IVA 21
		expected := dec.NewD2(95.45)
		result := totalIVA21(out)
		assert.Equal(expected, result, "IVA al 21 no dio, diferencia %v", result-expected)
	}
	{ // Neto 21
		expected := dec.NewD2(454.55)
		result := totalNeto21(out)
		assert.Equal(expected, result, "Neto al 21 no dio, diferencia %v", result-expected)
	}
	{ // Patrimonial
		expected := dec.NewD2(-550)
		result := totalPatrimonial(out)
		assert.Equal(expected, result, "Patrimonial no dio. Diferencia %v", result-expected)
	}
}

func TestRedondeoIVASignoContrario(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)
	// Conceptos
	gravado := new(iva.Concepto)
	*gravado = iva.BaseImponible
	IVA := new(iva.Concepto)
	*IVA = iva.IVA
	var a105 = new(iva.Alicuota)
	*a105 = iva.IVA10yMedio
	var a21 = new(iva.Alicuota)
	*a21 = iva.IVA21

	// Cuentas
	ctaIVA := 21

	pp := []ops.Partida{}

	directas := []types.PartidaDirecta{
		{
			CuentaID:          2,
			CuentaPatrimonial: 10,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA21,
			Monto:             dec.NewD4(9752.0661),
		},
		{
			CuentaID:          2,
			CuentaPatrimonial: 10,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA21,
			Monto:             dec.NewD4(9752.8926),
		},
		{
			CuentaID:          2,
			CuentaPatrimonial: 10,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA10yMedio,
			Monto:             dec.NewD4(9752.0661),
		},
		{
			CuentaID:          1,
			CuentaPatrimonial: 11,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA21,
			Monto:             dec.NewD4(19668.5950),
		},
	}
	{ // Partida de IVA
		p1 := ops.Partida{}
		p1.Pata = types.PataIVA
		p1.Cuenta = ctaIVA
		p1.Monto = dec.NewD2(-2047.93)
		p1.ConceptoIVA = IVA
		p1.AlicuotaIVA = a21

		p2 := ops.Partida{}
		p2.Pata = types.PataIVA
		p2.Cuenta = ctaIVA
		p2.Monto = dec.NewD2(-2048.11)
		p2.ConceptoIVA = IVA
		p2.AlicuotaIVA = a21

		p3 := ops.Partida{}
		p3.Pata = types.PataIVA
		p3.Cuenta = ctaIVA
		p3.Monto = dec.NewD2(-1023.97)
		p3.ConceptoIVA = IVA
		p3.AlicuotaIVA = a105

		p4 := ops.Partida{}
		p4.Pata = types.PataIVA
		p4.Cuenta = ctaIVA
		p4.Monto = dec.NewD2(-4130.40)
		p4.ConceptoIVA = IVA
		p4.AlicuotaIVA = a21
		pp = append(pp, p1, p2, p3, p4)
	}

	{ // Partidas de netho
		p1 := ops.Partida{}
		p1.Pata = types.PataProducto
		p1.Cuenta = ctaIVA
		p1.Monto = dec.NewD2(-9752.07)
		p1.ConceptoIVA = gravado
		p1.AlicuotaIVA = a21

		p2 := ops.Partida{}
		p2.Pata = types.PataProducto
		p2.Cuenta = ctaIVA
		p2.Monto = dec.NewD2(-9752.89)
		p2.ConceptoIVA = gravado
		p2.AlicuotaIVA = a21

		p3 := ops.Partida{}
		p3.Pata = types.PataProducto
		p3.Cuenta = ctaIVA
		p3.Monto = dec.NewD2(-9752.07)
		p3.ConceptoIVA = gravado
		p3.AlicuotaIVA = a105

		p4 := ops.Partida{}
		p4.Pata = types.PataProducto
		p4.Cuenta = ctaIVA
		p4.Monto = dec.NewD2(-19668.60)
		p4.ConceptoIVA = gravado
		p4.AlicuotaIVA = a21

		pp = append(pp, p1, p2, p3, p4)
	}

	{ // Partida patrimonial
		p1 := ops.Partida{}
		p1.Pata = types.PataFacturaPatrimonial
		p1.Monto = dec.NewD2(58176.04)
		pp = append(pp, p1)
	}

	signo := config.SignoFacturaVenta
	out, err := RedondearIVA(nil, nil, directas, signo, pp, 99)
	require.Nil(err)
	assert.Len(pp, 9)

	{ // Partida doble
		expected := dec.NewD2(0)
		result := totalDebeYHaber(out)
		assert.Equal(expected, result, "no coincide debe y haber (dif: %v) \n%v", result-expected)
	}
	{ // IVA 21
		expected := -dec.NewD2(8226.44 + 0.01)
		result := totalIVA21(out)
		assert.Equal(expected, result, "IVA al 21 no dio, diferencia %v", result-expected)
	}
	{ // IVA 10.5
		expected := -dec.NewD2(1023.97)
		result := totalIVA105(out)
		assert.Equal(expected, result, "IVA al 10,5 no dio, diferencia %v", result-expected)
	}
	{ // Neto 21
		expected := -dec.NewD2(39173.56 - 0.01)
		result := totalNeto21(out)
		assert.Equal(expected, result, "Neto al 21 no dio, diferencia %v", result-expected)
	}
	{ // Neto 10.5
		expected := -dec.NewD2(9752.07)
		result := totalNeto105(out)
		assert.Equal(expected, result, "Neto al 10,5 no dio, diferencia %v", result-expected)
	}
}

func TestAjustarDiferenciasRedondeoEnPartidas(t *testing.T) {

	assert := assert.New(t)

	// Conceptos
	gravado := new(iva.Concepto)
	*gravado = iva.BaseImponible
	IVA := new(iva.Concepto)
	*IVA = iva.IVA
	var a105 = new(iva.Alicuota)
	*a105 = iva.IVA10yMedio
	var a21 = new(iva.Alicuota)
	*a21 = iva.IVA21

	// Cuentas
	ctaIVA := 21

	pp := []ops.Partida{}

	f := types.Fact{}
	f.PartidasDirectas = []types.PartidaDirecta{
		{
			CuentaID:          2,
			CuentaPatrimonial: 10,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA21,
			Monto:             dec.NewD4(9752.0661),
		},
		{
			CuentaID:          2,
			CuentaPatrimonial: 10,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA21,
			Monto:             dec.NewD4(9752.8926),
		},
		{
			CuentaID:          2,
			CuentaPatrimonial: 10,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA10yMedio,
			Monto:             dec.NewD4(9752.0661),
		},
		{
			CuentaID:          1,
			CuentaPatrimonial: 11,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA21,
			Monto:             dec.NewD4(19668.5950),
		},
	}
	{ // Partida de IVA
		p1 := ops.Partida{}
		p1.Pata = types.PataIVA
		p1.Cuenta = ctaIVA
		p1.Monto = dec.NewD2(2047.93)
		p1.ConceptoIVA = IVA
		p1.AlicuotaIVA = a21

		p2 := ops.Partida{}
		p2.Pata = types.PataIVA
		p2.Cuenta = ctaIVA
		p2.Monto = dec.NewD2(2048.11)
		p2.ConceptoIVA = IVA
		p2.AlicuotaIVA = a21

		p3 := ops.Partida{}
		p3.Pata = types.PataIVA
		p3.Cuenta = ctaIVA
		p3.Monto = dec.NewD2(1023.97)
		p3.ConceptoIVA = IVA
		p3.AlicuotaIVA = a105

		p4 := ops.Partida{}
		p4.Pata = types.PataIVA
		p4.Cuenta = ctaIVA
		p4.Monto = dec.NewD2(4130.40)
		p4.ConceptoIVA = IVA
		p4.AlicuotaIVA = a21
		pp = append(pp, p1, p2, p3, p4)
	}

	{ // Partidas de neto
		p1 := ops.Partida{}
		p1.Pata = types.PataProducto
		p1.Cuenta = ctaIVA
		p1.Monto = dec.NewD2(9752.07)
		p1.ConceptoIVA = gravado
		p1.AlicuotaIVA = a21

		p2 := ops.Partida{}
		p2.Pata = types.PataProducto
		p2.Cuenta = ctaIVA
		p2.Monto = dec.NewD2(9752.89)
		p2.ConceptoIVA = gravado
		p2.AlicuotaIVA = a21

		p3 := ops.Partida{}
		p3.Pata = types.PataProducto
		p3.Cuenta = ctaIVA
		p3.Monto = dec.NewD2(9752.07)
		p3.ConceptoIVA = gravado
		p3.AlicuotaIVA = a105

		p4 := ops.Partida{}
		p4.Pata = types.PataProducto
		p4.Cuenta = ctaIVA
		p4.Monto = dec.NewD2(19668.60)
		p4.ConceptoIVA = gravado
		p4.AlicuotaIVA = a21

		pp = append(pp, p1, p2, p3, p4)
	}

	signo := config.SignoFacturaCompra

	{ // Partida patrimonial
		p1 := ops.Partida{}
		p1.Pata = types.PataFacturaPatrimonial
		p1.Monto = dec.NewD2(-58176.04)
		pp = append(pp, p1)
	}

	{ // Ajustes SIN MODIFICACIONES => Debería quedar igual
		m := map[iva.Alicuota]dec.D2{}
		res := ajustarDiferenciasRedondeoEnPartidas(pp, iva.BaseImponible, m, signo)
		assert.Equal(pp, res)
	}

	{ // Ajustes en NETO
		ajustes := map[iva.Alicuota]dec.D2{
			iva.IVA21:       dec.NewD2(0.01),
			iva.IVA10yMedio: dec.NewD2(0.03),
		}
		res := ajustarDiferenciasRedondeoEnPartidas(pp, iva.BaseImponible, ajustes, signo)
		nuevosNetos, _, err := montosContables(res)
		assert.Nil(err)
		netoExpected := map[iva.Alicuota]dec.D2{
			iva.IVA21:       dec.NewD2(39173.56 + 0.01),
			iva.IVA10yMedio: dec.NewD2(9752.07 + 0.03),
		}
		assert.Equal(netoExpected, nuevosNetos)
	}

	{ // Ajustes en IVA
		ajustes := map[iva.Alicuota]dec.D2{
			iva.IVA21:       dec.NewD2(0.01),
			iva.IVA10yMedio: dec.NewD2(0.03),
		}
		res := ajustarDiferenciasRedondeoEnPartidas(pp, iva.IVA, ajustes, signo)
		_, nuevosIVAs, err := montosContables(res)
		assert.Nil(err)
		netoExpected := map[iva.Alicuota]dec.D2{
			iva.IVA21:       dec.NewD2(8226.44 + 0.01),
			iva.IVA10yMedio: dec.NewD2(1023.97 + 0.03),
		}
		assert.Equal(netoExpected, nuevosIVAs)
	}

	{ // Ajustes en IVA (negativos)
		ajustes := map[iva.Alicuota]dec.D2{
			iva.IVA21:       dec.NewD2(-0.01),
			iva.IVA10yMedio: dec.NewD2(-0.03),
		}
		res := ajustarDiferenciasRedondeoEnPartidas(pp, iva.IVA, ajustes, signo)
		_, nuevosIVAs, err := montosContables(res)
		assert.Nil(err)
		netoExpected := map[iva.Alicuota]dec.D2{
			iva.IVA21:       dec.NewD2(8226.44 - 0.01),
			iva.IVA10yMedio: dec.NewD2(1023.97 - 0.03),
		}
		assert.Equal(netoExpected, nuevosIVAs)
	}
}

func TestMontosTeoricos(t *testing.T) {
	assert := assert.New(t)

	directas := []types.PartidaDirecta{
		{
			CuentaID:          2,
			CuentaPatrimonial: 10,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA21,
			Monto:             dec.NewD4(9752.0661),
		},
		{
			CuentaID:          2,
			CuentaPatrimonial: 10,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA21,
			Monto:             dec.NewD4(9752.8926),
		},
		{
			CuentaID:          2,
			CuentaPatrimonial: 10,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA10yMedio,
			Monto:             dec.NewD4(9752.0661),
		},
		{
			CuentaID:          1,
			CuentaPatrimonial: 11,
			Concepto:          iva.BaseImponible,
			AlicuotaIVA:       iva.IVA21,
			Monto:             dec.NewD4(19668.5950),
		},
	}

	{ // Signo factura comprar
		signo := config.SignoFacturaCompra
		netos, ivas, _, err := montosTeoricos(nil, nil, directas, signo)
		assert.Nil(err)

		{ // Netos
			expected := map[iva.Alicuota]dec.D4{
				iva.IVA21:       dec.NewD4(39173.5537),
				iva.IVA10yMedio: dec.NewD4(9752.0661),
			}
			assert.Equal(expected, netos)
		}

		{ // IVA
			expected := map[iva.Alicuota]dec.D4{
				iva.IVA21:       dec.NewD4(8226.446277),
				iva.IVA10yMedio: dec.NewD4(1023.9669405),
			}
			assert.Equal(expected, ivas)
		}
	}

	{ // Signo factura venta
		signo := config.SignoFacturaVenta
		netos, ivas, _, err := montosTeoricos(nil, nil, directas, signo)
		assert.Nil(err)

		{ // Netos
			expected := map[iva.Alicuota]dec.D4{
				iva.IVA21:       dec.NewD4(-39173.5537),
				iva.IVA10yMedio: dec.NewD4(-9752.0661),
			}
			assert.Equal(expected, netos)
		}

		{ // IVA
			expected := map[iva.Alicuota]dec.D4{
				iva.IVA21:       dec.NewD4(-8226.446277),
				iva.IVA10yMedio: dec.NewD4(-1023.9669405),
			}
			assert.Equal(expected, ivas)
		}
	}
}

func TestMontosContables(t *testing.T) {
	assert := assert.New(t)

	// Conceptos
	gravado := new(iva.Concepto)
	*gravado = iva.BaseImponible
	IVA := new(iva.Concepto)
	*IVA = iva.IVA
	var a105 = new(iva.Alicuota)
	*a105 = iva.IVA10yMedio
	var a21 = new(iva.Alicuota)
	*a21 = iva.IVA21

	pp := []ops.Partida{
		{
			Pata:        types.PataPartidaDirecta,
			ConceptoIVA: gravado,
			AlicuotaIVA: a21,
			Monto:       dec.NewD2(9752.07),
		},
		{
			Pata:        types.PataIVA,
			ConceptoIVA: IVA,
			AlicuotaIVA: a21,
			Monto:       dec.NewD2(2047.93),
		},
		{
			Pata:        types.PataPartidaDirecta,
			ConceptoIVA: gravado,
			AlicuotaIVA: a21,
			Monto:       dec.NewD2(9752.89),
		},
		{
			Pata:        types.PataIVA,
			ConceptoIVA: IVA,
			AlicuotaIVA: a21,
			Monto:       dec.NewD2(2048.11),
		},
		{
			Pata:        types.PataPartidaDirecta,
			ConceptoIVA: gravado,
			AlicuotaIVA: a105,
			Monto:       dec.NewD2(9752.07),
		},
		{
			Pata:        types.PataIVA,
			ConceptoIVA: IVA,
			AlicuotaIVA: a105,
			Monto:       dec.NewD2(1023.97),
		},
		{
			Pata:        types.PataPartidaDirecta,
			ConceptoIVA: gravado,
			AlicuotaIVA: a21,
			Monto:       dec.NewD2(19668.6),
		},
		{
			Pata:        types.PataIVA,
			ConceptoIVA: IVA,
			AlicuotaIVA: a21,
			Monto:       dec.NewD2(4130.4),
		},
	}
	netos, ivas, err := montosContables(pp)
	assert.Nil(err)

	{ // Netos
		expected := map[iva.Alicuota]dec.D2{
			iva.IVA21:       dec.NewD2(39173.56),
			iva.IVA10yMedio: dec.NewD2(9752.07),
		}
		assert.Equal(expected, netos)
	}

	{ // IVA
		expected := map[iva.Alicuota]dec.D2{
			iva.IVA21:       dec.NewD2(8226.44),
			iva.IVA10yMedio: dec.NewD2(1023.97),
		}
		assert.Equal(expected, ivas)
	}
}
func TestAgruparPartidasIVA(t *testing.T) {
	assert := assert.New(t)
	ctx := context.Background()
	c := 1
	r := &mockCuentas{}

	// Conceptos
	var gravado = new(iva.Concepto)
	*gravado = iva.IVA
	var a105 = new(iva.Alicuota)
	*a105 = iva.IVA10yMedio
	var a21 = new(iva.Alicuota)
	*a21 = iva.IVA21

	ctaIVA := 21

	p1 := ops.Partida{}
	p1.Pata = types.PataIVA
	p1.Cuenta = ctaIVA
	p1.ConceptoIVA = gravado
	p1.AlicuotaIVA = a21
	p1.Monto = dec.NewD2(100)

	p2 := ops.Partida{}
	p2.Pata = types.PataIVA
	p2.Cuenta = ctaIVA
	p2.ConceptoIVA = gravado
	p2.AlicuotaIVA = a21
	p2.Monto = dec.NewD2(100)

	p3 := ops.Partida{}
	p3.Pata = types.PataIVA
	p3.Cuenta = ctaIVA
	p3.ConceptoIVA = gravado
	p3.AlicuotaIVA = a105
	p3.Monto = dec.NewD2(50)

	pat := ops.Partida{}
	pat.Pata = types.PataFacturaPatrimonial
	pat.Cuenta = 45
	pat.Monto = 50

	{ // Ninguna partida IVA
		pp := []ops.Partida{}
		agr, err := asientos.AgruparPartidas(ctx, c, pp, r)
		assert.Nil(err)
		assert.Len(agr, 0)
		assert.Equal(dec.NewD2(0), totalIVA105(agr))
		assert.Equal(dec.NewD2(0), totalIVA21(agr))
	}
	{ // Dos alicuotas IVA
		pp := []ops.Partida{p1, p2, p3}
		agr, err := asientos.AgruparPartidas(ctx, c, pp, r)
		assert.Nil(err)
		assert.Len(agr, 2)
		assert.Equal(dec.NewD2(50), totalIVA105(agr))
		assert.Equal(dec.NewD2(200), totalIVA21(agr))
	}
	{ // Una partida que no es IVA
		pp := []ops.Partida{p1, p2, p3, pat}
		agr, err := asientos.AgruparPartidas(ctx, c, pp, r)
		assert.Nil(err)
		assert.Len(agr, 3)
		assert.Equal(dec.NewD2(50), totalIVA105(agr))
		assert.Equal(dec.NewD2(200), totalIVA21(agr))
	}
}

func TestAgruparPartidasPatrimoniales(t *testing.T) {

	ctx := context.Background()
	c := 1
	r := &mockCuentas{}

	// Conceptos
	var persona = new(uuid.UUID)
	*persona = uuid.FromStringOrNil("f1c57d91-1b8f-4411-806d-f69ed11252fa")
	var sucursal = new(int)
	*sucursal = 54968
	var centro = new(int)
	*centro = 112

	ctaClientes := 21
	ctaClientes2 := 22

	p1 := ops.Partida{}
	p1.Pata = types.PataFacturaPatrimonial
	p1.Cuenta = ctaClientes
	p1.Persona = persona
	p1.Sucursal = sucursal
	p1.Centro = centro
	p1.Monto = dec.NewD2(100)

	p2 := ops.Partida{}
	p2.Pata = types.PataFacturaPatrimonial
	p2.Cuenta = ctaClientes
	p2.Persona = persona
	p2.Sucursal = sucursal
	p2.Centro = centro
	p2.Monto = dec.NewD2(100)

	p3 := ops.Partida{}
	p3.Pata = types.PataFacturaPatrimonial
	p3.Cuenta = ctaClientes2
	p3.Persona = persona
	p3.Sucursal = sucursal
	p3.Centro = centro
	p3.Monto = dec.NewD2(50)

	iv := ops.Partida{}
	iv.Pata = types.PataIVA
	iv.Cuenta = 45
	iv.Monto = dec.NewD2(133)

	ren := ops.Partida{}
	ren.Pata = types.PataProducto
	ren.Cuenta = 45
	ren.Monto = dec.NewD2(18.22)

	{ // Ninguna partida IVA
		pp := []ops.Partida{}
		agr, err := asientos.AgruparPartidas(ctx, c, pp, r)
		assert.Nil(t, err)
		assert.Len(t, agr, 0)
		assert.Equal(t, totalPat(agr, *persona, 0, 0, 0), dec.NewD2(0))
	}
	{ // Dos cuentas contables distintas
		pp := []ops.Partida{p1, p2, p3}
		agr, err := asientos.AgruparPartidas(ctx, c, pp, r)
		assert.Nil(t, err)
		assert.Len(t, agr, 2)
		assert.Equal(t, totalPat(agr, *persona, ctaClientes, *sucursal, *centro), dec.NewD2(200))
		assert.Equal(t, totalPat(agr, *persona, ctaClientes2, *sucursal, *centro), dec.NewD2(50))
	}
	{ // Con partidas que no son patrimoniales
		pp := []ops.Partida{p1, p2, p3, iv, ren}
		agr, err := asientos.AgruparPartidas(ctx, c, pp, r)
		assert.Nil(t, err)
		assert.Len(t, agr, 4)
		assert.Equal(t, totalPat(agr, *persona, ctaClientes, *sucursal, *centro), dec.NewD2(200))
		assert.Equal(t, totalPat(agr, *persona, ctaClientes2, *sucursal, *centro), dec.NewD2(50))
	}
}

func totalPat(pp []ops.Partida, persona uuid.UUID, cuenta, sucursal, centro int) (out dec.D2) {

	for _, v := range pp {
		if v.Cuenta != cuenta {
			continue
		}
		if v.Persona != nil {
			if *v.Persona != persona {
				continue
			}
		}
		if v.Centro != nil {
			if centro != *v.Centro {
				continue
			}
		}
		if v.Sucursal != nil {
			if sucursal != *v.Sucursal {
				continue
			}
		}
		out += v.Monto
	}
	return
}

func totalNeto21(pp []ops.Partida) (sum dec.D2) {

	for _, v := range pp {
		if v.ConceptoIVA == nil {
			continue
		}
		if v.AlicuotaIVA == nil {
			continue
		}
		if *v.ConceptoIVA != iva.BaseImponible {
			continue
		}
		if *v.AlicuotaIVA != iva.IVA21 {
			continue
		}
		sum += v.Monto
	}
	return
}
func totalNeto105(pp []ops.Partida) (sum dec.D2) {

	for _, v := range pp {
		if v.ConceptoIVA == nil {
			continue
		}
		if v.AlicuotaIVA == nil {
			continue
		}
		if *v.ConceptoIVA != iva.BaseImponible {
			continue
		}
		if *v.AlicuotaIVA != iva.IVA10yMedio {
			continue
		}
		sum += v.Monto
	}
	return
}
func totalIVA21(pp []ops.Partida) (sum dec.D2) {

	for _, v := range pp {
		if v.ConceptoIVA == nil {
			continue
		}
		if v.AlicuotaIVA == nil {
			continue
		}
		if *v.ConceptoIVA != iva.IVA {
			continue
		}
		if *v.AlicuotaIVA != iva.IVA21 {
			continue
		}
		sum += v.Monto
	}
	return
}
func totalIVA105(pp []ops.Partida) (sum dec.D2) {

	for _, v := range pp {
		if v.ConceptoIVA == nil {
			continue
		}
		if v.AlicuotaIVA == nil {
			continue
		}
		if *v.ConceptoIVA != iva.IVA {
			continue
		}
		if *v.AlicuotaIVA != iva.IVA10yMedio {
			continue
		}
		sum += v.Monto
	}
	return
}
func totalDebeYHaber(pp []ops.Partida) (sum dec.D2) {

	for _, v := range pp {
		sum += v.Monto
	}
	return
}

type mockCuentas struct{}

func (m *mockCuentas) ReadOne(context.Context, cuentas.ReadOneReq) (out cuentas.Cuenta, err error) {
	return
}
