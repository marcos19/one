package reingreso

import (
	"context"
	"testing"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestGenerarReingreso(t *testing.T) {

	require := require.New(t)
	assert := assert.New(t)

	factura := ops.Op{}
	factura.Persona = new(uuid.UUID)
	*factura.Persona = uuid.FromStringOrNil("b20103d6-cbcb-43cd-a81f-b8ffdca44840")
	ctx := context.Background()
	ctasGetter := ctaGetterMock{}

	emp := 2
	iva := CuentaGananciaIVA // La cuenta de ganancia
	pat := CuentaPatrimonial
	rei := CuentaIVANoComputable // La cuenta de pérdida
	monto := dec.NewD2(21)
	signoFacturaCompra := 1

	t.Run("sin empresa", func(t *testing.T) { // Sin empresa
		_, err := GenerarOp(ctx, factura, monto, nil, &iva, &pat, &rei, signoFacturaCompra, ctasGetter)
		require.NotNil(err)
	})
	t.Run("sin cuenta IVA", func(t *testing.T) { // Sin cuenta iva
		_, err := GenerarOp(ctx, factura, monto, &emp, nil, &pat, &rei, signoFacturaCompra, ctasGetter)
		require.NotNil(err)
	})

	t.Run("sin cuenta patrimonial", func(t *testing.T) { // Sin cuenta patrimonial
		_, err := GenerarOp(ctx, factura, monto, &emp, &iva, nil, &rei, signoFacturaCompra, ctasGetter)
		require.NotNil(err)
	})
	t.Run("sin cuenta reingreso", func(t *testing.T) {
		_, err := GenerarOp(ctx, factura, monto, &emp, &iva, &pat, nil, signoFacturaCompra, ctasGetter)
		require.NotNil(err)
	})
	t.Run("sin signo", func(t *testing.T) {
		_, err := GenerarOp(ctx, factura, monto, &emp, &iva, &pat, &rei, 0, ctasGetter)
		require.NotNil(err)
	})

	t.Run("sin cuentas contables", func(t *testing.T) {
		_, err := GenerarOp(ctx, factura, monto, &emp, &iva, &pat, &rei, signoFacturaCompra, ctasGetter)
		require.NotNil(err)
	})

	{ // Creo OP COMPRA
		gastoReal := CuentaGasto
		iva := CuentaGananciaIVA // La cuenta de ganancia
		ivaReal := CuentaIVA
		patReal := CuentaPatrimonial
		rei := CuentaIVANoComputable // La cuenta de pérdida
		pp := []ops.Partida{
			{
				Pata:   types.PataProducto,
				Cuenta: gastoReal,
				Monto:  dec.NewD2(100),
			},
			{
				Pata:   types.PataIVA,
				Cuenta: ivaReal,
				Monto:  dec.NewD2(21),
			},
			{
				Pata:    types.PataFacturaPatrimonial,
				Cuenta:  patReal,
				Persona: factura.Persona,
				Monto:   dec.NewD2(-121),
			},
		}
		factura.PartidasContables = pp

		t.Run("compra con monto reingreso = IVA", func(t *testing.T) {
			r, err := GenerarOp(ctx, factura, monto, &emp, &iva, &pat, &rei, signoFacturaCompra, ctasGetter)
			require.Nil(err)
			assert.NotNil(r)
			// assert.Len(r.PartidasContables, 4, "%v", spew.Sdump(r.PartidasContables))
			assert.Len(r.PartidasContables, 4, "%v", r.AsciiTableString())
			assert.Equal(dec.NewD2(100), sumCuenta(r.PartidasContables, pat))
			assert.Equal(dec.NewD2(-21), sumCuenta(r.PartidasContables, iva))
			assert.Equal(dec.NewD2(21), sumCuenta(r.PartidasContables, rei))
			assert.Equal(dec.NewD2(-100), sumCuenta(r.PartidasContables, gastoReal))
		})

		t.Run("compra con_monto_reingreso_cero", func(t *testing.T) {
			r, err := GenerarOp(ctx, factura, 0, &emp, &iva, &pat, &rei, signoFacturaCompra, ctasGetter)
			require.Nil(err)
			require.NotNil(r)
			require.NotNil(r.PartidasContables)
			assert.Len(r.PartidasContables, 3)
			assert.Equal(dec.NewD2(121), sumCuenta(r.PartidasContables, pat))
			assert.Equal(dec.NewD2(-21), sumCuenta(r.PartidasContables, iva))
			assert.Equal(dec.NewD2(0), sumCuenta(r.PartidasContables, rei))
			assert.Equal(dec.NewD2(-100), sumCuenta(r.PartidasContables, gastoReal))
		})

		// La operación original tiene que quedar igual
		assert.Equal(dec.NewD2(100), sumCuenta(factura.PartidasContables, gastoReal))
		assert.Equal(dec.NewD2(21), sumCuenta(factura.PartidasContables, ivaReal))
		assert.Equal(dec.NewD2(-121), sumCuenta(factura.PartidasContables, patReal))
	}

	t.Run("monto negativo", func(t *testing.T) {
		_, err := GenerarOp(ctx, factura, -21, &emp, &iva, &pat, &rei, signoFacturaCompra, ctasGetter)
		require.NotNil(err)
	})

	{ // Creo OP VENTA
		gastoReal := CuentaGasto
		ivaReal := CuentaIVA
		patReal := CuentaPatrimonial
		pp := []ops.Partida{
			{
				Pata:   types.PataProducto,
				Cuenta: gastoReal,
				Monto:  dec.NewD2(-100),
			},
			{
				Pata:   types.PataIVA,
				Cuenta: ivaReal,
				Monto:  dec.NewD2(-21),
			},
			{
				Pata:    types.PataFacturaPatrimonial,
				Cuenta:  patReal,
				Persona: factura.Persona,
				Monto:   dec.NewD2(121),
			},
		}
		factura.PartidasContables = pp
		signoFacturaVenta := -1
		t.Run("venta con monto reingreso = IVA", func(t *testing.T) {
			r, err := GenerarOp(ctx, factura, monto, &emp, &iva, &pat, &rei, signoFacturaVenta, ctasGetter)
			require.Nil(err)
			assert.NotNil(r)
			assert.Len(r.PartidasContables, 4)
			assert.Equal(dec.NewD2(-100), sumCuenta(r.PartidasContables, pat))
			assert.Equal(dec.NewD2(21), sumCuenta(r.PartidasContables, iva))
			assert.Equal(dec.NewD2(-21), sumCuenta(r.PartidasContables, rei))
			assert.Equal(dec.NewD2(100), sumCuenta(r.PartidasContables, gastoReal))
		})

		t.Run("venta con monto reingreso = 0", func(t *testing.T) {
			r, err := GenerarOp(ctx, factura, 0, &emp, &iva, &pat, &rei, signoFacturaVenta, ctasGetter)
			require.Nil(err)
			require.NotNil(r)
			require.NotNil(r.PartidasContables)
			assert.Len(r.PartidasContables, 3)
			assert.Equal(dec.NewD2(-121), sumCuenta(r.PartidasContables, pat))
			assert.Equal(dec.NewD2(21), sumCuenta(r.PartidasContables, iva))
			assert.Equal(dec.NewD2(0), sumCuenta(r.PartidasContables, rei))
			assert.Equal(dec.NewD2(100), sumCuenta(r.PartidasContables, gastoReal))
		})

		// La operación original tiene que quedar igual
		assert.Equal(pp, factura.PartidasContables)
	}
}

func sumCuenta(pp []ops.Partida, cta int) (sum dec.D2) {

	for _, v := range pp {
		if v.Cuenta != cta {
			continue
		}
		sum += v.Monto
	}
	return sum
}

type ctaGetterMock struct{}

func (c ctaGetterMock) PorFuncionContable(ctx context.Context, comitente int, f cuentas.Funcion) ([]cuentas.Cuenta, error) {
	return nil, nil
}
func (c ctaGetterMock) ReadOne(ctx context.Context, req cuentas.ReadOneReq) (cuentas.Cuenta, error) {
	switch req.ID {
	case CuentaIVANoComputable:
		return cuentas.Cuenta{ID: req.ID, Nombre: "IVA no computable"}, nil
	case CuentaPatrimonial:
		return cuentas.Cuenta{ID: req.ID, Nombre: "Proveedores", UsaAplicaciones: true}, nil
	case CuentaDeudoresVarios:
		return cuentas.Cuenta{ID: req.ID, Nombre: "Deudores varios", UsaAplicaciones: true}, nil
	case CuentaGasto:
		return cuentas.Cuenta{ID: req.ID, Nombre: "Gasto real", UsaAplicaciones: true}, nil
	case CuentaIVA:
		return cuentas.Cuenta{ID: req.ID, Nombre: "IVA"}, nil
	case CuentaGananciaIVA:
		return cuentas.Cuenta{ID: req.ID, Nombre: "Ganancia por IVA"}, nil
	}
	return cuentas.Cuenta{}, errors.Errorf("not implemented")
}

const (
	CuentaIVA            = 2301
	CuentaGasto          = 9500
	CuentaPatrimonial    = 1401
	CuentaDeudoresVarios = 1499

	CuentaIVANoComputable = 9501
	CuentaGananciaIVA     = 9502
)
