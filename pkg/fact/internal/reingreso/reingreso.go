package reingreso

import (
	"context"
	"time"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/fact/internal/asientos"
	"bitbucket.org/marcos19/one/pkg/fact/internal/factura"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/iva"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
)

// Crea una nueva op en la otra empresa, con las partidas revertidas.
// Op es la op que esta en fact.Factura
// Si montoReingresado == IVA => la empresa paga el 100% del IVA.
// Si montoReingresado == 0   => todo el IVA de la factura es ganancia para la empresa.
//
// Esta función NO está preparada para funcionar sobre facts que utilizan
// opprod.
func GenerarOp(
	ctx context.Context,
	op ops.Op, // La factura que voy a reingresar
	reingresoMonto dec.D2,
	reingresoEmpresa *int,
	reingresoCuentaIVA *int,
	reingresoCuentaPatrimonial *int,
	reingresoCuentaReingreso *int,
	signoRenglonProducto int,
	cuentasGetter cuentas.ReaderOne,
) (out *ops.Op, err error) {

	// Valido
	if reingresoEmpresa == nil {
		return out, errors.Errorf("no se ingresó empresa de reingreso")
	}
	if reingresoCuentaIVA == nil {
		return out, errors.Errorf("no se ingresó cuenta de IVA de reingreso")
	}
	if reingresoCuentaPatrimonial == nil {
		return out, errors.Errorf("no se ingresó cuenta de patrimonial de reingreso")
	}
	if reingresoCuentaReingreso == nil {
		return out, errors.Errorf("no se ingresó cuenta resultado de reingreso")
	}

	switch signoRenglonProducto {
	case 1, -1:
		// ok
	default:
		return out, errors.Errorf("el signo debe ser 1 o -1")
	}
	if len(op.PartidasContables) == 0 {
		return out, errors.Errorf("la factura no tenía partidas contable")
	}
	if reingresoMonto < 0 {
		return out, errors.Errorf("el monto reingreso no puede ser menor a cero")
	}

	// Copio la original
	op.ID, err = uuid.NewV1()
	if err != nil {
		return out, err
	}
	op.CompItem = new(int)
	*op.CompItem = types.ItemReingreso
	op.Empresa = *reingresoEmpresa
	op.Impresion = tipos.JSON{}
	op.CreatedAt = new(time.Time)
	*op.CreatedAt = time.Now()
	if op.Detalle != "" {
		op.Detalle += " - "
	}
	op.Detalle += "Reingreso"
	pp := []ops.Partida{}

	// Doy vuelta los signos
	for _, v := range op.PartidasContables {

		// Borro los ID, se pegan luego
		v.ID = uuid.Nil
		v.PartidaAplicada = nil

		// Cambio signo
		v.Monto *= -1

		// Las aplicaciones de esta otra op no tienen nada que ver
		// con las aplicaciones de la op original
		if v.PartidaAplicada != nil {
			v.PartidaAplicada = nil
		}

		// Reemplazo las cuentas contables
		switch v.Pata {

		case types.PataIVA:
			v.Cuenta = *reingresoCuentaIVA

		case types.PataFacturaPatrimonial:
			v.Cuenta = *reingresoCuentaPatrimonial
		}
		pp = append(pp, v)
	}

	// Creo partida de reingreso y contracuenta
	if reingresoMonto != 0 {

		// Partida de resultado
		p := ops.Partida{}
		p.Pata = types.PataReingreso
		p.Monto = reingresoMonto * dec.D2(signoRenglonProducto)
		p.Cuenta = *reingresoCuentaReingreso
		p.ConceptoIVA = new(iva.Concepto)
		*p.ConceptoIVA = iva.IVA
		p.AlicuotaIVA = determinarAlicuotas(op.PartidasContables)
		pp = append(pp, p)

		// Partida patrimonial
		c := ops.Partida{}
		c.Pata = types.PataFacturaPatrimonial
		c.Monto = -reingresoMonto * dec.D2(signoRenglonProducto)
		c.Cuenta = *reingresoCuentaPatrimonial
		c.Persona = new(uuid.UUID)
		*c.Persona = *op.Persona
		if op.Sucursal != nil {
			c.Sucursal = new(int)
			*c.Sucursal = *op.Sucursal
		}
		pat, err := cuentasGetter.ReadOne(ctx, cuentas.ReadOneReq{
			Comitente: op.Comitente,
			ID:        *reingresoCuentaPatrimonial,
		})
		if err != nil {
			return out, errors.Wrap(err, "buscando cuenta patrimonial")
		}
		if pat.AperturaCentro {
			c.Centro, err = determinarCentro(op.PartidasContables)
			if err != nil {
				return out, errors.Wrap(err, "determinando centro de partida patrimonial de reingreso")
			}
		}
		pp = append(pp, c)
	}

	// Agrupo partidas patrimoniales
	pp, err = asientos.AgruparPartidas(ctx, op.Comitente, pp, cuentasGetter)
	if err != nil {
		return out, errors.Wrap(err, "agrupando partidas patrimoniales")
	}

	// Poner ID a las partidas aplicadas
	err = llenarPartidasPatrimoniales(ctx, op.Comitente, pp, cuentasGetter)
	if err != nil {
		return out, errors.Wrap(err, "llenando campo PartidaAplicada en partidas patrimoniales")
	}

	op.TotalesConceptos, err = factura.CalcularConceptos(pp, -signoRenglonProducto)
	if err != nil {
		return out, errors.Wrap(err, "calculanto totales conceptos")
	}
	op.TotalComp = new(dec.D2)
	*op.TotalComp = op.TotalesConceptos.Sum()

	op.PartidasContables = pp

	return &op, nil
}

// Si la cuenta patrimonial abre por centro, debo tomar un criterio para asignarle
// el centro al reingreso.
// Si las partidas patrimonial tienen un solo centro tomo ese.
// Si hay varios, tiro error
func determinarCentro(pp []ops.Partida) (out *int, err error) {
	m := map[int]struct{}{}
	for _, v := range pp {
		if v.Pata == types.PataFacturaPatrimonial {
			if v.Centro == nil {
				continue
			}
			m[*v.Centro] = struct{}{}
		}
	}
	if len(m) == 0 {
		return nil, nil
	}
	if len(m) == 1 {
		for k := range m {
			out = new(int)
			*out = k
			return
		}
	}
	return nil, errors.Errorf("se encontró más de un centro en cuentas patrimoniales, no se puede determinar centro de reingreso")
}

func determinarAlicuotas(pp []ops.Partida) (out *iva.Alicuota) {
	m := map[iva.Alicuota]struct{}{}
	for _, v := range pp {
		if v.AlicuotaIVA == nil {
			continue
		}
		m[*v.AlicuotaIVA] = struct{}{}
	}
	if len(m) == 1 {
		for k := range m {
			out = new(iva.Alicuota)
			*out = k
			return
		}
	}
	out = new(iva.Alicuota)
	*out = iva.IVANoGravado
	return
}

// Llena el campo PartidaAplicada para aquellas partidas que sean patrimoniales
// y cuyas cuentas estén tildadas como UsaAplicaciones
func llenarPartidasPatrimoniales(ctx context.Context, comitente int, pp []ops.Partida, cg cuentas.ReaderOne) (err error) {
	for i, v := range pp {
		// Si ya viene con partida aplicada es porque estoy aplicando a un
		// producto (ej: haciendo rececpión de una orden de compra)
		if v.PartidaAplicada != nil {
			continue
		}
		cta, err := cg.ReadOne(ctx, cuentas.ReadOneReq{
			Comitente: comitente,
			ID:        v.Cuenta,
		})
		if err != nil {
			return errors.Wrapf(err, "buscando cuenta %v", v.Cuenta)
		}
		if !cta.UsaAplicaciones {
			continue
		}

		// Debería llegar con un ID
		if v.ID == uuid.Nil {
			return errors.Errorf("partida con cuenta %v sin ID", v.Cuenta)
		}

		pp[i].PartidaAplicada = new(uuid.UUID)
		*pp[i].PartidaAplicada = pp[i].ID
	}

	return
}
