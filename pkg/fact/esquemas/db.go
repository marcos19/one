package esquemas

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/fact/permisos"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	"github.com/jackc/pgx/v4/pgxpool"
)

func readOne(ctx context.Context, conn *pgxpool.Pool, req ReadOneReq) (v Esquema, err error) {
	query := `SELECT 
		id,
		comitente,
		empresa,
		config,
		grupo,
		comprobantes,
		cajas,
		valores,
		partidas_directas_config,
		nombre,
		detalle,
		cae,
		enviar_facturas_por_mail,
		enviar_recibos_por_mail,
		created_at, 
		updated_at,
		depositos,
		condiciones
FROM fact_esquemas WHERE comitente=$1 AND id=$2`

	err = conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(
		&v.ID,
		&v.Comitente,
		&v.Empresa,
		&v.Config,
		&v.Grupo,
		&v.Comprobantes,
		&v.Cajas,
		&v.Valores,
		&v.PartidasDirectasConfig,
		&v.Nombre,
		&v.Detalle,
		&v.CAE,
		&v.EnviarFacturasPorMail,
		&v.EnviarRecibosPorMail,
		&v.CreatedAt,
		&v.UpdatedAt,
		&v.Depositos,
		&v.Condiciones,
	)
	if err != nil {
		return v, deferror.DB(err, "querying/scanning")
	}

	v.Permisos, err = permisos.PorEsquema(ctx, conn, permisos.PorEsquemaReq{
		Comitente: req.Comitente,
		ID:        req.ID,
	})
	if err != nil {
		return v, errors.Wrap(err, "buscando permisos de la config")
	}
	if v.Permisos == nil {
		v.Permisos = []permisos.Permiso{}
	}
	if v.Cajas == nil {
		v.Cajas = tipos.GrupoInts{}
	}
	if v.Depositos == nil {
		v.Depositos = tipos.GrupoInts{}
	}
	if v.Condiciones == nil {
		v.Condiciones = tipos.GrupoInts{}
	}
	return
}

func readMany(ctx context.Context, conn *pgxpool.Pool, req ReadManyReq) (out []Esquema, err error) {
	out = []Esquema{}

	ww := []string{"comitente=$1"}
	pp := []interface{}{req.Comitente}

	if req.Empresa != 0 {
		pp = append(pp, req.Empresa)
		ww = append(ww, fmt.Sprintf("empresa=$%v", len(pp)))
	}

	if req.Config != 0 {
		pp = append(pp, req.Config)
		ww = append(ww, fmt.Sprintf("config=$%v", len(pp)))
	}
	if req.Tipo != 0 {
		pp = append(pp, req.Tipo)
		ww = append(ww, fmt.Sprintf("id IN (SELECT id FROM fact_config WHERE tipo_op = $%v)", len(pp)))
	}

	// Si tiene user es porque estoy haciendo un lookup
	if req.User != "" {
		if req.Config == 0 {
			return out, errors.Errorf("lookup requiere que se ingrese config")
		}
		pp = append(pp, req.User, req.Config)
		ww = append(ww, fmt.Sprintf(`
		id IN (
			SELECT esquema FROM fact_permisos WHERE (
				usuario=$%v OR 
				(usuario IS NULL AND grupo_usuario IS NULL) OR 
				grupo_usuario IN (
					SELECT id
					FROM usuarios_grupos
					WHERE comitente=$%v AND usuarios @> ARRAY[$%v]
				)
			) AND "create"=true AND config=$%v
		)`, len(pp)-1, 1, len(pp)-1, len(pp)))
	}

	where := "WHERE " + strings.Join(ww, " AND ")
	query := fmt.Sprintf(`SELECT 
		id,
		comitente,
		empresa,
		config,
		grupo,
		comprobantes,
		cajas,
		valores,
		partidas_directas_config,
		nombre,
		detalle,
		cae,
		enviar_facturas_por_mail,
		enviar_recibos_por_mail,
		created_at, 
		updated_at,
		depositos,
		condiciones
FROM fact_esquemas 
%v
ORDER BY nombre`, where)

	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		v := Esquema{}
		err = rows.Scan(
			&v.ID,
			&v.Comitente,
			&v.Empresa,
			&v.Config,
			&v.Grupo,
			&v.Comprobantes,
			&v.Cajas,
			&v.Valores,
			&v.PartidasDirectasConfig,
			&v.Nombre,
			&v.Detalle,
			&v.CAE,
			&v.EnviarFacturasPorMail,
			&v.EnviarRecibosPorMail,
			&v.CreatedAt,
			&v.UpdatedAt,
			&v.Depositos,
			&v.Condiciones,
		)
		if err != nil {
			return out, deferror.DB(err, "querying/scanning")
		}
		if v.Cajas == nil {
			v.Depositos = tipos.GrupoInts{}
		}
		if v.Condiciones == nil {
			v.Depositos = tipos.GrupoInts{}
		}
		if v.Depositos == nil {
			v.Depositos = tipos.GrupoInts{}
		}
		if v.Permisos == nil {
			v.Permisos = []permisos.Permiso{}
		}
		out = append(out, v)
	}

	return
}
func create(ctx context.Context, conn *pgxpool.Pool, req Esquema) (err error) {
	query := `INSERT INTO fact_esquemas (
		comitente,
		empresa,
		config,
		grupo,
		comprobantes,
		cajas,
		valores,
		partidas_directas_config,
		nombre,
		detalle,
		cae,
		enviar_facturas_por_mail,
		enviar_recibos_por_mail,
		created_at,
		depositos,
		condiciones
		) 
	VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16)`

	_, err = conn.Exec(ctx, query,
		req.Comitente,
		req.Empresa,
		req.Config,
		req.Grupo,
		req.Comprobantes,
		req.Cajas,
		req.Valores,
		req.PartidasDirectasConfig,
		req.Nombre,
		req.Detalle,
		req.CAE,
		req.EnviarFacturasPorMail,
		req.EnviarRecibosPorMail,
		req.CreatedAt,
		req.Depositos,
		req.Condiciones,
	)
	if err != nil {
		return deferror.DB(err, "inserting")
	}
	return
}

func update(ctx context.Context, conn *pgxpool.Pool, req Esquema) (err error) {

	query := `UPDATE fact_esquemas SET
		empresa=$3,
		config=$4,
		grupo=$5,
		comprobantes=$6,
		cajas=$7,
		valores=$8,
		partidas_directas_config=$9,
		nombre=$10,
		detalle=$11,
		cae=$12,
		enviar_facturas_por_mail=$13,
		enviar_recibos_por_mail=$14,
		updated_at=$15,
		depositos=$16,
		condiciones=$17
	WHERE comitente=$1 AND id=$2
	`
	res, err := conn.Exec(ctx, query, req.Comitente, req.ID,
		req.Empresa,
		req.Config,
		req.Grupo,
		req.Comprobantes,
		req.Cajas,
		req.Valores,
		req.PartidasDirectasConfig,
		req.Nombre,
		req.Detalle,
		req.CAE,
		req.EnviarFacturasPorMail,
		req.EnviarRecibosPorMail,
		time.Now(),
		req.Depositos,
		req.Condiciones,
	)
	if err != nil {
		return deferror.DB(err, "updating")
	}

	if res.RowsAffected() == 0 {
		return errors.New("no se modificó ningún registro")
	}

	return
}

func delete(ctx context.Context, conn *pgxpool.Pool, req ReadOneReq, del Deleter) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó comitente")
	}
	if req.ID == 0 {
		return deferror.Validation("no se ingresó ID a borrar")
	}

	// Inicio TX
	tx, err := conn.Begin(ctx)
	if err != nil {
		return errors.Wrap(err, "iniciando tx")
	}
	defer tx.Rollback(ctx)

	{ // Está usado en FACT?
		count := 0
		err = tx.QueryRow(ctx, "SELECT COUNT(id) FROM fact WHERE comitente=$1 AND esquema=$2",
			req.Comitente, req.ID).Scan(&count)
		if err != nil {
			return deferror.DB(err, "determinando si el esquema estaba usado")
		}
		if count > 0 {
			return deferror.Validationf("no se puede borrar porque el esquema ya fue usado %v veces", count)
		}
	}

	// Borro permisos del esquema
	err = del.DeletePermisosDeEsquema(ctx, tx, req.Comitente, req.ID)
	if err != nil {
		return errors.Wrap(err, "borrando permisos del esquema")
	}

	// Borro esquema
	res, err := tx.Exec(ctx,
		"DELETE FROM fact_esquemas WHERE comitente=$1 AND id=$2", req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "deleting")
	}
	if res.RowsAffected() == 0 {
		return errors.New("no se borró ningún registro")
	}

	// Confirmo TX
	err = tx.Commit(ctx)
	if err != nil {
		return errors.Wrap(err, "confirmando transacción")
	}

	return
}
