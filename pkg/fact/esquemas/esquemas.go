package esquemas

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"reflect"
	"strconv"
	"time"

	"bitbucket.org/marcos19/one/pkg/fact/permisos"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
)

// Esquema representa un conjunto de transacción, comprobantes y permisos.
type Esquema struct {
	ID        int `json:",string"`
	Comitente int `json:",string"`
	Empresa   int `json:",string"`
	Config    int `json:",string"`
	Grupo     string

	// Define por ejemplo: {Factura: 342, Recibo: 4243}
	Comprobantes ComprobantesAsociados
	Nombre       string
	Detalle      string

	//
	CAE CAEOpcion

	Cajas                  tipos.GrupoInts
	Valores                tipos.GrupoInts
	PartidasDirectasConfig PartidasDirectasConfig

	Depositos   tipos.GrupoInts
	Condiciones tipos.GrupoInts

	EnviarFacturasPorMail bool
	EnviarRecibosPorMail  bool
	CreatedAt             *time.Time
	UpdatedAt             *time.Time
	// EnviarMailAnulacion bool

	// Solo para mandar al frontend en con ReadOne
	Permisos []permisos.Permiso
}

type CAEOpcion string

const (
	CAEAutomatico    = "Automático"
	CAEPreguntar     = "Preguntar"
	CAENingunaAccion = ""
)

func (e Esquema) TableName() string {
	return "fact_esquemas"
}

type ComprobanteAsociado int

const (
	AsociadoFactura          = "Factura"
	AsociadoRecibo           = "Recibo"
	AsociadoFacturaAnulacion = "Factura anulación"
)

type PartidaDirectaConfig struct {
	Cuenta       int `json:",string"`
	Conceptos    tipos.GrupoInts
	Alicuotas    tipos.GrupoInts
	ContraCuenta int `json:",string"`
}

type PartidasDirectasConfig []PartidaDirectaConfig

// Value cumple con la interface SQL
func (j PartidasDirectasConfig) Value() (driver.Value, error) {
	return json.Marshal(j)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (j *PartidasDirectasConfig) Scan(src interface{}) error {
	if src == nil {
		*j = PartidasDirectasConfig{}
		return nil
	}

	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.Errorf("Type assertion error .([]byte). Era: %v", reflect.TypeOf(src))

	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, j)
	if err != nil {
		return errors.Wrapf(err, "Unmarshalling los siguientes bytes: '%s'", source)
	}

	return nil
}

type ComprobantesAsociados map[string]ComprobanteAsociado

// Value cumple con la interface SQL
func (j ComprobantesAsociados) Value() (driver.Value, error) {
	return json.Marshal(j)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (j *ComprobantesAsociados) Scan(src interface{}) error {
	if src == nil {
		*j = ComprobantesAsociados{}
		return nil
	}

	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.Errorf("Type assertion error .([]byte). Era: ", reflect.TypeOf(src))
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, j)
	if err != nil {
		return errors.Wrapf(err, "Unmarshalling los siguientes bytes: '%s'", source)
	}

	return nil
}

func (g *ComprobantesAsociados) UnmarshalJSON(b []byte) error {
	mapStrings := map[string]*string{}
	err := json.Unmarshal(b, &mapStrings)
	if err != nil {
		// Si no puedo como strings, pruebo si está viniendo como []int
		mapInts := map[string]ComprobanteAsociado{}
		err2 := json.Unmarshal(b, &mapInts)
		if err != nil {
			return errors.Wrap(err, err.Error()+": "+err2.Error()+": no se pudo leer como []int ni como []string")
		}
		*g = ComprobantesAsociados(mapInts)
		return nil
	}

	out := map[string]ComprobanteAsociado{}
	for k, v := range mapStrings {
		if v == nil {
			continue
		}
		enInt, err := strconv.Atoi(*v)
		if err != nil {
			return errors.Wrapf(err, "convirtiendo a int %v", v)
		}
		out[k] = ComprobanteAsociado(enInt)
	}
	*g = ComprobantesAsociados(out)

	return nil
}

func (g ComprobantesAsociados) MarshalJSON() ([]byte, error) {

	out := map[string]string{}
	for k, v := range g {
		out[k] = fmt.Sprint(v)
	}

	return json.Marshal(out)
}
