package esquemas

import (
	"context"
	"fmt"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/niler"
	"github.com/dgraph-io/ristretto"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/rs/zerolog/log"
)

type Handler struct {
	conn           *pgxpool.Pool
	cache          *ristretto.Cache
	permisoDeleter Deleter
}

func NewHandler(c HandlerArgs) (h *Handler, err error) {

	// Valido
	if niler.IsNil(c.Conn) {
		return h, errors.New("Conn no puede ser nil")
	}
	if niler.IsNil(c.Cache) {
		return h, errors.New("Cache no puede ser nil")
	}
	if niler.IsNil(c.PermisoDeleter) {
		return h, errors.New("Cache no puede ser nil")
	}

	h = &Handler{
		conn:           c.Conn,
		cache:          c.Cache,
		permisoDeleter: c.PermisoDeleter,
	}

	return
}

type HandlerArgs struct {
	Conn           *pgxpool.Pool
	Cache          *ristretto.Cache
	PermisoDeleter Deleter
}

// Borra los permisos de un determinado esquema
type Deleter interface {
	DeletePermisosDeEsquema(ctx context.Context, tx pgx.Tx, comitente int, id int) error
}

func (h *Handler) Create(ctx context.Context, req Esquema) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se determinó comitente")
	}
	if req.Nombre == "" {
		return deferror.Validation("Debe ingresar un nombre para el esquema")
	}
	if req.Empresa == 0 {
		return deferror.Validation("No se seleccionó a que empresa corresponde")
	}
	if req.Config == 0 {
		return deferror.Validation("No se determinó que transacción usa")
	}

	// Inserto
	err = create(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "persistiendo esquema")
	}

	return
}

func (h *Handler) Update(ctx context.Context, req Esquema) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se determinó comitente")
	}
	if req.ID == 0 {
		return deferror.Validation("no se determinó ID")
	}

	// Por las dudas borro antes
	h.cache.Del(hashInt(ReadOneReq{Comitente: req.Comitente, ID: req.ID}))

	// Modifico
	err = update(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "modificando esquema")
	}

	return
}

type ReadOneReq struct {
	ID        int `json:",string"`
	Comitente int
}

func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (out Esquema, err error) {
	if req.ID == 0 {
		return out, deferror.Validation("no se determinó ID")
	}
	if req.Comitente == 0 {
		return out, deferror.Validation("no se determinó comitente")
	}

	// Busco en cache
	val, estaba := h.cache.Get(hashInt(req))
	if estaba {
		out, ok := val.(Esquema)
		if ok && req.Comitente == out.Comitente {
			log.Debug().Str("tabla", "fact_esquemas").Msg("leído de cache")
			return out, nil
		}
	}

	// No estaba, busco en DB
	out, err = readOne(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DBf(err, "buscando esquema por ID=%v", req.ID)
	}

	// Lo grabo en el cache
	h.cache.Set(hashInt(req), out, 0)

	return
}

type ReadManyReq struct {
	Comitente int
	Empresa   int `json:",string"`
	Config    int `json:",string"`
	Tipo      int `json:",string"`
	User      string
}

func (h *Handler) ReadMany(ctx context.Context, req ReadManyReq) (out []Esquema, err error) {
	if req.Comitente == 0 {
		return out, deferror.Validation("no se determinó ID de comitente")
	}

	out, err = readMany(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando esquemas")
	}
	if out == nil {
		out = []Esquema{}
	}
	return
}

func (h *Handler) Lookup(ctx context.Context, req ReadManyReq, user string) (out []Esquema, err error) {
	if req.Comitente == 0 {
		return out, deferror.Validation("no se determinó ID de comitente")
	}
	if user == "" {
		return out, deferror.Validation("no se determinó user")
	}
	req.User = user

	out, err = readMany(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando esquemas")
	}

	if out == nil {
		out = []Esquema{}
	}
	return
}

func (h *Handler) Delete(ctx context.Context, req ReadOneReq) (err error) {

	// Corroboro que no esté usado
	err = delete(ctx, h.conn, req, h.permisoDeleter)
	if err != nil {
		return deferror.DB(err, "borrando esquema")
	}

	return
}

func hashInt(req ReadOneReq) string {
	return fmt.Sprintf("%v/%v/%v", "FactEsquema", req.Comitente, req.ID)
}
