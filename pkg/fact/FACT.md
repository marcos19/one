# Fact

Puedo tener varias operaciones distintas dentro de un mismo proceso

- Solo factura
  - Queda la deuda de acuerdo a los vencimientos de la condición de pago => Facil
- Solo recibo

  - Aplicar a facturas anteriores
  - Quedar como anticipo => Agrupo vencimientos por fechas diferidas de valor => Facil
  - Ambos

- Recibo que aplique

## Recibos

Un recibo sin aplicación:

| Cuenta               | Debe | Haber | Vencimiento |
| -------------------- | ---: | ----: | ----------: |
| Caja                 |  100 |       |  01/01/2021 |
| a anticipos clientes |      |   100 |  01/01/2021 |

Un recibo sin aplicación con cheque:

| Cuenta               | Debe | Haber | Vencimiento |
| -------------------- | ---: | ----: | ----------: |
| Cheques en cartera   |  100 |       |  31/01/2021 |
| a anticipos clientes |      |   100 |  31/01/2021 |

Un recibo de sólo aplicaciones todos los movimientos van con fecha del recibo:

| Cuenta             | Debe | Haber | Vencimiento | Aplicacion |
| ------------------ | ---: | ----: | ----------: | ---------- |
| Anticipos clientes |  100 |       |  14/01/2021 | 1          |
| a Clientes         |      |   100 |  14/01/2021 | 2          |

Un recibo de que tenga aplicaciones y valores genera movimientos con diferentes
fechas de vencimiento.

| Cuenta             | Debe | Haber | Vencimiento | Aplicación |
| ------------------ | ---: | ----: | ----------: | ---------- |
| Anticipos clientes |  100 |       |  14/01/2021 | 1          |
| a Clientes         |      |   100 |  14/01/2021 | 2          |
| Cheques en cartera |   70 |       |  31/01/2021 |            |
| a Clientes         |      |    70 |  31/01/2021 | 2          |
