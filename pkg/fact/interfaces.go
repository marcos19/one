package fact

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/empresas"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"github.com/gofrs/uuid"
)

type LogoReader interface {
	ReadLogo(context.Context, empresas.ReadLogoReq) ([]byte, error)
}

// Busca en la base de datos el registro de la tabla 'fact' solamente.
type ReaderOne interface {
	ReadOne(context.Context, ReadOneReq) (types.Fact, error)
}

type ReadOneReq struct {
	ID        uuid.UUID
	Comitente int
}

type PDFReq struct {
	OpID      uuid.UUID
	Comitente int
}
