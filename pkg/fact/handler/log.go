package handler

import (
	"fmt"

	"bitbucket.org/marcos19/one/pkg/oplog"
	"bitbucket.org/marcos19/one/pkg/ops"
)

// Registro cuando se inserta una fact
func logCreate(oo ...*ops.Op) (rr []oplog.Registro) {
	for _, op := range oo {
		r := oplog.Registro{
			Comitente:    op.Comitente,
			CompID:       op.CompID,
			PuntoDeVenta: op.PuntoDeVenta,
			Persona:      op.Persona,
			OpID:         op.ID,
			NComp:        op.NComp,
			Usuario:      op.Usuario,
			Operacion:    oplog.OpCreate,
		}
		rr = append(rr, r)
	}
	return
}

// Registro cuando se anula una fact
func logAnulacion(usuario string, oo ...*ops.Op) (rr []oplog.Registro) {
	for _, op := range oo {
		r := oplog.Registro{
			Comitente:    op.Comitente,
			CompID:       op.CompID,
			PuntoDeVenta: op.PuntoDeVenta,
			Persona:      op.Persona,
			OpID:         op.ID,
			NComp:        op.NComp,
			Usuario:      usuario,
			Operacion:    oplog.OpCreate,
		}
		rr = append(rr, r)
	}
	return
}

// Registro cuando se borra una fact
func logDelete(usuario string, oo ...*ops.Op) (rr []oplog.Registro) {
	for _, op := range oo {
		r := oplog.Registro{
			Comitente:    op.Comitente,
			CompID:       op.CompID,
			PuntoDeVenta: op.PuntoDeVenta,
			NComp:        op.NComp,
			Persona:      op.Persona,
			OpID:         op.ID,
			Usuario:      usuario,
			Operacion:    oplog.OpDelete,
			Detalle:      fmt.Sprintf("%v por %v", op.PersonaNombre, op.TotalComp),
		}
		rr = append(rr, r)
	}
	return
}
