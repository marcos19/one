package handler

import (
	"testing"

	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestAutoAplicar(t *testing.T) {

	t.Run("Se aplican todas", func(t *testing.T) {
		id1 := uuid.FromStringOrNil("9a4916f4-07cd-4d1f-aaa4-38fefa4f5a03")
		id2 := uuid.FromStringOrNil("9a4916f4-07cd-4d1f-aaa4-38fefa4f5a04")
		pp := []ops.Partida{
			{
				ID:              id1,
				Pata:            types.PataFacturaPatrimonial,
				Cuenta:          100,
				Monto:           1000,
				PartidaAplicada: &id1,
			},
			{
				ID:              id2,
				Pata:            types.PataReciboPatrimonial,
				Cuenta:          800,
				Monto:           -1000,
				PartidaAplicada: &id2,
			},
		}
		ap, err := autoAplicar(pp)
		require.Nil(t, err)
		assert.Len(t, ap, 4)
		assert.Equal(t, dec.D2(0), sumCuenta(ap, 100))
		assert.Equal(t, dec.D2(0), sumCuenta(ap, 800))
	})

	t.Run("No se pueden aplicar", func(t *testing.T) {
		id1 := uuid.FromStringOrNil("9a4916f4-07cd-4d1f-aaa4-38fefa4f5a03")
		id2 := uuid.FromStringOrNil("9a4916f4-07cd-4d1f-aaa4-38fefa4f5a04")
		pp := []ops.Partida{
			{
				ID:              id1,
				Pata:            types.PataFacturaPatrimonial,
				Cuenta:          100,
				Monto:           1000,
				PartidaAplicada: &id1,
			},
			{
				ID:              id2,
				Pata:            types.PataReciboPatrimonial,
				Cuenta:          800,
				Monto:           -122,
				PartidaAplicada: &id2,
			},
		}
		_, err := autoAplicar(pp)
		require.NotNil(t, err)
	})
}
func TestValoresEnFactura(t *testing.T) {
	{ // Solo partidas de factura
		ff := []ops.Partida{
			{
				Pata:  types.PataFacturaPatrimonial,
				Monto: 100,
			},
		}
		rr := []ops.Partida{}
		assert.False(t, valoresVanEnFactura(ff, rr))
	}
	{ // Solo partidas de factura
		ff := []ops.Partida{}
		rr := []ops.Partida{
			{
				Pata:  types.PataReciboPatrimonial,
				Monto: -100,
			},
		}
		assert.False(t, valoresVanEnFactura(ff, rr))
	}
	{ // Contado
		ff := []ops.Partida{
			{
				Pata:  types.PataFacturaPatrimonial,
				Monto: 100,
			},
		}
		rr := []ops.Partida{
			{
				Pata:  types.PataReciboPatrimonial,
				Monto: -100,
			},
		}
		assert.True(t, valoresVanEnFactura(ff, rr))
	}

	{ // Pago parcial
		ff := []ops.Partida{
			{
				Pata:  types.PataFacturaPatrimonial,
				Monto: 50,
			},
		}
		rr := []ops.Partida{
			{
				Pata:  types.PataReciboPatrimonial,
				Monto: -100,
			},
		}
		assert.False(t, valoresVanEnFactura(ff, rr))
	}
}

func sumCuenta(pp []ops.Partida, cta int) (sum dec.D2) {

	for _, v := range pp {
		if v.Cuenta != cta {
			continue
		}
		sum += v.Monto
	}
	return sum
}
