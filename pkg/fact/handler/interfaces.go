package handler

import "bitbucket.org/marcos19/one/pkg/ops"

type OpHandler interface {
	ops.ReaderOne
	ops.Inserter
	ops.Deleter
	ops.ReaderMany
	ops.ReaderPartidas
}
