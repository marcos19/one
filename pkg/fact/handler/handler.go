package handler

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/afip"
	"bitbucket.org/marcos19/one/pkg/bus"
	"bitbucket.org/marcos19/one/pkg/cajas"
	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/condiciones"
	"bitbucket.org/marcos19/one/pkg/contables/limite"
	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/cuentasgrupos"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/depositos"
	"bitbucket.org/marcos19/one/pkg/emailsender"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"bitbucket.org/marcos19/one/pkg/fact"
	"bitbucket.org/marcos19/one/pkg/fact/config"
	"bitbucket.org/marcos19/one/pkg/fact/esquemas"
	"bitbucket.org/marcos19/one/pkg/fact/internal/db"
	"bitbucket.org/marcos19/one/pkg/fact/internal/diferimiento"
	"bitbucket.org/marcos19/one/pkg/fact/internal/email"
	"bitbucket.org/marcos19/one/pkg/fact/internal/email/comp"
	"bitbucket.org/marcos19/one/pkg/fact/internal/factura"
	"bitbucket.org/marcos19/one/pkg/fact/internal/lookups"
	"bitbucket.org/marcos19/one/pkg/fact/internal/opprod"
	"bitbucket.org/marcos19/one/pkg/fact/internal/pdfgen"
	"bitbucket.org/marcos19/one/pkg/fact/internal/recibo"
	"bitbucket.org/marcos19/one/pkg/fact/internal/reingreso"
	"bitbucket.org/marcos19/one/pkg/fact/internal/reingresopago"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/monedas"
	"bitbucket.org/marcos19/one/pkg/oplog"
	"bitbucket.org/marcos19/one/pkg/ops"
	opshandler "bitbucket.org/marcos19/one/pkg/ops/handler"
	"bitbucket.org/marcos19/one/pkg/personas"
	"bitbucket.org/marcos19/one/pkg/productos"
	"bitbucket.org/marcos19/one/pkg/productos/categorias"
	"bitbucket.org/marcos19/one/pkg/unicidades"
	"bitbucket.org/marcos19/one/pkg/unicidadesdef"
	"bitbucket.org/marcos19/one/pkg/usuarios"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/crosslogic/niler"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/rs/zerolog/log"
)

// Handler da acceso al a todos los routes de personas.
type Handler struct {
	conn *pgxpool.Pool

	afipHandler          *afip.Handler
	cajasStore           cajas.ReaderOne
	categoriasHandler    *categorias.Handler
	compHandler          comps.ReaderOne
	condicionesHandler   *condiciones.Handler
	configHandler        *config.Handler
	cuentasStore         cuentas.ReaderOne
	cuentasGrupos        cuentasgrupos.Getter
	depositosStore       depositos.Getter
	empresasHandler      empresas.ReaderOne
	logoReader           fact.LogoReader
	esquemasHandler      *esquemas.Handler
	monedaHandler        *monedas.Handler
	personasGetter       personas.ReaderOne
	productosGetter      *productos.Handler
	unicidadesReaderOne  unicidades.ReaderOne
	unicidadesCreater    unicidades.CreaterTx
	unicidadesdefHandler *unicidadesdef.Handler
	unicidadIDGetter     ops.UnicidadIDGetter
	sesionesHandler      usuarios.ReaderOne
	opsHandler           OpHandler

	Bus *bus.Bus
}

func NewHandler(

	conn *pgxpool.Pool,

	afipHandler *afip.Handler,
	cajasStore cajas.ReaderOne,
	categoriasHandler *categorias.Handler,
	compHandler comps.ReaderOne,
	condicionesHandler *condiciones.Handler,
	configHandler *config.Handler,
	cuentasStore cuentas.ReaderOne,
	cuentasgrupos cuentasgrupos.Getter,
	depositosStore depositos.Getter,
	empresasHandler empresas.ReaderOne,
	esquemasHandler *esquemas.Handler,
	logoReader fact.LogoReader,
	monedaHandler *monedas.Handler,
	personasGetter personas.ReaderOne,
	productosGetter *productos.Handler,
	unicidadesReader unicidades.ReaderOne,
	unicidadesCreater unicidades.CreaterTx,
	unicidadesdefHandler *unicidadesdef.Handler,
	unicidadIDGetter ops.UnicidadIDGetter,
	usuariosReader usuarios.ReaderOne,
	opsHandler *opshandler.Handler,
	Bus *bus.Bus,
) (h *Handler, err error) {

	// Valido

	if niler.IsNil(conn) {
		return h, errors.New("Conn era nil")
	}
	if niler.IsNil(afipHandler) {
		return h, errors.New("AFIP Handler era nil")
	}
	if niler.IsNil(cajasStore) {
		return h, errors.New("CAJAS Store era nil")
	}
	if niler.IsNil(categoriasHandler) {
		return h, errors.New("CATEGORIAS Handler era nil")
	}
	if niler.IsNil(compHandler) {
		return h, errors.New("COMPS Handler era nil")
	}
	if niler.IsNil(condicionesHandler) {
		return h, errors.New("CONDICIONES Store era nil")
	}
	if niler.IsNil(configHandler) {
		return h, errors.New("CONFIG Handler era nil")
	}
	if niler.IsNil(cuentasStore) {
		return h, errors.New("CUENTAS Store era nil")
	}
	if niler.IsNil(cuentasgrupos) {
		return h, errors.New("CUENTAS grupos era nil")
	}
	if niler.IsNil(depositosStore) {
		return h, errors.New("DEPOSITOS Store era nil")
	}
	if niler.IsNil(empresasHandler) {
		return h, errors.New("EMPRESAS Store era nil")
	}
	if niler.IsNil(esquemasHandler) {
		return h, errors.New("ESQUEMAS Handler era nil")
	}
	if niler.IsNil(logoReader) {
		return h, errors.New("LOGO reader era nil")
	}
	if niler.IsNil(monedaHandler) {
		return h, errors.New("MONEDAS Handler era nil")
	}
	if niler.IsNil(personasGetter) {
		return h, errors.New("PERSONAS Store era nil")
	}
	if niler.IsNil(productosGetter) {
		return h, errors.New("PRODUCTOS Store era nil")
	}
	if niler.IsNil(unicidadesReader) {
		return h, errors.New("UNICIDADES Store era nil")
	}
	if niler.IsNil(unicidadIDGetter) {
		return h, errors.New("UnicidadIDGetter era nil")
	}
	if niler.IsNil(usuariosReader) {
		return h, errors.New("USUARIOS Store era nil")
	}
	if niler.IsNil(opsHandler) {
		return h, errors.New("OpsHandler era nil")
	}
	// Inicio
	h = &Handler{}
	h.conn = conn
	h.afipHandler = afipHandler
	h.cajasStore = cajasStore
	h.compHandler = compHandler
	h.categoriasHandler = categoriasHandler
	h.condicionesHandler = condicionesHandler
	h.configHandler = configHandler
	h.cuentasStore = cuentasStore
	h.cuentasGrupos = cuentasgrupos
	h.depositosStore = depositosStore
	h.empresasHandler = empresasHandler
	h.esquemasHandler = esquemasHandler
	h.logoReader = logoReader
	h.monedaHandler = monedaHandler
	h.personasGetter = personasGetter
	h.productosGetter = productosGetter
	h.unicidadesCreater = unicidadesCreater
	h.unicidadesReaderOne = unicidadesReader
	h.unicidadesdefHandler = unicidadesdefHandler
	h.unicidadIDGetter = unicidadIDGetter
	h.sesionesHandler = usuariosReader
	h.opsHandler = opsHandler
	h.Bus = Bus
	return
}

func (h *Handler) Procesar(ctx context.Context, f *types.Fact, args CreateArgs) error {

	// Genero lookup
	lu, err := h.newLookup(ctx, f, args.Usuario)
	if err != nil {
		return err
	}

	// Valido
	if f.Persona.ID == uuid.Nil {
		return fact.ErrFaltaPersona
	}
	if f.ID == uuid.Nil {
		f.ID, _ = uuid.NewV1()
	}
	if len(f.Productos) > 0 && !lu.Config.PermiteProductos {
		return deferror.Validation("La configuración no permite que la operación tenga productos")
	}
	if len(f.PartidasDirectas) > 0 && !lu.Config.PermitePartidasDirectas {
		return deferror.Validation("La configuración no permite que la operación tenga partidas directas")
	}
	if len(f.Valores) > 0 && !lu.Config.PermiteValores {
		return deferror.Validation("La configuración no permite que la operación tenga valores")
	}

	// Factura
	err = factura.Procesar(ctx, f, lu, h.categoriasHandler, h.categoriasHandler, h.cuentasStore, h.productosGetter, h.conn, h.unicidadIDGetter)
	if err != nil {
		return errors.Wrap(err, "procesando factura")
	}

	// Partidas de recibo
	partidasRecibo, err := recibo.GenerarPartidas(ctx, f, lu, h.cuentasStore, h.unicidadIDGetter)
	if err != nil {
		return errors.Wrap(err, "generando partidas de recibo")
	}

	hayFactura := false
	if f.Factura != nil {
		hayFactura = true
	}

	valoresEnFactura := false
	if hayFactura {
		log.Debug().Bool("hayFactura", hayFactura).Msg("")
		valoresEnFactura = valoresVanEnFactura(f.Factura.PartidasContables, partidasRecibo)
		log.Debug().Bool("valoresEnFactura", valoresEnFactura).Msg("")
		if valoresEnFactura {
			f.Factura.PartidasContables = append(f.Factura.PartidasContables, partidasRecibo...)
			f.Factura.PartidasContables, err = autoAplicar(f.Factura.PartidasContables)
			if err != nil {
				return errors.Wrap(err, "autoaplicando")
			}
		}
	} else {
		log.Debug().Bool("hayFactura", hayFactura).Msg("")
	}

	// Recibo
	hayRecibo := len(partidasRecibo) > 0 && !valoresEnFactura
	if hayRecibo {
		log.Debug().Msg("Corresponde recibo")
		if hayFactura && !lu.Config.EmiteRecibo {
			return errors.Errorf("Factura no coincide con pago")
		}
		err = recibo.GenerarOp(f, partidasRecibo, lu)
		if err != nil {
			return errors.Wrap(err, "generando recibo")
		}
	} else {
		f.Recibo = nil
	}

	// ¿Hay reingreso?
	if lu.Config.ReingresoObligatorio && f.ReingresoMonto == nil {
		return deferror.Validation("El reingreso es obligatorio")
	}
	if lu.Config.GeneraReingreso && f.ReingresoMonto != nil {
		f.Reingreso, err = reingreso.GenerarOp(
			ctx,
			*f.Factura,
			*f.ReingresoMonto,
			lu.Config.ReingresoEmpresa,
			lu.Config.ReingresoCuentaIVA,
			lu.Config.ReingresoCuentaPatrimonial,
			lu.Config.ReingresoCuentaReingreso,
			lu.Config.SignoRenglonProducto,
			h.cuentasStore,
		)
		if err != nil {
			return errors.Wrap(err, "generando reingreso")
		}

		if lu.Config.ReingresoPagoObligatorio && f.ReingresoPagoCuenta == nil {
			return deferror.Validation("El pago del reingreso es obligatorio. No ingresó ninguna cuenta")
		}
		// ¿Hay pago de ese reingreso? => Pongo partidas
		if lu.Config.GeneraReingresoPago && f.ReingresoPagoCuenta != nil {
			req := reingresopago.Req{
				Cuenta:   *f.ReingresoPagoCuenta,
				Persona:  f.ReingresoPagoPersona,
				Sucursal: f.ReingresoPagoSucursal,
				// Centro:   f.ReingresoPagoCentro,
				Caja: f.ReingresoPagoCaja,
			}
			err = reingresopago.AgregarReingresosPagos(
				ctx,
				f.Comitente,
				f.Factura,
				f.Reingreso,
				req,
				h.cuentasStore,
			)
			if err != nil {
				return errors.Wrap(err, "generando reingreso")
			}
		}
	} else {
		f.Reingreso = nil
	}

	// ¿Hay diferimiento?
	if lu.Config.PermiteImputacionDiferida && f.ImputacionDesde != nil {
		c1, ok := lu.Esquema.Comprobantes["Diferimiento"]
		if !ok {
			return errors.Errorf("La configuración no define ningún comprobante 'Diferimiento'")
		}
		c2, ok := lu.Esquema.Comprobantes["Diferimiento (cancelación)"]
		if !ok {
			return errors.Errorf("La configuración no define ningún comprobante 'Diferimiento (cancelación)'")
		}
		cta := 0
		for _, v := range lu.Config.Imputaciones {
			if v.Categoria == 0 && v.Diferimiento != 0 {
				cta = v.Diferimiento
				break
			}
		}
		if cta == 0 {
			return errors.Errorf("La configuración no define cuenta patrimonial para realizar el diferimiento")
		}
		f.Diferimientos, err = diferimiento.CrearOp(ctx, f, cta, int(c1), int(c2))
		if err != nil {
			return errors.Wrap(err, "generando reingreso")
		}
	}

	// Esta tx la necesito para consultar, no registra nada
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return errors.Wrap(err, "iniciando transacción para procesar")
	}
	defer tx.Rollback(ctx)

	oo := f.Ops()

	// Valido
	err = h.opsHandler.Validate(ctx, tx, oo, nil)
	if err != nil {
		return errors.Wrap(err, "ops")
	}

	// Límite de crédito
	log.Debug().Msgf("Controla crédito %v", lu.Config.ControlaLimiteCredito)
	if lu.Config.ControlaLimiteCredito {
		if lu.Config.GrupoCuentasSaldo == nil {
			return fmt.Errorf("esta operación controla límite de crédito, pero no se ha definido grupo de cuentas de saldos")
		}
		limReq := limite.Req{
			Comitente:     f.Comitente,
			Persona:       f.Persona.ID,
			GruposCuentas: *lu.Config.GrupoCuentasSaldo,
			Ops:           oo,
			FechaAnalisis: f.Fecha,
		}
		res, err := limite.ControlarLimite(ctx, tx, h.personasGetter, h.cuentasGrupos, limReq)
		if err != nil {
			return fmt.Errorf("determinando límite de crédito: %w", err)
		}
		if !res.OK {
			return deferror.Validation(res.Msg)
		}
	}

	// Al frontend devuelvo la fact luego de procesada por ops.
	// Pongo cada op en su campo de acuerdo a CompItem. Diferimiento es un slice,
	// así que lo tengo que vaciar primero.
	f.Diferimientos = []ops.Op{}

	for _, v := range oo {
		switch *v.CompItem {
		case types.ItemFactura:
			f.Factura = v

		case types.ItemRecibo:
			f.Recibo = v

		case types.ItemReingreso:
			f.Reingreso = v

		case types.ItemDiferimiento, types.ItemDiferimientoCancelacion:
			f.Diferimientos = append(f.Diferimientos, *v)
		}
	}

	// ¿Hay nota de débito?

	// ¿Hay nota de crédito?

	return nil
}

func (h *Handler) newLookup(ctx context.Context, f *types.Fact, usuario string) (*lookups.Lookups, error) {

	l, err := lookups.New(ctx, h.conn, f, usuario,
		h.empresasHandler,
		h.sesionesHandler,
		h.configHandler,
		h.esquemasHandler,
		h.compHandler,
		h.afipHandler,
		h.personasGetter,
		h.condicionesHandler,
		h.cuentasStore,
		h.productosGetter,
		h.unicidadesdefHandler,
		h.categoriasHandler,
		h.opsHandler,
	)
	if err != nil {
		return nil, errors.Wrap(err, "trayendo lookups")
	}

	return &l, nil
}

type CreateArgs struct {
	OmitirReproceso      bool
	OmitirSaldos         bool
	OmitirCorregirFechas bool
	Usuario              string
}

// Insertar valida, genera el asiengo, inicia transacción e inserta
// en tablas fact, ops y unicidades.
func (h *Handler) Create(ctx context.Context, f *types.Fact, args ...CreateArgs) (err error) {

	arg := CreateArgs{}
	if len(args) == 1 {
		arg = args[0]
	}

	// Procesa?
	if !arg.OmitirReproceso {
		err = h.Procesar(ctx, f, arg)
		if err != nil {
			return errors.Wrap(err, "procesando operación")
		}
	}

	// Inicio transacción
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return deferror.DB(err, "iniciando la transacción")
	}
	defer tx.Rollback(ctx)

	// Inserto las unicidades nuevas
	log.Debug().Msg("persistiendo unicidades...")
	err = h.persistirUnicidades(ctx, tx, f)
	if err != nil {
		return errors.Wrap(err, "persistiendo unicidades nuevas")
	}

	// Inserto en tabla fact
	log.Debug().Msg("persistir...")
	err = db.Create(ctx, f, tx)
	if err != nil {
		return errors.Wrap(err, "persistiendo fact")
	}

	// Inserto tablas ops y partidas
	err = h.opsHandler.Insert(ctx, tx, f.Ops(), nil)
	if err != nil {
		return errors.Wrap(err, "ops")
	}

	// Inserto registro log
	log.Debug().Msg("insertando log...")
	rr := logCreate(f.Ops()...)
	err = oplog.Log(ctx, tx, rr...)
	if err != nil {
		return errors.Wrap(err, "logging")
	}

	// Confirmo transacción
	err = tx.Commit(ctx)
	if err != nil {
		return deferror.DB(err, "confirmando transacción")
	}

	return
}

func (h *Handler) ReadOne(ctx context.Context, req fact.ReadOneReq) (out types.Fact, err error) {

	// Valido
	if req.ID == uuid.Nil {
		return out, deferror.Validation("no se ingresó ID de fact")
	}
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó comitente")
	}

	// Busco fact
	out, err = db.ReadOne(ctx, h.conn, req.Comitente, req.ID)
	if err != nil {
		return out, deferror.DB(err, "buscando fact")
	}

	// Busco las ops
	oo, err := h.opsHandler.ReadMany(ctx, ops.ReadManyReq{TranID: req.ID, Comitente: req.Comitente})
	if err != nil {
		return out, deferror.DB(err, "buscando ops")
	}

	for _, v := range oo {

		// // Busco partidas contables
		// err = h.db.Find(&v.PartidasContables, "comitente = ? AND op_id = ?", req.Comitente, req.ID).Error
		// if err != nil {
		// 	return out, errors.Wrapf(err, "buscando partidas contables de %v %v", v.CompNombre, v.NCompStr)
		// }

		if v.CompItem == nil {
			continue
		}

		// // Para evitar los recibos por cuenta de terceros
		// if out.Factura != nil {
		// 	continue
		// }

		// Pego las ops a la fact
		switch *v.CompItem {

		case types.ItemFactura:
			out.Factura = new(ops.Op)
			*out.Factura = v

		case types.ItemRecibo:
			out.Recibo = new(ops.Op)
			*out.Recibo = v

		case types.ItemAnulacion:
			out.Anulacion = new(ops.Op)
			*out.Anulacion = v

		case types.ItemReingreso:
			out.Reingreso = new(ops.Op)
			*out.Reingreso = v

		case types.ItemDiferimiento:
			out.Diferimientos = append(out.Diferimientos, v)

		case types.ItemDiferimientoCancelacion:
			out.Diferimientos = append(out.Diferimientos, v)
		}
	}

	return
}

type DeleteReq struct {
	ID        uuid.UUID
	Comitente int
	Usuario   string
}

func (h *Handler) Delete(ctx context.Context, req DeleteReq) (err error) {

	// Valido
	if req.ID == uuid.Nil {
		return deferror.Validation("no se ingresó el ID de la fact a eliminar")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó el comitente")
	}
	if req.Usuario == "" {
		return deferror.Validation("no se determinó el usuario")
	}

	// Es factura electrónica autorizada?
	{
		query := `SELECT comp_id, cae FROM ops WHERE cae IS NOT NULL AND tran_id = $1 AND comitente = $2`
		rows, err := h.conn.Query(ctx, query, req.ID, req.Comitente)
		if err != nil {
			return deferror.DB(err, "buscando ops a borrar")
		}
		defer rows.Close()
		for rows.Next() {
			comp := 0
			cae := new(int64)
			err = rows.Scan(&comp, &cae)
			if err != nil {
				return errors.Wrap(err, "escaneando compID y CAE")
			}

			// Busco comp
			c, err := h.compHandler.ReadOne(ctx, comps.ReadOneReq{
				Comitente: req.Comitente,
				ID:        comp,
			})
			if err != nil {
				return errors.Wrap(err, "determinando si era comprobante electrónico")
			}
			if c.TipoEmision == comps.TipoEmisionWSFEV1 && cae != nil {
				return deferror.Validationf(
					"el comprobante %v (punto de venta %v) es electrónico y se encuentra autorizado. No se puede borrar.",
					c.Nombre, *c.PuntoDeVenta)
			}
		}
	}

	log.Debug().Msgf("entrando a Delete, %v", req)
	// Inicio transacción
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return deferror.DB(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)

	// Borro las unicidades nuevas
	idsUnicidades := []string{}
	{
		query := `
			SELECT valores
			FROM fact 
			WHERE 
				id = $1 AND comitente = $2
			;`
		rows, err := tx.Query(ctx, query, req.ID, req.Comitente)
		if err != nil {
			return deferror.DB(err, "buscando unicidades de fact")
		}
		defer rows.Close()

		for rows.Next() {
			pp := []types.PartidaValores{}
			err = rows.Scan(&pp)
			if err != nil {
				return deferror.DB(err, "escaneando rows de unicidades nuevas")
			}
			for _, v := range pp {
				if v.UnicidadNueva == nil {
					continue
				}
				idsUnicidades = append(idsUnicidades, "'"+v.UnicidadNueva.ID.String()+"'")
			}
		}

	}

	opsID := []uuid.UUID{}
	oo := []*ops.Op{} // Para el log

	{ // Busco los IDs de las ops que generó esta fact
		rows, err := tx.Query(ctx, "SELECT id, comp_id, punto_de_venta, n_comp, persona, total_comp, persona_nombre FROM ops WHERE tran_id = $1 AND comitente = $2;", req.ID, req.Comitente)
		if err != nil {
			return deferror.DB(err, "buscando ops de la transacción")
		}
		defer rows.Close()
		for rows.Next() {
			op := ops.Op{}
			err = rows.Scan(&op.ID, &op.CompID, &op.PuntoDeVenta, &op.NComp, &op.Persona, &op.TotalComp, &op.PersonaNombre)
			if err != nil {
				return deferror.DB(err, "escaneando ID de op")
			}
			opsID = append(opsID, op.ID)
			op.Comitente = req.Comitente
			oo = append(oo, &op)
		}
	}

	// TODO: hacer los controles antes de borrar

	// Inicio delete
	err = h.opsHandler.Delete(ctx, tx, req.Comitente, opsID)
	if err != nil {
		return errors.Wrap(err, "ops")
	}

	// Intento eliminar las unicidades. Si están usadas la base de datos debería
	// darme error
	if len(idsUnicidades) > 0 {
		del := fmt.Sprintf(`
				DELETE FROM unicidades
				WHERE id IN (%v)
				;`, strings.Join(idsUnicidades, ", "))
		_, err = tx.Exec(ctx, del)
		if err != nil {
			return deferror.DB(err, "eliminando las unicidades creadas por esta operación")
		}
	}

	// Borro la fact
	_, err = tx.Exec(ctx, "DELETE FROM fact WHERE id = $1 AND comitente = $2;", req.ID, req.Comitente)
	if err != nil {
		return deferror.DB(err, "borrando fact")
	}

	// Loggeo
	rr := logDelete(req.Usuario, oo...)
	err = oplog.Log(ctx, tx, rr...)
	if err != nil {
		return errors.Wrap(err, "logging")
	}

	// Confirmo transacción
	err = tx.Commit(ctx)
	if err != nil {
		return deferror.DB(err, "confirmando transacción")
	}

	return
}

// func (h *Handler)

func (h *Handler) PDF(ctx context.Context, req fact.PDFReq) (out pdfgen.Outputer, err error) {

	return pdfgen.PDF(ctx, req, h.opsHandler, h, h.configHandler, h.compHandler, h.logoReader, h.unicidadesReaderOne, h.monedaHandler)
}

func (h *Handler) persistirUnicidades(ctx context.Context, tx pgx.Tx, f *types.Fact) (err error) {

	uu := []unicidades.Unicidad{}

	// Agrego las unicidadesde fact (TODO, no se que va en este campo)
	// uu = append(uu, f.Unicidades...)

	// Agrego las unicidades de productos (por ejemplo recepción de obleas)
	for _, v := range f.Productos {
		uu = append(uu, v.UnicidadesNuevas...)
	}

	// Agrego las unicidades de productos (por ejemplo recepción de obleas)
	for _, v := range f.Valores {
		if v.UnicidadNueva != nil {
			uu = append(uu, *v.UnicidadNueva)
		}
	}

	log.Debug().Msg("Unicidades nuevas a persistir")

	for _, v := range uu {

		_, err = tx.Exec(ctx, `
				INSERT INTO unicidades
				(id, comitente, definicion, atts_indexados, atts, created_at) VALUES (
					$1, $2, $3, $4, $5, $6
				);
				`,
			v.ID, v.Comitente, v.Definicion, v.AttsIndexados, v.Atts, time.Now())
		if err != nil {
			return deferror.DB(err, "insertando unicidad")
		}
		log.Debug().Msgf("Insertada unicidad %v", v)
	}
	return
}

// func (h *Handler) actualizarNumerador(ctx context.Context, tx pgx.Tx, f *types.Fact) (err error) {
// 	proximos := map[int]int{}

// 	for _, v := range f.Ops() {

// 		// Si en una fact hay varias ops, las sucesivas van a sumar su numeración
// 		// no la va a buscar a la base para cada op.
// 		ultimo, ok := proximos[v.CompID]
// 		if ok {
// 			v.NComp = ultimo
// 			proximos[v.CompID] = ultimo + 1
// 			continue
// 		}

// 		// Me fijo con los datos del cache sí tiene numeración consecutiva
// 		req := comps.ReadOneReq{Comitente: f.Comitente, ID: v.CompID}
// 		comp, err := h.compHandler.ReadOne()
// 		if err != nil {
// 			return errors.Wrap(err, "buscando comp en cache")
// 		}
// 		if comp.TipoNumeracion == comps.Consecutiva {
// 			v.NComp, err = h.compHandler.AvanzarNumerador(ctx, tx, 1, v.CompID)
// 			if err != nil {
// 				return errors.Wrapf(err, "determinando número comp de %v; punto de venta: %v", v.CompNombre, v.PuntoDeVenta)
// 			}
// 			v.NCompStr, err = formatComp(v.PuntoDeVenta, v.NComp, "PPPPP-NNNNNNNN")
// 			if err != nil {
// 				return errors.Wrap(err, "creando string para ncomp")
// 			}
// 			// Grabo el próximo en el map
// 			proximos[v.CompID] = v.NComp + 1
// 		}
// 	}
// 	return
// }

type AnularReq struct {
	ID         uuid.UUID // de la op
	Comitente  int
	NuevaFecha fecha.Fecha
	Usuario    string
}

func (h *Handler) Anular(ctx context.Context, req AnularReq) (out uuid.UUID, err error) {

	// Valido
	if req.ID == uuid.Nil {
		return out, deferror.Validation("no se ingresó ID de operación a anular")
	}
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}
	if req.NuevaFecha == 0 {
		return out, deferror.Validation("no se ingresó la fecha para el nuevo comprobante")
	}

	// Busco la op
	op, err := h.opsHandler.ReadOne(ctx, ops.ReadOneReq{
		ID:        req.ID,
		Comitente: req.Comitente,
	})
	if err != nil {
		return out, err
	}

	// Busco la fact
	f, err := h.ReadOne(ctx, fact.ReadOneReq{
		ID:        op.TranID,
		Comitente: req.Comitente,
	})
	if err != nil {
		return out, errors.Wrap(err, "buscando fact")
	}

	// Busco esquema
	esq, err := h.esquemasHandler.ReadOne(ctx, esquemas.ReadOneReq{
		Comitente: req.Comitente,
		ID:        f.Esquema,
	})
	if err != nil {
		return out, errors.Wrap(err, "buscando esquema")
	}

	// ¿Es wsfev1? => Me aseguro que esté autorizado
	comp, err := h.compHandler.ReadOne(ctx, comps.ReadOneReq{
		Comitente: req.Comitente,
		ID:        op.CompID,
	})
	if err != nil {
		return out, errors.Wrap(err, "buscando comp de la operación original")
	}
	if comp.TipoEmision == comps.TipoEmisionWSFEV1 {
		if op.CAE == nil {
			return out, errors.Errorf("El comprobante que intenta anular es electrónico, y el mismo no ha sido autorizado aún. Autorícelo primero o elimínelo.")
		}
	}

	// Chequeo que no haya una anulación anterior
	{
		query := "SELECT id FROM ops WHERE tran_id = $1 AND comp_item = $2 AND comitente = $3"
		rows, err := h.conn.Query(ctx, query, op.TranID, types.ItemAnulacion, req.Comitente)
		if err != nil {
			return out, deferror.DB(err, "buscando si había anulación anterior")
		}
		defer rows.Close()
		count := 0
		for rows.Next() {
			count += 1
			id := uuid.UUID{}
			err = rows.Scan(&id)
			if err != nil {
				return out, deferror.DB(err, "escaneando row de anulación anterior")
			}
		}
		if count > 0 {
			return out, errors.Errorf("el comprobante que intenta anular ya se encontraba anulado")
		}
	}

	// ¿Está definido comprobante anulación?
	anul, ok := esq.Comprobantes[esquemas.AsociadoFacturaAnulacion]
	if !ok {
		return out, errors.Errorf("el esquema no tiene definido comprobante de anulación")
	}
	if anul == 0 {
		return out, errors.Errorf("el esquema no tiene definido comprobante de anulación")
	}

	// Creo nueva Op.
	// Uso la misma fact.
	newOp := op
	newOp.ID, err = uuid.NewV1()
	if err != nil {
		return out, errors.Wrap(err, "generando ID de fact")
	}
	newOp.Fecha = req.NuevaFecha
	newOp.FechaOriginal = newOp.Fecha
	newOp.CompItem = new(int)
	*newOp.CompItem = types.ItemAnulacion
	newOp.CAE = nil
	newOp.CAEVto = nil
	newOp.QR = nil
	newOp.BarCode = nil

	newOp.CompID = int(anul)
	newOp.Detalle = fmt.Sprintf("Este comprobante anula %v %v", op.CompNombre, op.NCompStr)

	// Traigo las partidas
	pp, err := h.opsHandler.Partidas(ctx, ops.PartidasReq{
		Comitente: req.Comitente,
		ID:        op.ID,
	})
	if err != nil {
		return out, errors.Wrap(err, "buscando partidas de op original")
	}

	// Las pego con el signo invertido
	for _, v := range pp {

		// Ignoro valores
		switch v.Pata {
		case types.PataValor, types.PataReciboPatrimonial:
			continue
		}
		v.ID, err = uuid.NewV1()
		if err != nil {
			return
		}
		v.OpID = newOp.ID
		v.Monto = -v.Monto
		if v.Cantidad != nil {
			*v.Cantidad = -*v.Cantidad
		}
		newOp.PartidasContables = append(newOp.PartidasContables, v)
	}

	// Dejo la referencia del comprobante que estoy anulando
	af := ops.AfipFields{}
	if op.NIdentificacion == nil {
		return out, errors.Errorf("la op no tiene definido número de identificación de persona")
	}
	if op.AFIPComprobanteID == nil {
		return out, errors.Errorf("la op no tiene definido tipo de comprobante")
	}
	af.CbtesAsoc = []ops.CbteAsoc{
		{
			Tipo:    *op.AFIPComprobanteID,
			PtoVta:  op.PuntoDeVenta,
			Nro:     op.NComp,
			CbteFch: op.Fecha.String(),
			Cuit:    fmt.Sprint(*op.NIdentificacion),
		},
	}

	nuevasOps := []*ops.Op{&newOp}

	// Inicio TX
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return out, errors.Wrap(err, "inciando transacción")
	}

	// Inserto
	err = h.opsHandler.Insert(ctx, tx, nuevasOps, nil)
	if err != nil {
		return out, errors.Wrap(err, "ops")
	}

	// Loggeo
	rr := logAnulacion(req.Usuario, nuevasOps...)
	err = oplog.Log(ctx, tx, rr...)
	if err != nil {
		return out, errors.Wrap(err, "logging")
	}

	// Confirmo
	err = tx.Commit(ctx)
	if err != nil {
		return out, errors.Wrap(err, "confirmando transacción")
	}

	return newOp.ID, nil
}

type DiferirReq struct {
	Comitente  int
	ID         uuid.UUID // TranID
	NuevaFecha fecha.Fecha
	Usuario    string
}

// Inserta ops de diferimiento con el mismo ID de tran de la fact.
func (h *Handler) Diferir(ctx context.Context, req DiferirReq) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó comitente")
	}
	if req.ID == uuid.Nil {
		return deferror.Validation("no se ingresó ID de op")
	}
	if req.NuevaFecha == 0 {
		return deferror.Validation("no se ingresó fecha")
	}

	// Busco fact
	f, err := h.ReadOne(ctx, fact.ReadOneReq{Comitente: req.Comitente, ID: req.ID})
	if err != nil {
		return errors.Wrap(err, "buscando fact")
	}

	// Busco partidas
	pp, err := h.opsHandler.Partidas(ctx, ops.PartidasReq{Comitente: f.Comitente, ID: f.Factura.ID})
	if err != nil {
		return errors.Wrap(err, "buscando partidas de fact")
	}
	f.Factura.PartidasContables = pp

	f.ImputacionDesde = new(fecha.Fecha)
	*f.ImputacionDesde = req.NuevaFecha

	// Genero lookup
	lu, err := h.newLookup(ctx, &f, req.Usuario)
	if err != nil {
		return errors.Wrap(err, "trayendo lookups")
	}

	c1, ok := lu.Esquema.Comprobantes["Diferimiento"]
	if !ok {
		return errors.Errorf("La configuración no define ningún comprobante 'Diferimiento'")
	}
	c2, ok := lu.Esquema.Comprobantes["Diferimiento (cancelación)"]
	if !ok {
		return errors.Errorf("La configuración no define ningún comprobante 'Diferimiento (cancelación)'")
	}

	cta := 0
	for _, v := range lu.Config.Imputaciones {
		if v.Categoria == 0 && v.Diferimiento != 0 {
			cta = v.Diferimiento
			break
		}
	}
	if cta == 0 {
		return errors.Errorf("La configuración no define cuenta patrimonial para realizar el diferimiento")
	}

	// Genero diferimientos
	oo, err := diferimiento.CrearOp(ctx, &f, cta, int(c1), int(c2))
	if err != nil {
		return errors.Wrap(err, "generando reingreso")
	}
	difs := []*ops.Op{}
	for i := range oo {
		oo[i].Usuario = req.Usuario
		difs = append(difs, &oo[i])
	}

	// Inicio transacción
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return deferror.DB(err, "iniciando la transacción")
	}
	defer tx.Rollback(ctx)

	// Le pego el campo a tabla fact
	_, err = h.conn.Exec(ctx, "UPDATE fact SET imputacion_desde=$3 WHERE comitente=$1 AND id=$2",
		req.Comitente, req.ID, req.NuevaFecha)
	if err != nil {
		return errors.Wrap(err, "modificando fact")
	}

	// Inserto tablas ops y partidas
	log.Debug().Msg("insertando tablas ops y partidas...")
	err = h.opsHandler.Insert(ctx, tx, difs, nil)
	if err != nil {
		return errors.Wrap(err, "insertando ops")
	}

	// Inserto registro log
	log.Debug().Msg("insertando log...")
	rr := logCreate(f.Ops()...)
	err = oplog.Log(ctx, tx, rr...)
	if err != nil {
		return errors.Wrap(err, "logging")
	}

	// Confirmo transacción
	err = tx.Commit(ctx)
	if err != nil {
		return deferror.DB(err, "confirmando transacción")
	}

	return
}

func (h *Handler) EnviarMail(ctx context.Context, comitente int, opsID []uuid.UUID) (err error) {

	// Busco los comprobantes
	dataGetter := comp.NewCompDataGet(h.conn, comitente, opsID)
	data, err := dataGetter.Get(ctx)
	if err != nil {
		return errors.Wrap(err, "determinando comprobantes")
	}

	// Para cada una genero PDF
	for i := range data {
		outputer, err := h.PDF(ctx, fact.PDFReq{
			Comitente: comitente,
			OpID:      data[i].ID,
		})
		if err != nil {
			return errors.Wrapf(err, "generando PDF de %v %v", data[i].CompNombre, data[i].NCompStr)
		}
		data[i].Outputer = outputer
		data[i].Extension = "pdf"
	}

	// Envío
	err = email.EnviarComprobantes(
		ctx,
		comitente,
		data,
		h.personasGetter,
		h.compHandler,
		h.empresasHandler,
		&emailsender.Sender{},
		true,
	)
	if err != nil {
		return errors.Wrap(err, "enviando mail")
	}

	return
}

func (h *Handler) PreviewMail(ctx context.Context, comitente int, opsID []uuid.UUID) (out emailsender.Req, err error) {

	// Busco los comprobantes
	dataGetter := comp.NewCompDataGet(h.conn, comitente, opsID)
	data, err := dataGetter.Get(ctx)
	if err != nil {
		return out, errors.Wrap(err, "determinando comprobantes")
	}

	// Para cada una genero PDF
	for i := range data {
		outputer, err := h.PDF(ctx, fact.PDFReq{
			Comitente: comitente,
			OpID:      data[i].ID,
		})
		if err != nil {
			return out, errors.Wrapf(err, "generando PDF de %v %v", data[i].CompNombre, data[i].NCompStr)
		}
		data[i].Outputer = outputer
		data[i].Extension = "pdf"
	}

	// Preview
	out, err = email.PreviewComprobantes(
		ctx,
		comitente,
		data,
		h.personasGetter,
		h.compHandler,
		h.empresasHandler,
		&emailsender.Sender{},
		true,
	)
	if err != nil {
		return out, errors.Wrap(err, "enviando mail")
	}

	return
}

type EnviarReq struct {
	Comitente int
	IDs       []uuid.UUID
	To        []string
	Subject   string
	BodyStr   string
}

func (h *Handler) PreviewMailConfirmar(ctx context.Context, req EnviarReq) (err error) {

	// Busco los comprobantes
	dataGetter := comp.NewCompDataGet(h.conn, req.Comitente, req.IDs)
	data, err := dataGetter.Get(ctx)
	if err != nil {
		return errors.Wrap(err, "determinando comprobantes")
	}

	// Para cada una genero PDF
	for i := range data {
		outputer, err := h.PDF(ctx, fact.PDFReq{
			Comitente: req.Comitente,
			OpID:      data[i].ID,
		})
		if err != nil {
			return errors.Wrapf(err, "generando PDF de %v %v", data[i].CompNombre, data[i].NCompStr)
		}
		data[i].Outputer = outputer
		data[i].Extension = "pdf"
	}

	reqq := email.EnviarReq{
		Comitente: req.Comitente,
		IDs:       req.IDs,
		To:        req.To,
		Subject:   req.Subject,
		BodyStr:   req.BodyStr,
		Data:      data,
	}

	// Envío
	err = email.PreviewComprobantesConfirmar(
		ctx,
		reqq,
		h.compHandler,
		h.empresasHandler,
		&emailsender.Sender{},
	)
	if err != nil {
		return errors.Wrap(err, "enviando mail")
	}

	return
}

func (h *Handler) OpprodConceptos() []opprod.Concepto {
	return opprod.Conceptos()
}

func (h *Handler) OpprodCuentas() []opprod.TipoOp {
	return opprod.TiposOps()
}

// Determina si es una operación de contado y no se hace recibo
func valoresVanEnFactura(partidasFactura []ops.Partida, partidasRecibo []ops.Partida) bool {

	habíaPatrimoniales := false
	habíaValores := false
	sum := dec.D2(0)

	for _, v := range append(partidasFactura, partidasRecibo...) {
		switch v.Pata {

		case types.PataFacturaPatrimonial:
			habíaPatrimoniales = true
			sum += v.Monto

		case types.PataReciboPatrimonial:
			habíaValores = true
			sum += v.Monto

		case types.PataAplicacion:
			sum += v.Monto
		}
	}

	if !(habíaValores && habíaPatrimoniales) {
		// No era una factura de contado
		return false
	}

	return sum == 0
}

// Si es una operación de contado, hace las partidas de aplicación entre factura y cobro.
func autoAplicar(pp []ops.Partida) (out []ops.Partida, err error) {
	aplis := []ops.Partida{}

	sum := dec.D2(0)
	for _, v := range pp {
		switch v.Pata {
		case types.PataFacturaPatrimonial, types.PataReciboPatrimonial:
			contra := v
			contra.ID, err = uuid.NewV1()
			if err != nil {
				return
			}
			contra.PartidaAplicada = new(uuid.UUID)
			*contra.PartidaAplicada = v.ID
			contra.Monto = -v.Monto
			contra.Detalle = new(string)
			*contra.Detalle = "Aplicación"
			contra.Pata = types.PataAplicacion
			out = append(out, v)
			aplis = append(aplis, contra)
			sum += v.Monto
		default:
			out = append(out, v)
		}
	}
	if sum != 0 {
		return out, errors.Errorf("no se cancelaron las partidas")
	}

	out = append(out, aplis...)
	return
}
