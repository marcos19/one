package permisos

import "time"

type Permiso struct {
	ID           int  `json:",string"`
	Comitente    int  `json:",string"`
	Empresa      int  `json:",string"`
	Config       int  `json:",string"`
	Esquema      *int `json:",string"`
	Usuario      *string
	GrupoUsuario *int `json:",string"`

	Create bool
	Read   bool
	Update bool
	Delete bool

	CreatedAt  time.Time
	UpdatedAt  *time.Time
	DiasFuturo bool
	DiasPasado bool
}

/*
CREATE TABLE fact_permisos (
id SERIAL PRIMARY KEY,
comitente INT NOT NULL REFERENCES comitentes,
empresa INT NOT NULL REFERENCES empresas,
config INT NOT NULL REFERENCES fact_config,
esquema INT,
usuario STRING,
grupo_usuario INT,
"create" BOOL,
"read" BOOL,
"update" BOOL,
"delete" BOOL,
created_at TIMESTAMP WITH TIME ZONE,
updated_at TIMESTAMP WITH TIME ZONE
);
*/
type Crud string

const (
	Create = Crud("create")
	Read   = Crud("read")
	Update = Crud("update")
	Delete = Crud("delete")
)

type ConfigsPermitidasReq struct {
	Comitente int
	Empresa   int
	Usuario   string
	Tipo      int
	Grupo     string
}
