package permisos

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/jackc/pgx/v4/pgxpool"
)

type PuedeReq struct {
	Comitente int `json:",string"`
	Usuario   string
	Config    int `json:",string"`
	Esquema   int `json:",string"`
	CRUD      Crud
}

func puede(ctx context.Context, conn *pgxpool.Pool, req PuedeReq) (out bool, err error) {
	if req.Comitente == 0 {
		return out, errors.Errorf("no se ingresó comitente")
	}
	if req.Usuario == "" {
		return out, errors.Errorf("no se ingresó usuario")
	}
	if req.Config == 0 {
		return out, errors.Errorf("no se ingresó config")
	}
	if req.Esquema == 0 {
		return out, errors.Errorf("no se ingresó esquema")
	}
	switch req.CRUD {
	case Create, Read, Update, Delete:
		// ok
	default:
		return out, errors.Errorf("el CRUD es inválido")
	}

	// Where clause
	ww := []string{"comitente=$1", "esquema=$2"}
	pp := []interface{}{req.Comitente, req.Esquema}

	// Usuario
	pp = append(pp, req.Usuario)
	ww = append(ww, fmt.Sprintf(`(
				usuario=$%v OR 
				(usuario IS NULL AND grupo_usuario IS NULL) OR 
				grupo_usuario IN (
					SELECT id
					FROM usuarios_grupos
					WHERE comitente=$%v AND usuarios @> ARRAY[$%v]
				)
			)`, len(pp), 1, len(pp)))

	where := "WHERE " + strings.Join(ww, " AND ")

	query := fmt.Sprintf(`SELECT "%v" FROM fact_permisos %v`, req.CRUD, where)

	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()
	for rows.Next() {
		crud := false
		err = rows.Scan(&crud)
		if err != nil {
			return out, deferror.DB(err, "escaneando")
		}
		if crud {
			return true, nil
		}
	}
	return
}

func create(
	ctx context.Context,
	conn *pgxpool.Pool,
	req Permiso,
	user string,
) (err error) {

	if req.Comitente == 0 {
		return errors.Errorf("no se definió comitente")
	}
	if req.Config == 0 {
		return errors.Errorf("no se definió config")
	}

	if req.Usuario != nil {
		if *req.Usuario == "" {
			req.Usuario = nil
		}
	}

	// Filters
	ww := []string{"comitente=$1", "config=$2"}
	pp := []interface{}{req.Comitente, req.Config}
	if req.Usuario != nil {
		pp = append(pp, *req.Usuario)
		ww = append(ww, fmt.Sprintf("usuario=$%v", len(pp)))
	} else {
		ww = append(ww, "usuario IS NULL")
	}
	if req.GrupoUsuario != nil {
		pp = append(pp, *req.GrupoUsuario)
		ww = append(ww, fmt.Sprintf("grupo_usuario=$%v", len(pp)))
	} else {
		ww = append(ww, "grupo_usuario IS NULL")
	}
	if req.Esquema != nil {
		pp = append(pp, *req.Esquema)
		ww = append(ww, fmt.Sprintf("esquema=$%v", len(pp)))
	} else {
		ww = append(ww, "esquema IS NULL")
	}
	where := "WHERE " + strings.Join(ww, " AND ")

	{ // Hay alguno con los mismas key?
		query := fmt.Sprintf(`SELECT id FROM fact_permisos %v`, where)
		rows, err2 := conn.Query(ctx, query, pp...)
		if err2 != nil {
			return errors.Wrap(err2, "querying permisos anteriores")
		}
		defer rows.Close()
		ids := []int{}
		for rows.Next() {
			id := 0
			err = rows.Scan(&id)
			if err != nil {
				return errors.Wrap(err, "scanning permiso anterior")
			}
			ids = append(ids, id)
		}
		if len(ids) == 1 {
			req.ID = ids[0]
			err = update(ctx, conn, req, user)
			if err != nil {
				return errors.Wrap(err, "modificando permiso anterior")
			}
			return
		}
	}

	// Inserto
	query := `INSERT INTO fact_permisos (
		comitente, empresa, config, esquema, usuario, grupo_usuario,
		"create", "read", "update", "delete", created_at) VALUES 
		($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)`
	_, err = conn.Exec(ctx, query, req.Comitente, req.Empresa,
		req.Config, req.Esquema, req.Usuario, req.GrupoUsuario,
		req.Create, req.Read, req.Update, req.Delete, time.Now())
	if err != nil {
		return deferror.DB(err, "insertando")
	}

	return
}

func update(
	ctx context.Context,
	conn *pgxpool.Pool,
	req Permiso,
	user string,
) (err error) {

	if req.Comitente == 0 {
		return errors.Errorf("no se definió comitente")
	}
	if req.Config == 0 {
		return errors.Errorf("no se definió config")
	}

	// Inserto
	query := `
	UPDATE fact_permisos 
	SET usuario=$3, grupo_usuario=$4, "create"=$5, "read"=$6, "update"=$7, "delete"=$8, updated_at=$9  
	WHERE comitente=$1 AND id=$2`
	res, err := conn.Exec(ctx, query, req.Comitente, req.ID,
		req.Usuario, req.GrupoUsuario,
		req.Create, req.Read, req.Update, req.Delete, req.UpdatedAt)
	if err != nil {
		return deferror.DB(err, "modificando")
	}
	if res.RowsAffected() == 0 {
		return errors.New("no se modificó ningún registro")
	}

	return
}

func delete(
	ctx context.Context,
	conn *pgxpool.Pool,
	req Permiso,
	user string,
) (err error) {

	if req.Comitente == 0 {
		return errors.Errorf("no se definió comitente")
	}
	if req.ID == 0 {
		return errors.Errorf("no se definió ID")
	}

	// Borro
	query := `DELETE FROM fact_permisos WHERE comitente=$1 AND id=$2`
	res, err := conn.Exec(ctx, query, req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "borrando")
	}
	if res.RowsAffected() == 0 {
		return errors.New("no se borró ningún registro")
	}

	return
}

type PorConfigReq struct {
	Comitente int `json:",string"`
	ID        int `json:",string"`
}

func PorConfig(ctx context.Context, conn *pgxpool.Pool, req PorConfigReq) (out []Permiso, err error) {
	out = []Permiso{}

	query := `SELECT id, comitente, empresa, config, esquema, usuario,
	grupo_usuario, "create", "read", "update", "delete", created_at, updated_at
	FROM fact_permisos
	WHERE comitente=$1 AND config=$2 AND esquema IS NULL
	ORDER BY created_at`

	rows, err := conn.Query(ctx, query, req.Comitente, req.ID)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		v := Permiso{}
		err = rows.Scan(&v.ID, &v.Comitente, &v.Empresa, &v.Config, &v.Esquema, &v.Usuario,
			&v.GrupoUsuario, &v.Create, &v.Read, &v.Update, &v.Delete, &v.CreatedAt, &v.UpdatedAt)
		if err != nil {
			return out, deferror.DB(err, "scanning")
		}
		out = append(out, v)
	}

	return
}

type PorEsquemaReq struct {
	Comitente int `json:",string"`
	ID        int `json:",string"`
}

func PorEsquema(ctx context.Context, conn *pgxpool.Pool, req PorEsquemaReq) (out []Permiso, err error) {
	out = []Permiso{}

	query := `SELECT id, comitente, empresa, config, esquema, usuario,
	grupo_usuario, "create", "read", "update", "delete", created_at, updated_at
	FROM fact_permisos
	WHERE comitente=$1 AND esquema=$2
	ORDER BY created_at`

	rows, err := conn.Query(ctx, query, req.Comitente, req.ID)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		v := Permiso{}
		err = rows.Scan(&v.ID, &v.Comitente, &v.Empresa, &v.Config, &v.Esquema, &v.Usuario,
			&v.GrupoUsuario, &v.Create, &v.Read, &v.Update, &v.Delete, &v.CreatedAt, &v.UpdatedAt)
		if err != nil {
			return out, deferror.DB(err, "scanning")
		}
		out = append(out, v)
	}

	return
}
