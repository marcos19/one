package permisos

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/usuarios"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/niler"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Handler struct {
	conn *pgxpool.Pool
}

func NewHandler(conn *pgxpool.Pool, ses usuarios.ReaderOne) (h *Handler, err error) {
	if conn == nil {
		return h, errors.New("Conn no puede ser nil")
	}
	if niler.IsNil(ses) {
		return h, errors.New("Sesiones handler no puede ser nil")
	}

	h = &Handler{
		conn: conn,
	}

	return
}

func Puede(ctx context.Context, conn *pgxpool.Pool, req PuedeReq) (out bool, err error) {
	return puede(ctx, conn, req)
}

func (h *Handler) Create(ctx context.Context, req Permiso, user string) (err error) {
	err = create(ctx, h.conn, req, user)
	if err != nil {
		return errors.Wrap(err, "creando nuevo permiso")
	}
	return
}

func (h *Handler) Update(ctx context.Context, req Permiso, user string) (err error) {
	err = update(ctx, h.conn, req, user)
	if err != nil {
		return errors.Wrap(err, "modificando permiso")
	}
	return
}

func (h *Handler) Delete(ctx context.Context, req Permiso, user string) (err error) {
	err = delete(ctx, h.conn, req, user)
	if err != nil {
		return errors.Wrap(err, "borrando permiso")
	}
	return
}

// Borra los permisos de una config usando la Tx
func (h *Handler) DeletePermisosDeConfig(ctx context.Context, tx pgx.Tx, comitente, configID int) (err error) {
	if configID == 0 {
		return errors.Errorf("no se definió comitente")
	}
	if comitente == 0 {
		return errors.Errorf("no se definió ID")
	}

	// Borro
	query := `DELETE FROM fact_permisos WHERE comitente=$1 AND config=$2 AND esquema IS NULL`
	res, err := tx.Exec(ctx, query, comitente, configID)
	if err != nil {
		return deferror.DB(err, "borrando")
	}
	if res.RowsAffected() == 0 {
		return errors.New("no se borró ningún registro")
	}

	return
}

// Borra los permisos de un esquema usando la Tx
func (h *Handler) DeletePermisosDeEsquema(ctx context.Context, tx pgx.Tx, comitente, configID int) (err error) {
	if configID == 0 {
		return errors.Errorf("no se definió comitente")
	}
	if comitente == 0 {
		return errors.Errorf("no se definió ID")
	}

	// Borro
	query := `DELETE FROM fact_permisos WHERE comitente=$1 AND esquema=$2`
	res, err := tx.Exec(ctx, query, comitente, configID)
	if err != nil {
		return deferror.DB(err, "borrando")
	}
	if res.RowsAffected() == 0 {
		return errors.New("no se borró ningún registro")
	}

	return
}

func (h *Handler) PorConfig(ctx context.Context, req PorConfigReq) (out []Permiso, err error) {
	out, err = PorConfig(ctx, h.conn, req)
	if err != nil {
		return out, errors.Wrap(err, "buscando permisos de config")
	}
	return
}

func (h *Handler) PorEsquema(ctx context.Context, req PorEsquemaReq) (out []Permiso, err error) {
	out, err = PorEsquema(ctx, h.conn, req)
	if err != nil {
		return out, errors.Wrap(err, "buscando permisos de config")
	}
	return
}
