package fact

//
type Bus interface {
	Emit(e interface{})
}

type EventMailSending struct {
	To string
}

type EventMailFailed struct {
	To    string
	Error string
}
type EventMailSent struct {
	To string
}
