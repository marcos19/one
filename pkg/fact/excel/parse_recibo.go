package excel

import (
	"io"
	"strconv"
	"strings"

	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/xuri/excelize/v2"
)

type Reg struct {
	Fecha              fecha.Fecha
	Monto              dec.D2
	Cuenta             int
	Centro             *int
	Persona            *uuid.UUID
	CuentaPago         int
	CuentaPagoPersona  *uuid.UUID
	CuentaPagoSucursal *int
	CuentaPagoCaja     *int
}

func ParseRecibos(reader io.Reader, hoja string) (out []Reg, err error) {
	// Leo
	h := handler{}
	f, err := excelize.OpenReader(reader)
	if err != nil {
		return out, errors.Wrap(err, "creando excelize.File")
	}

	// Inicio lookups
	err = h.iniciarLookups(f)
	if err != nil {
		return nil, errors.Wrap(err, "leyendo lookups")
	}

	// Leo rows
	rows, err := f.GetRows(hoja)
	if err != nil {
		return out, errors.Wrap(err, "leyendo rows")
	}

	// Map de columnas
	if len(rows) == 0 {
		return out, errors.Errorf("no había datos en la hoja %v", hoja)
	}
	cols := map[string]int{}
	for i, v := range rows[0] {
		cols[v] = i
	}

	// Controlo que estén todas
	columnas := []string{
		"Fecha",
		"Monto",
		"Cuenta",
		"Centro",
		"Persona",
		"Medio de pago",
		"Medio de pago – Persona",
		"Medio de pago – Sucursal",
		"Medio de pago – Caja",
	}
	for _, v := range columnas {
		_, ok := cols[v]
		if !ok {
			return out, errors.Errorf("no se encontró la columna '%v'", v)
		}
	}

	for i, row := range rows {
		if i == 0 {
			continue
		}
		if len(row) == 0 {
			continue
		}
		if len(row) < 3 {
			return out, errors.Errorf("los campos Fecha, Monto y Cuenta son obligatorios")
		}
		reg := Reg{}

		// Fecha
		reg.Fecha, err = fch(row[cols["Fecha"]])
		if err != nil {
			return nil, errors.Wrapf(err, "leyendo fecha en fila %v", i+1)
		}

		// Monto
		reg.Monto, err = d(row[cols["Monto"]])
		if err != nil {
			return nil, errors.Wrapf(err, "leyendo monto en fila %v", i+1)
		}

		// Cuenta
		reg.Cuenta, err = h.lookupCuenta(row[cols["Cuenta"]])
		if err != nil {
			return nil, errors.Wrapf(err, "leyendo cuenta en fila %v", i+1)
		}

		// Centro
		if row[cols["Centro"]] != "" {
			reg.Centro = new(int)
			*reg.Centro, err = strconv.Atoi(row[cols["Centro"]])
			if err != nil {
				return nil, errors.Wrapf(err, "leyendo centro en fila %v", i+1)
			}
		}

		// Persona
		if cols["Persona"]+1 <= len(row) {
			pers, err := h.lookupPersona(row[cols["Persona"]])
			if err != nil {
				return nil, errors.Wrapf(err, "leyendo persona en fila %v", i+1)
			}
			if pers != uuid.Nil {
				reg.Persona = new(uuid.UUID)
				*reg.Persona = pers
			}
		}

		// Cuenta
		if cols["Medio de pago"]+1 <= len(row) {
			reg.CuentaPago, err = h.lookupCuenta(row[cols["Medio de pago"]])
			if err != nil {
				return nil, errors.Wrapf(err, "leyendo medio de pago en fila %v", i+1)
			}
		}

		// Persona de medio de pago
		if cols["Medio de pago – Persona"]+1 <= len(row) {
			medPersona, err := h.lookupPersona(row[cols["Medio de pago – Persona"]])
			if err != nil {
				return nil, errors.Wrapf(err, "leyendo persona de medio de pago en fila %v", i+1)
			}
			if medPersona != uuid.Nil {
				reg.CuentaPagoPersona = new(uuid.UUID)
				*reg.CuentaPagoPersona = medPersona
			}
		}

		// Sucursal de medio de pago
		if cols["Medio de pago – Sucursal"]+1 <= len(row) {
			medSuc, err := h.lookupSucursal(row[cols["Medio de pago – Sucursal"]])
			if err != nil {
				return nil, errors.Wrapf(err, "leyendo sucursal de medio de pago en fila %v", i+1)
			}
			if medSuc != 0 {
				reg.CuentaPagoSucursal = new(int)
				*reg.CuentaPagoSucursal = medSuc
			}
		}

		// Sucursal de medio de pago
		if cols["Medio de pago – Caja"]+1 <= len(row) {
			caja, err := h.lookupCaja(row[cols["Medio de pago – Caja"]])
			if err != nil {
				return nil, errors.Wrapf(err, "leyendo caja de medio de pago en fila %v", i+1)
			}
			if caja != 0 {
				reg.CuentaPagoCaja = new(int)
				*reg.CuentaPagoCaja = caja
			}
		}
		out = append(out, reg)
	}

	return
}

type handler struct {
	cuentas    map[string]int
	personas   map[string]uuid.UUID
	centros    map[string]int
	sucursales map[string]int
	cajas      map[string]int
}

func (r *handler) lookupPersona(nombre string) (id uuid.UUID, err error) {

	if nombre == "" {
		return
	}
	if r.personas == nil {
		return id, errors.New("no se inició personas map")
	}

	id, ok := r.personas[nombre]
	if !ok {
		return id, errors.Errorf("no se definió ID de persona (nombre '%v')", nombre)
	}

	return
}

func (r *handler) lookupCaja(nombre string) (id int, err error) {

	if nombre == "" {
		return
	}
	if r.cajas == nil {
		return id, errors.New("no se inició cajas map")
	}

	id, ok := r.cajas[nombre]
	if !ok {
		return id, errors.Errorf("no se definió ID de caja(nombre '%v')", nombre)
	}

	return
}

func (r *handler) lookupSucursal(nombre string) (id int, err error) {

	if nombre == "" {
		return
	}
	if r.sucursales == nil {
		return id, errors.New("no se inició cuentas map")
	}

	id, ok := r.sucursales[nombre]
	if !ok {
		return id, errors.Errorf("no se definió ID de sucursal (nombre '%v')", nombre)
	}

	return
}

func (r *handler) lookupCuenta(nombre string) (id int, err error) {

	if nombre == "" {
		return
	}
	if r.cuentas == nil {
		return id, errors.New("no se inició cuentas map")
	}

	id, ok := r.cuentas[nombre]
	if !ok {
		return id, errors.Errorf("no se definió ID de cuenta (nombre %v)", nombre)
	}

	return
}

func (h *handler) iniciarLookups(xl *excelize.File) (err error) {
	{
		hoja := "Cuentas"
		rows, err := xl.GetRows(hoja)
		if err != nil {
			return errors.Wrapf(err, "leyendo rows de %v", hoja)
		}
		h.cuentas = map[string]int{}
		for i, row := range rows {
			if i == 0 {
				continue
			}
			if len(rows) == 0 {
				break
			}
			if row[0] == "" {
				break
			}
			if len(row) < 2 {
				return errors.Errorf("se esperaban 2 columnas en hoja %v fila %v, había %v", hoja, i+1, len(row))
			}
			h.cuentas[row[0]], err = strconv.Atoi(row[1])
			if err != nil {
				return errors.Wrapf(err, "convirtiendo a número cuenta %v", row[0])
			}
		}
	}
	{
		hoja := "Centros"
		rows, err := xl.GetRows(hoja)
		if err != nil {
			return errors.Wrapf(err, "leyendo rows de %v", hoja)
		}
		h.centros = map[string]int{}
		for i, row := range rows {
			if i == 0 {
				continue
			}
			if len(rows) == 0 {
				break
			}
			if row[0] == "" {
				break
			}
			if len(row) < 2 {
				return errors.Errorf("se esperaban 2 filas en hoja %v fila %v", hoja, i+1)
			}
			h.centros[row[0]], err = strconv.Atoi(row[1])
			if err != nil {
				return errors.Wrapf(err, "convirtiendo a número centro %v", row[0])
			}
		}
	}

	{
		hoja := "Personas"
		rows, err := xl.GetRows(hoja)
		if err != nil {
			return errors.Wrapf(err, "leyendo rows de %v", hoja)
		}
		h.personas = map[string]uuid.UUID{}
		for i, row := range rows {
			if i == 0 {
				continue
			}
			if len(rows) == 0 {
				break
			}
			if row[0] == "" {
				break
			}
			if len(row) < 2 {
				return errors.Errorf("se esperaban 2 filas en hoja %v fila %v", hoja, i+1)
			}
			h.personas[row[0]], err = uuid.FromString(strings.Trim(row[1], " "))
			if err != nil {
				return errors.Wrapf(err, "convirtiendo a UUID persona %v", row[0])
			}
		}
	}
	{
		hoja := "Sucursales"
		rows, err := xl.GetRows(hoja)
		if err != nil {
			return errors.Wrapf(err, "leyendo rows de %v", hoja)
		}
		h.sucursales = map[string]int{}
		for i, row := range rows {
			if i == 0 {
				continue
			}
			if len(rows) == 0 {
				break
			}
			if row[0] == "" {
				break
			}
			if len(row) < 2 {
				return errors.Errorf("se esperaban 2 filas en hoja %v fila %v", hoja, i+1)
			}
			h.sucursales[row[0]], err = strconv.Atoi(row[1])
			if err != nil {
				return errors.Wrapf(err, "convirtiendo a número sucursal %v", row[0])
			}
		}
	}
	{
		hoja := "Cajas"
		rows, err := xl.GetRows(hoja)
		if err != nil {
			return errors.Wrapf(err, "leyendo rows de %v", hoja)
		}
		h.cajas = map[string]int{}
		for i, row := range rows {
			if i == 0 {
				continue
			}
			if len(rows) == 0 {
				break
			}
			if row[0] == "" {
				break
			}
			if len(row) < 2 {
				return errors.Errorf("se esperaban 2 filas en hoja %v fila %v", hoja, i+1)
			}
			h.cajas[row[0]], err = strconv.Atoi(row[1])
			if err != nil {
				return errors.Wrapf(err, "convirtiendo a número caja %v", row[0])
			}
		}
	}
	return
}

func fch(str string) (f fecha.Fecha, err error) {

	f, err = fecha.NewFechaFromLayout("02/01/06", str)
	if err != nil {
		return f, errors.Wrap(err, "parseando a time")
	}

	return
}

func d(str string) (out dec.D2, err error) {
	if str == "" {
		return
	}
	fl, err := strconv.ParseFloat(str, 64)
	if err != nil {
		return
	}

	return dec.NewD2(fl), err
}
