package excel

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParse(t *testing.T) {
	assert := assert.New(t)
	f, err := os.Open("./testdata/pagos.xlsx")
	assert.Nil(err)

	rr, err := ParseRecibos(f, "Pagos")
	assert.Nil(err)

	assert.Len(rr, 13)
}
