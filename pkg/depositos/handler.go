package depositos

import (
	"context"
	"errors"
	"fmt"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/dgraph-io/ristretto"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler da acceso al a todos los routes de personas.
type Handler struct {
	conn  *pgxpool.Pool
	cache *ristretto.Cache
}

// NewHandler inicializa un cache y devuelve el puntero a este.
func NewHandler(conn *pgxpool.Pool, cache *ristretto.Cache) (h *Handler) {

	h = &Handler{
		conn:  conn,
		cache: cache,
	}
	return
}

type Getter interface {
	ReadOne(context.Context, ReadOneReq) (Deposito, error)
}

func (h *Handler) ReadMany(ctx context.Context, comitente int) (out []Deposito, err error) {

	query := `SELECT id, comitente, nombre, disponible_para_venta, acepta_stock_negativo, valido_desde, valido_hasta, created_at, updated_at
	FROM depositos WHERE comitente=$1`
	// Busco
	rows, err := h.conn.Query(ctx, query, comitente)
	if err != nil {
		return out, deferror.DB(err, "buscando depositos")
	}
	defer rows.Close()

	for rows.Next() {
		v := Deposito{}
		err = rows.Scan(&v.ID, &v.Comitente, &v.Nombre, &v.DisponibleParaVenta, &v.AceptaStockNegativo,
			&v.ValidoDesde, &v.ValidoHasta, &v.CreatedAt, &v.UpdatedAt)
		if err != nil {
			return out, deferror.DB(err, "escaneando")
		}
		out = append(out, v)
	}
	if out == nil {
		out = []Deposito{}
	}
	return
}

type ReadOneReq struct {
	ID        int `json:",string"`
	Comitente int
}

// HandleOne devuelve todos los depositos para el comitente.
func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (v Deposito, err error) {

	// Busco en cache
	key := hashInt(req)
	val, estaba := h.cache.Get(key)
	if estaba {
		out, ok := val.(Deposito)
		if ok && out.Comitente == req.Comitente {
			return out, nil
		}
	}

	// Busco en DB
	query := `SELECT id, comitente, nombre, disponible_para_venta, acepta_stock_negativo, valido_desde, valido_hasta, created_at, updated_at
	FROM depositos WHERE comitente=$1 AND id=$2`
	// Busco
	err = h.conn.
		QueryRow(ctx, query, req.Comitente, req.ID).
		Scan(&v.ID, &v.Comitente, &v.Nombre, &v.DisponibleParaVenta, &v.AceptaStockNegativo,
			&v.ValidoDesde, &v.ValidoHasta, &v.CreatedAt, &v.UpdatedAt,
		)
	if err != nil {
		return v, deferror.DB(err, "buscando depósito por ID")
	}

	// Lo agrego al cache
	h.cache.Set(key, v, 0)

	return
}

// HandleNuevo devuelve todos los depositos para el Comitente.
func (h *Handler) Create(ctx context.Context, req Deposito) (err error) {

	// Valido
	if req.Nombre == "" {
		return deferror.Validation("El nombre del depósito no puede estar en blanco")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no se determinó ID de comitente")
	}

	query := `INSERT INTO depositos (comitente, nombre, disponible_para_venta, acepta_stock_negativo, valido_desde, valido_hasta, created_at, updated_at)
	VALUES ($1,$2,$3,$4,$5,$6,$7,$8)`
	// Inserto
	_, err = h.conn.Exec(ctx, query, req.Comitente, req.Nombre, req.DisponibleParaVenta,
		req.AceptaStockNegativo, req.ValidoDesde, req.ValidoHasta, req.CreatedAt, req.UpdatedAt,
	)
	if err != nil {
		return deferror.DB(err, "persistiendo depósito en base de datos")
	}

	return
}

// HandleModificar persiste las modificaciones a un depósito
func (h *Handler) Update(ctx context.Context, req Deposito) (err error) {

	// Valido
	if req.Nombre == "" {
		return deferror.Validation("El nombre del depósito no puede estar en blanco")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no se definió comitente")
	}

	// Invalido cache
	key := hashInt(ReadOneReq{Comitente: req.Comitente, ID: req.ID})
	h.cache.Del(key)

	query := `UPDATE depositos SET (nombre=$3, disponible_para_venta=$4, acepta_stock_negativo=$5, valido_desde=$6, valido_hasta=$7, updated_at=$8)
		WHERE comitente=$1 AND id=$2
	`

	// Modifico
	res, err := h.conn.Exec(ctx, query, req.Comitente, req.ID, req.Nombre, req.DisponibleParaVenta,
		req.AceptaStockNegativo, req.ValidoDesde, req.ValidoHasta, req.UpdatedAt)
	if err != nil {
		return deferror.DB(err, "modificando depósito")
	}

	if res.RowsAffected() == 0 {
		return errors.New("no se modificó ningún registro")
	}

	return
}

func hashInt(req ReadOneReq) string {
	return fmt.Sprintf("%v/%v/%v", "Deposito", req.Comitente, req.ID)
}
