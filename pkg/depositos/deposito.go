package depositos

import (
	"time"

	"github.com/crosslogic/fecha"
)

// Deposito representa un deposito dentro de la empresa.
type Deposito struct {
	ID                  int `json:",string"`
	Comitente           int `json:",string"`
	Nombre              string
	DisponibleParaVenta bool
	AceptaStockNegativo bool
	ValidoDesde         *fecha.Fecha
	ValidoHasta         *fecha.Fecha
	CreatedAt           *time.Time
	UpdatedAt           *time.Time
}

// TableName es el nombre de la tabla en la base de datos.
func (u Deposito) TableName() string {
	return "depositos"
}

// // IsValid analiza si se puede usar el depósito en la fecha ingresada.
// func (u *Deposito) IsValid(fechaAnalisis fecha.Fecha) bool {
// 	if fechaAnalisis > u.ValidoHasta && u.ValidoHasta.IsValid() {
// 		return false
// 	}
// 	if fechaAnalisis < u.ValidoDesde && u.ValidoDesde.IsValid() {
// 		return false
// 	}
// 	return true
// }
