package cruds

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"strings"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler contiene el estado.
type Handler struct {
	conn *pgxpool.Pool
}

func NewHandler(conn *pgxpool.Pool) (h *Handler) {

	h = &Handler{}
	h.conn = conn
	return
}

// CrudRequest contiene los parámetros que determinan si el usuario
// puede llevar a cabo la operación
type Request struct {
	Usuario   string
	Comitente int
	Programa  Programa
	Crud      Crud
}

func (h *Handler) AuthMiddleware(ctx context.Context, r Request, then http.HandlerFunc) http.HandlerFunc {
	err := h.Autorizar(ctx, r)
	switch {
	case err == nil:
		return then
	default:
		// return deferror.Middleware(err)
		// TODO corregir esto
		return then
	}
}

func (h *Handler) Autorizar(ctx context.Context, r Request) (err error) {

	if r.Comitente == 0 {
		return errors.Errorf("no se ingresó ID de comitente")
	}
	if r.Programa == "" {
		return errors.Errorf("no se ingresó ID de programa")
	}
	// Es un usuario administrador?
	esAdmin, err := h.EsAdministrador(ctx, r.Usuario)
	if err != nil {
		return errors.Wrap(err, "determinando si usuario era administrador")
	}

	// Es admin, está permitido
	if esAdmin {
		return
	}

	if !r.Crud.Valid() {
		return errors.Errorf("'%v' no es una operación CRUD válida", r.Crud)
	}

	// ¿Pertenece a algún grupo?
	grupos, err := h.gruposPersonas(ctx, r.Usuario)
	if err != nil {
		return errors.Wrap(err, "al determinar grupos de usuarios")
	}

	ww := []string{"comitente=$1", "programa=$2"}
	pp := []interface{}{r.Comitente, r.Programa}

	// Filtro si se otorgó para un grupos de usuarios
	switch len(grupos) {

	case 0:
		log.Println("Caso CERO: usuario no estaba en ningún grupo")
		pp = append(pp, r.Usuario)
		ww = append(ww, fmt.Sprintf("(usuario = $%v OR usuario IS NULL)", len(pp)))
	case 1:
		log.Println("Caso UNO")
		pp = append(pp, grupos[0], r.Usuario)
		ww = append(ww, fmt.Sprintf("(grupo_usarios=$%v OR usuario=$%v OR usuario IS NULL)", len(pp), len(pp)+1))
	default:
		log.Println("Caso DEFAULT")
		pp = append(pp, r.Usuario)
		ww = append(ww, fmt.Sprintf("(grupo_usarios IN (%v) OR usuario=$%v OR usuario IS NULL)", arrayInt(grupos), len(pp)))
	}

	where := "WHERE " + strings.Join(ww, " AND ")
	query := fmt.Sprintf(`SELECT c,r,u,d FROM permisos_crud %v`, where)

	// Realizo query
	rows, err := h.conn.Query(ctx, query, pp...)
	if err != nil {
		return errors.Wrap(err, "consultando permiso CRUD por comitente")
	}
	defer rows.Close()

	for rows.Next() {
		v := CRUD{}
		err = rows.Scan(&v.Create, &v.Read, &v.Update, &v.Delete)
		if err != nil {
			return deferror.DB(err, "escaneando")
		}
		switch r.Crud {
		case Create:
			if v.Create {
				return
			}
		case Read:
			if v.Read {
				return
			}
		case Update:
			if v.Update {
				return
			}
		case Delete:
			if v.Delete {
				return
			}
		}
	}
	// Si llegué al final del loop y no encontré ninguno es porque no está permitido
	return NoAutorizado{r}
}

func (h *Handler) EsAdministrador(ctx context.Context, u string) (esAdmin bool, err error) {

	es := new(bool)
	err = h.conn.QueryRow(ctx, "SELECT administrador FROM usuarios WHERE id=$1", u).Scan(&es)
	if err != nil {
		return esAdmin, errors.Wrap(err, "querying/scanning")
	}
	if es != nil {
		esAdmin = *es
	}
	return
}

// Un usuario puede estar en varios grupos de usuarios.
// Determina los ids de grupos en los que está este usuario.
func (h *Handler) gruposPersonas(ctx context.Context, usuario string) (ids []int, err error) {

	gruposQuery := `
	SELECT id
	FROM grupos
	WHERE (
		de = 'usuarios' AND
		$1::string = ANY(strings)
	);
	`
	gruposRows, err := h.conn.Query(ctx, gruposQuery, usuario)
	if err != nil {
		return ids, errors.Wrapf(err, "quering")
	}
	defer gruposRows.Close()
	count := 0
	for gruposRows.Next() {
		count++
		id := 0
		err = gruposRows.Scan(&id)
		if err != nil {
			return ids, errors.Wrapf(err, "escanendo")
		}
		ids = append(ids, id)
	}
	return
}

func arrayInt(ids tipos.GrupoInts) string {
	idsStr := []string{}
	for _, v := range ids {
		idsStr = append(idsStr, fmt.Sprintf("%v", v))
	}
	return strings.Join(idsStr, ", ")
}
