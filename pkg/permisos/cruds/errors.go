package cruds

import "fmt"

type NoAutorizado struct {
	request Request
}

func (e NoAutorizado) Error() string {
	return fmt.Sprintf("No tiene permisos para %v en programa %v", e.request.Crud, e.request.Programa)
}
