package cruds

// CRUD es el registro en la base de datos que habilita
// a José, VER y CREAR en Facturación.
type CRUD struct {

	// Filtro
	Usuario       string
	GrupoUsuarios int
	Programa      int

	// Cero significa que TODOS las empresas están permitidas.
	// Null significa que el programa no separa por empresa
	Empresa *int

	// Operaciones permitidas
	Create bool
	Read   bool
	Update bool
	Delete bool
}

type Crud string

func (c Crud) Valid() bool {
	switch c {
	case "create", "read", "update", "delete":
		return true
	}
	return false
}

const (
	Create = Crud("create")
	Read   = Crud("read")
	Update = Crud("update")
	Delete = Crud("delete")
)

type Programa string
