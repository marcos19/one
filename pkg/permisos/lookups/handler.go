package lookups

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler contiene el estado.
type Handler struct {
	conn *pgxpool.Pool
}

// NewHandler inicializa un cache y devuelve el puntero a este.
func NewHandler(conn *pgxpool.Pool) (h *Handler) {

	h = &Handler{}
	h.conn = conn
	return
}

// LookupOne devuelve un permiso por su ID
func (h *Handler) LookupOne(ctx context.Context, id uuid.UUID, comitente int) (out DeLookups, err error) {

	out, err = readOne(ctx, h.conn, comitente, id)
	if err != nil {
		return out, errors.Wrap(err, "buscando permiso de lookup")
	}
	return
}

type LookupManyReq struct {
	Comitente    int
	PermisosPara string // Obligatorio (Ej: Cajas, Empresas, etc)
	Programa     string // si viene vacio los pido todos
	Usuario      string // si viene vacio los pido todos
	GrupoUsuario int    `json:",string"` // si viene vacio los pido todos
}

type LookupManyResponse struct {
	DeLookups
	NombreGrupo     *string `json:",omitempty"`
	NombreUsuario   *string `json:",omitempty"`
	ApellidoUsuario *string `json:",omitempty"`
}

// HandleMany determina si se puede realizar la operación crud solicitada.
func (h *Handler) LookupMany(ctx context.Context, r LookupManyReq) (out []LookupManyResponse, err error) {

	if r.PermisosPara == "" {
		errors.New("no se determinó PermisoPara")
		return
	}

	if r.Comitente == 0 {
		errors.New("no se determinó Comitente")
		return
	}

	ww := []string{"permisos_lookup.comitente=$1", "para=$2"}
	pp := []interface{}{r.Comitente, r.PermisosPara}

	if r.Programa != "" {
		pp = append(pp, r.Programa)
		ww = append(ww, fmt.Sprintf("programa=$%v", len(pp)))
	}
	if r.Usuario != "" {
		pp = append(pp, r.Usuario)
		ww = append(ww, fmt.Sprintf("usuario=$%v", len(pp)))
	}
	if r.GrupoUsuario != 0 {
		pp = append(pp, r.GrupoUsuario)
		ww = append(ww, fmt.Sprintf("grupo_usuarios=$%v", len(pp)))
	}
	where := "WHERE " + strings.Join(ww, " AND ")

	tail := ""
	joins := []string{
		"LEFT JOIN grupos ON grupos.id = permisos_lookup.grupo_usuarios",
		"LEFT JOIN usuarios ON usuarios.id = permisos_lookup.usuario",
	}
	// Le agrego los campos de lookup al SELECT
	switch r.PermisosPara {

	case "Comprobantes":
		tail = ", comps.nombre as detalle"
		joins = append(joins, "LEFT JOIN comps ON comps.id = permisos_lookup.int")

	case "Empresas":
		tail = ", empresas.nombre as detalle"
		joins = append(joins, "LEFT JOIN empresas ON empresas.id = permisos_lookup.int")

	case "Centros emisores":
		tail = ", talones.nombre as detalle"
		joins = append(joins, "LEFT JOIN talones ON talones.id = permisos_lookup.int")

	case "Cajas":
		tail = ", cajas.nombre as detalle"
		joins = append(joins, "LEFT JOIN cajas ON cajas.id = permisos_lookup.int")

	case "Programas":
		tail = ", programas.nombre as detalle"
		joins = append(joins, "LEFT JOIN programas ON programas.id = permisos_lookup.string")

	}

	query := fmt.Sprintf(`SELECT 
permisos_lookup.id, 
permisos_lookup.usuario, 
usuarios.nombre as nombre_usuario,
usuarios.apellido as apellido_usuario,
permisos_lookup.grupo_usuarios, 
grupos.nombre as nombre_grupo,
permisos_lookup.para,
permisos_lookup.int,
permisos_lookup.string
%v
FROM permisos_lookup
%v
%v
	`, tail, strings.Join(joins, " "), where)

	// Busco
	rows, err := h.conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	out = []LookupManyResponse{}
	for rows.Next() {
		v := LookupManyResponse{}
		err = rows.Scan(&v.ID, &v.Usuario, &v.NombreUsuario, &v.ApellidoUsuario, &v.GrupoUsuarios,
			&v.NombreGrupo, &v.Para, &v.Int, &v.String, &v.Detalle)
		if err != nil {
			return out, deferror.DB(err, "escaneando permisos lookup")
		}
		out = append(out, v)
	}
	return
}

// Insertar persiste un nuevo registro en la base de datos.
func (h *Handler) Insertar(ctx context.Context, r *DeLookups) (err error) {

	err = create(ctx, h.conn, r)
	if err != nil {
		return errors.Wrap(err, "creando permiso lookup")
	}

	return
}

func (h *Handler) Modificar(ctx context.Context, r *DeLookups) (err error) {

	err = update(ctx, h.conn, r)
	if err != nil {
		return errors.Wrap(err, "persistiendo modificación")
	}

	return
}
