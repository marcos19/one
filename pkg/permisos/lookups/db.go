package lookups

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/rs/zerolog/log"
)

func create(ctx context.Context, conn *pgxpool.Pool, req *DeLookups) (err error) {

	if req.Para == "" {
		return deferror.Validation("debe ingresar el campo Para")
	}

	req.ID, err = uuid.NewV4()
	if err != nil {
		return err
	}

	query := `INSERT INTO permisos_lookup (usuario, grupo_usuarios, para, int, string, comitente) VALUES 
	($1,$2,$3,$4,$5,$6)`

	_, err = conn.Exec(ctx, query, req.Usuario, req.GrupoUsuarios, req.Para, req.Int, req.String, req.Comitente)
	if err != nil {
		return deferror.DB(err, "inserting")
	}

	return
}

func update(ctx context.Context, conn *pgxpool.Pool, req *DeLookups) (err error) {

	if req.ID == uuid.Nil {
		return errors.New("no de definió ID")
	}
	if req.Comitente == 0 {
		return errors.New("no de definió ID de comitente")
	}
	if req.Para == "" {
		return deferror.Validation("debe ingresar el campo Para")
	}

	query := `UPDATE permisos_lookup SET usuario=$3, grupo_usuarios=$4, para=$5, int=$6, string=$7, comitente=$8 
	WHERE comitente=$1 AND id=$2`

	res, err := conn.Exec(ctx, query, req.Comitente, req.ID, req.Usuario, req.GrupoUsuarios, req.Para, req.Int, req.String, req.Comitente)
	if err != nil {
		return deferror.DB(err, "updating")
	}

	if res.RowsAffected() == 0 {
		return errors.New("no se modificó ningún registro")
	}

	return
}

func readOne(ctx context.Context, conn *pgxpool.Pool, comitente int, id uuid.UUID) (out DeLookups, err error) {
	if comitente == 0 {
		return out, errors.New("no se ingrersó ID de comitente")
	}
	if id == uuid.Nil {
		return out, errors.New("no se ingresó ID de persona")
	}
	query := `SELECT id, usuario, grupo_usuarios, para, int, string, comitente FROM permisos_lookup 
	WHERE comitente=$1 AND id=$2`

	err = conn.QueryRow(ctx, query, comitente, id).Scan(
		&out.ID, &out.Usuario, &out.GrupoUsuarios, &out.Para, &out.Int, &out.String, &out.Comitente)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}
	return
}

type permisosParaReq struct {
	comitente int
	para      string // Tabla
	usuario   Usuario
	programa  Programa
}

func permisosParaInt(ctx context.Context, conn *pgxpool.Pool, req permisosParaReq) (out tipos.GrupoInts, err error) {

	if req.usuario == "" {
		return out, errors.Errorf("no se ingresó usuario")
	}
	if req.comitente == 0 {
		return out, errors.Errorf("no se ingresó ID de comitente")
	}

	ww := []string{"comitente=$1", "para=$2"}
	pp := []interface{}{req.comitente, req.para}
	if req.programa != "" {
		pp = append(pp, req.programa)
		ww = append(ww, fmt.Sprintf("programa=$%v", len(pp)))
	}

	// Agrego los constraints de usuario
	ww, pp, err = usuarioWhereClause(ctx, conn, req.usuario, ww, pp)
	if err != nil {
		return out, err
	}

	where := "WHERE " + strings.Join(ww, " AND ")

	query := fmt.Sprintf(`SELECT int FROM permisos_lookup 
	%v`, where)

	log.Debug().Msg("\n\n" + query)
	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	m := map[int]struct{}{}
	for rows.Next() {
		id := 0
		err = rows.Scan(&id)
		if err != nil {
			return out, deferror.DB(err, "escaneando")
		}
		m[id] = struct{}{}
	}

	for k := range m {
		out = append(out, k)
	}
	return
}

// Esta función arma la sentencia SQL para filtrar los usuarios.
func usuarioWhereClause(ctx context.Context, conn *pgxpool.Pool, user Usuario, ww []string, pp []interface{}) ([]string, []interface{}, error) {

	grupos, err := gruposPersonas(ctx, conn, Usuario(user))
	if err != nil {
		return ww, pp, errors.Wrap(err, "determinando grupos de usuarios")
	}

	// Filtro si se otorgó para un grupos de usuarios
	switch len(grupos) {

	case 0:
		log.Debug().Msgf("%v: Caso CERO: usuario no estaba en ningún grupo", user)
		pp = append(pp, user)
		ww = append(ww, fmt.Sprintf("(usuario=$%v OR usuario='')", len(pp)))
	case 1:
		log.Debug().Msg("Caso UNO")
		pp = append(pp, grupos[0], user)
		ww = append(ww, fmt.Sprintf("(grupo_usarios=$%v OR usuario=$%v OR usuario='')", len(pp), len(pp)+1))
	default:
		log.Debug().Msg("Caso DEFAULT")
		pp = append(pp, user)
		ww = append(ww, fmt.Sprintf("(grupo_usarios IN (%v) OR usuario=$%v OR usuario='')", arrayInt(grupos), len(pp)))
	}

	return ww, pp, nil
}

// Un usuario puede estar en varios grupos de usuarios.
// Determina los ids de grupos en los que está este usuario.
func gruposPersonas(ctx context.Context, conn *pgxpool.Pool, usuario Usuario) (ids tipos.GrupoInts, err error) {

	query := `
	SELECT id
	FROM grupos
	WHERE (
		de = 'usuarios' AND
		$1::string = ANY(strings)
	);
	`
	rows, err := conn.Query(ctx, query, string(usuario))
	if err != nil {
		return ids, errors.Wrapf(err, "querying")
	}
	defer rows.Close()

	count := 0
	for rows.Next() {
		count++
		id := 0
		err = rows.Scan(&id)
		if err != nil {
			return ids, errors.Wrapf(err, "escanendo row de grupos de usuarios")
		}
		ids = append(ids, id)
	}
	return
}

func arrayInt(ids tipos.GrupoInts) string {
	idsStr := []string{}
	for _, v := range ids {
		idsStr = append(idsStr, fmt.Sprintf("%v", v))
	}
	return strings.Join(idsStr, ", ")
}
