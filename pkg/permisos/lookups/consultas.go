package lookups

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
)

// Empresas devuelve los ids de las empresas permitidas para este usuario.
// Cuando entre a algun programa transaccional, si el usuario tiene
// habilitada la empresa, podrá seleccionarla.
func (h *Handler) Empresas(ctx context.Context, user Usuario, comitente int) (cc []int, err error) {
	req := permisosParaReq{
		comitente: comitente,
		para:      "Empresas",
		usuario:   user,
	}
	cc, err = permisosParaInt(ctx, h.conn, req)
	if err != nil {
		return cc, errors.Wrap(err, "buscando empresas permitidas")
	}
	return cc, err
}

// Comprobantes determina los ID de los comprobantes permitidos para el usuario.
func (h *Handler) Comprobantes(ctx context.Context, user Usuario, comitente int) (cc []int, err error) {

	req := permisosParaReq{
		comitente: comitente,
		para:      "Comprobantes",
		usuario:   user,
	}
	cc, err = permisosParaInt(ctx, h.conn, req)
	if err != nil {
		return cc, errors.Wrap(err, "buscando comprobantes permitidos")
	}

	return
}

// Cajas devuelve los IDs de las cajas permitidas para los argumentos ingresados.
func (h *Handler) Cajas(ctx context.Context, comitente int, programa Programa, user Usuario) (out tipos.GrupoInts, err error) {

	req := permisosParaReq{
		comitente: comitente,
		para:      "Cajas",
		usuario:   user,
		programa:  programa,
	}
	out, err = permisosParaInt(ctx, h.conn, req)
	if err != nil {
		return out, errors.Wrap(err, "buscando cajas permitidas")
	}
	return
}

// // CentrosEmisores determina los ID de los centros emisores permitidos para los argumentos ingresados.
// func (h *Handler) CentrosEmisores(user Usuario) (cc []int, err error) {

// 	req := permisosParaReq{
// 		comitente: comitente,
// 		para:      "Centros emisores",
// 		usuario:   user,
// 		programa:  programa,
// 	}
// 	out, err = permisosParaInt(ctx, h.conn, req)
// 	if err != nil {
// 		return out, errors.Wrap(err, "buscando centros emisores permitidos")
// 	}
// 	return
// }

// // Depositos determina los ID de los depositos permitidos para los argumentos ingresados.
// func (h *Handler) Depositos(user Usuario) (cc []int, err error) {
// 	db := h.db.New().Table("permisos_lookup")

// 	cc = []int{}
// 	db = db.Where("para = 'Depositos'")
// 	db, err = usuarioWhereClause(db, user)
// 	if err != nil {
// 		return cc, err
// 	}
// 	db = db.Select("int")
// 	err = db.Find(&cc).Error
// 	if err != nil {
// 		return cc, errors.Wrap(err, "buscando depositos permitidos")
// 	}

// 	return cc, err
// }
