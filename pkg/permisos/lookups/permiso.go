// Package permisos permite administrar varios tipos de permisos:
//
//	(1) De programas:   func(user, programa, crud) bool
//	      => José puede VER y CREAR en Facturación.
//
//	(2) De consultas    func(user, programa, queCosa):
//	      => Jose, para el resumen de caja, puede ver las empresas 1 y 2 y la caja 3, pero para autorizar facturas solamente la empresa 1.
//	      => Jose, para autorizar facturas, solamente la empresa 1.
//	      => Cuando ingreso a facturación, cuáles son los comprobantes y centros emisores definidos.
//
// Tras bambalinas, José tendría grabado los siguientes permisos:
//   - Para el programa RESUMEN de CAJA => VER.
//   - Para los lookups:
//   - Empresa: empresas ID 1 y 2.
//   - Caja: caja ID 3.
//
// Hay métodos creados que devuelven únicamentes los IDs de las entidades permitidas.
// Están pensados para que los puedan usar otros packages directamente, o bien
// cuando el usuario por ejemplo pide el lookup de comprobantes disponibles a la
// hora de realizar una factura.
package lookups

import "github.com/gofrs/uuid"

// Usuario es el nombre de un usuario.
// Posee un tipo particular para evitar errores a la hora de ingresar los
// parámetros en las funciones.
type Usuario string

// Programa es el nombre del programa.
// Posee un tipo particular para evitar errores a la hora de ingresar los
// parámetros en las funciones.
type Programa string

// Crud es CREATE, READ, UPDATE, DELETE
// Posee un tipo particular para evitar errores a la hora de ingresar los
// parámetros en las funciones.
type Crud string

func (c Crud) Valid() bool {
	switch c {
	case "create", "read", "update", "delete":
		return true
	}
	return false
}

const (
	Create = Crud("create")
	Read   = Crud("read")
	Update = Crud("update")
	Delete = Crud("delete")

	ProgramaCajas         = Programa("cajas")
	ProgramaCentrosCosto  = Programa("centros")
	ProgramaComps         = Programa("comps")
	ProgramaCondiciones   = Programa("condiciones")
	ProgramaDepositos     = Programa("depositos")
	ProgramaEmpresas      = Programa("empresas")
	ProgramaEsquemas      = Programa("esquemas")
	ProgramaListasPrecio  = Programa("listasDePrecio")
	ProgramaPrecios       = Programa("precios")
	ProgramaProductos     = Programa("productos")
	ProgramaTransacciones = Programa("transacciones")
	ProgramaUnicidades    = Programa("unicidades")
)

// DeLookups es el registro en la base de datos que habilita
// que habilita a José una vez que ingresó a Facturación, a solamente poder elegir [Venta Directa, Venta anticipada] y [Facturas A, Factura B].
type DeLookups struct {
	ID        uuid.UUID
	Comitente int `json:"-"`

	// Filtro
	Usuario       *string // "" significa todos los usuarios, null significa ninguno
	GrupoUsuarios int     `json:",string"` // Cero significa ninguno

	Para string // Empresa, Cuenta Contable, etc

	// Operaciones permitidas
	Int    *int `json:",string"`
	String *string

	Detalle *string // Dice el nombre del int o string
}

// TableName devuelve el nombre de la tabla donde se persisten los permisos de lookup
func (d *DeLookups) TableName() string {
	return "permisos_lookup"
}
