package treesum

import (
	"fmt"
	"os"
	"testing"

	"github.com/crosslogic/dec"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestMakeTree(t *testing.T) {

	cc := []Account{
		{
			id:   1,
			name: "First grouper",
		},
		{
			id:   10,
			name: "Second grouper",
		},
		{
			id:   100,
			name: "Account A",
		},
		{
			id:   200,
			name: "Account B",
		},
	}
	cc[1].father = &cc[0].id
	cc[2].father = &cc[1].id
	cc[3].father = &cc[1].id

	cci := make([]Node[int], len(cc))
	for i, v := range cc {
		cci[i] = &v
	}

	values := map[int]dec.D2{
		100: 300,
		200: 700,
	}
	f := func(a, b *SumNode[*Account, dec.D2, int]) int {
		switch {
		case a.Node.name < b.Node.name:
			return -1

		case a.Node.name > b.Node.name:
			return 1
		}

		return 0
	}

	//func(a *SumNode[Node[int], dec.D2, int], b *SumNode[Node[int], dec.D2, int]) int) as
	//func(a *SumNode[*Account, dec.D2, int], b *SumNode[*Account, dec.D2, int])
	tree, err := MakeTree[*Account, dec.D2, int](cci, values, f)
	require.Nil(t, err)

	// Root
	assert.True(t, tree.root)
	assert.Nil(t, tree.Node)
	require.True(t, len(tree.Children) == 1)

	// First
	first := tree.Children[0]
	assert.NotNil(t, first.Node)
	assert.Equal(t, 1, first.Node.GetID())
	require.True(t, len(first.Children) == 1)
	assert.Equal(t, 0, first.Depth())

	// Second
	second := first.Children[0]
	assert.NotNil(t, second.Node)
	assert.Equal(t, 10, second.Node.GetID())
	require.True(t, len(second.Children) == 2)
	assert.Equal(t, 1, second.Depth())

	// Third
	third := second.Children[0]
	assert.NotNil(t, third.Node)
	assert.Equal(t, 100, third.Node.GetID())
	require.True(t, len(third.Children) == 0)
	assert.Equal(t, 2, third.Depth())

	// Fourth
	fourth := second.Children[1]
	assert.NotNil(t, fourth.Node)
	assert.Equal(t, 200, fourth.Node.GetID())
	require.True(t, len(fourth.Children) == 0)
	assert.Equal(t, 2, fourth.Depth())

	tree.Fprint(os.Stdout)

	flat := tree.Flaten()
	for _, v := range flat {
		fmt.Println(v.Node, v.Ammount)
	}
}

type Account struct {
	id     int
	father *int
	name   string
}

func (a *Account) GetID() int {
	return a.id
}
func (a *Account) GetFather() *int {
	return a.father
}
func (a Account) String() string {
	return a.name
}
