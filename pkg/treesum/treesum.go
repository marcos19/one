package treesum

import (
	"fmt"
	"io"
	"slices"

	"github.com/crosslogic/dec"
)

type Sumable interface {
	float64 | dec.D2
}

type SumNode[T Node[K], S Sumable, K comparable] struct {
	Node     T
	Ammount  S
	Children []*SumNode[T, S, K]
	SortFunc func(a, b *SumNode[T, S, K]) int
	root     bool

	father *SumNode[T, S, K]
}

// Prints a console tree
func (s *SumNode[T, S, K]) Fprint(w io.Writer) {

	var spaces string
	if !s.root {
		depth := s.Depth()
		vertical := "|"
		if depth == 0 {
			vertical = " "
		}
		for i := 0; i < depth; i++ {
			spaces += "  "
		}
		fmt.Fprintf(w, "%v%v_%v: %v\n", spaces, vertical, s.Node, s.Ammount)
	}
	for _, v := range s.Children {
		v.Fprint(w)
	}
}

func (s *SumNode[T, S, K]) Flaten() (out []*SumNode[T, S, K]) {
	for _, v := range s.Children {
		out = append(out, v)
		out = append(out, v.Flaten()...)
	}
	return
}

type Node[K comparable] interface {
	GetID() K
	GetFather() *K
}

// 0 is in root
func (s SumNode[T, S, K]) Depth() int {
	return s.depth(0)
}

func (s SumNode[T, S, K]) depth(previous int) int {
	if s.father == nil {
		return previous
	}
	return s.father.depth(previous + 1)
}

func MakeTree[T Node[K], S Sumable, K comparable](cc []Node[K], values map[K]S, sortFunc func(a, b *SumNode[T, S, K]) int) (root *SumNode[T, S, K], err error) {
	if len(cc) == 0 {
		return
	}
	m := map[K]*SumNode[T, S, K]{}

	integrity := map[K]struct{}{}
	for _, v := range cc {
		id := v.GetID()
		n := SumNode[T, S, K]{}
		n.Node = v.(T)
		m[id] = &n
		integrity[id] = struct{}{}
	}

	root = &SumNode[T, S, K]{root: true}
	for _, v := range m {
		fatherID := v.Node.GetFather()
		v.Ammount = values[v.Node.GetID()]
		if fatherID == nil {
			root.Children = append(root.Children, v)
			continue
		}
		father, ok := m[*fatherID]
		if !ok {
			return root, fmt.Errorf("node ID=%v with father=%v: %w", v.Node.GetID(), fatherID, ErrOrphanNode)
		}
		father.Children = append(father.Children, v)
		v.father = father
	}

	err = root.checkTreeCircular(integrity)
	if err != nil {
		return nil, fmt.Errorf("referencia circular: %w", err)
	}
	if len(integrity) != 0 {
		return nil, fmt.Errorf("quedaron huérfanas %v cuentas: %w", len(integrity), ErrCircularRef)
	}

	root.sum()
	root.sort(sortFunc)
	return
}

func (n *SumNode[T, S, K]) sum() (out S) {
	if len(n.Children) == 0 {
		return n.Ammount
	}
	for _, v := range n.Children {
		n.Ammount += v.sum()
	}
	out += n.Ammount
	return
}

func (n *SumNode[T, S, K]) sort(f func(a, b *SumNode[T, S, K]) int) {
	if len(n.Children) == 0 {
		return
	}
	slices.SortFunc(n.Children, f)
	for _, v := range n.Children {
		v.sort(f)
	}
}

func (n *SumNode[T, S, K]) checkTreeCircular(integrity map[K]struct{}) (err error) {

	if !n.root {
		id := n.Node.GetID()
		_, ok := integrity[id]
		if !ok {
			return ErrCircularRef
		}
		delete(integrity, id)
	}

	for _, v := range n.Children {
		v.checkTreeCircular(integrity)
	}
	return
}

var ErrCircularRef = fmt.Errorf("referencia circular")
var ErrOrphanNode = fmt.Errorf("orphan node")
