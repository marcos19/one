// Package deferror define diferentes tipos de errores que se pueden
// producir en el backend. Necesito saberlos
package deferror

import (
	"fmt"

	"github.com/cockroachdb/errors"
)

// DBError se produce cuando se hace una llamada a la base de datos y esta
// devuelve un error.
type DBError struct{}

func (e DBError) Error() string {
	return "en base de datos"
}

func DB(inner error, msg string) error {
	return errors.Wrap(errors.Mark(inner, DBError{}), msg)
}
func DBf(inner error, msg string, args ...interface{}) error {
	return errors.Wrapf(errors.Mark(inner, DBError{}), msg, args...)
}

// ValidationError se produce cuando el usuario no ingresó los datos como
// se esperaba recibir en el backend.
type ValidationError struct {
	msg string
}

func (e ValidationError) Error() string {
	return e.msg
}

func Validation(msg string) error {
	return errors.Mark(ValidationError{msg}, ValidationError{})
}
func Validationf(msg string, args ...interface{}) error {
	return errors.Mark(
		ValidationError{fmt.Sprintf(msg, args...)},
		ValidationError{},
	)
}

// BadRequestError se produce cuando el usuario no ingresó los datos como
// se esperaba recibir en el backend.
type BadRequestError struct{}

func (e BadRequestError) Error() string {
	return "Bad request"
}
func BadRequest(err error) error {
	str := err.Error()
	return errors.Wrap(errors.Mark(err, BadRequestError{}), str)
}

// ComitenteIndefinidoError cuando el no se pudo determinar el comitente en
// el request.
type ComitenteIndefinidoError struct{}

func (e ComitenteIndefinidoError) Error() string {
	return "comitente indefinido"
}
func ComitenteIndefinido() error {
	return ComitenteIndefinidoError{}
}

// EmpresaIndefinidaError se devuelve cuando una operación exigía EmpresaIndefinidaError
// pero la misma no fue informada.
type EmpresaIndefinidaError struct{}

func (e EmpresaIndefinidaError) Error() string {
	return "empresa indefinida"
}
func EmpresaIndefinida() error {
	return EmpresaIndefinidaError{}
}

// MuchosComitentesError da cuando un usuario tiene asignado más de un usuario
type MuchosComitenteError struct{}

func (e MuchosComitenteError) Error() string {
	return "Muchos comitentes"
}
func MuchosComitentes() error {
	return MuchosComitenteError{}
}

// NotFoundError se produce cuando la URL solicitada no se pudo
// asociar a una función.
type NotFoundError struct {
	path string
}

func (e NotFoundError) Error() string {
	return e.path
}
func NotFound(path string) error {
	return errors.Mark(NotFoundError{path}, NotFoundError{})
}

// NotFoundError se produce cuando la URL solicitada no se pudo
// asociar a una función.
type WebServiceError struct {
}

func (e WebServiceError) Error() string {
	return "web service respondió con error"
}

func WebService(msg string, args ...interface{}) error {
	texto := fmt.Sprintf("error devuelto por web service: "+msg, args...)
	marked := errors.Mark(errors.New(texto), WebServiceError{})
	return marked
}

//
//
// Autenticación y autorización
//
//

//// NoAutenticadoError se produce cuando el usuario quiere acceder
//// a una URL, pero no tiene una sesión vigente.
//type NoAutenticadoError struct{}

//func (e NoAutenticadoError) Error() string {
//return "No autenticado"
//}
//func NoAutenticado() error {
//return NoAutenticadoError{}
//}

//// NoAutenticadoError se produce cuando el usuario quiere acceder
//// a una URL, pero no tiene una sesión vigente.
//type AutenticacionIncorrectaError struct{}

//func (e AutenticacionIncorrectaError) Error() string {
//return "Autenticación incorrecta"
//}
//func AutenticacionIncorrecta() error {
//return AutenticacionIncorrectaError{}
//}

//// UnauthorizedError cuando el usuario solicita un recurso para el cual
//// no tiene permisos para realizar.
//type NoAutorizadoError struct{}

//func (e NoAutorizadoError) Error() string {
//return "No autorizado"
//}
//func Unauthorized(msg string, args ...interface{}) error {
//return errors.Mark(errors.Errorf(msg, args...), NoAutorizadoError{})
//}

//// ContraseñaVecnida se produce cuando se hace un login correcto pero
//// que tiene la fecha vencida.
//type ContraseñaVencidaError struct {
//User string
//Pass string
//}

//func (e ContraseñaVencidaError) Error() string {
//j, _ := json.Marshal(e)
//return string(j)
//}
//func ContraseñaVencida(user string, pass string) error {
//return errors.Mark(ContraseñaVencidaError{
//User: user,
//Pass: pass,
//}, ContraseñaVencidaError{})
//}
