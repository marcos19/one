package deferror

import (
	"encoding/json"
	"net/http"

	"github.com/cockroachdb/errors"
)

type Response struct {
	Tipo string
	Msg  string
}

// func Middleware(err error) http.HandlerFunc {
// 	return func(w http.ResponseWriter, r *http.Request) {
// 		resp := Response{}
// 		w.WriteHeader(http.StatusInternalServerError)

// 		// Bad request
// 		if errors.Is(err, BadRequestError{}) {
// 			resp.Tipo = "Bad request"
// 			resp.Msg = err.Error()
// 			json.NewEncoder(w).Encode(&resp)
// 			return
// 		}

// 		// Base de datos
// 		if errors.Is(err, DBError{}) {
// 			resp.Tipo = "Base de datos"
// 			resp.Msg = err.Error()
// 			json.NewEncoder(w).Encode(&resp)
// 			return
// 		}

// 		// Validación
// 		if errors.Is(err, ValidationError{}) {
// 			resp.Tipo = "Validación"
// 			resp.Msg = err.Error()
// 			json.NewEncoder(w).Encode(&resp)
// 			return
// 		}

// 		// NoAutenticado
// 		if errors.Is(err, NoAutenticadoError{}) {
// 			resp.Tipo = "No autenticado"
// 			json.NewEncoder(w).Encode(&resp)
// 			return
// 		}

// 		// Unauthorized
// 		if errors.Is(err, NoAutorizadoError{}) {
// 			resp.Tipo = "No autorizado"
// 			json.NewEncoder(w).Encode(&resp)
// 			return
// 		}

// 		// Comitente indefinido
// 		if errors.Is(err, ComitenteIndefinidoError{}) {
// 			resp.Tipo = "Comitente indefinido"
// 			json.NewEncoder(w).Encode(&resp)
// 			return
// 		}

// 		// Default, no debería llegar nunca acá
// 		resp.Tipo = "Interno"
// 		resp.Msg = err.Error()
// 		json.NewEncoder(w).Encode(&resp)

// 	}
// }

// Frontend responde al frontend un error para que este pueda tomar
// las acciones que correspondan.
// Siempre devuelvo error con código 500.
func Frontend(err error, w http.ResponseWriter, r *http.Request) {
	resp := Response{}

	switch {

	// NotFound
	case errors.Is(err, NotFoundError{}):
		resp.Tipo = "No encontrado"
		resp.Msg = err.Error()
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(&resp)

	// WebService
	case errors.Is(err, WebServiceError{}):
		resp.Tipo = "Web service error"
		resp.Msg = err.Error()
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(&resp)

	// Bad request
	case errors.Is(err, BadRequestError{}):
		resp.Tipo = "Bad request"
		resp.Msg = err.Error()
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(&resp)

	// Comitente indefinido
	case errors.Is(err, ComitenteIndefinidoError{}):
		resp.Tipo = "Comitente indefinido"
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(&resp)

	// Base de datos
	case errors.Is(err, DBError{}):
		resp.Tipo = "Base de datos"
		resp.Msg = err.Error()
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(&resp)

	// Validación
	case errors.Is(err, ValidationError{}):
		resp.Tipo = "Validación"
		resp.Msg = err.Error()
		w.WriteHeader(http.StatusUnprocessableEntity)
		json.NewEncoder(w).Encode(&resp)

	default:
		resp.Tipo = "Interno"
		resp.Msg = err.Error()
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(&resp)

	}
}
