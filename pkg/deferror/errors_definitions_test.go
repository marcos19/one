package deferror

import (
	"testing"

	"github.com/cockroachdb/errors"
	"github.com/davecgh/go-spew/spew"
	"github.com/stretchr/testify/assert"
)

func TestDBError(t *testing.T) {
	err := DB(errors.New("column name not found"), "buscando persona")

	// fmt.Println(err.Error())

	assert.True(t, errors.Is(DBError{}, DBError{}))
	assert.True(t, errors.Is(err, DBError{}))
	assert.Equal(t, "buscando persona: column name not found", err.Error())

	// Lo envuelvo nuevamente
	err2 := errors.Wrap(err, "superior")
	assert.True(t, errors.Is(err2, DBError{}))

	// fmt.Println("Error original", errors.UnwrapAll(err2))
	// fmt.Printf("Tipo: %t", err2)

}

func TestValidationError(t *testing.T) {
	err := Validation("No se ingresó ID")

	// fmt.Println(err.Error())

	assert.True(t, errors.Is(err, ValidationError{}), "%v", spew.Sdump(err))
	assert.Equal(t, "No se ingresó ID", err.Error())

	// Lo envuelvo nuevamente
	err2 := errors.Wrap(err, "superior")
	assert.True(t, errors.Is(err2, ValidationError{}))

}

func TestComitenteError(t *testing.T) {
	err := ComitenteIndefinido()

	// fmt.Println(err.Error())

	assert.True(t, errors.Is(err, ComitenteIndefinidoError{}))
	assert.Equal(t, "comitente indefinido", err.Error())

	// Lo envuelvo nuevamente
	err2 := errors.Wrap(err, "determinando el comitente")
	assert.True(t, errors.Is(err2, ComitenteIndefinidoError{}))
	assert.False(t, errors.Is(err2, ValidationError{}))

}
