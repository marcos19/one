package refundicion

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/minuta"
	"bitbucket.org/marcos19/one/pkg/minuta/configs"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

type Handler struct {
	db                  *pgxpool.Pool
	minutaHandler       *minuta.Handler
	minutaConfigHandler *configs.Handler
}

func NewHandler(db *pgxpool.Pool, minH *minuta.Handler, cfgH *configs.Handler) *Handler {
	return &Handler{db, minH, cfgH}
}

func (h *Handler) VistaPrevia(ctx context.Context, req VistaPreviaReq) (out []Partida, err error) {
	return traerSaldos(ctx, h.db, req)
}

func (h *Handler) Confirmar(ctx context.Context, req ConfirmarReq) (opID uuid.UUID, err error) {

	// Creo la minuta
	m, err := h.crearMinuta(ctx, req)
	if err != nil {
		return opID, errors.Wrap(err, "creando minuta")
	}

	// Persisto la minuta
	return h.minutaHandler.Create(ctx, &m, minuta.CreateArgs{EsRefundicion: true})
}
