package refundicion

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/minuta"
	"bitbucket.org/marcos19/one/pkg/minuta/configs"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

type Partida struct {
	Cuenta int
	Centro *int
	Monto  dec.D2
}

type VistaPreviaReq struct {
	Comitente int
	Usuario   string
	Empresa   int `json:",string"`
	Desde     fecha.Fecha
	Hasta     fecha.Fecha
}

func traerSaldos(ctx context.Context, db *pgxpool.Pool, req VistaPreviaReq) (out []Partida, err error) {

	if req.Empresa == 0 {
		return out, deferror.Validation("no se ingresó empresa")
	}
	if req.Desde == 0 {
		return out, deferror.Validation("no se ingresó fecha desde")
	}
	if req.Hasta == 0 {
		return out, deferror.Validation("no se ingresó fecha hasta")
	}
	if req.Hasta < req.Desde {
		return out, deferror.Validation("fecha hasta debe ser posterior a fecha desde")
	}
	query := `SELECT cuenta, centro, SUM(monto)
	FROM partidas
	WHERE
		empresa=$1 AND
		fecha_contable >= $2 AND
		fecha_contable <= $3 AND
		comitente = $4 AND
		cuenta IN (
			SELECT id FROM cuentas WHERE tipo_cuenta = 'Resultado'
		)
	GROUP BY cuenta, centro
	HAVING SUM(monto) <> 0;
	`
	rows, err := db.Query(ctx, query, req.Empresa, req.Desde, req.Hasta, req.Comitente)
	if err != nil {
		return out, errors.Wrap(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		v := Partida{}
		monto := 0
		err = rows.Scan(&v.Cuenta, &v.Centro, &monto)
		if err != nil {
			return out, errors.Wrap(err, "scanning")
		}
		v.Monto = -dec.D2(monto)
		out = append(out, v)
	}

	if out == nil {
		out = []Partida{}
	}
	return
}

type ConfirmarReq struct {
	VistaPreviaReq
	Config  int `json:",string"`
	Esquema int `json:",string"`
}

func (h *Handler) crearMinuta(ctx context.Context, req ConfirmarReq) (out minuta.Minuta, err error) {
	// Valido
	if req.Config == 0 {
		return out, deferror.Validation("no se ingresó config ID")
	}
	if req.Esquema == 0 {
		return out, deferror.Validation("no se ingresó esquema ID")
	}

	ss, err := traerSaldos(ctx, h.db, req.VistaPreviaReq)
	if err != nil {
		return out, errors.Wrap(err, "determinado saldos")
	}

	out = minuta.Minuta{
		Config:    req.Config,
		Esquema:   req.Esquema,
		Fecha:     req.Hasta,
		Comitente: req.Comitente,
		Empresa:   req.Empresa,
		Detalle:   "Asiento de refundición",
	}
	out.Usuario = new(string)
	*out.Usuario = req.Usuario

	sum := dec.D2(0)
	for _, v := range ss {
		p := minuta.Partida{
			Cuenta: v.Cuenta,
		}
		if v.Monto > 0 {
			p.Debe = v.Monto
		} else {
			p.Haber = -v.Monto
		}
		if v.Centro != nil {
			p.Centro = new(int)
			*p.Centro = *v.Centro
		}
		sum += v.Monto
		out.Partidas = append(out.Partidas, p)
	}

	cfg, err := h.minutaConfigHandler.ReadOne(ctx, configs.ReadOneReq{Comitente: req.Comitente, ID: req.Config})
	if err != nil {
		return out, errors.Wrap(err, "buscando config")
	}
	if cfg.CuentaRefundicion == nil {
		return out, errors.Errorf("la config no define cuenta para cerrar asiento")
	}

	// Partida cierre
	p := minuta.Partida{
		Cuenta: *cfg.CuentaRefundicion,
	}
	if sum > 0 {
		p.Haber = sum
	} else {
		p.Debe = -sum
	}
	out.Partidas = append(out.Partidas, p)
	return
}
