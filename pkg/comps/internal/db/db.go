package db

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

func Create(ctx context.Context, conn *pgxpool.Pool, req comps.Comp) (id int, err error) {

	query := `
INSERT INTO comps(
	comitente,
	empresa,
	tipo, 
	nombre, 
	detalle, 
	punto_de_venta,
	tipo_emision,
	tipo_numeracion,
	tipo_anulacion,
	ultimo_numero_usado, 
	imprime_datos_cajero, 
	calcula_iva, 
	discrimina_iva,
	letra, 
	va_al_libro_de_iva, 
	libro_iva, 
	comprobante_fijo,
	originales_aceptados,
	margen_izq,
	margen_der, 
	margen_sup, 
	margen_inf, 
	altura_cabecera,
	altura_persona,
	altura_pie, 
	imprime_logo, 
	imprime_bordes, 
	imprime_nombre_usuario, 
	imprime_ts, 
	created_at, 
	formato_numeracion, 
	productos_nombre_impresion,
	productos_ocultar_codigo, 
	productos_ocultar_cantidades, 
	punto_de_venta_obligatorio, 
	inactivo, 
	compra_o_venta, 
	fecha_desde, 
	fecha_hasta,
	mail_template,
	emisor
) VALUES (
	$1, $2, $3, $4, $5, $6, $7, $8, $9, $10,
	$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,
	$21,$22,$23,$24,$25,$26,$27,$28,$29,$30,
	$31,$32,$33,$34,$35,$36,$37,$38,$39,$40,
	$41) 
RETURNING id`

	err = conn.QueryRow(
		ctx, query,
		req.Comitente,
		req.Empresa,
		req.Tipo,
		req.Nombre,
		req.Detalle,
		req.PuntoDeVenta,
		req.TipoEmision,
		req.TipoNumeracion,
		req.TipoAnulacion,
		req.UltimoNumeroUsado,
		req.ImprimeDatosCajero,
		req.CalculaIVA,
		req.DiscriminaIVA,
		req.Letra,
		req.VaAlLibroDeIVA,
		req.LibroIVA,
		req.ComprobanteFijo,
		req.OriginalesAceptados,
		req.MargenIzq,
		req.MargenDer,
		req.MargenSup,
		req.MargenInf,
		req.AlturaCabecera,
		req.AlturaPersona,
		req.AlturaPie,
		req.ImprimeLogo,
		req.ImprimeBordes,
		req.ImprimeNombreUsuario,
		req.ImprimeTS,
		time.Now(),
		req.FormatoNumeracion,
		req.ProductosNombreImpresion,
		req.ProductosOcultarCodigo,
		req.ProductosOcultarCantidades,
		req.PuntoDeVentaObligatorio,
		req.Inactivo,
		req.CompraOVenta,
		req.FechaDesde,
		req.FechaHasta,
		req.MailTemplate,
		req.Emisor,
	).Scan(&id)
	if err != nil {
		return id, deferror.DB(err, "inserting")
	}
	return
}
func ReadOne(ctx context.Context, conn *pgxpool.Pool, req comps.ReadOneReq) (comps.Comp, error) {

	v := comps.Comp{}
	err := conn.QueryRow(ctx, readOneQuery(), req.Comitente, req.ID).
		Scan(&v.ID, &v.Comitente, &v.Empresa, &v.Tipo, &v.Nombre, &v.Detalle, &v.PuntoDeVenta, &v.TipoEmision,
			&v.TipoNumeracion, &v.TipoAnulacion, &v.UltimoNumeroUsado, &v.ImprimeDatosCajero, &v.CalculaIVA,
			&v.DiscriminaIVA, &v.Letra, &v.VaAlLibroDeIVA, &v.LibroIVA,
			&v.ComprobanteFijo, &v.OriginalesAceptados, &v.MargenIzq, &v.MargenDer, &v.MargenSup, &v.MargenInf,
			&v.AlturaCabecera, &v.AlturaPersona, &v.AlturaPie, &v.ImprimeLogo, &v.ImprimeBordes,
			&v.ImprimeNombreUsuario, &v.ImprimeTS, &v.CreatedAt, &v.UpdatedAt, &v.FormatoNumeracion,
			&v.ProductosNombreImpresion, &v.ProductosOcultarCodigo, &v.ProductosOcultarCantidades,
			&v.PuntoDeVentaObligatorio, &v.Inactivo, &v.CompraOVenta, &v.FechaDesde, &v.FechaHasta,
			&v.MailTemplate, &v.Emisor)

	if err != nil {
		return comps.Comp{}, deferror.DB(err, "querying/scanning")
	}
	return v, nil
}

func ReadOneTx(ctx context.Context, tx pgx.Tx, req comps.ReadOneReq) (comps.Comp, error) {

	v := comps.Comp{}
	err := tx.QueryRow(ctx, readOneQuery(), req.Comitente, req.ID).
		Scan(&v.ID, &v.Comitente, &v.Empresa, &v.Tipo, &v.Nombre, &v.Detalle, &v.PuntoDeVenta, &v.TipoEmision,
			&v.TipoNumeracion, &v.TipoAnulacion, &v.UltimoNumeroUsado, &v.ImprimeDatosCajero, &v.CalculaIVA,
			&v.DiscriminaIVA, &v.Letra, &v.VaAlLibroDeIVA, &v.LibroIVA,
			&v.ComprobanteFijo, &v.OriginalesAceptados, &v.MargenIzq, &v.MargenDer, &v.MargenSup, &v.MargenInf,
			&v.AlturaCabecera, &v.AlturaPersona, &v.AlturaPie, &v.ImprimeLogo, &v.ImprimeBordes,
			&v.ImprimeNombreUsuario, &v.ImprimeTS, &v.CreatedAt, &v.UpdatedAt, &v.FormatoNumeracion,
			&v.ProductosNombreImpresion, &v.ProductosOcultarCodigo, &v.ProductosOcultarCantidades,
			&v.PuntoDeVentaObligatorio, &v.Inactivo, &v.CompraOVenta, &v.FechaDesde, &v.FechaHasta,
			&v.MailTemplate, &v.Emisor)

	if err != nil {
		return comps.Comp{}, deferror.DB(err, "querying/scanning")
	}
	return v, nil
}

func readOneQuery() string {
	return `
SELECT 
	id, 
	comitente,
	empresa, 
	tipo, 
	nombre, 
	detalle, 
	punto_de_venta, 
	tipo_emision, 
	tipo_numeracion,
	tipo_anulacion, 
	ultimo_numero_usado, 
	imprime_datos_cajero, 
	calcula_iva, 
	discrimina_iva,
	letra, 
	va_al_libro_de_iva, 
	libro_iva, 
	comprobante_fijo,
	originales_aceptados, 
	margen_izq, 
	margen_der, 
	margen_sup, 
	margen_inf, 
	altura_cabecera,
	altura_persona, 
	altura_pie, 
	imprime_logo, 
	imprime_bordes, 
	imprime_nombre_usuario, 
	imprime_ts, 
	created_at, 
	updated_at, 
	formato_numeracion, 
	productos_nombre_impresion,
	productos_ocultar_codigo, 
	productos_ocultar_cantidades, 
	punto_de_venta_obligatorio, 
	inactivo, 
	compra_o_venta, 
	fecha_desde, 
	fecha_hasta,
	mail_template,
	emisor
FROM comps
WHERE comitente=$1 AND id=$2;`

}

func ReadMany(ctx context.Context, conn *pgxpool.Pool, req comps.ReadManyReq) (out []comps.Comp, err error) {
	out = []comps.Comp{}

	ww := []string{"comitente = $1"}
	pp := []interface{}{req.Comitente}

	// Empresa
	if req.Empresa != 0 {
		pp = append(pp, req.Empresa)
		ww = append(ww, fmt.Sprintf("empresa = $%v", len(pp)))
	}

	// Inactivos
	if !req.IncluirInactivos {
		ww = append(ww, "inactivo = false")
	}

	// IDs
	if len(req.IDs) > 0 {
		ww = append(ww, fmt.Sprintf("id IN %v", req.IDs.ParaClausulaIn()))
	}

	// Tipo
	if req.Tipo != "" {
		pp = append(pp, req.Tipo)
		ww = append(ww, fmt.Sprintf("tipo = $%v", len(pp)))
	}

	where := "WHERE " + strings.Join(ww, " AND ")

	query := fmt.Sprintf(`
SELECT 
	id, 
	comitente,
	empresa, 
	tipo, 
	nombre, 
	detalle, 
	punto_de_venta, 
	tipo_emision, 
	tipo_numeracion,
	tipo_anulacion, 
	ultimo_numero_usado, 
	imprime_datos_cajero, 
	calcula_iva, 
	discrimina_iva,
	letra, 
	va_al_libro_de_iva, 
	libro_iva, 
	comprobante_fijo,
	originales_aceptados, 
	margen_izq, 
	margen_der, 
	margen_sup, 
	margen_inf, 
	altura_cabecera,
	altura_persona, 
	altura_pie, 
	imprime_logo, 
	imprime_bordes, 
	imprime_nombre_usuario, 
	imprime_ts, 
	created_at, 
	updated_at, 
	formato_numeracion, 
	productos_nombre_impresion,
	productos_ocultar_codigo, 
	productos_ocultar_cantidades, 
	punto_de_venta_obligatorio, 
	inactivo, 
	compra_o_venta, 
	fecha_desde, 
	fecha_hasta,
	emisor
FROM comps
%v
ORDER BY nombre;`, where)

	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		v := comps.Comp{}
		err = rows.Scan(
			&v.ID,
			&v.Comitente,
			&v.Empresa,
			&v.Tipo,
			&v.Nombre,
			&v.Detalle,
			&v.PuntoDeVenta,
			&v.TipoEmision,
			&v.TipoNumeracion,
			&v.TipoAnulacion,
			&v.UltimoNumeroUsado,
			&v.ImprimeDatosCajero,
			&v.CalculaIVA,
			&v.DiscriminaIVA,
			&v.Letra,
			&v.VaAlLibroDeIVA,
			&v.LibroIVA,
			&v.ComprobanteFijo,
			&v.OriginalesAceptados,
			&v.MargenIzq,
			&v.MargenDer,
			&v.MargenSup,
			&v.MargenInf,
			&v.AlturaCabecera,
			&v.AlturaPersona,
			&v.AlturaPie,
			&v.ImprimeLogo,
			&v.ImprimeBordes,
			&v.ImprimeNombreUsuario,
			&v.ImprimeTS,
			&v.CreatedAt,
			&v.UpdatedAt,
			&v.FormatoNumeracion,
			&v.ProductosNombreImpresion,
			&v.ProductosOcultarCodigo,
			&v.ProductosOcultarCantidades,
			&v.PuntoDeVentaObligatorio,
			&v.Inactivo,
			&v.CompraOVenta,
			&v.FechaDesde,
			&v.FechaHasta,
			&v.Emisor,
		)
		if err != nil {
			return out, deferror.DB(err, "escaneando")
		}
		out = append(out, v)
	}
	return out, nil
	// TODO: Borrar de la tabla exige comprobante original

}

func Lookup(ctx context.Context, conn *pgxpool.Pool, comitente int) (out []comps.Lookup, err error) {

	// Valido
	if comitente == 0 {
		return out, deferror.Validation("no se ingresó comitente")
	}

	// Busco
	query := `SELECT id, empresa, nombre, punto_de_venta FROM comps WHERE comitente=$1 ORDER BY nombre`
	rows, err := conn.Query(ctx, query, comitente)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		v := comps.Lookup{}
		err = rows.Scan(&v.ID, &v.Empresa, &v.Nombre, &v.PuntoDeVenta)
		if err != nil {
			return out, deferror.DB(err, "scanning")
		}
		out = append(out, v)
	}
	return

}

func Update(ctx context.Context, conn *pgxpool.Pool, req comps.Comp) (err error) {

	query := `
UPDATE comps 
SET 
	empresa=$3, 
	tipo=$4, 
	nombre=$5, 
	detalle=$6, 
	punto_de_venta=$7, 
	tipo_emision=$8, 
	tipo_numeracion=$9,
	tipo_anulacion=$10, 
	ultimo_numero_usado=$11, 
	imprime_datos_cajero=$12, 
	calcula_iva=$13, 
	discrimina_iva=$14,
	letra=$15, 
	va_al_libro_de_iva=$16, 
	libro_iva=$17, 
	comprobante_fijo=$18,
	originales_aceptados=$19, 
	margen_izq=$20, 
	margen_der=$21, 
	margen_sup=$22, 
	margen_inf=$23, 
	altura_cabecera=$24,
	altura_persona=$25, 
	altura_pie=$26, 
	imprime_logo=$27, 
	imprime_bordes=$28, 
	imprime_nombre_usuario=$29, 
	imprime_ts=$30, 
	updated_at=$31, 
	formato_numeracion=$32, 
	productos_nombre_impresion=$33,
	productos_ocultar_codigo=$34, 
	productos_ocultar_cantidades=$35, 
	punto_de_venta_obligatorio=$36, 
	inactivo=$37, 
	compra_o_venta=$38, 
	fecha_desde=$39, 
	fecha_hasta=$40,
	mail_template=$41,
	emisor=$42
WHERE comitente=$1 AND id=$2;`
	_, err = conn.Exec(ctx, query,
		req.Comitente, req.ID,
		req.Empresa,
		req.Tipo,
		req.Nombre,
		req.Detalle,
		req.PuntoDeVenta,
		req.TipoEmision,
		req.TipoNumeracion,
		req.TipoAnulacion,
		req.UltimoNumeroUsado,
		req.ImprimeDatosCajero,
		req.CalculaIVA,
		req.DiscriminaIVA,
		req.Letra,
		req.VaAlLibroDeIVA,
		req.LibroIVA,
		req.ComprobanteFijo,
		req.OriginalesAceptados,
		req.MargenIzq,
		req.MargenDer,
		req.MargenSup,
		req.MargenInf,
		req.AlturaCabecera,
		req.AlturaPersona,
		req.AlturaPie,
		req.ImprimeLogo,
		req.ImprimeBordes,
		req.ImprimeNombreUsuario,
		req.ImprimeTS,
		time.Now(),
		req.FormatoNumeracion,
		req.ProductosNombreImpresion,
		req.ProductosOcultarCodigo,
		req.ProductosOcultarCantidades,
		req.PuntoDeVentaObligatorio,
		req.Inactivo,
		req.CompraOVenta,
		req.FechaDesde,
		req.FechaHasta,
		req.MailTemplate,
		req.Emisor)
	if err != nil {
		return deferror.DB(err, "updating")
	}
	return
}

func Delete(ctx context.Context, conn *pgxpool.Pool, req comps.ReadOneReq) (err error) {

	q := `SELECT comp_id FROM ops WHERE comitente=$1 AND comp_id=$2 LIMIT 1`
	rows, err := conn.Query(ctx, q, req.Comitente, req.ID)
	if err != nil {
		return errors.Wrap(err, "verificando si el comprobante estaba usado en la contabilidad")
	}
	defer rows.Close()
	for rows.Next() {
		return deferror.Validation("no se puede borrar el comprobante porque ya está usado en la contabilidad")
	}

	query := "DELETE FROM comps WHERE comitente=$1 AND id=$2;"
	res, err := conn.Exec(ctx, query, req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "borrando")
	}
	if res.RowsAffected() == 0 {
		return errors.Errorf("no se borró ningún registro")
	}
	return
}

//// Es para una vista previa.
//func UltimosNumeros(ctx context.Context, conn *pgxpool.Pool, ids tipos.GrupoInts) (out map[int]int, err error) {

//tx, err := conn.Begin(ctx)
//if err != nil {
//return out, errors.Wrap(err, "iniciando tx")
//}
//defer tx.Rollback(ctx)

//return UltimosNumerosTx(ctx, tx, ids)
//}

// Para aquellos comprobantes que tienen numeración correlativa
// devuelve un map con los últimos valores registrados.
// Si el compID no esta en el key del map, significa que no
// corresponde numerarlo.
func UltimosNumeros(ctx context.Context, tx pgx.Tx, comitente int, ids tipos.GrupoInts) (out map[int]int, err error) {

	q := fmt.Sprintf(
		`SELECT id, ultimo_numero_usado
FROM comps
WHERE id IN %v AND
tipo_numeracion = $1
FOR UPDATE`,
		ids.ParaClausulaIn())
	rows, err := tx.Query(ctx, q, comps.Consecutiva)
	if err != nil {
		return out, errors.Wrap(err, "buscando últimos números")
	}
	defer rows.Close()

	out = map[int]int{}

	// Scanneo
	for rows.Next() {
		id := 0
		var ultimo *int
		err = rows.Scan(&id, &ultimo)
		if err != nil {
			return out, errors.Wrap(err, "escaneando ultimos comps")
		}
		if ultimo == nil {
			continue
		}
		out[id] = *ultimo
	}

	return
}

// Vals contiene un map con CompID: ultimoUsado
func AvanzarNumeradores(ctx context.Context, tx pgx.Tx, comitente int, vals map[int]int) (err error) {

	if comitente == 0 {
		return errors.Errorf("no se ingresó comitente")
	}
	for k, v := range vals {
		_, err := tx.Exec(ctx, `
UPDATE comps
SET ultimo_numero_usado = $3
WHERE id = $1 AND comitente = $2
;`,
			k, comitente, v)
		if err != nil {
			return deferror.DB(err, "actualizando numerador")
		}
	}

	return
}
