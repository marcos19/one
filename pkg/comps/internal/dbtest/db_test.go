package dbtest

import (
	"context"
	"testing"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/comps/internal/db"
	"bitbucket.org/marcos19/one/pkg/database"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/crosslogic/fecha"
	"github.com/davecgh/go-spew/spew"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDB(t *testing.T) {
	ctx := context.Background()

	cn := database.TestDBConnectionString()

	cfg, err := pgxpool.ParseConfig(cn)
	require.Nil(t, err)
	conn, err := pgxpool.ConnectConfig(context.Background(), cfg)
	require.Nil(t, err)

	// Preparo base de datos
	err = Prepare(ctx, conn, "comps")
	require.Nil(t, err)

	id := 0
	t.Run("CREATE", func(t *testing.T) {
		id, err = db.Create(ctx, conn, compValido())
		require.Nil(t, err)
		assert.NotEqual(t, 0, id)
	})

	t.Run("READ", func(t *testing.T) {
		expected := compValido()
		expected.ID = id
		e, err := db.ReadOne(ctx, conn, comps.ReadOneReq{
			Comitente: 1,
			ID:        id,
		})
		require.Nil(t, err)
		assert.NotEqual(t, 0, e.ID)
		assert.NotNil(t, 0, e.CreatedAt)
		assert.Nil(t, e.UpdatedAt)
		e.CreatedAt = nil
		e.ID = id
		assert.Equal(t, expected, e)
	})
	t.Run("LOOKUP", func(t *testing.T) {
		val := compValido()
		expected := comps.Lookup{
			ID:           id,
			Empresa:      val.Empresa,
			Nombre:       val.Nombre,
			PuntoDeVenta: val.PuntoDeVenta,
		}
		expected.ID = id
		got, err := db.Lookup(ctx, conn, val.Comitente)
		require.Nil(t, err)
		require.Len(t, got, 1)
		assert.Equal(t, expected.ID, got[0].ID)
		assert.Equal(t, expected.Nombre, got[0].Nombre)
		assert.Equal(t, expected.Empresa, got[0].Empresa)
		assert.Equal(t, expected.PuntoDeVenta, got[0].PuntoDeVenta)
	})

	t.Run("UPDATE", func(t *testing.T) {
		expected := compValidoModificado()
		expected.ID = id
		err = db.Update(ctx, conn, expected)
		require.Nil(t, err)

		leido, err := db.ReadOne(ctx, conn, comps.ReadOneReq{
			Comitente: 1,
			ID:        id,
		})
		require.Nil(t, err)
		assert.NotNil(t, leido.UpdatedAt)
		assert.NotNil(t, leido.CreatedAt)
		leido.CreatedAt = nil
		leido.UpdatedAt = nil
		assert.Equal(t, expected, leido)
	})
	t.Run("DELETE sin partida", func(t *testing.T) {
		err := db.Delete(ctx, conn, comps.ReadOneReq{
			Comitente: 1,
			ID:        id,
		})
		require.Nil(t, err)

		_, err = db.ReadOne(ctx, conn, comps.ReadOneReq{
			Comitente: 1,
			ID:        id,
		})
		require.NotNil(t, err)
	})

	t.Run("DELETE con partida", func(t *testing.T) {

		// Creo nueva
		idConMov, err := db.Create(ctx, conn, compValido())
		require.Nil(t, err)
		assert.NotEqual(t, 0, id)

		// Borro partidas anteriores
		_, err = conn.Exec(ctx, "DELETE FROM ops;")
		require.Nil(t, err)

		// Le agrego movimiento
		_, err = conn.Exec(ctx, "INSERT INTO ops (id, comitente, empresa, comp_id) VALUES (1,1,1,$1)", idConMov)
		require.Nil(t, err)

		// No debe dejar borrar
		err = db.Delete(ctx, conn, comps.ReadOneReq{
			Comitente: 1,
			ID:        idConMov,
		})
		require.NotNil(t, err)

		_, err = db.ReadOne(ctx, conn, comps.ReadOneReq{
			Comitente: 1,
			ID:        idConMov,
		})

		// Tiene que seguir estando
		require.Nil(t, err)

	})
}

func compValido() comps.Comp {
	pv := 1
	desde := fecha.Fecha(20220101)
	hasta := fecha.Fecha(20230101)
	verdadero := true
	return comps.Comp{
		Comitente:                  1,
		Empresa:                    1,
		Tipo:                       "Tipo",
		Nombre:                     "Nombre",
		Detalle:                    "Detalle",
		PuntoDeVenta:               &pv,
		TipoEmision:                "TipoEmision",
		TipoNumeracion:             "TipoNumeracion",
		TipoAnulacion:              "TipoAnulacion",
		UltimoNumeroUsado:          1,
		Emisor:                     comps.EmisorPropiaEmpresa,
		ImprimeDatosCajero:         true,
		CalculaIVA:                 true,
		DiscriminaIVA:              true,
		Letra:                      "Letra",
		VaAlLibroDeIVA:             true,
		LibroIVA:                   "LibroIVA",
		ComprobanteFijo:            true,
		OriginalesAceptados:        tipos.GrupoInts{1, 2, 3},
		MargenIzq:                  1,
		MargenDer:                  1,
		MargenSup:                  1,
		MargenInf:                  1,
		AlturaCabecera:             1,
		AlturaPersona:              1,
		AlturaPie:                  1,
		ImprimeLogo:                &verdadero,
		ImprimeBordes:              &verdadero,
		ImprimeNombreUsuario:       &verdadero,
		ImprimeTS:                  &verdadero,
		FormatoNumeracion:          "sss",
		ProductosNombreImpresion:   true,
		ProductosOcultarCodigo:     true,
		ProductosOcultarCantidades: true,
		PuntoDeVentaObligatorio:    "PuntoDeVentaObligatorio",
		Inactivo:                   true,
		CompraOVenta:               "CompraOVenta",
		FechaDesde:                 &desde,
		FechaHasta:                 &hasta,
	}
}

func compValidoModificado() comps.Comp {
	pv := 1
	desde := fecha.Fecha(20220101)
	hasta := fecha.Fecha(20230101)
	verdadero := false
	return comps.Comp{
		Comitente:                  1,
		Empresa:                    2,
		Tipo:                       "Tipo2",
		Nombre:                     "Nombre2",
		Detalle:                    "Detalle2",
		PuntoDeVenta:               &pv,
		TipoEmision:                "TipoEmision2",
		TipoNumeracion:             "TipoNumeracion2",
		TipoAnulacion:              "TipoAnulacion2",
		UltimoNumeroUsado:          2,
		Emisor:                     comps.EmisorTercero,
		ImprimeDatosCajero:         false,
		CalculaIVA:                 false,
		DiscriminaIVA:              false,
		Letra:                      "Letra2",
		VaAlLibroDeIVA:             false,
		LibroIVA:                   "LibroIVA2",
		ComprobanteFijo:            false,
		OriginalesAceptados:        tipos.GrupoInts{34, 4},
		MargenIzq:                  2,
		MargenDer:                  2,
		MargenSup:                  2,
		MargenInf:                  2,
		AlturaCabecera:             2,
		AlturaPersona:              2,
		AlturaPie:                  2,
		ImprimeLogo:                &verdadero,
		ImprimeBordes:              &verdadero,
		ImprimeNombreUsuario:       &verdadero,
		ImprimeTS:                  &verdadero,
		FormatoNumeracion:          "sss2",
		ProductosNombreImpresion:   false,
		ProductosOcultarCodigo:     false,
		ProductosOcultarCantidades: false,
		PuntoDeVentaObligatorio:    "PuntoDeVentaObligatorio2",
		Inactivo:                   false,
		CompraOVenta:               "CompraOVenta2",
		FechaDesde:                 &hasta,
		FechaHasta:                 &desde,
	}
}

func TestNumerador(t *testing.T) {

	// Conecto
	ctx := context.Background()
	cn := database.TestDBConnectionString()
	cfg, err := pgxpool.ParseConfig(cn)
	require.Nil(t, err)
	conn, err := pgxpool.ConnectConfig(context.Background(), cfg)
	require.Nil(t, err)

	// Preparo base de datos
	err = Prepare(ctx, conn, "numerador")
	require.Nil(t, err)

	c := comps.Comp{
		Comitente:         1,
		Empresa:           1,
		Nombre:            "Para testear numerador",
		TipoNumeracion:    comps.Consecutiva,
		UltimoNumeroUsado: 10,
	}

	id, err := db.Create(ctx, conn, c)
	require.Nil(t, err)
	c.ID = id

	// Busco ultimos
	ids := []int{id, 897}
	tx, err := conn.Begin(ctx)
	require.Nil(t, err)

	ultimos, err := db.UltimosNumeros(ctx, tx, c.Comitente, ids)
	require.Nil(t, err)

	// Debería devolver uno solo
	require.Len(t, ultimos, 1)
	assert.Equal(t, 10, ultimos[id], "ultimos: %v", spew.Sdump(ultimos))

	ultimos[id] += 1

	// Avando numerador
	err = db.AvanzarNumeradores(ctx, tx, 1, ultimos)
	require.Nil(t, err)

	err = tx.Commit(ctx)
	require.Nil(t, err)

	res, err := db.ReadOne(ctx, conn, comps.ReadOneReq{Comitente: 1, ID: id})
	require.Nil(t, err)
	assert.Equal(t, 11, res.UltimoNumeroUsado)
}
