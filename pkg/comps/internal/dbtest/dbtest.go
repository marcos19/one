package dbtest

import (
	"context"
	"fmt"

	"bitbucket.org/marcos19/one/pkg/migrations/ddl"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

func Prepare(ctx context.Context, conn *pgxpool.Pool, dbName string) error {

	{ // Borro base de datos
		q := fmt.Sprintf(`DROP DATABASE IF EXISTS test_%v;`, dbName)
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "borro base de datos")
		}
	}
	{ // Creo base de datos
		q := fmt.Sprintf(`CREATE DATABASE test_%v; USE test_%v;`, dbName, dbName)
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando base de datos")
		}
	}

	{ // Tabla comitentes
		q := `CREATE TABLE IF NOT EXISTS comitentes (ID int PRIMARY KEY);`
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla comitentes")
		}

		_, err = conn.Exec(ctx, "UPSERT INTO comitentes (id) VALUES (1);")
		if err != nil {
			return errors.Wrap(err, "insertando comitente")
		}
	}
	{ // Tabla empresas
		q := `CREATE TABLE IF NOT EXISTS empresas (ID int PRIMARY KEY);`
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla empresas")
		}

		_, err = conn.Exec(ctx, "UPSERT INTO empresas (id) VALUES (1), (2);")
		if err != nil {
			return errors.Wrap(err, "insertando empresa")
		}
	}

	{ // Creo table de comps
		q := ddl.Comps().SQL
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla comps")
		}
	}
	{
		// Partidas (la necesito para borrar
		q := `CREATE TABLE IF NOT EXISTS ops(
		ID int PRIMARY KEY, 
		comitente INT REFERENCES comitentes, 
		empresa INT NOT NULL REFERENCES empresas,
		comp_id INT NOT NULL)`
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla partidas")
		}
	}

	return nil
}
