package handler

import (
	"context"
	"fmt"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/comps/internal/db"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/dgraph-io/ristretto"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/rs/zerolog/log"
)

// Handler es el encargado de recibir los http requests y emitir los responses.
type Handler struct {
	conn  *pgxpool.Pool
	cache *ristretto.Cache
	// eventbus EventBus
}

// NewHandler inicializa un handler
func NewHandler(conn *pgxpool.Pool, cache *ristretto.Cache) (h *Handler, err error) {

	// Valido
	if conn == nil {
		return h, errors.New("DB era nil")
	}
	if cache == nil {
		return h, errors.New("cache era nil")
	}

	// Creo handler
	h = &Handler{
		conn:  conn,
		cache: cache,
	}

	return
}

type EventBus interface {
	CambióComp(crud string, comp comps.Comp)
}

func (h *Handler) Update(ctx context.Context, req comps.Comp) (err error) {

	// Valido
	err = validar(&req)
	if err != nil {
		return err
	}

	// Borro del cache
	h.cache.Del(hashInt(comps.ReadOneReq{Comitente: req.Comitente, ID: req.ID}))

	// Modifico
	err = db.Update(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "modificando comp")
	}

	return
}

func (h *Handler) Create(ctx context.Context, req comps.Comp) (id int, err error) {

	// Valido
	err = validar(&req)
	if err != nil {
		return id, err
	}

	// Inserto
	id, err = db.Create(ctx, h.conn, req)
	if err != nil {
		return id, deferror.DB(err, "insertando registro")
	}

	return
}

// HandleOne devuelve el comprobante solicitado.
func (h *Handler) ReadOne(ctx context.Context, req comps.ReadOneReq) (out comps.Comp, err error) {

	if req.ID == 0 {
		return out, deferror.Validation("no se ingresó id de comprobante")
	}
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}

	log.Trace().Msgf("Buscando comp %v", req)
	// Busco en cache
	val, estaba := h.cache.Get(hashInt(req))
	if estaba {
		out, ok := val.(comps.Comp)
		if ok && out.Comitente == req.Comitente {
			log.Debug().Str("tipo", "comp").Msgf("Devolviendo valor de cache")
			return out, nil
		}
	}

	log.Trace().Str("tipo", "comp").Msgf("No estaba en el cache %v", req)
	// No estaba en cache
	out, err = db.ReadOne(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DBf(err, "buscando comp por ID=%v", req.ID)
	}
	log.Debug().Str("tipo", "comp").Msgf("Se buscó en DB %v", req)

	// Fijo valor en cache
	h.cache.Set(hashInt(req), out, 0)

	return

}

// HandleOne devuelve el comprobante solicitado.
func (h *Handler) ReadOneTx(ctx context.Context, tx pgx.Tx, req comps.ReadOneReq) (out comps.Comp, err error) {

	if req.ID == 0 {
		return out, deferror.Validation("no se ingresó id de comprobante")
	}
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}

	log.Trace().Msgf("Buscando comp %v", req)
	// Busco en cache
	val, estaba := h.cache.Get(hashInt(req))
	if estaba {
		out, ok := val.(comps.Comp)
		if ok && out.Comitente == req.Comitente {
			log.Debug().Str("tipo", "comp").Msgf("Devolviendo valor de cache")
			return out, nil
		}
	}

	log.Trace().Str("tipo", "comp").Msgf("No estaba en el cache %v", req)
	// No estaba en cache
	out, err = db.ReadOneTx(ctx, tx, req)
	if err != nil {
		return out, deferror.DBf(err, "buscando comp por ID=%v", req.ID)
	}
	log.Debug().Str("tipo", "comp").Msgf("Se buscó en DB %v", req)

	// Fijo valor en cache
	h.cache.Set(hashInt(req), out, 0)

	return
}

func (h *Handler) ReadMany(ctx context.Context, req comps.ReadManyReq) (out []comps.Comp, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}

	// Busco
	out, err = db.ReadMany(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando comprobantes")
	}

	return
}

func (h *Handler) Delete(ctx context.Context, req comps.ReadOneReq) (err error) {

	if req.Comitente == 0 {
		return deferror.ComitenteIndefinido()
	}
	if req.ID == 0 {
		return errors.Errorf("no se ingresó ID")
	}
	err = db.Delete(ctx, h.conn, req)
	return
}

// Mueve el numerador el número ingresado en 'cantidad'.
// Si es negativo lo retrocede.
// Ademas devuelve el nuevo número
func (h *Handler) AvanzarNumerador(ctx context.Context, tx pgx.Tx, cantidad int, req comps.ReadOneReq) (proximo int, err error) {

	rows, err := tx.Query(ctx, `
		UPDATE comps
		SET ultimo_numero_usado = ultimo_numero_usado + $1
		WHERE id = $2 AND comitente = $3
		RETURNING ultimo_numero_usado
				;`,
		cantidad, req.ID, req.Comitente)

	if err != nil {
		return 0, deferror.DB(err, "actualizando numerador")
	}
	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(&proximo)
		if err != nil {
			return 0, deferror.DB(err, "escaneando row")
		}
	}
	h.CambióNumero(req)

	return
}

func (h *Handler) UltimosNumeros(ctx context.Context, tx pgx.Tx, comitente int, ids []int) (map[int]int, error) {
	return db.UltimosNumeros(ctx, tx, comitente, ids)
}
func (h *Handler) AvanzarNumeradores(ctx context.Context, tx pgx.Tx, comitente int, vals map[int]int) (err error) {

	// Borro todos los cache por las dudas
	for k := range vals {

		key := comps.ReadOneReq{Comitente: comitente, ID: k}
		h.cache.Del(hashInt(key))
	}
	return db.AvanzarNumeradores(ctx, tx, comitente, vals)
}

func (h *Handler) Lookup(ctx context.Context, comitente int) (out []comps.Lookup, err error) {

	// Busco
	out, err = db.Lookup(ctx, h.conn, comitente)
	if err != nil {
		return nil, deferror.DB(err, "buscando comps lookup")
	}

	return
}

func validar(c *comps.Comp) error {
	// Valido
	if c.Empresa == 0 {
		return deferror.Validation("Empresa no puede ser cero")
	}
	if c.Nombre == "" {
		return deferror.Validation("No se definió el nombre del comprobante")
	}
	if c.Emisor == 0 {
		return deferror.Validation("Debe definir quién es el EMISOR del comprobante")
	}
	if c.TipoNumeracion == "" {
		return deferror.Validation("No se definió el tipo de numeración")
	}
	if c.TipoNumeracion == "" {
		return deferror.Validation("No se definió el tipo de anulación")
	}
	if c.VaAlLibroDeIVA && c.LibroIVA == "" {
		return deferror.Validation("Si el comprobante va al libro de IVA, tiene que definir a cual libro corresponde")
	}
	if !c.VaAlLibroDeIVA && c.LibroIVA != "" {
		c.LibroIVA = ""
	}
	if c.ComprobanteFijo {
		if len(c.OriginalesAceptados) != 1 {
			return deferror.Validationf("El comprobante %v-%v se definió como 'Comprobante fijo'. Debe seleccionar un comprobante original", c.ID, c.Nombre)
		}
	}

	return nil
}

// Una vez que se confirma la transacción, el campo ultimo_usado en la tabla
// comps queda bien, pero no el cache.
// Debo llamar a esta función para invalidar el cache.
func (h *Handler) CambióNumero(req comps.ReadOneReq) {
	h.cache.Del(hashInt(req))
}

func hashInt(req comps.ReadOneReq) string {
	return fmt.Sprintf("%v/%v/%v", "Comp", req.Comitente, req.ID)
}
