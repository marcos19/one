package comps

import (
	"time"

	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
)

// Comp es el comprobante. Debería tener uno por cada punto de venta fiscal y
// (factura) por cada punto de venta no fiscal (rendiciones de caja).
type Comp struct {
	ID        int `json:",string"`
	Comitente int
	Empresa   int `json:",string"`

	// factura, remito, rendicionCaja, recibo, etc...
	Tipo string

	// Cada comprobante está diseñado para un programa en particular.
	Nombre  string // Factura Remito
	Detalle string

	// Si es nil significa que no tiene uno definido (comprobantes de compra)
	PuntoDeVenta            *int
	PuntoDeVentaObligatorio string

	// Si continúa el anterior, lo trae de AFIP, o no hace nada
	// TODO
	TipoEmision       TipoEmision
	TipoNumeracion    TipoNumeracion
	TipoAnulacion     TipoAnulacion
	FormatoNumeracion string // Por defecto: PPPP-NNNNNNNN

	UltimoNumeroUsado int

	// Si se trata de un comprobante que genera la propia empresa
	// o si lo emitió un terecero y la empresa lo está cargando.
	Emisor Emisor `json:",string"`

	// Si es true, va a generar las partidas de IVA
	CalculaIVA bool
	// Si es true en la impresión lo muestra todo junto.
	DiscriminaIVA      bool
	ImprimeDatosCajero bool

	Letra string

	// Si es true significa que es un comprobante fiscal.
	// Exige condición fiscal, CUIT y otras cosas que pide AFIP.
	// También le pega los campos de ConceptoIVA y Alicuota.
	// Si es false y tiene tilde en calcula IVA, genera partidas de IVA
	// pero no pega conceptos.
	VaAlLibroDeIVA      bool
	CompraOVenta        string
	LibroIVA            string // Compras o Ventas
	ComprobanteFijo     bool   // Si es true, automáticamente le pega el código de original
	OriginalesAceptados tipos.GrupoInts

	// Impresión

	// Si es true, cuando imprima utilizará el nombre de impresión
	ProductosNombreImpresion   bool
	ProductosOcultarCodigo     bool
	ProductosOcultarCantidades bool

	MargenIzq dec.D2
	MargenDer dec.D2
	MargenSup dec.D2
	MargenInf dec.D2

	AlturaCabecera dec.D2
	AlturaPersona  dec.D2
	AlturaPie      dec.D2

	ImprimeLogo          *bool
	ImprimeBordes        *bool
	ImprimeNombreUsuario *bool
	ImprimeTS            *bool

	Inactivo   bool
	FechaDesde *fecha.Fecha
	FechaHasta *fecha.Fecha

	MailTemplate string

	CreatedAt *time.Time
	UpdatedAt *time.Time
}

type Lookup struct {
	ID           int `json:",string"`
	Empresa      int `json:",string"`
	Nombre       string
	PuntoDeVenta *int
}
type TipoObligatoriedad string

const (
	ObligatorioSi       = "Sí"
	ObligatorioNo       = "No"
	ObligatorioOriginal = "Original"
)

type TipoEmision string

const (
	// Factura electrónica común
	TipoEmisionWSFEV1 = "wsfev1"
)

// TipoAnulacion lista los tipos de metodología de anulación que se pueden usar
// en cada comprobante.
type TipoAnulacion string

const (
	// AnulacionBorrando signifca que cuando se le pone borrar,
	// directamente se borra de la base de datos.
	AnulacionBorrando TipoAnulacion = "Se borra"

	// AnulacionMismoComprobante significa que el proceso va a hacer otro
	// comprobante con el mismo tipo y número pero con el detalle "Anulación"
	// El proceso además le pega a ambos comprobantes la señal de "Anulado".
	// Tendrá el asiento inverso. Cuando no se quiere que el comprobante se
	// muestre en los listados, habrá que configurar el listado para "Ocultar Anulados"
	AnulacionMismoComprobante = "Mismo comprobante"

	// AnulacionOtroComprobante significa que para anular el comprobante hay
	// que hacer otro (como en el caso de la nota de crédito).
	AnulacionOtroComprobante = "Otro comprobante"
)

type TipoNumeracion string

// Define el comportamiento que debe tomarse al grabar una operación
// con respecto a su numeración.
const (
	Consecutiva = TipoNumeracion("Consecutiva")
	Externa     = TipoNumeracion("Externa")
)

type Emisor int

const (
	EmisorPropiaEmpresa = Emisor(1)
	EmisorTercero       = Emisor(2)
)

// // ProximoNumero se fija cual es el número del último comprobante registrado,
// // y devuelve el número que sigue.
// func ProximoNumero(tx pgx.Tx, comp, comitente int) (proximo int, err error) {
// 	query := "SELECT ultimo_numero_usado FROM comps WHERE id = $1 AND comitente = $2 FOR UPDATE"
// 	err = tx.QueryRow(context.Background(), query, comp, comitente).Scan(&proximo)
// 	if err != nil {
// 		return proximo, deferror.DB(err, "")
// 	}
// 	return proximo + 1, nil
// }

// // ProximoNumero se fija cual es el número del último comprobante registrado,
// // y devuelve el número que sigue.
// func ProximoNumeroSinBloqueo(tx pgx.Tx, comp, comitente int) (proximo int, err error) {
// 	query := "SELECT ultimo_numero_usado FROM comps WHERE id = $1 AND comitente = $2"
// 	err = tx.QueryRow(context.Background(), query, comp, comitente).Scan(&proximo)
// 	if err != nil {
// 		return proximo, deferror.DB(err, "")
// 	}
// 	return proximo + 1, nil
// }
