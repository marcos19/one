package comps

// Categoria la uso para filtrar a la hora de seleccionar.
type Categoria struct {
	ID     string
	Nombre string
}

const (
	Aplicacion    = "aplicacion"
	Diferimiento  = "diferimiento"
	Factura       = "factura"
	Minuta        = "minuta"
	Recibo        = "recibo"
	Remito        = "remito"
	RendicionCaja = "rendicionCaja"
)

func Categorias() []Categoria {
	return []Categoria{
		{ID: Aplicacion, Nombre: "Aplicación"},
		{ID: Diferimiento, Nombre: "Diferimiento"},
		{ID: Factura, Nombre: "Factura"},
		{ID: Minuta, Nombre: "Minuta"},
		{ID: Recibo, Nombre: "Recibo"},
		{ID: Remito, Nombre: "Remito"},
		{ID: RendicionCaja, Nombre: "Rendición de caja"},
	}
}
