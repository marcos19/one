package comps

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/jackc/pgx/v4"
)

type ReaderOne interface {
	ReadOne(context.Context, ReadOneReq) (Comp, error)
}

// Por llevar la numeración dentro de la misma tabla de comprobantes
type ReaderOneTx interface {
	ReadOneTx(context.Context, pgx.Tx, ReadOneReq) (Comp, error)
}
type ReaderMany interface {
	ReadMany(context.Context, ReadManyReq) ([]Comp, error)
}
type Creater interface {
	Create(context.Context, Comp) (int, error)
}
type Updater interface {
	Update(context.Context, Comp) error
}
type Deleter interface {
	Delete(context.Context, ReadOneReq) error
}
type LookerUp interface {
	Lookup(ctx context.Context, comitente int) ([]Lookup, error)
}

// El int es la cantidad que se desea avanzar
type Numerador interface {

	// Devuelve para cada CompID, cuál es el último usado.
	UltimosNumeros(context.Context, pgx.Tx, int, []int) (map[int]int, error)
	AvanzarNumeradores(c context.Context, tx pgx.Tx, comitente int, nn map[int]int) error
	CambióNumero(ReadOneReq)
}

type ReadManyReq struct {
	Empresa          int `json:",string"`
	IDs              tipos.GrupoInts
	IncluirInactivos bool
	Comitente        int
	Tipo             string
}

type ReadOneReq struct {
	ID        int `json:",string"`
	Comitente int
}
