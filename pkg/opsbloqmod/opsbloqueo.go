package opsbloqmod

// Cada pkg transaccional va a definir en su config a qué
// módulo pertenece.
// Al momento de registrar una operación (create, update o delete),
// va a controlar que la fecha caiga dentro del rango establecido.
type Bloqueo struct {
	Comitente         int
	Empresa           int `json:",string"`
	Modulo            string
	DiasHaciaAdelante int
	DiasHaciaAtras    int
}
