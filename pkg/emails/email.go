package emails

import (
	"database/sql/driver"
	"encoding/json"

	"github.com/cockroachdb/errors"
)

// Email es cada uno de los emails que van en el campo emails
type Email struct {
	Descripcion             string `json:",omitempty"`
	Email                   string
	NoEnviarAutomaticamente bool `json:",omitempty"`
}

// Emails representa un grupo de Emails.
type Emails []Email

// Value cumple con la interface SQL
func (p Emails) Value() (driver.Value, error) {
	return json.Marshal(p)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (p *Emails) Scan(src interface{}) error {
	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New("Type assertion error .([]byte)")
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, p)
	if err != nil {
		return errors.Wrap(err, "Unmarshalling")
	}

	return nil
}
