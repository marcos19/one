package cuentasasociadas

import (
	"context"
	"database/sql/driver"
	"encoding/json"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Cuenta struct {
	Comitente int       `json:"-"`
	Cuenta    int       `json:",string"`
	Persona   uuid.UUID `json:"-"`
	Sucursal  int       `json:"-"`
}

// PorPersona devuelve las cuentas asociadas que tiene una persona (no sus sucursales)
func PorPersona(ctx context.Context, conn *pgxpool.Pool, comitente int, persona uuid.UUID) (out Cuentas, err error) {
	out = Cuentas{}

	// Valido
	if comitente == 0 {
		return out, errors.Errorf("no se ingresó ID de comitente")
	}
	if persona == uuid.Nil {
		return out, errors.Errorf("no se ingresó ID de persona")
	}

	// Busco
	query := `SELECT comitente, cuenta, persona, sucursal FROM cuentas_asociadas WHERE comitente = $1 AND persona = $2 AND sucursal = 0`
	rows, err := conn.Query(ctx, query, comitente, persona)
	if err != nil {
		return out, deferror.DB(err, "querying cuentas asociadas")
	}
	defer rows.Close()

	for rows.Next() {
		c := Cuenta{}
		err = rows.Scan(&c.Comitente, &c.Cuenta, &c.Persona, &c.Sucursal)
		if err != nil {
			return out, deferror.DB(err, "escaneando")
		}
		out = append(out, c)
	}

	return
}

// Persiste una cuenta asociada con la transacción ingresada. No valida nada.
func Insert(ctx context.Context, tx pgx.Tx, req Cuenta) (err error) {
	if req.Comitente == 0 {
		return errors.Errorf("no se ingresó ID de comitente")
	}
	if req.Persona == uuid.Nil {
		return errors.Errorf("no se ingresó ID de persona")
	}
	if req.Cuenta == 0 {
		return errors.Errorf("no se ingresó ID de cuenta")
	}
	query := `INSERT INTO cuentas_asociadas (comitente, cuenta, persona, sucursal) VALUES ($1,$2,$3,$4)`
	_, err = tx.Exec(ctx, query, req.Comitente, req.Cuenta, req.Persona, req.Sucursal)
	if err != nil {
		return deferror.DB(err, "insertando")
	}
	return

}

func (c Cuenta) TableName() string {
	return "cuentas_asociadas"
}

type Cuentas []Cuenta

// Value cumple con la interface SQL
func (p Cuentas) Value() (driver.Value, error) {
	return json.Marshal(p)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (p *Cuentas) Scan(src interface{}) error {
	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.Errorf("Type assertion error, expected: []byte, got: %T", src)
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, p)
	if err != nil {
		return errors.Wrap(err, "Unmarshalling")
	}

	return nil
}

// // CtasAsociadas representa una cuenta contable que está asociada a una persona.
// type CtasAsociadas []int

// // Value cumple con la interface SQL
// func (p CtasAsociadas) Value() (driver.Value, error) {
// 	return json.Marshal(p)
// }

// // Scan implementa la interface SQL. Es para pegar los datos de la base
// // de datos en una struct.
// func (p *CtasAsociadas) Scan(src interface{}) error {
// 	source, ok := src.([]byte)
// 	if !ok {
// 		return errors.New("Type assertion error .([]byte)")
// 	}

// 	err := json.Unmarshal(source, &p)
// 	if err != nil {
// 		return errors.Wrap(err, "Unmarshalling json")
// 	}

// 	return nil
// }
