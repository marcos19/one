package resbancario

import (
	"time"

	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/resbancario/config"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
)

const (
	// Programa sirve para identificar que package persistió una op.
	Programa = "resbancario"
)

// Resumen es la tabla donde se llevan los resúmenes bancarios que se
// levantan del CSV del banco. Agrupa varias ops.
type Resumen struct {
	ID             uuid.UUID
	Comitente      int `json:",string"`
	Empresa        int `json:",string"`
	Config         int `json:",string"`
	Desde          fecha.Fecha
	Hasta          fecha.Fecha
	Persona        uuid.UUID
	PersonaNombre  string
	Sucursal       int `json:",string"`
	SucursalNombre string
	Partidas       PartidasImputadas // Las que se leyeron del CSV
	Imputaciones   config.Imputaciones
	CreatedAt      time.Time
	Usuario        string

	Ops []*ops.Op
}

func (r Resumen) TableName() string {
	return "resumenes_bancarios"
}
