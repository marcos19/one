package resbancario

import (
	"context"
	"encoding/csv"
	"fmt"
	"io"
	"math"
	"sort"
	"strconv"
	"strings"

	"bitbucket.org/marcos19/one/pkg/afip/afipmodels"
	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/iva"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/personas"
	"bitbucket.org/marcos19/one/pkg/resbancario/config"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
)

// ReadCSV toma un archivo CSV descargado de la página del banco y lo
// transforma a Partida, la estructura interna.
func ReadCSV(r io.Reader, config config.CSVConfig) (pp Partidas, err error) {

	reader := csv.NewReader(r)
	if len(config.Comma) == 0 {
		config.Comma = ","
	}
	if len(config.Comma) > 2 {
		return pp, errors.Errorf("el separador no puede tener más de un caracter")
	}
	reader.Comma = []rune(config.Comma)[0]
	reader.LazyQuotes = config.LazyQuotes
	records, err := reader.ReadAll()
	if err != nil {
		return pp, errors.Wrap(err, "leyendo CSV file")
	}

	for i, record := range records {
		if i < config.OmitirRenglones {
			continue
		}
		part := Partida{Item: i + 1}

		// Fecha
		part.Fecha, err = fecha.NewFechaFromLayout(config.FechaLayout, colValue("Fecha", record, config))
		if err != nil {
			return pp, errors.Wrapf(err, "leyendo fecha en registro número %v", i)
		}

		part.Codigo = colValue("Código", record, config)
		part.Detalle = sanitizeString(colValue("Detalle", record, config))
		part.NumeroRef = colValue("Número referencia", record, config)

		_, hayColumnaMonto := config.Columnas["Monto"]
		if hayColumnaMonto {
			str := colValue("Monto", record, config)
			monto, err := sanitizeFloat(config, str)
			if err != nil {
				return pp, errors.Wrapf(err, "en columna '%v' en renglón número %v", "Débito", i)
			}
			if monto > 0 {
				part.Credito = monto
			} else {
				part.Debito = -monto
			}
		}

		if !hayColumnaMonto {
			{ // Débito
				str := colValue("Débito", record, config)
				part.Debito, err = sanitizeFloat(config, str)
				if err != nil {
					return pp, errors.Wrapf(err, "en columna '%v' en renglón número %v", "Débito", i)
				}
			}
			{ // Crédito
				str := colValue("Crédito", record, config)
				part.Credito, err = sanitizeFloat(config, str)
				if err != nil {
					return pp, errors.Wrapf(err, "en columna '%v' en renglón número %v", "Crédito", i)
				}
			}
		}
		{ // Saldo
			str := colValue("Saldo", record, config)
			part.Saldo, err = sanitizeFloat(config, str)
			if err != nil {
				return pp, errors.Wrapf(err, "en columna '%v' en renglón número %v", "Saldo", i)
			}
		}

		pp = append(pp, part)
	}
	return
}

func sanitizeString(str string) string {
	return strings.Trim(str, " ")
}

func sanitizeFloat(c config.CSVConfig, origStr string) (out dec.D2, err error) {

	str := origStr
	if c.SeparadorMiles != "" {
		str = strings.ReplaceAll(str, c.SeparadorMiles, "")
	}
	if c.SeparadorDecimal != "." && c.SeparadorDecimal != "" {
		str = strings.ReplaceAll(str, c.SeparadorDecimal, ".")
	}
	if str == "" {
		str = "0"
	}
	fl, err := strconv.ParseFloat(str, 64)
	if err != nil {
		return out, errors.Wrapf(err, "parseando %v", origStr)
	}
	out = dec.NewD2(fl)
	return
}

// devuelve el valor la columna solicitada
func colValue(field string, record []string, cfg config.CSVConfig) string {
	idx := cfg.Columnas[field] - 1
	if idx < 0 {
		return ""
	}
	return record[idx]

}

// Le pega a cada partida la imputación de acuerdo a lo que establece la config.
func fijarImputaciones(pp Partidas, cfg config.Config) (out PartidasImputadas, err error) {
	if cfg.FiltraPorCodigo() {
		out = fijarImputacionesPorCodigo(pp, cfg.Imputaciones)
	} else {
		out, err = fijarImputacionesPorDescripcion(pp, cfg.Imputaciones)
		if err != nil {
			return out, errors.Wrap(err, "fijando imputación por descripción")
		}
	}
	return
}

// fijarImputaciones genera las partidas imputadas.
func fijarImputacionesPorCodigo(pp Partidas, ii config.Imputaciones) (out []PartidaImputada) {

	// Hago map
	impM := map[string]config.Imputacion{}
	for _, v := range ii {
		impM[v.Codigo] = v
	}

	for _, v := range pp {
		imp := impM[v.Codigo]

		// Si no está definido el código, devuelvo campos en valor cero/.
		o := PartidaImputada{}
		o.Partida = v
		o.Cuenta = imp.Cuenta
		o.IVAConcepto = imp.IVAConcepto
		o.IVAAlicuota = imp.IVAAlicuota
		o.Tipo = imp.Tipo
		out = append(out, o)
	}

	return out
}

// fijarImputaciones genera las partidas imputadas.
func fijarImputacionesPorDescripcion(pp Partidas, ii config.Imputaciones) (out []PartidaImputada, err error) {

	// Hago map
	impM := map[string]config.Imputacion{}
	for _, v := range ii {
		impM[v.Codigo] = v
	}

	for _, v := range pp {
		seEncontróImputacion := false
		found := map[string]string{}
		for _, imp := range ii {
			if !strings.Contains(v.Detalle, imp.Contiene) {
				continue
			}
			prev, estaba := found[v.Detalle]
			if estaba {
				return out, errors.Errorf("El movimiento '%v' fue seleccionado por '%v' y por '%v'. Borre una configuración o hágala más específica", v.Detalle, imp.Contiene, prev)
			}
			o := PartidaImputada{}
			o.Partida = v
			o.Cuenta = imp.Cuenta
			o.IVAConcepto = imp.IVAConcepto
			o.IVAAlicuota = imp.IVAAlicuota
			o.Tipo = imp.Tipo
			out = append(out, o)
			seEncontróImputacion = true
			found[v.Detalle] = imp.Contiene
		}
		if !seEncontróImputacion {
			out = append(out, PartidaImputada{Partida: v})
		}
	}

	return
}

// Crea las ops agrupando las partidas por día.
func (h *Handler) crearOps(
	ctx context.Context,
	pp PartidasImputadas,
	cfg config.Config,
	compID, ctaPat int,
) (oo []*ops.Op, err error) {

	// Determino fechas de ops
	ff := map[fecha.Fecha]PartidasImputadas{}
	for _, v := range pp {
		anterior := ff[v.Fecha]
		anterior = append(anterior, v)
		ff[v.Fecha] = anterior
	}
	fechas := []fecha.Fecha{}
	for k := range ff {
		fechas = append(fechas, k)
	}
	sort.Slice(fechas, func(i, j int) bool {
		return fechas[i] < fechas[j]
	})

	// Traigo comp
	comp, err := h.compReader.ReadOne(ctx, comps.ReadOneReq{Comitente: cfg.Comitente, ID: compID})
	if err != nil {
		return nil, errors.Wrapf(err, "buscando comp ID=%v", compID)
	}
	ncomp := comp.UltimoNumeroUsado

	// Traigo persona
	per, err := h.personas.ReadOne(context.Background(), personas.ReadOneReq{
		Comitente: cfg.Comitente,
		ID:        cfg.Persona,
	})
	if err != nil {
		return nil, errors.Wrap(err, "buscando persona")
	}

	// Traigo cuenta
	cta, err := h.cuentasGetter.ReadOne(ctx, cuentas.ReadOneReq{
		ID:        ctaPat,
		Comitente: cfg.Comitente,
	})
	if err != nil {
		return oo, errors.Wrapf(err, "buscando cuenta '%v' ", ctaPat)
	}

	// Traigo cuenta patrimonial
	for _, v := range fechas {
		op := ops.Op{}
		op.ID, _ = uuid.NewV1()
		op.Programa = Programa
		op.Fecha = v
		op.FechaOriginal = v
		op.Empresa = cfg.Empresa
		op.Comitente = cfg.Comitente
		op.CompID = compID
		op.CompNombre = comp.Nombre
		op.TranDef = cfg.ID
		op.Programa = Programa
		op.Persona = new(uuid.UUID)
		*op.Persona = cfg.Persona
		op.PersonaNombre = per.Nombre
		op.TipoIdentificacion = new(int)
		*op.TipoIdentificacion = per.TipoIdentificacionFiscal()
		op.NIdentificacion = new(int)
		*op.NIdentificacion = int(per.CUIT)
		op.CondicionFiscal = new(afipmodels.CondicionIVA)
		if per.CondicionIVA == nil {
			return oo, deferror.Validation("El banco no tiene definida condición frente a IVA")
		}
		*op.CondicionFiscal = *per.CondicionIVA
		op.Sucursal = new(int)
		*op.Sucursal = cfg.Sucursal

		ncomp++
		op.NComp = ncomp
		op.PuntoDeVenta = *comp.PuntoDeVenta
		op.TotalComp = new(dec.D2)
		op.NCompStr, err = formatComp(op.PuntoDeVenta, ncomp, "PPPP-NNNNNNNN")
		if err != nil {
			return nil, errors.Wrapf(err, "poniendole número a compID=%v, PuntoVenta=%v, Número=%v", compID, op.PuntoDeVenta, op.NComp)
		}

		for _, partida := range ff[v] {

			{ // Partida de patrimonial
				p := ops.Partida{}
				p.ID, _ = uuid.NewV1()
				p.Persona = new(uuid.UUID)
				*p.Persona = cfg.Persona
				p.Sucursal = new(int)
				*p.Sucursal = cfg.Sucursal
				p.Cuenta = ctaPat
				p.Detalle = new(string)
				*p.Detalle = partida.Detalle
				if partida.NumeroRef != "" {
					partida.Detalle += " - " + partida.NumeroRef
				}
				if partida.NumeroRef != "" {
					partida.Detalle += fmt.Sprintf(" (%v)", partida.Codigo)
				}
				if cta.UsaAplicaciones {
					p.PartidaAplicada = new(uuid.UUID)
					*p.PartidaAplicada = p.ID
				}

				switch {
				case partida.Debito != 0:
					p.Monto = -partida.Debito
				case partida.Credito != 0:
					p.Monto = partida.Credito
				}
				*op.TotalComp += p.Monto

				op.PartidasContables = append(op.PartidasContables, p)
			}

			{ // Partida renglón
				p := ops.Partida{}
				p.ID, _ = uuid.NewV1()
				p.Cuenta = partida.Cuenta

				// Traigo cuenta
				cta, err := h.cuentasGetter.ReadOne(ctx, cuentas.ReadOneReq{
					ID:        partida.Cuenta,
					Comitente: cfg.Comitente,
				})
				if err != nil {
					return oo, errors.Wrapf(err, "buscando cuenta '%v' ", partida.Cuenta)
				}
				if cta.AperturaPersona {
					p.Persona = new(uuid.UUID)
					*p.Persona = cfg.Persona
					p.Sucursal = new(int)
					*p.Sucursal = cfg.Sucursal
				}
				if cta.UsaAplicaciones {
					p.PartidaAplicada = new(uuid.UUID)
					*p.PartidaAplicada = p.ID
				}

				if partida.IVAAlicuota.Valid() {
					p.AlicuotaIVA = new(iva.Alicuota)
					*p.AlicuotaIVA = partida.IVAAlicuota
				}
				if partida.IVAConcepto.Valid() {
					p.ConceptoIVA = new(iva.Concepto)
					*p.ConceptoIVA = partida.IVAConcepto
				}

				p.Detalle = new(string)
				*p.Detalle = partida.Detalle
				if partida.NumeroRef != "" {
					partida.Detalle += " - " + partida.NumeroRef
				}
				if partida.NumeroRef != "" {
					partida.Detalle += fmt.Sprintf(" (%v)", partida.Codigo)
				}
				switch {
				case partida.Debito != 0:
					p.Monto = partida.Debito
				case partida.Credito != 0:
					p.Monto = -partida.Credito
				default:
					continue
				}
				op.PartidasContables = append(op.PartidasContables, p)
			}
		}
		*op.TotalComp = dec.NewD2(math.Abs(op.TotalComp.Float()))
		oo = append(oo, &op)
	}
	return
}

// // Crea las ops agrupando las partidas por día.
// func (h *Handler) crearOpsGastos(pp PartidasImputadas, cfg config.Config) (oo []ops.Op, err error) {

// 	// Determino fechas de ops
// 	ff := map[fecha.Fecha]PartidasImputadas{}
// 	for _, v := range pp {
// 		anterior := ff[v.Fecha]
// 		anterior = append(anterior, v)
// 		ff[v.Fecha] = anterior
// 	}
// 	fechas := []fecha.Fecha{}
// 	for k := range ff {
// 		fechas = append(fechas, k)
// 	}
// 	sort.Slice(fechas, func(i, j int) bool {
// 		return fechas[i] < fechas[j]
// 	})

// 	comp, err := h.compsHandler.ReadOne(comps.ReadOneReq{Comitente: cfg.Comitente, ID: cfg.CompGastos})
// 	if err != nil {
// 		return nil, errors.Wrapf(err, "buscando comp ID=%v", cfg.CompGastos)
// 	}
// 	ncomp := comp.UltimoNumeroUsado

// 	for _, v := range fechas {
// 		op := ops.Op{}
// 		op.ID, _ = uuid.NewV1()
// 		op.Programa = Programa
// 		op.Fecha = v
// 		op.FechaOriginal = v
// 		op.Empresa = cfg.Empresa
// 		op.Comitente = cfg.Comitente
// 		op.CompID = cfg.CompGastos
// 		op.CompNombre = comp.Nombre
// 		op.TranDef = cfg.ID
// 		ncomp++
// 		op.NComp = ncomp
// 		op.PuntoDeVenta = *comp.PuntoDeVenta
// 		op.TotalComp = new(dec.D2)
// 		op.NCompStr, err = formatComp(op.PuntoDeVenta, ncomp, "PPPP-NNNNNNNN")
// 		if err != nil {
// 			return nil, errors.Wrapf(err, "poniendole número a comprobante gastos, PuntoVenta=%v, Número=%v", op.PuntoDeVenta, op.NComp)
// 		}

// 		for _, partida := range ff[v] {

// 			{ // partida de 'Cuenta Bancaria'
// 				p := ops.Partida{}
// 				p.Persona = new(uuid.UUID)
// 				*p.Persona = cfg.Persona
// 				p.Sucursal = new(int)
// 				*p.Sucursal = cfg.Sucursal
// 				p.Cuenta = cfg.CuentaContableGastos
// 				p.Detalle = new(string)
// 				*p.Detalle = partida.Detalle
// 				if partida.NumeroRef != "" {
// 					partida.Detalle += " - " + partida.NumeroRef
// 				}
// 				if partida.NumeroRef != "" {
// 					partida.Detalle += fmt.Sprintf(" (%v)", partida.Codigo)
// 				}

// 				switch {
// 				case partida.Debito != 0:
// 					p.Monto = -partida.Debito
// 				case partida.Credito != 0:
// 					p.Monto = partida.Credito
// 				}
// 				*op.TotalComp += p.Monto

// 				op.PartidasContables = append(op.PartidasContables, p)
// 			}

// 			{ // partida de 'Cuenta Bancaria a conciliar'
// 				p := ops.Partida{}
// 				p.Persona = new(uuid.UUID)
// 				*p.Persona = cfg.Persona
// 				p.Sucursal = new(int)
// 				*p.Sucursal = cfg.Sucursal
// 				p.Cuenta = partida.Cuenta
// 				p.Detalle = new(string)

// 				if partida.IVAAlicuota.Valid() {
// 					p.AlicuotaIVA = new(iva.Alicuota)
// 					*p.AlicuotaIVA = partida.IVAAlicuota
// 				}
// 				if partida.IVAConcepto.Valid() {
// 					p.ConceptoIVA = new(iva.Concepto)
// 					*p.ConceptoIVA = partida.IVAConcepto
// 				}

//					*p.Detalle = partida.Detalle
//					if partida.NumeroRef != "" {
//						partida.Detalle += " - " + partida.NumeroRef
//					}
//					if partida.NumeroRef != "" {
//						partida.Detalle += fmt.Sprintf(" (%v)", partida.Codigo)
//					}
//					switch {
//					case partida.Debito != 0:
//						p.Monto = partida.Debito
//					case partida.Credito != 0:
//						p.Monto = -partida.Credito
//					default:
//						continue
//					}
//					op.PartidasContables = append(op.PartidasContables, p)
//				}
//			}
//			*op.TotalComp = dec.NewD2(math.Abs(op.TotalComp.Float()))
//			oo = append(oo, op)
//		}
//		return
//	}
func (h *Handler) generarOps(ctx context.Context, pp []PartidaImputada, cfg config.Config) (oo []*ops.Op, err error) {
	gastos := []PartidaImputada{}
	movs := []PartidaImputada{}

	for _, p := range pp {
		if p.Tipo == "G" {
			gastos = append(gastos, p)
		}
		if p.Tipo == "M" {
			movs = append(movs, p)
		}
	}

	opsGastos, err := h.crearOps(ctx, gastos, cfg, cfg.CompGastos, cfg.CuentaContableGastos)
	if err != nil {
		return nil, err
	}

	opsMovs, err := h.crearOps(ctx, movs, cfg, cfg.CompMovs, cfg.CuentaContableMovs)
	if err != nil {
		return nil, err
	}

	oo = []*ops.Op{}
	oo = append(oo, opsGastos...)
	oo = append(oo, opsMovs...)

	return
}

func formatComp(puntoVenta, numero int, mask string) (out string, err error) {

	mask = strings.ToUpper(mask)
	partes := []string{}

	cantidadPV := strings.Count(mask, "P")
	pv, err := llenarCeros(puntoVenta, cantidadPV)
	if err != nil {
		return out, errors.Wrap(err, "llenando con ceros el punto de venta")
	}
	if pv != "" {
		partes = append(partes, pv)
	}

	cantidadN := strings.Count(mask, "N")
	n, err := llenarCeros(numero, cantidadN)
	if err != nil {
		return out, errors.Wrap(err, "llenando con ceros el comprobante")
	}
	if n != "" {
		partes = append(partes, n)
	}
	out = strings.Join(partes, "-")

	return
}
func llenarCeros(n, digitos int) (out string, err error) {
	if digitos == 0 {
		return "", nil
	}
	nStr := fmt.Sprint(n)
	if len(nStr) > digitos {
		return out, errors.Errorf("había más dígitos (%v) que espacios (%v)", len(nStr), digitos)
	}
	cantidad := digitos - len(nStr)
	for i := 1; i <= cantidad; i++ {
		out += "0"
	}
	return out + nStr, nil
}
