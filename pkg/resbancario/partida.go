package resbancario

import (
	"database/sql/driver"
	"encoding/json"

	"bitbucket.org/marcos19/one/pkg/iva"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
)

// Partidas cumple la intefaz para SQL
type Partidas []Partida

// Partida es cada una de las partidas que componen el CSV descargado del banco
type Partida struct {
	Item      int
	Fecha     fecha.Fecha
	Codigo    string
	Detalle   string
	NumeroRef string
	Debito    dec.D2
	Credito   dec.D2
	Saldo     dec.D2
}

// Value cumple con la interface SQL
func (p Partidas) Value() (driver.Value, error) {
	return json.Marshal(p)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (p *Partidas) Scan(src interface{}) error {
	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New("Type assertion error .([]byte)")
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, p)
	if err != nil {
		return errors.Wrap(err, "Unmarshalling")
	}

	return nil
}

// PartidaImputada contiene la partida y la imputación
type PartidaImputada struct {
	Partida
	Tipo        string       // G para gasto, M para movimiento
	Cuenta      int          `json:",string"`
	IVAConcepto iva.Concepto `json:",string"`
	IVAAlicuota iva.Alicuota `json:",string"`
	Contiene    string
}

// PartidasImputadas cumple la intefaz para SQL
type PartidasImputadas []PartidaImputada

// Value cumple con la interface SQL
func (p PartidasImputadas) Value() (driver.Value, error) {
	return json.Marshal(p)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (p *PartidasImputadas) Scan(src interface{}) error {
	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New("Type assertion error .([]byte)")
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, p)
	if err != nil {
		return errors.Wrap(err, "Unmarshalling")
	}

	return nil
}
