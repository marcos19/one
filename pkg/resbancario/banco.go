package resbancario

import "github.com/gofrs/uuid"

type Banco struct {
	Persona        uuid.UUID
	PersonaNombre  string
	Sucursal       *int `json:",string"`
	SucursalNombre string
}
