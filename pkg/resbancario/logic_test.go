package resbancario

import (
	"os"
	"testing"

	"bitbucket.org/marcos19/one/pkg/iva"
	"bitbucket.org/marcos19/one/pkg/resbancario/config"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestReadCSVCredicoop(t *testing.T) {
	require := require.New(t)
	assert := assert.New(t)

	file, err := os.Open("./testdata/credicoop.csv")
	require.Nil(err)

	cfg, err := config.NewCSVConfig(nil)
	require.NotNil(err)

	cfg, err = config.NewCSVConfig(map[string]int{
		"Fecha":             1,
		"Detalle":           2,
		"Número referencia": 3,
		"Débito":            4,
		"Crédito":           5,
		"Saldo":             6,
		"Código":            7,
	})
	assert.Nil(err)

	pp, err := ReadCSV(file, cfg)
	cfg.SeparadorDecimal = "."
	require.Nil(err)
	require.Len(pp, 142)

	if len(pp) == 0 {
		t.Fatal("no se leyeron registros")
		return
	}
	assert.Equal(pp[0].Codigo, "IVA08")
	assert.Equal(pp[0].Fecha, fecha.Fecha(20200921))
	assert.Equal(pp[0].Detalle, "IVA - Alicuota Inscripto")
	assert.Equal(pp[0].Debito, dec.NewD2(34.23))
	assert.Equal(pp[0].Credito, dec.NewD2(0))
	assert.Equal(pp[0].Saldo, dec.NewD2(1079729.16))
}

func TestReadCSVBancoNacion(t *testing.T) {
	require := require.New(t)
	assert := assert.New(t)

	file, err := os.Open("./testdata/banco_nacion.csv")
	require.Nil(err)

	cfg, err := config.NewCSVConfig(nil)
	require.NotNil(err)

	cfg, err = config.NewCSVConfig(map[string]int{
		"Fecha":             1,
		"Detalle":           5,
		"Número referencia": 4,
		"Monto":             3,
		"Saldo":             6,
	})
	require.Nil(err)
	cfg.OmitirRenglones = 8
	cfg.Comma = ";"
	cfg.SeparadorDecimal = ","
	cfg.SeparadorMiles = "."

	pp, err := ReadCSV(file, cfg)
	require.Nil(err)
	registrosEsperados := 209
	require.Len(pp, registrosEsperados, "se esperaban %v registros, se obtuvieron %v", registrosEsperados, len(pp))

	{
		expected := fecha.Fecha(20210901)
		result := pp[0].Fecha
		assert.Equal(expected, result)
	}
	{
		expected := dec.NewD2(34030.94)
		result := pp[0].Debito
		assert.Equal(expected, result)
	}
	{
		expected := dec.D2(0)
		result := pp[0].Credito
		assert.Equal(expected, result)
	}
	{
		expected := dec.D2(0)
		result := pp[0].Saldo
		assert.Equal(expected, result)
	}
	{
		expected := "48HS. BANCOS    000208072"
		result := pp[0].Detalle
		assert.Equal(expected, result)
	}
	{
		expected := dec.NewD2(5031992.64)
		result := pp[13].Saldo
		assert.Equal(expected, result)
	}
}

func TestFijarImputacionesPorCodigo(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)

	pp := []Partida{
		{
			Item:   1,
			Codigo: "001",
		},
		{
			Item:   2,
			Codigo: "002",
		},
		{
			Item:   3,
			Codigo: "001",
		},
	}

	ii := []config.Imputacion{

		{
			Codigo:      "001",
			Cuenta:      1,
			Detalle:     "Impuesto al cheque",
			Tipo:        "G",
			IVAAlicuota: iva.IVANoGravado,
			IVAConcepto: iva.NoGravado,
		},
		{
			Codigo:      "002",
			Cuenta:      2,
			Detalle:     "Comisión bancaria",
			Tipo:        "G",
			IVAAlicuota: iva.IVA21,
			IVAConcepto: iva.BaseImponible,
		},
	}

	out := fijarImputacionesPorCodigo(pp, ii)
	require.Len(out, 3)
	assert.Equal(out[0].Cuenta, 1)
	assert.Equal(out[1].Cuenta, 2)
	assert.Equal(out[2].Cuenta, 1)

	assert.Equal(out[0].Tipo, "G")
	assert.Equal(out[1].Tipo, "G")
	assert.Equal(out[2].Tipo, "G")

	assert.Equal(out[0].Partida, pp[0])
	assert.Equal(out[1].Partida, pp[1])
	assert.Equal(out[2].Partida, pp[2])
}

func TestFijarImputacionesPorDescripcion(t *testing.T) {
	assert := assert.New(t)
	require := require.New(t)

	pp := []Partida{
		{
			Item:    1,
			Detalle: "GRAVAMEN LEY 25413 S/DEB",
		},
		{
			Item:    2,
			Detalle: "48HS. CANJE ZONA000208075",
		},
		// {
		// 	Item:    3,
		// 	Detalle: "GRAVAMEN LEY 25413 S/CRED",
		// },
	}

	ii := []config.Imputacion{

		{
			Cuenta:      1,
			Contiene:    "GRAVAMEN",
			Tipo:        "G",
			IVAAlicuota: iva.IVANoGravado,
			IVAConcepto: iva.NoGravado,
		},
		{
			Cuenta:   2,
			Contiene: "48HS.",
			Tipo:     "M",
		},
	}

	out, err := fijarImputacionesPorDescripcion(pp, ii)
	require.Nil(err)
	require.Len(out, 2)
	assert.Equal(out[0].Cuenta, 1)
	assert.Equal(out[1].Cuenta, 2)

	assert.Equal(out[0].Tipo, "G")
	assert.Equal(out[1].Tipo, "M")

	assert.Equal(out[0].Partida, pp[0])
	assert.Equal(out[1].Partida, pp[1])
}

func TestFijarImputacionesPorDescripcionSuperpuesto(t *testing.T) {
	require := require.New(t)

	pp := []Partida{
		{
			Item:    1,
			Detalle: "GRAVAMEN LEY 25413 S/DEB",
		},
		{
			Item:    2,
			Detalle: "48HS. CANJE ZONA000208075",
		},
		{
			Item:    3,
			Detalle: "GRAVAMEN LEY 25413 S/CRED",
		},
	}

	ii := []config.Imputacion{

		{
			Cuenta:      1,
			Contiene:    "GRAVAMEN",
			Tipo:        "G",
			IVAAlicuota: iva.IVANoGravado,
			IVAConcepto: iva.NoGravado,
		},
		{
			Cuenta:      1,
			Contiene:    "GRAVAMEN LEY",
			Tipo:        "G",
			IVAAlicuota: iva.IVANoGravado,
			IVAConcepto: iva.NoGravado,
		},
		{
			Cuenta:   2,
			Contiene: "48HS.",
			Tipo:     "M",
		},
	}

	_, err := fijarImputacionesPorDescripcion(pp, ii)
	require.NotNil(err)
}
