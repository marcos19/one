package config

import (
	"context"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/jackc/pgx/v4/pgxpool"
)

func create(ctx context.Context, conn *pgxpool.Pool, req Config) (err error) {

	query := `INSERT INTO resumenes_bancarios_config (comitente, empresa, nombre, persona, sucursal, descripcion, csv_config, imputaciones, comp_gastos, comp_movs, cuenta_contable_gastos, cuenta_contable_movs, created_at)
	VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13)`

	_, err = conn.Exec(ctx, query, req.Comitente, req.Empresa, req.Nombre, req.Persona, req.Sucursal, req.Descripcion, req.CSVConfig, req.Imputaciones, req.CompGastos, req.CompMovs, req.CuentaContableGastos, req.CuentaContableMovs, req.CreatedAt)
	if err != nil {
		return deferror.DB(err, "inserting")
	}

	return
}

type ReadManyReq struct {
	Empresa   int `json:",string"`
	Comitente int
}

type ReadManyResp struct {
	ID        int `json:",string"`
	Nombre    string
	CreatedAt time.Time
}

func readMany(ctx context.Context, conn *pgxpool.Pool, req ReadManyReq) (out []ReadManyResp, err error) {

	out = []ReadManyResp{}

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}
	if req.Empresa == 0 {
		return out, deferror.Validation("no se ingresó empresa")
	}

	query := `SELECT id, nombre, created_at FROM resumenes_bancarios_config WHERE comitente=$1 AND empresa=$2 ORDER BY nombre`

	rows, err := conn.Query(ctx, query, req.Comitente, req.Empresa)
	if err != nil {
		return out, errors.Wrap(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		v := ReadManyResp{}
		err = rows.Scan(&v.ID, &v.Nombre, &v.CreatedAt)
		if err != nil {
			return out, deferror.DB(err, "scanning")
		}
		out = append(out, v)
	}

	return
}

func readOne(ctx context.Context, conn *pgxpool.Pool, req ReadOneReq) (out Config, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}
	if req.ID == 0 {
		return out, deferror.Validation("no se ingresó ID")
	}

	query := `
		SELECT 
			id, comitente, empresa, nombre, persona, sucursal, descripcion, 
			csv_config, imputaciones, comp_gastos, comp_movs, cuenta_contable_gastos, cuenta_contable_movs,
			updated_at, created_at 
		FROM resumenes_bancarios_config
		WHERE comitente=$1 AND id=$2`

	err = conn.
		QueryRow(ctx, query, req.Comitente, req.ID).
		Scan(
			&out.ID, &out.Comitente, &out.Empresa, &out.Nombre, &out.Persona, &out.Sucursal, &out.Descripcion,
			&out.CSVConfig, &out.Imputaciones, &out.CompGastos, &out.CompMovs, &out.CuentaContableGastos, &out.CuentaContableMovs,
			&out.UpdatedAt, &out.CreatedAt)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}
	return
}

func update(ctx context.Context, conn *pgxpool.Pool, req Config) (err error) {
	query := `
	UPDATE resumenes_bancarios_config 
	SET nombre=$1, persona=$2, sucursal=$3, descripcion=$4, csv_config=$5, imputaciones=$6, 
		comp_gastos=$7, comp_movs=$8, cuenta_contable_gastos=$9, cuenta_contable_movs=$10, updated_at=$11
	WHERE comitente=$12 AND id=$13
`
	_, err = conn.Exec(ctx, query, req.Nombre, req.Persona, req.Sucursal, req.Descripcion, req.CSVConfig,
		req.Imputaciones, req.CompGastos, req.CompMovs, req.CuentaContableGastos,
		req.CuentaContableMovs, time.Now(), req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "updating")
	}
	return
}
