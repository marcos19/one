package config

import (
	"database/sql/driver"
	"encoding/json"

	"github.com/cockroachdb/errors"
)

// CSVConfig tiene el diseño de la estructura del archivo CSV
type CSVConfig struct {
	// Encoding        TextEncoding
	OmitirRenglones  int // Si tiene cabecera sería 1
	Comma            string
	LazyQuotes       bool
	Columnas         map[string]int
	FechaLayout      string
	SeparadorDecimal string
	SeparadorMiles   string
}

// Value cumple con la interface SQL
func (c CSVConfig) Value() (driver.Value, error) {
	return json.Marshal(c)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (c *CSVConfig) Scan(src interface{}) error {
	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New("Type assertion error .([]byte)")
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, c)
	if err != nil {
		return errors.Wrap(err, "Unmarshalling")
	}

	return nil
}

// type TextEncoding string

// const (
// 	EncodingWindows1252 = "Windows 1252"
// 	EncodingUTF8        = "UTF-8"
// )

// NewCSVConfig devuelve una configuración de archivo CSV, con algunos valores
// por defecto.
func NewCSVConfig(columnas map[string]int) (config CSVConfig, err error) {

	if columnas == nil {
		return config, errors.Errorf("no de ingresó la estructura de columnas para CSV")
	}
	obligatorias := []string{"Fecha", "Detalle", "Número referencia", "Saldo"}
	repetidas := map[string]int{}

	//
	_, debitoCreditoOK := columnas["Monto"]
	if !debitoCreditoOK {
		_, debitoOK := columnas["Débito"]
		if !debitoOK {
			return config, errors.Errorf("No se definió columna de 'Monto' ni de 'Débito'")
		}
		_, creditoOK := columnas["Crédito"]
		if !creditoOK {
			return config, errors.Errorf("No se definió columna de 'Monto' ni de 'Crédito'")
		}
	}

	// Estaban todas las columnas definidas?
	for _, v := range obligatorias {
		_, estaba := columnas[v]
		if !estaba {
			return config, errors.Errorf("no se definió cual es la columna %v", v)
		}
		repetidas[v]++
	}

	// Hay dos nombres que apuntan a una misma columna?
	for k, v := range repetidas {
		if v > 1 {
			return config, errors.Errorf("la columna %v se definió %v veces", k, v)
		}
	}
	config.Columnas = columnas

	config.OmitirRenglones = 1
	config.FechaLayout = "02/01/2006"
	config.Comma = ","
	// config.Encoding = "Windows 1252"

	return
}
