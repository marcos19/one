package config

import (
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/gofrs/uuid"
	"github.com/rs/zerolog/log"
)

type Config struct {
	ID        int `json:",string"`
	Comitente int `json:",string"`
	Empresa   int `json:",string"`

	Nombre       string // Credicoop 1101/6
	Persona      uuid.UUID
	Sucursal     int `json:",string"`
	Descripcion  string
	CSVConfig    CSVConfig
	Imputaciones Imputaciones

	CompGastos           int `json:",string"`
	CompMovs             int `json:",string"`
	CuentaContableGastos int `json:",string"`
	CuentaContableMovs   int `json:",string"`

	CreatedAt time.Time
	UpdatedAt *time.Time
}

// Si el config tiene definida columna de código, filtra por esa
func (c *Config) FiltraPorCodigo() bool {

	col, estaba := c.CSVConfig.Columnas["Código"]
	if !estaba {
		return false
	}
	if col == 0 {
		return false
	}
	return true
}

// FijarMovimiento fija en la configuración la imputación contable
// para el codigo de movimiento.
func fijarImputacion(cfg *Config, imp Imputacion) {

	if cfg.FiltraPorCodigo() {
		fijarMovPorCodigo(cfg, imp)
	} else {
		fijarMovPorDetalle(cfg, imp)
	}
}

// FijarMovimiento fija en la configuración la imputación contable
// para el codigo de movimiento.
func fijarMovPorCodigo(cfg *Config, imp Imputacion) {

	estaba := false
	for i, v := range cfg.Imputaciones {
		if v.ID == imp.ID {
			cfg.Imputaciones[i].Cuenta = imp.Cuenta
			cfg.Imputaciones[i].IVAAlicuota = imp.IVAAlicuota
			cfg.Imputaciones[i].IVAConcepto = imp.IVAConcepto
			cfg.Imputaciones[i].Codigo = imp.Codigo
			cfg.Imputaciones[i].Contiene = imp.Contiene
			cfg.Imputaciones[i].Detalle = imp.Detalle
			cfg.Imputaciones[i].Tipo = imp.Tipo
			estaba = true
			if cfg.Imputaciones[i].ID == uuid.Nil {
				cfg.Imputaciones[i].ID, _ = uuid.NewV4()
			}
			break
		}
	}
	if !estaba {
		imp := Imputacion{
			ID:          imp.ID,
			Codigo:      imp.Codigo,
			Detalle:     imp.Detalle,
			IVAAlicuota: imp.IVAAlicuota,
			IVAConcepto: imp.IVAConcepto,
			Cuenta:      imp.Cuenta,
			Tipo:        imp.Tipo,
		}
		if imp.ID == uuid.Nil {
			imp.ID, _ = uuid.NewV4()
		}
		cfg.Imputaciones = append(cfg.Imputaciones, imp)
	}
}

// FijarMovimiento fija en la configuración la imputación contable
// en base al detalle.
func fijarMovPorDetalle(cfg *Config, imp Imputacion) {

	estaba := false
	for i, v := range cfg.Imputaciones {
		if v.ID == imp.ID {
			cfg.Imputaciones[i].Cuenta = imp.Cuenta
			cfg.Imputaciones[i].IVAAlicuota = imp.IVAAlicuota
			cfg.Imputaciones[i].IVAConcepto = imp.IVAConcepto
			cfg.Imputaciones[i].Codigo = imp.Codigo
			cfg.Imputaciones[i].Contiene = imp.Contiene
			cfg.Imputaciones[i].Detalle = imp.Detalle
			cfg.Imputaciones[i].Tipo = imp.Tipo
			estaba = true
			if cfg.Imputaciones[i].ID == uuid.Nil {
				cfg.Imputaciones[i].ID, _ = uuid.NewV4()
			}
			log.Debug().Msgf("modifcada partida sin codigo %v", spew.Sdump(cfg.Imputaciones[i]))
			break
		}
	}
	if !estaba {
		imp := Imputacion{
			ID:          imp.ID,
			Codigo:      imp.Codigo,
			Contiene:    imp.Contiene,
			Detalle:     imp.Detalle,
			IVAAlicuota: imp.IVAAlicuota,
			IVAConcepto: imp.IVAConcepto,
			Cuenta:      imp.Cuenta,
			Tipo:        imp.Tipo,
		}
		if imp.ID == uuid.Nil {
			imp.ID, _ = uuid.NewV4()
		}
		cfg.Imputaciones = append(cfg.Imputaciones, imp)
	}
}
