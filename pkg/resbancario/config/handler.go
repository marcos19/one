package config

import (
	"context"
	"fmt"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/personas"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/niler"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Handler struct {
	conn           *pgxpool.Pool
	cuentasGetter  cuentas.ReaderOne
	personaHandler personas.ReaderOne
}

type HandlerArgs struct {
	Conn          *pgxpool.Pool
	CuentasGetter cuentas.ReaderOne
	PersonaGetter personas.ReaderOne
}

func NewHandler(c HandlerArgs) (h *Handler, err error) {
	if c.Conn == nil {
		return h, errors.New("Conn no puede ser nil")
	}
	if niler.IsNil(c.CuentasGetter) {
		return h, errors.New("CuentasHandler no puede ser nil")
	}
	if c.PersonaGetter == nil {
		return h, errors.New("PersonasStore no puede ser nil")
	}

	h = &Handler{
		conn:           c.Conn,
		cuentasGetter:  c.CuentasGetter,
		personaHandler: c.PersonaGetter,
	}

	return
}

// HandleConfigCreate persiste una nueva configuración de resumen bancario para
// levantar.
func (h *Handler) Create(ctx context.Context, req Config) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}
	if req.CuentaContableGastos == 0 {
		return deferror.Validation("no se ingresó cuenta contable de gastos")
	}
	if req.CuentaContableMovs == 0 {
		return deferror.Validation("no se ingresó cuenta contable de movimientos")
	}
	if req.Empresa == 0 {
		return deferror.Validation("no se ingresó empresa")
	}
	if req.Persona == uuid.Nil {
		return deferror.Validation("no se definió la persona")
	}
	if req.Sucursal == 0 {
		return deferror.Validation("no se definió la cuenta bancaria")
	}

	// Busco persona para el Nombre
	pers, err := h.personaHandler.ReadOne(ctx, personas.ReadOneReq{Comitente: req.Comitente, ID: req.Persona})
	if err != nil {
		return errors.Wrap(err, "buscando persona")
	}

	// Nombre de la sucursal
	sucNombre := ""
	for _, v := range pers.Sucursales {
		if *v.ID == req.Sucursal {
			sucNombre = v.Nombre
		}
	}
	req.Nombre = fmt.Sprintf("%v - %v", pers.Nombre, sucNombre)

	// Persisto
	err = create(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "persistiendo configuración")
	}

	return
}

// HandleConfigs devuelve las configs para la empresa solicitada
func (h *Handler) ReadMany(ctx context.Context, req ReadManyReq) (out []ReadManyResp, err error) {

	// Busco
	out, err = readMany(ctx, h.conn, req)
	if err != nil {
		return nil, deferror.DB(err, "buscando configuraciones de resúmenes bancarios")
	}
	return
}

type ReadOneReq struct {
	ID        int `json:",string"`
	Comitente int
}

// HandleConfig devuelve las config solicitada por su ID.
func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (out Config, err error) {

	// Busco configs
	out, err = readOne(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando resumen bancario config")
	}

	if out.CSVConfig.Columnas == nil {
		out.CSVConfig.Columnas = map[string]int{}
	}
	if out.Imputaciones == nil {
		out.Imputaciones = Imputaciones{}
	}

	return
}

func (h *Handler) Update(ctx context.Context, req Config) (err error) {

	// Valido
	if req.ID == 0 {
		return deferror.Validation("no se ingresó ID a modificar")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}
	if req.CuentaContableGastos == 0 {
		return deferror.Validation("no se ingresó cuenta contable de gastos")
	}
	if req.CuentaContableMovs == 0 {
		return deferror.Validation("no se ingresó cuenta contable de movimientos")
	}
	if req.Empresa == 0 {
		return deferror.Validation("no se ingresó empresa")
	}
	if req.Nombre == "" {
		return deferror.Validation("no se ingresó nombre")
	}
	if req.Persona == uuid.Nil {
		return deferror.Validation("no se definió la persona")
	}
	if req.Sucursal == 0 {
		return deferror.Validation("no se definió la cuenta bancaria")
	}
	if ctx == nil {
		ctx = context.Background()
	}

	for _, v := range req.Imputaciones {
		if v.Cuenta == 0 {
			return deferror.Validation("Imputación sin cuenta contable")
		}
		if v.Codigo == "" && v.Contiene == "" {
			return deferror.Validation("Imputación sin codigo de movimiento y sin descripción")
		}
		if v.Tipo != "M" && v.Tipo != "G" {
			return deferror.Validation("El tipo de imputación debe ser M o G")
		}
		if v.Tipo == "G" {
			if !v.IVAAlicuota.Valid() {
				return deferror.Validation("La alicuota de IVA no es válida")
			}
			if !v.IVAConcepto.Valid() {
				return deferror.Validation("El concepto de IVA no es válida")
			}
		}
	}

	// Busco persona para el Nombre
	pers, err := h.personaHandler.ReadOne(ctx, personas.ReadOneReq{Comitente: req.Comitente, ID: req.Persona})
	if err != nil {
		return errors.Wrap(err, "buscando persona-banco")
	}

	// Nombre de la sucursal
	sucNombre := ""
	for _, v := range pers.Sucursales {
		if *v.ID == req.Sucursal {
			sucNombre = v.Nombre
		}
	}
	req.Nombre = fmt.Sprintf("%v - %v", pers.Nombre, sucNombre)

	// Busco anterior
	anterior, err := readOne(ctx, h.conn, ReadOneReq{Comitente: req.Comitente, ID: req.ID})
	if err != nil {
		return errors.Wrap(err, "buscando registro anterior")
	}

	// Le pongo ID a las imputaciones que no tenían
	for i := range req.Imputaciones {
		if req.Imputaciones[i].ID == uuid.Nil {
			req.Imputaciones[i].ID, _ = uuid.NewV4()
		}
	}
	// Modifico
	req.ID = anterior.ID

	err = update(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "persistiendo modificación de configuración de resumen bancario")
	}

	return
}

func (h *Handler) FijarImputacion(ctx context.Context, cfg *Config, imp Imputacion) (err error) {
	fijarImputacion(cfg, imp)

	err = h.Update(ctx, *cfg)
	if err != nil {
		return errors.Wrap(err, "modificando config")
	}
	return
}
