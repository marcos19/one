package config

import (
	"database/sql/driver"
	"encoding/json"

	"bitbucket.org/marcos19/one/pkg/iva"
	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
)

// Imputacion contiene el mapping entre código del banco, cta contable
// y concepto a utilizar
type Imputacion struct {
	// Hay resúmenes que no los manejo por código, así que uso este código interno.
	ID          uuid.UUID
	Codigo      string
	Contiene    string
	Detalle     string // Quiero corroborar que es el código
	Cuenta      int    `json:",string"`
	Tipo        string
	IVAConcepto iva.Concepto `json:",string"`
	IVAAlicuota iva.Alicuota `json:",string"`
}

// Imputaciones cumple la intefaz para SQL
type Imputaciones []Imputacion

// Value cumple con la interface SQL
func (p Imputaciones) Value() (driver.Value, error) {
	return json.Marshal(p)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (p *Imputaciones) Scan(src interface{}) error {
	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New("Type assertion error .([]byte)")
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, p)
	if err != nil {
		return errors.Wrap(err, "Unmarshalling")
	}

	return nil
}
