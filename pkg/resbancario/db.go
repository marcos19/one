package resbancario

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

func readOne(ctx context.Context, conn *pgxpool.Pool, opR ops.ReaderMany, req ReadOneReq) (out Resumen, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, errors.Errorf("no se ingresó ID de comitente")
	}
	if req.ID == uuid.Nil {
		return out, errors.Errorf("no se ingresó ID")
	}

	// Busco los resúmenes
	query := `
		SELECT 
			id, comitente, empresa, config, desde, hasta, 
			persona, persona_nombre, sucursal, sucursal_nombre, partidas, 
			imputaciones, usuario, created_at
		FROM resumenes_bancarios
		WHERE comitente=$1 AND id=$2`

	err = conn.QueryRow(ctx, query, req.Comitente, req.ID).
		Scan(&out.ID, &out.Comitente, &out.Empresa, &out.Config, &out.Desde, &out.Hasta,
			&out.Persona, &out.PersonaNombre, &out.Sucursal, &out.SucursalNombre,
			&out.Partidas, &out.Imputaciones, &out.Usuario, &out.CreatedAt)

	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}

	// Busco las ops
	vv, err := opR.ReadMany(ctx, ops.ReadManyReq{Comitente: req.Comitente, TranID: req.ID})
	if err != nil {
		return out, errors.Wrap(err, "buscando las ops del resumen")
	}
	for _, v := range vv {
		out.Ops = append(out.Ops, &v)
	}
	return
}

type ReadManyReq struct {
	Comitente int
	Empresa   int `json:",string"`
	Desde     fecha.Fecha
	Hasta     fecha.Fecha
}

func readMany(ctx context.Context, conn *pgxpool.Pool, req ReadManyReq) (out []Resumen, err error) {
	out = []Resumen{}

	if req.Comitente == 0 {
		return out, errors.New("no se ingresó ID de comitente")
	}

	ww := []string{"comitente = $1"}
	pp := []interface{}{req.Comitente}

	if req.Empresa != 0 {
		pp = append(pp, req.Empresa)
		ww = append(ww, fmt.Sprintf("empresa = $%v", len(pp)))
	}

	if req.Desde != 0 {
		pp = append(pp, req.Desde)
		ww = append(ww, fmt.Sprintf("desde >= $%v", len(pp)))
	}
	if req.Hasta != 0 {
		pp = append(pp, req.Hasta)
		ww = append(ww, fmt.Sprintf("hasta <= $%v", len(pp)))
	}
	where := fmt.Sprintf("WHERE %v", strings.Join(ww, " AND "))

	query := fmt.Sprintf(`
SELECT id, empresa, desde, hasta, persona_nombre, created_at
FROM resumenes_bancarios
%v
ORDER BY desde`, where)

	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		v := Resumen{}
		err = rows.Scan(&v.ID, &v.Empresa, &v.Desde, &v.Hasta, &v.PersonaNombre, &v.CreatedAt)
		if err != nil {
			return out, deferror.DB(err, "scanning")
		}
		out = append(out, v)
	}
	return
}
