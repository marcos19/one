package resbancario

import (
	"bytes"
	"context"
	"encoding/base64"
	"fmt"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/personas"
	"bitbucket.org/marcos19/one/pkg/resbancario/config"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/fecha"
	"github.com/crosslogic/niler"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/rs/zerolog/log"
)

// Handler da acceso al a todos los routes de personas.
type Handler struct {
	// conn           *pgxpool.Pool
	// cuentasHandler *cuentas.Handler
	// personas       personas.Store
	configHandler *config.Handler

	conn                *pgxpool.Pool
	personas            personas.ReaderOne
	cuentasGetter       cuentas.ReaderOne
	cuentaFuncionGetter cuentas.PorFuncionGetter
	opInserter          ops.Inserter
	opDeleter           ops.Deleter
	opReaderMany        ops.ReaderMany
	compReader          comps.ReaderOne
	compNumerador       comps.Numerador
}

// NewHandler inicializa un cache y devuelve el puntero a este.
func NewHandler(
	conf *config.Handler,
	conn *pgxpool.Pool,
	personas personas.ReaderOne,
	cuentasGetter cuentas.ReaderOne,
	cuentasFuncionGetter cuentas.PorFuncionGetter,
	opHandler OpsHandler,
	compReader comps.ReaderOne,
	compNumerador comps.Numerador,
) (h *Handler, err error) {

	if conn == nil {
		return h, errors.New("DB no puede ser nil")
	}
	if niler.IsNil(personas) {
		return h, errors.New("PersonasReaderOne no puede ser nil")
	}
	if niler.IsNil(cuentasGetter) {
		return h, errors.New("cuentas.ReaderOne no puede ser nil")
	}
	if niler.IsNil(cuentasFuncionGetter) {
		return h, errors.New("cuentas.PorFuncionGetter no puede ser nil")
	}
	if niler.IsNil(opHandler) {
		return h, errors.New("OpsHandler no puede ser nil")
	}
	if niler.IsNil(compReader) {
		return h, errors.New("comps.Numerador no puede ser nil")
	}
	if niler.IsNil(compNumerador) {
		return h, errors.New("comps.Numerador no puede ser nil")
	}

	h = &Handler{
		configHandler:       conf,
		conn:                conn,
		personas:            personas,
		cuentasGetter:       cuentasGetter,
		cuentaFuncionGetter: cuentasFuncionGetter,
		opInserter:          opHandler,
		opDeleter:           opHandler,
		opReaderMany:        opHandler,
		compReader:          compReader,
		compNumerador:       compNumerador,
	}
	return
}

type CreateReq struct {
	ID        uuid.UUID // Para update
	Config    int       `json:",string"`
	Desde     fecha.Fecha
	Hasta     fecha.Fecha
	Partidas  PartidasImputadas
	Comitente int `json:"-"`
}

// HandleCreate persiste un resumen bancario con sus imputaciones
// y partidas contables
func (h *Handler) Create(ctx context.Context, req CreateReq) (out *Resumen, err error) {

	// Inicio transacción
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return out, deferror.DB(err, "iniciando transacción")
	}

	defer tx.Rollback(ctx)
	// Proceso request
	out, err = h.procesar(ctx, tx, req)
	if err != nil {
		return out, err
	}

	// Inserto resbancario
	err = h.insert(ctx, tx, out)
	if err != nil {
		return out, err
	}

	// Inserto ops
	err = h.opInserter.Insert(ctx, tx, out.Ops, nil)
	if err != nil {
		return out, errors.Wrap(err, "ops")
	}

	// Confirmo
	err = tx.Commit(ctx)
	if err != nil {
		return out, err
	}
	return
}

// Valida request y genera las ops.
func (h *Handler) procesar(ctx context.Context, tx pgx.Tx, req CreateReq) (out *Resumen, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}
	if req.Desde == 0 {
		return out, deferror.Validation("no se ingresó fecha desde")
	}
	if req.Hasta == 0 {
		return out, deferror.Validation("no se ingresó fecha hasta")
	}
	if req.Desde > req.Hasta {
		return out, deferror.Validation("fecha desde tiene tiene que ser anterior que fecha hasta")
	}
	if len(req.Partidas) == 0 {
		return out, deferror.Validation("no se ingresaron partidas")
	}
	if req.Config == 0 {
		return out, deferror.Validation("no se definieron las config")
	}

	// Busco config
	c := config.ReadOneReq{ID: req.Config, Comitente: req.Comitente}
	cfg, err := h.configHandler.ReadOne(ctx, c)
	if err != nil {
		return out, err
	}

	// Creo resumen
	out = &Resumen{}
	if req.ID == uuid.Nil {
		out.ID, _ = uuid.NewV4()
	} else {
		out.ID = req.ID
	}
	out.Comitente = req.Comitente
	out.Empresa = cfg.Empresa
	out.Config = cfg.ID
	out.Desde = req.Desde
	out.Hasta = req.Hasta
	out.Persona = cfg.Persona
	out.Sucursal = cfg.Sucursal
	out.PersonaNombre = cfg.Nombre

	// Pongo las imputaciones
	pp := []Partida{}
	for _, v := range req.Partidas {
		pp = append(pp, v.Partida)
	}
	out.Partidas, err = fijarImputaciones(pp, cfg)
	if err != nil {
		return
	}

	// Valido de nuevo
	if out.Persona == uuid.Nil {
		return out, deferror.Validation("el config no tenía definido persona")
	}
	if out.Persona == uuid.Nil {
		return out, deferror.Validation("el config no tenía definido cuenta")
	}

	// Creo las partidas de las ops
	gastos := PartidasImputadas{}
	movs := PartidasImputadas{}

	for _, v := range out.Partidas {
		switch v.Tipo {
		case "G":
			gastos = append(gastos, v)
		case "M":
			movs = append(movs, v)
		default:
			return out, deferror.Validationf("El movimiento <strong>Código:</strong> '%v'; <strong>Detalle:</strong> '%v' no tiene definido tipo de comprobante", v.Codigo, v.Detalle)
		}
	}

	// Creo las ops de "Movimientos"
	opsMovs, err := h.generarOps(ctx, movs, cfg)
	if err != nil {
		return out, errors.Wrap(err, "generando op de 'Movimientos'")
	}
	out.Ops = append(out.Ops, opsMovs...)

	// Creo las ops de "Gastos"
	opsGastos, err := h.generarOps(ctx, gastos, cfg)
	if err != nil {
		return out, errors.Wrap(err, "generando op de 'Gastos'")
	}
	out.Ops = append(out.Ops, opsGastos...)

	// Pego número de transacción
	for i := range out.Ops {
		out.Ops[i].TranID = out.ID
	}

	// Valido
	err = h.opInserter.Validate(ctx, tx, out.Ops, nil)
	if err != nil {
		return out, err
	}

	return
}

func (h *Handler) insert(ctx context.Context, tx pgx.Tx, res *Resumen) (err error) {

	query := `
		INSERT INTO resumenes_bancarios 
			(id, comitente, empresa, config, desde, hasta, persona,
			persona_nombre, sucursal, sucursal_nombre, partidas, imputaciones,
			usuario, created_at) 
		VALUES
			($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, now());
	`
	_, err = tx.Exec(ctx, query, res.ID, res.Comitente, res.Empresa, res.Config,
		res.Desde, res.Hasta, res.Persona, res.PersonaNombre, res.Sucursal,
		res.SucursalNombre, res.Partidas, res.Imputaciones, res.Usuario,
	)
	if err != nil {
		return deferror.DB(err, "persistiendo resúmen bancario")
	}

	return
}

func (h *Handler) ReadMany(ctx context.Context, req ReadManyReq) (out []Resumen, err error) {
	out, err = readMany(ctx, h.conn, req)
	if err != nil {
		return out, errors.Wrap(err, "buscando resúmenes bancarios")
	}
	return
}

type ReadOneReq struct {
	ID        uuid.UUID
	Comitente int
}

// HandleOne devuelve on resumen bancario por su ID
func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (out Resumen, err error) {
	out, err = readOne(ctx, h.conn, h.opReaderMany, req)
	if err != nil {
		return out, errors.Wrap(err, "buscando resumen bancario")
	}
	return
}

func (h *Handler) Update(ctx context.Context, req CreateReq) (out *Resumen, err error) {

	// Inicio transacción
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return out, deferror.DB(err, "iniciando transacción de borrado")
	}

	defer tx.Rollback(ctx)
	// Proceso request
	out, err = h.procesar(ctx, tx, req)
	if err != nil {
		return out, errors.Wrap(err, "procesando")
	}

	// Borro anterior
	err = h.delete(ctx, tx, DeleteReq{Comitente: req.Comitente, ID: req.ID})
	if err != nil {
		return out, err
	}

	// Inserto nuevo
	err = h.insert(ctx, tx, out)
	if err != nil {
		return out, err
	}

	// Confirmo
	err = tx.Commit(ctx)
	if err != nil {
		return out, deferror.DB(err, "confirmando transacción")
	}
	return
}

type DeleteReq struct {
	ID        uuid.UUID
	Comitente int
}

func (h *Handler) Delete(ctx context.Context, req DeleteReq) (err error) {

	// Inicio transacción
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return deferror.DB(err, "iniciando transacción de borrado")
	}
	defer tx.Rollback(ctx)

	// Borro
	err = h.delete(ctx, tx, req)
	if err != nil {
		return err
	}

	// Confirmo
	err = tx.Commit(ctx)
	if err != nil {
		return deferror.DB(err, "confirmando transacción")
	}
	return
}

func (h *Handler) delete(ctx context.Context, tx pgx.Tx, req DeleteReq) (err error) {

	// Valido
	if req.ID == uuid.Nil {
		return deferror.Validation("No se ingresó ID de resumen eliminar")
	}
	if req.Comitente == 0 {
		return deferror.Validation("No se ingresó ID de comitente")
	}

	// Busco los IDS de las OPS
	ids := []uuid.UUID{}
	rows, err := tx.Query(ctx, "SELECT id FROM ops WHERE tran_id = $1", req.ID)
	if err != nil {
		return deferror.DB(err, "Querying ops IDs")
	}
	defer rows.Close()
	for rows.Next() {
		v := uuid.UUID{}
		err = rows.Scan(&v)
		if err != nil {
			return deferror.DB(err, "escaneando row ID de OP")
		}
		ids = append(ids, v)
	}

	// Borro ops
	err = h.opDeleter.Delete(ctx, tx, req.Comitente, ids)
	if err != nil {
		return errors.Wrap(err, "llamando a DeleteFlow")
	}

	// Borro el resumen
	_, err = tx.Exec(ctx, "DELETE FROM resumenes_bancarios WHERE id = $1", req.ID)
	if err != nil {
		return deferror.DB(err, "borrando resumen bancario anterior")
	}

	return
}

type FijarMovReq struct {
	Config     int `json:",string"`
	Imputacion config.Imputacion
	Partidas   PartidasImputadas
	Comitente  int
}

// FijarMovimiento fija en la configuración la imputación contable
// para el codigo de movimiento y devuelve todo el resumen completo.
func (h *Handler) FijarMov(ctx context.Context, req FijarMovReq) (out []PartidaImputada, err error) {

	// Busco config
	cfg, err := h.configHandler.ReadOne(ctx, config.ReadOneReq{Comitente: req.Comitente, ID: req.Config})
	if err != nil {
		return out, errors.Wrap(err, "buscando config")
	}

	// Grabo config
	err = h.configHandler.FijarImputacion(ctx, &cfg, req.Imputacion)
	if err != nil {
		return out, errors.Wrap(err, "fijando imputación")
	}

	// Reimputo las partidas de este resumen
	pp := Partidas{}
	for _, v := range req.Partidas {
		pp = append(pp, v.Partida)
	}
	out, err = fijarImputaciones(pp, cfg)
	if err != nil {
		return
	}

	return
}

type LeerCSVReq struct {
	Config    int `json:",string"`
	Archivo   string
	Comitente int
}

type LeerCSVResponse struct {
	Ops      []*ops.Op
	Partidas []PartidaImputada
}

// LeerCSV lee el archivo CSV y devuelve las partidas imputadas y ops
func (h *Handler) LeerCSV(ctx context.Context, req LeerCSVReq) (out LeerCSVResponse, err error) {

	// Busco config
	config, err := h.configHandler.ReadOne(ctx, config.ReadOneReq{Comitente: req.Comitente, ID: req.Config})
	if err != nil {
		return out, errors.Wrap(err, "buscando config")
	}

	data, err := base64.StdEncoding.DecodeString(req.Archivo)
	if err != nil {
		return out, errors.Wrap(err, "decoding b64")
	}

	// Leo CSV
	pp, err := ReadCSV(bytes.NewBuffer(data), config.CSVConfig)
	if err != nil {
		return out, errors.Wrap(err, "leyendo CSV")
	}

	// Pongo las imputaciones
	out.Partidas, err = fijarImputaciones(pp, config)
	if err != nil {
		return
	}

	// Genero ops
	out.Ops, err = h.generarOps(ctx, out.Partidas, config)
	if err != nil {
		return out, errors.Wrap(err, "generando ops")
	}

	return
}

// HandleBancos devuelve aquellas personas que tienen asociada la
// la cuenta contable definida como cuenta bancaria.
func (h *Handler) Bancos(ctx context.Context, comitente int) (out []personas.Persona, err error) {

	// ¿Qué cuentas contables son cuentas bancarias?
	cc, err := h.cuentaFuncionGetter.PorFuncionContable(ctx, comitente, cuentas.CuentaBancaria)
	if err != nil {
		return nil, errors.Wrap(err, "buscando las cuentas contables definidas como 'Cuentas bancarias'")
	}
	if len(cc) == 0 {
		return out, errors.Errorf("no hay ninguna cuenta contable configurada con función 'Cuenta Bancaria'")
	}
	gg := tipos.GrupoInts{}
	for _, v := range cc {
		gg = append(gg, v.ID)
	}

	// ¿Qué personas tienen esta cuenta asociada?
	query := fmt.Sprintf(`
		SELECT persona
		FROM cuentas_asociadas 
		WHERE comitente = $1 AND cuenta IN %v
		GROUP BY persona
		`, gg.ParaClausulaIn())
	log.Debug().Msg(query)
	rows, err := h.conn.Query(ctx, query, comitente)
	if err != nil {
		return nil, deferror.DB(err, "buscando personas en 'cuentas_asociadas'")
	}
	defer rows.Close()

	// IDs de las personas
	for rows.Next() {
		id := uuid.UUID{}
		err = rows.Scan(&id)
		if err != nil {
			return nil, deferror.DB(err, "escaneando id persona")
		}
		p, err := h.personas.ReadOne(ctx, personas.ReadOneReq{Comitente: comitente, ID: id})
		if err != nil {
			return out, errors.Wrap(err, "buscando persona")
		}
		out = append(out, p)
	}

	return
}
