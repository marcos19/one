package parafacturar

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/fact/config"
	"bitbucket.org/marcos19/one/pkg/productos"
	"bitbucket.org/marcos19/one/pkg/productos/categorias"
	"bitbucket.org/marcos19/one/pkg/unicidadescont/porcantidad"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

type Req struct {
	Comitente int
	Producto  uuid.UUID
	SKU       uuid.UUID
	Config    int `json:",string"`

	// Para reducir la búsqueda, hace un igual el atributo indexado.
	Filtros map[string]interface{}
	Limit   int
	Page    int
}

// Devuelve las unicidades disponibles en base a los filtros.
// Determina que "están disponibles" cuando tiene una cantidad distinta de cero.
func Disponibles(ctx context.Context, conn *pgxpool.Pool, req Req,
	cfgR config.ReaderOne,
	prodR productos.ReaderOne,
	supR categorias.SuperiorGetter,
) (out []porcantidad.Resp, err error) {

	// Cuentas contables
	cc, err := determinarCuentasContables(ctx, req.Comitente, cfgR, req.Config, prodR, req.Producto, supR)
	if err != nil {
		return out, errors.Wrap(err, "buscando cuentas contables")
	}

	// Determino ID de empresa
	cfg, err := cfgR.ReadOne(ctx, config.ReadOneReq{Comitente: req.Comitente, ID: req.Config})
	if err != nil {
		return out, errors.Wrap(err, "buscando fact config")
	}

	// Armo req
	newReq := porcantidad.Req{
		Comitente: req.Comitente,
		Cuentas:   cc,
		Empresa:   cfg.Empresa,
		Producto:  req.Producto,
		SKU:       req.SKU,
		Filtros:   req.Filtros,
		Limit:     req.Limit,
		Page:      req.Page,
	}
	return porcantidad.Disponibles(ctx, conn, newReq)

}

// En base a la fact/config y a las categoría del producto,
// determina cuales son las cuentas sobre las que debe buscar saldos.
func determinarCuentasContables(
	ctx context.Context,
	comitente int,
	cfgR config.ReaderOne,
	configID int,
	prodR productos.ReaderOne,
	prodID uuid.UUID,
	supR categorias.SuperiorGetter,

) (out []int, err error) {

	// Valido
	if configID == 0 {
		return out, errors.Errorf("no se ingresó comitente")
	}
	if configID == 0 {
		return out, errors.Errorf("no se ingresó config ID")
	}
	if prodID == uuid.Nil {
		return out, errors.Errorf("no se ingresó producto ID")
	}

	// Traigo config
	cfg, err := cfgR.ReadOne(ctx, config.ReadOneReq{
		Comitente: comitente,
		ID:        configID,
	})
	if err != nil {
		return out, errors.Wrap(err, "buscando config")
	}

	// Traigo producto
	prod, err := prodR.ReadOne(ctx, productos.ReadOneReq{
		Comitente: comitente,
		ID:        prodID,
	})
	if err != nil {
		return nil, errors.Wrapf(err, "buscando producto %v", prod.ID)
	}

	// Busco imputación para categoría de producto. Si no está empiezo a
	// subir en la jerarquía hasta llegar a la root.
	count := 0
	cat := prod.Categoria
	for {
		count++
		if count > 50 {
			return out, errors.Errorf("superado límite de iteraciones")
		}
		for _, v := range cfg.Imputaciones {
			if v.Categoria == cat {
				out = append(out, v.CuentasSaldosUnicidad...)
				return
			}
		}

		// Ninguna imputación referenciaba esta categoría.
		// Empiezo a subir
		sup, err := supR.CategoriaSuperior(ctx, categorias.ReadOneReq{
			ID:        cat,
			Comitente: comitente,
		})
		if err != nil {
			return out, errors.Wrapf(err, "buscando categoría superior a %v", cat)
		}
		if sup == nil {
			return out, errors.Errorf("no se determinó cuenta para aplicar unicidades de producto %v (ni su categoría, ni superiores)", prod.Nombre)
		}
	}

}
