package porop

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/unicidades"
	"bitbucket.org/marcos19/one/pkg/unicidadesdef"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Req struct {
	ID        uuid.UUID
	Comitente int
}

type Resp struct {
	Movs     []Mov
	Def      unicidadesdef.Def
	Unicidad unicidades.Unicidad
}

type Mov struct {
	ID         uuid.UUID
	OpID       uuid.UUID
	Fecha      fecha.Fecha
	CompNombre string
	NCompStr   string
	Cuenta     int  `json:",string"`
	Caja       *int `json:",string"`
	Monto      dec.D2
	Detalle    *string
}

// OpsPorUnicidad devuelve el listado de ops que tocan
// esta unicidad.
func Movimientos(
	ctx context.Context,
	conn *pgxpool.Pool,
	req Req,
	unic unicidades.ReaderOne,
	def unicidadesdef.ReaderOne,
) (out Resp, err error) {

	{ // Busco movimientos
		query := `
		SELECT id, fecha_contable, comp_nombre, n_comp_str, cuenta, caja, monto, detalle 
		FROM partidas
		WHERE 
			comitente = $1 AND unicidad = $2
		ORDER BY fecha_contable
		;`

		rows, err := conn.Query(ctx, query, req.Comitente, req.ID)
		if err != nil {
			return out, deferror.DB(err, "buscando movimientos")
		}
		defer rows.Close()
		for rows.Next() {
			v := Mov{}
			err = rows.Scan(&v.ID, &v.Fecha, &v.CompNombre, &v.NCompStr, &v.Cuenta, &v.Caja, &v.Monto, &v.Detalle)
			if err != nil {
				return out, deferror.DB(err, "escaneando movimientos")
			}
			out.Movs = append(out.Movs, v)
		}
	}

	// Busco unicidad
	out.Unicidad, err = unic.ReadOne(ctx, unicidades.ReadOneReq{
		Comitente: req.Comitente,
		ID:        req.ID,
	})
	if err != nil {
		return out, err
	}

	// Busco definición unicidad
	out.Def, err = def.ReadOne(ctx, unicidadesdef.ReadOneReq{
		Comitente: req.Comitente,
		ID:        out.Unicidad.Definicion,
	})
	if err != nil {
		return out, err
	}

	return
}
