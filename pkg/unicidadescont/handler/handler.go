package unicidadescont

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/fact/config"
	"bitbucket.org/marcos19/one/pkg/productos"
	"bitbucket.org/marcos19/one/pkg/productos/categorias"
	"bitbucket.org/marcos19/one/pkg/unicidades"
	"bitbucket.org/marcos19/one/pkg/unicidadescont/parafacturar"
	"bitbucket.org/marcos19/one/pkg/unicidadescont/porcantidad"
	"bitbucket.org/marcos19/one/pkg/unicidadescont/pormonto"
	"bitbucket.org/marcos19/one/pkg/unicidadescont/porop"
	"bitbucket.org/marcos19/one/pkg/unicidadesdef"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler es el encargado de recibir los http requests y emitir los responses.
type Handler struct {
	conn          *pgxpool.Pool
	unicidadesdef *unicidadesdef.Handler
	unicidades    *unicidades.Handler
	factconfig    *config.Handler
	categorias    *categorias.Handler
	productos     *productos.Handler
}

// NewHandler instancia el handler de fórmulas.
func NewHandler(args HandlerArgs) (h *Handler, err error) {
	h = &Handler{}
	if args.Conn == nil {
		return h, deferror.Validation("Conn no puede ser nil")
	}
	if args.Unicidadesdef == nil {
		return h, deferror.Validation("UnicidadesdefHandler no puede ser nil")
	}
	if args.Unicidades == nil {
		return h, deferror.Validation("UnicidadesHandler no puede ser nil")
	}
	if args.FactConfig == nil {
		return h, deferror.Validation("FactConfigHandler no puede ser nil")
	}
	if args.Categorias == nil {
		return h, deferror.Validation("CategoriasHandler no puede ser nil")
	}
	if args.Productos == nil {
		return h, deferror.Validation("ProductosHandler no puede ser nil")
	}

	h = &Handler{
		conn:          args.Conn,
		unicidadesdef: args.Unicidadesdef,
		unicidades:    args.Unicidades,
		factconfig:    args.FactConfig,
		categorias:    args.Categorias,
		productos:     args.Productos,
	}

	return
}

type HandlerArgs struct {
	Conn          *pgxpool.Pool
	Unicidadesdef *unicidadesdef.Handler
	Unicidades    *unicidades.Handler
	FactConfig    *config.Handler
	Categorias    *categorias.Handler
	Productos     *productos.Handler
}

// OpsPorUnicidad devuelve el listado de ops que tocan
// esta unicidad.
func (h *Handler) OpsPorUnicidad(ctx context.Context, req porop.Req) (out porop.Resp, err error) {
	return porop.Movimientos(ctx, h.conn, req, h.unicidades, h.unicidadesdef)
}

// Es la consulta que uso cuando necesito saber las unicidades de PRODUCTO disponibles.
// Es la consulta que uso para tirar la apertura en buscador de productos
// Se basa en las CANTIDADES no en los pesos. No es retroactiva, no
// creo que deba serla tampoco.
func (h *Handler) AperturaPorCantidad(ctx context.Context, req porcantidad.Req) (out []porcantidad.Resp, err error) {

	return porcantidad.Disponibles(ctx, h.conn, req)
}

// Es la consulta que hace cuando se está por facturar un producto que
// Abre por unicidad y hay que seleccionar cuál es
// Se basa en las CANTIDADES no en los pesos. No es retroactiva.
func (h *Handler) AperturaPorCantidadFactura(ctx context.Context, req parafacturar.Req) (out []porcantidad.Resp, err error) {

	return parafacturar.Disponibles(ctx, h.conn, req, h.factconfig, h.productos, h.categorias)
}

func (h *Handler) AperturaPorMonto(ctx context.Context, req pormonto.Req) (out []pormonto.Resp, err error) {

	return pormonto.Disponibles(ctx, h.conn, req)
}
