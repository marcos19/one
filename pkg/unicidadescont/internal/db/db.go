package db

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"bitbucket.org/marcos19/one/pkg/unicidades"
	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Trae las unicidades por ID de partida aplicada
func UnicidadesPorPartidaAplicadaID(
	ctx context.Context,
	conn *pgxpool.Pool,
	comitente int,
	ids []uuid.UUID,
	filtroAtts map[string]any,
) (out map[uuid.UUID]unicidades.Unicidad, err error) {

	out = map[uuid.UUID]unicidades.Unicidad{}

	ww := []string{}
	pp := []any{}

	{ // Fitro de IDs
		w := fmt.Sprintf("partidas.partida_aplicada IN (%v)", ArrayUUID(ids))
		ww = append(ww, w)
	}

	{ // Filtro de atts
		strs, params := clauseFiltroAtts(comitente, filtroAtts)
		ww = append(ww, strs...)
		pp = append(pp, params...)
	}

	where := strings.Join(ww, " AND ")
	query := fmt.Sprintf(`
			SELECT unicidades.id, unicidades.definicion, unicidades.atts_indexados, unicidades.atts, unicidades.created_at, partidas.partida_aplicada 
			FROM unicidades
			INNER JOIN partidas ON partidas.unicidad = unicidades.id
			WHERE %v`, where)

	// Busco
	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		v := unicidades.Unicidad{}
		partidaAplicada := uuid.UUID{}
		err = rows.Scan(&v.ID, &v.Definicion, &v.AttsIndexados, &v.Atts, &v.CreatedAt, &partidaAplicada)
		if err != nil {
			return out, errors.Wrap(err, "escaneando")
		}
		if v.AttsIndexados == nil {
			v.AttsIndexados = tipos.JSON{}
		}
		if v.Atts == nil {
			v.Atts = tipos.JSON{}
		}
		out[partidaAplicada] = v
	}
	return
}

func clauseFiltroAtts(comitente int, m map[string]any) (ww []string, pp []any) {
	if len(m) == 0 {
		return
	}

	ww = append(ww, "partidas.comitente=$1")
	pp = append(pp, comitente)

	count := 2

	for k, v := range m {
		ww = append(ww, fmt.Sprintf("atts_indexados ->> $%v::string = $%v", count, count+1))
		pp = append(pp, k, v)
		count += 2
	}

	return

}

// Converts UUID to bytes for table SALDOS
func ToBytes(in uuid.UUID) string {
	if in == uuid.Nil {
		return "x''"
	} else {
		return fmt.Sprintf("x'%x'", in)
	}
}

// Returns string to put in SQL IN clause
func ArrayUUID(ids []uuid.UUID) string {
	idsStr := []string{}
	for _, v := range ids {
		idsStr = append(idsStr, fmt.Sprintf("'%v'", v.String()))
	}
	return strings.Join(idsStr, ", ")
}
