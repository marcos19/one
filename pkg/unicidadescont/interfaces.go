package unicidadescont

import (
	"github.com/gofrs/uuid"
)

type CuentasPorProductoReq struct {
	Comitente  int
	ProductoID uuid.UUID
}
