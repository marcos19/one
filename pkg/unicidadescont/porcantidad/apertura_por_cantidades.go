package porcantidad

import (
	"context"
	"fmt"
	"sort"
	"strings"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"bitbucket.org/marcos19/one/pkg/unicidades"
	"bitbucket.org/marcos19/one/pkg/unicidadescont/internal/db"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Req struct {
	Comitente int
	Cuentas   tipos.GrupoInts
	Empresa   int `json:",string"`
	Producto  uuid.UUID
	SKU       uuid.UUID

	// Para reducir la búsqueda, hace un igual el atributo indexado.
	Filtros map[string]interface{}

	// Cuantos items quiero limitar la respuesta
	Limit int

	// Dentro de los resultados, que página mostrar
	Page int
}

type Resp struct {
	unicidades.Unicidad
	Cantidad dec.D4
}

// Devuelve las unicidades disponibles en base a los filtros.
// Determina que "están disponibles" cuando tiene una cantidad distinta de cero.
func Disponibles(ctx context.Context, conn *pgxpool.Pool, req Req) (out []Resp, err error) {
	if req.Comitente == 0 {
		return out, errors.Errorf("no se ingresó comitente ID")
	}
	if len(req.Cuentas) == 0 {
		return out, errors.Errorf("no se ingresó ninugna cuenta contable")
	}
	if req.Producto == uuid.Nil {
		return out, errors.Errorf("no se ingresó producto")
	}
	if req.SKU == uuid.Nil {
		return out, errors.Errorf("no se ingresó SKU")
	}

	// Busco cuales tienen saldo (de cantidad)
	saldos, err := partidasDisponiblesPorCantidad(ctx, conn, req)
	if err != nil {
		return out, errors.Wrap(err, "buscando partidas disponibles por cantidad")
	}

	// Paso a slice
	ids := make([]uuid.UUID, len(saldos))
	i := 0
	for k := range saldos {
		ids[i] = k
		i++
	}

	// Busco las unicidades
	unics, err := db.UnicidadesPorPartidaAplicadaID(ctx, conn, req.Comitente, ids, req.Filtros)
	if err != nil {
		return out, errors.Wrap(err, "buscando unicidades")
	}

	// Junto ambos
	for k, v := range saldos {
		// Si no encuentro unicidad, puede ser por un filtro de atributos
		// El comportamiento es correcto.
		u, ok := unics[k]
		if !ok {
			continue
		}

		o := Resp{
			Unicidad: u,
			Cantidad: v,
		}
		out = append(out, o)
	}

	// Ordeno
	sort.Slice(out, func(i, j int) bool {
		return out[i].ID.String() < out[j].ID.String()
	})
	return
}

func partidasDisponiblesPorCantidad(
	ctx context.Context,
	conn *pgxpool.Pool,
	req Req) (out map[uuid.UUID]dec.D4, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se definió ID comitente")
	}

	// Clauslas where
	ww := []string{}
	{ // Comitente
		w := fmt.Sprintf("comitente = '%v'", req.Comitente)
		ww = append(ww, w)
	}

	// Omito las que están en cero
	w := "cantidad <> 0"
	ww = append(ww, w)

	// Cuenta
	if len(req.Cuentas) > 0 {
		w := fmt.Sprintf("cuenta IN %v", req.Cuentas.ParaClausulaIn())
		ww = append(ww, w)
	}

	// Empresa
	if req.Empresa != 0 {
		w := fmt.Sprintf("empresa = %v", req.Empresa)
		ww = append(ww, w)
	}

	// Producto
	if req.Producto != uuid.Nil {
		w := fmt.Sprintf("producto = %v", db.ToBytes(req.Producto))
		ww = append(ww, w)
	}

	// SKU
	if req.SKU != uuid.Nil {
		w := fmt.Sprintf("sku = %v", db.ToBytes(req.SKU))
		ww = append(ww, w)
	}

	whereClause := strings.Join(ww, " AND ")

	// Paginación
	limit := ""
	if req.Limit != 0 {
		limit = fmt.Sprintf("LIMIT %v", req.Limit)
		if req.Page != 0 {
			limit += fmt.Sprintf(" OFFSET %v", req.Page*req.Limit)
		}
	}

	out = map[uuid.UUID]dec.D4{}

	// Busco
	query := fmt.Sprintf(`
			SELECT partida_aplicada, SUM(cantidad)
			FROM saldos
			WHERE %v
			GROUP BY partida_aplicada
			HAVING sum(cantidad) <> 0
			%v
			;`, whereClause, limit)
	rows, err := conn.Query(ctx, query)
	if err != nil {
		return out, deferror.DB(err, "realizando query de saldos pendientes")
	}
	defer rows.Close()

	for rows.Next() {
		idBytes := []byte{}
		saldo := new(int)
		err = rows.Scan(&idBytes, &saldo)
		if err != nil {
			return out, errors.Wrap(err, "escaneando saldo")
		}

		id := uuid.FromBytesOrNil(idBytes)
		if err != nil {
			return out, errors.Wrap(err, "escaneando UUID de tabla de saldos")
		}
		if saldo == nil {
			return out, errors.Wrap(err, "el saldo era null")
		}
		out[id] = dec.D4(float64(*saldo))
	}

	return
}
