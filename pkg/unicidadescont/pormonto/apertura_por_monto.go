package pormonto

import (
	"context"
	"fmt"
	"sort"
	"strings"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/unicidades"
	"bitbucket.org/marcos19/one/pkg/unicidadescont/internal/db"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Req struct {
	Comitente int
	Cuenta    int `json:",string"`
	Empresa   int `json:",string"` // Opcional
	Caja      int `json:",string"` // Opcional

	// Para cambio de estados de cheques
	Persona  uuid.UUID // Opcional
	Sucursal int       `json:",string"` // Opcional

	// Para reducir la búsqueda, hace un igual el atributo indexado.
	Definicion int `json:",string"`
	Filtros    map[string]interface{}
	SinLimite  bool
}

type Resp struct {
	unicidades.Unicidad
	Saldo dec.D2
}

func Disponibles(
	ctx context.Context,
	conn *pgxpool.Pool,
	req Req,
) (out []Resp, err error) {

	// Busco cuales tienen saldo (de monto)
	saldos, err := partidasDeSaldos(ctx, conn, req)
	if err != nil {
		return out, errors.Wrap(err, "buscando partida de saldos")
	}

	// Paso a slice
	ids := make([]uuid.UUID, len(saldos))
	i := 0
	for k := range saldos {
		ids[i] = k
		i++
	}

	// Busco las unicidades
	unics, err := db.UnicidadesPorPartidaAplicadaID(ctx, conn, req.Comitente, ids, req.Filtros)
	if err != nil {
		return out, errors.Wrap(err, "buscando unicidades")
	}

	// Junto ambos
	for k, v := range saldos {
		o := Resp{
			Unicidad: unics[k],
			Saldo:    v,
		}
		out = append(out, o)
	}

	// Ordeno
	sort.Slice(out, func(i, j int) bool {
		return out[i].ID.String() < out[j].ID.String()
	})
	return
}

// Busca en tabla saldos las partidas que tengan "monto"
func partidasDeSaldos(ctx context.Context, conn *pgxpool.Pool, req Req) (out map[uuid.UUID]dec.D2, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se definió ID comitente")
	}
	if req.Cuenta == 0 {
		return out, errors.Errorf("no se ingresó cuenta contable")
	}

	// Clauslas where
	ww := []string{}
	{ // Comitente
		w := fmt.Sprintf("comitente = '%v'", req.Comitente)
		ww = append(ww, w)
	}

	if req.Cuenta != 0 {
		w := fmt.Sprintf("cuenta=%v", req.Cuenta)
		ww = append(ww, w)
	}

	if req.Caja != 0 {
		w := fmt.Sprintf("caja = %v", req.Caja)
		ww = append(ww, w)
	}

	if req.Empresa != 0 {
		w := fmt.Sprintf("empresa = %v", req.Empresa)
		ww = append(ww, w)
	}

	{ // Centro
		ww = append(ww, "centro = 0")
	}

	// Persona
	if req.Persona != uuid.Nil {
		w := fmt.Sprintf("persona = %v", db.ToBytes(req.Persona))
		ww = append(ww, w)
	}

	// Sucursal
	if req.Sucursal != 0 {
		w := fmt.Sprintf("sucursal = %v", req.Sucursal)
		ww = append(ww, w)
	}

	{ // Omito las que están en cero
		w := "monto <> 0"
		ww = append(ww, w)
	}

	whereClause := strings.Join(ww, " AND ")

	out = map[uuid.UUID]dec.D2{}
	limit := ""
	if !req.SinLimite {
		limit = "LIMIT 500"
	}

	query := fmt.Sprintf(`
SELECT partida_aplicada, monto
FROM saldos
WHERE %v
%v;`, whereClause, limit)

	rows, err := conn.Query(ctx, query)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		idBytes := []byte{}
		saldo := dec.D2(0)
		err = rows.Scan(&idBytes, &saldo)
		if err != nil {
			return out, errors.Wrap(err, "escaneando")
		}

		id, err := uuid.FromBytes(idBytes)
		if err != nil {
			return out, errors.Wrap(err, "escaneando UUID")
		}
		out[id] = saldo
	}
	return
}
