package oplog

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Registro struct {
	Comitente    int `json:"-"`
	CompID       int `json:",string"`
	PuntoDeVenta int `json:",string"`
	NComp        int `json:",string"`
	TS           time.Time
	Persona      *uuid.UUID // Si es un comprobante de terceros
	// Si se borra y se crea una op con el mismo número, este número
	// me permite identificar cual fue la op sobre la que se hicieron
	// las operaciones.
	OpID      uuid.UUID
	Usuario   string
	Operacion int `json:",string"`
	Detalle   string
}

func (r Registro) TableName() string {
	return "oplog"
}

/*
CREATE TABLE IF NOT EXISTS oplog (
	comitente
		INT,
	comp_id
		INT,
	punto_de_venta
		INT,
	n_comp
		INT,
	ts
		TIMESTAMP WITH TIME ZONE DEFAULT now(),
	usuario
		STRING,
	operacion
		INT,
	detalle
		STRING,
	PRIMARY KEY (comitente, comp_id, punto_de_venta, n_comp, ts)
);
*/

type PorComprobanteReq struct {
	Comitente    int `json:",string"`
	CompID       int `json:",string"`
	PuntoDeVenta int `json:",string"`
	NComp        int `json:",string"`
	OpID         uuid.UUID
}

// Devuelve los registros para un comprobante
func PorComprobante(ctx context.Context, conn *pgxpool.Pool, req PorComprobanteReq) (out []Registro, err error) {

	where := "comitente=$1 AND comp_id=$2 AND punto_de_venta=$3 AND n_comp=$4"
	pp := []any{req.Comitente, req.CompID, req.PuntoDeVenta, req.NComp}
	if req.OpID != uuid.Nil {
		where = "op_id=$1 AND comitente=$2"
		pp = []any{req.OpID, req.Comitente}
	}
	query := fmt.Sprintf(`SELECT comp_id, punto_de_venta, n_comp, ts, usuario, operacion, detalle, persona, op_id
	FROM oplog
	WHERE  %v
	ORDER BY ts`, where)

	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		v := Registro{}
		err = rows.Scan(&v.CompID, &v.PuntoDeVenta, &v.NComp, &v.TS, &v.Usuario,
			&v.Operacion, &v.Detalle, &v.Persona, &v.OpID)
		out = append(out, v)
	}
	return
}

// Inserta un registro en tabla oplog
func Log(ctx context.Context, tx pgx.Tx, rr ...Registro) (err error) {

	acum := 0
	values := []string{}
	for _, v := range rr {
		if v.Comitente == 0 {
			return errors.Errorf("no se determinó comitente")
		}
		if v.Usuario == "" {
			return errors.Errorf("no se determinó usuario")
		}
		if v.CompID == 0 {
			return errors.Errorf("no se determinó CompID")
		}
		if v.NComp == 0 {
			return errors.Errorf("no se determinó número de comprobante")
		}
		vv := []string{}
		for j := 1; j <= 10; j++ {
			acum++
			vv = append(vv, fmt.Sprintf("$%v", acum))
		}
		values = append(values, fmt.Sprintf("(%v)", strings.Join(vv, ",")))
	}
	query := fmt.Sprintf(`
		INSERT INTO oplog (
			comitente, comp_id, punto_de_venta, n_comp, ts, persona, op_id, usuario, operacion, detalle
		) VALUES 
			%v
		;
	`, strings.Join(values, ", "))
	params := []interface{}{}
	for _, v := range rr {
		ts := time.Now()
		params = append(params, v.Comitente, v.CompID, v.PuntoDeVenta, v.NComp, ts, v.Persona, v.OpID, v.Usuario, v.Operacion, v.Detalle)
	}
	_, err = tx.Exec(ctx, query, params...)
	if err != nil {
		return deferror.DB(err, "insertando registro en log")
	}

	return
}

type TipoOp int

const (
	OpCreate           = 1
	OpRead             = 2
	OpUpdate           = 3
	OpDelete           = 4
	OpSolicitarCAE     = 5
	OpEnviarMailOK     = 6
	OpEnviarMailFail   = 16
	OpDatosModificados = 7
)
