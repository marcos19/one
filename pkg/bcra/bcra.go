package bcra

// Banco es cada una de las entidades financieras definidas por el Banco Central
type Banco struct {
	ID     int
	Nombre string
}

func (b Banco) TableName() string {
	return "bcra_bancos"
}
