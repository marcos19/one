package bcra

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler es la estructura que recibe los request y emite los responses
type Handler struct {
	conn *pgxpool.Pool
}

// NewHandler instancia el handler de productos.
func NewHandler(conn *pgxpool.Pool) (h *Handler) {
	h = &Handler{}
	h.conn = conn

	return
}

// HandleBancos devuelve el listado de bancos definidos por el BCRA
func (h *Handler) Bancos(ctx context.Context) (out []Banco, err error) {

	query := `SELECT * FROM bcra_bancos ORDER BY nombre`
	rows, err := h.conn.Query(ctx, query)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()
	for rows.Next() {
		b := Banco{}
		err = rows.Scan(&b.ID, &b.Nombre)
		if err != nil {
			return out, deferror.DB(err, "scanning")
		}
		out = append(out, b)
	}

	return
}
