package db

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/personas"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/cuits"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Busca un registro de la tabla personas
func ReadOne(ctx context.Context, conn *pgxpool.Pool, comitente int, id uuid.UUID) (p personas.Persona, err error) {

	query := `
		SELECT 
			id, comitente,nombre, nombre_fantasia, tipo, cuit, tipo_cuit, dni, tipo_dni, 
			calle, numero, piso, depto, ciudad_id, ciudad_cp, ciudad_nombre, 
			provincia_id, provincia_nombre, pais,
			domicilios, emails, telefonos, condicion_iva, condicion_gcias, condicion_mt,
			observaciones, activo, motivo_inactividad, usa_sucursales, atts, cuentas_asociadas, 
			saldo_contable,	saldo_financiero, created_at, updated_at
		FROM personas 
		WHERE comitente = $1 AND id = $2
	`

	err = conn.
		QueryRow(ctx, query, comitente, id).
		Scan(&p.ID, &p.Comitente, &p.Nombre, &p.NombreFantasia, &p.Tipo, &p.CUIT, &p.TipoCUIT, &p.DNI, &p.TipoDNI,
			&p.Calle, &p.Numero, &p.Piso, &p.Depto, &p.CiudadID, &p.CiudadCP, &p.CiudadNombre,
			&p.ProvinciaID, &p.ProvinciaNombre, &p.Pais,
			&p.Domicilios, &p.Emails, &p.Telefonos, &p.CondicionIVA, &p.CondicionGcias, &p.CondicionMT, &p.Observaciones,
			&p.Activo, &p.MotivoInactividad, &p.UsaSucursales, &p.Atts, &p.CuentasAsociadas,
			&p.SaldoContable, &p.SaldoFinanciero, &p.CreatedAt, &p.UpdatedAt)
	if err != nil {
		return p, errors.Wrap(err, "buscando persona")
	}
	return
}

func Create(ctx context.Context, tx pgx.Tx, req personas.Persona) (err error) {

	query := `
INSERT INTO personas ( 
	id,
	comitente,
	nombre,
	nombre_fantasia,
	tipo,
	cuit,
	tipo_cuit,
	dni,
	tipo_dni,
	calle,
	numero,
	piso,
	depto,
	ciudad_id,
	ciudad_cp,
	ciudad_nombre,
	provincia_id,
	provincia_nombre,
	pais,
	domicilios,
	emails,
	telefonos,
	condicion_iva,
	condicion_gcias,
	condicion_mt,
	observaciones,
	activo,
	motivo_inactividad,
	usa_sucursales,
	atts,
	cuentas_asociadas,
	saldo_contable,
	saldo_financiero,
	updated_at) 
VALUES (
	 $1, $2, $3, $4, $5, $6, $7, $8, $9,$10,
	$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,
	$21,$22,$23,$24,$25,$26,$27,$28,$29,$30,
	$31,$32,$33,$34
);`
	_, err = tx.Exec(ctx, query,
		req.ID,
		req.Comitente,
		req.Nombre,
		req.NombreFantasia,
		req.Tipo,
		req.CUIT,
		req.TipoCUIT,
		req.DNI,
		req.TipoDNI,
		req.Calle,
		req.Numero,
		req.Piso,
		req.Depto,
		req.CiudadID,
		req.CiudadCP,
		req.CiudadNombre,
		req.ProvinciaID,
		req.ProvinciaNombre,
		req.Pais,
		req.Domicilios,
		req.Emails,
		req.Telefonos,
		req.CondicionIVA,
		req.CondicionGcias,
		req.CondicionMT,
		req.Observaciones,
		req.Activo,
		req.MotivoInactividad,
		req.UsaSucursales,
		req.Atts,
		req.CuentasAsociadas,
		req.SaldoContable,
		req.SaldoFinanciero,
		time.Now(),
	)
	if err != nil {
		return deferror.DB(err, "modificando tabla personas")
	}
	return
}

func ReadMany(
	ctx context.Context,
	conn *pgxpool.Pool,
	req personas.ReadManyReq,
	cuentasReader cuentas.ReaderOne,
) (out []personas.ReadManyResp, err error) {

	ww := []string{"comitente=$1"}
	pp := []interface{}{req.Comitente}

	if len(req.Cuentas) > 0 {
		// Determino si alguna de las cuentas es restrictiva
		libre := false
		for _, v := range req.Cuentas {
			cta, err := cuentasReader.ReadOne(ctx, cuentas.ReadOneReq{
				Comitente: req.Comitente,
				ID:        v,
			})
			if err != nil {
				return out, errors.Wrap(err, "determinando cuentas restrictivas")
			}
			if !cta.AperturaPersonaRestrictiva {
				libre = true
				break
			}
		}
		if !libre {
			query := fmt.Sprintf("id IN (SELECT persona FROM cuentas_asociadas WHERE cuenta IN %v)", req.Cuentas.ParaClausulaIn())
			ww = append(ww, query)
		}
	}

	// Estoy buscando por DNI o CUIT?
	if req.Texto != "" {

		// Es un CUIT o documento?
		enInt, err := strconv.Atoi(req.Texto)
		if err == nil {
			ww = append(ww, fmt.Sprintf("(cuit = %v OR dni = %v)", enInt, enInt))
		} else {
			n := len(pp) + 1
			ww = append(ww, fmt.Sprintf("(nombre ILIKE $%v OR nombre_fantasia ILIKE $%v)", n, n))
			pp = append(pp, "%"+req.Texto+"%")
		}

	}
	where := "WHERE " + strings.Join(ww, " AND ")

	limit := ""
	if !req.SinLimite {
		limit = "LIMIT 300"
	}

	query := fmt.Sprintf(`
		SELECT id, nombre, nombre_fantasia, cuit, dni, provincia_nombre, ciudad_nombre, calle, numero, piso, depto, telefonos, activo, usa_sucursales, cuentas_asociadas
		FROM personas
		%v
		ORDER BY LOWER(nombre) 
		%v
	`, where, limit)

	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "buscando personas")
	}
	defer rows.Close()

	for rows.Next() {
		r := personas.ReadManyResp{}
		err = rows.Scan(&r.ID, &r.Nombre, &r.NombreFantasia, &r.CUIT, &r.DNI,
			&r.ProvinciaNombre, &r.CiudadNombre, &r.Calle, &r.Numero, &r.Piso, &r.Depto,
			&r.Telefonos, &r.Activo, &r.UsaSucursales, &r.CuentasAsociadas)
		if err != nil {
			return out, deferror.DB(err, "escanenado persona")
		}
		out = append(out, r)
	}

	if out == nil {
		out = []personas.ReadManyResp{}
	}
	return
}

func ExistePersona(ctx context.Context, conn *pgxpool.Pool, cuit cuits.CUIT, dni int, comitente int) (existe bool, err error) {

	ors := []string{}
	if cuit != 0 {
		ors = append(ors, fmt.Sprintf("cuit = %v", int(cuit)))
	}
	if dni != 0 {
		ors = append(ors, fmt.Sprintf("dni = %v", dni))
	}
	orsStr := fmt.Sprintf("(%v)", strings.Join(ors, " OR "))

	filtros := []string{
		fmt.Sprintf("comitente = %v", comitente),
		orsStr,
	}
	where := strings.Join(filtros, " AND ")
	query := fmt.Sprintf("SELECT COUNT(id) FROM personas WHERE %v", where)

	// Busco
	count := 0
	err = conn.QueryRow(context.Background(), query).Scan(&count)
	if err != nil {
		return existe, deferror.DB(err, "buscando en base de datos")
	}
	if count == 1 {
		return existe, deferror.Validation("ya existe otra persona con el DNI y/o CUIT")
	}
	if count > 1 {
		return existe, deferror.Validationf("ya existen %v personas con el DNI y/o CUIT", count)
	}
	return
}

func Update(
	ctx context.Context,
	conn *pgxpool.Pool,
	req personas.Persona,
) (err error) {

	// Controlo que existía
	count := 0
	existsQuery := "SELECT COUNT(id) FROM personas WHERE comitente=$1 AND id=$2"
	err = conn.QueryRow(ctx, existsQuery, req.Comitente, req.ID).Scan(&count)
	if err != nil {
		return errors.Wrap(err, "verificando si existía persona")
	}
	if count == 0 {
		return errors.Errorf("la persona que se intenta modificar no existe")
	}

	tx, err := conn.Begin(ctx)
	if err != nil {
		return deferror.DB(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)
	query := `
UPDATE personas
SET 
	nombre = $1,
	nombre_fantasia = $2,
	tipo = $3,
	cuit = $4,
	tipo_cuit = $5,
	dni = $6,
	tipo_dni = $7,
	calle = $8,
	numero = $9,
	piso = $10,
	depto = $11,
	ciudad_id = $12,
	ciudad_cp = $13,
	ciudad_nombre = $14,
	provincia_id = $15,
	provincia_nombre = $16,
	pais = $17,
	domicilios = $18,
	emails = $19,
	telefonos = $20,
	condicion_iva = $21,
	condicion_gcias = $22,
	condicion_mt = $23,
	observaciones = $24,
	activo = $25,
	motivo_inactividad = $26,
	usa_sucursales = $27,
	atts = $28,
	cuentas_asociadas = $29,
	saldo_contable = $30,
	saldo_financiero = $31,
	updated_at = $32
WHERE id = $33 AND comitente = $34
;`
	_, err = tx.Exec(ctx, query,
		req.Nombre,
		req.NombreFantasia,
		req.Tipo,
		req.CUIT,
		req.TipoCUIT,
		req.DNI,
		req.TipoDNI,
		req.Calle,
		req.Numero,
		req.Piso,
		req.Depto,
		req.CiudadID,
		req.CiudadCP,
		req.CiudadNombre,
		req.ProvinciaID,
		req.ProvinciaNombre,
		req.Pais,
		req.Domicilios,
		req.Emails,
		req.Telefonos,
		req.CondicionIVA,
		req.CondicionGcias,
		req.CondicionMT,
		req.Observaciones,
		req.Activo,
		req.MotivoInactividad,
		req.UsaSucursales,
		req.Atts,
		req.CuentasAsociadas,
		req.SaldoContable,
		req.SaldoFinanciero,
		time.Now(),
		req.ID,
		req.Comitente,
	)
	if err != nil {
		return deferror.DB(err, "modificando tabla personas")
	}

	{ // Cuentas asociadas

		// Borro anteriores
		query := `DELETE FROM cuentas_asociadas WHERE comitente = $1 AND persona = $2 AND sucursal = 0;`
		_, err = tx.Exec(ctx, query, req.Comitente, req.ID)
		if err != nil {
			return errors.Wrap(err, "borrando cuentas asociadas")
		}

		// Inserto nuevas
		items := []string{}
		for _, v := range req.CuentasAsociadas {
			it := fmt.Sprintf("(%v,%v,'%v',%v)", req.Comitente, v.Cuenta, req.ID, 0)
			items = append(items, it)
		}
		if len(items) > 0 {
			values := strings.Join(items, ",")
			ins := fmt.Sprintf(
				"INSERT INTO cuentas_asociadas (comitente, cuenta, persona, sucursal) VALUES %v;",
				values,
			)
			_, err = tx.Exec(ctx, ins)
			if err != nil {
				return deferror.DB(err, "insertando cuentas asociadas")
			}
		}
	}

	// Confirmo
	err = tx.Commit(ctx)
	if err != nil {
		return deferror.DB(err, "confirmando transacción")
	}

	return
}

func Delete(
	ctx context.Context,
	conn *pgxpool.Pool,
	req personas.ReadOneReq,
) (err error) {

	{ // Está usado en la partidas?
		count := 0
		existsQuery := "SELECT COUNT(persona) FROM partidas WHERE comitente=$1 AND persona=$2"
		err = conn.QueryRow(ctx, existsQuery, req.Comitente, req.ID).Scan(&count)
		if err != nil {
			return errors.Wrap(err, "verificando si la persona estaba usada en contabilidad")
		}
		if count > 0 {
			return deferror.Validationf("no se puede borrar porque la persona está usada %v veces en la contabilidad", count)
		}
	}

	{ // Está usado en ops?
		count := 0
		existsQuery := "SELECT COUNT(persona) FROM ops WHERE comitente=$1 AND persona=$2"
		err = conn.QueryRow(ctx, existsQuery, req.Comitente, req.ID).Scan(&count)
		if err != nil {
			return errors.Wrap(err, "verificando si la persona estaba usada en operaciones")
		}
		if count > 0 {
			return deferror.Validationf("no se puede borrar porque la persona está usada %v veces en operaciones", count)
		}
	}

	// Inicio transacción
	tx, err := conn.Begin(ctx)
	if err != nil {
		return deferror.DB(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)

	// Cuentas asociadas
	_, err = tx.Exec(ctx, "DELETE FROM cuentas_asociadas WHERE comitente = $1 AND persona = $2", req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "borrando cuentas asociadas")
	}

	// Elimino persona de la base
	res, err := tx.Exec(ctx, "DELETE FROM personas WHERE comitente=$1 AND id=$2", req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "borrando persona")
	}
	if res.RowsAffected() == 0 {
		return errors.Errorf("no se borró ningún registro")
	}

	// Confirmo transacción
	err = tx.Commit(ctx)
	if err != nil {
		return deferror.DB(err, "confirmando transacción")
	}
	return
}
