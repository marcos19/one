package handler

import (
	"fmt"
	"testing"

	"bitbucket.org/marcos19/one/pkg/personas"
	"github.com/stretchr/testify/assert"
)

func TestDiff(t *testing.T) {
	p1 := personas.Persona{
		Nombre: "Marcos",
		Tipo:   "Fantasma",
	}
	p2 := p1
	str := diffPerson(p1, p2)
	assert.Equal(t, str, "[]")
	fmt.Println(str)

	p2.Tipo = "Perro"
	str = diffPerson(p1, p2)
	fmt.Println(str)
}
