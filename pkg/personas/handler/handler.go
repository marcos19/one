package handler

import (
	"context"
	"encoding/json"
	"fmt"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/cuentasasociadas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/domicilios"
	"bitbucket.org/marcos19/one/pkg/log"
	"bitbucket.org/marcos19/one/pkg/personas"
	"bitbucket.org/marcos19/one/pkg/personas/internal/db"
	"bitbucket.org/marcos19/one/pkg/sucursales"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/niler"
	"github.com/dgraph-io/ristretto"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/r3labs/diff/v3"
	"golang.org/x/exp/slog"
)

// Handler es el encargado de recibir los http requests y emitir los responses.
type Handler struct {
	conn    *pgxpool.Pool
	cache   *ristretto.Cache
	cuentas cuentas.ReaderOne
	bus     personas.Bus

	// Si no se ingresa ningún bus, no emite eventos.
	emitirEventos bool
}

type HandlerConfig struct {
	Conn    *pgxpool.Pool
	Cache   *ristretto.Cache
	Cuentas cuentas.ReaderOne
}

// NewHandler instancia el handler de fórmulas.
func NewHandler(
	conn *pgxpool.Pool,
	cache *ristretto.Cache,
	cuentas cuentas.ReaderOne,
	bus personas.Bus,
) (h *Handler, err error) {
	if conn == nil {
		return h, errors.New("Conn no puede ser nil")
	}
	if cache == nil {
		return h, errors.New("Cache no puede ser nil")
	}
	if niler.IsNil(cuentas) {
		return h, errors.New("cuentas.ReaderOne no puede ser nil")
	}
	if !niler.IsNil(bus) {
		h.emitirEventos = true
	}

	h = &Handler{
		conn:    conn,
		cache:   cache,
		cuentas: cuentas,
		bus:     bus,
	}
	return
}

type Getter interface {
	ReadOne(context.Context, personas.ReadOneReq) (personas.Persona, error)
}

// HandlePersonaNueva devuelve una persona
func (h *Handler) Create(ctx context.Context, req *personas.Persona, usuario string) (id uuid.UUID, err error) {

	// Valido
	err = validarPersona(req)
	if err != nil {
		return
	}

	// Existe alguien con ese CUIT o DNI?
	existe, err := h.existePersona(context.Background(), req)
	if err != nil {
		return id, errors.Wrap(err, "corroborando si ya existía")
	}
	if existe {
		return id, deferror.Validation("Ya existía una persona con ese número de DNI o CUIT")
	}

	// Creo ID
	if req.ID == uuid.Nil {
		req.ID, err = uuid.NewV4()
		if err != nil {
			return id, errors.Wrap(err, "creando UUID")
		}
	}

	// Persisto
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return id, errors.Wrap(err, "inciando transacción")
	}
	defer tx.Rollback(ctx)

	// Inserto persona
	err = db.Create(ctx, tx, *req)
	if err != nil {
		return id, deferror.DB(err, "insertando persona")
	}

	// Inserto las cuentas asociadas
	for _, v := range req.CuentasAsociadas {
		v.Comitente = req.Comitente
		v.Persona = req.ID
		v.Sucursal = 0
		err = cuentasasociadas.Insert(ctx, tx, v)
		if err != nil {
			return id, deferror.DB(err, "insertando cuenta asociada")
		}
	}

	// Confirmo transacción
	err = tx.Commit(ctx)
	if err != nil {
		deferror.DB(err, "confirmando transacción")
		return
	}

	if h.emitirEventos {
		h.bus.Emit(personas.EvPersonaCreada{
			Persona: *req,
			Usuario: usuario,
		})
	}

	log.Info(ctx, "persona creada", slog.String("id", req.ID.String()))

	return req.ID, nil
}

func (h *Handler) ReadMany(ctx context.Context, req personas.ReadManyReq) (out []personas.ReadManyResp, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("No se ingresó ID de comitente")
	}

	return db.ReadMany(ctx, h.conn, req, h.cuentas)
}

// HandlePersonaPorID devuelve una persona
func (h *Handler) ReadOne(ctx context.Context, req personas.ReadOneReq) (out personas.Persona, err error) {

	// Busco en cache
	key := hashUUID(req)
	val, estaba := h.cache.Get(key)
	if estaba {
		out, ok := val.(personas.Persona)
		if ok && out.Comitente == req.Comitente {
			return out, nil
		}
	}

	// Persona
	out, err = db.ReadOne(ctx, h.conn, req.Comitente, req.ID)
	if err != nil {
		return out, errors.Wrap(err, "buscando persona por ID")
	}

	// Cuentas asociadas
	out.CuentasAsociadas, err = cuentasasociadas.PorPersona(ctx, h.conn, req.Comitente, req.ID)
	if err != nil {
		return out, errors.Wrap(err, "buscando cuentas asociadas")
	}

	// Busco sucursales en DB
	if out.UsaSucursales {
		out.Sucursales, err = sucursales.ReadMany(ctx, h.conn,
			sucursales.ReadManyReq{Comitente: req.Comitente, Persona: req.ID},
		)
		if err != nil {
			return out, errors.Wrap(err, "buscando sucursales")
		}
	}

	// Fijo en cache
	h.cache.Set(key, out, 0)

	return
}

// HandlePersonaModificar devuelve una persona
func (h *Handler) Update(ctx context.Context, req personas.Persona, usuario string) (err error) {

	// Valido
	if req.ID == uuid.Nil {
		return deferror.Validation("no se ingresó ID de persona")
	}

	err = validarPersona(&req)
	if err != nil {
		return errors.Wrap(err, "validando")
	}

	// Traigo anterior para loggear diferencias
	p1, err := db.ReadOne(ctx, h.conn, req.Comitente, req.ID)
	if err != nil {
		return fmt.Errorf("buscando persona a modificar: %w", err)
	}

	// Actualizo base de datos
	err = db.Update(ctx, h.conn, req)
	if err != nil {
		return errors.Wrap(err, "base de datos")
	}

	// Log
	changelog, err := diff.Diff(p1, req)
	if err != nil {
		log.Error(ctx, fmt.Errorf("diffing persona: %w", err))
	} else {
		by, err := json.Marshal(changelog)
		if err != nil {
			log.Error(ctx, fmt.Errorf("marshaling persona: %w", err))
		}
		log.Info(ctx, "persona modificada", slog.Any("changelog", string(by)))
	}

	// Invalido cache
	h.cache.Del(hashUUID(personas.ReadOneReq{Comitente: req.Comitente, ID: req.ID}))

	if h.emitirEventos {
		h.bus.Emit(personas.EvPersonaModificada{
			Persona: req,
			Usuario: usuario,
		})
	}

	return
}

func (h *Handler) Delete(ctx context.Context, req personas.ReadOneReq, usuario string) (err error) {

	// Traigo datos para broadcast
	p, err := h.ReadOne(ctx, req)
	if err != nil {
		return errors.Wrap(err, "buscando persona")
	}

	err = db.Delete(ctx, h.conn, req)
	if err != nil {
		return errors.Wrap(err, "base de datos")
	}

	// Invalido cache
	h.cache.Del(hashUUID(personas.ReadOneReq(req)))

	//
	if h.emitirEventos {
		h.bus.Emit(personas.EvPersonaEliminada{
			Persona: p,
			Usuario: usuario,
		})
	}

	log.Info(ctx, "persona eliminada", slog.String("id", req.ID.String()))

	return
}
func (h *Handler) existePersona(ctx context.Context, p *personas.Persona) (existe bool, err error) {

	if p.CUIT == 0 && p.DNI == 0 {
		return
	}

	return db.ExistePersona(ctx, h.conn, p.CUIT, p.DNI, p.Comitente)
}

func validarPersona(p *personas.Persona) (err error) {
	if p == nil {
		return deferror.Validation("no se ingresó persona")
	}

	if p.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}
	if p.Nombre == "" {
		return deferror.Validation("debe ingresarse un nombre legal")
	}
	switch p.Tipo {
	case personas.TipoPersonaHumana, personas.TipoPersonaJuridica:
		// Ok
	default:
		return deferror.Validation("debe definir si se tranta de una persona humana o jurídica")
	}
	if p.CUIT != 0 && p.TipoCUIT == nil {
		return deferror.Validation("si ingresa un número de CUIT/CUIL debe definir el tipo")
	}
	if p.TipoCUIT != nil && p.CUIT == 0 {
		return deferror.Validation("si selecciónó un tipo de CUIT/CUIL debe ingresar el número, de lo contrario elimine tipo")
	}
	if p.DNI != 0 && p.TipoDNI == nil {
		return deferror.Validation("si ingresa un número de DNI debe definir el tipo")
	}
	if p.DNI < 0 {
		return deferror.Validation("DNI no puede ser menor a cero")
	}
	if p.CUIT != 0 && !p.CUIT.Valid() {
		return deferror.Validation("el CUIT ingresado es inválido")
	}
	// Es humana? Extraigo DNI
	if p.Tipo == personas.TipoPersonaHumana && p.CUIT != 0 {
		p.DNI, err = p.CUIT.ExtraerDNI()
		if err != nil {
			return errors.Wrap(err, "extrayendo número de DNI del CUIT")
		}
	}
	if p.Tipo == personas.TipoPersonaJuridica && p.DNI != 0 {
		return deferror.Validation("si es persona jurídica no puede cargar número de documento")
	}

	{ // Domicilio
		legalCount := 0
		for _, v := range p.Domicilios {
			if v.Tipo == "Legal" {
				legalCount++
			}
			fijarDomicilio(v, p)
		}
		if legalCount > 1 {
			return deferror.Validation("sólo puede haber un domicilio legal")
		}
		if len(p.Domicilios) == 1 {
			fijarDomicilio(p.Domicilios[0], p)
		}
	}

	return
}

func fijarDomicilio(dom domicilios.Domicilio, per *personas.Persona) {
	per.CiudadNombre = dom.CiudadNombre
	per.CiudadCP = dom.CiudadCP
	per.CiudadID = dom.CiudadID
	per.ProvinciaID = dom.ProvinciaID
	per.ProvinciaNombre = dom.ProvinciaNombre
	per.Calle = dom.DomCalle
	per.Numero = dom.DomNumero
	per.Piso = dom.DomPiso
	per.Depto = dom.DomDpto
}

func hashUUID(req personas.ReadOneReq) string {
	return fmt.Sprintf("%v/%v/%x", "Persona", req.Comitente, req.ID)
}
