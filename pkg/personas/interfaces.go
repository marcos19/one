package personas

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/cuentasasociadas"
	"bitbucket.org/marcos19/one/pkg/telefonos"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/crosslogic/cuits"
	"github.com/gofrs/uuid"
)

// Devuelve una persona
type ReaderOne interface {
	ReadOne(context.Context, ReadOneReq) (Persona, error)
}

// Devuelve varias personas. Es para buscar
type ReaderMany interface {
	ReadMany(context.Context, ReadManyReq) ([]ReadManyResp, error)
}

// typ
type Creater interface {
	Create(context.Context, Persona) error
}

type Updater interface {
	Update(context.Context, Persona) error
}

type Deleter interface {
	Delete(context.Context, DeleteReq) error
}

// Los eventos a este bus.
type Bus interface {
	Emit(event any)
}

type ReadOneReq struct {
	ID        uuid.UUID
	Comitente int
}

type ReadManyReq struct {
	Texto     string
	Tipo      string
	Cuentas   tipos.GrupoInts // Aquellas personas que tienen permitida esta cuenta
	IDs       []uuid.UUID
	Comitente int
	SinLimite bool
}
type ReadManyResp struct {
	ID               uuid.UUID
	Nombre           string
	NombreFantasia   string     `json:",string,omitempty"`
	DNI              int        `json:",string,omitempty"`
	CUIT             cuits.CUIT `json:",string,omitempty"`
	Calle            string
	Numero           string
	Piso             string
	Depto            string
	CiudadNombre     string
	ProvinciaNombre  string
	Telefonos        []telefonos.Telefono
	Activo           bool
	UsaSucursales    bool
	CuentasAsociadas cuentasasociadas.Cuentas
}
type DeleteReq struct {
	ID        uuid.UUID
	Comitente int
}
