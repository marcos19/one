// Package personas contiene las definiciones de personas con sus métodos.
package personas

import (
	"time"

	"bitbucket.org/marcos19/one/pkg/afip/afipmodels"
	"bitbucket.org/marcos19/one/pkg/cuentasasociadas"
	"bitbucket.org/marcos19/one/pkg/domicilios"
	"bitbucket.org/marcos19/one/pkg/emails"
	"bitbucket.org/marcos19/one/pkg/sucursales"
	"bitbucket.org/marcos19/one/pkg/telefonos"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/crosslogic/cuits"
	"github.com/crosslogic/dec"

	uuid "github.com/gofrs/uuid"
)

// Tipos de personas
const (
	// TipoPersonaHumana se usa cuando se trata de una persona humana
	TipoPersonaHumana = "H"

	// TipoPersonaJuridica se usa cuando se trata de una persona jurídica
	TipoPersonaJuridica = "J"
)

// Persona contiene los datos de una persona.
type Persona struct {
	ID             uuid.UUID
	Comitente      int
	Nombre         string
	NombreFantasia string
	Tipo           string
	DNI            int        `json:",string,omitempty"`
	TipoDNI        *int       `json:",string,omitempty"`
	CUIT           cuits.CUIT `json:",string,omitempty"`
	TipoCUIT       *int       `json:",string,omitempty"`

	Calle           string
	Numero          string
	Piso            string
	Depto           string
	CiudadID        int
	CiudadCP        int
	CiudadNombre    string
	ProvinciaID     int
	ProvinciaNombre string
	Pais            string

	Domicilios domicilios.Domicilios
	Emails     emails.Emails
	Telefonos  []telefonos.Telefono

	CondicionGcias *CondicionGcias
	CondicionIVA   *afipmodels.CondicionIVA `json:",string,omitempty"`
	CondicionMT    *CondicionMT

	Atts          tipos.JSON
	Observaciones string

	// Funcionamiento
	Activo            bool
	MotivoInactividad string
	UsaSucursales     bool

	SaldoContable   *dec.D2
	SaldoFinanciero *dec.D2

	CuentasAsociadas cuentasasociadas.Cuentas

	Sucursales []sucursales.Sucursal
	CreatedAt  *time.Time
	UpdatedAt  *time.Time
}

func (p *Persona) TipoIdentificacionFiscal() int {

	// Si tiene definido tipo CUIT => devuelvo ese
	if p.TipoCUIT != nil {
		if *p.TipoCUIT != 0 {
			return *p.TipoCUIT
		}
	}

	if p.TipoDNI != nil {
		if *p.TipoDNI != 0 {
			return *p.TipoDNI
		}
	}

	return 99
}

func (p *Persona) NIdentificacionFiscal() int {

	// Si tiene definido tipo CUIT => devuelvo ese
	if p.CUIT != 0 {
		return int(p.CUIT)
	}

	if p.TipoDNI != nil {
		if *p.TipoDNI != 0 {
			return p.DNI
		}
	}

	return 0
}

type CondicionGcias string

const (
	GciasNoInscripto   = CondicionGcias("NI")
	GciasActivo        = CondicionGcias("AC")
	GciasExento        = CondicionGcias("EX")
	GciasNoCorresponde = CondicionGcias("NC")
)

func (c CondicionGcias) String() string {
	switch c {
	case GciasNoInscripto:
		return "No inscripto"
	case GciasActivo:
		return "Activo"
	case GciasExento:
		return "Exento"
	case GciasNoCorresponde:
		return "No corresponde"
	}
	return "N/D"
}

// type CondicionIVA string

// const (
// 	IVANoInscripto       = CondicionIVA("NI")
// 	IVAActivo            = CondicionIVA("AC")
// 	IVAExento            = CondicionIVA("EX")
// 	IVANoAlcanzado       = CondicionIVA("NA")
// 	IVAExentoNoAlcanzado = CondicionIVA("XN")
// 	IVAActivoNoAlcanzado = CondicionIVA("AN")
// )

// func (c CondicionIVA) String() string {
// 	switch c {
// 	case IVANoInscripto:
// 		return "No inscripto"
// 	case IVAActivo:
// 		return "Activo"
// 	case IVAExento:
// 		return "Exento"
// 	case IVANoAlcanzado:
// 		return "No alcanzado"
// 	case IVAExentoNoAlcanzado:
// 		return "Exento no alcanzado"
// 	case IVAActivoNoAlcanzado:
// 		return "Activo no alcanzado"
// 	}
// 	return "N/D"
// }

type CondicionMT string

const (
	MTNoInscripto = CondicionMT("NI")
	MTInscripto   = CondicionMT("AC")
)

func (p Persona) DomicilioString() string {
	ub := domicilios.Domicilio{
		DomCalle:     p.Calle,
		DomNumero:    p.Numero,
		DomPiso:      p.Piso,
		DomDpto:      p.Depto,
		CiudadNombre: p.CiudadNombre,
		CiudadCP:     p.CiudadCP,
	}
	return ub.String()
}
