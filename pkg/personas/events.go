package personas

type EvPersonaCreada struct {
	Persona Persona
	Usuario string
}

type EvPersonaModificada struct {
	Persona Persona
	Usuario string
}

type EvPersonaEliminada struct {
	Persona Persona
	Usuario string
}
