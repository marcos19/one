package resumencaja

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/cajas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	uuid "github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

type Resumen struct {
	Cuenta   int `json:",string"`
	Inicio   dec.D2
	Entradas dec.D2
	Salidas  dec.D2
	Final    dec.D2

	InicioOrig   dec.D4
	EntradasOrig dec.D4
	SalidasOrig  dec.D4
	FinalOrig    dec.D4
}

type MovimientoDeCaja struct {
	ID        uuid.UUID
	Fecha     fecha.Fecha
	TS        time.Time
	Cuenta    int `json:",string"`
	Moneda    int
	Monto     dec.D2
	MontoOrig dec.D4
	Saldo     dec.D2
	SaldoOrig dec.D4
	Comp      *string `json:",omitempty"`
	NComp     *string `json:",omitempty"`
	Detalle   *string `json:",omitempty"`
	Persona   *string `json:",omitempty"`
}

type Req struct {
	Comitente int
	Empresa   *int `json:",string"`
	Cajas     tipos.GrupoInts
	Cuenta    *int `json:",string"` // Es para mostrar menos movimientos
	Moneda    *int `json:",string"` // Es para mostrar menos movimientos
	Cuentas   tipos.GrupoInts
	Desde     *fecha.Fecha
	Hasta     *fecha.Fecha
	Agrupado  bool
}

type Response struct {
	Saldos  map[string][]Resumen
	Totales map[string]Resumen
	Movs    []MovimientoDeCaja
}

func Informe(ctx context.Context, conn *pgxpool.Pool, req Req, reader cajas.ReaderOne) (out Response, err error) {
	if req.Comitente == 0 {
		return out, errors.Errorf("no se ingresó comitente")
	}
	if req.Desde != nil && req.Hasta != nil {
		if *req.Desde > *req.Hasta {
			return out, errors.Errorf("la fecha hasta debe ser posterior que la fecha desde")
		}
	}
	if len(req.Cajas) == 0 {
		return out, errors.Errorf("debe seleccionar alguna caja")
	}

	// Busco la caja para determinar las cuentas adicionales
	ctasUsadas := map[int]struct{}{}
	for _, v := range req.Cajas {
		c, err := reader.ReadOne(ctx, cajas.ReadOneReq{Comitente: req.Comitente, ID: v})
		if err != nil {
			return out, errors.Wrap(err, "buscando caja")
		}
		for _, ctaID := range c.OtrasCuentas {
			ctasUsadas[ctaID] = struct{}{}
		}
	}
	for k := range ctasUsadas {
		req.Cuentas = append(req.Cuentas, k)
	}

	// Inicio tx
	tx, err := conn.Begin(ctx)
	if err != nil {
		return out, errors.Wrap(err, "iniciando tx")
	}
	defer tx.Rollback(ctx)

	// Saldos de cierre
	cierre, err := traerSaldosCierre(ctx, tx, req)
	if err != nil {
		return out, errors.Wrap(err, "trayendo saldos cierre")
	}

	// Movimientos posteriores
	posteriores, err := traerMovimientosPosteriores(ctx, tx, req)
	if err != nil {
		return out, errors.Wrap(err, "trayendo movimientos posteriores")
	}

	// Traigo movimientos
	var mm []MovimientoDeCaja
	if req.Agrupado {
		mm, err = traerMovimientosResumidos(ctx, tx, req)
		if err != nil {
			return out, errors.Wrap(err, "trayendo movimientos resumidos")
		}
	} else {
		mm, err = traerMovimientosDetallados(ctx, tx, req)
		if err != nil {
			return out, errors.Wrap(err, "trayendo movimientos detallados")
		}
	}

	monedasUsadas := map[int]struct{}{}

	// Sumo entradas y salidas dentro del período
	ent := map[key]val{}
	sal := map[key]val{}
	for _, v := range mm {
		k := key{cuenta: v.Cuenta, moneda: v.Moneda}
		if v.Monto >= 0 {
			prev := ent[k]
			prev.monto += v.Monto
			prev.montoOrig += v.MontoOrig
			ent[k] = prev
		}
		if v.Monto < 0 {
			prev := sal[k]
			prev.monto += v.Monto
			prev.montoOrig += v.MontoOrig
			sal[k] = prev
		}
	}

	// Anoto cuentas y monedas usadas
	for k := range cierre {
		ctasUsadas[k.cuenta] = struct{}{}
		monedasUsadas[k.moneda] = struct{}{}
	}
	for _, v := range mm {
		ctasUsadas[v.Cuenta] = struct{}{}
		monedasUsadas[v.Moneda] = struct{}{}
	}

	// Ajusto saldos de cierre
	for k, v := range posteriores {
		ctasUsadas[k.cuenta] = struct{}{}
		monedasUsadas[k.moneda] = struct{}{}
		prev, ok := cierre[k]
		if !ok {
			continue
		}
		prev.monto -= v.monto
		prev.montoOrig -= v.montoOrig
		cierre[k] = prev
	}

	// Calculo saldos de inicio
	inicio := map[key]val{}
	for k, v := range cierre {
		inicio[k] = v
	}
	for _, v := range mm {
		k := key{cuenta: v.Cuenta, moneda: v.Moneda}
		ctasUsadas[k.cuenta] = struct{}{}
		monedasUsadas[k.moneda] = struct{}{}
		prev := inicio[k]
		prev.monto -= v.Monto
		prev.montoOrig -= v.MontoOrig
		inicio[k] = prev
	}

	porMoneda := map[int][]Resumen{}
	for moneda := range monedasUsadas {

		for cuenta := range ctasUsadas {
			res := Resumen{Cuenta: cuenta}

			k := key{cuenta: cuenta, moneda: moneda}

			{ // Inicio
				v := inicio[k]
				res.Inicio = v.monto
				res.InicioOrig = v.montoOrig
			}
			{ // Entradas
				v := ent[k]
				res.Entradas = v.monto
				res.EntradasOrig = v.montoOrig
			}
			{ // Salidas
				v := sal[k]
				res.Salidas = v.monto
				res.SalidasOrig = v.montoOrig
			}
			{ // Final
				v := cierre[k]
				res.Final = v.monto
				res.FinalOrig = v.montoOrig
			}
			prev := porMoneda[moneda]
			prev = append(prev, res)
			porMoneda[moneda] = prev
		}
	}

	// Oculto los renglones que estaban todos en cero
	porMoneda = quitarVacias(porMoneda)

	saldoInicio := dec.D2(0)
	saldoInicioOrig := dec.D4(0)

	// Saldos
	out.Saldos = map[string][]Resumen{}
	out.Totales = map[string]Resumen{}
	for k, vv := range porMoneda {
		out.Saldos[strconv.Itoa(k)] = vv
		sum := Resumen{}
		for k, v := range vv {
			sum.Cuenta = k
			sum.Inicio += v.Inicio
			sum.Entradas += v.Entradas
			sum.Salidas += v.Salidas
			sum.Final += v.Final
			sum.InicioOrig += v.InicioOrig
			sum.EntradasOrig += v.EntradasOrig
			sum.SalidasOrig += v.SalidasOrig
			sum.FinalOrig += v.FinalOrig
			saldoInicio += v.Inicio
			saldoInicioOrig += v.InicioOrig
		}
		out.Totales[strconv.Itoa(k)] = sum
	}

	if req.Cuenta != nil {
		moneda := 0
		if req.Moneda != nil {
			moneda = *req.Moneda
		}
		key := key{cuenta: *req.Cuenta, moneda: moneda}
		tot := inicio[key]
		saldoInicioOrig = tot.montoOrig
		saldoInicio = tot.monto
	}

	out.Movs = []MovimientoDeCaja{}
	// Movimientos
	for _, v := range mm {
		// Estoy filtrando una cuenta en particular?
		if req.Cuenta != nil {
			if v.Cuenta != *req.Cuenta {
				continue
			}
		}
		saldoInicio += v.Monto
		v.Saldo = saldoInicio

		saldoInicioOrig += v.MontoOrig
		v.SaldoOrig = saldoInicioOrig

		out.Movs = append(out.Movs, v)
	}

	return
}

type key struct {
	cuenta int
	moneda int
}
type val struct {
	monto     dec.D2
	montoOrig dec.D4
}

// Trae saldos de la tabla saldos.
func traerSaldosCierre(ctx context.Context, tx pgx.Tx, req Req) (out map[key]val, err error) {
	out = map[key]val{}

	ww, pp := filtro(req)

	where := fmt.Sprintf(" WHERE %v", strings.Join(ww, " AND "))
	query := fmt.Sprintf(`SELECT cuenta, moneda, SUM(monto)::INT64, SUM(monto_orig)::INT64 FROM saldos %v GROUP BY cuenta, moneda`, where)

	// Busco
	rows, err := tx.Query(ctx, query, pp...)
	if err != nil {
		return out, errors.Wrap(err, "querying")
	}
	defer rows.Close()

	// Escaneo
	for rows.Next() {
		k := key{}
		v := val{}
		err = rows.Scan(&k.cuenta, &k.moneda, &v.monto, &v.montoOrig)
		if err != nil {
			return out, errors.Wrap(err, "escaneando")
		}
		out[k] = v
	}

	return
}

// Trae los saldos de los movimientos posteriores a la fecha solicitada
func traerMovimientosPosteriores(ctx context.Context, tx pgx.Tx, req Req) (out map[key]val, err error) {

	out = map[key]val{}
	if req.Hasta == nil {
		return out, errors.Errorf("debe ingresar fecha hasta")
	}

	ww, pp := filtro(req)
	{ // Fecha hasta
		ww = append(ww, fmt.Sprintf("fecha_contable > $%v", len(pp)+1))
		pp = append(pp, *req.Hasta)
	}

	where := fmt.Sprintf(" WHERE %v", strings.Join(ww, " AND "))
	query := fmt.Sprintf(`SELECT cuenta, moneda, SUM(monto)::INT64, SUM(monto_moneda_original)::INT64 FROM partidas %v GROUP BY cuenta, moneda`, where)

	// Busco
	rows, err := tx.Query(ctx, query, pp...)
	if err != nil {
		return out, errors.Wrap(err, "querying")
	}
	defer rows.Close()

	// Escaneo
	for rows.Next() {
		k := key{}
		v := val{}
		var moneda *int
		err = rows.Scan(&k.cuenta, &moneda, &v.monto, &v.montoOrig)
		if moneda != nil {
			k.moneda = *moneda
		}
		if err != nil {
			return out, errors.Wrap(err, "escaneando")
		}
		out[k] = v
	}

	return
}

// Agrupa por comprobante. Es práctico si por ejemplo se cobraron 20 cheques,
// los muestra como un sólo renglón.
func traerMovimientosResumidos(ctx context.Context, tx pgx.Tx, req Req) (out []MovimientoDeCaja, err error) {

	ww, pp := filtroMovimientos(req)
	where := fmt.Sprintf(" WHERE %v", strings.Join(ww, " AND "))

	queryMovs := fmt.Sprintf(`
SELECT ops.id, ops.fecha, ops.created_at, partidas.cuenta, partidas.moneda,
	ops.comp_nombre, ops.n_comp_str, ops.persona_nombre, ops.detalle,
	SUM(partidas.monto)::INT64, SUM(partidas.monto_moneda_original)::INT64
FROM partidas 
INNER JOIN ops ON ops.id = partidas.op_id
%v 
GROUP BY ops.id, ops.fecha, ops.created_at, partidas.cuenta, partidas.moneda,
	ops.comp_nombre, ops.n_comp_str, ops.persona_nombre, ops.detalle
ORDER BY ops.fecha, ops.created_at;`, where)

	// Busco los movimientos
	rows, err := tx.Query(ctx, queryMovs, pp...)
	if err != nil {
		return out, deferror.DB(err, "buscando detalle de movimientos")
	}
	defer rows.Close()

	// Escaneo
	for rows.Next() {
		m := MovimientoDeCaja{}
		detOp := new(string)
		moneda := new(int)
		err = rows.Scan(&m.ID, &m.Fecha, &m.TS, &m.Cuenta, &moneda,
			&m.Comp, &m.NComp, &m.Persona, &detOp, &m.Monto, &m.MontoOrig)
		if err != nil {
			return out, deferror.DB(err, "escaneando row de movimiento de caja")
		}

		if moneda != nil {
			m.Moneda = *moneda
		}
		// Detalle
		det := []string{}
		if m.Persona != nil {
			if *m.Persona != "" {
				det = append(det, *m.Persona)
			}
		}
		if detOp != nil {
			if *detOp != "" {
				det = append(det, *detOp)
			}
		}
		if len(det) > 0 {
			m.Detalle = new(string)
			*m.Detalle = strings.Join(det, " - ")
		}
		out = append(out, m)
	}

	return
}

func traerMovimientosDetallados(ctx context.Context, tx pgx.Tx, req Req) (out []MovimientoDeCaja, err error) {

	ww, pp := filtroMovimientos(req)
	where := fmt.Sprintf(" WHERE %v", strings.Join(ww, " AND "))

	queryMovs := fmt.Sprintf(`
SELECT partidas.id, partidas.fecha_contable, partidas.created_at, partidas.cuenta,
partidas.moneda, ops.comp_nombre, ops.n_comp_str, ops.persona_nombre, ops.detalle, partidas.detalle,
partidas.monto, partidas.monto_moneda_original  
FROM partidas 
INNER JOIN ops ON ops.id = partidas.op_id
%v 
ORDER BY fecha_contable, created_at;`, where)

	// Busco los movimientos
	rows, err := tx.Query(ctx, queryMovs, pp...)
	if err != nil {
		return out, deferror.DB(err, "buscando detalle de movimientos")
	}
	defer rows.Close()

	// Escaneo
	for rows.Next() {
		m := MovimientoDeCaja{}
		detPartida := new(string)
		detOp := new(string)
		var moneda *int
		err = rows.Scan(&m.ID, &m.Fecha, &m.TS, &m.Cuenta, &moneda, &m.Comp, &m.NComp, &m.Persona, &detPartida, &detOp, &m.Monto, &m.MontoOrig)
		if err != nil {
			return out, deferror.DB(err, "escaneando row de movimiento de caja")
		}
		if moneda != nil {
			m.Moneda = *moneda
		}
		det := []string{}
		if detPartida != nil {
			if *detPartida != "" {
				det = append(det, *detPartida)
			}
		}
		if detOp != nil {
			if *detOp != "" {
				det = append(det, *detOp)
			}
		}

		if len(det) > 0 {
			m.Detalle = new(string)
			*m.Detalle = strings.Join(det, " - ")
		}
		out = append(out, m)
	}

	return
}

func filtro(req Req) (ww []string, pp []any) {

	ww = append(ww, fmt.Sprintf("comitente = $%v", len(pp)+1))
	pp = append(pp, req.Comitente)

	// Empresa
	if req.Empresa != nil {
		ww = append(ww, fmt.Sprintf("empresa = $%v", len(pp)+1))
		pp = append(pp, *req.Empresa)
	}

	// Caja
	if len(req.Cajas) > 0 {
		ww = append(ww, fmt.Sprintf(
			"(caja IN %v OR cuenta IN %v)",
			req.Cajas.ParaClausulaIn(),
			req.Cuentas.ParaClausulaIn(),
		))
	} else {
		ww = append(ww, fmt.Sprintf("(caja <> 0 OR cuenta IN %v)", req.Cuentas.ParaClausulaIn()))
	}

	// Cuentas
	//if len(req.Cuentas) > 0 {
	//ww = append(ww, fmt.Sprintf("cuenta IN %v", req.Cuentas.ParaClausulaIn()))
	//}

	return
}

func filtroMovimientos(req Req) (ww []string, pp []any) {

	ww = append(ww, fmt.Sprintf("partidas.comitente = $%v", len(pp)+1))
	pp = append(pp, req.Comitente)

	if req.Empresa != nil {
		ww = append(ww, fmt.Sprintf("partidas.empresa = $%v", len(pp)+1))
		pp = append(pp, *req.Empresa)
	}

	if req.Desde != nil {
		ww = append(ww, fmt.Sprintf("partidas.fecha_contable >= $%v", len(pp)+1))
		pp = append(pp, *req.Desde)
	}

	if req.Hasta != nil {
		ww = append(ww, fmt.Sprintf("partidas.fecha_contable <= $%v", len(pp)+1))
		pp = append(pp, *req.Hasta)
	}

	// Caja
	if len(req.Cajas) > 0 {
		ww = append(ww, fmt.Sprintf(
			"(caja IN %v OR cuenta IN %v)",
			req.Cajas.ParaClausulaIn(),
			req.Cuentas.ParaClausulaIn(),
		))
	} else {
		ww = append(ww, fmt.Sprintf("(caja <> 0 OR cuenta IN %v)", req.Cuentas.ParaClausulaIn()))
	}

	return
}

// Quita los saldos que están todos en cero
func quitarVacias(in map[int][]Resumen) (out map[int][]Resumen) {

	out = map[int][]Resumen{}

	for k, vv := range in {
		quedan := []Resumen{}
		for _, v := range vv {
			vacía := v.Inicio == 0 &&
				v.Entradas == 0 &&
				v.Salidas == 0 &&
				v.Final == 0 &&
				v.InicioOrig == 0 &&
				v.EntradasOrig == 0 &&
				v.SalidasOrig == 0 &&
				v.FinalOrig == 0
			if vacía {
				continue
			}
			quedan = append(quedan, v)
		}
		out[k] = quedan
	}
	return
}
