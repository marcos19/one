package resumencaja

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/cajas"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/niler"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Handler struct {
	conn         *pgxpool.Pool
	cajasHandler cajas.ReaderOne
}

func NewHandler(conn *pgxpool.Pool, c cajas.ReaderOne) (out *Handler, err error) {
	if niler.IsNil(c) {
		return out, errors.Errorf("cajas.ReaderOne no puede ser nil")
	}
	return &Handler{conn, c}, nil
}

func (h *Handler) Informe(ctx context.Context, req Req) (out Response, err error) {
	return Informe(ctx, h.conn, req, h.cajasHandler)
}
