package vistapersona

import (
	"context"
	"fmt"
	"math"
	"sort"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/contables/aplifin"
	"bitbucket.org/marcos19/one/pkg/contables/mayor"
	"bitbucket.org/marcos19/one/pkg/contables/vistapersona/pdf"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/empresas"
	opshandler "bitbucket.org/marcos19/one/pkg/ops/handler"

	"bitbucket.org/marcos19/one/pkg/personas"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/crosslogic/niler"
	uuid "github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Handler struct {
	conn            *pgxpool.Pool
	opsHandler      *opshandler.Handler
	empresasHandler empresas.ReaderOne
	logoReader      empresas.ReaderLogo
	personasHandler personas.ReaderOne
	mayorHandler    *mayor.Handler
	aplifingetter   aplifin.DisponiblesGetter
}

func NewHandler(
	conn *pgxpool.Pool,
	opsHandler *opshandler.Handler,
	empresasHandler empresas.ReaderOne,
	logoReader empresas.ReaderLogo,
	personasHandler personas.ReaderOne,
	mayorHandler *mayor.Handler,
	aplifingetter aplifin.DisponiblesGetter,
) (h *Handler, err error) {

	if conn == nil {
		return h, deferror.Validation("Conn no puede ser nil")
	}
	if opsHandler == nil {
		return h, deferror.Validation("OpsHandler no puede ser nil")
	}
	if niler.IsNil(empresasHandler) {
		return h, deferror.Validation("EmpresasHandler no puede ser nil")
	}
	if niler.IsNil(logoReader) {
		return h, deferror.Validation("EmpresasHandler no puede ser nil")
	}
	if niler.IsNil(personasHandler) {
		return h, deferror.Validation("PersonasHandler no puede ser nil")
	}
	if mayorHandler == nil {
		return h, deferror.Validation("MayorHandler no puede ser nil")
	}
	if niler.IsNil(aplifingetter) {
		return h, errors.Errorf("aplifin getter no puede ser nil")
	}

	h = &Handler{
		conn:            conn,
		opsHandler:      opsHandler,
		empresasHandler: empresasHandler,
		logoReader:      logoReader,
		personasHandler: personasHandler,
		mayorHandler:    mayorHandler,
		aplifingetter:   aplifingetter,
	}
	return
}

type CuentaCorrienteReq struct {
	Comitente int
	ID        uuid.UUID
	Sucursal  *int `json:",string"`
	Empresa   int  `json:",string"`
}

type saldo struct {
	Cuenta              int
	Centro              int
	Monto               dec.D2
	Anticuacion         int
	AnticuacionPromedio int
	ponderado           float64
}

type CuentaCorrienteResponse struct {
	Partidas []aplifin.Partida
	Saldos   []saldo
}

func (h *Handler) CuentaCorriente(ctx context.Context, req CuentaCorrienteReq) (out CuentaCorrienteResponse, err error) {

	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó comitente")
	}
	// if req.Empresa == 0 {
	// 	return out, deferror.Validation("no se ingresó empresa")
	// }

	// Busco las partidas
	filtro := aplifin.PartidasDisponiblesReq{}
	filtro.Persona = new(uuid.UUID)
	*filtro.Persona = req.ID
	if req.Sucursal != nil {
		filtro.Sucursal = new(int)
		*filtro.Sucursal = *req.Sucursal
	}
	filtro.Empresa = req.Empresa
	filtro.Comitente = req.Comitente

	out.Partidas, err = h.aplifingetter.PartidasDisponibles(ctx, filtro)
	if err != nil {
		return out, errors.Wrap(err, "buscando partidas disponibles")
	}

	// Agrupo las partidas
	type key struct {
		cuenta int
		centro int
	}
	m := map[key]saldo{}
	hoy := fecha.NewFechaFromTime(time.Now())

	for _, v := range out.Partidas {
		ctro := 0
		if v.Centro != nil {
			ctro = *v.Centro
		}
		k := key{cuenta: v.Cuenta, centro: ctro}
		s := m[k]
		s.Monto += v.MontoPendiente

		// Calculo anticuación
		dias := hoy.Menos(v.FechaFinanciera)
		if dias > s.Anticuacion {
			s.Anticuacion = dias
		}
		s.Cuenta = v.Cuenta
		s.Centro = ctro
		s.ponderado += float64(dias) * v.MontoPendiente.Float()
		m[k] = s
	}

	for _, v := range m {
		if v.Monto == 0 {
			continue
		}
		v.AnticuacionPromedio = int(math.Round(v.ponderado / v.Monto.Float()))
		out.Saldos = append(out.Saldos, v)
	}

	return
}

type MovimientosReq struct {
	Comitente int
	PersonaID uuid.UUID
	Sucursal  *int `json:",string"`
	Empresa   int  `json:",string"`
	Desde     fecha.Fecha
	Hasta     fecha.Fecha
	Cuentas   tipos.GrupoInts
	TipoFecha string
	Cantidad  int
}
type Mov struct {
	ID         uuid.UUID
	Fecha      fecha.Fecha
	FechaVto   fecha.Fecha
	CompNombre string
	NComp      string
	Cuentas    int  `json:",string,omitempty"`
	Centro     *int `json:",string,omitempty"`
	Monto      dec.D2
	Saldo      dec.D2
	Detalle    string `json:",omitempty"`
	CreatedAt  time.Time
}

type MovimientosResponse struct {
	SaldoInicioFecha fecha.Fecha
	SaldoInicio      dec.D2
	Movimientos      []Mov
	SaldoFinal       dec.D2
	SaldoFinalFecha  fecha.Fecha
}

func (h *Handler) Movimientos(ctx context.Context, req MovimientosReq) (out MovimientosResponse, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó el ID de comitente")
	}
	if req.PersonaID == uuid.Nil {
		return out, deferror.Validation("No se ingresó el ID de persona")
	}

	// Había en el request alguna cuenta que no estaba en las de recién?

	// Pongo los filtros
	f := mayor.Filtro{
		Comitente: req.Comitente,
		Personas:  []uuid.UUID{req.PersonaID},
	}
	if req.Empresa != 0 {
		f.Empresas = []int{req.Empresa}
	}
	if req.Sucursal != nil {
		if *req.Sucursal != 0 {
			f.Sucursales = []int{*req.Sucursal}
		}
	}

	// Sólo las cuentas que abren por persona y usan aplicaciones.
	rows, err := h.conn.Query(ctx, "SELECT id FROM cuentas WHERE comitente = $1 AND usa_aplicaciones = true AND apertura_persona = true",
		req.Comitente)
	if err != nil {
		return out, deferror.DB(err, "determinando cuentas contables")
	}
	defer rows.Close()

	idMap := map[int]struct{}{}
	idArr := []int{}
	for rows.Next() {
		id := 0
		err = rows.Scan(&id)
		if err != nil {
			return MovimientosResponse{}, errors.Wrap(err, "escaneando ID de cuenta")
		}
		idMap[id] = struct{}{}
		idArr = append(idArr, id)
	}

	// Corroboro
	for _, v := range req.Cuentas {
		_, estaba := idMap[v]
		if !estaba {
			return out, deferror.Validationf("la cuenta contable debe tener apertura por persona y usar apicaciones, la cuenta %v no cumplía", v)
		}
	}
	if len(req.Cuentas) != 0 {
		f.Cuentas = req.Cuentas
	} else {
		f.Cuentas = idArr
	}

	// Sino lo pongo no me trae saldos finales
	f.IncluirSaldosInicio = true

	// Estos cuatro no deberían afectar la consulta, pero por las dudas los pongo.
	f.IncluirApertura = true
	f.IncluirCierre = true
	f.IncluirRefundicion = true
	f.IncluirInflacion = true

	switch req.TipoFecha {
	case "fecha_contable":
		f.FechaDesde = req.Desde
		f.FechaHasta = req.Hasta

	case "fecha_financiera":
		f.FechaFinancieraDesde = req.Desde
		f.FechaFinancieraHasta = req.Hasta

	default:
		return out, deferror.Validation("No se determinó el tipo de fecha a usar")
	}

	// Mayorizo
	mayor, err := h.mayorHandler.Mayorizar(ctx, f)
	if err != nil {
		return out, err
	}

	// Agrupo por comprobante
	type key struct {
		ID            uuid.UUID
		FechaContable fecha.Fecha
		FechaVto      fecha.Fecha
		CompNombre    string
		NCompStr      string
		DetalleComp   string
		CreatedAt     time.Time
	}
	m := map[key]dec.D2{}
	for _, v := range mayor.Partidas {

		k := key{
			ID:          v.OpID,
			CompNombre:  v.CompNombre,
			NCompStr:    v.NCompStr,
			DetalleComp: v.DetalleComp,
			CreatedAt:   v.CreatedAt,
		}
		// if req.TipoFecha == "fecha_contable" {
		k.FechaContable = v.FechaContable
		// }
		if req.TipoFecha == "fecha_financiera" {
			k.FechaVto = v.FechaVto
		}
		m[k] += v.Monto
	}

	// Creo slice
	mm := []Mov{}
	for k, v := range m {
		mov := Mov{
			ID:         k.ID,
			Fecha:      k.FechaContable,
			FechaVto:   k.FechaVto,
			CompNombre: k.CompNombre,
			NComp:      k.NCompStr,
			Monto:      v,
			Detalle:    k.DetalleComp,
			CreatedAt:  k.CreatedAt,
		}
		mm = append(mm, mov)
	}

	// Lo ordeno
	sort.Slice(mm, func(i, j int) bool {
		switch req.TipoFecha {

		case "fecha_contable":
			if mm[i].Fecha < mm[j].Fecha {
				return true
			}
			if mm[i].Fecha > mm[j].Fecha {
				return false
			}
			// Tienen la misma fecha, ordeno por ID
			return mm[i].CreatedAt.Before(mm[j].CreatedAt)

		case "fecha_financiera":
			if mm[i].FechaVto < mm[j].FechaVto {
				return true
			}
			if mm[i].FechaVto > mm[j].FechaVto {
				return false
			}
			// Tienen la misma fecha, ordeno por creación
			return mm[i].CreatedAt.Before(mm[j].CreatedAt)
		}
		return false
	})

	// Pongo los saldos
	acum := mayor.SaldoInicio
	for i := range mm {
		acum += mm[i].Monto
		mm[i].Saldo = acum

	}
	out.Movimientos = mm
	if len(mm) > 0 {
		if mayor.SaldoFinal != mm[len(mm)-1].Saldo {
			return out, errors.Errorf("no me coinciden saldos. Según mayor: %v; según movs: %v", mayor.SaldoFinal, mm[len(mm)-1].Saldo)
		}
	}

	if out.SaldoInicioFecha.IsValid() {
		out.SaldoInicioFecha = req.Desde.AgregarDias(-1)
	}
	out.SaldoInicio = mayor.SaldoInicio
	out.Movimientos = mm
	out.SaldoFinal = mayor.SaldoFinal
	out.SaldoFinalFecha = req.Hasta

	return

}

func (h *Handler) MovimientosPDF(ctx context.Context, req MovimientosReq) (out pdf.Outputer, err error) {

	// if req.Hasta == nil {
	// 	return out, deferror.Validation("no se ingresó fecha hasta")
	// }

	// Busco movimientos
	movs, err := h.Movimientos(ctx, req)
	if err != nil {
		return out, err
	}

	f := pdf.New(pdf.DefaultFormatoArgs())

	f.TipoFecha = req.TipoFecha
	f.FechaDesde = req.Desde
	f.FechaHasta = req.Hasta

	// Traigo logo
	logo, err := h.logoReader.ReadLogo(context.Background(), empresas.ReadLogoReq{
		Comitente: req.Comitente,
		Empresa:   req.Empresa,
	})
	if err != nil {
		return nil, errors.Wrap(err, "buscando empresa")
	}
	f.Logo = logo
	f.LogoFormato = "png"

	// Traigo empresa
	emp, err := h.empresasHandler.ReadOne(ctx, empresas.ReadOneReq{
		Comitente: req.Comitente,
		ID:        req.Empresa,
	})
	if err != nil {
		return out, errors.Wrap(err, "trayendo empresa")
	}

	f.EmpresaNombre = emp.ImpresionNombre
	f.HeaderRenglonesLeft = strings.Split(emp.ImpresionRenglonesLeft, "\n")
	f.HeaderRenglonesRight = strings.Split(emp.ImpresionRenglonesRight, "\n")

	// Traigo persona
	per, err := h.personasHandler.ReadOne(ctx, personas.ReadOneReq{
		Comitente: req.Comitente,
		ID:        req.PersonaID,
	})
	if err != nil {
		return out, errors.Wrap(err, "buscando persona")
	}
	f.PersonaNombre = per.Nombre
	f.PersonaCamposLeft = []string{}

	// Sucursal
	if req.Sucursal != nil {
		for _, v := range per.Sucursales {
			if *v.ID == *req.Sucursal {
				f.PersonaNombre += fmt.Sprintf(" (%v)", v.Nombre)
				f.PersonaCamposLeft = append(f.PersonaCamposLeft, fmt.Sprintf(
					"Domicilio: %v", v.DomicilioString()),
				)
				break
			}
		}
	} else {
		// "CUIT: 20-32889647-9",
		// "Domicilio: Gral Roca 657 - LAS ROSAS (2520) Santa Fe",
		// "Condición frente IVA: Responsable inscripto",
		f.PersonaCamposLeft = append(f.PersonaCamposLeft, fmt.Sprintf(
			"Domicilio: %v", per.DomicilioString()),
		)
	}

	per.TipoIdentificacionFiscal()

	f.SaldoInicio = movs.SaldoInicio
	saldo := f.SaldoInicio
	for _, v := range movs.Movimientos {
		r := pdf.Renglon{}
		r.FechaContable = v.Fecha
		r.FechaVto = v.FechaVto
		r.Comp = v.CompNombre
		r.NComp = v.NComp
		if (v.Monto) > 0 {
			r.Debe = v.Monto
		} else {
			r.Haber = -v.Monto
		}
		saldo += v.Monto

		r.Saldo = saldo
		r.Detalle = v.Detalle
		f.Renglones = append(f.Renglones, r)
	}
	f.SaldoFinal = saldo

	return &f, nil
}

type Filtro struct {
	Comitente int
	// Moneda string

	FechaDesde fecha.Fecha
	FechaHasta fecha.Fecha
	// // FechaAnterior           fecha.Fecha
	FechaFinancieraDesde fecha.Fecha
	FechaFinancieraHasta fecha.Fecha
	// // FechaFinancieraAnterior fecha.Fecha
	Empresas []int
	// Funcion   string
	// Cuentas   []int
	// Cajas     []int
	// Depositos []int
	// // Conceptos               []int
	Personas []uuid.UUID
	// Sucursales []int
	// Productos  []uuid.UUID
	// Skus       []uuid.UUID

	// MostarInflacion bool
	// // Sólo se devuelven los campos especificados
	// Fields                 []string
	// OrderByFechaFinanciera bool
	// Limit                  int
}

func (h *Handler) Ventas(ctx context.Context, req VentasReq) (out []VentasResp, err error) {
	return ventas(ctx, h.conn, req)
}
