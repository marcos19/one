package pdf

import (
	"io"
	"os"
	"testing"

	"github.com/crosslogic/fecha"
	"github.com/stretchr/testify/assert"
)

func TestFactura(t *testing.T) {

	f := New(DefaultFormatoArgs())
	f.EmpresaNombre = "Rafaela Revisión Técnica Vehicular SA"

	f.HeaderRenglonesLeft = []string{
		"Domicilio Taller: Ruta Provincial Nº 13 - KM 1.",
		"Domicilio Legal: Gral. Roca 657 - Las Rosas (2520) SF",
		"contacto@rrtvtecnica.com - 03401-449424 / 25",
	}
	f.HeaderRenglonesRight = []string{
		"Fecha Inicio Actividades: 01/11/2012",
		"CUIT: 30-71152196-4",
		"Ingresos Brutos: 121-016857-1",
	}
	f.FechaDesde = fecha.Fecha(20200621)

	f.PersonaNombre = "BORTOLUSSI MARCOS"
	f.PersonaCamposLeft = []string{
		"CUIT: 20-32889647-9",
		"Domicilio: Gral Roca 657 - LAS ROSAS (2520) Santa Fe",
		"Condición frente IVA: Responsable inscripto",
	}
	f.PersonaCamposRight = []string{
		// "Condición de pago: Cuenta corriente",
	}

	f.Renglones = []Renglon{
		{
			FechaContable: 20200109,
			FechaVto:      20210109,
			Comp:          "Factura A",
			NComp:         "0002-00000516",
			Debe:          51645381,
			Saldo:         516512,
			Detalle:       "Este es un comprobante de prueba que se usó para probar",
		}, {
			FechaContable: 20200109,
			FechaVto:      20210109,
			Comp:          "Factura A",
			NComp:         "0002-00000516",
			Debe:          51645381,
			Saldo:         516512,
			Detalle:       "Este es un comprobante de prueba que se usó para probar",
		}, {
			FechaContable: 20200109,
			FechaVto:      20210109,
			Comp:          "Factura A",
			NComp:         "0002-00000516",
			Debe:          51645381,
			Saldo:         516512,
		}, {
			FechaContable: 20200109,
			FechaVto:      20210109,
			Comp:          "Factura A",
			NComp:         "0002-00000516",
			Debe:          51645381,
			Saldo:         516512,
			Detalle:       "Este es un comprobante de prueba que se usó para probar",
		}, {
			FechaContable: 20200109,
			FechaVto:      20210109,
			Comp:          "Factura A",
			NComp:         "0002-00000516",
			Debe:          51645381,
			Saldo:         516512,
			Detalle:       "Este es un comprobante de prueba que se usó para probar",
		}, {
			FechaContable: 20200109,
			FechaVto:      20210109,
			Comp:          "Factura A",
			NComp:         "0002-00000516",
			Debe:          51645381,
			Saldo:         516512,
			Detalle:       "Este es un comprobante de prueba que se usó para probar",
		}, {
			FechaContable: 20200109,
			FechaVto:      20210109,
			Comp:          "Factura A",
			NComp:         "0002-00000516",
			Debe:          51645381,
			Saldo:         516512,
			Detalle:       "Este es un comprobante de prueba que se usó para probar",
		}, {
			FechaContable: 20200109,
			FechaVto:      20210109,
			Comp:          "Factura A",
			NComp:         "0002-00000516",
			Debe:          51645381,
			Saldo:         516512,
			Detalle:       "Este es un comprobante de prueba que se usó para probar",
		}, {
			FechaContable: 20200109,
			FechaVto:      20210109,
			Comp:          "Factura A",
			NComp:         "0002-00000516",
			Debe:          51645381,
			Saldo:         516512,
			Detalle:       "Este es un comprobante de prueba que se usó para probar",
		}, {
			FechaContable: 20200109,
			FechaVto:      20210109,
			Comp:          "Factura A",
			NComp:         "0002-00000516",
			Debe:          51645381,
			Saldo:         516512,
			Detalle:       "Este es un comprobante de prueba que se usó para probar",
		}, {
			FechaContable: 20200109,
			Comp:          "Factura A",
			NComp:         "0002-00000516",
			Debe:          51645381,
			Saldo:         516512,
			Detalle:       "Este es un comprobante de prueba que se usó para probar",
		}, {
			FechaContable: 20200109,
			Comp:          "Factura A",
			NComp:         "0002-00000516",
			Debe:          51645381,
			Saldo:         516512,
			Detalle:       "Este es un comprobante de prueba que se usó para probar",
		}, {
			FechaContable: 20200109,
			Comp:          "Factura A",
			NComp:         "0002-00000516",
			Debe:          51645381,
			Saldo:         516512,
			Detalle:       "Este es un comprobante de prueba que se usó para probar",
		}, {
			FechaContable: 20200109,
			Comp:          "Factura A",
			NComp:         "0002-00000516",
			Debe:          51645381,
			Saldo:         516512,
			Detalle:       "Este es un comprobante de prueba que se usó para probar",
		}, {
			FechaContable: 20200109,
			Comp:          "Factura A",
			NComp:         "0002-00000516",
			Debe:          51645381,
			Saldo:         516512,
			Detalle:       "Este es un comprobante de prueba que se usó para probar",
		}, {
			FechaContable: 20200109,
			Comp:          "Factura A",
			NComp:         "0002-00000516",
			Debe:          51645381,
			Saldo:         516512,
			Detalle:       "Este es un comprobante de prueba que se usó para probar",
		}, {
			FechaContable: 20200109,
			Comp:          "Factura A",
			NComp:         "0002-00000516",
			Debe:          51645381,
			Saldo:         516512,
			Detalle:       "Este es un comprobante de prueba que se usó para probar",
		}, {
			FechaContable: 20200109,
			Comp:          "Factura A",
			NComp:         "0002-00000516",
			Debe:          51645381,
			Saldo:         516512,
			Detalle:       "Este es un comprobante de prueba que se usó para probar",
		}, {
			FechaContable: 20200109,
			Comp:          "Factura A",
			NComp:         "0002-00000516",
			Debe:          51645381,
			Saldo:         516512,
			Detalle:       "Este es un comprobante de prueba que se usó para probar",
		}, {
			FechaContable: 20200109,
			Comp:          "Factura A",
			NComp:         "0002-00000516",
			Debe:          51645381,
			Saldo:         516512,
			Detalle:       "Este es un comprobante de prueba que se usó para probar",
		},
	}

	{ // Con logo ALTO
		// Logo
		logo, err := os.Open("./testdata/logo_square.png")
		f.LogoFormato = "png"
		assert.Nil(t, err)
		f.Logo, err = io.ReadAll(logo)
		assert.Nil(t, err)
		file, err := os.Create("./output/with_square_logo.pdf")
		assert.Nil(t, err)
		err = f.Output(file)
		assert.Nil(t, err)
	}
	{ // Con logo ANCHO
		logo, err := os.Open("./testdata/logo_horizontal.png")
		assert.Nil(t, err)
		f.LogoFormato = "png"
		f.Logo, err = io.ReadAll(logo)
		assert.Nil(t, err)
		file, err := os.Create("./output/with_horizontal_logo.pdf")
		assert.Nil(t, err)
		err = f.Output(file)
		assert.Nil(t, err)
	}
	{ // Con logo ANCHO
		logo, err := os.Open("testdata/logo_vertical.png")
		assert.Nil(t, err)
		f.LogoFormato = "png"
		f.Logo, err = io.ReadAll(logo)
		assert.Nil(t, err)
		file, err := os.Create("output/with_vertical_logo.pdf")
		assert.Nil(t, err)
		err = f.Output(file)
		assert.Nil(t, err)
	}

	{ // Sin logo
		file, err := os.Create("./output/without_logo.pdf")
		assert.Nil(t, err)
		f.Logo = []byte{}
		err = f.Output(file)
		assert.Nil(t, err)
	}
}
