package pdf

import (
	"bytes"
	"fmt"
	"io"
	"math"

	"bitbucket.org/marcos19/one/pkg/vectorial"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/jung-kurt/gofpdf"
)

var oro = math.Sqrt(2.0) / 2.

const (
	A4Ancho = 210.0
	A4Alto  = 297.0
)

type Resumen struct {
	fpdf *gofpdf.Fpdf
	tr   func(string) string

	FechaDesde fecha.Fecha
	FechaHasta fecha.Fecha
	TipoFecha  string

	// Header
	EmpresaNombre string
	Logo          []byte
	LogoFormato   string

	HeaderRenglonesLeft  []string
	HeaderRenglonesRight []string

	// Persona
	PersonaNombre      string
	PersonaCamposLeft  []string
	PersonaCamposRight []string

	MargenIzq            float64
	MargenDer            float64
	MargenSup            float64
	MargenInf            float64
	MargenEntreSecciones float64

	SaldoInicio dec.D2
	Renglones   []Renglon
	SaldoFinal  dec.D2

	HeaderHeight  float64
	PersonaHeight float64
	FooterHeight  float64

	Bordes  bool
	padding float64
}

type Renglon struct {
	FechaContable fecha.Fecha
	Comp          string
	NComp         string
	Debe          dec.D2
	Haber         dec.D2
	Saldo         dec.D2
	Detalle       string
	FechaVto      fecha.Fecha
}

func (c *Resumen) AnchoImprimible() float64 {
	ancho, _ := c.fpdf.GetPageSize()
	return ancho - c.MargenIzq - c.MargenDer - c.padding*2
}

type FormatoArgs struct {
	MargenIzq            float64
	MargenDer            float64
	MargenSup            float64
	MargenInf            float64
	MargenEntreSecciones float64

	HeaderHeight  float64
	PersonaHeight float64
	ResumenHeight float64
	FooterHeight  float64
}

func DefaultFormatoArgs() (f FormatoArgs) {

	// Seteo margenes
	f.MargenIzq = A4Ancho * oro / 14
	f.MargenDer = A4Ancho * oro / 14
	f.MargenSup = A4Ancho * oro / 14
	f.MargenInf = A4Ancho * oro / 14
	f.MargenEntreSecciones = A4Ancho * oro / 200 //0.6

	// Secciones
	f.HeaderHeight = A4Alto * oro / 8.5
	f.PersonaHeight = 23
	f.FooterHeight = A4Alto * oro / 7

	return
}

// New crea un nuevo comprobante con los valores por defecto.
func New(a FormatoArgs) (f Resumen) {

	// Margenes
	f.MargenIzq = a.MargenIzq
	f.MargenDer = a.MargenDer
	f.MargenSup = a.MargenSup
	f.MargenInf = a.MargenInf
	f.MargenEntreSecciones = a.MargenEntreSecciones

	// Secciones
	f.HeaderHeight = a.HeaderHeight
	f.PersonaHeight = a.PersonaHeight
	f.FooterHeight = a.FooterHeight
	f.Bordes = true
	return f
}

type Outputer interface {
	Output(w io.Writer) error
}

// Output graba en el io.Writer la factura PDF
func (c *Resumen) Output(w io.Writer) (err error) {

	c.fpdf = gofpdf.New("L", "mm", "A4", "")
	c.tr = c.fpdf.UnicodeTranslatorFromDescriptor("")

	p := c.fpdf
	p.SetMargins(c.MargenIzq, c.MargenSup, c.MargenDer)
	p.SetAutoPageBreak(true, c.MargenInf)

	// Fuentes
	fontSize := 8.

	// Encabezado
	// p.SetHeaderFunc(func(c.printHeader)

	p.SetFont("Arial", "", fontSize)

	if c.Bordes {
		c.padding = A4Ancho * oro / 30
		c.MargenIzq -= 3
		c.MargenDer -= 3
		c.MargenInf -= 3
	}

	top := 0.

	p.SetDrawColor(0, 0, 0)
	anchoTabla = c.AnchoImprimible()
	sizeDet = anchoTabla - sizeFecha*2 - sizeComp - sizeNComp - sizeDebe*3

	p.SetHeaderFunc(func() {

		c.fpdf.SetDrawColor(0, 0, 0)
		c.printHeader(top)
		c.printCuadroCliente(top)

		// Recuadro de renglones
		_, h := p.GetPageSize()
		altoBody := h - c.MargenSup - c.HeaderHeight - c.PersonaHeight - c.MargenEntreSecciones*2 - c.MargenInf
		offsetY := top + c.MargenSup + c.HeaderHeight + c.MargenEntreSecciones*4 + c.PersonaHeight
		// bottom := top + c.MargenInf - c.FooterHeight - c.MargenEntreSecciones*3

		// offsetX := c.MargenIzq

		// Recuadro
		if c.Bordes {
			p.ClipRoundedRect(c.MargenIzq, offsetY, c.AnchoImprimible()+c.padding*2, altoBody, 3, true)
			p.ClipEnd()
			p.SetLeftMargin(c.MargenIzq + c.padding)
		}
		p.Ln(12)
		// Cabecera de productos
		p.SetDrawColor(192, 192, 192)
		p.SetFont("Arial", "B", 8)
		fch1 := "Fecha contable"
		fch2 := ""
		if c.TipoFecha == "fecha_financiera" {
			fch1, fch2 = "Vencimiento", "Fecha contable"
		}
		p.CellFormat(sizeFecha, headH, c.tr(fch1), "", 0, "MC", false, 0, "")
		p.CellFormat(sizeComp, headH, c.tr("Comprobante"), "", 0, "MC", false, 0, "")
		p.CellFormat(sizeNComp, headH, c.tr("Numero"), "", 0, "MC", false, 0, "")
		p.CellFormat(sizeDebe, headH, c.tr("Debe"), "", 0, "MR", false, 0, "")
		p.CellFormat(sizeDebe, headH, c.tr("Haber"), "", 0, "MR", false, 0, "")
		p.CellFormat(sizeDebe, headH, c.tr("Saldo"), "", 0, "MR", false, 0, "")
		p.CellFormat(sizeDet, headH, c.tr("Detalle"), "", 0, "MC", false, 0, "")
		p.CellFormat(sizeFecha, headH, c.tr(fch2), "", 1, "MC", false, 0, "")

	})

	p.AddPage()
	c.printBody(top)

	// Corroboro que no haya habido un error
	err = p.Error()
	if err != nil {
		return errors.Wrap(err, "error durante la creación del PDF")
	}

	err = p.Output(w)
	if err != nil {
		return errors.Wrap(err, "exportando el PDF al responseWriter")
	}
	return
}

func (c *Resumen) printHeader(top float64) {
	// NombreEmpresa
	p := c.fpdf
	top += c.MargenSup
	offsetY := top
	// anchoCuadritoLetra := 10.0

	if len(c.Logo) > 0 {
		switch c.LogoFormato {
		case "png", "jpg":
			// Ok
		default:
			offsetY += c.HeaderHeight*(1-oro) + 1
			goto saltearLogo
		}
		buf := bytes.NewReader(c.Logo)
		p.RegisterImageOptionsReader("logo", gofpdf.ImageOptions{ImageType: c.LogoFormato}, buf)
		info := p.GetImageInfo("logo")
		if info == nil {
			return
		}

		div := vectorial.Rect{
			X:      c.MargenIzq + 62,
			Y:      top + 1,
			Width:  40,
			Height: c.HeaderHeight - 2,
		}
		img := vectorial.Rect{
			Width:  info.Width(),
			Height: info.Height(),
		}

		img = vectorial.Fit(img, div)
		img = vectorial.AlignVerticalCenter(img, div)
		img = vectorial.AlignRight(img, div)
		p.Image("logo", img.X, img.Y, img.Width, img.Height, false, "", 0, "")

		offsetY += 7
	} else {
		// Lo pongo a la misma altura que 'Factura A 0001'
		offsetY += c.HeaderHeight*(1-oro) + 1
	}

saltearLogo:

	// Resumen de cuenta
	p.SetXY(c.MargenIzq+c.HeaderHeight/4, c.MargenSup+c.HeaderHeight/2-1)
	p.SetFont("Arial", "B", 16)
	p.CellFormat(c.AnchoImprimible()/3+c.padding, 1, c.tr("Resumen de cuenta"), "", 2, "L", false, 0, "")

	// Nombre empresa
	p.SetLeftMargin(c.MargenIzq + c.AnchoImprimible()/3)
	p.SetY(offsetY)
	p.SetFont("Arial", "B", 10)
	p.CellFormat(c.AnchoImprimible()/3+c.padding, 1, c.tr(c.EmpresaNombre), "", 1, "C", false, 0, "")
	p.Ln(1)
	// p.WriteAligned(
	// 	c.AnchoImprimible()/2, // Width
	// 	0,                     // Line height
	// 	c.tr(c.EmpresaNombre), "C")

	// Renglones de la izquierda
	p.SetFont("Arial", "", 8)
	offsetY += 3
	p.SetY(offsetY)
	for _, v := range c.HeaderRenglonesLeft {
		p.CellFormat(c.AnchoImprimible()/3+c.padding, 3, c.tr(v), "", 1, "C", false, 0, "")
		p.Ln(0)
	}

	// // Fecha
	// p.SetXY(c.MargenIzq+c.AnchoImprimible()/2+c.padding, top+7+5+5)
	// p.SetFont("Arial", "", 9)
	// fchStr := c.tr("Fecha emisión: " + c.Fecha.String())
	// // p.Text(c.MargenIzq+c.AnchoImprimible()/2+15, top+20, fchStr)
	// p.CellFormat(c.AnchoImprimible()/2+c.padding, 3, fchStr, "", 1, "C", false, 0, "")

	// Renglones de la derecha
	p.SetY(top + 10)
	p.SetFont("Arial", "", 8)
	for _, v := range c.HeaderRenglonesRight {
		// p.SetX(c.MargenIzq + c.AnchoImprimible()/2)
		p.SetX(c.AnchoImprimible()/3*2 + c.MargenIzq + c.padding)
		p.CellFormat(c.AnchoImprimible()/3+c.padding, 3, c.tr(v), "", 1, "C", false, 0, "")
		p.Ln(0)
	}

	// Cuadrado del header
	if c.Bordes {
		p.ClipRoundedRect(c.MargenIzq, top, c.AnchoImprimible()+c.padding*2, c.HeaderHeight, 3, true)
		p.ClipEnd()
	}

}

//func reduceAndCenter(origW, origY, maxAlto, maxAncho float64) (x, y, w, h float64) {

//// Limito altura mantengo proporcion original
//alto := 20.0
//maxAlto := 20.0
//maxAncho := 40.0
//ancho := maxAlto * info.Width() / info.Height()

//minX := +c.MargenIzq + 62
////p.Rect(minX, top, maxX-minX, c.HeaderHeight, "D")
//y := top + 2

//// Si es apaisado, el límite me lo da el ancho
//if ancho > maxAncho {
//ancho = maxAncho
//alto = maxAncho / info.Width() * info.Height()
//y += (maxAlto - alto) / 2
//}

//x := minX + (maxAncho - ancho)
//}

// Dibuja el cuadro del cliente a la altura indicada
func (c *Resumen) printCuadroCliente(top float64) {
	p := c.fpdf
	offsetY := top + c.MargenSup + c.HeaderHeight + c.MargenEntreSecciones*2
	offsetX := c.MargenIzq

	// Cuadrado del header
	if c.Bordes {
		p.ClipRoundedRect(offsetX, offsetY, c.AnchoImprimible()+c.padding*2, c.PersonaHeight, 3, true)
		p.ClipEnd()
	}

	p.SetLeftMargin(c.MargenIzq + c.padding)

	// // Cliente
	// offsetY += 2
	// p.SetFont("Arial", "B", 8)
	// p.SetTextColor(128, 128, 128)
	// p.CellFormat(c.AnchoImprimible(), 3, c.tr("Cliente"), "", 2, "L", false, 0, "")
	// p.Ln(1.5)
	// p.SetTextColor(0, 0, 0)

	// Nombre
	offsetY += 5
	p.SetFont("Arial", "B", 10)
	p.SetY(offsetY)
	p.CellFormat(c.AnchoImprimible(), 3, c.tr(c.PersonaNombre), "", 2, "L", false, 0, "")
	p.Ln(1.125)

	p.SetFont("Arial", "", 8)

	for _, v := range c.PersonaCamposLeft {
		p.CellFormat(c.AnchoImprimible()/2, 3, c.tr(v), "", 2, "L", false, 0, "")
	}
	if len(c.PersonaCamposLeft) > 3 {
		c.PersonaHeight += float64(len(c.PersonaCamposLeft)-3) * 3
	}
	// p.SetXY(c.MargenIzq+c.AnchoImprimible()/2+c.padding*2, alturaCampos)
	// for _, v := range c.PersonaCamposRight {
	// 	p.CellFormat(c.AnchoImprimible()/2, 3, c.tr(v), "", 2, "L", false, 0, "")
	// }

}

var ()

var (
	headH      = 10.
	anchoTabla = 0.
	sizeFecha  = 20.
	sizeComp   = 40.
	sizeNComp  = 25.
	sizeDebe   = 22.
	sizeDet    = 0.
)

// Dibuja el cuadro del footer
func (c *Resumen) printBody(top float64) {
	p := c.fpdf

	// Renglones producto
	p.SetLeftMargin(c.MargenIzq + c.padding)
	p.SetFont("Arial", "", 8)
	p.SetFillColor(243, 243, 243)

	renglonH := 4.
	pintar := true

	fchInicio := ""
	if c.FechaDesde.IsZero() {
		fchInicio = "Saldo inicio"
	} else {
		fchInicio = "Saldo al " + (c.FechaDesde).AgregarDias(-1).String()
	}

	// Saldo inicio
	p.CellFormat(anchoTabla-sizeFecha-sizeDet-sizeDebe, renglonH, c.tr(fchInicio), "", 0, "MR", pintar, 0, "")
	p.CellFormat(sizeDebe, renglonH, c.tr(c.SaldoInicio.String()), "", 0, "MR", pintar, 0, "")
	p.CellFormat(sizeDet+sizeFecha, renglonH, "", "", 1, "MR", pintar, 0, "")
	for _, v := range c.Renglones {
		pintar = !pintar
		// Si es un resumen por fecha financiera muestro las dos
		// Si es por fecha contable no muestro la fecha financiera,
		// porque podría tener un comprobante con varios vencimientos.
		fch1 := v.FechaContable
		if c.TipoFecha == "fecha_financiera" {
			fch1 = v.FechaVto
		}
		p.CellFormat(sizeFecha, renglonH, c.tr(fch1.String()), "", 0, "MC", pintar, 0, "")
		p.CellFormat(sizeComp, renglonH, c.tr(v.Comp), "", 0, "MC", pintar, 0, "")
		p.CellFormat(sizeNComp, renglonH, c.tr(v.NComp), "", 0, "MR", pintar, 0, "")
		p.CellFormat(sizeDebe, renglonH, c.tr(v.Debe.String()), "", 0, "MR", pintar, 0, "")
		p.CellFormat(sizeDebe, renglonH, c.tr(v.Haber.String()), "", 0, "MR", pintar, 0, "")
		p.CellFormat(sizeDebe, renglonH, c.tr(v.Saldo.String()), "", 0, "MR", pintar, 0, "")
		p.CellFormat(sizeDet, renglonH, c.tr(v.Detalle), "", 0, "ML", pintar, 0, "")
		fch2 := ""
		if v.FechaVto != 0 {
			fch2 = v.FechaVto.String()
		}
		p.CellFormat(sizeFecha, renglonH, c.tr(fch2), "", 1, "ML", pintar, 0, "")
	}
	pintar = false

	fchFinal := ""
	if c.FechaHasta.IsZero() {
		fchFinal = "Saldo final"
	} else {
		fchFinal = fmt.Sprintf("Saldo al %v:", c.FechaHasta)
	}

	p.SetFont("Arial", "B", 8)
	p.CellFormat(anchoTabla-sizeFecha-sizeDet-sizeDebe, renglonH, c.tr(fchFinal), "", 0, "MR", pintar, 0, "")
	p.CellFormat(sizeDebe, renglonH, c.tr(c.SaldoFinal.String()), "", 0, "MR", pintar, 0, "")
	p.CellFormat(sizeDet, renglonH, "", "", 1, "MR", pintar, 0, "")
}

// // Dibuja  la sección footer
// func (c *Resumen) printFooter(top float64) {
// 	p := c.fpdf
// 	offsetY := top - c.MargenInf - c.FooterHeight
// 	offsetX := c.MargenIzq

// 	// Recuadro
// 	if c.Bordes {
// 		p.ClipRoundedRect(offsetX, offsetY, c.AnchoImprimible()+c.padding*2, c.FooterHeight, 3, true)
// 		p.ClipEnd()
// 	}

// }
