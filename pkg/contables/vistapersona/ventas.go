package vistapersona

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	uuid "github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type VentasReq struct {
	Comitente int
	Empresa   int `json:",string"`
	Persona   uuid.UUID
	Desde     fecha.Fecha
	Hasta     fecha.Fecha
	Cuentas   tipos.GrupoInts
}

type VentasResp struct {
	ID       uuid.UUID
	Nombre   string
	UM       *string
	Cantidad dec.D4
	Monto    dec.D2
}

func ventas(ctx context.Context, conn *pgxpool.Pool, req VentasReq) (out []VentasResp, err error) {
	if req.Comitente == 0 {
		return out, deferror.ComitenteIndefinido()
	}
	if req.Persona == uuid.Nil {
		return out, deferror.Validationf("no se ingresó persona")
	}
	if len(req.Cuentas) == 0 {
		return out, deferror.Validationf("no se ingresó ninguna cuenta contable")
	}
	ww := []string{
		"partidas.comitente = $1",
		"ops.persona = $2",
		fmt.Sprintf("partidas.cuenta IN %v", req.Cuentas.ParaClausulaIn()),
	}
	pp := []interface{}{req.Comitente, req.Persona}

	// Empresa
	if req.Empresa != 0 {
		pp = append(pp, req.Empresa)
		ww = append(ww, fmt.Sprintf("partidas.empresa = $%v", len(pp)))
	}

	// Desde
	if req.Desde != 0 {
		pp = append(pp, req.Desde)
		ww = append(ww, fmt.Sprintf("partidas.fecha_contable >= $%v", len(pp)))
	}

	// Hasta
	if req.Hasta != 0 {
		pp = append(pp, req.Hasta)
		ww = append(ww, fmt.Sprintf("partidas.fecha_contable <= $%v", len(pp)))
	}

	where := strings.Join(ww, " AND ")

	query := fmt.Sprintf(`
		SELECT partidas.producto, productos.nombre, partidas.um, -SUM(cantidad)::INT64 as cantidad, -SUM(monto)::INT64 as total 
	FROM partidas 
	INNER JOIN productos ON productos.id = partidas.producto 
	INNER JOIN ops ON ops.id = partidas.op_id 
	WHERE %v
	GROUP BY partidas.producto, partidas.um, productos.nombre
	ORDER BY sum(monto);
	`, where)

	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying ventas")
	}
	defer rows.Close()
	for rows.Next() {
		v := VentasResp{}
		err = rows.Scan(&v.ID, &v.Nombre, &v.UM, &v.Cantidad, &v.Monto)
		if err != nil {
			return out, errors.Wrap(err, "scanning ventas result")
		}
		out = append(out, v)
	}

	return
}
