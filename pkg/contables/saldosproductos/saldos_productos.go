package saldosproductos

import (
	"context"
	"database/sql"
	"fmt"
	"strings"

	"bitbucket.org/marcos19/one/pkg/cuentasgrupos"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	uuid "github.com/jackc/pgtype/ext/gofrs-uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Handler struct {
	conn                *pgxpool.Pool
	cuentasGrupoHandler *cuentasgrupos.Handler
}

func NewHandler(conn *pgxpool.Pool, cc *cuentasgrupos.Handler) *Handler {
	return &Handler{conn, cc}
}

type SaldoStock struct {
	Productos []ProductoConCantidades
}

type ProductoConCantidades struct {
	SKU      uuid.UUID
	Codigo   string
	Nombre   string
	Cantidad dec.D4
	UM       *string
	Monto    dec.D2
}

type SaldosProductosReq struct {
	Comitente    int
	Empresa      int `json:",string"`
	Fecha        fecha.Fecha
	GrupoCuentas int  `json:",string"`
	Deposito     *int `json:",string"`
}

// SaldosProductos devuelve el listado de productos en stock
func (h *Handler) SaldosProductos(ctx context.Context, req SaldosProductosReq) (out []ProductoConCantidades, err error) {

	out = []ProductoConCantidades{}
	// Valido
	if req.Comitente == 0 {
		return out, deferror.ComitenteIndefinido()
	}
	if req.GrupoCuentas == 0 {
		return out, errors.Errorf("no se definió grupo de cuentas a consultar")
	}

	// Filtros
	where := ""
	ww := []string{
		fmt.Sprintf("partidas.comitente = %v", req.Comitente),
	}

	// Cuentas
	gr, err := h.cuentasGrupoHandler.ReadOne(ctx, cuentasgrupos.ReadOneReq{Comitente: req.Comitente, ID: req.GrupoCuentas})
	if err != nil {
		return out, errors.Wrap(err, "buscando cuentas del grupo")
	}
	ww = append(ww, fmt.Sprintf("cuenta IN %v", gr.Cuentas.ParaClausulaIn()))

	// Fecha
	if req.Fecha != 0 {
		ww = append(ww, fmt.Sprintf("partidas.fecha_contable <= '%v'", req.Fecha.JSONString()))
	}

	// Empresa
	if req.Empresa != 0 {
		ww = append(ww, fmt.Sprintf("partidas.empresa = %v", req.Empresa))
	}

	// Depósito
	if req.Deposito != nil {
		ww = append(ww, fmt.Sprintf("partidas.deposito = %v", *req.Deposito))
	}

	if len(ww) > 0 {
		where = "WHERE "
		where += strings.Join(ww, " AND ")
	}

	// Busco
	query := fmt.Sprintf(`SELECT skus.id, skus.codigo, skus.nombre, partidas.um, 
		SUM(partidas.cantidad)::INT, SUM(partidas.monto)::INT 
			FROM partidas
			INNER JOIN skus ON partidas.sku = skus.id
			%v
			GROUP BY skus.id, skus.codigo, skus.nombre, partidas.um
			ORDER BY skus.nombre`, where)

	rows, err := h.conn.Query(ctx, query)
	if err != nil {
		return out, errors.Wrap(err, "llamando a saldos de productos")
	}
	defer rows.Close()

	for rows.Next() {
		p := ProductoConCantidades{}
		cantidad := sql.NullInt64{}
		monto := sql.NullInt64{}
		err = rows.Scan(&p.SKU, &p.Codigo, &p.Nombre, &p.UM, &cantidad, &monto)

		if cantidad.Valid {
			p.Cantidad = dec.D4(int(cantidad.Int64))
		}
		if monto.Valid {
			p.Monto = dec.D2(int(monto.Int64))
		}
		if err != nil {
			return out, deferror.DB(err, "escaneando producto con cantidades")
		}
		out = append(out, p)
	}

	return
}
