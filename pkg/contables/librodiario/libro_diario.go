package librodiario

import (
	"context"
	"fmt"
	"io"
	"sort"
	"time"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/crosslogic/niler"
	uuid "github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/jung-kurt/gofpdf"
)

type Handler struct {
	conn           *pgxpool.Pool
	empresasGetter empresas.ReaderOne
	ctaGetter      cuentas.ReaderOne
}

func NewHandler(conn *pgxpool.Pool, e empresas.Getter, ctaGetter cuentas.ReaderOne) (*Handler, error) {
	if niler.IsNil(conn) {
		return nil, errors.Errorf("conn era nil")
	}
	if niler.IsNil(e) {
		return nil, errors.Errorf("empresa getter era nil")
	}
	if niler.IsNil(e) {
		return nil, errors.Errorf("cuenta getter era nil")
	}
	return &Handler{conn, e, ctaGetter}, nil
}

type RenglonLibroDiario struct {
	Cta       string
	CtaNombre string
	Debe      dec.D2
	Haber     dec.D2
	Detalle   string
}

type Asiento struct {
	OpID       uuid.UUID
	Fecha      fecha.Fecha
	NombreComp string
	NComp      string
	Detalle    string
	Partidas   []RenglonLibroDiario
	TS         time.Time
}

func (a *Asiento) TotalDebe() (sum dec.D2) {
	for _, v := range a.Partidas {
		sum += v.Debe
	}
	return
}
func (a *Asiento) TotalHaber() (sum dec.D2) {
	for _, v := range a.Partidas {
		sum += v.Haber
	}
	return
}

type LibroDiario struct {
	EmpresaNombre   string
	CierreEjercicio fecha.Fecha
	Asientos        []Asiento
	PrimeraHoja     int
}

// PDF devuelve un archivo PDF del libro diario.
func (l *LibroDiario) PDF(w io.Writer) (err error) {
	p := gofpdf.New("P", "mm", "A4", "")
	tr := p.UnicodeTranslatorFromDescriptor("")

	// Seteo margenes
	margenIzq := 10.
	margenDer := 10.
	margenSup := 21.
	ancho, _ := p.GetPageSize()
	anchoImprimible := ancho - margenIzq - margenDer
	// margenInf := 10.
	p.SetMargins(margenIzq, margenSup, margenDer)

	// Fuentes
	fontSize := 8.

	// Encabezado
	p.SetHeaderFunc(func() {
		// NombreEmpresa
		p.SetFont("Arial", "B", 16)
		p.Text(margenIzq+5, 19, tr(l.EmpresaNombre))

		// "Libro diario"
		p.SetFont("Arial", "B", 13)
		p.Text(margenIzq+anchoImprimible-40, 17, "Libro Diario")

		// Ejercicio con fecha de cierre
		p.Ln(3)
		p.SetFont("Arial", "", 8)
		p.Text(margenIzq+anchoImprimible-40, +21, fmt.Sprint("Fecha de cierre ej: ", l.CierreEjercicio))

		// "Número de hoja"
		p.Ln(3)
		p.SetX(160)
		text := fmt.Sprint(tr("Página: "), p.PageNo()+l.PrimeraHoja)
		p.SetFont("Arial", "", 8)
		p.Text(margenIzq+anchoImprimible-40, +24, text)

		// Cuadrado del header
		p.ClipRoundedRect(margenIzq, margenDer, anchoImprimible, 16, 3, true)
		p.ClipEnd()
	})

	p.AddPage()
	p.SetFont("Arial", "", fontSize)

	// Asientos
	for _, v := range l.Asientos {
		p.Ln(3)
		p.SetFont("Arial", "B", fontSize)
		p.Cell(20, 4, v.Fecha.String())
		tituloAsiento := tr(v.NombreComp) + " " + v.NComp
		if v.Detalle != "" {
			tituloAsiento += " - " + tr(v.Detalle)
			if len(tituloAsiento) > 100 {
				tituloAsiento = tituloAsiento[:100]
			}
		}
		p.Cell(40, 4, tituloAsiento)
		p.SetFont("Arial", "", fontSize)
		p.Ln(6)

		pintar := false
		p.SetFillColor(220, 220, 220)
		altoRenglon := 3.
		for i, w := range v.Partidas {
			pintar = !pintar
			bordeArriba := ""
			if i == 0 {
				bordeArriba = "T"
			}
			bordeAbajo := ""
			if i == len(v.Partidas)-1 {
				bordeAbajo = "B"
			}
			p.CellFormat(5, altoRenglon, "", "L"+bordeArriba+bordeAbajo, 0, "MR", pintar, 0, "")
			p.CellFormat(20, altoRenglon, fmt.Sprint(w.Cta), bordeArriba+bordeAbajo, 0, "MR", pintar, 0, "")

			det := w.CtaNombre
			if w.Detalle != "" {
				det += " (" + w.Detalle + ")"
				if len(det) > 70 {
					det = det[0:70]
				}
			}
			p.CellFormat(anchoImprimible-5-20-25-25, altoRenglon, tr(det), bordeArriba+bordeAbajo, 0, "ML", pintar, 0, "")
			p.CellFormat(25., altoRenglon, w.Debe.String(), "L", 0, "MR", pintar, 0, "")
			p.CellFormat(25., altoRenglon, w.Haber.String(), "", 0, "MR", pintar, 0, "")
			p.Ln(altoRenglon)
		}

		// Total de asiento

		p.CellFormat(anchoImprimible-25-25, altoRenglon, "Total", "", 0, "MR", false, 0, "")
		p.SetFont("Arial", "B", fontSize)
		p.CellFormat(25., altoRenglon, v.TotalDebe().String(), "", 0, "MR", false, 0, "")
		p.CellFormat(25., altoRenglon, v.TotalHaber().String(), "", 0, "MR", false, 0, "")
		p.Ln(5)
		p.SetFont("Arial", "", fontSize)
	}

	err = p.Output(w)
	if err != nil {
		return errors.Wrap(err, "generando PDF de libro diario")
	}

	return
}

type LibroDiarioReq struct {
	Comitente   int
	Empresa     int `json:",string"`
	Desde       fecha.Fecha
	Hasta       fecha.Fecha
	FechaCierre fecha.Fecha
}

func (h *Handler) GenerarLibroDiario(ctx context.Context, req LibroDiarioReq) (libro LibroDiario, err error) {

	if req.Empresa == 0 {
		return libro, deferror.Validation("No se definió la empresa")
	}
	if req.Desde == 0 {
		return libro, deferror.Validation("No se definió la fecha desde")
	}
	if req.Hasta == 0 {
		return libro, deferror.Validation("No se definió la fecha hasta")
	}
	if req.FechaCierre == 0 {
		return libro, deferror.Validation("No se definió la fecha de cierre")
	}
	if req.FechaCierre == 0 {
		return libro, deferror.Validation("no se ingreso ID de comiente")
	}

	personas := map[uuid.UUID]string{}
	{ // Lookup personas
		rows, err := h.conn.Query(ctx, "SELECT id, nombre FROM personas WHERE comitente = $1", req.Comitente)
		if err != nil {
			return libro, errors.Wrap(err, "buscando nombres de personas")
		}
		defer rows.Close()
		for rows.Next() {
			id := uuid.UUID{}
			nombre := ""
			err = rows.Scan(&id, &nombre)
			if err != nil {
				return libro, errors.Wrap(err, "escaneando personas nombres")
			}
			personas[id] = nombre
		}
	}

	headers := map[uuid.UUID]Asiento{}

	{ // Busco los headers
		query := `
		SELECT 
			id, 
			fecha,
			comp_nombre,
			n_comp_str,
			detalle,
			created_at
		FROM ops 
		WHERE 	ops.comitente = $1 AND
				ops.empresa = $2 AND
				ops.fecha >= $3 AND 
				ops.fecha <= $4
		GROUP BY 
			id, 
			fecha_contable, 
			comp_nombre,
			n_comp_str,
			detalle,
			created_at
		`

		rows, err := h.conn.Query(ctx, query, req.Comitente, req.Empresa, req.Desde, req.Hasta)
		if err != nil {
			return libro, errors.Wrap(err, "querying headers")
		}
		defer rows.Close()
		for rows.Next() {
			h := Asiento{}

			err = rows.Scan(&h.OpID, &h.Fecha, &h.NombreComp, &h.NComp, &h.Detalle, &h.TS)
			if err != nil {
				return libro, errors.Wrap(err, "scanning header")
			}
			headers[h.OpID] = h
		}
	}

	{
		query := `
		SELECT
			op_id,
			cuenta,
			persona,
			sum(monto)
		FROM partidas
		WHERE comitente=$1 AND empresa=$2 AND fecha_contable >= $3 AND fecha_contable <= $4
		GROUP BY 
			op_id,
			cuenta,
			persona
		`

		rows, err := h.conn.Query(ctx, query, req.Comitente, req.Empresa, req.Desde, req.Hasta)
		if err != nil {
			return libro, deferror.DB(err, "querying partidas")
		}
		defer rows.Close()
		for rows.Next() {
			r := RenglonLibroDiario{}
			ctaID := 0
			opID := uuid.UUID{}
			monto := int(0)
			persona := new(uuid.UUID)
			err = rows.Scan(&opID, &ctaID, &persona, &monto)
			if err != nil {
				return libro, errors.Wrap(err, "escaneando renglones")
			}
			c, err := h.ctaGetter.ReadOne(ctx, cuentas.ReadOneReq{Comitente: req.Comitente, ID: ctaID})
			if err != nil {
				return libro, errors.Wrapf(err, "buscando cuenta contable %v", ctaID)
			}
			r.Cta = c.Codigo
			r.CtaNombre = c.Nombre
			if persona != nil {
				r.Detalle = personas[*persona]
			}
			if monto > 0 {
				r.Debe = dec.D2(monto)
			} else {
				r.Haber = -dec.D2(monto)
			}

			anterior, ok := headers[opID]
			if !ok {
				return libro, errors.Errorf("no se encontró header id=%v", opID)
			}
			anterior.Partidas = append(anterior.Partidas, r)
			headers[opID] = anterior
		}

	}

	for _, v := range headers {
		debe := dec.D2(0)
		haber := dec.D2(0)
		for _, part := range v.Partidas {
			debe += part.Debe
			haber += part.Haber
		}
		if debe == 0 && haber == 0 {
			continue
		}
		libro.Asientos = append(libro.Asientos, v)
	}

	// Ordeno
	sort.Slice(libro.Asientos, func(i, j int) bool {
		if libro.Asientos[i].Fecha != libro.Asientos[j].Fecha {
			return libro.Asientos[i].Fecha < libro.Asientos[j].Fecha
		}
		return libro.Asientos[i].TS.Before(libro.Asientos[j].TS)
	})

	// Pongo los datos de la empresa
	emp, err := h.empresasGetter.ReadOne(ctx, empresas.ReadOneReq{Comitente: req.Comitente, ID: req.Empresa})
	if err != nil {
		return libro, errors.Wrap(err, "buscando datos de empresa")
	}

	libro.EmpresaNombre = emp.Nombre
	libro.CierreEjercicio = req.FechaCierre
	return
}
