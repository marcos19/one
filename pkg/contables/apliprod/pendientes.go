package apliprod

import (
	"context"
	"fmt"
	"sort"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type PendientesReq struct {
	Comitente int
	Empresa   int `json:",string"`
	Cuentas   tipos.GrupoInts
	Persona   uuid.UUID
	Sucursal  int `json:",string"`
}

type Pendiente struct {
	SKU    uuid.UUID
	Alfa   string
	UM     string
	Envase string
	Nombre string
	Vals   map[string]dec.D4 // Cuenta => Cantidad
}

// Devuelve un listado de los SKU pendientes en las cuentas indicadas.
func Pendientes(ctx context.Context, conn *pgxpool.Pool, req PendientesReq) (out []Pendiente, err error) {

	if req.Comitente == 0 {
		return out, errors.Errorf("no se ingresó ID de comitente")
	}
	if len(req.Cuentas) == 0 {
		return out, errors.Errorf("no se ingresó ninguna cuenta a consultar")
	}

	// Comitente
	qq := []string{"comitente=$1"}
	pp := []interface{}{req.Comitente}

	// Cuentas
	qq = append(qq, fmt.Sprintf("cuenta IN %v", req.Cuentas.ParaClausulaIn()))

	// Empresa
	if req.Empresa != 0 {
		pp = append(pp, req.Empresa)
		qq = append(qq, fmt.Sprintf("empresa=$%v", len(pp)))
	}

	// Persona
	if req.Persona != uuid.Nil {
		pp = append(pp, byArgs(req.Persona))
		qq = append(qq, fmt.Sprintf("persona=$%v", len(pp)))
	}
	// Sucursal
	if req.Sucursal != 0 {
		pp = append(pp, req.Sucursal)
		qq = append(qq, fmt.Sprintf("sucursal=$%v", len(pp)))
	}

	where := strings.Join(qq, " AND ")
	query := fmt.Sprintf(`SELECT sku, cuenta, sum(cantidad) FROM saldos WHERE %v GROUP BY sku, cuenta`, where)

	// Busco
	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying pendientes")
	}
	defer rows.Close()

	skus := map[uuid.UUID]map[int]dec.D4{}

	for rows.Next() {
		sku := uuid.UUID{}
		cuenta := 0
		cantidad := 0
		err = rows.Scan(&sku, &cuenta, &cantidad)
		if err != nil {
			return out, deferror.DB(err, "scanning pendientes")
		}
		prev := skus[sku]
		if prev == nil {
			prev = map[int]dec.D4{}
		}
		prev[cuenta] = dec.D4(float64(cantidad))
		skus[sku] = prev
	}

	ids := []uuid.UUID{}
	for k, v := range skus {
		p := Pendiente{}
		p.SKU = k
		p.Vals = map[string]dec.D4{}
		for cta, cantidad := range v {
			p.Vals[strconv.Itoa(cta)] = cantidad
		}
		out = append(out, p)
		ids = append(ids, k)
	}

	// Traigo datos SKU
	if len(out) > 0 {
		q := fmt.Sprintf(`SELECT id, codigo, nombre, um, envase FROM skus WHERE id IN (%v)`, arrayUUID(ids))
		rows, err := conn.Query(ctx, q)
		if err != nil {
			return out, deferror.DB(err, "querying SKUs")
		}
		defer rows.Close()
		type skudata struct {
			alfa   string
			nombre string
			um     *string
			envase *string
		}
		m := map[uuid.UUID]skudata{}

		for rows.Next() {
			s := skudata{}
			id := uuid.UUID{}
			err = rows.Scan(&id, &s.alfa, &s.nombre, &s.um, &s.envase)
			if err != nil {
				return out, deferror.DB(err, "scanning SKUs")
			}
			m[id] = s
		}

		for i, v := range out {
			found := m[v.SKU]
			out[i].Nombre = found.nombre
			out[i].Alfa = found.alfa
			if found.um != nil {
				out[i].UM = *found.um
			}
			if found.envase != nil {
				out[i].Envase = *found.envase
			}
		}
	}

	sort.Slice(out, func(i, j int) bool {
		return out[i].Nombre < out[j].Nombre
	})
	return
}

type PendientesPorPartidaReq struct {
	Comitente int
	Empresa   int `json:",string"`
	Cuentas   tipos.GrupoInts
	Persona   uuid.UUID
}

type PendientePorPartidaResp struct {
	OpID               uuid.UUID
	Fecha              fecha.Fecha
	CompNombre         string
	NCompStr           string
	Detalle            string
	PartidaAplicada    uuid.UUID
	SKU                uuid.UUID
	Cuenta             int `json:",string"`
	Codigo             string
	UM                 string
	Envase             string
	Nombre             string
	CantidadDisponible dec.D4
	MontoDisponible    dec.D2
	CreatedAt          time.Time
}

// Devuelve un listado de los SKU pendientes en las cuentas indicadas.
// Separa por renglón si corresponde a varias partidas
// Este listado se usa para aplicar en fact.
func PendientesPorPartida(ctx context.Context, conn *pgxpool.Pool, req PendientesPorPartidaReq) (out []PendientePorPartidaResp, err error) {

	if req.Comitente == 0 {
		return out, errors.Errorf("no se ingresó ID de comitente")
	}
	if len(req.Cuentas) == 0 {
		return out, errors.Errorf("no se ingresó ninguna cuenta a consultar")
	}
	if req.Empresa == 0 {
		return out, errors.Errorf("no se ingresó empresa")
	}

	skuIDs := []uuid.UUID{}
	partidasAplicadasID := []uuid.UUID{}

	{ // Busco IDs de partidas disponibles

		// Comitente
		qq := []string{"comitente=$1"}
		pp := []interface{}{req.Comitente}

		// Empresa
		pp = append(pp, req.Empresa)
		qq = append(qq, fmt.Sprintf("empresa=$%v", len(pp)))

		// Cuentas
		qq = append(qq, fmt.Sprintf("cuenta IN %v", req.Cuentas.ParaClausulaIn()))

		// Persona
		if req.Persona != uuid.Nil {
			pp = append(pp, byArgs(req.Persona))
			qq = append(qq, fmt.Sprintf("persona=$%v", len(pp)))
		}

		where := strings.Join(qq, " AND ")
		query := fmt.Sprintf(`
			SELECT partida_aplicada, sku, cuenta, SUM(cantidad)::INT, SUM(monto)::INT
			FROM saldos 
			WHERE %v
			GROUP BY partida_aplicada, sku, cuenta
			HAVING SUM(cantidad) <> 0;
			`, where)

		// Busco
		rows, err := conn.Query(ctx, query, pp...)
		if err != nil {
			return out, deferror.DB(err, "querying pendientes")
		}
		defer rows.Close()

		for rows.Next() {
			v := PendientePorPartidaResp{}
			var cant, mont *int
			err = rows.Scan(&v.PartidaAplicada, &v.SKU, &v.Cuenta, &cant, &mont)
			if err != nil {
				return out, deferror.DB(err, "scanning pendientes")
			}
			if cant != nil {
				v.CantidadDisponible = dec.D4(*cant)
			}
			if mont != nil {
				v.MontoDisponible = dec.D2(*mont)
			}
			out = append(out, v)
			skuIDs = append(skuIDs, v.SKU)
			partidasAplicadasID = append(partidasAplicadasID, v.PartidaAplicada)
		}
	}

	// Traigo datos SKU
	if len(out) > 0 {
		q := fmt.Sprintf(`SELECT id, codigo, nombre, um, envase FROM skus WHERE id IN (%v)`, arrayUUID(skuIDs))
		rows, err := conn.Query(ctx, q)
		if err != nil {
			return out, deferror.DB(err, "querying SKUs")
		}
		defer rows.Close()
		type skudata struct {
			alfa   string
			nombre string
			um     *string
			envase *string
		}
		m := map[uuid.UUID]skudata{}

		for rows.Next() {
			s := skudata{}
			id := uuid.UUID{}
			err = rows.Scan(&id, &s.alfa, &s.nombre, &s.um, &s.envase)
			if err != nil {
				return out, deferror.DB(err, "scanning SKUs")
			}
			m[id] = s
		}

		for i, v := range out {
			found := m[v.SKU]
			out[i].Nombre = found.nombre
			out[i].Codigo = found.alfa
			if found.um != nil {
				out[i].UM = *found.um
			}
			if found.envase != nil {
				out[i].Envase = *found.envase
			}
		}
	}

	// Traigo datos de comprobante
	if len(out) > 0 {
		q := fmt.Sprintf(`
			SELECT ops.id, partidas.id, ops.fecha, ops.comp_nombre, ops.n_comp_str, ops.detalle, partidas.created_at
			FROM partidas
			INNER JOIN ops ON partidas.op_id = ops.id
			WHERE partidas.id IN (%v)`, arrayUUID(partidasAplicadasID))
		rows, err := conn.Query(ctx, q)
		if err != nil {
			return out, deferror.DB(err, "querying op data")
		}
		defer rows.Close()
		type datoOp struct {
			opID       uuid.UUID
			partidaID  uuid.UUID
			fecha      fecha.Fecha
			compNombre string
			nCompStr   string
			detalle    string
			createdAt  time.Time
		}
		m := map[uuid.UUID]datoOp{}

		for rows.Next() {
			s := datoOp{}
			err = rows.Scan(&s.opID, &s.partidaID, &s.fecha, &s.compNombre, &s.nCompStr, &s.detalle, &s.createdAt)
			if err != nil {
				return out, deferror.DB(err, "scanning SKUs")
			}
			m[s.partidaID] = s
		}

		for i, v := range out {
			found := m[v.PartidaAplicada]
			out[i].OpID = found.opID
			out[i].Fecha = found.fecha
			out[i].CompNombre = found.compNombre
			out[i].NCompStr = found.nCompStr
			out[i].Detalle = found.detalle
			out[i].CreatedAt = found.createdAt
		}
	}

	// Ordeno por fecha y comprobante
	sort.Slice(out, func(i, j int) bool {
		if out[i].Fecha < out[j].Fecha {
			return true
		}
		if out[i].Fecha > out[j].Fecha {
			return false
		}
		return out[i].CreatedAt.Before(out[j].CreatedAt)
	})

	if out == nil {
		out = []PendientePorPartidaResp{}
	}
	return
}

type PendientePorSKUReq struct {
	Comitente int
	Empresa   int `json:",string"`
	Cuentas   tipos.GrupoInts
	SKU       uuid.UUID
}

type PendientePorSKUResp struct {
	OpID              uuid.UUID
	PartidasID        tipos.GrupoUUID
	FechaContable     fecha.Fecha
	FechaVto          fecha.Fecha
	CompNombre        string
	NCompStr          string
	CantidadOriginal  dec.D4
	CantidadPendiente dec.D4
	MontoPendiente    dec.D2
	Detalle           *string
	DetallePartida    *string
	CantidadAcumulada dec.D4
}

// Devuelve un listado de los SKU pendientes en las cuentas indicadas.
func PendientesPorSKU(ctx context.Context, conn *pgxpool.Pool, req PendientePorSKUReq) (out []PendientePorSKUResp, err error) {

	if req.Comitente == 0 {
		return out, errors.Errorf("no se ingresó ID de comitente")
	}
	if len(req.Cuentas) == 0 {
		return out, errors.Errorf("no se ingresó ninguna cuenta a consultar")
	}
	if req.SKU == uuid.Nil {
		return out, errors.Errorf("no se ingresó ningún producto para consultar")
	}
	// Comitente
	qq := []string{"comitente=$1", "sku=$2"}
	pp := []interface{}{req.Comitente, byArgs(req.SKU)}

	// Cuentas
	qq = append(qq, fmt.Sprintf("cuenta IN %v", req.Cuentas.ParaClausulaIn()))

	// Empresa
	if req.Empresa != 0 {
		pp = append(pp, req.Empresa)
		qq = append(qq, fmt.Sprintf("empresa=$%v", len(pp)))
	}

	where := strings.Join(qq, " AND ")
	query := fmt.Sprintf(`SELECT partida_aplicada, cantidad, monto FROM saldos WHERE %v`, where)

	// Busco
	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying pendientes")
	}
	defer rows.Close()

	type cant struct {
		cantidad dec.D4
		monto    dec.D2
	}

	saldos := map[uuid.UUID]cant{}
	ids := []string{}

	for rows.Next() {
		partidaAplicada := uuid.UUID{}
		c := cant{}
		err = rows.Scan(&partidaAplicada, &c.cantidad, &c.monto)
		if err != nil {
			return out, deferror.DB(err, "scanning pendientes")
		}
		_, ok := saldos[partidaAplicada]
		if ok {
			// No debería pasar
			return out, errors.Errorf("había más de una partida de saldo para una misma partida")
		}
		saldos[partidaAplicada] = c
		ids = append(ids, fmt.Sprintf("'%v'", partidaAplicada.String()))
	}

	{ // Datos de la operación original
		whereClauses := []string{
			fmt.Sprintf("partidas.comitente = %v", req.Comitente),
			fmt.Sprintf("partidas.id IN (%v)", strings.Join(ids, ",")),
		}
		where := strings.Join(whereClauses, " AND ")

		// Busco datos de las partidas
		query := fmt.Sprintf(`
			SELECT partidas.op_id, partidas.id, partidas.fecha_contable, partidas.fecha_financiera, ops.comp_nombre, ops.n_comp_str, partidas.cantidad, ops.detalle, partidas.detalle
			FROM partidas
			INNER JOIN ops ON ops.id = partidas.op_id
			WHERE %v
			ORDER BY partidas.fecha_contable, partidas.created_at
			;`, where)

		rows, err := conn.Query(ctx, query)
		if err != nil {
			return nil, deferror.DB(err, "realizando query de partidas")
		}
		defer rows.Close()
		qAcum := dec.D4(0)
		for rows.Next() {
			k := PendientePorSKUResp{}
			partidaID := uuid.UUID{}
			err = rows.Scan(
				&k.OpID, &partidaID, &k.FechaContable, &k.FechaVto,
				&k.CompNombre, &k.NCompStr, &k.CantidadOriginal, &k.Detalle, &k.DetallePartida)
			if err != nil {
				return nil, errors.Wrap(err, "escaneando row de partidas")
			}

			// Pongo el ID de partida
			k.PartidasID = append(k.PartidasID, partidaID)

			k.CantidadPendiente = saldos[partidaID].cantidad
			k.MontoPendiente = saldos[partidaID].monto

			qAcum += k.CantidadPendiente
			k.CantidadAcumulada = qAcum
			out = append(out, k)
		}
	}

	return
}
func arrayUUID(ids []uuid.UUID) string {
	idsStr := []string{}
	for _, v := range ids {
		idsStr = append(idsStr, fmt.Sprintf("'%v'", v.String()))
	}
	return strings.Join(idsStr, ", ")
}

func byArgs(id uuid.UUID) []byte {
	if id == uuid.Nil {
		return []byte{}
	}
	return id.Bytes()
}
