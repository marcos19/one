package apliprod

import (
	"context"

	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler es el encargado de recibir los http requests y emitir los responses.
type Handler struct {
	conn *pgxpool.Pool
}

// NewHandler inicializa un cache y devuelve el puntero a este.
func NewHandler(conn *pgxpool.Pool) (h *Handler) {

	h = &Handler{
		conn: conn,
	}
	return
}

func (h *Handler) Pendientes(ctx context.Context, req PendientesReq) (pp []Pendiente, err error) {
	return Pendientes(ctx, h.conn, req)
}

func (h *Handler) PendientesPorSKU(ctx context.Context, req PendientePorSKUReq) (pp []PendientePorSKUResp, err error) {
	return PendientesPorSKU(ctx, h.conn, req)
}

func (h *Handler) PendientesPorPartida(ctx context.Context, req PendientesPorPartidaReq) (pp []PendientePorPartidaResp, err error) {
	return PendientesPorPartida(ctx, h.conn, req)
}
