package mayorproductos

import (
	"context"
	"database/sql"
	"fmt"
	"strings"

	"bitbucket.org/marcos19/one/pkg/cuentasgrupos"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

type Handler struct {
	conn                *pgxpool.Pool
	cuentasGrupoHandler *cuentasgrupos.Handler
}

func NewHandler(conn *pgxpool.Pool, cc *cuentasgrupos.Handler) *Handler {
	return &Handler{conn, cc}
}

type Mayor struct {
	// SKU         uuid.UUID
	// Codigo      string
	// Nombre      string
	// UM          string
	SaldoInicio      dec.D4
	SaldoInicioMonto dec.D2
	Partidas         []Partida
	SaldoFinal       dec.D4
	SaldoFinalMonto  dec.D2
}

type Partida struct {
	ID            uuid.UUID // ID de tabla partidas
	Empresa       int
	Fecha         fecha.Fecha
	Cuenta        int
	CompNombre    string
	NCompStr      string
	Cantidad      dec.D4
	SaldoCantidad dec.D4
	Deposito      *int `json:",omitempty"`
	UM            *string
	Monto         dec.D2
	SaldoMonto    dec.D2
	Detalle       string `json:",omitempty"`
}

type Req struct {
	Comitente    int
	SKU          uuid.UUID
	Empresa      *int `json:",string"`
	Desde        fecha.Fecha
	Hasta        fecha.Fecha
	GrupoCuentas int  `json:",string"`
	Deposito     *int `json:",string"`
}

// SaldosProductos devuelve el listado de productos en stock
func (h *Handler) Mayorizar(ctx context.Context, req Req) (out Mayor, err error) {

	out.Partidas = []Partida{}

	// Valido
	if req.SKU == uuid.Nil {
		return out, errors.Errorf("no se ingresó SKU a consultar")
	}
	if req.Comitente == 0 {
		return out, deferror.ComitenteIndefinido()
	}
	if req.GrupoCuentas == 0 {
		return out, errors.Errorf("no se definió grupo de cuentas a consultar")
	}

	// Filtros
	where := ""
	ww := []string{
		fmt.Sprintf("partidas.comitente = %v", req.Comitente),
		fmt.Sprintf("partidas.sku = '%v'", req.SKU),
	}

	// Cuentas
	gr, err := h.cuentasGrupoHandler.ReadOne(ctx, cuentasgrupos.ReadOneReq{Comitente: req.Comitente, ID: req.GrupoCuentas})
	if err != nil {
		return out, errors.Wrap(err, "buscando cuentas del grupo")
	}
	ww = append(ww, fmt.Sprintf("cuenta IN %v", gr.Cuentas.ParaClausulaIn()))

	// Desde
	if req.Desde != 0 {
		ww = append(ww, fmt.Sprintf("partidas.fecha_contable >= '%v'", req.Desde.JSONString()))
	}

	// Hasta
	if req.Hasta != 0 {
		ww = append(ww, fmt.Sprintf("partidas.fecha_contable <= '%v'", req.Hasta.JSONString()))
	}

	// Empresa
	if req.Empresa != nil {
		ww = append(ww, fmt.Sprintf("partidas.empresa = %v", *req.Empresa))
	}

	// Depósito
	if req.Deposito != nil {
		ww = append(ww, fmt.Sprintf("partidas.deposito = %v", *req.Deposito))
	}

	if len(ww) > 0 {
		where = "WHERE "
		where += strings.Join(ww, " AND ")
	}

	// Busco
	query := fmt.Sprintf(`SELECT 
partidas.id,
partidas.empresa,
partidas.fecha_contable, 
partidas.cuenta,
ops.comp_nombre,
ops.n_comp_str,
partidas.deposito,
partidas.um, 
partidas.cantidad::INT,
partidas.monto::INT
	FROM partidas
	INNER JOIN ops ON partidas.op_id = ops.id
	%v
	ORDER BY partidas.fecha_contable, partidas.created_at`, where)

	rows, err := h.conn.Query(ctx, query)
	if err != nil {
		return out, errors.Wrap(err, "querying mayor productos")
	}
	defer rows.Close()

	for rows.Next() {
		p := Partida{}
		cantidad := sql.NullInt64{}
		monto := sql.NullInt64{}
		err = rows.Scan(&p.ID, &p.Empresa, &p.Fecha, &p.Cuenta, &p.CompNombre, &p.NCompStr,
			&p.Deposito, &p.UM, &p.Cantidad, &p.Monto)

		if cantidad.Valid {
			p.Cantidad = dec.D4(int(cantidad.Int64))
		}
		if monto.Valid {
			p.Monto = dec.D2(int(monto.Int64))
		}
		if err != nil {
			return out, deferror.DB(err, "escaneando producto con cantidades")
		}
		out.Partidas = append(out.Partidas, p)
	}

	acumCantidad := out.SaldoInicio
	acumMonto := out.SaldoInicioMonto
	for i := range out.Partidas {
		acumCantidad += out.Partidas[i].Cantidad
		acumMonto += out.Partidas[i].Monto
		out.Partidas[i].SaldoCantidad += acumCantidad
	}
	out.SaldoFinal = acumCantidad
	out.SaldoFinalMonto = acumMonto

	return

}
