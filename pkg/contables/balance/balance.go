package balance

import (
	"context"
	"fmt"
	"strings"

	centroshandler "bitbucket.org/marcos19/one/pkg/centros/handler"
	"bitbucket.org/marcos19/one/pkg/centrosgrupos"
	cuentashandler "bitbucket.org/marcos19/one/pkg/cuentas/handler"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Handler struct {
	conn           *pgxpool.Pool
	cuentasGetter  *cuentashandler.Handler
	centrosgrupos  *centrosgrupos.Handler
	centros        *centroshandler.Handler
	empresasGetter empresas.Getter
}

type gruposGetter interface {
	CentrosID(ctx context.Context, comitente int, grupos, ids tipos.GrupoInts) (tipos.GrupoInts, error)
}

func NewHandler(
	conn *pgxpool.Pool,
	ctas *cuentashandler.Handler,
	centrosgrupos *centrosgrupos.Handler,
	centros *centroshandler.Handler,
	empresas empresas.Getter,
) *Handler {
	return &Handler{conn, ctas, centrosgrupos, centros, empresas}
}

type BalanceReq struct {
	Comitente           int
	Empresa             int `json:",string"`
	Centros             tipos.GrupoInts
	CentrosGrupos       tipos.GrupoInts
	Desde               fecha.Fecha
	Hasta               fecha.Fecha
	IncluirSaldosInicio bool
	IncluirInflacion    bool
	IncluirApertura     bool
	IncluirCierre       bool
	IncluirRefundicion  bool
}

func (h *Handler) TirarBalance(ctx context.Context, req BalanceReq) (out []Partida, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingreso el número de comitente")
	}

	switch {

	case req.IncluirSaldosInicio:
		// Filtros para tabla saldos
		filtroSaldos := []string{
			"comitente = $1",
		}
		paramsSaldos := []interface{}{
			req.Comitente,
		}

		// Filtro empresas
		if req.Empresa != 0 {
			paramsSaldos = append(paramsSaldos, req.Empresa)
			filtroSaldos = append(filtroSaldos, fmt.Sprintf("empresa = $%v", len(paramsSaldos)))
		}

		// Filtro centros de costo
		if len(req.CentrosGrupos) > 0 || len(req.Centros) > 0 {
			ids, err := h.centrosgrupos.CentrosID(ctx, req.Comitente, req.CentrosGrupos, req.Centros)
			if err != nil {
				return out, err
			}
			if len(ids) > 0 {
				filtroSaldos = append(filtroSaldos, fmt.Sprintf("centro IN %v", ids.ParaClausulaIn()))
			}
		}

		whereClause := "WHERE " + strings.Join(filtroSaldos, " AND ")

		query := fmt.Sprintf(`
		SELECT cuenta, SUM(monto)
		FROM saldos
		%v
		GROUP BY cuenta
		;`, whereClause)

		// Busco en tabla SALDOS
		rows, err := h.conn.Query(ctx, query, paramsSaldos...)
		if err != nil {
			return out, deferror.DB(err, "buscando tabla saldos")
		}
		defer rows.Close()

		// Map para juntar saldos y partidas posteriores
		m := map[int]dec.D2{}

		for rows.Next() {
			cuenta := 0
			monto := 0.
			err = rows.Scan(&cuenta, &monto)
			if err != nil {
				deferror.DB(err, "escaneando row de saldos")
				return out, err
			}
			m[cuenta] = dec.NewD2(monto / 100)
		}

		// Busco movimientos posteriores
		if req.Hasta != 0 {

			paramsSaldos = append(paramsSaldos, req.Hasta)
			filtroSaldos = append(filtroSaldos, fmt.Sprintf("fecha_contable > $%v", len(paramsSaldos)))
			whereClause := "WHERE " + strings.Join(filtroSaldos, " AND ")

			query := fmt.Sprintf(`
			SELECT cuenta, SUM(monto)
			FROM partidas
			%v
			GROUP BY cuenta
			;`, whereClause)

			// log.Debug().Msgf("Query posteriores %v\nParams: %v", query, paramsSaldos)
			rows, err := h.conn.Query(ctx, query, paramsSaldos...)
			if err != nil {
				return out, deferror.DB(err, "buscando movimientos posteriores")
			}
			defer rows.Close()

			for rows.Next() {
				cuenta := 0
				monto := 0.
				err = rows.Scan(&cuenta, &monto)
				if err != nil {
					return out, deferror.DB(err, "escaneando row de movimientos posteriores")

				}
				montoDec := dec.NewD2(monto / 100)
				m[cuenta] -= montoDec
			}
		}
		// Genero un slice
		for k, v := range m {
			p := Partida{
				Cuenta: k,
				Monto:  v,
			}
			out = append(out, p)
		}

	case !req.IncluirSaldosInicio:

		// Filtros para tabla partidas
		ff := []string{
			"comitente = $1",
		}
		params := []interface{}{
			req.Comitente,
		}
		if req.Desde != 0 {
			params = append(params, req.Desde)
			ff = append(ff, fmt.Sprintf("fecha_contable >= $%v", len(params)))
		}
		if req.Hasta != 0 {
			params = append(params, req.Hasta)
			ff = append(ff, fmt.Sprintf("fecha_contable <= $%v", len(params)))
		}

		if req.Empresa != 0 {
			params = append(params, req.Empresa)
			ff = append(ff, fmt.Sprintf("empresa = $%v", len(params)))
		}

		if !req.IncluirInflacion {
			params = append(params, ops.Inflacion)
			ff = append(ff, fmt.Sprintf("(tipo_asiento IS NULL OR tipo_asiento <> $%v)", len(params)))
		}
		if !req.IncluirRefundicion {
			params = append(params, ops.Refundicion)
			ff = append(ff, fmt.Sprintf("(tipo_asiento IS NULL OR tipo_asiento <> $%v)", len(params)))
		}
		if !req.IncluirCierre {
			params = append(params, ops.Cierre)
			ff = append(ff, fmt.Sprintf("(tipo_asiento IS NULL OR tipo_asiento <> $%v)", len(params)))
		}
		if !req.IncluirApertura {
			params = append(params, ops.Apertura)
			ff = append(ff, fmt.Sprintf("(tipo_asiento IS NULL OR tipo_asiento <> $%v)", len(params)))
		}

		// Filtro centros de costo
		if len(req.CentrosGrupos) > 0 || len(req.Centros) > 0 {
			ids, err := h.centrosgrupos.CentrosID(ctx, req.Comitente, req.CentrosGrupos, req.Centros)
			if err != nil {
				return out, err
			}
			if len(ids) > 0 {
				ff = append(ff, fmt.Sprintf("centro IN %v", ids.ParaClausulaIn()))
			}
		}
		whereClause := ""

		if len(ff) > 0 {
			whereClause = "WHERE " + strings.Join(ff, " AND ")
		}

		query := fmt.Sprintf(`
		SELECT cuenta, SUM(monto)
		FROM partidas
		%v
		GROUP BY cuenta
		;`, whereClause)

		rows, err := h.conn.Query(ctx, query, params...)
		if err != nil {
			return out, deferror.DB(err, "buscando partidas")
		}
		defer rows.Close()

		for rows.Next() {
			cuenta := 0
			monto := 0.
			err = rows.Scan(&cuenta, &monto)
			if err != nil {
				return out, deferror.DB(err, "escaneando partidas")
			}
			montoDec := dec.NewD2(monto / 100)
			out = append(out, Partida{Cuenta: cuenta, Monto: montoDec})
		}
	}

	if out == nil {
		out = []Partida{}
	}
	return
}

// BalancePartida representa cada una de las partidas que componen un balance
type Partida struct {
	Cuenta       int `json:",string"`
	CuentaNombre string
	CuentaTipo   string
	PadreID      int `json:",string"`
	Centro       int `json:",string"`
	Monto        dec.D2
	Cantidad     float64

	jerarquia int
	imputable bool
}

// BalanceFiltro tiene todo el input de datos que se le hace a la consulta.
type Filtro struct {
	Empresa    int // Provisorio
	Empresas   []int
	FechaDesde fecha.Fecha
	FechaHasta fecha.Fecha
	Centros    []int

	AbrirPorCentro            bool
	IncluirAsientoCierre      bool
	IncluirAsientoRefundicion bool
	IncluirAsientoApertura    bool
}
