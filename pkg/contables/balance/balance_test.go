package balance

import (
	"os"
	"testing"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/excel"
	"github.com/crosslogic/dec"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/xuri/excelize/v2"
)

func TestHojaDatos(t *testing.T) {

	f := excelize.NewFile()
	req := BalanceReq{
		Empresa:             1,
		Desde:               20230501,
		Hasta:               20240430,
		IncluirSaldosInicio: true,
		IncluirInflacion:    true,
		IncluirApertura:     true,
		IncluirCierre:       true,
		IncluirRefundicion:  true,
	}
	datos := datos{
		empresaNombre: "Empresa de prueba",
	}
	err := balanceHojaDatos(f, req, datos)
	require.Nil(t, err)
	f.DeleteSheet("Sheet1")

	output, err := os.Create("testdata/hoja_datos.xlsx")
	require.Nil(t, err)
	err = f.Write(output)
	require.Nil(t, err)

}

func TestHojaBalance(t *testing.T) {

	f, estilos, err := excel.New()
	require.Nil(t, err)

	pp := []DTO{
		{
			Cuenta: &cuentas.Cuenta{ID: 1, Nombre: "Activo", TipoCuenta: cuentas.TipoCuentaAgrupadora},
			Monto:  1000000,
		},
		{
			Cuenta:    &cuentas.Cuenta{ID: 1001, Nombre: "Efectivo", TipoCuenta: cuentas.TipoCuentaAgrupadora},
			Monto:     300000,
			jerarquia: 1,
		},
		{
			Cuenta:    &cuentas.Cuenta{ID: 1002, Nombre: "Cuenta bancaria", TipoCuenta: cuentas.TipoCuentaPatrimonial},
			Monto:     700000,
			jerarquia: 1,
		},
	}
	err = balanceHojaBalance(f, pp, estilos)
	require.Nil(t, err)
	f.DeleteSheet("Sheet1")

	output, err := os.Create("testdata/hoja_balance.xlsx")
	require.Nil(t, err)
	err = f.Write(output)
	require.Nil(t, err)

}

func TestSumNodes(t *testing.T) {
	values := map[int]dec.D2{}
	values[1001] = 300000
	values[1002] = 700000
	sums := map[int]dec.D2{}

	root := cuentas.Node{
		Cuenta: &cuentas.Cuenta{
			ID:         1,
			Nombre:     "Activo",
			TipoCuenta: cuentas.TipoCuentaAgrupadora,
		},
		Children: []*cuentas.Node{
			{
				Cuenta: &cuentas.Cuenta{
					ID:         1001,
					Nombre:     "Efectivo",
					TipoCuenta: cuentas.TipoCuentaPatrimonial,
				},
			},
			{
				Cuenta: &cuentas.Cuenta{
					ID:         1002,
					Nombre:     "Cuenta bancaria",
					TipoCuenta: cuentas.TipoCuentaPatrimonial,
				},
			},
		},
	}
	sumNodes(&root, values, sums)
	assert.True(t, len(sums) == 3)
	assert.Equal(t, dec.D2(1000000), sums[1])
	assert.Equal(t, dec.D2(300000), sums[1001])
	assert.Equal(t, dec.D2(700000), sums[1002])
}
