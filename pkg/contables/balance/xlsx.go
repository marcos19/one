package balance

import (
	"context"
	"fmt"
	"io"
	"time"

	"bitbucket.org/marcos19/one/pkg/centros"
	"bitbucket.org/marcos19/one/pkg/centrosgrupos"
	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"bitbucket.org/marcos19/one/pkg/excel"
	"bitbucket.org/marcos19/one/pkg/treesum"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/xuri/excelize/v2"
)

// HandleBalanceCSV devuelve el balance desde la tabla de saldos
func (h *Handler) BalanceXLSX(ctx context.Context, w io.Writer, req BalanceReq) (err error) {

	// Traigo cuentas
	cc, err := h.cuentasGetter.ReadMany(ctx, cuentas.ReadManyReq{Comitente: req.Comitente})
	if err != nil {
		return fmt.Errorf("trayendo cuentas: %w", err)
	}

	// Tiro balance
	pp, err := h.TirarBalance(ctx, req)
	if err != nil {
		return fmt.Errorf("tirando balance: %w", err)
	}

	type Cuenta[T comparable] struct {
		c cuentas.Cuenta
	}
	// Creo interfaz para tree
	ctas := make([]treesum.Node[int], len(cc))
	for i := range cc {
		ctas[i] = &cc[i]
	}
	// Values para tree
	values := map[int]dec.D2{}
	for _, v := range pp {
		values[v.Cuenta] = v.Monto
	}

	sortFunc := func(a, b *treesum.SumNode[*cuentas.Cuenta, dec.D2, int]) int {
		//a_ := a.Node.(*cuentas.Cuenta)
		//b_ := b.Node.(*cuentas.Cuenta)

		out := a.Node.Sort(b.Node)
		return out
	}
	// Creo arbol
	tree, err := treesum.MakeTree[*cuentas.Cuenta, dec.D2, int](ctas, values, sortFunc)
	if err != nil {
		return fmt.Errorf("creando árbol: %w", err)
	}

	lines := tree.Flaten()

	datos, err := h.llenarDatos(ctx, req)
	if err != nil {
		return fmt.Errorf("trayendo datos lookup: %w", err)
	}
	file, err := balanceXLSX(req, datos, lines)
	if err != nil {
		return fmt.Errorf("generando Excel: %w", err)
	}

	err = file.Write(w)
	if err != nil {
		return fmt.Errorf("writing: %w", err)
	}

	return
}

// HandleBalanceCSV devuelve el balance desde la tabla de saldos
func balanceXLSX(req BalanceReq, datos datos, pp []*treesum.SumNode[*cuentas.Cuenta, dec.D2, int]) (out *excelize.File, err error) {

	out, estilos, err := excel.New()
	err = balanceHojaDatos(out, req, datos)
	if err != nil {
		return out, fmt.Errorf("en hoja datos: %w:", err)
	}

	err = balanceHojaBalance(out, pp, estilos)
	if err != nil {
		return out, fmt.Errorf("en hoja datos: %w:", err)
	}

	out.DeleteSheet("Sheet1")

	return
}

// Dentro del Excel hay dos hojas, esta es la que tiene el listado de cuentas y saldos.
func balanceHojaBalance(f *excelize.File, pp []*treesum.SumNode[*cuentas.Cuenta, dec.D2, int], estilos excel.Estilos) (err error) {

	s := "Suma y saldos"
	idx, err := f.NewSheet(s)
	if err != nil {
		return fmt.Errorf("creando nueva hoja: %w", err)
	}
	f.SetActiveSheet(idx)

	err = f.SetColWidth(s, "B", "B", 60)
	if err != nil {
		return fmt.Errorf("dando ancho columna: %w", err)
	}
	err = f.SetColWidth(s, "C", "D", 18)
	if err != nil {
		return fmt.Errorf("dando ancho columna: %w", err)
	}
	err = f.SetColStyle(s, "C:D", estilos.Dinero)
	if err != nil {
		return fmt.Errorf("fijando estilo dinero: %w", err)
	}

	// Headers
	// Estilo header
	err = f.SetCellStyle(s, "A1", "D1", estilos.Header)
	if err != nil {
		return fmt.Errorf("pegando formato header: %w", err)
	}
	err = f.SetRowHeight(s, 1, 22)
	if err != nil {
		return fmt.Errorf("dando altura header: %w", err)
	}
	err = f.SetSheetRow(s, "A1", &[]string{"Código", "Cuenta", "Suma", "Saldo"})
	if err != nil {
		return fmt.Errorf("escribiendo headers: %w", err)
	}

	jer, err := newJerStyle(pp, f)
	if err != nil {
		return fmt.Errorf("creando formatos de jeraquías")
	}

	for i, v := range pp {
		row := make([]any, 4)
		row[0] = v.Node.Codigo
		row[1] = v.Node.Nombre
		imputable := v.Node.TipoCuenta != cuentas.TipoCuentaAgrupadora
		if imputable {
			row[3] = v.Ammount.Float()
		} else {
			row[2] = v.Ammount.Float()
		}
		err = f.SetSheetRow(s, fmt.Sprintf("A%v", i+2), &row)
		if err != nil {
			return fmt.Errorf("escribiendo fila numero %v: %w", i+1, err)
		}
		format := jer.getJerarquiaStyle(v.Depth(), imputable)
		f.SetCellStyle(s, fmt.Sprintf("B%v", i+2), fmt.Sprintf("B%v", i+2), format)
	}
	return
}

// Dentro del Excel hay dos hojas, esta es la que tiene los parámetros
// usados para tirar el informe.
func balanceHojaDatos(f *excelize.File, req BalanceReq, datos datos) (err error) {

	hoja := "Datos"
	_, err = f.NewSheet(hoja)
	if err != nil {
		return fmt.Errorf("creando nueva hoja: %w", err)
	}

	sty, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Bold:   true,
			Family: "Calibri",
			Size:   18,
		},
	})
	if err != nil {
		return fmt.Errorf("creando estilo título: %w", err)
	}
	f.SetCellStyle(hoja, "A1", "A1", sty)
	f.SetRowHeight(hoja, 1, 22)
	f.SetColWidth(hoja, "A", "A", 30)
	f.SetColWidth(hoja, "B", "B", 20)

	f.SetCellStr(hoja, "A1", "Balance de suma y saldos")

	boolStr := func(in bool) string {
		if in {
			return "Si"
		}
		return "No"
	}

	values := [][]any{
		{"Informe emitido", time.Now().Format("02/01/2006 15:04:05")},
		{},
		{"Empresa", formatEmpresa(req.Empresa, datos)},
		{"Desde", formatFecha(req.Desde)},
		{"Hasta", formatFecha(req.Hasta)},
		{"Incluye saldos de inicio", boolStr(req.IncluirSaldosInicio)},
	}
	if !req.IncluirSaldosInicio {
		values = append(values,
			[]any{"Incluye ajustes por inflación", boolStr(req.IncluirInflacion)},
			[]any{"Incluye asientos de apertura", boolStr(req.IncluirApertura)},
			[]any{"Incluye asientos de cierre", boolStr(req.IncluirCierre)},
			[]any{"Incluye asientos de refundición", boolStr(req.IncluirRefundicion)},
		)
	}
	if len(datos.centrosgrupos) > 0 {
		values = append(values, []any{"Grupos de centros de costo:"})
		for _, v := range datos.centrosgrupos {
			values = append(values, []any{"", fmt.Sprintf("%v (%v)", v.name, v.id)})
		}
	}
	if len(datos.centros) > 0 {
		values = append(values, []any{"Centros de costo:"})
		for _, v := range datos.centros {
			values = append(values, []any{"", fmt.Sprintf("%v (%v)", v.name, v.id)})
		}
	}

	for i, row := range values {
		err = f.SetSheetRow(hoja, fmt.Sprintf("A%v", i+4), &row)
		if err != nil {
			return fmt.Errorf("en row '%v' - '%v': %w", row[0], row[1], err)
		}
	}

	return
}
func formatEmpresa(e int, d datos) string {
	if e == 0 {
		return "Todas"
	}
	return d.empresaNombre
}
func formatFecha(f fecha.Fecha) string {
	if f == 0 {
		return "Sin filtro"
	}
	return f.String()
}

// Datos adicionales para completar informes
type datos struct {
	empresaNombre string
	centrosgrupos []kv
	centros       []kv
}

type kv struct {
	id   int
	name string
}

func (h *Handler) llenarDatos(ctx context.Context, req BalanceReq) (out datos, err error) {

	// Nombre empresa
	if req.Empresa != 0 {
		e, err := h.empresasGetter.ReadOne(ctx, empresas.ReadOneReq{Comitente: req.Comitente, ID: req.Empresa})
		if err != nil {
			return out, fmt.Errorf("buscando empresa: %w", err)
		}
		out.empresaNombre = e.Nombre
	}

	// Centros grupos
	if len(req.CentrosGrupos) > 0 {
		out.centrosgrupos = make([]kv, len(req.CentrosGrupos))
		for i, v := range req.CentrosGrupos {
			gr, err := h.centrosgrupos.ReadOne(ctx, centrosgrupos.ReadOneReq{Comitente: req.Comitente, ID: v})
			if err != nil {
				return out, fmt.Errorf("buscando grupo '%v': %w", v, err)
			}
			out.centrosgrupos[i] = kv{v, gr.Nombre}
		}
	}

	// Centros
	if len(req.Centros) > 0 {
		out.centros = make([]kv, len(req.Centros))
		for i, v := range req.Centros {
			gr, err := h.centros.ReadOne(ctx, centros.ReadOneReq{Comitente: req.Comitente, ID: v})
			if err != nil {
				return out, fmt.Errorf("buscando grupo '%v': %w", v, err)
			}
			out.centros[i] = kv{v, gr.Nombre}
		}
	}
	return
}

// Devuelve el estilo con indentado y bold
type jerStyle struct {
	imputables  map[int]int
	agrupadoras map[int]int
	file        *excelize.File
}

func newJerStyle(pp []*treesum.SumNode[*cuentas.Cuenta, dec.D2, int], f *excelize.File) (out jerStyle, err error) {

	out.file = f
	out.imputables = map[int]int{}
	out.agrupadoras = map[int]int{}

	for _, v := range pp {
		switch {
		case v.Node.TipoCuenta != cuentas.TipoCuentaAgrupadora:
			_, ok := out.imputables[v.Depth()]
			if ok {
				continue
			}
			err := out.crearJerarquiaStyle(v.Depth(), true)
			if err != nil {
				return out, fmt.Errorf("creando estilo jerarquía imputable: %w", err)
			}

		case v.Node.TipoCuenta == cuentas.TipoCuentaAgrupadora:
			_, ok := out.agrupadoras[v.Depth()]
			if ok {
				continue
			}
			err := out.crearJerarquiaStyle(v.Depth(), false)
			if err != nil {
				return out, fmt.Errorf("creando estilo jerarquía no imputable: %w", err)
			}
		}

	}
	return
}

func (j jerStyle) crearJerarquiaStyle(jerarquia int, imputable bool) error {
	// Nueva jerarquía
	jer := excelize.Style{}
	jer.Alignment = &excelize.Alignment{}
	jer.Alignment.Horizontal = "left"
	jer.Alignment.Indent = jerarquia * 2

	switch {
	case imputable:
		idx, err := j.file.NewStyle(&jer)
		if err != nil {
			return err
		}
		j.imputables[jerarquia] = idx

	case !imputable:
		// Agrego bold
		jer.Font = &excelize.Font{}
		jer.Font.Bold = true
		idx, err := j.file.NewStyle(&jer)
		if err != nil {
			return err
		}
		j.agrupadoras[jerarquia] = idx
	}
	return nil
}

func (j jerStyle) getJerarquiaStyle(jerarquia int, imputable bool) int {
	switch {
	case imputable:
		return j.imputables[jerarquia]

	case !imputable:
		return j.agrupadoras[jerarquia]
	}
	return 0
}
