package balance

import (
	"context"
	"fmt"

	"bitbucket.org/marcos19/one/pkg/cuentas"
	"github.com/cockroachdb/errors"
)

// HandleBalanceCSV devuelve el balance desde la tabla de saldos
func (h *Handler) BalanceCSV(ctx context.Context, req BalanceReq) (out [][]string, err error) {

	out = [][]string{
		{
			"cuenta_id",
			"cuenta_codigo",
			"cuenta_nombre",
			"cuenta_tipo",
			"monto",
		},
	}

	// Tiro balance
	pp, err := h.TirarBalance(ctx, req)
	if err != nil {
		return out, errors.Wrap(err, "tirando balance")
	}

	for _, v := range pp {
		ctaDatos, err := h.cuentasGetter.ReadOne(ctx, cuentas.ReadOneReq{Comitente: req.Comitente, ID: v.Cuenta})
		if err != nil {
			return nil, errors.Wrapf(err, "buscando cuenta %v", v.Cuenta)
		}
		renglon := []string{
			fmt.Sprint(v.Cuenta),
			ctaDatos.Codigo,
			ctaDatos.Nombre,
			fmt.Sprint(ctaDatos.TipoCuenta),
			fmt.Sprintf("%.2f", v.Monto.Float()),
		}
		out = append(out, renglon)
	}

	return
}
