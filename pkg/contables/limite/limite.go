package limite

import (
	"context"
	"fmt"

	"bitbucket.org/marcos19/one/pkg/contables/saldospersonas"
	"bitbucket.org/marcos19/one/pkg/cuentasgrupos"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/personas"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
)

type Req struct {
	Comitente     int
	Persona       uuid.UUID
	GruposCuentas int
	FechaAnalisis fecha.Fecha

	// Para determinar saldo de la operación que se está intentando hacer
	Ops []*ops.Op
}

type Resp struct {
	SaldoContable    dec.D2
	SaldoFinanciero  dec.D2
	LimiteContable   *dec.D2
	LimiteFinanciero *dec.D2
	OK               bool
	Msg              string
}

func ControlarLimite(
	ctx context.Context,
	tx pgx.Tx,
	h personas.ReaderOne,
	grh cuentasgrupos.Getter,
	req Req,
) (out Resp, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.ComitenteIndefinido()
	}
	if req.Persona == uuid.Nil {
		return out, fmt.Errorf("no se ingresó ID de persona")
	}
	if req.GruposCuentas == 0 {
		return out, fmt.Errorf("no se ingresaron grupos de cuentas para determinar saldo")
	}
	if req.FechaAnalisis == 0 {
		return out, fmt.Errorf("no se ingresó fecha análisis")
	}

	// Busco límites
	pers, err := h.ReadOne(ctx, personas.ReadOneReq{Comitente: req.Comitente, ID: req.Persona})
	if err != nil {
		return out, fmt.Errorf("buscando persona: %w", err)
	}

	out.LimiteContable = pers.SaldoContable
	out.LimiteFinanciero = pers.SaldoFinanciero
	if !hayLimites(out) {
		out.OK = true
		return
	}

	// Aplano ID de cuentas
	cc, err := grh.Cuentas(ctx, req.Comitente, []int{req.GruposCuentas})
	if err != nil {
		return out, fmt.Errorf("determinando cuentas de saldo")
	}

	// Sumo esta operación
	cont, fin := req.determinarSaldoOperacion(cc, req.FechaAnalisis)
	out.SaldoContable += cont
	out.SaldoFinanciero += fin

	// Hay límite definido, busco saldos
	saldoReq := saldospersonas.PersonaReq{
		Comitente:     req.Comitente,
		Persona:       req.Persona,
		GruposCuentas: []int{req.GruposCuentas},
	}
	s, err := saldospersonas.PersonaTx(ctx, tx, grh, saldoReq, req.FechaAnalisis)
	if err != nil {
		return out, fmt.Errorf("buscando saldos persona: %w", err)
	}

	out.SaldoContable += s.SaldoContable
	out.SaldoFinanciero += s.SaldoFinanciero

	controlarLimites(&out)

	return
}

func (r *Req) determinarSaldoOperacion(cc tipos.GrupoInts, fch fecha.Fecha) (cont, fin dec.D2) {
	for _, op := range r.Ops {
		for _, part := range op.PartidasContables {
			if !cc.Contains(part.Cuenta) {
				continue
			}
			if part.Persona == nil {
				continue
			}
			if *part.Persona != r.Persona {
				continue
			}
			cont += part.Monto
			if part.FechaFinanciera <= fch {
				fin += part.Monto
			}
		}
	}
	return
}

func controlarLimites(r *Resp) {
	if !hayLimites(*r) {
		r.OK = true
		return
	}

	msg := "El límite de crédito %v establecido es de %v, el saldo luego de esta operación sería %v. No se puede continuar porque supera en %v"

	if r.LimiteContable != nil {
		if r.SaldoContable > *r.LimiteContable {
			r.OK = false
			r.Msg = fmt.Sprintf(msg, "contable", *r.LimiteContable, r.SaldoContable, r.SaldoContable-*r.LimiteContable)
			return
		}
	}
	if r.LimiteFinanciero != nil {
		if r.SaldoFinanciero > *r.LimiteFinanciero {
			r.OK = false
			r.Msg = fmt.Sprintf(msg, "financiero", *r.LimiteFinanciero, r.SaldoFinanciero, r.SaldoFinanciero-*r.LimiteFinanciero)
			return
		}
	}
	r.OK = true
}

func hayLimites(r Resp) bool {
	if r.LimiteContable == nil && r.LimiteFinanciero == nil {
		return false
	}
	return true
}
