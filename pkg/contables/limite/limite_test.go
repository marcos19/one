package limite

import (
	"testing"

	"github.com/crosslogic/dec"
	"github.com/stretchr/testify/assert"
)

func TestLimite(t *testing.T) {

	{ // Sin limite
		r := &Resp{}
		controlarLimites(r)
		assert.True(t, r.OK)
	}

	{ // Límite contable, saldo cero
		l := dec.D2(10)
		r := &Resp{
			LimiteContable: &l,
		}
		controlarLimites(r)
		assert.True(t, r.OK)
	}

	{ // Límite contable, con saldo pero menor
		l := dec.D2(10)
		r := &Resp{
			LimiteContable: &l,
			SaldoContable:  10,
		}
		controlarLimites(r)
		assert.True(t, r.OK)
	}
	{ // Límite contable, con saldo mayor
		l := dec.D2(10)
		r := &Resp{
			LimiteContable: &l,
			SaldoContable:  11,
		}
		controlarLimites(r)
		assert.False(t, r.OK)
	}

	{ // Límite financiero, saldo cero
		l := dec.D2(10)
		r := &Resp{
			LimiteFinanciero: &l,
		}
		controlarLimites(r)
		assert.True(t, r.OK)
	}
	{ // Límite financiero, saldo menor
		l := dec.D2(10)
		r := &Resp{
			LimiteFinanciero: &l,
			SaldoFinanciero:  10,
		}
		controlarLimites(r)
		assert.True(t, r.OK)
	}
	{ // Límite financiero, saldo mayor
		l := dec.D2(10)
		r := &Resp{
			LimiteFinanciero: &l,
			SaldoFinanciero:  11,
		}
		controlarLimites(r)
		assert.False(t, r.OK)
	}

}
