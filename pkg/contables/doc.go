// Package contables tiene las consultas contables clásicas: mayor, saldos y balance.
//
// Tendría más sentido que este paquete esté en /pkg, pero el problema que tengo
// es que la partida de mayor tiene la misma estructura subyacente que partida contable.
// Me ahorro mucho trabajo de esta manera.
package contables
