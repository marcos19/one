package saldospersonas

import (
	"context"
	"fmt"
	"sort"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/contables/saldospersonas/gpj"
	"bitbucket.org/marcos19/one/pkg/cuentasgrupos"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/domicilios"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/cuits"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/crosslogic/niler"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/rs/zerolog/log"
)

type SaldoPersona struct {
	ID              uuid.UUID // ID de la persona
	Nombre          string
	CUIT            cuits.CUIT
	SaldoContable   dec.D2
	SaldoFinanciero dec.D2
	Provincia       string
	Ciudad          string
	Domicilio       string
}

type SaldosPersonasReq struct {
	Comitente           int
	Empresa             int `json:",string"`
	Desde               fecha.Fecha
	Hasta               fecha.Fecha
	Cuentas             tipos.GrupoInts
	IncluirSaldosInicio bool
}

// TirarSaldosPersonas devuelve los saldos financieros y contables de las personas.
func Diferenciales(ctx context.Context, conn *pgxpool.Pool, req SaldosPersonasReq) (out []SaldoPersona, err error) {

	log.Debug().Msg("entrando a Saldos")
	out = []SaldoPersona{}

	// Filtros para tabla saldos
	ff := []string{}
	params := []interface{}{}

	// Comitente
	{
		ff = append(ff, "partidas.comitente = $1")
		params = append(params, req.Comitente)
	}

	// Empresa
	if req.Empresa != 0 {
		ff = append(ff, fmt.Sprintf("partidas.empresa = $%v", len(ff)+1))
		params = append(params, req.Empresa)
	}

	// Desde
	if req.Desde != 0 {
		ff = append(ff, fmt.Sprintf("partidas.fecha_contable >= $%v", len(ff)+1))
		params = append(params, req.Desde)
	}

	// Hasta
	if req.Hasta != 0 {
		ff = append(ff, fmt.Sprintf("partidas.fecha_contable <= $%v", len(ff)+1))
		params = append(params, req.Hasta)
	}

	// Cuentas
	if len(req.Cuentas) > 0 {
		ff = append(ff, fmt.Sprintf("partidas.cuenta IN %v", req.Cuentas.ParaClausulaIn()))
	}

	// Armo filtro
	where := ""
	if len(ff) > 0 {
		where = fmt.Sprintf("WHERE %v", strings.Join(ff, " AND "))
	}

	query := fmt.Sprintf(`
SELECT personas.id, personas.nombre, sum(partidas.monto) 
FROM partidas INNER JOIN personas ON personas.id = partidas.persona 
%v 
GROUP BY personas.id, personas.nombre
HAVING sum(partidas.monto) <> 0;`, where)

	// Busco
	rows, err := conn.Query(ctx, query, params...)
	if err != nil {
		return out, deferror.DB(err, "buscando saldos diferenciales de personas")
	}
	defer rows.Close()

	// Escaneo
	for rows.Next() {
		id := uuid.UUID{}
		nombre := ""
		monto := 0
		err = rows.Scan(&id, &nombre, &monto)
		if err != nil {
			return out, deferror.DB(err, "escaneando row de saldos diferenciales")
		}

		s := SaldoPersona{
			ID:            id,
			Nombre:        nombre,
			SaldoContable: dec.D2(monto),
		}
		out = append(out, s)
	}

	sort.Slice(out, func(i, j int) bool {
		return out[i].SaldoContable < out[j].SaldoContable
	})
	return
}

func ConSaldoInicio(ctx context.Context, conn *pgxpool.Pool, req SaldosPersonasReq) (out []SaldoPersona, err error) {

	out = []SaldoPersona{}

	log.Debug().Msg("entrando a Saldos")
	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó comitente")
	}

	m := map[uuid.UUID]SaldoPersona{}

	where := ""
	whereSaldos := []string{
		fmt.Sprintf("comitente = %v", req.Comitente),
		"persona <> ''",
	}
	wherePartidas := []string{
		fmt.Sprintf("comitente = %v", req.Comitente),
		"persona IS NOT NULL",
	}

	// Filtro cuentas
	if len(req.Cuentas) > 0 {
		whereSaldos = append(whereSaldos, fmt.Sprintf("cuenta IN %v", req.Cuentas.ParaClausulaIn()))
		wherePartidas = append(wherePartidas, fmt.Sprintf("cuenta IN %v", req.Cuentas.ParaClausulaIn()))
	}

	// Filtro empresas
	if req.Empresa != 0 {
		whereSaldos = append(whereSaldos, fmt.Sprintf("empresa = %v", req.Empresa))
		wherePartidas = append(wherePartidas, fmt.Sprintf("empresa = %v", req.Empresa))
	}

	{ // Busco los saldos contables
		whereSaldosStr := "WHERE " + strings.Join(whereSaldos, " AND ")
		query := fmt.Sprintf(`
				SELECT persona, SUM(monto)
				FROM saldos
				%v
				GROUP BY persona
				HAVING SUM(monto) <> 0
				;`, whereSaldosStr)

		log.Debug().Msgf("Saldo contable %v", query)
		rows, err := conn.Query(ctx, query)
		if err != nil {
			return out, deferror.DB(err, "buscando saldos contables")
		}
		defer rows.Close()

		for rows.Next() {
			idBy := []byte{}
			saldo := 0.
			err = rows.Scan(&idBy, &saldo)
			if err != nil {
				return out, deferror.DB(err, "escaneando saldos contables")
			}
			id := uuid.FromBytesOrNil(idBy)
			anterior := m[id]
			anterior.SaldoContable = dec.D2(saldo)
			anterior.SaldoFinanciero = dec.D2(saldo)
			m[id] = anterior
		}
	}

	{ // A los saldos contables le quito los movimientos posteriores
		localWhere := fmt.Sprintf("fecha_contable > '%v'", req.Hasta.JSONString())
		ww2 := append(wherePartidas, localWhere)
		where = "WHERE " + strings.Join(ww2, " AND ")
		query := fmt.Sprintf(`
				SELECT persona, SUM(monto)
				FROM partidas
				%v
				GROUP BY persona
				HAVING SUM(monto) <> 0
				;`, where)

		log.Debug().Msgf("Saldos contables posteriores %v", query)
		rows, err := conn.Query(ctx, query)
		if err != nil {
			return out, deferror.DB(err, "buscando saldos contables posteriores de movimientos contables")
		}
		defer rows.Close()

		for rows.Next() {
			id := uuid.UUID{}
			saldo := 0.
			err = rows.Scan(&id, &saldo)
			if err != nil {
				return out, deferror.DB(err, "escaneando saldos contables posteriores")
			}
			anterior := m[id]
			anterior.SaldoContable -= dec.D2(saldo)
			m[id] = anterior
		}
	}

	{ // A los saldos financieros le quito los movimientos posteriores
		localWhere := fmt.Sprintf("fecha_financiera > '%v'", req.Hasta.JSONString())
		ww2 := append(wherePartidas, localWhere)
		where = "WHERE " + strings.Join(ww2, " AND ")
		query := fmt.Sprintf(`
				SELECT persona, SUM(monto)
				FROM partidas
				%v
				GROUP BY persona
				HAVING SUM(monto) <> 0
				;`, where)

		log.Debug().Msgf("Saldos financieros contables posteriores %v", query)
		rows, err := conn.Query(ctx, query)
		if err != nil {
			return out, deferror.DB(err, "buscando saldos contables posteriores de movimientos financieros")
		}
		defer rows.Close()

		for rows.Next() {
			id := uuid.UUID{}
			saldo := 0.
			err = rows.Scan(&id, &saldo)
			if err != nil {
				return out, deferror.DB(err, "escaneando saldos contables posteriores")
			}
			anterior := m[id]
			anterior.SaldoFinanciero -= dec.D2(saldo)
			m[id] = anterior
		}
	}

	// Busco los movimientos diferidos
	if req.Hasta != 0 {
		wherePartidas = append(wherePartidas,
			fmt.Sprintf("fecha_contable <= '%v' AND fecha_financiera > '%v'",
				req.Hasta.JSONString(), req.Hasta.JSONString(),
			),
		)

		where = "WHERE " + strings.Join(wherePartidas, " AND ")
		query := fmt.Sprintf(`
			SELECT persona, SUM(monto) 
			FROM partidas
			%v
			GROUP BY persona
			HAVING SUM(monto) <> 0
			`, where)

		log.Debug().Msgf("Saldo financiero %v", query)
		rows, err := conn.Query(ctx, query)
		if err != nil {
			return out, deferror.DB(err, "buscando movimientos diferidos")
		}
		defer rows.Close()

		for rows.Next() {
			id := uuid.UUID{}
			saldo := 0.
			err = rows.Scan(&id, &saldo)
			if err != nil {
				return out, deferror.DB(err, "escaneando movimientos diferidos")
			}
			anterior := m[id]
			anterior.SaldoFinanciero = anterior.SaldoContable - dec.D2(saldo)
			m[id] = anterior
		}
	} else {
		// Si no se puso fecha hasta hasta contable y financieron son iguales
		for k, v := range m {
			v.SaldoFinanciero = v.SaldoContable
			m[k] = v
		}
	}

	{ // Busco los otros datos de la persona
		uu := []uuid.UUID{}
		for k := range m {
			uu = append(uu, k)
		}
		query := fmt.Sprintf(`
				SELECT id, nombre, cuit, provincia_nombre, ciudad_nombre, calle, numero, piso, depto FROM personas
				WHERE id IN (%v)	
			`, arrayUUID(uu))

		log.Debug().Msgf("Datos persona %v", query)
		rows, err := conn.Query(ctx, query)
		if err != nil {
			return out, deferror.DB(err, "buscando datos de personas")
		}
		defer rows.Close()
		for rows.Next() {
			id := uuid.UUID{}
			nombre := ""
			cuit := cuits.CUIT(0)
			d := domicilios.Domicilio{}
			err = rows.Scan(&id, &nombre, &cuit, &d.ProvinciaNombre, &d.CiudadNombre, &d.DomCalle, &d.DomNumero, &d.DomPiso, &d.DomDpto)
			if err != nil {
				return out, deferror.DB(err, "escaneando datos de personas")
			}
			anterior := m[id]
			anterior.ID = id
			anterior.Nombre = nombre
			anterior.CUIT = cuit
			anterior.Provincia = d.ProvinciaNombre
			anterior.Ciudad = d.CiudadNombre
			anterior.Domicilio = d.DireccionString()

			m[id] = anterior
		}
	}

	// Creo slice
	for _, v := range m {
		if v.SaldoContable == 0 && v.SaldoFinanciero == 0 {
			continue
		}
		out = append(out, v)
	}
	sort.Slice(out, func(i, j int) bool {
		return out[i].Nombre < out[j].Nombre
	})

	return
}

type SiapReq struct {
	SaldosPersonasReq
	Tipo string // 'clientes', 'proveedores'
}

func conSaldoInicioSIAP(ctx context.Context, conn *pgxpool.Pool, req SiapReq) (out string, err error) {
	if req.Tipo == "" {
		return out, errors.Errorf("no se ingresó tipo listado (clientes o proveedores)")
	}
	ss, err := ConSaldoInicio(ctx, conn, req.SaldosPersonasReq)
	if err != nil {
		return out, errors.Wrap(err, "obteniendo saldos")
	}

	switch req.Tipo {

	case "proveedores":
		return siapProveedores(ss)

	case "clientes":
		return siapClientes(ss)
	}

	return
}

func siapProveedores(ss []SaldoPersona) (out string, err error) {

	rr := []gpj.SaldoProveedor{}
	for _, v := range ss {
		r := gpj.SaldoProveedor{}
		r.CUIT = v.CUIT
		r.Nombre = v.Nombre
		r.Saldo = -v.SaldoContable
		rr = append(rr, r)
	}
	sort.Slice(rr, func(i, j int) bool {
		return rr[i].Saldo > rr[j].Saldo
	})

	out = gpj.ExportarProveedores(rr)

	return
}

func siapClientes(ss []SaldoPersona) (out string, err error) {

	rr := []gpj.SaldoCliente{}
	for _, v := range ss {
		r := gpj.SaldoCliente{}
		r.CUIT = v.CUIT
		r.Nombre = v.Nombre
		r.Saldo = v.SaldoContable
		rr = append(rr, r)
	}
	sort.Slice(rr, func(i, j int) bool {
		return rr[i].Saldo > rr[j].Saldo
	})

	out = gpj.ExportarClientes(rr)

	return
}

type PersonaReq struct {
	Comitente     int
	Persona       uuid.UUID
	GruposCuentas tipos.GrupoInts
}
type PersonaResp struct {
	SaldoContable   dec.D2
	SaldoFinanciero dec.D2
}

// SaldoPersona devuelve los saldos financieros y contables de
// las persona. Se usa al facturar. No tiene en cuenta empresa.
func Persona(ctx context.Context, conn *pgxpool.Pool, gruposH cuentasgrupos.Getter, req PersonaReq, fch fecha.Fecha) (out PersonaResp, err error) {

	tx, err := conn.Begin(ctx)
	if err != nil {
		return out, fmt.Errorf("iniciando transacción: %w", err)
	}
	defer tx.Rollback(ctx)

	return PersonaTx(ctx, tx, gruposH, req, fch)
}

// SaldoPersona devuelve los saldos financieros y contables de
// las persona. Se usa al facturar. No tiene en cuenta empresa.
func PersonaTx(ctx context.Context, tx pgx.Tx, gruposH cuentasgrupos.Getter, req PersonaReq, fch fecha.Fecha) (out PersonaResp, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó comitente")
	}
	if req.Persona == uuid.Nil {
		return out, deferror.Validation("no se ingresó persona")
	}
	if niler.IsNil(gruposH) {
		return out, fmt.Errorf("nil grupos cuentas handler")
	}
	if niler.IsNil(gruposH) {
		return out, fmt.Errorf("nil grupos cuentas handler")
	}

	if fch == 0 {
		fch = fecha.NewFechaFromTime(time.Now())
	}

	where := ""
	whereSaldos := []string{
		fmt.Sprintf("comitente = %v", req.Comitente),
		fmt.Sprintf("persona = %v", by(req.Persona)),
	}
	wherePartidas := []string{
		fmt.Sprintf("comitente = %v", req.Comitente),
		fmt.Sprintf("persona = '%v'", req.Persona.String()),
	}

	// Determino cuentas contables
	cuentas, err := gruposH.Cuentas(ctx, req.Comitente, req.GruposCuentas)
	if err != nil {
		return out, errors.Wrap(err, "determinando cuentas de grupos")
	}
	if len(cuentas) == 0 {
		return out, errors.Errorf("ninguna cuenta")
	}
	whereSaldos = append(whereSaldos, fmt.Sprintf("cuenta IN %v", cuentas.ParaClausulaIn()))
	wherePartidas = append(wherePartidas, fmt.Sprintf("cuenta IN %v", cuentas.ParaClausulaIn()))

	{ // Busco los saldos contables
		whereSaldosStr := "WHERE " + strings.Join(whereSaldos, " AND ")
		query := fmt.Sprintf(`
				SELECT SUM(monto)
				FROM saldos
				%v
				;`, whereSaldosStr)

		log.Debug().Msgf("Saldo contable %v", query)
		rows, err := tx.Query(ctx, query)
		if err != nil {
			return out, deferror.DB(err, "buscando saldos contables")
		}
		defer rows.Close()

		for rows.Next() {
			saldo := new(int)
			err = rows.Scan(&saldo)
			if err != nil {
				return out, deferror.DB(err, "escaneando saldo contable")
			}
			if saldo != nil {
				out.SaldoContable = dec.D2(*saldo)
			}
		}
	}

	{ // Quito los movimientos posteriores
		localWhere := fmt.Sprintf("fecha_financiera > '%v'", fch.JSONString())
		ww2 := append(wherePartidas, localWhere)
		where = "WHERE " + strings.Join(ww2, " AND ")
		query := fmt.Sprintf(`
				SELECT SUM(monto)
				FROM partidas
				%v
				;`, where)

		log.Debug().Msgf("Saldos contables posteriores %v", query)
		rows, err := tx.Query(ctx, query)
		if err != nil {
			return out, deferror.DB(err, "buscando saldos contables posteriores de movimientos contables")
		}
		defer rows.Close()

		out.SaldoFinanciero = out.SaldoContable

		for rows.Next() {
			saldo := new(int)
			err = rows.Scan(&saldo)
			if err != nil {
				return out, deferror.DB(err, "escaneando movimientos posteriores")
			}
			if saldo != nil {
				out.SaldoFinanciero -= dec.D2(*saldo)
			}
		}
	}

	return
}

func arrayUUID(ids []uuid.UUID) string {
	idsStr := []string{}
	for _, v := range ids {
		idsStr = append(idsStr, fmt.Sprintf("'%v'", v.String()))
	}
	return strings.Join(idsStr, ", ")
}

func by(in uuid.UUID) string {
	if in == uuid.Nil {
		return "x''"
	} else {
		return fmt.Sprintf("x'%x'", in)
	}
}
