package gpj

import (
	"strings"

	"github.com/crosslogic/cuits"
	"github.com/crosslogic/dec"
)

type SaldoCliente struct {
	CUIT          cuits.CUIT
	Nombre        string
	Saldo         dec.D2
	Exteriorizado bool
}

func ExportarClientes(ss []SaldoCliente) (out string) {
	rr := []string{}
	sumOtros := dec.D2(0)
	for i, v := range ss {
		if i >= 20 {
			sumOtros += v.Saldo
			continue
		}
		rr = append(rr, v.generarRenglon())
	}
	if sumOtros != 0 {
		rr = append(rr, renglonOtrosClientes(sumOtros, false))
	}
	out = strings.Join(rr, "")
	return
}

// Devuelve el úlitmo renglon que incluye todo el resto
func renglonOtrosClientes(monto dec.D2, exteriorizado bool) string {

	vv := []string{
		"           ",
		llenarTexto("Otros", 61-12+1),
		llenarD2(monto, 76-62+1),
		llenarBool(exteriorizado),
		"\r\n",
	}
	return strings.Join(vv, "")
}

func (o SaldoCliente) generarRenglon() string {

	cuit := o.CUIT.StringSinGuiones()
	if o.CUIT == 0 {
		cuit = "00000000000"
	}
	rr := []string{
		cuit,
		llenarTexto(o.Nombre, 61-12+1),
		llenarD2(o.Saldo, 76-62+1),
		llenarBool(o.Exteriorizado),
		"\r\n",
	}

	return strings.Join(rr, "")
}
