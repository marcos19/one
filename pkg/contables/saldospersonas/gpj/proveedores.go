package gpj

import (
	"strings"

	"github.com/crosslogic/cuits"
	"github.com/crosslogic/dec"
)

type SaldoProveedor struct {
	CUIT   cuits.CUIT
	Nombre string
	Saldo  dec.D2
}

func ExportarProveedores(ss []SaldoProveedor) (out string) {
	rr := []string{}
	sumOtros := dec.D2(0)
	for i, v := range ss {
		if i >= 20 {
			sumOtros += v.Saldo
			continue
		}
		rr = append(rr, v.generarRenglon())
	}
	if sumOtros != 0 {
		rr = append(rr, renglonOtrosProveedores(sumOtros))
	}
	out = strings.Join(rr, "")
	return
}

func (o SaldoProveedor) generarRenglon() string {

	rr := []string{
		o.CUIT.StringSinGuiones(),
		llenarTexto(o.Nombre, 61-12+1),
		llenarD2(o.Saldo, 76-62+1),
		"\r\n",
	}

	return strings.Join(rr, "")
}

// Devuelve el úlitmo renglon que incluye todo el resto
func renglonOtrosProveedores(monto dec.D2) string {

	vv := []string{
		"           ",
		llenarTexto("Otros", 61-12+1),
		llenarD2(monto, 76-62+1),
		"\r\n",
	}
	return strings.Join(vv, "")
}
