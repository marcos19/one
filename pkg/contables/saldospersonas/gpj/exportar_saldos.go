package gpj

import (
	"github.com/crosslogic/dec"
)

func llenarTexto(str string, largo int) string {

	// Si el texto es más largo, lo recorto
	if len(str) > largo {
		return str[:largo]
	}

	// Si el más corto, relleno
	cantidadAgregar := largo - len(str)
	relleno := ""
	for i := 1; i <= cantidadAgregar; i++ {
		relleno += " "
	}
	return str + relleno
}

func llenarD2(v dec.D2, largo int) string {
	return v.ExportarParaCSV(2, "", ",", largo, " ", true)
}
func llenarBool(val bool) string {
	if val {
		return "1"
	}
	return "0"
}
