package saldospersonas

import (
	"fmt"

	"bitbucket.org/marcos19/one/pkg/excel"
	"github.com/cockroachdb/errors"
	"github.com/xuri/excelize/v2"
)

// Exporta un Excel con el listado de saldos de personas
func exportarExcel(ss []SaldoPersona) (out *excelize.File, err error) {

	s := "Saldos personas"

	out, estilos, err := excel.New()
	if err != nil {
		return out, errors.Wrap(err, "creando Excel")
	}
	out.NewSheet(s)

	cabeceras := []string{
		"CUIT",
		"Nombre",
		"Saldo contable",
		"Saldo financiero",
		"Provincia",
		"Ciudad",
		"Domicilio",
	}
	out.SetSheetRow(s, "a1", &cabeceras)

	// Estilo fecha
	err = out.SetColStyle(s, "B", estilos.Fecha)
	if err != nil {
		return out, errors.Wrap(err, "formateando fecha")
	}

	// Estilo pesos
	err = out.SetColStyle(s, "C:D", estilos.Dinero)
	if err != nil {
		return out, errors.Wrap(err, "formateando dinero")
	}

	// Estilo header
	err = out.SetCellStyle(s, "A1", "G1", estilos.Header)
	if err != nil {
		return out, errors.Wrap(err, "pegando formato header")
	}
	err = out.SetRowHeight(s, 1, 22)
	if err != nil {
		return out, errors.Wrap(err, "dando altura header")
	}
	out.SetColWidth(s, "A", "Z", 15)
	out.SetColWidth(s, "B", "B", 36)
	out.SetColWidth(s, "G", "G", 36)

	empiezaEn := 2
	for i, v := range ss {

		r := []interface{}{
			v.CUIT.String(),
			v.Nombre,
			v.SaldoContable.Float(),
			v.SaldoFinanciero.Float(),
			v.Provincia,
			v.Ciudad,
			v.Domicilio,
		}
		out.SetSheetRow(s, fmt.Sprintf("A%v", empiezaEn+i), &r)
	}

	out.DeleteSheet("Sheet1")
	return
}
