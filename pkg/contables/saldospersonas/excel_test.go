package saldospersonas

import (
	"os"
	"testing"

	"github.com/crosslogic/dec"
	"github.com/stretchr/testify/require"
)

func TestGenerarExcel(t *testing.T) {

	t.Run("vacío", func(t *testing.T) {
		ss := []SaldoPersona{}
		out, err := exportarExcel(ss)
		require.Nil(t, err)

		const filePath = "testdata/saldos_personas_vacio.xlsx"
		f, err := os.Create(filePath)
		require.Nil(t, err)
		err = out.Write(f)
		require.Nil(t, err)
	})

	t.Run("con datos", func(t *testing.T) {
		ss := []SaldoPersona{
			{
				CUIT:            20328896479,
				Nombre:          "LOPEZ JUAN CARLOS",
				SaldoContable:   dec.NewD2(189387.32),
				SaldoFinanciero: dec.NewD2(239387.32),
				Provincia:       "SANTA FE",
				Ciudad:          "ROSARIO",
				Domicilio:       "AVENIDA EVA PERON 3424 PISO 4 DEPTO D",
			},
			{
				CUIT:            20328896479,
				Nombre:          "LOPEZ JUAN CARLOS",
				SaldoContable:   dec.NewD2(189387.32),
				SaldoFinanciero: dec.NewD2(0),
				Provincia:       "SANTA FE",
				Ciudad:          "ROSARIO",
				Domicilio:       "AVENIDA EVA PERON 3424 PISO 4 DEPTO D",
			},
		}
		out, err := exportarExcel(ss)
		require.Nil(t, err)

		const filePath = "testdata/saldos_personas_valores.xlsx"
		f, err := os.Create(filePath)
		require.Nil(t, err)
		err = out.Write(f)
		require.Nil(t, err)
	})
}
