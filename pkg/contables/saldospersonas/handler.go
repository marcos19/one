package saldospersonas

import (
	"context"
	"fmt"
	"time"

	"bitbucket.org/marcos19/one/pkg/cuentasgrupos"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/crosslogic/fecha"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/xuri/excelize/v2"
)

type Handler struct {
	conn          *pgxpool.Pool
	gruposCuentas *cuentasgrupos.Handler
}

func NewHandler(conn *pgxpool.Pool, g *cuentasgrupos.Handler) (out *Handler, err error) {
	if conn == nil {
		return nil, deferror.Validation("no se ingresó conn")
	}
	if g == nil {
		return nil, deferror.Validation("no se ingresó cuentasgrupos")
	}
	return &Handler{conn, g}, nil
}

// TirarSaldosPersonas devuelve los saldos financieros y contables de las personas.
func (h *Handler) SaldosPersonas(ctx context.Context, req SaldosPersonasReq) (out []SaldoPersona, err error) {
	switch {
	case req.IncluirSaldosInicio:
		out, err = h.ConSaldoInicio(ctx, req)
		if err != nil {
			return out, fmt.Errorf("con saldo de inicio: %w", err)
		}

	case !req.IncluirSaldosInicio:
		out, err = h.Diferenciales(ctx, req)
		if err != nil {
			return out, fmt.Errorf("diferenciales: %w", err)
		}
	}

	return
}

// TirarSaldosPersonas devuelve los saldos financieros y contables de las personas.
func (h *Handler) ExportarExcel(ctx context.Context, req SaldosPersonasReq) (out *excelize.File, err error) {
	var ss []SaldoPersona
	switch {
	case req.IncluirSaldosInicio:
		ss, err = h.ConSaldoInicio(ctx, req)
		if err != nil {
			return out, fmt.Errorf("con saldo de inicio: %w", err)
		}

	case !req.IncluirSaldosInicio:
		ss, err = h.Diferenciales(ctx, req)
		if err != nil {
			return out, fmt.Errorf("diferenciales: %w", err)
		}
	}

	out, err = exportarExcel(ss)
	return
}

// TirarSaldosPersonas devuelve los saldos financieros y contables de las personas.
func (h *Handler) Diferenciales(ctx context.Context, req SaldosPersonasReq) (out []SaldoPersona, err error) {
	return Diferenciales(ctx, h.conn, req)
}

func (h *Handler) ConSaldoInicio(ctx context.Context, req SaldosPersonasReq) (out []SaldoPersona, err error) {
	return ConSaldoInicio(ctx, h.conn, req)
}

// Devuelve el archivo plano para importar al SIAP
func (h *Handler) ConSaldoInicioSIAP(ctx context.Context, req SiapReq) (out string, err error) {
	return conSaldoInicioSIAP(ctx, h.conn, req)
}

// SaldoPersona devuelve los saldos financieros y contables de
// las persona. Se usa al facturar. No tiene en cuenta empresa.
func (h *Handler) Persona(ctx context.Context, req PersonaReq) (out PersonaResp, err error) {
	fch := fecha.NewFechaFromTime(time.Now())
	return Persona(ctx, h.conn, h.gruposCuentas, req, fch)
}
