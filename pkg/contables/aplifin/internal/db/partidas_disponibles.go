package db

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/marcos19/one/pkg/contables/aplifin"
	"bitbucket.org/marcos19/one/pkg/cuentasgrupos"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

// PartidasDisponibles busca dentro de los saldos cuales son las partidas
// que están disponibles para ser aplicadas.
func PartidasDisponibles(
	ctx context.Context,
	conn *pgxpool.Pool,
	req aplifin.PartidasDisponiblesReq,
	ctaGrupoGetter cuentasgrupos.ReaderOne,
) (out []aplifin.Partida, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("No se definió comitente")
	}

	// Clauslas where
	ww := []string{}
	{
		w := fmt.Sprintf("comitente = '%v'", req.Comitente)
		ww = append(ww, w)
	}

	if req.Empresa != 0 {
		w := fmt.Sprintf("empresa = '%v'", req.Empresa)
		ww = append(ww, w)
	}

	// Personas
	if req.Persona != nil {
		w := fmt.Sprintf("persona = x'%x'", *req.Persona)
		ww = append(ww, w)

		if req.Sucursal != nil {
			w := fmt.Sprintf("sucursal = %v", *req.Sucursal)
			ww = append(ww, w)
		}
	}

	// Cuentas
	if req.Cuentas != nil {
		if len(*req.Cuentas) > 0 {

			ctas := []string{}
			for _, v := range *req.Cuentas {
				ctas = append(ctas, fmt.Sprintf("'%v'", v))
			}
			strings.Join(ctas, ", ")
			w := fmt.Sprintf("cuenta IN ('%v')", ctas)
			ww = append(ww, w)
		}
	}

	// Grupo cuentas
	if req.GrupoCuentas != 0 {
		gr, err := ctaGrupoGetter.ReadOne(ctx, cuentasgrupos.ReadOneReq{Comitente: req.Comitente, ID: req.GrupoCuentas})
		if err != nil {
			return out, errors.Wrap(err, "buscando grupo de cuentas")
		}
		w := fmt.Sprintf("cuenta IN %v", gr.Cuentas.ParaClausulaIn())
		ww = append(ww, w)

	}

	// Cajas
	if req.Cajas != nil {
		cajas := []string{}
		for _, v := range *req.Cajas {
			cajas = append(cajas, fmt.Sprintf("'%v'", v))
		}
		strings.Join(cajas, ", ")
		w := fmt.Sprintf("caja IN (%v)", *req.Cajas)
		ww = append(ww, w)
	}

	whereClause := strings.Join(ww, " AND ")
	// 	query := fmt.Sprintf(`
	// SELECT
	// partidas.fecha_contable, partidas.fecha_financiera, ops.comp_nombre,
	// ops.n_comp_str, partidas.monto, partidas.detalle, saldos.cuenta, saldos.centro,
	// saldos.partida_aplicada, saldos.monto
	// FROM saldos
	// INNER JOIN partidas ON partidas.id = saldos.partida_aplicada
	// INNER JOIN ops ON partidas.op_id = ops.id
	// WHERE %v
	// ORDER BY partidas.fecha_financiera
	// ;`, whereClause)

	query := fmt.Sprintf(`
	SELECT partida_aplicada, monto
	FROM saldos
	WHERE %v
	;`, whereClause)

	rows, err := conn.Query(ctx, query)
	if err != nil {
		return out, deferror.DB(err, "realizando query de saldos pendientes")
	}
	defer rows.Close()

	// Guardo los saldos
	saldos := map[uuid.UUID]dec.D2{}
	ids := []uuid.UUID{}
	for rows.Next() {
		p := []byte{}
		monto := dec.D2(0)
		err = rows.Scan(&p, &monto)
		if err != nil {
			return nil, errors.Wrap(err, "escaneando en tabla saldos")
		}
		id := uuid.FromBytesOrNil(p)
		saldos[id] = monto
		ids = append(ids, id)
	}
	idsStr := arrayUUID(ids)

	{ // Busco partidas
		queryParts := fmt.Sprintf(`
	SELECT
	partidas.fecha_contable, partidas.fecha_financiera, ops.comp_nombre, 
	ops.n_comp_str, partidas.monto, partidas.detalle, partidas.cuenta, partidas.centro,
	partidas.id 
	FROM partidas 
	INNER JOIN ops on partidas.op_id = ops.id
	WHERE partidas.id IN (%v) 
	ORDER BY fecha_financiera;
	`, idsStr)
		rows, err := conn.Query(ctx, queryParts)
		if err != nil {
			return nil, errors.Wrap(err, "buscando en tabla partidas")
		}
		defer rows.Close()
		out = []aplifin.Partida{}
		for rows.Next() {
			p := aplifin.Partida{}
			err = rows.Scan(&p.FechaContable, &p.FechaFinanciera, &p.TipoComp,
				&p.NComp, &p.MontoOriginal, &p.Detalle, &p.Cuenta, &p.Centro, &p.ID)
			if err != nil {
				return out, deferror.DB(err, "escanenado partidas de saldos")
			}
			p.MontoPendiente = saldos[*p.ID]
			out = append(out, p)
		}
	}
	return
}

func arrayUUID(ids []uuid.UUID) string {
	idsStr := []string{}
	for _, v := range ids {
		idsStr = append(idsStr, fmt.Sprintf("'%v'", v.String()))
	}
	return strings.Join(idsStr, ", ")
}
