package aplifin

import (
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
)

type PartidasDisponiblesReq struct {
	Empresa      int `json:",string"`
	Persona      *uuid.UUID
	Sucursal     *int
	Cuentas      *[]int
	Cajas        *[]int
	GrupoCuentas int `json:",string"`

	Comitente int
}

type Partida struct {
	// El ID de la partida aplicada
	ID *uuid.UUID `json:",omitempty"`

	// Datos del comprobante al que está aplicando este
	FechaContable   fecha.Fecha `json:",omitempty"`
	FechaFinanciera fecha.Fecha `json:",omitempty"` // la fecha de la partida, el comprobante no tiene fecha financiera.
	TipoComp        string      `json:",omitempty"`
	NComp           string      `json:",omitempty"`
	Detalle         *string     `json:",omitempty"`

	Cuenta int  `json:",string,omitempty"`
	Centro *int `json:",string,omitempty"`

	// El MontoAplicado es el monto que se va a contabilizar en monto
	MontoOriginal  *dec.D2 // Me interesa para las cuentas corrientes nomás
	MontoPendiente dec.D2

	Dias          int    `json:",omitempty"`
	MontoAplicado dec.D2 `json:",omitempty"` // Sin signo, dato entrado desde el frontend
	Aplicacion    dec.D2 `json:",omitempty"` // Con el signo
}
