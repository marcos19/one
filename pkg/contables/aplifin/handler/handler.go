package handler

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/contables/aplifin"
	"bitbucket.org/marcos19/one/pkg/contables/aplifin/internal/db"
	"bitbucket.org/marcos19/one/pkg/cuentasgrupos"
	"github.com/crosslogic/niler"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

type Handler struct {
	conn           *pgxpool.Pool
	ctaGrupoGetter cuentasgrupos.ReaderOne
}

func New(conn *pgxpool.Pool, cta cuentasgrupos.ReaderOne) (*Handler, error) {
	if niler.IsNil(conn) {
		return nil, errors.Errorf("conn no puede ser nil")
	}
	if niler.IsNil(cta) {
		return nil, errors.Errorf("cuentasgrupos no puede ser nil")
	}
	return &Handler{
		conn:           conn,
		ctaGrupoGetter: cta,
	}, nil
}

func (h *Handler) PartidasDisponibles(
	ctx context.Context,
	req aplifin.PartidasDisponiblesReq,
) (out []aplifin.Partida, err error) {

	return db.PartidasDisponibles(ctx, h.conn, req, h.ctaGrupoGetter)
}
