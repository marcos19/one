package aplifin

import "context"

type DisponiblesGetter interface {
	PartidasDisponibles(context.Context, PartidasDisponiblesReq) ([]Partida, error)
}
