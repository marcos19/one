package mayor

import (
	"fmt"
	"io"

	"github.com/xuri/excelize/v2"
)

// Excel crea un archivo Excel con el mayor y lo graba en el io.Writer
func (m *Mayor) CSV() (out [][]string) {
	out = [][]string{
		{
			"fecha_contable",
			"comp_nombre",
			"n_comp",
			"monto",
			"cantidad",
			"detalle",
			"id",
		},
	}

	for _, v := range m.Partidas {
		det := ""
		if v.Detalle != nil {
			det = *v.Detalle
		}
		renglon := []string{
			v.FechaContable.JSONString(),
			fmt.Sprint(v.CompNombre),
			fmt.Sprint(v.NCompStr),
			fmt.Sprintf("%.2f", v.Monto.Float()),
			fmt.Sprintf("%.4f", v.Cantidad.Float()),
			fmt.Sprint(det),
			// nullAMissing(v.Centro),
			fmt.Sprint(v.ID),
		}
		out = append(out, renglon)
	}
	return
}

// Excel crea un archivo Excel con el mayor y lo graba en el io.Writer
func (m *Mayor) Excel(w io.Writer) (err error) {

	f := excelize.NewFile()
	hoja := "Mayor"
	idx, err := f.NewSheet(hoja)
	if err != nil {
		return fmt.Errorf("creando nueva hoja: %w", err)
	}
	f.DeleteSheet("Sheet1")

	// Formato fecha
	fechaString := "dd/mm/yyyy"
	fechaStyle, err := f.NewStyle(&excelize.Style{CustomNumFmt: &fechaString})
	if err != nil {
		return fmt.Errorf("creando estilo de fecha: %w", err)
	}
	err = f.SetColStyle(hoja, "B:D", fechaStyle)
	if err != nil {
		return fmt.Errorf("fijando formato fecha: %w", err)
	}

	// Formato dinero
	dineroString := `_(* #,##0.00_);[Red]_(* -#,##0.00;_(* "-"??_);_(@_)`
	dineroStyle, err := f.NewStyle(&excelize.Style{CustomNumFmt: &dineroString})
	if err != nil {
		return fmt.Errorf("fijando estilo dinero: %w", err)
	}
	err = f.SetColStyle(hoja, "I:K", dineroStyle)
	if err != nil {
		return fmt.Errorf("setting column style: %w", err)
	}
	err = f.SetColStyle(hoja, "T:T", dineroStyle)
	if err != nil {
		return fmt.Errorf("setting column style: %w", err)
	}

	// Formato column header
	colHeaderStyle, err := f.NewStyle(&excelize.Style{
		Font: &excelize.Font{
			Bold: true,
		},
		Alignment: &excelize.Alignment{
			Horizontal: "center",
			WrapText:   true,
		},
	})
	if err != nil {
		return fmt.Errorf("creando formato header: %w", err)
	}

	err = f.SetCellStyle(hoja, "A3", "V3", colHeaderStyle)
	if err != nil {
		return fmt.Errorf("pegando formato header: %w", err)
	}

	debe := 0.
	haber := 0.
	if m.SaldoInicio > 0 {
		debe = m.SaldoInicio.Float()
	} else {
		haber = -m.SaldoInicio.Float()
	}
	saldo := debe - haber

	fch := ""
	if len(m.Partidas) > 0 {
		fch = fmt.Sprintf("Saldo al %v", m.Partidas[0].FechaContable)
	}
	err = f.SetCellStr(hoja, "A1", fch)
	if err != nil {
		return fmt.Errorf("escribiendo saldo: %w", err)
	}
	f.SetCellFloat(hoja, "C1", m.SaldoInicio.Float(), 2, 64)
	f.SetCellStyle(hoja, "C1", "C1", dineroStyle)

	f.SetRowHeight(hoja, 3, 30)
	f.SetColWidth(hoja, "A", "D", 12)
	f.SetColWidth(hoja, "E", "F", 16)
	f.SetColWidth(hoja, "G", "G", 30)
	f.SetColWidth(hoja, "H", "K", 16)
	f.SetColWidth(hoja, "L", "M", 30)

	// Headers
	colNames := []interface{}{"Empresa", "Fecha contable", "Fecha financiera", "Fecha original", "Comprobante", "Número", "Persona comprobante", "Sucursal comprobante", "Debe", "Haber", "Saldo",
		"Detalle partida", "Detalle comprobante", "Centro", "Moneda", "Tipo cambio", "Moneda original",
		"Producto codigo", "Producto", "SKU", "Unidad medida", "Cantidad", "OpID", "PartidaID"}
	err = f.SetSheetRow(hoja, "A3", &colNames)
	if err != nil {
		return fmt.Errorf("pegando cabecera de tabla: %w", err)
	}

	for i, v := range m.Partidas {
		det := ""
		if v.Detalle != nil {
			det = *v.Detalle
		}
		debe := 0.
		haber := 0.
		if v.Monto > 0 {
			debe = v.Monto.Float()
			saldo += debe
		} else {
			haber = -v.Monto.Float()
			saldo -= haber
		}
		var tc any
		if v.TC == nil {
			tc = ""
		} else {
			tc = (*v.TC).Float()
		}
		var um string
		if v.UM == nil {
			um = ""
		} else {
			um = *v.UM
		}
		var orig any
		if v.MontoMonedaOriginal == nil {
			orig = ""
		} else {
			orig = v.MontoMonedaOriginal.Float()
		}
		var cantidad any
		if v.Cantidad == nil {
			cantidad = ""
		} else {
			cantidad = v.Cantidad.Float()
		}
		var sucursal any
		if v.Sucursal != nil {
			sucursal = *v.Sucursal
		}
		var centro any
		if v.Centro != nil {
			centro = *v.Centro
		}
		var producto any
		if v.Producto != nil {
			producto = *v.Producto
		}
		var skuCod any
		if v.SKUCodigo != nil {
			skuCod = *v.SKUCodigo
		}
		var sku any
		if v.SKU != nil {
			sku = *v.SKU
		}

		renglon := []interface{}{
			v.EmpresaNombre,
			v.FechaContable.Time(),
			v.FechaVto.Time(),
			v.FechaOriginal.Time(),
			v.CompNombre,
			v.NCompStr,
			v.Persona,
			sucursal,
			debe,
			haber,
			saldo,
			det,
			v.DetalleComp,
			centro,
			v.MonedaNombre,
			tc,
			orig,
			producto,
			skuCod,
			sku,
			um,
			cantidad,
			v.OpID.String(),
			v.ID.String(),
		}
		err = f.SetSheetRow("Mayor", fmt.Sprintf("A%v", i+4), &renglon)
		if err != nil {
			return fmt.Errorf("pegando row %v: %w", i, err)
		}
	}

	// Saldo final
	err = f.SetCellFloat(hoja, fmt.Sprintf("H%v", len(m.Partidas)+5), saldo, 2, 64)
	if err != nil {
		return fmt.Errorf("fijando formato: %w", err)
	}

	finalStr := "Saldo final"
	if !m.filtro.FechaHasta.IsZero() {
		finalStr = fmt.Sprintf("Saldo al %v", m.filtro.FechaHasta)
	}

	err = f.SetCellStr(hoja, fmt.Sprintf("G%v", len(m.Partidas)+5), finalStr)
	if err != nil {
		return fmt.Errorf("fijando string: %w", err)
	}

	f.SetActiveSheet(idx)

	err = f.Write(w)
	if err != nil {
		return fmt.Errorf("writing: %w", err)
	}
	return
}
