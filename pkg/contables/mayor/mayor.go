package mayor

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/centrosgrupos"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/monedas"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/crosslogic/niler"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Handler struct {
	conn           *pgxpool.Pool
	monedasHandler *monedas.Handler
	centrosGruposH centrosgrupos.ReaderOne
}

func NewHandler(conn *pgxpool.Pool, mh *monedas.Handler, cg centrosgrupos.ReaderOne) (h *Handler, err error) {
	if niler.IsNil(conn) {
		return h, errors.Errorf("conn no puede ser nil")
	}
	if niler.IsNil(mh) {
		return h, errors.Errorf("monedas.Handler no puede ser nil")
	}
	if niler.IsNil(cg) {
		return h, errors.Errorf("centrosgrupos.ReaderOne no puede ser nil")
	}
	return &Handler{
		conn:           conn,
		monedasHandler: mh,
		centrosGruposH: cg,
	}, nil
}

// Mayor contiene el saldo de inicio, una serie de movimientos
// y el saldo final. En el caso de reordenar las partidas, a cada partida
// se le modificará el saldo acumulado.
type Mayor struct {
	SaldoInicio dec.D2

	Partidas   []Partida
	SaldoFinal dec.D2

	filtro Filtro
}

// MayorPartida es cada una de las partidas q se encuentran dentro de un Mayor.
// Además de la partida contable original, contiene datos del comprobante.
type Partida struct {
	ID   uuid.UUID
	OpID uuid.UUID

	// Datos de Op
	Empresa       int         `json:"emp,string"`
	EmpresaNombre string      `json:"empnom,string"`
	FechaContable fecha.Fecha `json:"fch"`
	FechaVto      fecha.Fecha `json:"vto"`
	FechaOriginal fecha.Fecha `json:"fch_or"`
	CompNombre    string      `json:"cmp"`
	NCompStr      string      `json:"n"`
	DetalleComp   string      `json:"detc,omitempty"`
	Persona       string      `json:"per,omitempty"`
	Sucursal      *string     `json:"suc,omitempty"`

	// Datos de la partida contable
	Cuenta              int    `json:"cta,string"`
	Monto               dec.D2 `json:"mto"`
	moneda              *int
	MonedaNombre        string  `json:"mon,omitempty,string"`
	MontoMonedaOriginal *dec.D4 `json:"orig,omitempty"`
	TC                  *dec.D4 `json:"tc,omitempty"`
	Producto            *string `json:"prod,omitempty"`
	SKU                 *string `json:"sku,omitempty"`
	SKUCodigo           *string `json:"cod,omitempty"`
	Centro              *string `json:"centro,omitempty"`
	UM                  *string `json:"um,omitempty"`
	Cantidad            *dec.D4 `json:"q,omitempty"`
	Detalle             *string `json:"det,omitempty"`

	// Calculado
	SaldoAcum dec.D2 `json:"ac"`

	CreatedAt time.Time
}

// TableName devuelve el nombre de la tabla en la base de datos correspondiente
// a las partidas contables.
func (m *Partida) TableName() string {
	return "partidas"
}

// MayorFiltro define filtros para partidas contables.
type Filtro struct {
	Comitente int

	FechaDesde fecha.Fecha
	FechaHasta fecha.Fecha
	// FechaAnterior           fecha.Fecha
	FechaFinancieraDesde fecha.Fecha
	FechaFinancieraHasta fecha.Fecha
	// FechaFinancieraAnterior fecha.Fecha
	Empresas      tipos.GrupoInts
	Funcion       string
	Cuentas       tipos.GrupoInts
	Cajas         tipos.GrupoInts
	Centros       tipos.GrupoInts
	CentrosGrupos tipos.GrupoInts
	Depositos     tipos.GrupoInts
	Monedas       tipos.GrupoInts
	// Conceptos               []int
	Personas   []uuid.UUID
	Sucursales tipos.GrupoInts
	Productos  []uuid.UUID
	Skus       []uuid.UUID

	IncluirSaldosInicio bool
	IncluirInflacion    bool
	IncluirApertura     bool
	IncluirCierre       bool
	IncluirRefundicion  bool
	// Sólo se devuelven los campos especificados
	// Fields                 []string
	// OrderByFechaFinanciera bool
	// Limit                  int
}

// Mayorizar base al filtro que se le ingrese va a generar un Mayor.
func (h *Handler) Mayorizar(ctx context.Context, f Filtro) (m Mayor, err error) {

	// Valido
	if f.Comitente == 0 {
		return m, deferror.Validation("no se ingresó ID de comitente")
	}
	if len(f.Cuentas) == 0 && len(f.Personas) == 0 {
		return m, deferror.Validation("No se ingresó cuenta contable ni persona")
	}
	if len(f.Centros) > 0 && len(f.CentrosGrupos) > 0 {
		return m, deferror.Validation("Si selecciona grupo de centros no debe seleccionar ningún centro. Si selecciona un centro no debe seleccionar ningún grupo")
	}
	if len(f.CentrosGrupos) > 0 {
		ids := map[int]struct{}{}
		for _, v := range f.CentrosGrupos {
			c, err := h.centrosGruposH.ReadOne(ctx, centrosgrupos.ReadOneReq{
				Comitente: f.Comitente,
				ID:        v,
			})
			if err != nil {
				return m, errors.Wrap(err, "buscando grupo de centros")
			}
			for _, id := range c.Centros {
				ids[id] = struct{}{}
			}
		}
		for k := range ids {
			f.Centros = append(f.Centros, k)
		}
	}

	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return m, errors.Wrap(err, "iniciando transacción mayor")
	}
	defer tx.Rollback(ctx)

	// Busco PARTIDAS
	m.Partidas, err = traerPartidas(ctx, tx, f)
	if err != nil {
		return m, errors.Wrap(err, "buscando partidas de mayor")
	}
	if m.Partidas == nil {
		m.Partidas = []Partida{}
	}

	// Saldo
	if f.IncluirSaldosInicio {

		// Traigo saldo de tabla saldos
		m.SaldoFinal, err = traerSaldoDeCuenta(ctx, tx, f)
		if err != nil {
			return m, errors.Wrap(err, "determinando saldo inicio de mayor")
		}

		// Traigo posteriores
		if f.FechaHasta != 0 {
			saldoPosterior, err := traerSaldoMovimientosPosteriores(ctx, tx, f)
			if err != nil {
				return m, errors.Wrap(err, "determinando saldo inicio de mayor")
			}
			m.SaldoFinal -= saldoPosterior
		}
	}

	// Completo columna saldos
	err = m.completarColumnaSaldo(ctx, h.monedasHandler, f.Comitente, f.IncluirSaldosInicio)
	if err != nil {
		return m, errors.Wrap(err, "completando columna de saldos")
	}

	m.filtro = f
	return
}

// Trae sólo las partidas del mayor solicitado
func traerPartidas(ctx context.Context, tx pgx.Tx, f Filtro) (out []Partida, err error) {

	// Clausula WHERE
	where, params, err := mayorWhereClause(f)
	if err != nil {
		return out, errors.Wrap(err, "armando query de mayor")
	}
	if where == "" {
		return out, errors.Errorf("el filtro no puede estar vacío")
	}

	query := fmt.Sprintf(`
		SELECT 
			partidas.id, ops.id, partidas.empresa, 
			partidas.fecha_contable, partidas.fecha_financiera, ops.fecha_original,
			ops.comp_nombre, ops.n_comp_str, partidas.cuenta, 
			partidas.detalle, partidas.monto, partidas.um, partidas.cantidad,
			ops.persona_nombre, sucursales.nombre, ops.detalle, ops.created_at,
			partidas.moneda, partidas.tc, partidas.monto_moneda_original,
			centros.nombre, productos.nombre, skus.nombre, productos.codigo_alfanumerico,
			empresas.nombre
		FROM partidas 
		INNER JOIN ops ON ops.id = partidas.op_id
		LEFT JOIN centros ON partidas.centro = centros.id
		LEFT JOIN productos ON partidas.producto = productos.id
		LEFT JOIN skus ON partidas.sku = skus.id
		LEFT JOIN empresas ON partidas.empresa = empresas.id
		LEFT JOIN sucursales ON sucursales.id = ops.sucursal
		WHERE %v
		ORDER BY fecha_contable, created_at
		;`, where)

	rows, err := tx.Query(ctx, query, params...)
	if err != nil {
		return out, deferror.DB(err, "buscando partidas")
	}
	defer rows.Close()

	for rows.Next() {
		p := Partida{}
		err = rows.Scan(
			&p.ID, &p.OpID, &p.Empresa,
			&p.FechaContable, &p.FechaVto, &p.FechaOriginal,
			&p.CompNombre, &p.NCompStr, &p.Cuenta,
			&p.Detalle, &p.Monto, &p.UM, &p.Cantidad,
			&p.Persona, &p.Sucursal, &p.DetalleComp, &p.CreatedAt,
			&p.moneda, &p.TC, &p.MontoMonedaOriginal,
			&p.Centro, &p.Producto, &p.SKU, &p.SKUCodigo,
			&p.EmpresaNombre,
		)
		if err != nil {
			return out, errors.Wrap(err, "escanenando partidas de mayor")
		}
		out = append(out, p)
	}

	return
}

// Trae saldo de la tabla saldos
func traerSaldoDeCuenta(ctx context.Context, tx pgx.Tx, f Filtro) (out dec.D2, err error) {

	whereClause, err := saldoWhereClause(f)
	if err != nil {
		return out, errors.Wrap(err, "buscando saldos")
	}
	// Traigo el saldo de la tabla de saldos
	query := fmt.Sprintf(`
		SELECT SUM(monto)::INT
		FROM saldos
		WHERE %v;`, whereClause)

	res := new(dec.D2)
	err = tx.QueryRow(ctx, query).Scan(&res)
	if err != nil {
		return out, deferror.DB(err, "buscando saldo final")
	}
	if res != nil {
		out = *res
	}

	return
}

// Devuelve saldo de movimientos posteriores a fecha hasta
func traerSaldoMovimientosPosteriores(ctx context.Context, tx pgx.Tx, f Filtro) (out dec.D2, err error) {

	whereClause, err := saldoWhereClause(f)
	if err != nil {
		return out, deferror.DB(err, "buscando saldos")
	}
	query := fmt.Sprintf(`
			SELECT SUM(monto)::INT
			FROM partidas
			WHERE %v AND fecha_contable > '%v';
		`, whereClause, f.FechaHasta.JSONString())

	var res *dec.D2
	err = tx.QueryRow(ctx, query).Scan(&res)
	if err != nil {
		return out, deferror.DB(err, "buscando saldo diferencial")
	}
	if res != nil {
		out = *res
	}
	return
}

func mayorWhereClause(filtro Filtro) (where string, pp []interface{}, err error) {
	ff := []string{}

	// Valido
	if filtro.FechaDesde != 0 && filtro.FechaHasta != 0 {
		if filtro.FechaDesde > filtro.FechaHasta {
			return where, pp, deferror.Validation("La fecha HASTA debe ser posterior a la fecha DESDE")
		}
	}
	if filtro.FechaFinancieraDesde != 0 && filtro.FechaFinancieraHasta != 0 {
		if filtro.FechaFinancieraDesde > filtro.FechaFinancieraHasta {
			return where, pp, deferror.Validation("La fecha financiera HASTA debe ser posterior a la fecha financiera DESDE")
		}
	}

	// Comitente
	if filtro.Comitente == 0 {
		return where, pp, deferror.Validation("no se definió el comitente")
	}
	pp = append(pp, filtro.Comitente)
	f := fmt.Sprintf("partidas.comitente = $%v", len(pp))
	ff = append(ff, f)

	// FechaDesde
	if filtro.FechaDesde != 0 {
		pp = append(pp, filtro.FechaDesde)
		f := fmt.Sprintf("partidas.fecha_contable >= $%v", len(pp))
		ff = append(ff, f)
	}

	// FechaHasta
	if filtro.FechaHasta != 0 {
		pp = append(pp, filtro.FechaHasta)
		f := fmt.Sprintf("partidas.fecha_contable <= $%v", len(pp))
		ff = append(ff, f)
	}

	// // Fecha Anterior a (para saldos de inicio)
	// if f.FechaAnterior != 0 {
	// 	enTime := f.FechaAnterior.Time()
	// 	filtro := fmt.Sprintf(
	// 		"fecha_contable < date '%v'",
	// 		enTime.Format("2006-01-02"),
	// 	)
	// 	filtros = append(filtros, filtro)
	// }

	// Fecha financiera desde
	if filtro.FechaFinancieraDesde != 0 {
		pp = append(pp, filtro.FechaFinancieraDesde)
		f := fmt.Sprintf("partidas.fecha_financiera >= $%v", len(pp))
		ff = append(ff, f)
	}

	// Fecha financiera hasta
	if filtro.FechaFinancieraHasta != 0 {
		pp = append(pp, filtro.FechaFinancieraHasta)
		f := fmt.Sprintf("partidas.fecha_financiera <= $%v", len(pp))
		ff = append(ff, f)
	}

	// // Fecha Anterior a (para saldos de inicio)
	// if f.FechaFinancieraAnterior != 0 {
	// 	enTime := f.FechaFinancieraAnterior.Time()
	// 	filtro := fmt.Sprintf("fecha_financiera < date '%v'",
	// 		enTime.Format("2006-01-02"))
	// 	filtros = append(filtros, filtro)
	// }

	// Cuentas
	if len(filtro.Cuentas) != 0 {
		f := fmt.Sprintf("partidas.cuenta IN (%v)", arrayInt(filtro.Cuentas))
		ff = append(ff, f)
	}

	// Cajas
	if len(filtro.Cajas) != 0 {
		f := fmt.Sprintf("partidas.caja IN (%v)", arrayInt(filtro.Cajas))
		ff = append(ff, f)
	}

	// Depositos
	if len(filtro.Depositos) != 0 {
		f := fmt.Sprintf("partidas.deposito IN (%v)", arrayInt(filtro.Depositos))
		ff = append(ff, f)
	}

	// Centros
	if len(filtro.Centros) != 0 {
		f := fmt.Sprintf("partidas.centro IN (%v)", arrayInt(filtro.Centros))
		ff = append(ff, f)
	}

	// Personas
	if len(filtro.Personas) != 0 {
		f := fmt.Sprintf("partidas.persona IN (%v)", arrayUUID(filtro.Personas))
		ff = append(ff, f)
	}

	// Sucursales
	if len(filtro.Sucursales) != 0 {
		f := fmt.Sprintf("partidas.sucursal IN (%v)", arrayInt(filtro.Sucursales))
		ff = append(ff, f)
	}

	// Empresas
	if len(filtro.Empresas) != 0 {
		f := fmt.Sprintf("partidas.empresa IN (%v)", arrayInt(filtro.Empresas))
		ff = append(ff, f)
	}

	// Productos
	if len(filtro.Productos) != 0 {
		f := fmt.Sprintf("partidas.producto IN (%v)", arrayUUID(filtro.Productos))
		ff = append(ff, f)
	}

	// Sku
	if len(filtro.Skus) != 0 {
		f := fmt.Sprintf("partidas.sku IN (%v)", arrayUUID(filtro.Skus))
		ff = append(ff, f)
	}

	// Moneda
	if len(filtro.Monedas) != 0 {
		f := fmt.Sprintf("partidas.moneda IN (%v)", arrayInt(filtro.Monedas))
		ff = append(ff, f)
	}

	// Asientos de ajuste de inflación
	tipos := []int{}
	if filtro.IncluirInflacion {
		tipos = append(tipos, ops.Inflacion)
	}
	if filtro.IncluirRefundicion {
		tipos = append(tipos, ops.Refundicion)
	}
	if filtro.IncluirCierre {
		tipos = append(tipos, ops.Cierre)
	}
	if filtro.IncluirApertura {
		tipos = append(tipos, ops.Apertura)
	}
	if len(tipos) != 0 {
		f := fmt.Sprintf("(partidas.tipo_asiento IS NULL OR partidas.tipo_asiento IN (%v))", arrayInt(tipos))
		ff = append(ff, f)
	} else {
		f := "partidas.tipo_asiento IS NULL"
		ff = append(ff, f)
	}

	// Por las dudas que no haya ningún filtro
	if len(ff) == 0 {
		return "", nil, nil
	}

	// Uno los filtros
	where = strings.Join(ff, " AND ")
	return
}

// Devuelve todo lo que viene después del "WHERE " para calcular el saldo final
func saldoWhereClause(filtro Filtro) (where string, err error) {
	ff := []string{}

	// Comitente
	if filtro.Comitente == 0 {
		return where, deferror.Validation("no se definió el comitente")
	}
	f := fmt.Sprintf("comitente = %v", filtro.Comitente)
	ff = append(ff, f)

	// Cuentas
	if len(filtro.Cuentas) != 0 {
		f := fmt.Sprintf("cuenta IN (%v)", arrayInt(filtro.Cuentas))
		ff = append(ff, f)
	}

	// Centros
	if len(filtro.Centros) != 0 {
		f := fmt.Sprintf("centro IN (%v)", arrayInt(filtro.Centros))
		ff = append(ff, f)
	}

	// Cajas
	if len(filtro.Cajas) != 0 {
		f := fmt.Sprintf("caja IN (%v)", arrayInt(filtro.Cajas))
		ff = append(ff, f)
	}

	// Depositos
	if len(filtro.Depositos) != 0 {
		f := fmt.Sprintf("deposito IN (%v)", arrayInt(filtro.Depositos))
		ff = append(ff, f)
	}

	// Personas
	if len(filtro.Personas) != 0 {
		f := fmt.Sprintf("persona IN (%v)", arrayUUIDSaldos(filtro.Personas))
		ff = append(ff, f)
	}

	// Sucursales
	if len(filtro.Sucursales) != 0 {
		f := fmt.Sprintf("sucursal IN (%v)", arrayInt(filtro.Sucursales))
		ff = append(ff, f)
	}

	// Empresas
	if len(filtro.Empresas) != 0 {
		f := fmt.Sprintf("empresa IN (%v)", arrayInt(filtro.Empresas))
		ff = append(ff, f)
	}

	// Productos
	if len(filtro.Productos) != 0 {
		f := fmt.Sprintf("producto IN (%v)", arrayUUIDSaldos(filtro.Productos))
		ff = append(ff, f)
	}

	// Sku
	if len(filtro.Skus) != 0 {
		f := fmt.Sprintf("sku IN (%v)", arrayUUIDSaldos(filtro.Skus))
		ff = append(ff, f)
	}

	// Por las dudas que no haya ningún filtro
	if len(ff) == 0 {
		return "", nil
	}

	// Uno los filtros
	where = strings.Join(ff, " AND ")
	return
}

// Pega el saldo acumulado en la columna de saldo
func (m *Mayor) completarColumnaSaldo(ctx context.Context, mh *monedas.Handler, comitente int, conSaldosInicio bool) (err error) {
	if conSaldosInicio {
		return m.completarColumnaSaldoConSaldoInicio(ctx, mh, comitente)
	}

	return m.completarColumnaSaldoSinSaldoInicio()
}

func (m *Mayor) completarColumnaSaldoSinSaldoInicio() (err error) {

	acum := dec.D2(0)
	for i := range m.Partidas {
		acum += m.Partidas[i].Monto
		m.Partidas[i].SaldoAcum = acum
	}
	m.SaldoFinal = acum
	return
}

func (m *Mayor) completarColumnaSaldoConSaldoInicio(ctx context.Context, mh *monedas.Handler, comitente int) (err error) {

	if len(m.Partidas) == 0 {
		m.SaldoInicio = m.SaldoFinal
		return
	}

	// Calculo saldos acum partidas
	acum := m.SaldoFinal

	for i := len(m.Partidas) - 1; i >= 0; i-- {
		m.Partidas[i].SaldoAcum = acum
		acum -= m.Partidas[i].Monto

		if m.Partidas[i].moneda != nil {
			found, err := mh.ReadOne(ctx, monedas.ReadOneReq{Comitente: comitente, ID: *m.Partidas[i].moneda})
			if err != nil {
				return errors.Wrapf(err, "buscando moneda %v", m.Partidas[i].moneda)
			}
			m.Partidas[i].MonedaNombre = found.Codigo
		}
	}

	m.SaldoInicio = m.Partidas[0].SaldoAcum - m.Partidas[0].Monto
	return
}

func arrayUUID(ids []uuid.UUID) string {
	idsStr := []string{}
	for _, v := range ids {
		idsStr = append(idsStr, fmt.Sprintf("'%v'", v.String()))
	}
	return strings.Join(idsStr, ", ")
}

func arrayUUIDSaldos(ids []uuid.UUID) string {
	idsStr := []string{}
	for _, v := range ids {
		idsStr = append(idsStr, fmt.Sprintf("x'%x'", v))
	}
	return strings.Join(idsStr, ", ")
}

func arrayInt(ids tipos.GrupoInts) string {
	idsStr := []string{}
	for _, v := range ids {
		idsStr = append(idsStr, fmt.Sprintf("%v", v))
	}
	return strings.Join(idsStr, ", ")
}
