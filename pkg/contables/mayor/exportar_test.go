package mayor

import (
	"os"
	"testing"

	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/require"
)

func TestExcel(t *testing.T) {

	f, err := os.Create("testdata/test.xlsx")
	require.Nil(t, err)

	m := Mayor{
		SaldoInicio: 10000,
		SaldoFinal:  45390454,
		Partidas: []Partida{
			{
				ID:            uuid.FromStringOrNil("611aee6f-77d2-462b-a44c-6a8fe55ffa2f"),
				OpID:          uuid.FromStringOrNil("07f14cca-59e8-4e36-9fc6-396efb63a84b"),
				Empresa:       2,
				EmpresaNombre: "CrossLogic SA",
				FechaContable: fecha.Fecha(20230105),
				FechaVto:      fecha.Fecha(20230205),
				CompNombre:    "Factura A",
				NCompStr:      "0001-00516456",
				DetalleComp:   "Venta a una persona",
				Persona:       "Juan de los Palotes",
				Cuenta:        1234,
				Monto:         342343,
				SaldoAcum:     5468468,
				CreatedAt:     fecha.Fecha(20230902).Time(),
			},
		},
	}

	err = m.Excel(f)
	require.Nil(t, err)
	f.Close()
}

func TestExcelEmpty(t *testing.T) {

	f, err := os.Create("testdata/test_empty.xlsx")
	require.Nil(t, err)

	m := Mayor{
		SaldoInicio: 10000,
		SaldoFinal:  45390454,
	}

	err = m.Excel(f)
	require.Nil(t, err)
	f.Close()
}
