package unicidadesdef

import (
	"context"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/jackc/pgx/v4/pgxpool"
)

func create(ctx context.Context, conn *pgxpool.Pool, req *Def) (err error) {
	// Valido
	if req.Comitente == 0 {
		return errors.Errorf("No se definió ID de comitente")
	}
	if req.Nombre == "" {
		return deferror.Validation("Debe ingresar un nombre")
	}
	if req.Tipo == "" {
		return deferror.Validation("Debe definir el tipo")
	}

	query := `INSERT INTO unicidades_def (comitente, nombre, nombre_singular, descripcion, tipo, predefinida,
	genero, atts, atts_indexados, estados, activo, created_at) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)`

	_, err = conn.Exec(ctx, query, req.Comitente, req.Nombre, req.NombreSingular, req.Descripcion,
		req.Tipo, req.Predefinida, req.Genero, req.Atts, req.AttsIndexados, req.Estados, req.Activo, req.CreatedAt,
	)
	if err != nil {
		return deferror.DB(err, "inserting")
	}
	return

}

type ReadOneReq struct {
	Comitente int
	ID        int `json:",string"`
}

func readOne(ctx context.Context, conn *pgxpool.Pool, req ReadOneReq) (out Def, err error) {

	query := `SELECT id, comitente, nombre, nombre_singular, descripcion, tipo, predefinida, 
	genero, atts, atts_indexados, estados, activo, created_at, updated_at
	FROM unicidades_def
	WHERE comitente=$1 AND id=$2`

	err = conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(
		&out.ID, &out.Comitente, &out.Nombre, &out.NombreSingular, &out.Descripcion, &out.Tipo,
		&out.Predefinida, &out.Genero, &out.Atts, &out.AttsIndexados, &out.Estados, &out.Activo,
		&out.CreatedAt, &out.UpdatedAt)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}

	return
}

type ReadManyResp struct {
	ID          int `json:",string"`
	Nombre      string
	Descripcion string
	Tipo        string
	CreatedAt   *time.Time
}

func buscar(ctx context.Context, conn *pgxpool.Pool, comitente int) (out []ReadManyResp, err error) {
	out = []ReadManyResp{}

	// Valido
	if comitente == 0 {
		return out, deferror.Validation("no se ingresó comitente")
	}

	query := `SELECT id, nombre, descripcion, tipo, created_at
	FROM unicidades_def
	WHERE comitente=$1`

	rows, err := conn.Query(ctx, query, comitente)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		v := ReadManyResp{}
		err = rows.Scan(
			&v.ID, &v.Nombre, &v.Descripcion, &v.Tipo, &v.CreatedAt)
		if err != nil {
			return out, deferror.DB(err, "scanning")
		}
		out = append(out, v)
	}

	return
}

func readMany(ctx context.Context, conn *pgxpool.Pool, comitente int) (out []Def, err error) {
	out = []Def{}

	// Valido
	if comitente == 0 {
		return out, deferror.Validation("no se ingresó comitente")
	}

	query := `
	SELECT id, comitente, nombre, nombre_singular, descripcion, tipo, predefinida, 
	genero, atts, atts_indexados, estados, activo, created_at, updated_at
	FROM unicidades_def
	WHERE comitente=$1`

	rows, err := conn.Query(ctx, query, comitente)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		v := Def{}
		err = rows.Scan(
			&v.ID, &v.Comitente, &v.Nombre, &v.NombreSingular, &v.Descripcion, &v.Tipo,
			&v.Predefinida, &v.Genero, &v.Atts, &v.AttsIndexados, &v.Estados, &v.Activo,
			&v.CreatedAt, &v.UpdatedAt,
		)
		if err != nil {
			return out, deferror.DB(err, "scanning")
		}
		out = append(out, v)
	}

	return
}

func update(ctx context.Context, conn *pgxpool.Pool, req *Def) (err error) {

	query := `UPDATE unicidades_def 
	SET nombre=$3, nombre_singular=$4, descripcion=$5, tipo=$6, predefinida=$7, 
	genero=$8, atts=$9, atts_indexados=$10, estados=$11, activo=$12, updated_at=$13
	WHERE comitente=$1 AND id=$2`

	_, err = conn.Exec(ctx, query, req.Comitente, req.ID,
		&req.Nombre, &req.NombreSingular, &req.Descripcion, &req.Tipo,
		&req.Predefinida, &req.Genero, &req.Atts, &req.AttsIndexados, &req.Estados, &req.Activo,
		&req.UpdatedAt)
	if err != nil {
		return deferror.DB(err, "updating")
	}

	return
}
