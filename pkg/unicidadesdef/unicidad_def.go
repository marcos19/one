// Package unicidades maneja las definiciones de unicidades. Estas son entidades que crea
// el usuario, cada una tiene un ID y una serie de atributos personalizables
// por el usuario.s
//
// Se pueden definir para ser:
// - Productos: aquellos productos que definan que abren por unicidad, para
// usarlos se deberá seleccionar (o crear) una unicidad.
// - Valores: se pueden definir cuentas contables como medio de pago que
// abren por unicidad. Entonces por ejemplo crear una "Retención rutas provinciales Chaco"
// porque necesitan realizar ciertos controles programáticos.
package unicidadesdef

import (
	"database/sql/driver"
	"encoding/json"
	"time"

	"github.com/cockroachdb/errors"
)

type Def struct {
	ID             int `json:",string"`
	Comitente      int `json:"-"`
	Nombre         string
	NombreSingular string // Sin la s final
	Tipo           TipoUnicidad
	Predefinida    TipoUnicidadPredefinida // Las que son especiales (Como los cheques)
	Descripcion    string
	Genero         string
	Activo         bool
	Atts           Atts
	AttsIndexados  Atts
	Estados        Estados
	CreatedAt      *time.Time
	UpdatedAt      *time.Time
}

type TipoUnicidad string

const (
	DeUso       = TipoUnicidad("Uso")
	Referencial = TipoUnicidad("Referencial")
)

type TipoUnicidadPredefinida string

const (
	TipoChequeDeTercero = TipoUnicidadPredefinida("Cheque de tercero")
)

func (u Def) TableName() string {
	return "unicidades_def"
}

// Estado detalla cada estado por el que puede pasar una unicidad 'de uso'.
type Estado struct {
	Nombre         string
	Contabiliza    bool
	CuentaContable int
}

type Estados []Estado

// Value cumple con la interface SQL
func (e Estados) Value() (driver.Value, error) {
	return json.Marshal(e)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (e *Estados) Scan(src interface{}) error {
	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New("Type assertion error .([]byte)")
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, e)
	if err != nil {
		return errors.Wrap(err, "Unmarshalling")
	}

	return nil
}

type Att struct {
	Nombre              string
	Type                TipoDato
	Obligatorio         bool
	IncluirEnDetalle    int  // Si es distinto de cero lo inluye para fomar el string
	IncluirKeyEnDetalle bool // si es true va a decir Lote: 8; Cupón: 3
	Funcion             Funcion
}

type TipoDato string

const (
	TipoString = TipoDato("string")
	TipoBool   = TipoDato("bool")
	TipoFecha  = TipoDato("fecha")
	TipoD2     = TipoDato("d2")
	TipoInt    = TipoDato("int")
)

// Atts es un map de key valor para atributos personalizados
// Esta estructura está pensada para satisfacer la búsqueda en la base de datos.
// Voy a usar otro artefacto para darle la jerarquía que quiero y mostrarlos
// como me convenga.
type Atts []Att

// Value cumple con la interface SQL
func (a Atts) Value() (driver.Value, error) {
	return json.Marshal(a)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (a *Atts) Scan(src interface{}) error {
	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New("Type assertion error .([]byte)")
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, a)
	if err != nil {
		return errors.Wrap(err, "Unmarshalling")
	}

	return nil
}

type Funcion string

const (
	FuncionMonto    = Funcion("monto")
	FuncionFechaVto = Funcion("fecha vto")
)
