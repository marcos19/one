package unicidadesdef

import "context"

type ReaderOne interface {
	ReadOne(context.Context, ReadOneReq) (Def, error)
}
