package unicidadesdef

import (
	"context"
	"fmt"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/dgraph-io/ristretto"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler es el encargado de recibir los http requests y emitir los responses.
type Handler struct {
	conn     *pgxpool.Pool
	cache    *ristretto.Cache
	eventbus EventBus
}

// NewHandler instancia el handler de fórmulas.
func NewHandler(args HandlerArgs) (h *Handler, err error) {

	// Valido
	if args.Conn == nil {
		return h, deferror.Validation("Conn no pueder ser nil")
	}
	if args.Cache == nil {
		return h, deferror.Validation("Cache no pueder ser nil")
	}

	h = &Handler{
		eventbus: args.EventBus,
		conn:     args.Conn,
		cache:    args.Cache,
	}

	return
}

type HandlerArgs struct {
	Conn     *pgxpool.Pool
	EventBus EventBus
	Cache    *ristretto.Cache
}

type EventBus interface {
	CambióUnicidad(crud string, u Def)
}

func (h *Handler) Create(ctx context.Context, req Def) (err error) {

	// Persisto
	err = create(ctx, h.conn, &req)
	if err != nil {
		return deferror.DB(err, "persistiendo definición de unicidad")
	}

	return
}

// HandleMany devuelve el listado de todas las unicidades
func (h *Handler) ReadMany(ctx context.Context, comitente int) (out []ReadManyResp, err error) {

	// Busco
	out, err = buscar(ctx, h.conn, comitente)
	if err != nil {
		return out, deferror.DB(err, "buscando unicidades")
	}

	// for i := range out {
	// 	if out[i].Atts == nil {
	// 		out[i].Atts = []Att{}
	// 	}
	// 	if out[i].AttsIndexados == nil {
	// 		out[i].AttsIndexados = []Att{}
	// 	}
	// }

	return
}

//  HandleDefinicionesParaStore devuelve las definiciones para el store del frontend
func (h *Handler) HandleDefinicionesParaStore(ctx context.Context, comitente int) (out []Def, err error) {

	out, err = readMany(ctx, h.conn, comitente)
	if err != nil {
		return out, deferror.DB(err, "buscando unicidades")
	}
	return

}

// HandleOne devuelve el listado de todas las unicidades
func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (out Def, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó el ID de comitente")
	}
	if req.ID == 0 {
		return out, deferror.Validation("no se ingresó el ID")
	}

	// Busco cache
	key := hashInt(req)
	val, estaba := h.cache.Get(key)
	if estaba {
		out, ok := val.(Def)
		if ok && out.Comitente == req.Comitente {
			return out, nil
		}
	}

	// No estaba en cache, busco en DB
	out, err = readOne(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando por ID")
	}

	// Guardo en cache
	h.cache.Set(key, out, 0)

	return
}

func (h *Handler) Update(ctx context.Context, req Def) (err error) {

	// Valido
	if req.ID == 0 {
		return deferror.Validation("no se ingresó ID")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó el ID del comitente")
	}

	// Invalido cache
	h.cache.Del(hashInt(ReadOneReq{ID: req.ID, Comitente: req.Comitente}))

	// Persisto
	err = update(ctx, h.conn, &req)
	if err != nil {
		return deferror.DB(err, "persistiendo definición de unicidad")
	}

	return
}

func hashInt(req ReadOneReq) string {
	return fmt.Sprintf("%v/%v/%v", "udef", req.Comitente, req.ID)
}
