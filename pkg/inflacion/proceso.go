package inflacion

import (
	"context"
	"fmt"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/inflacion/config"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/rs/zerolog/log"
)

type Req struct {
	Comitente int
	Usuario   string

	Empresa int `json:",string"`
	Config  int `json:",string"`
	Desde   fecha.Mes
	Hasta   fecha.Mes
}

// AjustePorInflacion calcula y genera las ops para los meses ingresados.
func AjustePorInflacion(
	ctx context.Context,
	tx pgx.Tx,
	req Req,
	cfg config.Config,
	num comps.Numerador,
) (out []*ops.Op, err error) {

	// Valido
	if req.Empresa == 0 {
		return out, deferror.Validation("no se definió empresa")
	}
	if req.Config == 0 {
		return out, deferror.Validation("No se definió config que se utilizará para registrar las operaciones")
	}
	if req.Desde.Zero() {
		return out, deferror.Validation("No se definió fecha desde")
	}
	if req.Hasta.Zero() {
		return out, deferror.Validation("No se definió fecha hasta")
	}
	if req.Desde.PrimerDia() > req.Hasta.PrimerDia() {
		return out, deferror.Validation("Fecha hasta debe ser posterior a fecha desde")
	}

	// Busco el coeficiente destino
	coefReq := CoeficienteReq{
		Comitente: req.Comitente,
		Mes:       req.Hasta,
		Indice:    cfg.Indice,
	}
	coefDestino, err := coeficiente(ctx, tx, coefReq)
	if err != nil {
		return out, errors.Wrap(err, "determinando coeficiente destino")
	}
	if coefDestino == 0 {
		return out, errors.Errorf("el coeficiente para el período %v no puede ser cero", req.Hasta)
	}

	// Determino meses
	meses := determinarMeses(req.Desde, req.Hasta)

	for _, mes := range meses {
		if mes.Posterior(req.Hasta) {
			// El último mes no se ajusta
			log.Debug().Str("mes", mes.String()).Msg("Finalizando proceso de ajuste de inflación")
			return
		}
		log.Debug().Str("mes", mes.String()).Msg("Inciando análisis")

		op, err := opAjusteInflacionMes(ctx, tx, cfg, req, mes, coefDestino)
		if err != nil {
			return out, errors.Wrapf(err, "creando op de mes %v", mes)
		}
		// Si no había ajustes, devuelve nil
		if op != nil {
			out = append(out, op)
		}
	}

	return
}

// Realiza la op con el ajuste de inflación correspondiente al mes ingresado.
// correspondiente al mes ingresado
func opAjusteInflacionMes(
	ctx context.Context,
	tx pgx.Tx,
	cfg config.Config,
	req Req,
	mes fecha.Mes,
	coefDestino float64,
) (*ops.Op, error) {

	// Busco el coeficiente
	coefReq := CoeficienteReq{
		Comitente: req.Comitente,
		Mes:       mes,
		Indice:    cfg.Indice,
	}
	coefOrigen, err := coeficiente(ctx, tx, coefReq)
	if err != nil {
		return nil, errors.Wrapf(err, "determinando coeficiente origen mes %v", mes)
	}

	// Busco saldos del mes
	saldosReq := saldosMensualesReq{
		comitente: req.Comitente,
		empresa:   req.Empresa,
		mes:       mes,
	}
	saldos, err := saldosMensuales(tx, ctx, saldosReq)
	if err != nil {
		return nil, errors.Wrapf(err, "realizando ajuste por inflación mes %v", mes)
	}
	log.Debug().Str("mes", mes.String()).Int("cantidad", len(saldos)).Msg("Saldos determinados")

	if len(saldos) == 0 {
		return nil, nil
	}

	// Calculo los asientos
	ajustes := ajustarSaldos(saldos, coefOrigen, coefDestino)
	log.Debug().Str("mes", mes.String()).Int("cantidad", len(saldos)).Msg("Ajustes determinados")
	if len(ajustes) == 0 {
		return nil, nil
	}

	// Creo la op
	op := ops.Op{}
	op.Programa = "inflacion"
	op.ID, _ = uuid.NewV1()
	op.TranDef = req.Config
	op.Comitente = req.Comitente
	op.Empresa = req.Empresa

	op.Fecha = mes.UltimoDia()
	op.FechaOriginal = mes.UltimoDia()
	op.CompID = cfg.Comp
	op.Detalle = fmt.Sprintf("Ajuste por inflación %v => %v (%.4f => %.4f)", mes.String(), req.Hasta.String(), coefOrigen, coefDestino)
	op.Programa = Programa
	op.Usuario = req.Usuario
	op.Impresion = tipos.JSON{
		"CoefOrigen":    coefOrigen,
		"CoefDestino":   coefDestino,
		"CoefUtilizado": coefDestino / coefOrigen,
	}

	var sum dec.D2
	for _, v := range ajustes {
		p := ops.Partida{}
		p.ID, _ = uuid.NewV1()
		p.Cuenta = v.Cuenta
		if v.Centro != 0 {
			p.Centro = new(int)
			*p.Centro = v.Centro
		}
		if v.Producto != uuid.Nil {
			p.Producto = new(uuid.UUID)
			*p.Producto = v.Producto
		}
		if v.SKU != uuid.Nil {
			p.SKU = new(uuid.UUID)
			*p.SKU = v.SKU
		}
		p.Monto = v.Monto
		sum += v.Monto
		p.Detalle = new(string)
		*p.Detalle = op.Detalle
		p.TipoAsiento = new(ops.TipoAsiento)
		*p.TipoAsiento = ops.Inflacion
		op.PartidasContables = append(op.PartidasContables, p)
	}

	// Cierro el asiento con la cuenta de RECPAM
	p := ops.Partida{}
	p.ID, _ = uuid.NewV1()
	p.Cuenta = cfg.CuentaRecpam
	p.Monto = -sum
	p.Detalle = new(string)
	*p.Detalle = op.Detalle
	p.TipoAsiento = new(ops.TipoAsiento)
	*p.TipoAsiento = ops.Inflacion
	op.PartidasContables = append(op.PartidasContables, p)

	return &op, nil
}

type saldo struct {
	Cuenta   int
	Centro   int
	Monto    dec.D2
	Producto uuid.UUID
	SKU      uuid.UUID
}

// Devuelve los ajustes que hay que contabilizar
func ajustarSaldos(ss []saldo, coefOrigen, coefDestino float64) (ajustes []saldo) {
	coef := coefDestino / coefOrigen
	for _, v := range ss {
		v.Monto = dec.NewD2(v.Monto.Float()*coef) - v.Monto
		if v.Monto != 0 {
			ajustes = append(ajustes, v)
		}
	}

	return
}

type CoeficienteReq struct {
	Comitente int
	Mes       fecha.Mes
	Indice    string
}

// Busca el coeficiente de inflación para ese mes
func coeficiente(ctx context.Context, tx pgx.Tx, req CoeficienteReq) (coef float64, err error) {

	// Valido
	if req.Comitente == 0 {
		return coef, deferror.Validation("no se ingresó ID de comitente")
	}
	if req.Indice == "" {
		return coef, deferror.Validation("no se ingresó índice")
	}
	if req.Mes.Zero() {
		return coef, deferror.Validation("no se ingresó mes")
	}

	query := `
SELECT coeficiente FROM indices_inflacion
WHERE 
	comitente = $1 AND
	mes = $2 AND 
	indice = $3
	`
	err = tx.
		QueryRow(ctx, query, req.Comitente, req.Mes, req.Indice).
		Scan(&coef)
	if err == pgx.ErrNoRows {
		return coef, errors.Errorf("no se encontró el coeficiente para el período %v", req.Mes)
	}
	if err != nil {
		return coef, errors.Wrap(err, "buscando índice")
	}

	return
}

// Busca el coeficiente de inflación para ese mes
func Coeficiente(ctx context.Context, conn *pgxpool.Pool, req CoeficienteReq) (coef float64, err error) {

	// Valido
	if req.Comitente == 0 {
		return coef, deferror.Validation("no se ingresó ID de comitente")
	}
	if req.Indice == "" {
		return coef, deferror.Validation("no se ingresó índice")
	}
	if req.Mes.Zero() {
		return coef, deferror.Validation("no se ingresó mes")
	}

	query := `
SELECT coeficiente FROM indices_inflacion
WHERE 
	comitente = $1 AND
	mes = $2 AND 
	indice = $3
	`
	err = conn.
		QueryRow(ctx, query, req.Comitente, req.Mes, req.Indice).
		Scan(&coef)
	if err == pgx.ErrNoRows {
		return coef, errors.Errorf("no se encontró coeficiente")
	}
	if err != nil {
		return coef, errors.Wrap(err, "buscando índice")
	}

	return
}

type saldosMensualesReq struct {
	comitente int
	empresa   int
	mes       fecha.Mes
}

// saldosMensuales devuelve los saldos de las cuentas ajustables y el
// coeficiente con que deben ajustarse.
func saldosMensuales(tx pgx.Tx, ctx context.Context, req saldosMensualesReq) (out []saldo, err error) {

	// Valido
	if req.comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}
	if req.empresa == 0 {
		return out, deferror.Validation("no se ingresó ID de empresa")
	}
	if req.mes.Zero() {
		return out, deferror.Validation("no se ingresó mes")
	}

	query := `
SELECT cuenta, centro, producto, sku, sum(monto)
FROM partidas
WHERE 
	fecha_contable >= $1 AND 
	fecha_contable <= $2 AND
	comitente = $3 AND
	empresa = $4 AND
	cuenta IN (
		SELECT id 
		FROM cuentas 
		WHERE comitente = $3 AND ajusta_por_inflacion = TRUE
	) AND
	tipo_asiento IS NULL
GROUP BY cuenta, centro, producto, sku
HAVING sum(monto) <> 0;
	`

	// log.Trace().
	// 	Str("desde", mes.PrimerDia().String()).
	// 	Str("hasta", mes.UltimoDia().String()).
	// 	Int("comitente", comitente).
	// 	Msg(query)
	// Busco saldos
	rows, err := tx.Query(
		ctx, query,
		req.mes.PrimerDia(), req.mes.UltimoDia(), req.comitente, req.empresa,
	)
	if err != nil {
		return out, deferror.DBf(err, "buscando saldos del mes %v", req.mes)
	}
	defer rows.Close()
	// Escaneo saldos
	for rows.Next() {

		saldo := saldo{}
		var centro *int
		var producto *uuid.UUID
		var sku *uuid.UUID
		var monto *int

		err = rows.Scan(&saldo.Cuenta, &centro, &producto, &sku, &monto)
		if err != nil {
			return out, errors.Wrapf(err, "escaneando row index=%v", len(out))
		}

		if producto != nil {
			saldo.Producto = *producto
		}
		if sku != nil {
			saldo.SKU = *sku
		}
		if centro != nil {
			saldo.Centro = *centro
		}
		if monto != nil {
			saldo.Monto = dec.D2(*monto)
		}

		out = append(out, saldo)
	}

	return
}

func determinarMeses(desde, hasta fecha.Mes) (out []fecha.Mes) {

	for i := 0; ; i++ {
		mes := desde.SumarMeses(i)
		if mes.Posterior(hasta) {
			// El último mes no se ajusta
			log.Debug().Str("mes", mes.String()).Msg("Finalizando proceso de ajuste de inflación")
			break
		}
		out = append(out, mes)
	}
	return
}
