package inflacion

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/inflacion/config"
	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/niler"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler es el encargado de recibir los http requests y emitir los responses.
type Handler struct {
	conn          *pgxpool.Pool
	opInserter    ops.Inserter
	opDeleter     ops.Deleter
	compNumerador comps.Numerador
}

// NewHandler inicializa un cache y devuelve el puntero a este.
func NewHandler(
	conn *pgxpool.Pool,
	opH OpsHandler,
	num comps.Numerador,
) (out *Handler, err error) {

	if niler.IsNil(opH) {
		return out, errors.Errorf("ops.Deleter no puede ser nil")
	}
	out = &Handler{}
	out.conn = conn
	out.opInserter = opH
	out.opDeleter = opH
	out.compNumerador = num
	return
}

type ReadManyReq struct {
	Comitente int
	Empresa   int `json:",string"`
}

// Lista los ajustes ya realizados
func (h *Handler) ReadMany(ctx context.Context, req ReadManyReq) (out []Tran, err error) {

	if req.Comitente == 0 {
		return out, deferror.ComitenteIndefinido()
	}
	if req.Empresa == 0 {
		return out, deferror.EmpresaIndefinida()
	}
	q := `SELECT id, empresa, config, desde, hasta, ops, created_at, usuario FROM inflacion WHERE comitente = $1 AND empresa = $2 ORDER BY desde`
	rows, err := h.conn.Query(ctx, q, req.Comitente, req.Empresa)
	if err != nil {
		return out, deferror.DB(err, "buscando ajustes")
	}
	defer rows.Close()

	for rows.Next() {
		t := Tran{}
		err = rows.Scan(&t.ID, &t.Empresa, &t.Config, &t.Desde, &t.Hasta, &t.Ops, &t.CreatedAt, &t.Usuario)
		if err != nil {
			return out, errors.Wrap(err, "escaneando ajuste")
		}
		out = append(out, t)
	}
	if out == nil {
		out = []Tran{}
	}
	return
}

type ReadOneReq struct {
	Comitente int
	ID        uuid.UUID
}

// Lista los ajustes ya realizados
func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (out Tran, err error) {

	q := `SELECT id, empresa, config, desde, hasta, ops, created_at, usuario FROM inflacion WHERE comitente=$1 AND id=$2`
	rows, err := h.conn.Query(ctx, q, req.Comitente, req.ID)
	if err != nil {
		return out, deferror.DB(err, "buscando ajustes")
	}
	defer rows.Close()

	for rows.Next() {
		t := Tran{}
		err = rows.Scan(&t.ID, &t.Empresa, &t.Config, &t.Desde, &t.Hasta, &t.Ops, &t.CreatedAt, &t.Usuario)
		if err != nil {
			return out, errors.Wrap(err, "escaneando ajuste")
		}
		return t, nil
	}
	return
}

// HandlePrevisualizar genera las ops y las devuelve para que las vea el usuario.
func (h *Handler) Previsualizar(ctx context.Context, req Req) (out []*ops.Op, err error) {

	// Busco config
	cfg, err := config.ReadOne(ctx, h.conn, config.ReadOneReq{Comitente: req.Comitente, ID: req.Config})
	if err != nil {
		return out, errors.Wrapf(err, "buscando config")
	}

	// Inicio transacción
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return out, deferror.DB(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)

	// Genero las ops
	out, err = AjustePorInflacion(ctx, tx, req, cfg, h.compNumerador)
	if err != nil {
		return out, errors.Wrap(err, "generando operaciones de ajuste por inflación")
	}

	return
}

// HandleConfirmar genera las ops con los ajustes por inflación y los persiste.
func (h *Handler) Confirmar(ctx context.Context, req Req) (out []*ops.Op, err error) {

	// Busco config
	cfg, err := config.ReadOne(ctx, h.conn, config.ReadOneReq{Comitente: req.Comitente, ID: req.Config})
	if err != nil {
		return out, errors.Wrapf(err, "buscando config")
	}

	// Inicio transacción
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return out, deferror.DB(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)

	// Genero las ops
	oo, err := AjustePorInflacion(ctx, tx, req, cfg, h.compNumerador)
	if err != nil {
		return out, errors.Wrap(err, "generando ops")
	}
	if oo == nil {
		return out, errors.New("no había ningún ajuste a persistir")
	}

	// Persisto las ops
	err = h.opInserter.Insert(ctx, tx, oo, nil)
	if err != nil {
		return out, errors.Wrap(err, "procesando ops")
	}

	// Genero tran
	tran, err := crearTran(req, cfg, oo)
	if err != nil {
		return out, errors.Wrap(err, "creando tran")
	}

	// Persisto tran
	err = persistirTran(ctx, tx, tran)
	if err != nil {
		return out, err
	}
	for i := range oo {
		oo[i].TranID = tran.ID
	}

	// Confirmo transacción
	err = tx.Commit(ctx)
	if err != nil {
		return out, deferror.DB(err, "confirmando transacción")
	}

	return
}

func (h *Handler) Delete(ctx context.Context, req ReadOneReq) (err error) {
	if req.Comitente == 0 {
		return deferror.ComitenteIndefinido()
	}
	if req.ID == uuid.Nil {
		return errors.Errorf("no se ingresó ID")
	}

	// Inicio transacción
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return errors.Wrap(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)

	{ // Elimino de tabla inflación
		q := `DELETE FROM inflacion WHERE comitente=$1 AND id=$2`
		res, err := tx.Exec(ctx, q, req.Comitente, req.ID)
		if err != nil {
			return deferror.DB(err, "borrando tran")
		}

		if res.RowsAffected() == 0 {
			return errors.Errorf("no se borró ningún registro")
		}
	}

	{ // Elimino ops asociadas
		// Busco ops de la tran
		ids := []uuid.UUID{}
		q := `SELECT id FROM ops WHERE comitente=$1 AND tran_id=$2`
		rows, err := tx.Query(ctx, q, req.Comitente, req.ID)
		if err != nil {
			return deferror.DB(err, "determinando IDs de ops")
		}
		defer rows.Close()

		for rows.Next() {
			id := uuid.UUID{}
			err = rows.Scan(&id)
			if err != nil {
				return errors.Wrap(err, "escaneando ID")
			}
			ids = append(ids, id)
		}

		if len(ids) == 0 {
			return errors.Errorf("no se pudo determinar IDs de ops")
		}

		err = h.opDeleter.Delete(ctx, tx, req.Comitente, ids)
		if err != nil {
			return errors.Wrapf(err, "borrando ops")
		}
	}

	// Confirmo transacción
	err = tx.Commit(ctx)
	if err != nil {
		return deferror.DB(err, "confirmando transacción")
	}
	return
}
func cuentaRecpam(conn *pgxpool.Pool, comitente int) (cta int, err error) {

	query := "SELECT id FROM cuentas WHERE comitente = $1 AND funcion = $2"
	err = conn.QueryRow(context.Background(), query, comitente, cuentas.Recpam).Scan(&cta)
	if err != nil {
		return 0, err
	}

	return
}
