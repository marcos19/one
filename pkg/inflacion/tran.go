package inflacion

import (
	"context"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/inflacion/config"
	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/crosslogic/fecha"
	uuid "github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
)

type Tran struct {
	ID        uuid.UUID
	Comitente int `json:",string"`
	Empresa   int `json:",string"`
	Config    int `json:",string"`
	Desde     fecha.Mes
	Hasta     fecha.Mes
	Ops       []Op
	CreatedAt time.Time
	Usuario   string
}

type Op struct {
	ID               uuid.UUID
	Fecha            fecha.Fecha
	Nombre           string
	NCompStr         string
	Detalle          string
	CantidadPartidas int
}

func crearTran(req Req, cfg config.Config, oo []*ops.Op) (out Tran, err error) {

	out.ID, err = uuid.NewV1()
	if err != nil {
		return out, err
	}
	out.Comitente = cfg.Comitente
	out.Config = cfg.ID
	out.Empresa = cfg.Empresa
	out.Desde = req.Desde
	out.Hasta = req.Hasta
	for i, v := range oo {
		o := Op{
			ID:               v.ID,
			Fecha:            v.Fecha,
			Nombre:           v.CompNombre,
			NCompStr:         v.NCompStr,
			Detalle:          v.Detalle,
			CantidadPartidas: len(v.PartidasContables),
		}
		out.Ops = append(out.Ops, o)

		// Pego ID de tran a la op
		oo[i].TranID = v.ID
	}
	out.Usuario = req.Usuario
	return

}

func persistirTran(ctx context.Context, tx pgx.Tx, t Tran) (err error) {

	q := `INSERT INTO inflacion (id, comitente, empresa, config, desde, hasta, ops, usuario, created_at) VALUES (
		$1, $2, $3, $4, $5, $6, $7, $8, now());
	`

	_, err = tx.Exec(ctx, q, t.ID, t.Comitente, t.Empresa, t.Config, t.Desde, t.Hasta, t.Ops, t.Usuario)
	if err != nil {
		return deferror.DB(err, "insertando tran")
	}

	return
}

/*
CREATE TABLE inflacion (
	id UUID PRIMARY KEY,
	comitente INT NOT NULL REFERENCES comitentes,
	empresa INT NOT NULL REFERENCES empresas,
	config INT NOT NULL REFERENCES inflacion_config,
	desde DATE NOT NULL,
	hasta DATE NOT NULL,
	ops JSONB NOT NULL,
	created_at TIMESTAMP WITH TIME ZONE DEFAULT now(),
	usuario STRING NOT NULL
);
*/
