package inflacion

import (
	"testing"

	"github.com/crosslogic/fecha"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDeterminarMeses(t *testing.T) {

	from := fecha.NewMesMust(2022, 2)
	to := fecha.NewMesMust(2023, 1)
	mm := determinarMeses(from, to)

	const expectedLen = 12
	require.Len(t, mm, expectedLen)
	assert.Equal(t, from, mm[0])
	assert.Equal(t, to, mm[expectedLen-1])
}
