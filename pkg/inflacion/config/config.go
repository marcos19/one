package config

import (
	"context"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

type Config struct {
	ID           int `json:",string"`
	Comitente    int `json:"-"`
	Empresa      int `json:",string"`
	Comp         int `json:",string"`
	CuentaRecpam int `json:",string"`
	Indice       string
	Nombre       string
	CreatedAt    time.Time
	UpdatedAt    *time.Time
}

type ReadOneReq struct {
	Comitente int
	ID        int `json:",string"`
}

func ReadOne(ctx context.Context, conn *pgxpool.Pool, req ReadOneReq) (out Config, err error) {
	query := `
		SELECT id, comitente, empresa, comp, cuenta_recpam, indice, nombre, created_at, updated_at
		FROM inflacion_config
		WHERE comitente=$1 AND id=$2
	`
	err = conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(
		&out.ID, &out.Comitente, &out.Empresa, &out.Comp, &out.CuentaRecpam, &out.Indice,
		&out.Nombre, &out.CreatedAt, &out.UpdatedAt)
	if err != nil {
		return out, deferror.DB(err, "buscando config")
	}
	return
}

type ReadManyReq struct {
	Comitente int
	Empresa   int `json:",string"`
}

func ReadMany(ctx context.Context, conn *pgxpool.Pool, req ReadManyReq) (out []Config, err error) {

	if req.Empresa == 0 {
		return out, errors.Errorf("no se seleccionó empresa")
	}

	out = []Config{}
	query := `
		SELECT id, comitente, empresa, comp, cuenta_recpam, indice, nombre, created_at, updated_at
		FROM inflacion_config
		WHERE comitente=$1 AND empresa=$2
		ORDER BY nombre
	`
	rows, err := conn.Query(ctx, query, req.Comitente, req.Empresa)
	if err != nil {
		return out, deferror.DB(err, "buscando config")
	}
	defer rows.Close()

	for rows.Next() {
		c := Config{}
		err = rows.Scan(
			&c.ID, &c.Comitente, &c.Empresa, &c.Comp, &c.CuentaRecpam, &c.Indice,
			&c.Nombre, &c.CreatedAt, &c.UpdatedAt)

		if err != nil {
			return out, errors.Wrapf(err, "escaneando config")
		}
		out = append(out, c)
	}
	return
}

func Create(ctx context.Context, conn *pgxpool.Pool, req Config) (err error) {
	// Valido
	if req.Comitente == 0 {
		return deferror.ComitenteIndefinido()
	}
	if req.Empresa == 0 {
		return deferror.EmpresaIndefinida()
	}
	if req.Comp == 0 {
		return errors.Errorf("no se definió comprobante a utilizar")
	}
	if req.CuentaRecpam == 0 {
		return errors.Errorf("no se definió cuenta RECPAM a utilizar")
	}
	if req.Indice == "" {
		return errors.Errorf("no se definió el índice a utilizar")
	}
	if req.Nombre == "" {
		return errors.Errorf("no se definió nombre de la transacción")
	}

	query := `
		INSERT INTO inflacion_config (comitente, empresa, comp, cuenta_recpam, 
			indice, nombre, created_at) VALUES (
		$1, $2,$3,$4,$5,$6, now());`

	_, err = conn.Exec(ctx, query, req.Comitente, req.Empresa, req.Comp, req.CuentaRecpam,
		req.Indice, req.Nombre)
	if err != nil {
		return deferror.DB(err, "creating config")
	}

	return
}

func Update(ctx context.Context, conn *pgxpool.Pool, req Config) (err error) {
	// Valido
	if req.Comitente == 0 {
		return deferror.ComitenteIndefinido()
	}
	if req.ID == 0 {
		return errors.Errorf("no se ingresó ID")
	}
	if req.Empresa == 0 {
		return deferror.EmpresaIndefinida()
	}
	if req.Comp == 0 {
		return errors.Errorf("no se definió comprobante a utilizar")
	}
	if req.CuentaRecpam == 0 {
		return errors.Errorf("no se definió cuenta RECPAM a utilizar")
	}
	if req.Indice == "" {
		return errors.Errorf("no se definió el índice a utilizar")
	}
	if req.Nombre == "" {
		return errors.Errorf("no se definió nombre de la transacción")
	}

	query := `
		UPDATE inflacion_config SET 
			comp=$3, 
			cuenta_recpam=$4, 
			indice=$5, 
			nombre=$6, 
			updated_at=now()
		WHERE comitente=$1 AND id=$2;`

	res, err := conn.Exec(ctx, query, req.Comitente, req.ID, req.Comp, req.CuentaRecpam,
		req.Indice, req.Nombre)
	if err != nil {
		return deferror.DB(err, "creating config")
	}
	if res.RowsAffected() == 0 {
		return errors.Errorf("no se modificó ningún registro")
	}

	return
}

func Delete(ctx context.Context, conn *pgxpool.Pool, req ReadOneReq) (err error) {

	// Está usada
	count := 0
	err = conn.QueryRow(ctx, "SELECT COUNT(id) FROM ops WHERE comitente=$1 AND tran_def=$2", req.Comitente, req.ID).Scan(&count)
	if err != nil {
		return deferror.DB(err, "determinando si la operación estaba usada")
	}

	if count > 0 {
		return errors.Errorf("no se puede borrar porque la config está usada %v veces en la contabilidad", count)
	}

	// Se puede borrar
	res, err := conn.Exec(ctx, "DELETE FROM inflacion_config WHERE comitente=$1 AND id=$2", req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "borrando config")
	}

	// Se borra
	if res.RowsAffected() == 0 {
		return errors.Errorf("no se borró ninguna operación")
	}

	return
}

/*
CREATE TABLE inflacion_config (
	id SERIAL PRIMARY KEY,
	comitente INT NOT NULL REFERENCES comitentes,
	empresa INT NOT NULL REFERENCES empresas,
	comp INT NOT NULL REFERENCES comps,
	cuenta_recpam INT NOT NULL,
	indice STRING NOT NULL,
	nombre STRING NOT NULL,
	created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
	updated_at TIMESTAMP WITH TIME ZONE
);
*/
