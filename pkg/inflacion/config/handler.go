package config

import (
	"context"

	"github.com/jackc/pgx/v4/pgxpool"
)

type Handler struct {
	conn *pgxpool.Pool
}

func NewHandler(conn *pgxpool.Pool) *Handler {
	return &Handler{conn}
}

func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (Config, error) {
	return ReadOne(ctx, h.conn, req)
}

func (h *Handler) ReadMany(ctx context.Context, req ReadManyReq) ([]Config, error) {
	return ReadMany(ctx, h.conn, req)
}

func (h *Handler) Create(ctx context.Context, req Config) error {
	return Create(ctx, h.conn, req)
}

func (h *Handler) Update(ctx context.Context, req Config) error {
	return Update(ctx, h.conn, req)
}

func (h *Handler) Delete(ctx context.Context, req ReadOneReq) error {
	return Delete(ctx, h.conn, req)
}
