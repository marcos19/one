package inflacion

import (
	"time"

	"github.com/crosslogic/fecha"
)

// Programa es el nombre del programa que registra las operaciones
const Programa = "inflacion"

type IndiceInflacion struct {
	Comitente *int
	Tipo      string // IPC, IPIM, etc
	Mes       fecha.Mes
	CreatedAt time.Time
}
