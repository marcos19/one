package unicidades

import (
	"fmt"
	"testing"

	"bitbucket.org/marcos19/one/pkg/unicidadesdef"
	"github.com/stretchr/testify/assert"
)

func TestString(t *testing.T) {

	def := unicidadesdef.Def{}
	def.ID = 1
	def.Atts = []unicidadesdef.Att{
		{
			Nombre:           "Tarjeta",
			Type:             unicidadesdef.TipoString,
			IncluirEnDetalle: 1,
		},
		{
			Nombre:              "Vencimiento",
			Type:                unicidadesdef.TipoFecha,
			IncluirKeyEnDetalle: true,
			IncluirEnDetalle:    3,
		},
		{
			Nombre:           "Dato a ignorar",
			Type:             unicidadesdef.TipoFecha,
			IncluirEnDetalle: 0,
		},
		{
			Nombre:              "Verificada",
			Type:                unicidadesdef.TipoBool,
			IncluirKeyEnDetalle: true,
			IncluirEnDetalle:    8,
		},
	}

	def.AttsIndexados = unicidadesdef.Atts{
		{
			Nombre:           "Número",
			Type:             unicidadesdef.TipoInt,
			IncluirEnDetalle: 2,
		},
	}

	// Unicidad
	u := Unicidad{}
	u.Atts = map[string]interface{}{}
	u.AttsIndexados = map[string]interface{}{}
	u.Atts["Tarjeta"] = "VISA"
	u.Atts["Vencimiento"] = "2021-05-01"
	u.Atts["Dato a ignorar"] = "Datos innecesarios, lorem ipsum"
	u.Atts["Verificada"] = true
	u.AttsIndexados["Número"] = 3425345.

	str, err := String(u, def)
	assert.Nil(t, err)
	fmt.Println(str)
}
