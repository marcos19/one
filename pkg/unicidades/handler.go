package unicidades

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"bitbucket.org/marcos19/one/pkg/unicidadesdef"
	"github.com/cockroachdb/errors"
	"github.com/dgraph-io/ristretto"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler es el encargado de recibir los http requests y emitir los responses.
type Handler struct {
	conn                 *pgxpool.Pool
	cache                *ristretto.Cache
	unicidadesdefHandler *unicidadesdef.Handler
}

// NewHandler instancia el handler de fórmulas.
func NewHandler(args HandlerArgs) (h *Handler, err error) {
	h = &Handler{}
	if args.Conn == nil {
		return h, deferror.Validation("Conn no puede ser nil")
	}
	if args.Cache == nil {
		return h, deferror.Validation("Cache no puede ser nil")
	}
	if args.UnicidadesdefHandler == nil {
		return h, deferror.Validation("UnicidadesdefHandler no puede ser nil")
	}

	h = &Handler{
		conn:                 args.Conn,
		cache:                args.Cache,
		unicidadesdefHandler: args.UnicidadesdefHandler,
	}

	return
}

type HandlerArgs struct {
	Conn                 *pgxpool.Pool
	Cache                *ristretto.Cache
	UnicidadesdefHandler *unicidadesdef.Handler
}

// HandleCreate persiste una unicidad referencial.
func (h *Handler) Create(ctx context.Context, req *Unicidad) (err error) {

	// Corroboro que exista definición
	_, err = h.unicidadesdefHandler.ReadOne(ctx, unicidadesdef.ReadOneReq{Comitente: req.Comitente, ID: req.Definicion})
	if err != nil {
		return errors.Wrap(err, "buscando definición de unicidad")
	}

	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return deferror.DB(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)

	// Inserto
	err = Create(ctx, tx, req)
	if err != nil {
		return deferror.DB(err, "insertando unicidad")
	}

	err = tx.Commit(ctx)
	if err != nil {
		return deferror.DB(err, "confirmando transacción")
	}
	return
}

// HandleCreate persiste una unicidad referencial.
func (h *Handler) CreateTx(ctx context.Context, tx pgx.Tx, req *Unicidad) (err error) {

	// Corroboro que exista definición
	_, err = h.unicidadesdefHandler.ReadOne(ctx, unicidadesdef.ReadOneReq{Comitente: req.Comitente, ID: req.Definicion})
	if err != nil {
		return errors.Wrap(err, "buscando definición de unicidad")
	}

	// Inserto
	err = Create(ctx, tx, req)
	if err != nil {
		return deferror.DB(err, "insertando unicidad")
	}

	return
}

type ReadOneReq struct {
	ID        uuid.UUID
	Comitente int
}

// HandleBuscar devuelve el listado de todas las unicidades
func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (out Unicidad, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}
	if req.ID == uuid.Nil {
		return out, deferror.Validation("no se ingresó ID de unicidad")
	}

	// Busco en cache
	key := hashUUID(req)
	val, estaba := h.cache.Get(key)
	if estaba {
		out, ok := val.(Unicidad)
		if ok && out.Comitente == req.Comitente {
			return out, nil
		}
	}

	// Busco en DB
	out, err = readOne(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando por ID")
	}

	// Agrego a cache
	h.cache.Set(key, out, 0)

	return
}

// HandleBuscar devuelve el listado de todas las unicidades
func (h *Handler) ReadMany(ctx context.Context, req ReadManyReq) (out []Unicidad, err error) {

	// Busco la definición
	def, err := h.unicidadesdefHandler.ReadOne(ctx, unicidadesdef.ReadOneReq{
		Comitente: req.Comitente,
		ID:        req.Definicion,
	})
	if err != nil {
		return out, deferror.DBf(err, "buscando definición id %v", req.Definicion)
	}

	// Busco unicidades
	out, err = readMany(ctx, h.conn, def, req)
	if err != nil {
		return out, errors.Wrap(err, "buscando unicidades")
	}
	return

}

func (h *Handler) Update(ctx context.Context, req *Unicidad) (err error) {

	// Valido
	if req.Definicion == 0 {
		return deferror.Validation("No se determinó tipo de unicidad")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}

	// Busco anterior
	_, err = h.ReadOne(ctx, ReadOneReq{Comitente: req.Comitente, ID: req.ID})
	if err != nil {
		return errors.Wrap(err, "buscando registro a modificar")
	}

	// Invalido cache
	h.cache.Del(hashUUID(ReadOneReq{Comitente: req.Comitente, ID: req.ID}))

	// Modifico
	err = update(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "modificando unicidad")
	}

	return
}

func (h *Handler) UpdateWithTx(ctx context.Context, req *Unicidad, tx pgx.Tx) (err error) {

	// Valido
	if req.Definicion == 0 {
		return deferror.Validation("No se determinó tipo de unicidad")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}

	// Busco anterior
	_, err = h.ReadOne(ctx, ReadOneReq{Comitente: req.Comitente, ID: req.ID})
	if err != nil {
		return errors.Wrap(err, "buscando registro a modificar")
	}

	// Invalido cache
	h.cache.Del(hashUUID(ReadOneReq{Comitente: req.Comitente, ID: req.ID}))

	// Modifico
	err = updateWithTx(ctx, tx, req)
	if err != nil {
		return deferror.DB(err, "modificando unicidad")
	}

	return
}

// Crea un lote
type GenerarLoteReq struct {
	Comitente  int
	Definicion int `json:",string"`
	Cantidad   int
	Campos     []CampoLote
}

type CampoLote struct {
	Nombre    string
	Desde     int
	Loopear   bool
	ValorFijo string
}

// Genera las unicidades que aumentan número de serie
func (h *Handler) GenerarLote(ctx context.Context, req GenerarLoteReq) (out []Unicidad, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se definió ID de comitente")
	}
	if req.Definicion == 0 {
		return out, deferror.Validation("no se ingresó definición de unicidad")
	}
	if len(req.Campos) == 0 {
		return out, deferror.Validation("no se ingresó ningún campo")
	}

	// Busco def
	def, err := h.unicidadesdefHandler.ReadOne(ctx, unicidadesdef.ReadOneReq{
		Comitente: req.Comitente,
		ID:        req.Definicion,
	})
	if err != nil {
		return out, errors.Wrapf(err, "buscando definición de unicidad")
	}

	// Determino campo a loopear
	campo := ""
	desde := 0
	campoEsIndexado := true
	for _, v := range req.Campos {
		if v.Loopear {
			campo = v.Nombre
			desde = v.Desde
			break
		}
	}
	if campo == "" {
		return out, deferror.Validation("no se determinó el campo a aumentar")
	}

	// Genero los atributos fijos (todas las unicidades tendrán los mismos)
	atts := tipos.JSON{}
	attsIndexados := tipos.JSON{}
	for _, campoDef := range def.Atts {
		for _, campoReq := range req.Campos {
			if campoReq.Nombre == campoDef.Nombre && !campoReq.Loopear {
				// Agrego atributo fijo
				atts[campoReq.Nombre] = campoReq.ValorFijo
			}
		}
		if campoDef.Nombre == campo {
			campoEsIndexado = false
		}
	}
	for _, campoDef := range def.AttsIndexados {
		for _, campoReq := range req.Campos {
			if campoReq.Nombre == campoDef.Nombre && !campoReq.Loopear {
				// Agrego atributo fijo
				attsIndexados[campoReq.Nombre] = campoReq.ValorFijo
			}
		}
	}

	// Genero
	for i := 0; i < req.Cantidad; i++ {

		u := Unicidad{}
		u.ID, err = uuid.NewV1()
		if err != nil {
			return nil, errors.Wrap(err, "generando ID")
		}
		u.Comitente = req.Comitente
		u.Definicion = req.Definicion
		u.Atts = tipos.JSON{}

		// Copio atts sin indexar
		for k, v := range atts {
			u.Atts[k] = v
		}

		// Copio atts indexados
		u.AttsIndexados = tipos.JSON{}
		for k, v := range attsIndexados {
			u.AttsIndexados[k] = v
		}

		// Pego el número
		switch campoEsIndexado {
		case true:
			u.AttsIndexados[campo] = desde + i
		case false:
			u.Atts[campo] = desde + i
		}

		out = append(out, u)
	}

	return
}

func hashUUID(req ReadOneReq) string {
	return fmt.Sprintf("%v/%v/%x", "Unicidad", req.Comitente, req.ID)
}

func arrayUUID(ids []uuid.UUID) string {
	idsStr := []string{}
	for _, v := range ids {
		idsStr = append(idsStr, fmt.Sprintf("'%v'", v.String()))
	}
	return strings.Join(idsStr, ", ")
}
