package unicidades

import (
	"context"

	"github.com/jackc/pgx/v4"
)

// Devuelve una unicidad por su ID
type ReaderOne interface {
	ReadOne(context.Context, ReadOneReq) (Unicidad, error)
}

// Inserta una unicidad usando la Tx.
// El manejo de la transacción corresponde a la función llamadora.
type CreaterTx interface {
	CreateTx(context.Context, pgx.Tx, *Unicidad) (err error)
}
