package unicidades

import (
	"fmt"
	"sort"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/tipos"
	"bitbucket.org/marcos19/one/pkg/unicidadesdef"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
)

// Unicidad es el registro en la base de datos de cada una de las unicidades
// (vehículo, cheque, etc)
type Unicidad struct {
	ID            uuid.UUID
	Comitente     int `json:",string"`
	Definicion    int `json:",string"`
	AttsIndexados tipos.JSON
	Atts          tipos.JSON
	CreatedAt     *time.Time `json:",omitempty"`
	UpdatedAt     *time.Time `json:",omitempty"`
}

func (u Unicidad) TableName() string {
	return "unicidades"
}

// En base a la definición de la unidad devuelve un string con los
// campos que están marcados como imprimibles
func String(u Unicidad, def unicidadesdef.Def) (str string, err error) {

	type attStr struct {
		Index int
		Valor string
	}
	imprimibles := []attStr{}

	// Atts no indexados
	for _, v := range def.Atts {
		if v.IncluirEnDetalle > 0 {

			// Nombre del atributo
			s := ""
			if v.IncluirKeyEnDetalle {
				s = fmt.Sprintf("%v: ", v.Nombre)
			}

			// Busco valor del atributo
			switch v.Type {
			case unicidadesdef.TipoString:
				val, err := u.AttStr(v.Nombre)
				if err != nil {
					return str, errors.Wrapf(err, "nombrando atributo %v", v.Nombre)
				}
				s += val
			case unicidadesdef.TipoD2:
				val, err := u.AttD2(v.Nombre)
				if err != nil {
					return str, errors.Wrapf(err, "nombrando atributo %v", v.Nombre)
				}
				s += val.String()
			case unicidadesdef.TipoFecha:
				val, err := u.AttFecha(v.Nombre)
				if err != nil {
					return str, errors.Wrapf(err, "nombrando atributo %v", v.Nombre)
				}
				s += val.String()
			case unicidadesdef.TipoInt:
				val, err := u.AttInt(v.Nombre)
				if err != nil {
					return str, errors.Wrapf(err, "nombrando atributo %v", v.Nombre)
				}
				s += fmt.Sprint(val)
			case unicidadesdef.TipoBool:
				val, err := u.AttBool(v.Nombre)
				if err != nil {
					return str, errors.Wrapf(err, "nombrando atributo %v", v.Nombre)
				}
				if val {
					s += "Sí"
				} else {
					s += "No"
				}
			}

			imprimibles = append(imprimibles, attStr{
				Index: v.IncluirEnDetalle,
				Valor: s,
			})
		}
	}

	// Atts indexados
	for _, v := range def.AttsIndexados {
		if v.IncluirEnDetalle > 0 {

			// Nombre del atributo
			s := ""
			if v.IncluirKeyEnDetalle {
				s = fmt.Sprintf("%v: ", v.Nombre)
			}

			// Busco valor del atributo
			switch v.Type {
			case unicidadesdef.TipoString:
				val, err := u.AttStr(v.Nombre)
				if err != nil {
					return str, errors.Wrapf(err, "nombrando atributo %v", v.Nombre)
				}
				s += val
			case unicidadesdef.TipoD2:
				val, err := u.AttD2(v.Nombre)
				if err != nil {
					return str, errors.Wrapf(err, "nombrando atributo %v", v.Nombre)
				}
				s += val.String()
			case unicidadesdef.TipoFecha:
				val, err := u.AttFecha(v.Nombre)
				if err != nil {
					return str, errors.Wrapf(err, "nombrando atributo %v", v.Nombre)
				}
				s += val.String()
			case unicidadesdef.TipoInt:
				val, err := u.AttInt(v.Nombre)
				if err != nil {
					return str, errors.Wrapf(err, "nombrando atributo %v", v.Nombre)
				}
				s += fmt.Sprint(val)
			case unicidadesdef.TipoBool:
				val, err := u.AttBool(v.Nombre)
				if err != nil {
					return str, errors.Wrapf(err, "nombrando atributo %v", v.Nombre)
				}
				if val {
					s += "Sí"
				} else {
					s += "No"
				}
			}

			imprimibles = append(imprimibles, attStr{
				Index: v.IncluirEnDetalle,
				Valor: s,
			})
		}
	}

	sort.Slice(imprimibles, func(i, j int) bool {
		return imprimibles[i].Index < imprimibles[j].Index
	})

	ordenadas := []string{}
	for _, v := range imprimibles {
		ordenadas = append(ordenadas, v.Valor)
	}

	str = strings.Join(ordenadas, " | ")

	return
}

// AttFechaDiferida determina si tiene un atributo que función de fecha diferida.
// Si está, devuelve la fecha vencimiento de la unicidad
func AttFechaDiferida(def unicidadesdef.Def, u *Unicidad) (nombre string) {

	for _, v := range def.Atts {
		if v.Funcion == unicidadesdef.FuncionFechaVto {
			return v.Nombre
		}
	}

	for _, v := range def.AttsIndexados {
		if v.Funcion == unicidadesdef.FuncionFechaVto {
			return v.Nombre
		}
	}
	return
}

// AttMonto determina si tiene un atributo con función de monto.
// Si está, devuelve el nombre.
func AttMonto(def unicidadesdef.Def, u *Unicidad) (nombre string) {

	for _, v := range def.Atts {
		if v.Funcion == unicidadesdef.FuncionMonto {
			return v.Nombre
		}
	}

	for _, v := range def.AttsIndexados {
		if v.Funcion == unicidadesdef.FuncionMonto {
			return v.Nombre
		}
	}
	return
}

// AttStr busca el atributo y lo transforma a string.
// Si no se encuentra devuelve un error.
func (u Unicidad) AttStr(key string) (s string, err error) {

	// log.Debug().Str("att", key).Msg("Buscando atributo")
	res, ok := u.AttsIndexados[key]
	if !ok {
		res, ok = u.Atts[key]
		if !ok {
			return s, errors.Errorf("no se encontró el atributo con key " + key)
		}
	}

	return fmt.Sprint(res), nil
}

// AttFecha busca el atributo y lo devuelve como fecha.
// Si no se encuentra o no se puede parsear devuelve un error.
func (u Unicidad) AttFecha(key string) (f fecha.Fecha, err error) {

	res, ok := u.AttsIndexados[key]
	if !ok {
		res, ok = u.Atts[key]
		if !ok {
			return f, errors.Errorf("no se encontró el atributo con key " + key)
		}
	}

	fStr, ok := res.(string)
	if !ok {
		return f, errors.Errorf("el atributo %v no era un string", key)
	}

	// Parseo
	f, err = fecha.NewFecha(fStr)

	return
}

// AttInt busca el atributo y lo devuelve como int.
// Si no se encuentra o no se puede parsear devuelve un error.
func (u Unicidad) AttInt(key string) (val int, err error) {

	res, ok := u.AttsIndexados[key]
	if !ok {
		res, ok = u.Atts[key]
		if !ok {
			return val, errors.Errorf("no se encontró el atributo con key " + key)

		}
	}

	fl, ok := res.(float64)
	if !ok {
		return val, errors.Errorf("el atributo %v no era un float, era %T", key, res)
	}

	return int(fl), nil
}

// AttInt busca el atributo y lo devuelve como Sí o No.
// Si no se encuentra o no se puede parsear devuelve un error.
func (u Unicidad) AttBool(key string) (val bool, err error) {

	res, ok := u.AttsIndexados[key]
	if !ok {
		res, ok = u.Atts[key]
		if !ok {
			return val, errors.Errorf("no se encontró el atributo con key " + key)

		}
	}

	bo, ok := res.(bool)
	if !ok {
		return val, errors.Errorf("el atributo %v no era un float, era %T", key, res)
	}

	return bo, nil
}

// AttD2 busca el atributo y lo devuelve como D2.
// Si no se encuentra o no se puede parsear devuelve un error.
func (u Unicidad) AttD2(key string) (val dec.D2, err error) {

	res, ok := u.AttsIndexados[key]
	if !ok {
		res, ok = u.Atts[key]
		if !ok {
			return val, errors.Errorf("no se encontró el atributo con key " + key)
		}
	}

	fl, ok := res.(float64)
	if !ok {
		return val, errors.Errorf("no se pudo convertir %v a float, era %T", key, res)
	}

	val = dec.NewD2(fl)

	return
}
