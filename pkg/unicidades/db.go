package unicidades

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"bitbucket.org/marcos19/one/pkg/unicidadesdef"
	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

func Create(ctx context.Context, tx pgx.Tx, req *Unicidad) (err error) {
	if req.Comitente == 0 {
		return errors.Errorf("no se ingresó ID de comitente")
	}
	if req.Definicion == 0 {
		return deferror.Validation("No se determinó tipo de unicidad")
	}

	if req.ID == uuid.Nil {
		req.ID, err = uuid.NewV1()
		if err != nil {
			return
		}
	}
	req.CreatedAt = new(time.Time)
	*req.CreatedAt = time.Now()

	query := `INSERT INTO unicidades (id, comitente, definicion, atts_indexados, atts, created_at) VALUES
	($1,$2,$3,$4,$5,$6)`
	_, err = tx.Exec(ctx, query, req.ID, req.Comitente, req.Definicion, req.AttsIndexados, req.Atts, req.CreatedAt)
	if err != nil {
		return deferror.DB(err, "inserting")
	}

	return

}

func readOne(ctx context.Context, conn *pgxpool.Pool, req ReadOneReq) (out Unicidad, err error) {

	query := `SELECT id, comitente, definicion, atts_indexados, atts, created_at, updated_at
	FROM unicidades 
	WHERE comitente=$1 AND id=$2`

	err = conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(&out.ID, &out.Comitente, &out.Definicion,
		&out.AttsIndexados, &out.Atts, &out.CreatedAt, &out.UpdatedAt)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}
	return
}

type ReadManyReq struct {
	Comitente  int
	Definicion int `json:",string"`
	Filtro     map[string]interface{}
	IDs        []uuid.UUID
	Limit      int
}

func readMany(ctx context.Context, conn *pgxpool.Pool, def unicidadesdef.Def, req ReadManyReq) (out []Unicidad, err error) {
	out = []Unicidad{}
	if req.Comitente == 0 {
		return out, errors.New("no se definió ID de comitente")
	}
	if req.Definicion == 0 {
		return out, deferror.Validation("no se ingresó ID de definición de unicidad")
	}

	ww := []string{"comitente=$1", "definicion=$2"}
	pp := []interface{}{req.Comitente, req.Definicion}

	// Si pedí IDs
	if len(req.IDs) > 0 {
		ww = append(ww, fmt.Sprintf("id IN (%v)", arrayUUID(req.IDs)))
	}

	count := 3
	if req.Filtro != nil {
		for k, v := range req.Filtro {
			// determino el type del atributo
			castTo := ""
			for _, att := range def.AttsIndexados {
				if att.Nombre != k {
					continue
				}
				switch att.Type {
				case "int", "d2":
					castTo = "INT::STRING"
				default:
					castTo = "STRING"
				}
			}
			w := fmt.Sprintf(`atts_indexados ->> $%v::STRING = $%v::%v`, count, count+1, castTo)
			ww = append(ww, w)
			pp = append(pp, k, v)
			count += 2
		}
	}

	where := "WHERE " + strings.Join(ww, " AND ")
	limit := ""
	if req.Limit != 0 {
		limit = fmt.Sprintf("LIMIT %v", req.Limit)
	}

	query := fmt.Sprintf(`
		SELECT id, comitente, definicion, atts_indexados, atts, created_at, updated_at 
		FROM unicidades 
		%v
		%v
		;`, where, limit)

	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		u := Unicidad{}
		err = rows.Scan(
			&u.ID, &u.Comitente, &u.Definicion,
			&u.AttsIndexados, &u.Atts, &u.CreatedAt, &u.UpdatedAt)
		if err != nil {
			return out, deferror.DB(err, "escaneando unicidad")
		}
		if u.Atts == nil {
			u.Atts = tipos.JSON{}
		}
		if u.AttsIndexados == nil {
			u.AttsIndexados = tipos.JSON{}
		}
		out = append(out, u)
	}

	return
}

func update(ctx context.Context, conn *pgxpool.Pool, req *Unicidad) (err error) {

	tx, err := conn.Begin(ctx)
	if err != nil {
		return deferror.DB(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)

	err = updateWithTx(ctx, tx, req)
	if err != nil {
		return err
	}

	err = tx.Commit(ctx)
	if err != nil {
		return deferror.DB(err, "confirmando transacción")
	}
	return
}

func updateWithTx(ctx context.Context, tx pgx.Tx, req *Unicidad) (err error) {
	req.UpdatedAt = new(time.Time)
	*req.UpdatedAt = time.Now()

	query := `UPDATE unicidades 
	SET atts_indexados=$3, atts=$4, updated_at=now()
	WHERE comitente=$1 AND id=$2`

	res, err := tx.Exec(ctx, query, req.Comitente, req.ID,
		req.AttsIndexados, req.Atts)
	if err != nil {
		return deferror.DB(err, "updating")
	}

	if res.RowsAffected() == 0 {
		return errors.New("la operación no modificó ningún registro")
	}
	return
}
