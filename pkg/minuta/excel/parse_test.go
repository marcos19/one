package excel

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParse(t *testing.T) {

	file, err := os.Open("./testdata/minuta.xlsx")
	assert.Nil(t, err)

	out, err := ParseXLSX(file)
	if err != nil {
		t.Fatal(err)
	}

	assert.Len(t, out, 2)
	assert.Len(t, out[0].Partidas, 3)

}
