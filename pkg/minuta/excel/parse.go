package excel

import (
	"io"
	"log"
	"strconv"
	"strings"

	"bitbucket.org/marcos19/one/pkg/minuta"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/xuri/excelize/v2"
)

type handler struct {
	cuentas    map[string]int
	personas   map[string]uuid.UUID
	sucursales map[string]int
	centros    map[string]int
	cajas      map[string]int
}

// ParseXLSX lee una un archivo de Excel que contiene minutas.
// Para importar datos.
func ParseXLSX(reader io.Reader) (out []minuta.Minuta, err error) {

	// Leo
	r := &handler{}
	f, err := excelize.OpenReader(reader)
	if err != nil {
		return out, errors.Wrap(err, "leyendo reader")
	}

	// Inicio lookups
	err = r.iniciarLookups(f)
	if err != nil {
		return nil, errors.Wrap(err, "leyendo lookups")
	}

	hoja := "Asientos"
	rows, err := f.GetRows(hoja)
	if err != nil {
		return out, errors.Wrap(err, "leyendo rows")
	}

	var m minuta.Minuta

	for i, row := range rows {
		if len(row) == 0 {
			continue
		}
		switch row[0] {
		case "":
			// Termino minuta
			continue
		case "Fecha":
			// Empieza una nueva
			if len(row) > 1 {
				m = minuta.Minuta{}
				m.Fecha, err = fch(row[1])
			}

		case "Detalle":
			if len(row) > 1 {
				m.Detalle = row[1]
			}

		case "Cuenta":
			// Empieza asiento

			// Map de columnas
			columnas := map[string]int{}
			for j, header := range row {
				columnas[header] = j
			}

			// Parseo partidas
			pp, renglones, err := r.parsearPartidas(columnas, rows[i+1:])
			if err != nil {
				return out, errors.Wrapf(err, "parseando partidas que arrancan en fila %v", i+1)
			}
			i += renglones + 1
			m.Partidas = append(m.Partidas, pp...)
			if len(m.Partidas) > 0 {
				out = append(out, m)
			}

		}
	}
	return
}

// func (r *handler) nuevoAsiento(rows [][]string) (m minuta.Minuta, renglones int, err error) {

// }

func (r *handler) parsearPartidas(cols map[string]int, rows [][]string) (out []minuta.Partida, renglones int, err error) {
	var row []string
	for renglones, row = range rows {

		if len(row) == 0 {
			continue
		}
		switch row[0] {
		case "", "Fecha":
			// Nuevo asiento
			return
		}

		p := minuta.Partida{}

		// Cuenta
		p.Cuenta, err = r.lookupCuenta(row[cols["Cuenta"]])
		if err != nil {
			return out, renglones, errors.Wrapf(err, "buscando cuenta")
		}

		// Debe
		if len(row) >= cols["Debe"]+1 {
			p.Debe, err = d(row[cols["Debe"]])
			if err != nil {
				return out, renglones, errors.Wrap(err, "leyendo debe")
			}
		}

		// Haber
		if len(row) >= cols["Haber"]+1 {
			p.Haber, err = d(row[cols["Haber"]])
			if err != nil {
				return nil, renglones, errors.Wrap(err, "leyendo haber")
			}
		}

		if p.Debe != 0 && p.Haber != 0 {
			return out, renglones, errors.Errorf("hay valor en columna debe y haber")
		}
		if p.Debe == 0 && p.Haber == 0 {
			continue
		}

		if len(row) >= cols["Detalle"]+1 {
			p.Detalle = row[cols["Detalle"]]
		}

		// Centro
		if len(row) >= cols["Centro"]+1 {
			centro, err := r.lookupCentro(row[cols["Centro"]])
			if err != nil {
				return nil, renglones, errors.Wrapf(err, "buscando centro %v", row[cols["Centro"]])
			}
			if centro != 0 {
				p.Centro = new(int)
				*p.Centro = centro
			}
		}
		// Caja
		_, ok := cols["Caja"]
		if ok {
			if len(row) >= cols["Caja"]+1 {
				if row[cols["Caja"]] != "" {
					caja, err := r.lookupCaja(row[cols["Caja"]])
					if err != nil {
						return nil, renglones, errors.Wrapf(err, "buscando caja %v", row[cols["Caja"]])
					}
					if caja != 0 {
						p.Caja = new(int)
						*p.Caja = caja
					}
				}
			}
		}

		// Persona
		if len(row) >= cols["Persona"]+1 {
			persona, err := r.lookupPersona(row[cols["Persona"]])
			if err != nil {
				return nil, renglones, errors.Wrapf(err, "buscando persona %v", row[cols["Persona"]])
			}
			if persona != uuid.Nil {
				p.Persona = new(uuid.UUID)
				*p.Persona = persona
			}
		}

		// Sucursal
		if len(row) >= cols["Sucursal"]+1 {
			if row[cols["Sucursal"]] != "" {
				sucursal, err := r.lookupSucursal(row[cols["Sucursal"]])
				if err != nil {
					return nil, renglones, errors.Wrapf(err, "buscando sucursal %v", row[cols["Sucursal"]])
				}
				if sucursal != 0 {
					p.Sucursal = new(int)
					*p.Sucursal = sucursal
				}
			}
		}

		// Vencimiento
		if len(row) >= cols["Vencimiento"]+1 {
			if row[cols["Vencimiento"]] != "" {

				vto, err := fch(row[cols["Vencimiento"]])
				if err != nil {
					return nil, renglones, errors.Wrapf(err, "parseando vencimiento %v", row[cols["Vencimiento"]])
				}
				if vto != 0 {
					p.Vencimiento = new(fecha.Fecha)
					*p.Vencimiento = vto
				}
			}
		}

		out = append(out, p)
	}

	return
}

func (r *handler) lookupPersona(nombre string) (id uuid.UUID, err error) {

	if nombre == "" {
		return
	}
	if r.personas == nil {
		return id, errors.New("no se inició personas map")
	}

	id, ok := r.personas[nombre]
	if !ok {
		return id, errors.Errorf("no se definió ID de persona (nombre '%v')", nombre)
	}

	return
}
func (r *handler) lookupSucursal(nombre string) (id int, err error) {

	if nombre == "" {
		return
	}
	if r.sucursales == nil {
		return id, errors.New("no se inició sucursales map")
	}

	id, ok := r.sucursales[nombre]
	if !ok {
		return id, errors.Errorf("no se definió ID de sucursal(nombre '%v')", nombre)
	}

	return
}
func (r *handler) lookupCentro(nombre string) (id int, err error) {

	if nombre == "" {
		return
	}
	if r.centros == nil {
		return id, errors.New("no se inició centros map")
	}

	id, ok := r.centros[nombre]
	if !ok {
		return id, errors.Errorf("no se definió ID de centro (nombre '%v')", nombre)
	}

	return
}

func (r *handler) lookupCuenta(nombre string) (id int, err error) {

	if r.cuentas == nil {
		return id, errors.New("no se inició cuentas map")
	}

	id, ok := r.cuentas[nombre]
	if !ok {
		return id, errors.Errorf("no se definió ID de cuentas (nombre '%v')", nombre)
	}

	return
}

func (r *handler) lookupCaja(nombre string) (id int, err error) {

	if r.cajas == nil {
		return id, errors.New("no se inició cajas map")
	}

	id, ok := r.cajas[nombre]
	if !ok {
		return id, errors.Errorf("no se definió ID de caja (nombre '%v')", nombre)
	}

	return
}
func (r *handler) iniciarLookups(xl *excelize.File) (err error) {
	{
		hoja := "Personas"
		rows, err := xl.GetRows(hoja)
		if err != nil {
			return errors.Wrapf(err, "leyendo rows de %v", hoja)
		}
		r.personas = map[string]uuid.UUID{}
		for i, row := range rows {
			if i == 0 {
				continue
			}
			if len(row) == 0 {
				break
			}
			if row[0] == "" {
				break
			}
			if len(row) < 2 {
				return errors.Errorf("se esperaban 2 filas en hoja %v fila %v", hoja, i+1)
			}
			r.personas[row[0]], err = uuid.FromString(strings.Trim(row[1], " "))
			if err != nil {
				return errors.Wrapf(err, "convirtiendo a UUID persona %v", row[0])
			}
		}
	}

	{
		hoja := "Sucursales"
		rows, err := xl.GetRows(hoja)
		if err != nil {
			return errors.Wrapf(err, "leyendo rows de %v", hoja)
		}
		r.sucursales = map[string]int{}
		for i, row := range rows {
			if i == 0 {
				continue
			}
			if len(row) == 0 {
				break
			}
			if row[0] == "" {
				break
			}
			if len(row) < 2 {
				return errors.Errorf("se esperaban 2 filas en hoja %v fila %v", hoja, i+1)
			}
			id, err := strconv.Atoi(row[1])
			if err != nil {
				return errors.Wrap(err, "leyendo sucursal")
			}
			r.sucursales[row[0]] = id
		}
	}
	{
		hoja := "Cuentas"
		rows, err := xl.GetRows(hoja)
		if err != nil {
			return errors.Wrapf(err, "leyendo rows de %v", hoja)
		}
		r.cuentas = map[string]int{}
		for i, row := range rows {
			if i == 0 {
				continue
			}
			if len(row) == 0 {
				break
			}
			if row[0] == "" {
				break
			}
			if len(row) < 2 {
				return errors.Errorf("se esperaban 2 filas en hoja %v fila %v", hoja, i+1)
			}
			log.Println(row[0])
			r.cuentas[row[0]], err = strconv.Atoi(row[1])
			if err != nil {
				return errors.Wrapf(err, "convirtiendo a número cuenta %v", row[0])
			}
		}
	}
	{
		hoja := "Centros"
		rows, err := xl.GetRows(hoja)
		if err != nil {
			return errors.Wrapf(err, "leyendo rows de %v", hoja)
		}
		r.centros = map[string]int{}
		for i, row := range rows {
			if i == 0 {
				continue
			}
			if len(row) == 0 {
				break
			}
			if row[0] == "" {
				break
			}
			if len(row) < 2 {
				return errors.Errorf("se esperaban 2 filas en hoja %v fila %v", hoja, i+1)
			}
			r.centros[row[0]], err = strconv.Atoi(row[1])
			if err != nil {
				return errors.Wrapf(err, "convirtiendo a número centro %v", row[0])
			}
		}
	}

	{
		hoja := "Cajas"
		rows, err := xl.GetRows(hoja)
		if err != nil {
			return errors.Wrapf(err, "leyendo rows de %v", hoja)
		}
		r.cajas = map[string]int{}
		for i, row := range rows {
			if i == 0 {
				continue
			}
			if len(row) == 0 {
				break
			}
			if row[0] == "" {
				break
			}
			if len(row) < 2 {
				return errors.Errorf("se esperaban 2 filas en hoja %v fila %v", hoja, i+1)
			}
			r.cajas[row[0]], err = strconv.Atoi(row[1])
			if err != nil {
				return errors.Wrapf(err, "convirtiendo a número caja %v", row[0])
			}
		}
	}
	return
}
func fch(str string) (f fecha.Fecha, err error) {

	f, err = fecha.NewFechaFromLayout("02/01/06", str)
	if err != nil {
		return f, errors.Wrap(err, "parseando a time")
	}

	return
}

func d(str string) (out dec.D2, err error) {
	if str == "" {
		return
	}
	fl, err := strconv.ParseFloat(str, 64)
	if err != nil {
		return
	}

	return dec.NewD2(fl), err
}
