package configs

import "time"

type Config struct {
	ID                *int `json:",string"`
	Comitente         int  `json:",string"`
	Empresa           int  `json:",string"`
	Nombre            string
	Detalle           string
	Tipo              *int `json:",string"`
	CuentaRefundicion *int `json:",string"`
	CreatedAt         time.Time
	UpdatedAt         *time.Time
}

func (c Config) TableName() string {
	return "minutas_config"
}
