package configs

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/cockroachdb/errors"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler se encarga de recibir los request y darles respuesta.
type Handler struct {
	conn *pgxpool.Pool
}

// NewHandler inicializa un cache y devuelve el puntero a este.
func NewHandler(conn *pgxpool.Pool) (h *Handler) {

	h = &Handler{}
	h.conn = conn

	return
}

type ReadOneReq struct {
	ID        int `json:",string"`
	Comitente int
}

// HandleConfig devuelve el listado de configs de minutas
func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (out Config, err error) {

	query := `SELECT id, comitente, empresa, nombre, detalle, tipo, cuenta_refundicion, created_at, updated_at
	FROM minutas_config
	WHERE comitente=$1 AND id=$2`

	// Busco
	err = h.conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(
		&out.ID, &out.Comitente, &out.Empresa, &out.Nombre, &out.Detalle,
		&out.Tipo, &out.CuentaRefundicion, &out.CreatedAt, &out.UpdatedAt)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}

	return
}

type ReadManyReq struct {
	Empresa   int `json:",string"`
	Comitente int
}

// HandleConfigs devuelve el listado de configs de minutas
func (h *Handler) ReadMany(ctx context.Context, req ReadManyReq) (out []Config, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}

	ww := []string{"comitente=$1"}
	pp := []interface{}{req.Comitente}

	if req.Empresa != 0 {
		pp = append(pp, req.Empresa)
		ww = append(ww, fmt.Sprintf("empresa=$%v", len(pp)))
	}
	where := "WHERE " + strings.Join(ww, " AND ")

	query := fmt.Sprintf(`SELECT id, comitente, empresa, nombre, detalle, tipo, cuenta_refundicion, created_at, updated_at
	FROM minutas_config
	%v ORDER BY nombre`, where)

	rows, err := h.conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying config")
	}
	defer rows.Close()

	for rows.Next() {
		v := Config{}
		err = rows.Scan(
			&v.ID, &v.Comitente, &v.Empresa, &v.Nombre, &v.Detalle,
			&v.Tipo, &v.CuentaRefundicion, &v.CreatedAt, &v.UpdatedAt,
		)
		if err != nil {
			return out, deferror.DB(err, "escaneando config")
		}
		out = append(out, v)
	}

	return
}

// HandleConfigCreate crea una nueva configuración de minuta.
func (h *Handler) Create(ctx context.Context, req Config) (err error) {

	// Valido
	if req.Nombre == "" {
		return deferror.Validation("no se ingresó nombre")
	}
	if req.Empresa == 0 {
		return deferror.Validation("no se ingresó empresa")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó el ID de comitente")
	}
	if req.Tipo != nil {
		if *req.Tipo == ops.Refundicion && req.CuentaRefundicion == nil {
			return deferror.Validation("asiento de refundición exige cuenta para cerrar asiento")
		}
		if *req.Tipo != ops.Refundicion {
			req.CuentaRefundicion = nil
		}
	}

	query := `INSERT INTO minutas_config (comitente, empresa, nombre, detalle, tipo, cuenta_refundicion, created_at)
	VALUES ($1,$2,$3,$4,$5,$6,$7)`

	// Inserto
	_, err = h.conn.Exec(ctx, query, req.Comitente, req.Empresa,
		req.Nombre, req.Detalle, req.Tipo, req.CuentaRefundicion, time.Now())
	if err != nil {
		return deferror.DB(err, "insertando config")
	}
	return

}

// HandleConfigUpdate modifica una minuta existente.
func (h *Handler) Update(ctx context.Context, req Config) (err error) {

	// Valido
	if req.Nombre == "" {
		return deferror.Validation("no se ingresó nombre")
	}
	if req.Empresa == 0 {
		return deferror.Validation("no se ingresó empresa")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó el ID de comitente")
	}
	if req.Tipo != nil {
		if *req.Tipo == ops.Refundicion && req.CuentaRefundicion == nil {
			return deferror.Validation("asiento de refundición exige cuenta para cerrar asiento")
		}
		if *req.Tipo != ops.Refundicion {
			req.CuentaRefundicion = nil
		}
	}

	query := `UPDATE minutas_config SET nombre=$3, detalle=$4, tipo=$5, cuenta_refundicion=$6 WHERE comitente=$1 AND id=$2`

	// Modifico
	res, err := h.conn.Exec(ctx, query, req.Comitente, req.ID, req.Nombre, req.Detalle, req.Tipo, req.CuentaRefundicion)
	if err != nil {
		return deferror.DB(err, "updating")
	}

	if res.RowsAffected() == 0 {
		return errors.New("no se modificó ningún registro")
	}

	return
}

type DeleteReq struct {
	ID        int `json:",string"`
	Comitente int
}

func (h *Handler) Delete(ctx context.Context, req ReadOneReq) (err error) {

	// Valido
	if req.ID == 0 {
		return deferror.Validation("no se ingresó ID")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}

	// Está usada?
	q := 0
	err = h.conn.QueryRow(ctx,
		"SELECT COUNT(id) FROM minutas where comitente=$1 AND config=$2",
		req.Comitente, req.ID).Scan(&q)
	if err != nil {
		return errors.Wrap(err, "buscando si estaba usada la config")
	}
	if q > 0 {
		return errors.Errorf("no se puede borrar config porque la misma fue usada %v veces", q)
	}

	// Borro
	res, err := h.conn.Exec(ctx, "DELETE FROM minutas_config WHERE comitente=$1 AND id=$2",
		req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "borrando config")
	}

	if res.RowsAffected() == 0 {
		return errors.New("no se borró ningún registro")
	}

	return
}
