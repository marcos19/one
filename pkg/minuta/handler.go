// Package minuta es el módulo de minutas contables.
//
// # Características
//
// Se seleccionan CuentasContables y se les asigna un monto en pesos.
//
// De ahí en mas sigue las directivas de la cuenta contable en cuanto
// a los datos adicionales que se deben ingresar (persona, producto,
// fechaVto, cantidad, etc.)
//
// No voy a usar partidas en las minutas, directamente trabajo con partidas
// contables.
//
// # Validaciones
//
// La mayoría de las validaciones van a ser las que vienen dadas
// por la partida contable o por ops.
package minuta

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/niler"
	"github.com/rs/zerolog/log"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/minuta/configs"
	"bitbucket.org/marcos19/one/pkg/minuta/esquemas"
	"bitbucket.org/marcos19/one/pkg/oplog"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/unicidades"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler se encarga de recibir los request y darles respuesta.
type Handler struct {
	conn           *pgxpool.Pool
	opInserter     ops.Inserter
	opDeleter      ops.Deleter
	opUpdater      ops.Updater
	compNumerador  comps.Numerador
	compReader     comps.ReaderOne
	cuentaReader   cuentas.ReaderOne
	configHandler  *configs.Handler
	esquemaHandler *esquemas.Handler
}

// NewHandler inicializa un cache y devuelve el puntero a este.
func NewHandler(
	conn *pgxpool.Pool,
	opsHandler OpsHandler,
	num comps.Numerador,
	comp comps.ReaderOne,
	ctas cuentas.ReaderOne,
	cfg *configs.Handler,
	esq *esquemas.Handler,
) (h *Handler, err error) {

	if conn == nil {
		return h, errors.New("Conn no puede ser nil")
	}
	if niler.IsNil(opsHandler) {
		return h, errors.New("ops.Inserter no puede ser nil")
	}
	if niler.IsNil(num) {
		return h, errors.New("comps.Numerador no puede ser nil")
	}
	if niler.IsNil(comp) {
		return h, errors.New("comps.Readerone no puede ser nil")
	}
	if niler.IsNil(ctas) {
		return h, errors.New("cuentas.ReaderOne no puede ser nil")
	}
	if niler.IsNil(cfg) {
		return h, errors.New("configs.Handler no puede ser nil")
	}
	if niler.IsNil(esq) {
		return h, errors.New("esquemas.Handler no puede ser nil")
	}

	h = &Handler{
		conn:           conn,
		opInserter:     opsHandler,
		opDeleter:      opsHandler,
		opUpdater:      opsHandler,
		compNumerador:  num,
		compReader:     comp,
		cuentaReader:   ctas,
		configHandler:  cfg,
		esquemaHandler: esq,
	}

	return
}

type ReadOneReq struct {
	ID        uuid.UUID
	Comitente int
}

// HandleBuscarPorID devuelve la minuta solicitada por ID.
// Los listados los saco desde el general.
func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (out Minuta, err error) {
	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}
	if req.ID == uuid.Nil {
		return out, deferror.Validation("no se ingresó ID")
	}

	// Busco la cabecera
	query := `SELECT id, comitente, empresa, config, esquema, comp, fecha,
	punto_de_venta, n_comp, detalle, partidas, created_at, updated_at,
	usuario
	FROM minutas
	WHERE comitente=$1 AND id=$2`
	err = h.conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(
		&out.ID, &out.Comitente, &out.Empresa, &out.Config, &out.Esquema, &out.Comp,
		&out.Fecha, &out.PuntoDeVenta, &out.NComp, &out.Detalle, &out.Partidas,
		&out.CreatedAt, &out.UpdatedAt, &out.Usuario)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}

	return
}

type CreateArgs struct {
	OmitirSaldos     bool
	NumeracionManual bool

	// Si es una minuta de refundición, cierre o apertura no pega personas,
	// unicidades, productos ni contratos.
	EsRefundicion bool
}

// HandleCreate crea una nueva minuta.
func (h *Handler) Create(ctx context.Context, req *Minuta, args ...CreateArgs) (id uuid.UUID, err error) {

	// Valido
	if req.Fecha == 0 {
		return id, deferror.Validation("no se ingresó fecha")
	}
	if req.Comitente == 0 {
		return id, deferror.Validation("no se ingresó ID de comitente")
	}
	if req.Config == 0 {
		return id, deferror.Validation("no se ingresó ID de config")
	}
	if req.Esquema == 0 {
		return id, deferror.Validation("no se ingresó ID de esquema")
	}
	if req.Empresa == 0 {
		return id, deferror.Validation("no se ingresó ID de empresa")
	}
	if len(args) > 1 {
		return id, deferror.Validation("se ingresó mas de un CreateArgs")
	}

	// Si viene con ID, le dejo ese
	if req.ID == uuid.Nil {
		req.ID, err = uuid.NewV1()
		if err != nil {
			return id, err
		}
	}
	req.CreatedAt = new(time.Time)
	*req.CreatedAt = time.Now()

	// Creo las Ops
	err = h.CrearOp(ctx, req)
	if err != nil {
		return id, errors.Wrap(err, "creando operación")
	}

	// Inicio transacción
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return id, deferror.DB(err, "inciando transacción")
	}
	defer tx.Rollback(ctx)

	// Persisto minuta, op y unicidades (si hay)
	err = h.create(ctx, tx, req, args...)
	if err != nil {
		return id, errors.Wrap(err, "insertando minuta")
	}

	// Loggeo
	rr := logCreate(req.Ops...)
	err = oplog.Log(ctx, tx, rr...)
	if err != nil {
		return id, errors.Wrap(err, "logging")
	}

	// Confirmo la transacción
	err = tx.Commit(ctx)
	if err != nil {
		return id, deferror.DB(err, "confirmando transacción")
	}

	// Invalido cache
	h.compNumerador.CambióNumero(comps.ReadOneReq{Comitente: req.Comitente, ID: req.Comp})

	return req.ID, nil
}

func (h *Handler) create(ctx context.Context, tx pgx.Tx, req *Minuta, args ...CreateArgs) (err error) {

	// Inserto minuta
	err = PersistirMinuta(ctx, tx, req)
	if err != nil {
		return errors.Wrap(err, "persistiendo minuta")
	}

	// Inserto unicidades (si hay)
	if len(req.Unicidades) > 0 {
		err = persistirUnicidades(ctx, tx, req.Unicidades)
		if err != nil {
			return errors.Wrap(err, "persistiendo unicidades")
		}
	}

	// Creo el flow
	log.Debug().Msg("Llamando NewInsertFlow...")
	var cfg *ops.InsertConfig
	if len(args) == 1 {
		cfg = &ops.InsertConfig{}
		cfg.AsientoEspecial = args[0].EsRefundicion
	}
	err = h.opInserter.Insert(ctx, tx, req.Ops, cfg)
	if err != nil {
		return errors.Wrap(err, "ops")
	}

	return
}

func (h *Handler) Update(ctx context.Context, req Minuta, usuario string) (err error) {

	// Creo las Ops
	err = h.CrearOp(ctx, &req)
	if err != nil {
		return errors.Wrap(err, "creando operación")
	}

	// Inicio transacción
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return errors.Wrap(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)

	// Modifico op y partidas
	err = h.opUpdater.Update(ctx, tx, req.Ops)
	if err != nil {
		return errors.Wrap(err, "realizando validaciones rápidas")
	}

	// Borro la anterior
	delReq := DeleteReq{
		Comitente: req.Comitente,
		ID:        req.ID,
		Usuario:   usuario,
	}
	ooBorradas, err := h.delete(ctx, tx, delReq)
	if err != nil {
		return errors.Wrap(err, "borrando minuta anterior")
	}

	// Creo la nueva
	args := CreateArgs{
		OmitirSaldos:     false,
		NumeracionManual: true,
	}

	log.Debug().Msg("h.create()")
	err = h.create(ctx, tx, &req, args)
	if err != nil {
		return errors.Wrap(err, "insertando nueva minuta")
	}

	// Loggeo
	rr := logUpdate(usuario, ooBorradas...)
	log.Debug().Msg("logging update")
	err = oplog.Log(ctx, tx, rr...)
	if err != nil {
		return errors.Wrap(err, "logging")
	}

	// Confirmo transacción
	log.Debug().Msg("confirmando...")
	err = tx.Commit(ctx)
	if err != nil {
		return errors.Wrap(err, "confirmando transacción")
	}

	return
}

type DeleteReq struct {
	ID        uuid.UUID
	Comitente int
	Usuario   string
}

func (h *Handler) Delete(ctx context.Context, req DeleteReq) (err error) {
	// Valido
	if req.ID == uuid.Nil {
		return deferror.Validation("no se ingresó ID de minuta")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}
	if req.Usuario == "" {
		return deferror.Validation("no se ingresó usuario")
	}

	// Inicio transacción
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return errors.Wrap(err, "inciando transacción")
	}
	defer tx.Rollback(ctx)

	// Borro
	oo, err := h.delete(ctx, tx, req)
	if err != nil {
		return errors.Wrap(err, "borrando")
	}

	// Loggeo
	rr := logDelete(req.Usuario, oo...)
	log.Debug().Msg("logging update")
	err = oplog.Log(ctx, tx, rr...)
	if err != nil {
		return errors.Wrap(err, "logging")
	}

	// Confirmo transacción
	err = tx.Commit(ctx)
	if err != nil {
		return errors.Wrap(err, "confirmando transacción")
	}

	return
}

func (h *Handler) delete(ctx context.Context, tx pgx.Tx, req DeleteReq) (oo []*ops.Op, err error) {
	// Valido
	if req.ID == uuid.Nil {
		return oo, deferror.Validation("no se ingresó ID de minuta")
	}
	if req.Comitente == 0 {
		return oo, deferror.Validation("no se ingresó ID de comitente")
	}
	if req.Usuario == "" {
		return oo, deferror.Validation("no se ingresó usuario")
	}

	// Busco ID de ops
	// acq, err := h.conn.Acquire(ctx)
	// if err != nil {
	// 	return oo, errors.Wrap(err, "acquiring")
	// }

	rows, err := h.conn.Query(ctx, "SELECT id, comitente, comp_id, punto_de_venta, n_comp FROM ops WHERE tran_id = $1 AND comitente = $2", req.ID, req.Comitente)
	if err != nil {
		return oo, deferror.DB(err, "buscando ops id")
	}
	defer rows.Close()
	ids := []uuid.UUID{}
	for rows.Next() {
		op := ops.Op{}
		err = rows.Scan(&op.ID, &op.Comitente, &op.CompID, &op.PuntoDeVenta, &op.NComp)
		if err != nil {
			return oo, errors.Wrap(err, "escaneando ID de op")
		}
		oo = append(oo, &op)
		ids = append(ids, op.ID)
	}

	// Borro ops y partidas
	err = h.opDeleter.Delete(ctx, tx, req.Comitente, ids)
	if err != nil {
		return oo, errors.Wrap(err, "borrando minuta")
	}

	// Borro tabla minutas
	_, err = tx.Exec(ctx, "DELETE FROM minutas WHERE id = $1 AND comitente = $2", req.ID, req.Comitente)
	if err != nil {
		return oo, errors.Wrap(err, "borrando en tabla minutas")
	}
	return
}

func (h *Handler) CrearOp(ctx context.Context, m *Minuta) (err error) {

	op := ops.Op{}
	op.ID, err = uuid.NewV1()
	if err != nil {
		return err
	}
	op.Comitente = m.Comitente
	op.Empresa = m.Empresa
	op.TranID = m.ID
	op.TranDef = m.Config
	op.CompID = m.Comp
	op.Fecha = m.Fecha
	op.FechaOriginal = m.Fecha
	op.Detalle = m.Detalle
	op.NComp = m.NComp
	if m.Usuario != nil {
		op.Usuario = *m.Usuario
	}

	// Traigo config
	cfg, err := h.configHandler.ReadOne(ctx, configs.ReadOneReq{
		Comitente: m.Comitente,
		ID:        m.Config,
	})
	if err != nil {
		return errors.Wrap(err, "trayendo config de minuta")
	}
	var tipoAsiento *ops.TipoAsiento
	if cfg.Tipo != nil {
		tipoAsiento = new(ops.TipoAsiento)
		*tipoAsiento = ops.TipoAsiento(*cfg.Tipo)
	}

	// Traigo esquema
	esq, err := h.esquemaHandler.ReadOne(ctx, esquemas.ReadOneReq{
		Comitente: m.Comitente,
		ID:        m.Esquema,
	})
	if err != nil {
		return errors.Wrap(err, "trayendo esquema de minuta")
	}
	m.Comp = esq.Comp
	op.CompID = esq.Comp

	// Traigo comp
	comp, err := h.compReader.ReadOne(ctx, comps.ReadOneReq{
		Comitente: m.Comitente,
		ID:        m.Comp,
	})
	if err != nil {
		return errors.Wrapf(err, "buscando comprobante %v", m.Comp)
	}
	if comp.TipoNumeracion == comps.Consecutiva {
		m.PuntoDeVenta = *comp.PuntoDeVenta
	}

	op.CompNombre = comp.Nombre
	op.PuntoDeVenta = m.PuntoDeVenta
	op.NCompStr, err = formatComp(m.PuntoDeVenta, m.NComp, "PPPP-NNNNNNNN")
	if err != nil {
		return errors.Wrap(err, "formateando número de comprobante")
	}
	op.Programa = "minuta"
	for _, v := range m.Partidas {
		p := ops.Partida{}
		p.ID, err = uuid.NewV1()
		if err != nil {
			return err
		}
		// Busco cuenta
		cta, err := h.cuentaReader.ReadOne(ctx, cuentas.ReadOneReq{
			ID:        v.Cuenta,
			Comitente: op.Comitente,
		})
		if err != nil {
			return errors.Wrapf(err, "no se pudo encontrar cuenta %v", v.Cuenta)
		}
		p.Cuenta = v.Cuenta
		if cta.UsaAplicaciones {
			p.PartidaAplicada = new(uuid.UUID)
			*p.PartidaAplicada = p.ID
		}

		if v.Caja != nil {
			p.Caja = new(int)
			*p.Caja = *v.Caja
		}
		if v.Centro != nil {
			p.Centro = new(int)
			*p.Centro = *v.Centro
		}
		if v.Persona != nil {
			p.Persona = new(uuid.UUID)
			*p.Persona = *v.Persona
		}
		if v.Sucursal != nil {
			p.Sucursal = new(int)
			*p.Sucursal = *v.Sucursal
		}
		if v.Moneda != nil {
			p.Moneda = new(int)
			*p.Moneda = *v.Moneda
		}
		if v.TC != nil {
			p.TC = new(dec.D4)
			*p.TC = *v.TC
		}
		if v.MontoMonedaOriginal != nil {
			p.MontoMonedaOriginal = new(dec.D4)
			signo := dec.D4(1)
			if v.Haber > v.Debe {
				signo = -1.
			}
			*p.MontoMonedaOriginal = *v.MontoMonedaOriginal * signo
		}
		if v.Detalle != "" {
			p.Detalle = new(string)
			*p.Detalle = v.Detalle
		}
		if cta.UsaAplicaciones {
			p.PartidaAplicada = new(uuid.UUID)
			*p.PartidaAplicada = p.ID
		}
		if v.Unicidad != nil {
			p.Unicidad = new(uuid.UUID)
			*p.Unicidad = *v.Unicidad
		}
		if v.PartidaAplicada != nil {
			p.PartidaAplicada = new(uuid.UUID)
			*p.PartidaAplicada = *v.PartidaAplicada
		}
		if v.Producto != nil {
			p.Producto = new(uuid.UUID)
			*p.Producto = *v.Producto
		}
		if v.SKU != nil {
			p.SKU = new(uuid.UUID)
			*p.SKU = *v.SKU
		}
		if v.UM != "" {
			p.UM = new(string)
			*p.UM = v.UM
		}
		if v.Cantidad != 0 {
			p.Cantidad = new(dec.D4)
			*p.Cantidad = v.Cantidad
		}
		if cta.AperturaProducto {
			p.Cantidad = new(dec.D4)
			*p.Cantidad = v.Cantidad
		}

		p.Monto = v.Debe - v.Haber
		p.TipoAsiento = tipoAsiento
		op.PartidasContables = append(op.PartidasContables, p)
	}

	m.Ops = []*ops.Op{&op}

	return
}

func formatComp(puntoVenta, numero int, mask string) (out string, err error) {

	mask = strings.ToUpper(mask)
	partes := []string{}

	cantidadPV := strings.Count(mask, "P")
	pv, err := llenarCeros(puntoVenta, cantidadPV)
	if err != nil {
		return out, errors.Wrap(err, "llenando con ceros el punto de venta")
	}
	if pv != "" {
		partes = append(partes, pv)
	}

	cantidadN := strings.Count(mask, "N")
	n, err := llenarCeros(numero, cantidadN)
	if err != nil {
		return out, errors.Wrap(err, "llenando con ceros el comprobante")
	}
	if n != "" {
		partes = append(partes, n)
	}

	return strings.Join(partes, "-"), nil
}

func llenarCeros(n, digitos int) (out string, err error) {
	if digitos == 0 {
		return "", nil
	}
	nStr := fmt.Sprint(n)
	if len(nStr) > digitos {
		return out, errors.Errorf("había más dígitos (%v) que espacios (%v)", len(nStr), digitos)
	}
	cantidad := digitos - len(nStr)
	for i := 1; i <= cantidad; i++ {
		out += "0"
	}
	return out + nStr, nil
}

func persistirUnicidades(ctx context.Context, tx pgx.Tx, mm []unicidades.Unicidad) (err error) {
	for i := range mm {
		err = unicidades.Create(ctx, tx, &mm[i])
		if err != nil {
			return errors.Wrapf(err, "peristiendo unicidad %v", i)
		}
	}
	return
}

// Persistir persiste la struct en la tabla 'minutas'.
// No realiza ningún control.
func PersistirMinuta(ctx context.Context, tx pgx.Tx, m *Minuta) (err error) {
	query := `
		INSERT INTO minutas (id, comitente, empresa, fecha, config, esquema, comp, punto_de_venta, n_comp, detalle, partidas, created_at) VALUES 
		($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12);
	`
	_, err = tx.Exec(ctx, query, m.ID, m.Comitente, m.Empresa, m.Fecha, m.Config, m.Esquema, m.Comp, m.PuntoDeVenta, m.NComp, m.Detalle, m.Partidas, m.CreatedAt)
	if err != nil {
		return deferror.DB(err, "insertando minuta")
	}
	return
}
