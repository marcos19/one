package minuta

import "bitbucket.org/marcos19/one/pkg/ops"

type OpsHandler interface {
	ops.Inserter
	ops.Deleter
	ops.Updater
}
