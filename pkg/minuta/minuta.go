package minuta

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"io"
	"time"

	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/unicidades"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/olekukonko/tablewriter"
)

const PkgID = "minuta"

type Minuta struct {
	ID           uuid.UUID
	Comitente    int `json:",string"`
	Empresa      int `json:",string"`
	Config       int `json:",string"`
	Esquema      int `json:",string"`
	Comp         int `json:",string"`
	Fecha        fecha.Fecha
	PuntoDeVenta int
	NComp        int
	Detalle      string
	Partidas     Partidas
	CreatedAt    *time.Time
	UpdatedAt    *time.Time
	Usuario      *string

	Ops        []*ops.Op
	Unicidades []unicidades.Unicidad
}

type Partidas []Partida

type Partida struct {
	Cuenta              int          `json:",string"`
	Centro              *int         `json:",string,omitempty"`
	Caja                *int         `json:",string,omitempty"`
	Moneda              *int         `json:",string,omitempty"`
	TC                  *dec.D4      `json:",omitempty"`
	MontoMonedaOriginal *dec.D4      `json:",omitempty"`
	Deposito            *int         `json:",string,omitempty"`
	Persona             *uuid.UUID   `json:",omitempty"`
	Sucursal            *int         `json:",string,omitempty"`
	Vencimiento         *fecha.Fecha `json:",omitempty"`
	Unicidad            *uuid.UUID   `json:",omitempty"`
	PartidaAplicada     *uuid.UUID   `json:",omitempty"`

	Debe    dec.D2 `json:",omitempty"`
	Haber   dec.D2 `json:",omitempty"`
	Detalle string

	Producto *uuid.UUID `json:",omitempty"`
	SKU      *uuid.UUID `json:",omitempty"`
	UM       string
	Cantidad dec.D4
}

func (m *Minuta) PartidaDoble() bool {
	sum := dec.D2(0)
	for _, v := range m.Partidas {
		sum += v.Debe - v.Haber
	}
	return sum == 0
}

func (m *Minuta) TotalDebe() dec.D2 {
	sum := dec.D2(0)
	for _, v := range m.Partidas {
		sum += v.Debe
	}
	return sum
}

func (m *Minuta) TotalHaber() dec.D2 {
	sum := dec.D2(0)
	for _, v := range m.Partidas {
		sum += v.Haber
	}
	return sum
}

// Value cumple con la interface SQL
func (p Partidas) Value() (driver.Value, error) {
	return json.Marshal(p)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (p *Partidas) Scan(src interface{}) error {
	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New("Type assertion error .([]byte)")
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, p)
	if err != nil {
		return errors.Wrap(err, "Unmarshalling")
	}

	return nil
}

func (m Minuta) AsciiTable(w io.Writer) {

	{
		t := tablewriter.NewWriter(w)
		t.Append([]string{
			"Empresa", fmt.Sprint(m.Empresa),
		})
		t.Append([]string{
			"Config", fmt.Sprint(m.Config),
		})
		t.Append([]string{
			"Esquema", fmt.Sprint(m.Esquema),
		})
		t.Append([]string{
			"Fecha", fmt.Sprint(m.Fecha),
		})
		t.Append([]string{
			"Detalle", fmt.Sprint(m.Detalle),
		})
		t.Append([]string{
			"Número", fmt.Sprintf("%v-%v", m.PuntoDeVenta, m.NComp),
		})
		t.Render()
	}

	table := tablewriter.NewWriter(w)
	// Determino columnas
	van := map[string]struct{}{}
	cuentas := map[int]struct{}{}
	for _, v := range m.Partidas {
		if v.Persona != nil {
			van["persona"] = struct{}{}
		}
		if v.Sucursal != nil {
			van["sucursal"] = struct{}{}
		}
		if v.Centro != nil {
			van["centro"] = struct{}{}
		}
		if v.Unicidad != nil {
			van["unicidad"] = struct{}{}
		}
		if v.PartidaAplicada != nil {
			van["partida_aplicada"] = struct{}{}
		}
		if v.Producto != nil {
			van["producto"] = struct{}{}
		}
		if v.SKU != nil {
			van["sku"] = struct{}{}
		}
		if v.Detalle != "" {
			van["detalle"] = struct{}{}
		}
		if v.Vencimiento != nil {
			van["fecha_vto"] = struct{}{}
		}
		if v.Cantidad != 0 {
			van["cantidad"] = struct{}{}
		}
		cuentas[v.Cuenta] = struct{}{}
	}
	colores := map[int]int{}
	count := 0
	for k := range cuentas {
		count++
		switch count {
		case 1:
			colores[k] = tablewriter.FgGreenColor
		case 2:
			colores[k] = tablewriter.FgBlueColor
		case 3:
			colores[k] = tablewriter.FgHiRedColor
		case 4:
			colores[k] = tablewriter.FgMagentaColor
			// case 5:
			// 	colores[k] = tablewriter.BgBlueColor
			// case 6:
			// 	colores[k] = tablewriter.BgBlueColor
			// case 7:
			// 	colores[k] = tablewriter.BgBlueColor
			// case 8:
			// 	colores[k] = tablewriter.BgBlueColor
			// case 9:
			// 	colores[k] = tablewriter.BgBlueColor
			// case 10:
			// 	colores[k] = tablewriter.BgBlueColor
		}
	}
	headers := []string{
		"cuenta",
	}
	var ok bool

	_, ok = van["persona"]
	if ok {
		headers = append(headers, "persona")
	}

	_, ok = van["sucursal"]
	if ok {
		headers = append(headers, "sucursal")
	}

	_, ok = van["centro"]
	if ok {
		headers = append(headers, "centro")
	}

	_, ok = van["unicidad"]
	if ok {
		headers = append(headers, "unicidad")
	}

	_, ok = van["partida_aplicada"]
	if ok {
		headers = append(headers, "partida_aplicada")
	}
	_, ok = van["producto"]
	if ok {
		headers = append(headers, "producto")
	}

	_, ok = van["sku"]
	if ok {
		headers = append(headers, "sku")
	}

	_, ok = van["deposito"]
	if ok {
		headers = append(headers, "deposito")
	}

	_, ok = van["cantidad"]
	if ok {
		headers = append(headers, "cantidad")
	}

	headers = append(headers, "debe", "haber")

	_, ok = van["fecha_vto"]
	if ok {
		headers = append(headers, "fecha_vto")
	}

	_, ok = van["detalle"]
	if ok {
		headers = append(headers, "detalle")
	}
	table.SetHeader(headers)
	table.SetBorder(false)

	for _, v := range m.Partidas {
		row := []string{
			fmt.Sprint(v.Cuenta),
		}

		var ok bool

		_, ok = van["persona"]
		if ok {
			row = append(row, fmt.Sprint(v.Persona))
		}

		_, ok = van["sucursal"]
		if ok {
			row = append(row, fmt.Sprint(v.Sucursal))
		}

		_, ok = van["centro"]
		if ok {
			if v.Centro == nil {
				row = append(row, "")
			} else {
				row = append(row, fmt.Sprintf("%d", v.Centro))
			}
		}

		_, ok = van["unicidad"]
		if ok {
			row = append(row, fmt.Sprint(v.Unicidad))
		}

		_, ok = van["partida_aplicada"]
		if ok {
			row = append(row, fmt.Sprint(v.PartidaAplicada))
		}

		_, ok = van["producto"]
		if ok {
			row = append(row, fmt.Sprint(v.Producto))
		}

		_, ok = van["sku"]
		if ok {
			row = append(row, fmt.Sprint(v.SKU))
		}

		_, ok = van["deposito"]
		if ok {
			row = append(row, fmt.Sprint(v.Deposito))
		}

		_, ok = van["cantidad"]
		if ok {
			row = append(row, fmt.Sprintf(v.Cantidad.StringDos()))
		}

		row = append(row,
			fmt.Sprint(v.Debe),
			fmt.Sprint(v.Haber),
		)

		_, ok = van["detalle"]
		if ok {
			row = append(row, fmt.Sprintf(v.Detalle))
		}
		_, ok = van["fecha_vto"]
		if ok {
			row = append(row, fmt.Sprint(v.Vencimiento))
		}

		color := colores[v.Cuenta]
		cc := []tablewriter.Colors{}

		for range headers {
			cc = append(cc, tablewriter.Colors{color})
		}
		table.Rich(row, cc)
	}
	total := []string{}
	count = 0
	for range van {
		if count >= 1 {
			total = append(total, "")
		}
		count++
	}
	total = append(total, "Total", m.TotalDebe().String(), m.TotalHaber().String())
	for i := 0; len(total) < len(headers); i++ {
		total = append(total, "")
	}
	table.SetFooter(total)
	table.Render()
}
