package esquemas

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler se encarga de recibir los request y darles respuesta.
type Handler struct {
	conn *pgxpool.Pool
}

// NewHandler inicializa un cache y devuelve el puntero a este.
func NewHandler(conn *pgxpool.Pool) (h *Handler) {

	h = &Handler{}
	h.conn = conn

	return
}

type ReadOneReq struct {
	ID        int `json:",string"`
	Comitente int
}

// HandleEsquema devuelve el listado de configs de minutas
func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (v Esquema, err error) {

	query := `SELECT id, comitente, empresa, nombre, detalle, config, comp, created_at, updated_at 
		FROM minutas_esquemas 
		WHERE comitente=$1 AND id=$2`

	err = h.conn.
		QueryRow(ctx, query, req.Comitente, req.ID).
		Scan(&v.ID, &v.Comitente, &v.Empresa, &v.Nombre, &v.Detalle, &v.Config, &v.Comp, &v.CreatedAt, &v.UpdatedAt)

	if err != nil {
		return v, deferror.DB(err, "buscando en base de datos")
	}

	return
}

type ReadManyReq struct {
	Comitente int
	Empresa   int `json:",string"`
}

// HandleEsquemas devuelve el listado de configs de minutas
func (h *Handler) ReadMany(ctx context.Context, req ReadManyReq) (out []Esquema, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}

	ww := []string{"comitente=$1"}
	pp := []interface{}{req.Comitente}
	if req.Empresa != 0 {
		pp = append(pp, req.Empresa)
		ww = append(ww, fmt.Sprintf("empresa=$%v", len(pp)))
	}

	where := "WHERE " + strings.Join(ww, " AND ")
	query := fmt.Sprintf(
		`SELECT id, comitente, empresa, nombre, detalle, config, comp, created_at, updated_at 
		FROM minutas_esquemas 
		%v 
		ORDER BY nombre`,
		where)

	// Busco
	rows, err := h.conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()
	for rows.Next() {
		v := Esquema{}
		err = rows.Scan(&v.ID, &v.Comitente, &v.Empresa, &v.Nombre, &v.Detalle, &v.Config, &v.Comp, &v.CreatedAt, &v.UpdatedAt)
		if err != nil {
			return out, deferror.DB(err, "escaneando")
		}
		out = append(out, v)
	}

	if out == nil {
		out = []Esquema{}
	}
	return
}

// HandleEsquemaCreate crea una nueva configuración de minuta.
func (h *Handler) Create(ctx context.Context, req Esquema) (err error) {

	// Validación
	if req.Nombre == "" {
		return deferror.Validation("no se ingresó nombre")
	}
	if req.Empresa == 0 {
		return deferror.Validation("no se ingresó empresa")
	}
	if req.Comp == 0 {
		return deferror.Validation("no se determinó comprobante a utilizar")
	}

	query := `INSERT INTO minutas_esquemas (comitente, empresa, nombre, detalle, config, comp, created_at)
		VALUES ($1,$2,$3,$4,$5,$6,$7)`

	_, err = h.conn.Exec(ctx, query, req.Comitente, req.Empresa, req.Nombre, req.Detalle, req.Config, req.Comp, time.Now())
	if err != nil {
		return deferror.DB(err, "creando registro en base de datos")
	}

	return
}

func (h *Handler) Update(ctx context.Context, req Esquema) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.ComitenteIndefinido()
	}
	if req.ID == nil {
		return errors.New("no se ingresó ID")
	}
	if *req.ID == 0 {
		return errors.New("no se ingresó ID")
	}
	if req.Nombre == "" {
		return deferror.Validation("debe ingresar un nombre")
	}
	if req.Empresa == 0 {
		return deferror.Validation("no se ingresó ID de empresa")
	}
	if req.Comp == 0 {
		return deferror.Validation("no se ingresó comprobante a utilizar")
	}

	query := `UPDATE minutas_esquemas SET nombre=$3, detalle=$4, 
	config=$5, comp=$6, updated_at=now()
	WHERE comitente=$1 AND id=$2`

	_, err = h.conn.Exec(ctx, query, req.Comitente, req.ID,
		req.Nombre, req.Detalle, req.Config, req.Comp,
	)
	if err != nil {
		return deferror.DB(err, "exec")
	}
	return
}

func (h *Handler) Delete(ctx context.Context, req ReadOneReq) (err error) {

	{ // Está usado?
		count := 0
		err = h.conn.QueryRow(ctx, "SELECT COUNT(id) FROM minutas WHERE comitente=$1 AND config=$2",
			req.Comitente, req.ID).Scan(&count)
		if err != nil {
			return deferror.DB(err, "determinando si la config fue usada")
		}
		if count > 0 {
			return deferror.Validationf("no se puede borrar porque esta configuración ya fue usada %v veces", count)
		}
	}

	res, err := h.conn.Exec(ctx,
		"DELETE FROM minutas_esquemas WHERE comitente=$1 AND id=$2", req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "deleting")
	}
	if res.RowsAffected() == 0 {
		return errors.New("no se borró ningún registro")
	}
	return
}
