package esquemas

import "time"

type Esquema struct {
	ID        *int `json:",string"`
	Comitente int  `json:",string"`
	Empresa   int  `json:",string"`
	Nombre    string
	Detalle   string
	Config    int `json:",string"`
	Comp      int `json:",string"`
	CreatedAt time.Time
	UpdatedAt *time.Time
}

func (e Esquema) TableName() string {
	return "minutas_esquemas"
}
