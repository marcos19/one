package minuta

import (
	"bitbucket.org/marcos19/one/pkg/oplog"
	"bitbucket.org/marcos19/one/pkg/ops"
)

// Cuando se inserta una rendición de caja
func logCreate(oo ...*ops.Op) (rr []oplog.Registro) {
	for _, op := range oo {
		r := oplog.Registro{
			Comitente:    op.Comitente,
			CompID:       op.CompID,
			PuntoDeVenta: op.PuntoDeVenta,
			NComp:        op.NComp,
			OpID:         op.ID,
			Usuario:      op.Usuario,
			Operacion:    oplog.OpCreate,
		}
		rr = append(rr, r)
	}
	return
}

// Cuando se borra una rendición de caja
func logDelete(usuario string, oo ...*ops.Op) (rr []oplog.Registro) {
	for _, op := range oo {
		r := oplog.Registro{
			Comitente:    op.Comitente,
			CompID:       op.CompID,
			PuntoDeVenta: op.PuntoDeVenta,
			NComp:        op.NComp,
			OpID:         op.ID,
			Usuario:      usuario,
			Operacion:    oplog.OpDelete,
		}
		rr = append(rr, r)
	}
	return
}

// Cuando se borra una rendición de caja
func logUpdate(usuario string, oo ...*ops.Op) (rr []oplog.Registro) {
	for _, op := range oo {
		r := oplog.Registro{
			Comitente:    op.Comitente,
			CompID:       op.CompID,
			PuntoDeVenta: op.PuntoDeVenta,
			NComp:        op.NComp,
			OpID:         op.ID,
			Usuario:      usuario,
			Operacion:    oplog.OpUpdate,
		}
		rr = append(rr, r)
	}
	return
}
