package tipos

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"reflect"

	"github.com/cockroachdb/errors"
)

type JSON map[string]interface{}

// Value cumple con la interface SQL
func (j JSON) Value() (driver.Value, error) {
	return json.Marshal(j)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (j *JSON) Scan(src interface{}) error {
	if src == nil {
		*j = JSON{}
		return nil
	}

	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New(fmt.Sprint(
			"Type assertion error .([]byte). Era: ",
			reflect.TypeOf(src),
		))
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, j)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("Unmarshalling los siguientes bytes: '%s'", source))
	}

	return nil
}
