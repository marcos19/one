package tipos

import (
	"database/sql/driver"
	"encoding/json"
	"reflect"

	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
)

type GrupoUUID []uuid.UUID

// Value cumple con la interface SQL
func (g GrupoUUID) Value() (driver.Value, error) {

	return json.Marshal(g)

}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (g *GrupoUUID) Scan(src interface{}) error {
	if src == nil {
		*g = GrupoUUID{}
		return nil
	}

	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.Errorf("Type assertion error .([]byte). Era: %v",
			reflect.TypeOf(src),
		)
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, g)
	if err != nil {
		return errors.Wrapf(err, "Unmarshalling los siguientes bytes: '%s'", source)
	}
	return nil
}
