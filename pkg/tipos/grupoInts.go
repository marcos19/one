package tipos

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"

	"github.com/cockroachdb/errors"
	"github.com/jackc/pgtype"
)

// GrupoInts es el tipo que uso para guardar slices de ints en las structs.
// En la base de datos se guardan como []int.
// Cuando se codifican como JSON, se envían como slices de strings, para
// evitar el redondeo que hace javascripts los int64. Obviamente cuando
// lee los JSON los vuelve a convertir a números.
type GrupoInts []int

// ParaClausulaIn devuelve un string que permite hacer la query así:
// "id IN ?" => "id IN (34,543,23,13)"
func (g GrupoInts) ParaClausulaIn() string {
	str := []string{}
	for _, v := range g {
		str = append(str, fmt.Sprint(v))
	}
	return "(" + strings.Join(str, ",") + ")"
}

func (g GrupoInts) Contains(val int) bool {
	for _, v := range g {
		if v == val {
			return true
		}
	}
	return false
}

func (dst *GrupoInts) DecodeBinary(ci *pgtype.ConnInfo, src []byte) error {
	deInts := pgtype.Int8Array{}
	err := deInts.DecodeBinary(ci, src)
	if err != nil {
		return errors.Wrap(err, "decoding GrupoInts")
	}

	*dst = GrupoInts{}
	for _, v := range deInts.Elements {
		*dst = append(*dst, int(v.Int))
	}
	return nil
}

func (src GrupoInts) EncodeBinary(ci *pgtype.ConnInfo, buf []byte) ([]byte, error) {

	d := pgtype.Int8Array{}

	d.Dimensions = []pgtype.ArrayDimension{
		{
			Length:     int32(len(src)),
			LowerBound: -2147483648,
		},
	}
	d.Status = pgtype.Present
	for _, v := range src {
		d.Elements = append(d.Elements, pgtype.Int8{Int: int64(v), Status: pgtype.Present})
	}
	return d.EncodeBinary(ci, buf)
}

// Value cumple con la interface SQL
func (g GrupoInts) Value() (driver.Value, error) {

	str := []string{}
	for _, v := range g {
		str = append(str, strconv.Itoa(v))
	}

	out := []byte("{" + strings.Join(str, ",") + "}")

	return out, nil
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (g *GrupoInts) Scan(src interface{}) error {
	// p := pq.Int64Array(g)

	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New("Type assertion error .([]byte)")
	}
	if source == nil {
		return nil
	}

	str := strings.Replace(string(source), " ", "", -1)
	if str == "{}" {
		return nil
	}

	*g = GrupoInts{}
	vals := strings.Split(str[1:len(str)-1], ",")
	for _, v := range vals {
		if v == "" {
			continue
		}
		integer, err := strconv.Atoi(v)
		if err != nil {
			return errors.Wrap(err, "no se pudo transformar a número el valor "+v)
		}
		*g = append(*g, integer)
	}
	// // Chequeo que se condiga con la struct
	// err := json.Unmarshal(source, g)
	// if err != nil {
	// 	return errors.E(err, "Unmarshalling")
	// }

	return nil
}

func (g *GrupoInts) UnmarshalJSON(b []byte) error {
	ss := []string{}
	if err := json.Unmarshal(b, &ss); err != nil {
		// Si no puedo como strings, pruebo si está viniendo como []int
		ii := []int{}
		err = json.Unmarshal(b, &ii)
		if err != nil {
			return errors.Wrap(err, "no se pudo leer como []int ni como []string")
		}
		*g = ii
		return nil
	}

	arr := []int{}
	*g = arr
	for _, v := range ss {
		enInt, err := strconv.Atoi(v)
		if err != nil {
			return errors.Wrapf(err, "convirtiendo a int %v", v)
		}
		*g = append(*g, int(enInt))
	}

	return nil
}

func (g GrupoInts) MarshalJSON() ([]byte, error) {

	out := []string{}
	for _, v := range g {
		out = append(out, fmt.Sprint(v))
	}

	return json.Marshal(out)
}
