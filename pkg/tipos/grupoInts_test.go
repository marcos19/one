package tipos

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValue(t *testing.T) {

	{ // Con valores
		g := GrupoInts([]int{1, 2, 3})
		v, err := g.Value()
		assert.Nil(t, err)
		assert.Equal(t, []byte("{1,2,3}"), v, string(v.([]byte)))
	}

	{ // Vacio
		g := GrupoInts([]int{})
		v, err := g.Value()
		assert.Nil(t, err)
		assert.Equal(t, []byte("{}"), v, string(v.([]byte)))
	}
}
