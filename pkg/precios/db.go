package precios

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

func createPrecio(ctx context.Context, tx pgx.Tx, p Precio) (err error) {

	query := `INSERT INTO precios (
		comitente, sku, empresa, lista, precio_neto, precio_final, precio_fecha, 
		valido_desde, valido_hasta, created_at, updated_at
	) VALUES (
		$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11
	);`

	_, err = tx.Exec(ctx, query, p.Comitente, p.SKU, p.Empresa, p.Lista, p.PrecioNeto, p.PrecioFinal,
		p.PrecioFecha, p.ValidoDesde, p.ValidoHasta, p.CreatedAt, p.UpdatedAt)
	if err != nil {
		return deferror.DB(err, "inserting")
	}
	return
}

type ReadManyReq struct {
	IDs       []uuid.UUID // ID de SKU. Es opcional
	Empresa   int         `json:",string"`
	Listas    tipos.GrupoInts
	Comitente int
}

func readMany(ctx context.Context, conn *pgxpool.Pool, req ReadManyReq) (out []Precio, err error) {
	out = []Precio{}

	if req.Comitente == 0 {
		return out, errors.Errorf("no se ingresó ID de comitente")
	}

	ww := []string{}
	pp := []interface{}{}

	// Comitente
	pp = append(pp, req.Comitente)
	ww = append(ww, fmt.Sprintf("comitente=$%v", len(pp)))

	// Empresa
	if req.Empresa != 0 {
		pp = append(pp, req.Empresa)
		ww = append(ww, fmt.Sprintf("empresa=$%v", len(pp)))
	}

	// Lista
	if len(req.Listas) > 0 {
		ww = append(ww, fmt.Sprintf("lista IN (%v)", arrayInt(req.Listas)))
	}

	// IDs
	if len(req.IDs) > 0 {
		ww = append(ww, fmt.Sprintf("sku IN (%v)", arrayUUID(req.IDs)))
	}

	where := "WHERE " + strings.Join(ww, " AND ")

	query := fmt.Sprintf(`SELECT comitente, sku, empresa, lista, precio_neto, precio_final, precio_fecha, valido_desde, valido_hasta, created_at, updated_at
	FROM precios %v`, where)

	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()
	for rows.Next() {
		p := Precio{}
		err = rows.Scan(&p.Comitente, &p.SKU, &p.Empresa, &p.Lista, &p.PrecioNeto, &p.PrecioFinal, &p.PrecioFecha, &p.ValidoDesde, &p.ValidoHasta, &p.CreatedAt, &p.UpdatedAt)
		if err != nil {
			return out, deferror.DB(err, "scanning")
		}
		out = append(out, p)
	}
	return
}

func arrayUUID(ids []uuid.UUID) string {
	idsStr := []string{}
	for _, v := range ids {
		idsStr = append(idsStr, fmt.Sprintf("'%v'", v.String()))
	}
	return strings.Join(idsStr, ", ")
}
func arrayInt(ids tipos.GrupoInts) string {
	idsStr := []string{}
	for _, v := range ids {
		idsStr = append(idsStr, fmt.Sprintf("%v", v))
	}
	return strings.Join(idsStr, ", ")
}
