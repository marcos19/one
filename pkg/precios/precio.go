package precios

import (
	"time"

	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
)

// Precio es el valor determinado para un producto.
// Se fija cuando se quiere
type Precio struct {
	Comitente int
	SKU       uuid.UUID // Siempre la pongo para buscar
	Empresa   int       `json:",string"` // Siempre la pongo para buscar
	Lista     int       `json:",string"` //

	PrecioNeto  dec.D4
	PrecioFinal dec.D4
	PrecioFecha fecha.Fecha

	ValidoDesde *fecha.Fecha
	ValidoHasta *fecha.Fecha
	CreatedAt   *time.Time
	UpdatedAt   *time.Time
}

// Costo se actualiza con las recepciones (o se ingresa manualmente)
type Costo struct {
	Comitente int       `json:",string"` // Key
	Empresa   int       `json:",string"` // key
	SKU       uuid.UUID // Key

	Costo              dec.D4
	Moneda             dec.D4
	TipoCosto          string
	FechaFijacionCosto fecha.Fecha
}

// CostoHistorico guarda los datos históricos cada vez que se actualiza el
// costo de un producto
type CostoHistorico struct {
	Comitente  int         `json:",string"` // Key
	Empresa    int         `json:",string"` // Key
	SKU        uuid.UUID   // Key
	FechaDesde fecha.Fecha // Key
	FechaHasta fecha.Fecha

	Costo     dec.D4
	Moneda    string
	TipoCosto string
}
