package precios

import (
	"context"
	"errors"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/productos"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler es el encargado de recibir los http requests y emitir los responses.
type Handler struct {
	conn *pgxpool.Pool
}

type HandlerConfig struct {
	Conn *pgxpool.Pool
}

// NewHandler inicializa un cache y devuelve el puntero a este.
func NewHandler(conn *pgxpool.Pool) (h *Handler, err error) {

	if conn == nil {
		return h, errors.New("conn no puede ser nil")
	}
	h = &Handler{
		conn: conn,
	}

	return
}

type UpdatePorSkuReq struct {
	SKU     uuid.UUID
	Empresa int `json:",string"`
	Precios []Precio

	Comitente int
}

func (h *Handler) UpdatePorSku(ctx context.Context, req UpdatePorSkuReq) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}
	if req.SKU == uuid.Nil {
		return deferror.Validation("no se ingresó ID de SKU")
	}
	if req.Empresa == 0 {
		return deferror.Validation("no se ingresó ID de empresa")
	}

	// Completo las structs
	for i := range req.Precios {
		if req.Precios[i].PrecioNeto < 0 {
			return deferror.Validation("Todos los precios tienen que ser positivos")
		}
		req.Precios[i].Comitente = req.Comitente
		req.Precios[i].Empresa = req.Empresa
		req.Precios[i].SKU = req.SKU
		req.Precios[i].PrecioFecha = fecha.NewFechaFromTime(time.Now())
	}

	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return deferror.DB(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)

	{ // Borro todos los que había antes
		query := `DELETE FROM precios WHERE comitente=$1 AND sku=$2 AND empresa=$3`
		_, err = tx.Exec(ctx, query, req.Comitente, req.SKU, req.Empresa)
		if err != nil {
			return deferror.DB(err, "borrando precios anteriores")
		}
	}

	// Inserto nuevos
	for _, v := range req.Precios {
		// Si tiene precio cero, significa que no tiene precio para esta
		// lista, así que no lo inserto
		if v.PrecioNeto == 0 || v.PrecioFinal == 0 {
			continue
		}
		v.Comitente = req.Comitente
		err = createPrecio(ctx, tx, v)
		if err != nil {
			return deferror.DB(err, "persistiendo nuevos precios")
		}
	}

	// Confirmo transacción
	err = tx.Commit(ctx)
	if err != nil {
		return deferror.DB(err, "confirmando transacción")
	}

	return
}

func (h *Handler) UpdatePorSkuYLista(ctx context.Context, req Precio) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}
	if req.SKU == uuid.Nil {
		return deferror.Validation("no se ingresó ID de SKU")
	}
	if req.Empresa == 0 {
		return deferror.Validation("no se ingresó ID de empresa")
	}
	if req.Lista == 0 {
		return deferror.Validation("no se ingresó ID de lista")
	}

	// Completo las structs
	if req.PrecioNeto < 0 {
		return deferror.Validation("Todos los precios tienen que ser positivos")
	}
	req.PrecioFecha = fecha.NewFechaFromTime(time.Now())

	tx, err := h.conn.Begin(ctx)
	defer tx.Rollback(ctx)

	{ // Borro viejo
		query := `DELETE FROM precios WHERE comitente=$1 AND sku=$2 AND empresa=$3 AND lista=$4`
		_, err = tx.Exec(ctx, query, req.Comitente, req.SKU, req.Empresa, req.Lista)
		if err != nil {
			return deferror.DB(err, "borrando precio anterior")
		}
	}
	{ // Inserto nuevos
		err = createPrecio(ctx, tx, req)
		if err != nil {
			return deferror.DB(err, "persistiendo nuevo precio")
		}
	}

	// Confirmo transacción
	err = tx.Commit(ctx)
	if err != nil {
		return deferror.DB(err, "confirmando transacción")
	}

	return
}

type ProductoSkusYPreciosReq struct {
	Producto  uuid.UUID
	Empresa   int `json:",string"`
	Listas    tipos.GrupoInts
	Comitente int
}

type ProductoSkusYPreciosResponse struct {
	Producto productos.Producto
	SKUs     []productos.SKU
	Precios  []Precio
}

// ProductoSkusYPrecios devuelve todos los datos al seleccionar
func (h *Handler) ProductoSkusYPrecios(ctx context.Context, req ProductoSkusYPreciosReq) (out ProductoSkusYPreciosResponse, err error) {

	// Valido
	if req.Producto == uuid.Nil {
		return out, deferror.Validation("no se ingresó ID de producto")
	}
	if req.Empresa == 0 {
		return out, deferror.Validation("no se ingresó ID de empresa")
	}

	// Busco Producto
	out.Producto, err = productos.ReadOneProducto(ctx, h.conn, productos.ReadOneReq{
		Comitente: req.Comitente,
		ID:        req.Producto,
	})
	if err != nil {
		return out, deferror.DB(err, "buscando producto")
	}

	// Busco SKUs
	out.SKUs, err = productos.ReadManySKU(ctx, h.conn, productos.ReadManySKUReq{Comitente: req.Comitente, ID: req.Producto})
	if err != nil {
		return out, deferror.DB(err, "buscando SKUs")
	}

	if len(req.Listas) > 0 {
		// Busco precios
		skuIDs := []uuid.UUID{}
		for _, v := range out.SKUs {
			skuIDs = append(skuIDs, v.ID)
		}
		out.Precios, err = readMany(ctx, h.conn, ReadManyReq{
			Comitente: req.Comitente,
			IDs:       skuIDs,
			Listas:    req.Listas,
			Empresa:   req.Empresa,
		})
		if err != nil {
			return out, deferror.DB(err, "buscando precios")
		}
	}

	return
}

type PreciosPorSKUReq struct {
	ID        uuid.UUID
	Empresa   int `json:",string"`
	Comitente int
}

type PreciosPorSKUResp struct {
	Producto productos.Producto
	SKU      productos.SKU
	Precios  []Precio
}

func (h *Handler) PreciosPorSKU(ctx context.Context, req PreciosPorSKUReq) (out PreciosPorSKUResp, err error) {

	// Valido
	if req.ID == uuid.Nil {
		return out, deferror.Validation("no se ingresó ID de producto")
	}
	if req.Empresa == 0 {
		return out, deferror.Validation("no se ingresó ID de empresa")
	}
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}

	// Busco SKU
	out.SKU, err = productos.ReadOneSKU(ctx, h.conn, productos.ReadOneReq{Comitente: req.Comitente, ID: req.ID})
	// h.db.First(&out.SKU, "comitente = ? AND id = ?", req.Comitente, req.ID).Error
	if err != nil {
		return out, deferror.DB(err, "buscando SKU")
	}

	// Busco Producto
	out.Producto, err = productos.ReadOneProducto(ctx, h.conn, productos.ReadOneReq{
		Comitente: req.Comitente,
		ID:        out.SKU.Producto,
	})
	// err = h.db.First(&out.Producto, "comitente = ? AND id = ?",
	// 	req.Comitente, out.SKU.Producto).Error
	if err != nil {
		return out, deferror.DB(err, "buscando producto")
	}

	// Busco precios
	out.Precios, err = readMany(ctx, h.conn, ReadManyReq{
		Comitente: req.Comitente,
		IDs:       []uuid.UUID{req.ID},
		Empresa:   req.Empresa,
	})
	// err = h.db.
	// 	Where("comitente = ? AND sku = ? AND empresa = ?", req.Comitente, req.ID, req.Empresa).
	// 	Find(&out.Precios).Error
	if err != nil {
		return out, deferror.DB(err, "buscando precios")
	}

	return
}

func (h *Handler) ReadMany(ctx context.Context, req ReadManyReq) (out []Precio, err error) {

	out, err = readMany(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando precios")
	}

	return
}
