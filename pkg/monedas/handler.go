package monedas

import (
	"context"
	"fmt"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/alash3al/go-pubsub"
	"github.com/cockroachdb/errors"
	"github.com/dgraph-io/ristretto"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Handler struct {
	conn  *pgxpool.Pool
	cache *ristretto.Cache
	bus   *pubsub.Broker
}

// NewHandler inicializa un handler.
func NewHandler(conn *pgxpool.Pool, cache *ristretto.Cache) (h *Handler, err error) {

	if conn == nil {
		return h, errors.New("Conn no puede ser nil")
	}
	h = &Handler{
		conn:  conn,
		cache: cache,
		bus:   pubsub.NewBroker(),
	}
	return
}
func (h *Handler) Create(ctx context.Context, req Moneda, usuario string) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}
	if req.Nombre == "" {
		return deferror.Validation("no se ingresó el nombre")
	}

	// Inserto
	err = create(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "persistiendo")
	}

	h.bus.Broadcast(EvMonedaCreada{usuario, req}, "")
	return
}

// Devuelve un channel en el que se publican los eventos
func (h *Handler) EventChan() (out <-chan *pubsub.Message, err error) {
	// Creo suscritptor
	sub, err := h.bus.Attach()
	if err != nil {
		return nil, errors.Wrap(err, "creando suscriptor")
	}

	// Lo suscribo
	h.bus.Subscribe(sub, "")

	out = sub.GetMessages()
	return
}

func (h *Handler) ReadMany(ctx context.Context, comitente int) (out []Moneda, err error) {

	// Valido
	if comitente == 0 {
		return out, deferror.Validation("no se ingresó comitente")
	}

	// Busco
	out, err = readMany(ctx, h.conn, comitente)
	if err != nil {
		return out, deferror.DB(err, "buscando monedas")
	}

	return
}

func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (out Moneda, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó comitente")
	}
	if req.ID == 0 {
		return out, deferror.Validation("no se ingresó ID")
	}

	// Busco en cache
	val, estaba := h.cache.Get(hashInt(req))
	if estaba {
		out, ok := val.(Moneda)
		if ok && out.Comitente == req.Comitente {
			return out, nil
		}
	}

	// Busco
	out, err = readOne(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando en base de datos")
	}

	// Agrego a cache
	h.cache.Set(hashInt(req), out, 0)

	return
}

func (h *Handler) Update(ctx context.Context, req Moneda, usuario string) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó comitente")
	}
	if req.ID == nil {
		return deferror.Validation("no se ingresó ID")
	}
	if *req.ID == 0 {
		return deferror.Validation("no se ingresó ID")
	}

	// Invalido cache
	h.cache.Del(hashInt(ReadOneReq{Comitente: req.Comitente, ID: *req.ID}))

	// Grabo
	err = update(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "modificando en base de datos")
	}

	h.bus.Broadcast(EvMonedaModificada{usuario, req}, "")

	return
}

func (h *Handler) Delete(ctx context.Context, req ReadOneReq, usuario string) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó comitente")
	}
	if req.ID == 0 {
		return deferror.Validation("no se ingresó ID")
	}

	// Busco anterior
	anterior, err := h.ReadOne(ctx, req)
	if err != nil {
		return errors.Wrap(err, "buscando moneda")
	}

	// Invalido cache
	h.cache.Del(hashInt(ReadOneReq{Comitente: req.Comitente, ID: req.ID}))

	// Borro
	err = delete(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "borrando moneda")
	}

	h.bus.Broadcast(EvMonedaEliminada{usuario, anterior}, "")

	return
}

func hashInt(req ReadOneReq) string {
	return fmt.Sprintf("%v/%v/%v", "Moneda", req.Comitente, req.ID)
}
