package monedas

import "context"

type ReaderOne interface {
	ReadOne(context.Context, ReadOneReq) (Moneda, error)
}
