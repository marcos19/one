package monedas

type Moneda struct {
	Comitente    int  `json:",string"`
	ID           *int `json:",string"`
	Codigo       string
	Nombre       string
	NombrePlural string
	Simbolo      string
}

/*
CREATE TABLE monedas (
	id
		SERIAL PRIMARY KEY,
	comitente
		INT NOT NULL REFERENCES comitentes,
	codigo
		STRING,
	nombre
		STRING NOT NULL,
	nombre_plural
		STRING NOT NULL,
	simbolo
		STRING NOT NULL
);
*/
