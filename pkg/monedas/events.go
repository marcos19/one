package monedas

type EvMonedaCreada struct {
	Usuario string
	Moneda  Moneda
}

type EvMonedaModificada struct {
	Usuario string
	Moneda  Moneda
}

type EvMonedaEliminada struct {
	Usuario string
	Moneda  Moneda
}
