package monedas

import (
	"context"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/jackc/pgx/v4/pgxpool"
)

func create(ctx context.Context, conn *pgxpool.Pool, req Moneda) (err error) {
	query := `INSERT INTO monedas (comitente, codigo, nombre, nombre_plural, simbolo) VALUES
	($1,$2,$3,$4,$5)`

	_, err = conn.Exec(ctx, query, req.Comitente, req.Codigo, req.Nombre, req.NombrePlural, req.Simbolo)
	if err != nil {
		return deferror.DB(err, "inserting")
	}
	return
}

func readMany(ctx context.Context, conn *pgxpool.Pool, comitente int) (out []Moneda, err error) {
	out = []Moneda{}

	query := `SELECT id, comitente, codigo, nombre, nombre_plural, simbolo FROM monedas WHERE comitente=$1`

	rows, err := conn.Query(ctx, query, comitente)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		v := Moneda{}
		err = rows.Scan(&v.ID, &v.Comitente, &v.Codigo, &v.Nombre, &v.NombrePlural, &v.Simbolo)
		if err != nil {
			return out, deferror.DB(err, "escaneando")
		}
		out = append(out, v)
	}
	return

}

type ReadOneReq struct {
	Comitente int
	ID        int `json:",string"`
}

func readOne(ctx context.Context, conn *pgxpool.Pool, req ReadOneReq) (out Moneda, err error) {

	query := `SELECT id, comitente, codigo, nombre, nombre_plural, simbolo 
	FROM monedas 
	WHERE comitente=$1 AND id=$2`

	err = conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(
		&out.ID, &out.Comitente, &out.Codigo, &out.Nombre, &out.NombrePlural, &out.Simbolo)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}
	return

}

func update(ctx context.Context, conn *pgxpool.Pool, req Moneda) (err error) {

	query := `UPDATE monedas SET codigo=$3, nombre=$4, nombre_plural=$5, simbolo=$6
	WHERE comitente=$1 AND id=$2`

	res, err := conn.Exec(ctx, query, req.Comitente, req.ID, req.Codigo, req.Nombre, req.NombrePlural, req.Simbolo)
	if err != nil {
		return deferror.DB(err, "querying/scanning")
	}

	if res.RowsAffected() == 0 {
		return errors.New("no se modificó ningún registro")
	}
	return

}

func delete(ctx context.Context, conn *pgxpool.Pool, req ReadOneReq) (err error) {

	{ // Está usada?
		count := 0
		err = conn.QueryRow(ctx, "SELECT COUNT(moneda) FROM partidas WHERE comitente=$1 AND moneda=$2",
			req.Comitente, req.ID).Scan(&count)
		if err != nil {
			return deferror.DB(err, "determinando si la moneda estaba usada en la contabilidad")
		}
		if count > 0 {
			return deferror.Validationf("No se puede borrar la moneda porque la misma ya ha sido usada en la contabilidad %v veces.", count)
		}
	}

	query := `DELETE FROM monedas WHERE comitente=$1 AND id=$2`

	res, err := conn.Exec(ctx, query, req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "querying/scanning")
	}

	if res.RowsAffected() == 0 {
		return errors.New("no se borró ningún registro")
	}
	return

}
