package sesiones

import (
	"fmt"
	"time"

	"github.com/cockroachdb/errors"
	jwt "github.com/dgrijalva/jwt-go/v4"
)

// // MailSender implementa el envío de un mail.
// type MailSender interface {
// 	Send(to, from, subject, body string) error
// 	// SenderAlias es el nombre que aparece en FROM
// 	SenderAlias() string
// }

type Handler struct {
	secretKey      []byte
	DuracionSesion time.Duration
}

// New instancia un nuevo handler de sesiones.
func NewHandler(key []byte) (h *Handler, err error) {

	// Valido
	if len(key) == 0 {
		return h, errors.Errorf("no se ingresó key para encriptar JWT")
	}

	h = &Handler{secretKey: key}

	// Datos por defecto SESION
	h.DuracionSesion = time.Hour * 15

	return
}

// NewToken genera un nuevo token para el usuario ingresado

func (h *Handler) NewToken(userID string) (out string, err error) {
	// Create the token
	token := jwt.New(jwt.SigningMethodHS256)

	// Create a map to store our claims
	claims := token.Claims.(jwt.MapClaims)

	// Fijo la expiración del token
	claims["exp"] = time.Now().Add(h.DuracionSesion).Unix()
	claims["userID"] = userID

	// Encripto
	out, err = token.SignedString(h.secretKey)
	if err != nil {
		return out, errors.Wrap(err, "encriptando token")
	}

	return
}

type Fields struct {
	UserID    string
	Comitente int
	Exp       int64
}

// Le actualiza la hora que se usó por última vez el token
func (h *Handler) TouchTime(token string) (newToken string, f Fields, err error) {

	// Parseo
	t, err := h.parseToken(token)
	if err != nil {
		// Suele ser porque venció. Razón por la que fuere, devuelvo que no hay token
		return newToken, f, errors.Wrap(ErrNotAuthenticated{}, "parseando token touch time")
	}

	f.Exp = time.Now().Add(h.DuracionSesion).Unix()

	claims := t.Claims.(jwt.MapClaims)
	claims["exp"] = f.Exp
	var ok bool
	f.UserID, ok = claims["userID"].(string)
	if !ok {
		return newToken, f, errors.Errorf("missing en token userID")
	}

	{ // Comitente
		com, ok := claims["comitente"]
		if ok {
			comFl, ok := com.(float64)
			if !ok {
				return newToken, f, errors.Errorf("comitente tenía tipo incorrecto (%T), debería haber sido %T", com, 0.)
			}
			f.Comitente = int(comFl)
		}
	}

	// Encripto
	newToken, err = t.SignedString(h.secretKey)
	if err != nil {
		return newToken, f, errors.Wrap(err, "encriptando token")
	}

	return
}

// Devuelve un tokenn en el que la tiene la fecha vencida
func (h *Handler) InvalidateToken(tokenStr string) (out string, err error) {

	// Si no lo puede parsear es porque ya está inválido, no hago nada
	t, err := h.parseToken(tokenStr)
	if err != nil {
		return out, errors.Wrap(err, "parseando token invalidate")
	}

	claims := t.Claims.(jwt.MapClaims)
	claims["exp"] = time.Now().Add(-h.DuracionSesion).Unix()

	// Encrito
	out, err = t.SignedString(h.secretKey)
	if err != nil {
		return out, errors.Wrap(err, "encriptando token")
	}

	return
}

// Le agrega al token el dato del comitente
func (h *Handler) SetComitente(tokenStr string, comitente int) (out string, err error) {

	// Si no lo puede parsear es porque ya está inválido, no hago nada
	t, err := h.parseToken(tokenStr)
	if err != nil {
		return out, errors.Wrap(err, "parseando token set comitente")
	}

	claims := t.Claims.(jwt.MapClaims)

	// Create a map to store our claims
	claims["comitente"] = comitente

	// Encripto
	out, err = t.SignedString(h.secretKey)
	if err != nil {
		return out, errors.Wrap(err, "encriptando token")
	}

	return
}

// parseToken transforma el string en un jwt.Token
func (h *Handler) parseToken(tokenString string) (token *jwt.Token, err error) {

	token, err = jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return h.secretKey, nil
	})
	if err != nil {
		return token, errors.Wrap(err, "parseando JWT")
	}

	// Corroboro
	_, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		return token, errors.New("token inválido")
	}
	return
}

// // UsuarioID devuelve el campo Nombre para el usuario de la sesión. Esta función la van
// // a usar los otros packages.
// func (h *Handler) UsuarioID(r *http.Request) (id string, err error) {

// 	tokenString, err := extraerToken(r)
// 	if err != nil {
// 		return id, errors.Wrap(err, "extrayendo token del request")
// 	}

// 	token, err := h.parseToken(tokenString)
// 	if err != nil {
// 		return id, errors.Wrap(err, "parseando token")
// 	}

// 	// Infiero tipo
// 	claims := token.Claims.(jwt.MapClaims)
// 	id = claims["userID"].(string)

// 	return id, err

// }

// type NuevoUsuarioReq struct {
// 	Alias     string
// 	Nombre    string
// 	Apellido  string
// 	Mail      *string
// 	Pass      string
// 	Admin     bool
// 	Comitente int
// }

// type NuevoUsuarioYComitenteReq struct {
// 	Alias    string
// 	Nombre   string
// 	Apellido string
// 	Mail     string
// 	Pass     string

// 	OmitirConfirmacion bool
// }

// // NuevoUsuarioYComitente da de alta un usuario, envía el mail y lo deja en estado
// // "Pendiente de Confirmación".
// func (h *Handler) NuevoUsuarioYComitente(ctx context.Context, req NuevoUsuarioYComitenteReq) (err error) {

// 	// Valido
// 	if req.Mail == "" {
// 		return deferror.Validation("Debe ingresar un e-mail")
// 	}
// 	if req.Nombre == "" {
// 		return deferror.Validation("Debe ingresar un nombre")
// 	}
// 	id := req.Alias
// 	if req.Alias == "" {
// 		id = req.Mail
// 	}

// 	// Que el id ingresado no exista.
// 	_, existe, err := h.existeUsuario(ctx, id)
// 	if err != nil {
// 		return errors.Wrap(err, "corroborando existencia del usuario")
// 	}
// 	if existe {
// 		return deferror.Validationf("ya exsite un usuario con ID '%v'", id)
// 	}

// 	// Creo el struct Usuario
// 	u := Usuario{}
// 	u.ID = id
// 	u.Nombre = req.Nombre
// 	u.Apellido = req.Apellido

// 	// Le pego el hash de la password.
// 	u.Hash = calcularHash(req.Pass)
// 	u.UltimaActualizacionContraseña = time.Now()
// 	u.BlanquearProximoIngreso = false
// 	if req.OmitirConfirmacion {
// 		u.Estado = EstadoConfirmado
// 	} else {
// 		u.Estado = EstadoPendienteConfirmación
// 	}

// 	tx, err := h.conn.Begin(ctx)
// 	if err != nil {
// 		return deferror.DB(err, "iniciando transacción")
// 	}
// 	defer tx.Rollback(ctx)

// 	// Creo el comitente
// 	com := comitentes.Comitente{}
// 	com.Nombre = fmt.Sprintf("%v, %v", u.Apellido, u.Nombre)

// 	// Persisto el comitente
// 	err = comitentes.CreateComitente(ctx, tx, &com)
// 	if err != nil {
// 		return deferror.DB(err, "creando comitente")
// 	}

// 	// Persisto el usuario
// 	u.Comitentes = tipos.GrupoInts{*com.ID}
// 	err = createUserTx(ctx, tx, u)
// 	if err != nil {
// 		return deferror.DB(err, "persistiendo usuario en base de datos")
// 	}

// 	mail := MailConfirmacion{}
// 	if !req.OmitirConfirmacion {
// 		// Creo el registro con el codigo de confirmación.
// 		conf := UsuarioConfirmacion{}
// 		conf.ID, _ = uuid.NewV4()
// 		conf.UserID = u.ID
// 		conf.Motivo = MotivoCreacion

// 		err = createUserConfirmationTx(ctx, tx, conf)
// 		if err != nil {
// 			return deferror.DB(err, "creando confirmación de usuario")
// 		}

// 		mail = MailConfirmacion{
// 			To:               conf.UserID,
// 			From:             h.MailFrom,
// 			NombreUsuario:    conf.UserID,
// 			LinkConfirmacion: fmt.Sprintf("%v?id=%v", h.LinkConfirmarBlanqueo, conf.ID),
// 		}
// 	}

// 	// Confirmo
// 	err = tx.Commit(ctx)
// 	if err != nil {
// 		return deferror.DB(err, "confirmando transacción")
// 	}

// 	if !req.OmitirConfirmacion {
// 		// Creo mail para confirmar usuario
// 		msg, err := mail.Generar()
// 		if err != nil {
// 			return errors.Wrap(err, "generando mail de confirmación de casilla")
// 		}

// 		// Envio mail
// 		err = h.dialer.DialAndSend(msg)
// 		if err != nil {
// 			return errors.Wrap(err, "enviando mail de confirmación de usuario")
// 		}
// 	}

// 	log.Info().Msg("Nuevo usuario y comitentes creados")

// 	return
// }

// type ReenviarMailConfirmacion struct {
// 	UserID string
// }

// // ReenviarMailConfirmacion manda nuevamente el mail que está pendiente de
// // confirmación de nuevo usuario
// func (h *Handler) ReenviarMailConfirmacion(ctx context.Context, req ReenviarMailConfirmacion) (err error) {

// 	// Busco el nombre de este usuario
// 	u, err := readUser(ctx, h.conn, req.UserID)
// 	if err != nil {
// 		return deferror.DB(err, "buscando usuario")
// 	}

// 	// Creo el registro con el codigo de confirmación.
// 	conf := UsuarioConfirmacion{}
// 	conf.ID, _ = uuid.NewV4()
// 	conf.UserID = req.UserID
// 	conf.Motivo = MotivoCreacion

// 	err = createUserConfirmation(ctx, h.conn, conf)
// 	if err != nil {
// 		return errors.Wrap(err, "creando confirmación de usuario")
// 	}

// 	// Creo mail para confirmar usuario
// 	mail := MailConfirmacion{
// 		To:               conf.UserID,
// 		From:             h.MailFrom,
// 		NombreUsuario:    u.Nombre,
// 		LinkConfirmacion: fmt.Sprintf("%v?id=%v", h.LinkConfirmarBlanqueo, conf.ID),
// 	}

// 	// Genero mail
// 	msg, err := mail.Generar()
// 	if err != nil {
// 		return errors.Wrap(err, "generando mail de confirmación de casilla")
// 	}

// 	// Envio mail
// 	err = h.dialer.DialAndSend(msg)
// 	if err != nil {
// 		return errors.Wrap(err, "enviando mail de confirmación de usuario")
// 	}

// 	log.Info().Msg("mail confirmación renviado")
// 	return
// }

// type ConfirmarUsarioReq struct {
// 	ID uuid.UUID
// }

// // ConfirmarUsuario tilda el usuario como "Confirmado". Tiene que hacerlo con
// // el link que le llega al mail.
// func (h *Handler) ConfirmarUsuario(ctx context.Context, req ConfirmarUsarioReq) (err error) {

// 	// Busco que esté disponible esa confirmación
// 	c, err := readUserConfirmation(ctx, h.conn, req.ID, MotivoCreacion)
// 	if err != nil {
// 		return errors.Wrap(err, "no se pudo obtener el registro de confirmación")
// 	}
// 	if c.Confirmada {
// 		return errors.Wrap(err, "la cuenta ya estaba confirmada")
// 	}

// 	err = confirmarUsuario(ctx, h.conn, c)
// 	if err != nil {
// 		return errors.Wrap(err, "confirmando usuario")
// 	}

// 	log.Info().Msg("usuario confirmado")

// 	return
// }

// type SolicitarBlanqueoReq struct {
// 	UserID string
// }

// // SolicitarBlanqueo le envía un mail al usuario con un link desde el que
// // puede entrar y poner sun nueva clave.
// func (h *Handler) SolicitarBlanqueo(ctx context.Context, req SolicitarBlanqueoReq) (err error) {

// 	if h.LinkConfirmarBlanqueo == "" {
// 		return errors.New("no se definió URL para confirmar blanqueo")
// 	}

// 	// Que el id ingresado  exista.
// 	usuario, existe, err := h.existeUsuario(ctx, req.UserID)
// 	if err != nil {
// 		return errors.Wrap(err, "corroborando existencia del usuario")
// 	}
// 	if !existe {
// 		return errors.Wrapf(err, "no existe ningún usuario con el mail %v", req.UserID)
// 	}

// 	// Creo el registro con el codigo de confirmación.
// 	conf := UsuarioConfirmacion{}
// 	conf.ID, _ = uuid.NewV4()
// 	conf.UserID = req.UserID
// 	conf.Motivo = MotivoBlanqueo
// 	err = createUserConfirmation(ctx, h.conn, conf)
// 	if err != nil {
// 		return deferror.DB(err, "creando confirmación de usuario")
// 	}

// 	// Creo mail con link de blanqueo
// 	mail := MailBlanqueo{
// 		To:               conf.UserID,
// 		From:             h.MailFrom,
// 		NombreUsuario:    usuario.Nombre,
// 		LinkConfirmacion: fmt.Sprintf("%v?id=%v", h.LinkConfirmarBlanqueo, conf.ID),
// 	}

// 	msg, err := mail.Generar()
// 	if err != nil {
// 		return errors.Wrap(err, "generando mail de confirmación de casilla")
// 	}

// 	// Envío el mail
// 	err = h.dialer.DialAndSend(msg)
// 	if err != nil {
// 		return errors.Wrap(err, "enviando mail de blanqueo de contraseña")
// 	}

// 	log.Info().Msg("enviado mail blanqueo")

// 	return
// }

// type ConfirmarBlanqueoReq struct {
// 	CodigoConfirmacion uuid.UUID
// 	Pass               string
// }

// // ConfirmarBlanqueo se llama desde la página /blanquear
// func (h *Handler) ConfirmarBlanqueo(ctx context.Context, req ConfirmarBlanqueoReq) (err error) {

// 	// Busco que esté disponible esa confirmación
// 	c, err := readUserConfirmation(ctx, h.conn, req.CodigoConfirmacion, MotivoBlanqueo)
// 	if err != nil {
// 		return deferror.DB(err, "no se pudo obtener el registro de confirmación")
// 	}

// 	if c.Confirmada {
// 		return errors.New("el blanqueo ya se había realizado")
// 	}

// 	// Estamos ok, procedemos con el blanqueo

// 	// Grabo UsuarioConfirmación
// 	c.Confirmada = true
// 	c.FechaConfirmacion = time.Now()
// 	err = confirmarUsuario(ctx, h.conn, c)
// 	if err != nil {
// 		return deferror.DB(err, "confirmando usuario")
// 	}

// 	// Cambio el hash de la constraseña
// 	err = h.cambiarPassword(ctx, c.UserID, req.Pass, false)
// 	if err != nil {
// 		return errors.Wrap(err, "cambiando contraseña")
// 	}

// 	log.Info().Msg("blanqueo confirmado")

// 	return
// }

// type CambiarContraseñaReq struct {
// 	User           string
// 	Actual         string
// 	Pass           string
// 	Pass2          string
// 	BlanquearLuego bool
// }

// type UsuarioDTO struct {
// 	ID            string
// 	Email         *string
// 	Nombre        string
// 	Apellido      string
// 	DNI           int
// 	Estado        string
// 	Administrador bool
// 	Inactivo      bool
// 	PieMail       *string

// 	UltimaActualizacionContraseña time.Time
// 	CreatedAt                     time.Time
// }

// func (u UsuarioDTO) TableName() string {
// 	return "usuarios"
// }

// func (h *Handler) CreateUsuario(ctx context.Context, req UsuarioDTO, comitente int) (err error) {

// 	// Valido
// 	if comitente == 0 {
// 		return deferror.Validation("no se ingresó ID de comitente")
// 	}
// 	if req.ID == "" {
// 		return deferror.Validation("no se ingresó ID de usuario")
// 	}
// 	if req.Nombre == "" {
// 		return deferror.Validation("no se ingresó nombre")
// 	}
// 	if req.Apellido == "" {
// 		return deferror.Validation("no se ingresó apellido")
// 	}

// 	// Existe?
// 	_, existe, err := h.existeUsuario(ctx, req.ID)
// 	if err != nil {
// 		return errors.Wrap(err, "buscando usuario a modificar")
// 	}
// 	if existe {
// 		return deferror.Validationf("ya existe un usuario con ID %v", req.ID)
// 	}

// 	nuevoReq := NuevoUsuarioReq{
// 		Alias:     req.ID,
// 		Nombre:    req.Nombre,
// 		Apellido:  req.Apellido,
// 		Mail:      req.Email,
// 		Admin:     false,
// 		Comitente: comitente,
// 		Pass:      req.ID,
// 	}

// 	// Creo usuario
// 	err = h.NuevoUsuario(ctx, nuevoReq)
// 	if err != nil {
// 		log.Error().Err(err).Msg("creando usuario")
// 		return err
// 	}

// 	log.Info().Int("comitente", comitente).Str("user", req.ID).Msg("nuevo usuario")
// 	return
// }

// type UsuarioReq struct {
// 	ID        string
// 	Comitente int
// }
