package sesiones

type ErrMuchosComitentes struct{}

func (e ErrMuchosComitentes) Error() string {
	return "muchos comitentes para elegir"
}

func (e ErrMuchosComitentes) Frontend() string {
	return "muchos comitentes"
}

type ErrNotAuthenticated struct{}

func (e ErrNotAuthenticated) Error() string {
	return "no autenticado"
}

func (e ErrNotAuthenticated) Frontend() string {
	return "no autenticado"
}

// Muchos comitentes
//case errors.Is(err, MuchosComitenteError{}):
//resp.Tipo = "Muchos comitentes"
//w.WriteHeader(http.StatusBadRequest)
//json.NewEncoder(w).Encode(&resp)
//// NoAutenticado
//case errors.Is(err, usuarios.NoAutenticadoError{}):
//resp.Tipo = "No autenticado"
//w.WriteHeader(http.StatusUnauthorized)
//json.NewEncoder(w).Encode(&resp)

//// AutenticacionIncorrecta
//case errors.Is(err, AutenticacionIncorrectaError{}):
//resp.Tipo = "Autenticación incorrecta"
//w.WriteHeader(http.StatusInternalServerError)
//json.NewEncoder(w).Encode(&resp)

//// NoAutorizado
//case errors.Is(err, NoAutorizadoError{}):
//resp.Tipo = "No autorizado"
//resp.Msg = err.Error()
//w.WriteHeader(http.StatusForbidden)
//json.NewEncoder(w).Encode(&resp)

//// ContraseñaVencida
//case errors.Is(err, ContraseñaVencidaError{}):
//resp.Tipo = "Contraseña vencida"
//resp.Msg = err.Error()
//w.WriteHeader(http.StatusInternalServerError)
//json.NewEncoder(w).Encode(&resp)
