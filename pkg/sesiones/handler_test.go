package sesiones

import (
	"testing"
	"time"

	jwt "github.com/dgrijalva/jwt-go/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestHandler(t *testing.T) {

	key := []byte("test")
	userID := "marcos"
	h, err := NewHandler(key)
	require.Nil(t, err)

	// Creo token
	tokenStr, err := h.NewToken(userID)
	require.Nil(t, err)
	assert.True(t, len(tokenStr) > 10)

	prevTime := float64(0)
	{ // Tiene pegado usuario?
		token, err := h.parseToken(tokenStr)
		require.Nil(t, err)
		claims := token.Claims.(jwt.MapClaims)
		assert.Equal(t, userID, claims["userID"])
		prevTime = claims["exp"].(float64)
		tokenStr, err = token.SignedString(key)
		require.Nil(t, err)
	}

	{ // Touch
		time.Sleep(time.Second)
		nuevo, f, err := h.TouchTime(tokenStr)
		require.Nil(t, err)
		token, err := h.parseToken(nuevo)
		require.Nil(t, err)
		assert.Equal(t, userID, f.UserID)
		assert.Equal(t, 0, f.Comitente)
		assert.True(t, f.Exp > 0)

		// Parse
		tokenStr, err = token.SignedString(key)
		require.Nil(t, err)
	}

	{ // Set comitente
		tokenStr, err = h.SetComitente(tokenStr, 1)
		require.Nil(t, err)

		token, err := h.parseToken(tokenStr)
		require.Nil(t, err)
		claims := token.Claims.(jwt.MapClaims)
		assert.Equal(t, "marcos", claims["userID"])
		assert.Equal(t, 1., claims["comitente"])
		nuevoTime := claims["exp"].(float64)
		assert.NotEqual(t, prevTime, nuevoTime,
			"prev: %v, now: %v", time.Unix(int64(prevTime), 0), time.Unix(int64(nuevoTime), 0),
		)
	}

	{ // Invalidate
		nuevo, err := h.InvalidateToken(tokenStr)
		require.Nil(t, err)
		_, err = h.parseToken(nuevo)
		require.NotNil(t, err)
	}
}
