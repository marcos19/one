// Package factinter engloba procesos de varios packages.
// - fact: grabar una operación
// - afip: autorizar una factura
// - email: luego de autorizada enviar por mail
//
// En un paradigma lineal, llamar a la función y esperar el rersultado
// es lo más fácil y seguro.
//
// Tengo un proceso en el cual interactuan dos partes.
// Debo desacoplar lo más posible. Un sistema de eventos sería lo más razonable.
package factinter

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/marcos19/one/pkg/afip/wsfev1"
	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/fact/esquemas"
	facthandler "bitbucket.org/marcos19/one/pkg/fact/handler"
	"bitbucket.org/marcos19/one/pkg/fact/types"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/socket"
	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
	"github.com/rs/zerolog/log"
)

type Handler struct {
	fact            *facthandler.Handler
	afip            *wsfev1.Handler
	esquemasHandler *esquemas.Handler
	socketHandler   EventBus
	compHandler     comps.ReaderOne
}

func NewHandler(args Args) (h *Handler) {
	h = &Handler{}
	h.fact = args.Fact
	h.afip = args.Afip
	h.esquemasHandler = args.Esquemas
	h.socketHandler = args.Socket
	h.compHandler = args.Comp

	return h
}

type Args struct {
	Fact     *facthandler.Handler
	Afip     *wsfev1.Handler
	Esquemas *esquemas.Handler
	Socket   EventBus
	Comp     comps.ReaderOne
}

type EventBus interface {
	NotificaciónUsuario(string, socket.NotificacionFormato, string)
}

func (h *Handler) NuevaFact(ctx context.Context, f *types.Fact, args ...facthandler.CreateArgs) (err error) {

	// Proceso y grabo
	err = h.fact.Create(ctx, f, args...)
	if err != nil {
		return
	}

	// Busco esquema
	esq, err := h.esquemasHandler.ReadOne(ctx, esquemas.ReadOneReq{
		Comitente: f.Comitente,
		ID:        f.Esquema,
	})
	if err != nil {
		return errors.Wrap(err, "buscando esquema")
	}

	// Busco comp
	esFacturaElectronica := false
	if f.Factura != nil {

		comp, err := h.compHandler.ReadOne(ctx, comps.ReadOneReq{
			Comitente: f.Comitente,
			ID:        f.Factura.CompID,
		})
		if err != nil {
			log.Err(err).Msg("buscando comp")
			return err
		}
		esFacturaElectronica = comp.TipoEmision == comps.TipoEmisionWSFEV1
	}

	pedirCAEAutomaticamente := esq.CAE == esquemas.CAEAutomatico
	seAutorizo := false

	// Hay factura?
	if f.Factura != nil {
		log.Trace().Msg("Había factura")

		if esFacturaElectronica && pedirCAEAutomaticamente {

			log.Debug().Msg("Era electrónica")
			// Era electrónico => autorizo
			resp, err := h.afip.SolicitarCAE(ctx, wsfev1.SolicitarCAEReq{
				Comitente: f.Comitente,
				OpID:      f.Factura.ID,
			})
			if err != nil {
				return errors.Wrap(err, "la factura se grabó, pero falló la solicitud de CAE")
			}

			if !resp.AutorizadoOK() {
				ee := []string{}
				for _, v := range resp.Body.FECAESolicitarResponse.FECAESolicitarResult.Errors {
					ee = append(ee, fmt.Sprintf("%v - %v", v.Code, v.Msg))
				}
				return errors.Errorf("Se grabó la factura pero no pudo ser autorizada. Motivos:\n%v",
					strings.Join(ee, "\n"))
			}
			seAutorizo = true
		}
	}

	correspondeEnviarFactura := ((esFacturaElectronica && seAutorizo) || !esFacturaElectronica) && esq.EnviarFacturasPorMail

	comps := []*ops.Op{}

	// Corresponde enviar factura por mail?
	if f.Factura != nil && correspondeEnviarFactura {
		log.Trace().Msg("Corresponde enviar factura")
		comps = append(comps, f.Factura)
	} else {
		log.Trace().Msg("No corresponde enviar factura")
	}

	// Corresponde enviar recibo por mail?
	if esq.EnviarRecibosPorMail && f.Recibo != nil {
		log.Trace().Msg("Corresponde enviar recibo")
		comps = append(comps, f.Recibo)
	} else {
		log.Trace().Msg("No corresponde enviar recibo")
	}

	ids := []uuid.UUID{}
	for _, v := range comps {
		ids = append(ids, v.ID)
	}
	if len(comps) > 0 {

		go func() {
			// Queda en el log del comprobante
			// TODO: poner esto
			err := h.fact.EnviarMail(context.Background(), f.Comitente, ids)
			if err != nil {
				log.Error().Err(err).Msg("")
			}
		}()
	} else {
		log.Debug().Msgf("No correspondía mail")
		return
	}

	return
}
