package handler

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/ops/internal/db"
	"bitbucket.org/marcos19/one/pkg/ops/internal/delete"
	"bitbucket.org/marcos19/one/pkg/ops/internal/insert"
	"bitbucket.org/marcos19/one/pkg/ops/internal/saldos"
	"bitbucket.org/marcos19/one/pkg/ops/internal/update"
	"bitbucket.org/marcos19/one/pkg/personas"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/niler"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler es el encargado de recibir los http requests y emitir los responses.
type Handler struct {
	conn     *pgxpool.Pool
	inserter ops.Inserter
	deleter  ops.Deleter
	updater  ops.Updater

	bloqueoCuenta ops.BloqueoCuenta

	// Si es true, cuando se quiera registrar una operación retroactiva,
	// le va a poner la hora 29:59
	CorregirFechas bool
}

var (
	FechaLimiteDesde = time.Now().Add(-time.Hour * 24 * 365 * 60)
	FechaLimiteHasta = time.Now().Add(time.Hour * 24 * 365 * 39)
)

// NewHandler inicializa un cache y devuelve el puntero a este
func NewHandler(
	conn *pgxpool.Pool,
	comp comps.ReaderOneTx,
	ctas cuentas.ReaderOne,
	empr empresas.ReaderOne,
	pers personas.ReaderOne,
	num comps.Numerador,
	bloq ops.BloqueoCuenta,
) (h *Handler, err error) {

	// Valido
	if niler.IsNil(conn) {
		return h, errors.New("Conn no puede ser nil")
	}
	if niler.IsNil(comp) {
		return h, errors.New("comps.ReaderOne no puede ser nil")
	}
	if niler.IsNil(ctas) {
		return h, errors.New("cuentas.ReaderOne no puede ser nil")
	}
	if niler.IsNil(empr) {
		return h, errors.New("empresas.ReaderOne no puede ser nil")
	}
	if niler.IsNil(pers) {
		return h, errors.New("personas.ReaderOne no puede ser nil")
	}
	if niler.IsNil(num) {
		return h, errors.New("comps.Numerador no puede ser nil")
	}

	h = &Handler{
		conn: conn,
	}

	// Inserter
	h.inserter, err = insert.NewInserter(conn, comp, num, ctas, empr, pers, bloq)
	if err != nil {
		return h, errors.Wrap(err, "iniciando Inserter")
	}

	// Deleter
	h.deleter, err = delete.New(num)
	if err != nil {
		return h, errors.Wrap(err, "iniciando Inserter")
	}

	// Updater
	h.updater, err = update.New(conn, comp, empr, h, bloq)
	if err != nil {
		return h, errors.Wrap(err, "iniciando Inserter")
	}

	//if niler.IsNil(cfg.BloqueoCuenta) {
	//return h, errors.New("Bloqueo cuenta no puede ser nil")
	//}
	//h.bloqueoCuenta = cfg.BloqueoCuenta

	h.CorregirFechas = true

	return
}

func (h *Handler) Validate(ctx context.Context, tx pgx.Tx, oo []*ops.Op, cfg *ops.InsertConfig) error {
	return h.inserter.Validate(ctx, tx, oo, cfg)
}

func (h *Handler) Insert(ctx context.Context, tx pgx.Tx, oo []*ops.Op, cfg *ops.InsertConfig) error {
	return h.inserter.Insert(ctx, tx, oo, cfg)
}

func (h *Handler) Update(ctx context.Context, tx pgx.Tx, oo []*ops.Op) error {
	return h.updater.Update(ctx, tx, oo)
}

func (h *Handler) ModificarDatos(ctx context.Context, req ops.ModificarDatosReq) error {
	return h.updater.ModificarDatos(ctx, req)
}

func (h *Handler) Delete(ctx context.Context, tx pgx.Tx, comitente int, ids []uuid.UUID) error {
	return h.deleter.Delete(ctx, tx, comitente, ids)
}

func (h *Handler) GetPartidaAplicada(ctx context.Context, comitente int, id uuid.UUID) (out uuid.UUID, err error) {
	return db.GetPartidaAplicada(ctx, h.conn, comitente, id)
}

// HandleProfundizarPartida devuelve los datos para poder redireccionar el frontend
// al comprobante que dio origen a esta partida.
func (h *Handler) ProfundizarPartida(ctx context.Context, req ops.ProfundizarReq) (out ops.ProfundizarResponse, err error) {
	return db.ProfundizarPartida(ctx, h.conn, req)
}

// HandleProfundizarOp devuelve los datos para poder redireccionar el frontend
// al comprobante que dio origen a esta partida.
func (h *Handler) ProfundizarOp(ctx context.Context, req ops.ProfundizarReq) (out ops.ProfundizarResponse, err error) {
	return db.ProfundizarOp(ctx, h.conn, req)
}

// HandlePartidas devuelve las partidas contables de una operación por su ID
func (h *Handler) Partidas(ctx context.Context, req ops.PartidasReq) (out []ops.Partida, err error) {
	return db.Partidas(ctx, h.conn, req)
}

func (h *Handler) Historial(ctx context.Context, req ops.HistorialReq) (out []ops.HistorialResp, err error) {
	return db.BuscarOps(ctx, h.conn, req)
}

// Devuelve la struct de una op. No incluye asientos contables.
func (h *Handler) ReadOne(ctx context.Context, req ops.ReadOneReq) (out ops.Op, err error) {
	return db.ReadOne(ctx, h.conn, req)
}

func (h *Handler) ReadMany(ctx context.Context, req ops.ReadManyReq) (out []ops.Op, err error) {
	return db.ReadMany(ctx, h.conn, req)
}

// Reconstruir saldos suma los saldos desde tabla partidas y los inserta
// en la tabla saldos.
func (h *Handler) ReconstruirSaldos(ctx context.Context, req ops.ReconstruirReq) (err error) {
	return saldos.ReconstruirSaldos(ctx, h.conn, req)
}

// ValidacionesRapidas hace los chequeos que no demandan llamadas a la
// base de datos.
// Además completa los datos de las partidas contables con los datos de
// la op.
func (h *Handler) ValidacionesRapidas(oo []*ops.Op) (err error) {

	for j := range oo {

		if oo[j].Comitente == 0 {
			return deferror.Validationf("Op %v %v no tiene definida comitente", oo[j].CompNombre, oo[j].NCompStr)
		}
		if oo[j].TranDef == 0 {
			return deferror.Validationf("Op %v %v no tiene definida id de config", oo[j].CompNombre, oo[j].NCompStr)
		}
		if oo[j].CompID == 0 {
			return deferror.Validationf("Op %v %v no tiene definida id de comp", oo[j].CompNombre, oo[j].NCompStr)
		}
		if oo[j].Fecha == 0 {
			return deferror.Validationf("Op %v %v no tiene definida Fecha", oo[j].CompNombre, oo[j].NCompStr)
		}

		if oo[j].Empresa == 0 {
			return deferror.Validationf("El comprobante %v %v no tiene definido empresa", oo[j].CompNombre, oo[j].NCompStr)
		}
		if oo[j].Programa == "" {
			return deferror.Validationf("El comprobante %v %v no tiene definido programa", oo[j].CompNombre, oo[j].NCompStr)
		}
		if oo[j].FechaOriginal == 0 {
			return deferror.Validationf("El comprobante %v %v no tiene definido fecha de comprobante original", oo[j].CompNombre, oo[j].NCompStr)
		}
	}
	return
}

type ProfundizarAsientoResp struct {
	Personas              map[uuid.UUID]string
	Sucursales            map[string]string
	AplicacionesDeEste    map[uuid.UUID]string
	AplicacionesSobreEste map[uuid.UUID][]string
}

// Devuelve los lookups de personas, productos, aplicaciones para mostrar al usuario.
func (h *Handler) ProfundizarAsiento(ctx context.Context, req ops.ReadOneReq) (out ProfundizarAsientoResp, err error) {
	pp, err := h.Partidas(ctx, ops.PartidasReq{Comitente: req.Comitente, ID: req.ID})
	if err != nil {
		return out, err
	}
	out.Personas = map[uuid.UUID]string{}
	out.Sucursales = map[string]string{}
	out.AplicacionesDeEste = map[uuid.UUID]string{}
	out.AplicacionesSobreEste = map[uuid.UUID][]string{}

	sucs := map[int]string{}
	// Junto por si hay repetidas
	for _, v := range pp {
		if v.Persona != nil {
			out.Personas[*v.Persona] = ""
		}
		if v.Sucursal != nil {
			sucs[*v.Sucursal] = ""
		}
		if v.PartidaAplicada != nil {
			if *v.PartidaAplicada == v.ID {
				out.AplicacionesSobreEste[*v.PartidaAplicada] = []string{}
			} else {
				out.AplicacionesDeEste[*v.PartidaAplicada] = ""
			}
		}
	}

	// Pego personas
	err = lookupPersonas(ctx, h.conn, out.Personas)
	if err != nil {
		return out, err
	}

	// Pego sucursales
	err = lookupSucursales(ctx, h.conn, sucs)
	if err != nil {
		return out, err
	}
	for k, v := range sucs {
		out.Sucursales[strconv.Itoa(k)] = v
	}

	// Pego los comps a los que aplica esta op
	err = lookupAplica(ctx, h.conn, out.AplicacionesDeEste)
	if err != nil {
		return out, err
	}

	// Pego aplicaciones
	err = lookupAplicaciones(ctx, h.conn, out.AplicacionesSobreEste)
	if err != nil {
		return out, err
	}
	return
}

func lookupPersonas(ctx context.Context, conn *pgxpool.Pool, m map[uuid.UUID]string) (err error) {
	if len(m) == 0 {
		return
	}
	ids := []uuid.UUID{}
	for k := range m {
		ids = append(ids, k)
	}
	query := fmt.Sprintf("SELECT id, nombre FROM personas WHERE id IN (%v)", arrayUUID(ids))

	rows, err := conn.Query(ctx, query)
	if err != nil {
		return errors.Wrap(err, "buscando nombres personas")
	}
	defer rows.Close()
	for rows.Next() {
		id := uuid.UUID{}
		name := ""
		err = rows.Scan(&id, &name)
		if err != nil {
			return errors.Wrap(err, "escaneando nombre persona")
		}
		m[id] = name
	}
	return
}

func lookupSucursales(ctx context.Context, conn *pgxpool.Pool, m map[int]string) (err error) {
	if len(m) == 0 {
		return
	}

	ids := tipos.GrupoInts{}
	for k := range m {
		ids = append(ids, k)
	}
	query := fmt.Sprintf("SELECT id, nombre FROM sucursales WHERE id IN %v", ids.ParaClausulaIn())

	rows, err := conn.Query(ctx, query)
	if err != nil {
		return errors.Wrap(err, "buscando nombres personas")
	}
	defer rows.Close()
	for rows.Next() {
		id := 0
		name := ""
		err = rows.Scan(&id, &name)
		if err != nil {
			return errors.Wrap(err, "escaneando nombre persona")
		}
		m[id] = name
	}
	return
}

// Los comprobantes a los que aplica este asiento
func lookupAplica(ctx context.Context, conn *pgxpool.Pool, m map[uuid.UUID]string) (err error) {
	if len(m) == 0 {
		return
	}

	ids := []uuid.UUID{}
	for k := range m {
		ids = append(ids, k)
	}
	query := fmt.Sprintf(`SELECT partidas.id, ops.comp_nombre, ops.n_comp_str
		FROM partidas
		INNER JOIN ops ON ops.id = partidas.op_id
		WHERE partidas.id IN (%v)`, arrayUUID(ids))

	rows, err := conn.Query(ctx, query)
	if err != nil {
		return errors.Wrap(err, "buscando nombres aplicaciones")
	}
	defer rows.Close()
	for rows.Next() {
		id := uuid.UUID{}
		compNombre := ""
		nCompStr := ""
		err = rows.Scan(&id, &compNombre, &nCompStr)
		if err != nil {
			return errors.Wrap(err, "escaneando nombre aplicaciones")
		}
		m[id] = fmt.Sprintf("%v %v", compNombre, nCompStr)
	}
	return
}

// Devuelve el los comprobantes que aplican a esta op.
func lookupAplicaciones(ctx context.Context, conn *pgxpool.Pool, m map[uuid.UUID][]string) (err error) {
	if len(m) == 0 {
		return
	}

	ids := []uuid.UUID{}
	for k := range m {
		ids = append(ids, k)
	}
	query := fmt.Sprintf(`SELECT partidas.id, partida_aplicada, ops.comp_nombre, ops.n_comp_str
		FROM partidas
		INNER JOIN ops ON ops.id = partidas.op_id
		WHERE partida_aplicada IN (%v)`, arrayUUID(ids))

	rows, err := conn.Query(ctx, query)
	if err != nil {
		return errors.Wrap(err, "buscando nombres aplicaciones")
	}
	defer rows.Close()

	for rows.Next() {
		id := uuid.UUID{}
		aplicada := uuid.UUID{}
		compNombre := ""
		nCompStr := ""
		err = rows.Scan(&id, &aplicada, &compNombre, &nCompStr)
		if err != nil {
			return errors.Wrap(err, "escaneando nombre aplicaciones")
		}
		if id == aplicada {
			// Es el original
			// delete(m, aplicada)
			continue
		}
		//fmt.Println("dejando", id, aplicada, compNombre, nCompStr)
		prev := m[aplicada]
		prev = append(prev, fmt.Sprintf("%v %v", compNombre, nCompStr))
		m[aplicada] = prev
	}
	return
}

func arrayUUID(ids []uuid.UUID) string {
	idsStr := []string{}
	for _, v := range ids {
		idsStr = append(idsStr, fmt.Sprintf("'%v'", v.String()))
	}
	return strings.Join(idsStr, ", ")
}
