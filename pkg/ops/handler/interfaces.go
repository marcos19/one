package handler

import (
	"bitbucket.org/marcos19/one/pkg/ops"
)

// Una vez pasados los controles generales, analiza los controles
// de saldos de caja, inventario, etc.
type CustomValidator interface {
	Validate(oo []*ops.Op) error
}

type ErrValidacion struct {
	msg string
}

func (e ErrValidacion) Error() string {
	return e.msg
}
