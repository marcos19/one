package ops

import (
	"fmt"
	"time"
)

// Ocurre cuando se intenta cargar un comprobante que ya se encuentra cargado.
type ErrComprobanteCargado struct {
	createdAt time.Time
	tipo      int
	pv        int
	ncomp     int
}

func (e ErrComprobanteCargado) Error() string {
	return fmt.Sprintf("comprobante tipo %v número %v-%v fue cargado el %v", e.tipo, e.pv, e.ncomp, e.createdAt)
}
