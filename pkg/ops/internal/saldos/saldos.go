package saldos

import (
	"context"
	"math"

	"bitbucket.org/marcos19/one/pkg/cajas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/depositos"
	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	uuid "github.com/gofrs/uuid"
)

// Aggregate es el registro de saldos
type Aggregate struct {
	Monto     dec.D2
	Cantidad  dec.D4
	MontoOrig dec.D4
}

type AggKey struct {
	Comitente       int
	Cuenta          int
	Centro          int
	Persona         uuid.UUID
	Sucursal        int
	Caja            int
	Producto        uuid.UUID
	SKU             uuid.UUID
	Deposito        int
	PartidaAplicada uuid.UUID
	Moneda          int
	Contrato        int
	Empresa         int
}

type Saldos map[AggKey]Aggregate

func crearKey(part ops.Partida) AggKey {

	key := AggKey{
		Comitente: part.Comitente,
		Cuenta:    part.Cuenta,
		Empresa:   part.Empresa,
	}
	if part.Centro != nil {
		key.Centro = *part.Centro
	}
	if part.Persona != nil {
		key.Persona = *part.Persona
	}
	if part.Sucursal != nil {
		key.Sucursal = *part.Sucursal
	}
	if part.Caja != nil {
		key.Caja = *part.Caja
	}
	if part.Producto != nil {
		key.Producto = *part.Producto
	}
	if part.SKU != nil {
		key.SKU = *part.SKU
	}
	if part.Deposito != nil {
		key.Deposito = *part.Deposito
	}
	if part.PartidaAplicada != nil {
		key.PartidaAplicada = *part.PartidaAplicada
	}
	if part.Contrato != nil {
		key.Contrato = *part.Contrato
	}
	if part.Moneda != nil {
		key.Moneda = *part.Moneda
	}
	return key
}

// Si hay partidas que se pueden agregar las junto.
func resumirPartidas(pp []ops.Partida) (ss Saldos) {
	ss = make(Saldos)
	for _, part := range pp {

		key := crearKey(part)

		// Estaba?
		anterior := ss[key]
		if part.Cantidad != nil {
			anterior.Cantidad += *part.Cantidad
		}
		if part.MontoMonedaOriginal != nil {
			anterior.MontoOrig += *part.MontoMonedaOriginal
		}
		anterior.Monto += part.Monto

		// Si me da cero, directamente la borro
		if anterior.Monto == 0 && anterior.Cantidad == 0 && anterior.MontoOrig == 0 {
			delete(ss, key)
			continue
		}

		ss[key] = anterior
	}
	return
}

// sobreAplicadas controla que ninguna partida haya sido sobre aplicada.
func partidasSobreAplicadas(ssAntes, ssDespues Saldos) (err error) {

	for k, despues := range ssDespues {
		if k.PartidaAplicada == uuid.Nil {
			continue
		}

		antes := ssAntes[k]

		// Aplicación de MONTOS

		// Si la factura era de $100, después de aplicar el saldo tiene que quedar
		// entre 0 y 100
		// Si la factura era de $-100, después de aplicar el saldo tiene que quedar
		// entre 0 y -100
		if (antes.Monto < 0 && despues.Monto > 0) || (antes.Monto > 0 && despues.Monto < 0) {
			return deferror.Validationf("Se sobre aplicó la partida cuenta %v, antes: %v, ahora: %v",
				k.Cuenta, antes.Monto, despues.Monto)
		}
		// El saldo de la partida tiene que ser menor que antes
		if math.Abs(float64(despues.Monto)) > math.Abs(float64(antes.Monto)) {
			return deferror.Validationf("Se sobre aplicó la partida cuenta %v, antes: %v, ahora: %v",
				k.Cuenta, antes.Monto, despues.Monto)
		}

		// Aplicación de CANTIDADES

		// El saldo de la partida tiene que ser menor que antes
		if (antes.Cantidad < 0 && despues.Cantidad > 0) || (antes.Cantidad > 0 && despues.Cantidad < 0) {
			return deferror.Validationf("Se sobre aplicaron cantidades para cuenta %v, antes: %v, ahora: %v",
				k.Cuenta, antes.Monto, despues.Monto)
		}
		// El saldo de la partida tiene que ser menor que antes
		if math.Abs(float64(despues.Cantidad)) > math.Abs(float64(antes.Cantidad)) {
			return deferror.Validationf(
				"Se sobre aplicó la partida cuenta %v, antes: %v, ahora: %v",
				k.Cuenta, antes.Monto, despues.Monto)
		}

	}
	return
}

// Controlo que la caja no quede con saldo negativo luego de esta operación.
func SaldosCajaPositivos(ctx context.Context, ssAntes, ssDespues Saldos, store cajas.ReaderOne) (err error) {

	for k, despues := range ssDespues {
		if k.Caja == 0 {
			continue
		}

		// Busco caja
		caja, err := store.ReadOne(ctx, cajas.ReadOneReq{Comitente: k.Comitente, ID: k.Caja})
		if err != nil {
			return errors.Wrapf(err, "buscando caja %v", k.Caja)
		}

		// Esta okey
		if caja.AceptaSaldoNegativo {
			continue
		}

		if despues.Monto < 0 {
			return deferror.Validationf(
				"La cuenta %v no puede quedar con saldo negativo en la caja %v",
				k.Cuenta, k.Caja,
			)
		}
	}
	return nil
}

// Controla que los saldos de stock no queden en negativo luego de esta operación.
func SaldosStockPositivos(ctx context.Context, ssAntes, ssDespues Saldos, store depositos.Getter) (err error) {

	for k, despues := range ssDespues {
		if k.Deposito == 0 {
			continue
		}

		// Busco el depósito
		deposito, err := store.ReadOne(ctx, depositos.ReadOneReq{Comitente: k.Comitente, ID: k.Deposito})
		if err != nil {
			return errors.Wrapf(err, "buscando depósito ID='%v'", k.Deposito)
		}

		// Esta okey
		if deposito.AceptaStockNegativo {
			continue
		}

		// Verifico la cantidad > 0
		if despues.Cantidad < 0 {
			return deferror.Validationf("El producto %v no puede quedar con saldo negativo en el depósito %v",
				k.SKU, deposito.Nombre)
		}
	}
	return nil
}
