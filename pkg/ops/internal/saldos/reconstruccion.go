package saldos

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	uuid "github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/rs/zerolog/log"
)

// Reconstruir saldos suma los saldos desde tabla partidas y los inserta
// en la tabla saldos.
func ReconstruirSaldos(ctx context.Context, conn *pgxpool.Pool, req ops.ReconstruirReq) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó el ID de comitente")
	}

	// Inicio transacción
	log.Debug().Msg("Iniciando transaccion...")
	tx, err := conn.Begin(ctx)
	if err != nil {
		return errors.Wrap(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)

	// Busco saldos
	log.Debug().Msg("Buscando saldos...")
	ss, err := generarSaldos(ctx, tx, req)
	if err != nil {
		return errors.Wrap(err, "buscando saldos")
	}

	// Borro saldos anteriores
	err = borrarSaldos(ctx, tx, req)
	if err != nil {
		return errors.Wrap(err, "borrando saldos anteriores")
	}

	// Inserto nuevos saldos
	err = insertarSaldos(ctx, tx, ss)
	if err != nil {
		return errors.Wrap(err, "insertando nuevos saldos")
	}

	// Confirmo transacción
	err = tx.Commit(ctx)
	if err != nil {
		return errors.Wrap(err, "confirmando transacción")
	}
	return
}

func generarSaldos(ctx context.Context, tx pgx.Tx, conf ops.ReconstruirReq) (ss Saldos, err error) {
	ww := []string{}
	pp := []interface{}{}
	where := ""

	if conf.Comitente != 0 {
		ww = append(ww, fmt.Sprintf("comitente = $%v", len(pp)+1))
		pp = append(pp, conf.Comitente)
	}

	if conf.Empresa != 0 {
		ww = append(ww, fmt.Sprintf("empresa = $%v", len(pp)+1))
		pp = append(pp, conf.Empresa)
	}

	if conf.Cuenta != 0 {
		ww = append(ww, fmt.Sprintf("cuenta = $%v", len(pp)+1))
		pp = append(pp, conf.Cuenta)
	}

	// Excluyo partidas de asientos especiales (inflacion, cierre, apertura, etc)
	// ww = append(ww, fmt.Sprintf("tipo_asiento IS NULL"))

	if len(ww) > 0 {
		where = fmt.Sprintf(" WHERE %v", strings.Join(ww, " AND "))
	}

	query := fmt.Sprintf(`
	SELECT
		comitente,
		cuenta,
		centro,
		persona, 
		sucursal,
		caja,
		producto,
		sku,
		deposito,
		moneda,
		partida_aplicada,
		contrato,
		empresa,
		SUM(monto), 
		SUM(cantidad),
		SUM(monto_moneda_original)
	FROM partidas
	%v
	GROUP BY
		comitente,
		cuenta,
		centro,
		persona, 
		sucursal,
		caja,
		producto,
		sku,
		deposito,
		moneda,
		partida_aplicada,
		contrato,
		empresa
	HAVING SUM(monto) <> 0 OR SUM(cantidad) <> 0 OR SUM(monto_moneda_original) <>0
	;`, where)

	log.Trace().Msg(query)
	rows, err := tx.Query(ctx, query, pp...)
	if err != nil {
		return ss, deferror.DB(err, "ejectuando query SELECT para determinar saldos")
	}
	defer rows.Close()
	ss = Saldos{}

	// Escaneo cada saldo
	for rows.Next() {
		k := AggKey{}
		v := Aggregate{}
		centro := new(int)
		persona := &uuid.UUID{}
		sucursal := new(int)
		caja := new(int)
		producto := &uuid.UUID{}
		sku := &uuid.UUID{}
		deposito := new(int)
		moneda := new(int)
		partidaAplicada := &uuid.UUID{}
		contrato := new(int)
		monto := new(int64)
		cantidad := new(int64)
		montoOrig := new(int64)

		err = rows.Scan(
			&k.Comitente,
			&k.Cuenta,
			&centro,
			&persona,
			&sucursal,
			&caja,
			&producto,
			&sku,
			&deposito,
			&moneda,
			&partidaAplicada,
			&contrato,
			&k.Empresa,
			&monto,
			&cantidad,
			&montoOrig,
		)
		if err != nil {
			return ss, deferror.DB(err, "escaneando row")
		}
		if centro != nil {
			k.Centro = *centro
		}
		if persona != nil {
			k.Persona = *persona
		}
		if sucursal != nil {
			k.Sucursal = *sucursal
		}
		if caja != nil {
			k.Caja = *caja
		}
		if producto != nil {
			k.Producto = *producto
		}
		if sku != nil {
			k.SKU = *sku
		}
		if deposito != nil {
			k.Deposito = *deposito
		}
		if moneda != nil {
			k.Moneda = *moneda
		}
		if partidaAplicada != nil {
			k.PartidaAplicada = *partidaAplicada
		}
		if contrato != nil {
			k.Contrato = *contrato
		}
		if cantidad != nil {
			v.Cantidad = dec.D4(*cantidad)
		}
		if monto != nil {
			v.Monto = dec.D2(*monto)
		}
		if montoOrig != nil {
			v.MontoOrig = dec.D4(*montoOrig)
		}
		ss[k] = v
	}
	return
}

func borrarSaldos(ctx context.Context, tx pgx.Tx, conf ops.ReconstruirReq) (err error) {

	ww := []string{}
	pp := []interface{}{}
	where := ""

	if conf.Comitente != 0 {
		ww = append(ww, fmt.Sprintf("comitente = $%v", len(pp)+1))
		pp = append(pp, conf.Comitente)
	}

	if conf.Empresa != 0 {
		ww = append(ww, fmt.Sprintf("empresa = $%v", len(pp)+1))
		pp = append(pp, conf.Empresa)
	}

	if conf.Cuenta != 0 {
		ww = append(ww, fmt.Sprintf("cuenta = $%v", len(pp)+1))
		pp = append(pp, conf.Cuenta)
	}

	if len(ww) > 0 {
		where = fmt.Sprintf(" WHERE %v", strings.Join(ww, " AND "))
	}

	delQuery := fmt.Sprintf("DELETE FROM saldos %v; ", where)
	_, err = tx.Exec(ctx, delQuery, pp...)
	if err != nil {
		return deferror.DB(err, "ejecutando query DELETE")
	}
	return
}

func insertarSaldos(ctx context.Context, tx pgx.Tx, ss Saldos) (err error) {
	if len(ss) == 0 {
		return
	}

	kk := []AggKey{}
	vv := []Aggregate{}
	for k, v := range ss {
		kk = append(kk, k)
		vv = append(vv, v)
	}

	log.Debug().Int("keys", len(kk)).Int("values", len(vv)).Msgf("Insertando saldos...")

	count := 0
	for {
		vals := []string{}
		if count > (len(kk)) {
			log.Debug().Msg("count > len(ss)")
			break
		}
		for i := range kk {
			// log.Debug().Int("i", i).Int("count", count).Msg("loopeando kk")
			if i >= 10000 || count+1 > len(kk) {
				// log.Debug().Msg("i > 10000")
				break
			}
			val := fmt.Sprintf("(%v,%v,%v,%v,%v,%v,%v,%v,%v,%v,%v,%v,%v,%v,%v,%v)",
				kk[count].Comitente,
				kk[count].Cuenta,
				kk[count].Centro,
				by(kk[count].Persona),
				kk[count].Sucursal,
				kk[count].Caja,
				by(kk[count].Producto),
				by(kk[count].SKU),
				kk[count].Deposito,
				kk[count].Moneda,
				by(kk[count].PartidaAplicada),
				kk[count].Contrato,
				kk[count].Empresa,
				int(vv[count].Monto),
				int(vv[count].Cantidad),
				int(vv[count].MontoOrig),
			)
			count++
			vals = append(vals, val)
		}
		values := strings.Join(vals, ",\n")
		query := fmt.Sprintf(`
			 INSERT INTO saldos (comitente, cuenta, centro, persona, sucursal, caja, 
				producto, sku, deposito, moneda, partida_aplicada, 
				contrato, empresa, monto, cantidad, monto_orig) VALUES %v `, values)

		// log.Trace().Int("cantidad", len(vals)).Msgf("Insertando saldos...")
		_, err = tx.Exec(ctx, query)
		if err != nil {
			log.Debug().Int("cantidad", len(vals)).Msg(query)
			return deferror.DB(err, "ejecutando INSERT de nuevos saldos")
		}
		if count+1 > len(kk) {
			break
		}
	}
	return
}
