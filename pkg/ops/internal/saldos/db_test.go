package saldos

import (
	"context"
	"testing"

	"bitbucket.org/marcos19/one/pkg/database"
	"bitbucket.org/marcos19/one/pkg/migrations/ddl"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	uuid "github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func Prepare(ctx context.Context, conn *pgxpool.Pool) error {

	{ // Borro base de datos
		q := `DROP DATABASE IF EXISTS test_saldos;`
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "borro base de datos")
		}
	}
	{ // Creo base de datos
		q := `CREATE DATABASE test_saldos; USE test_saldos;`
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando base de datos")
		}
	}

	{ // Partidas (la necesito para borrar
		q := ddl.Saldos().SQL
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla partidas")
		}
	}

	return nil
}

func TestInsertSaldos(t *testing.T) {

	ctx := context.Background()

	cn := database.TestDBConnectionString()

	cfg, err := pgxpool.ParseConfig(cn)
	require.Nil(t, err)
	conn, err := pgxpool.ConnectConfig(context.Background(), cfg)
	require.Nil(t, err)

	// Preparo base de datos
	err = Prepare(ctx, conn)
	require.Nil(t, err)

	comitente := 1
	cuenta := 2
	centro := 3
	persona := uuid.FromStringOrNil("54db92f5-721e-431f-ab34-410149c1aaaa")
	sucursal := 4
	caja := 5
	producto := uuid.FromStringOrNil("54db92f5-721e-431f-ab34-410149c1bbbb")
	sku := uuid.FromStringOrNil("54db92f5-721e-431f-ab34-410149c1cccc")
	deposito := 6
	partidaAplicada := uuid.FromStringOrNil("54db92f5-721e-431f-ab34-410149c1dddd")
	moneda := 7
	contrato := 8
	empresa := 9

	t.Run("INSERT", func(t *testing.T) { // Inserto
		tx, err := conn.Begin(ctx)
		require.Nil(t, err)

		key1 := AggKey{
			Comitente:       comitente,
			Cuenta:          cuenta,
			Centro:          centro,
			Persona:         persona,
			Sucursal:        sucursal,
			Caja:            caja,
			Producto:        producto,
			SKU:             sku,
			Deposito:        deposito,
			PartidaAplicada: partidaAplicada,
			Moneda:          moneda,
			Contrato:        contrato,
			Empresa:         empresa,
		}
		val1 := Aggregate{
			Monto:     dec.NewD2(100),
			Cantidad:  dec.NewD4(100),
			MontoOrig: dec.NewD4(100),
		}
		ss := Saldos{}
		ss[key1] = val1
		err = FijarSaldos(ctx, tx, ss)
		require.Nil(t, err)

		err = tx.Commit(ctx)
		require.Nil(t, err)
	})

	t.Run("READ", func(t *testing.T) { // Inserto

		tx, err := conn.Begin(ctx)
		require.Nil(t, err)

		key1 := AggKey{
			Comitente:       comitente,
			Cuenta:          cuenta,
			Centro:          centro,
			Persona:         persona,
			Sucursal:        sucursal,
			Caja:            caja,
			Producto:        producto,
			SKU:             sku,
			Deposito:        deposito,
			PartidaAplicada: partidaAplicada,
			Moneda:          moneda,
			Contrato:        contrato,
			Empresa:         empresa,
		}
		val1 := Aggregate{
			Monto:     dec.NewD2(100),
			Cantidad:  dec.NewD4(100),
			MontoOrig: dec.NewD4(100),
		}
		ss := Saldos{}
		ss[key1] = val1
		antes, despues, err := DeterminarSaldos(ctx, tx, ss)
		require.Nil(t, err)
		assert.Equal(t, antes, ss)

		ss[key1] = Aggregate{
			Monto:     dec.NewD2(200),
			Cantidad:  dec.NewD4(200),
			MontoOrig: dec.NewD4(200),
		}
		assert.Equal(t, despues, ss)

	})
}
