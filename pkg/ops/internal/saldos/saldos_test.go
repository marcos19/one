package saldos

import (
	"testing"

	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/crosslogic/dec"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestSobreAplicadas(t *testing.T) {
	antes := Saldos{}
	id1, _ := uuid.NewV1()
	id2, _ := uuid.NewV1()

	antes[AggKey{Cuenta: 1001, PartidaAplicada: id1}] = Aggregate{Monto: 100}
	antes[AggKey{Cuenta: 1002, PartidaAplicada: id2}] = Aggregate{Monto: -30}

	{ // Aplico una partida bien
		despues := Saldos{}
		despues[AggKey{Cuenta: 1001, PartidaAplicada: id1}] = Aggregate{Monto: 90}
		despues[AggKey{Cuenta: 1002, PartidaAplicada: id2}] = Aggregate{Monto: -30}
		err := partidasSobreAplicadas(antes, despues)
		assert.Nil(t, err)
	}

	{ // Cambio de signo una partida
		despues := Saldos{}
		despues[AggKey{Cuenta: 1001, PartidaAplicada: id1}] = Aggregate{Monto: -1}
		despues[AggKey{Cuenta: 1002, PartidaAplicada: id2}] = Aggregate{Monto: -30}
		err := partidasSobreAplicadas(antes, despues)
		assert.NotNil(t, err)
	}

	{ // Mismo signo y mayor monto
		despues := Saldos{}
		despues[AggKey{Cuenta: 1001, PartidaAplicada: id1}] = Aggregate{Monto: 120}
		despues[AggKey{Cuenta: 1002, PartidaAplicada: id2}] = Aggregate{Monto: -30}
		err := partidasSobreAplicadas(antes, despues)
		assert.NotNil(t, err)
	}
}

func TestCrearKey(t *testing.T) {

	t.Run("todos los campos", func(t *testing.T) {
		comitente := 1
		cuenta := 2
		centro := 3
		persona := uuid.FromStringOrNil("54db92f5-721e-431f-ab34-410149c1aaaa")
		sucursal := 4
		caja := 5
		producto := uuid.FromStringOrNil("54db92f5-721e-431f-ab34-410149c1bbbb")
		sku := uuid.FromStringOrNil("54db92f5-721e-431f-ab34-410149c1cccc")
		deposito := 6
		partidaAplicada := uuid.FromStringOrNil("54db92f5-721e-431f-ab34-410149c1dddd")
		moneda := 7
		contrato := 8
		empresa := 9

		p := ops.Partida{
			Comitente:       comitente,
			Cuenta:          cuenta,
			Centro:          &centro,
			Persona:         &persona,
			Sucursal:        &sucursal,
			Caja:            &caja,
			Producto:        &producto,
			SKU:             &sku,
			Deposito:        &deposito,
			PartidaAplicada: &partidaAplicada,
			Moneda:          &moneda,
			Contrato:        &contrato,
			Empresa:         empresa,
		}
		k := crearKey(p)
		expected := AggKey{
			Comitente:       comitente,
			Cuenta:          cuenta,
			Centro:          centro,
			Persona:         persona,
			Sucursal:        sucursal,
			Caja:            caja,
			Producto:        producto,
			SKU:             sku,
			Deposito:        deposito,
			PartidaAplicada: partidaAplicada,
			Moneda:          moneda,
			Contrato:        contrato,
			Empresa:         empresa,
		}
		assert.Equal(t, expected, k)
	})

	t.Run("campos vacíos", func(t *testing.T) {

		p := ops.Partida{}
		k := crearKey(p)
		expected := AggKey{}
		assert.Equal(t, expected, k)
	})
}

func TestResumirPartidas(t *testing.T) {

	t.Run("vacía", func(t *testing.T) {
		out := resumirPartidas(nil)
		require.Len(t, out, 0)
	})

	comitente := 1
	cuenta := 2
	centro := 3
	persona := uuid.FromStringOrNil("54db92f5-721e-431f-ab34-410149c1aaaa")
	sucursal := 4
	caja := 5
	producto := uuid.FromStringOrNil("54db92f5-721e-431f-ab34-410149c1bbbb")
	sku := uuid.FromStringOrNil("54db92f5-721e-431f-ab34-410149c1cccc")
	deposito := 6
	partidaAplicada := uuid.FromStringOrNil("54db92f5-721e-431f-ab34-410149c1dddd")
	moneda := 7
	contrato := 8
	empresa := 9

	t.Run("1 partida", func(t *testing.T) {
		monto := dec.NewD2(100)
		cantidad := dec.NewD4(200)
		montoOrig := dec.NewD4(300)

		p := ops.Partida{
			Comitente:           comitente,
			Cuenta:              cuenta,
			Centro:              &centro,
			Persona:             &persona,
			Sucursal:            &sucursal,
			Caja:                &caja,
			Producto:            &producto,
			SKU:                 &sku,
			Deposito:            &deposito,
			PartidaAplicada:     &partidaAplicada,
			Moneda:              &moneda,
			Contrato:            &contrato,
			Empresa:             empresa,
			Monto:               monto,
			Cantidad:            &cantidad,
			MontoMonedaOriginal: &montoOrig,
		}
		out := resumirPartidas([]ops.Partida{p})
		require.Len(t, out, 1)
		for k, v := range out {
			assert.Equal(t, comitente, k.Comitente)
			assert.Equal(t, monto, v.Monto)
			assert.Equal(t, cantidad, v.Cantidad)
			assert.Equal(t, montoOrig, v.MontoOrig)
		}
	})

	t.Run("2 partidas", func(t *testing.T) {
		monto := dec.NewD2(100)
		cantidad := dec.NewD4(200)
		montoOrig := dec.NewD4(300)

		p := ops.Partida{
			Comitente:           comitente,
			Cuenta:              cuenta,
			Centro:              &centro,
			Persona:             &persona,
			Sucursal:            &sucursal,
			Caja:                &caja,
			Producto:            &producto,
			SKU:                 &sku,
			Deposito:            &deposito,
			PartidaAplicada:     &partidaAplicada,
			Moneda:              &moneda,
			Contrato:            &contrato,
			Empresa:             empresa,
			Monto:               monto,
			Cantidad:            &cantidad,
			MontoMonedaOriginal: &montoOrig,
		}
		p2 := ops.Partida{
			Comitente:           comitente,
			Cuenta:              cuenta,
			Centro:              &centro,
			Persona:             &persona,
			Sucursal:            &sucursal,
			Caja:                &caja,
			Producto:            &producto,
			SKU:                 &sku,
			Deposito:            &deposito,
			PartidaAplicada:     &partidaAplicada,
			Moneda:              &moneda,
			Contrato:            &contrato,
			Empresa:             empresa,
			Monto:               monto,
			Cantidad:            &cantidad,
			MontoMonedaOriginal: &montoOrig,
		}
		out := resumirPartidas([]ops.Partida{p, p2})
		require.Len(t, out, 1)
		for k, v := range out {
			assert.Equal(t, comitente, k.Comitente)
			assert.Equal(t, monto*2, v.Monto)
			assert.Equal(t, cantidad*2, v.Cantidad)
			assert.Equal(t, montoOrig*2, v.MontoOrig)
		}
	})

	t.Run("da cero", func(t *testing.T) {
		monto := dec.NewD2(100)
		cantidad := dec.NewD4(200)
		montoOrig := dec.NewD4(300)

		p := ops.Partida{
			Comitente:           comitente,
			Cuenta:              cuenta,
			Centro:              &centro,
			Persona:             &persona,
			Sucursal:            &sucursal,
			Caja:                &caja,
			Producto:            &producto,
			SKU:                 &sku,
			Deposito:            &deposito,
			PartidaAplicada:     &partidaAplicada,
			Moneda:              &moneda,
			Contrato:            &contrato,
			Empresa:             empresa,
			Monto:               monto,
			Cantidad:            &cantidad,
			MontoMonedaOriginal: &montoOrig,
		}

		monto2 := dec.NewD2(-100)
		cantidad2 := dec.NewD4(-200)
		montoOrig2 := dec.NewD4(-300)
		p2 := ops.Partida{
			Comitente:           comitente,
			Cuenta:              cuenta,
			Centro:              &centro,
			Persona:             &persona,
			Sucursal:            &sucursal,
			Caja:                &caja,
			Producto:            &producto,
			SKU:                 &sku,
			Deposito:            &deposito,
			PartidaAplicada:     &partidaAplicada,
			Moneda:              &moneda,
			Contrato:            &contrato,
			Empresa:             empresa,
			Monto:               monto2,
			Cantidad:            &cantidad2,
			MontoMonedaOriginal: &montoOrig2,
		}
		out := resumirPartidas([]ops.Partida{p, p2})
		require.Len(t, out, 0)
	})

	t.Run("queda solamente cantidad", func(t *testing.T) {
		monto := dec.NewD2(100)
		cantidad := dec.NewD4(200)
		montoOrig := dec.NewD4(300)

		p := ops.Partida{
			Comitente:           comitente,
			Cuenta:              cuenta,
			Centro:              &centro,
			Persona:             &persona,
			Sucursal:            &sucursal,
			Caja:                &caja,
			Producto:            &producto,
			SKU:                 &sku,
			Deposito:            &deposito,
			PartidaAplicada:     &partidaAplicada,
			Moneda:              &moneda,
			Contrato:            &contrato,
			Empresa:             empresa,
			Monto:               monto,
			Cantidad:            &cantidad,
			MontoMonedaOriginal: &montoOrig,
		}

		monto2 := dec.NewD2(-100)
		cantidad2 := dec.NewD4(-200)
		montoOrig2 := dec.NewD4(-301)
		p2 := ops.Partida{
			Comitente:           comitente,
			Cuenta:              cuenta,
			Centro:              &centro,
			Persona:             &persona,
			Sucursal:            &sucursal,
			Caja:                &caja,
			Producto:            &producto,
			SKU:                 &sku,
			Deposito:            &deposito,
			PartidaAplicada:     &partidaAplicada,
			Moneda:              &moneda,
			Contrato:            &contrato,
			Empresa:             empresa,
			Monto:               monto2,
			Cantidad:            &cantidad2,
			MontoMonedaOriginal: &montoOrig2,
		}
		out := resumirPartidas([]ops.Partida{p, p2})
		require.Len(t, out, 1)
		for k, v := range out {
			assert.Equal(t, comitente, k.Comitente)
			assert.Equal(t, dec.NewD2(0), v.Monto)
			assert.Equal(t, dec.NewD4(0), v.Cantidad)
			assert.Equal(t, dec.NewD4(-1), v.MontoOrig)
		}
	})
}
