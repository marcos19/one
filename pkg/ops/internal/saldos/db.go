package saldos

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/dec"
	uuid "github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/rs/zerolog/log"
)

// Devuelve como quedarían los saldos luego de ser modificados por estas ops.
func TraerSaldos(ctx context.Context, tx pgx.Tx, oo []*ops.Op) (out Saldos, err error) {

	// Aplano las partidas contables
	pp := []ops.Partida{}
	for _, v := range oo {
		pp = append(pp, v.PartidasContables...)
	}
	ss := resumirPartidas(pp)

	// Determino los saldos
	_, out, err = DeterminarSaldos(ctx, tx, ss)
	if err != nil {
		return out, errors.Wrap(err, "buscando saldos")
	}

	return
}

// Devuelve los saldos que generan las Ops ingresadas. Sólamente de esas.
func TraerSaldosDeOps(ctx context.Context, tx pgx.Tx, comitente int, ids []uuid.UUID) (out Saldos, err error) {

	query := fmt.Sprintf(`
SELECT comitente, cuenta, centro, persona, sucursal, caja,
producto, sku, deposito, unicidad, partida_aplicada, contrato, empresa,
SUM(monto), SUM(cantidad), SUM(monto_moneda_original)
FROM partidas
WHERE op_id IN (%v) AND comitente = %v
GROUP BY comitente, cuenta, centro, persona, sucursal, caja,
producto, sku, deposito, unicidad, partida_aplicada, 
contrato, empresa;
`, arrayUUID(ids), comitente)

	log.Debug().Msgf("Query para buscar saldos de la op %v", query)
	rows, err := tx.Query(ctx, query)
	if err != nil {
		return out, deferror.DB(err, "buscando partidas de las ops")
	}
	defer rows.Close()
	out = make(Saldos)

	count := 0
	for rows.Next() {
		count++
		var centro, sucursal, caja, deposito, moneda, contrato, monto, cantidad, montoOrig *int
		var persona, producto, sku, unicidad, partidaAplicada *uuid.UUID
		k := AggKey{}
		v := Aggregate{}
		err = rows.Scan(
			&k.Comitente,
			&k.Cuenta,
			&centro,
			&persona,
			&sucursal,
			&caja,
			&producto,
			&sku,
			&deposito,
			&unicidad,
			&partidaAplicada,
			&contrato,
			&k.Empresa,
			&monto,
			&cantidad,
			&montoOrig,
		)
		if err != nil {
			return out, errors.Wrap(err, "escanendo partida")
		}
		if centro != nil {
			k.Centro = *centro
		}
		if persona != nil {
			k.Persona = *persona
		}
		if sucursal != nil {
			k.Sucursal = *sucursal
		}
		if caja != nil {
			k.Caja = *caja
		}
		if producto != nil {
			k.Producto = *producto
		}
		if sku != nil {
			k.SKU = *sku
		}
		if deposito != nil {
			k.Deposito = *deposito
		}
		if moneda != nil {
			k.Moneda = *moneda
		}
		if partidaAplicada != nil {
			k.PartidaAplicada = *partidaAplicada
		}
		if contrato != nil {
			k.Contrato = *contrato
		}
		if monto != nil {
			v.Monto = dec.D2(*monto)
		}
		if cantidad != nil {
			v.Cantidad = dec.D4(*cantidad)
		}
		if montoOrig != nil {
			v.MontoOrig = dec.D4(*montoOrig)
		}
		// Le doy vuelta los signos
		v.Monto = -v.Monto
		v.Cantidad = -v.Cantidad
		v.MontoOrig = -v.MontoOrig
		log.Debug().Int("iteración", count).Msgf("Saldo a borrar %v", k)
		out[k] = v
	}

	return
}

// Se ingresan los saldos de la operación, que se va a persistir.
//
// Devuelve los saldos actuales en la base de datos (antes de esta operación),
// y también como quedarían después.
func DeterminarSaldos(ctx context.Context, tx pgx.Tx, saldosOp Saldos) (antes, despues Saldos, err error) {

	antes = make(Saldos)
	despues = make(Saldos)

	query := `
SELECT monto, cantidad, monto_orig FROM saldos
WHERE 
	comitente = $1 AND
	cuenta = $2 AND
	centro = $3 AND
	persona = $4 AND
	sucursal = $5 AND
	caja = $6 AND
	producto = $7 AND
	sku = $8 AND
	deposito = $9 AND
	moneda = $10 AND
	partida_aplicada = $11 AND
	contrato = $12 AND
	empresa = $13
FOR UPDATE
;`

	for k, w := range saldosOp {
		row := tx.QueryRow(ctx, query,
			k.Comitente,
			k.Cuenta,
			k.Centro,
			byArgs(k.Persona),
			k.Sucursal,
			k.Caja,
			byArgs(k.Producto),
			byArgs(k.SKU),
			k.Deposito,
			k.Moneda,
			byArgs(k.PartidaAplicada),
			k.Contrato,
			k.Empresa,
		)
		var montoAnterior, cantidadAnterior, montoOrig int64
		err = row.Scan(&montoAnterior, &cantidadAnterior, &montoOrig)
		if err != nil {
			if err == pgx.ErrNoRows {
				// No pasa nada
			} else {
				return nil, nil, deferror.DB(err, "escaneando row")
			}
		}
		err = nil
		antes[k] = Aggregate{
			Monto:     dec.D2(montoAnterior),
			Cantidad:  dec.D4(cantidadAnterior),
			MontoOrig: dec.D4(montoOrig),
		}
		// log.Debug().Msgf("Monto anterior: %v, w.Monto=%v, cantidadAnterior=%v", dec.D2(montoAnterior), w.Monto, dec.D4(cantidadAnterior))
		despues[k] = Aggregate{
			Monto:     dec.D2(montoAnterior) + w.Monto,
			Cantidad:  dec.D4(cantidadAnterior) + w.Cantidad,
			MontoOrig: dec.D4(montoOrig) + w.MontoOrig,
		}
	}
	return
}

// Persiste los saldos ingresados en la tabla "saldos"
func FijarSaldos(ctx context.Context, tx pgx.Tx, ss Saldos) (err error) {

	upsert, deletes := queryFijarSaldos(ss)
	//log.Debug().Str("upsert query", upsert).Str("delete query", fmt.Sprint(deletes)).Msg("")

	// Upserts
	_, err = tx.Exec(ctx, upsert)
	if err != nil {
		return deferror.DB(err, "upserts")
	}

	// Deletes
	for _, v := range deletes {
		_, err = tx.Exec(ctx, v)
		if err != nil {
			return deferror.DB(err, "deletes")
		}
	}

	return
}

// Genera la sentencia SQL para dejar la tabla saldos con los
// saldos ingresados.
func queryFijarSaldos(ss Saldos) (upserts string, deletes []string) {
	uu := []string{}

	for k, v := range ss {

		// Si me da cero, directamente la borro
		if v.Monto == 0 && v.Cantidad == 0 && v.MontoOrig == 0 {
			value := fmt.Sprintf(`
			DELETE FROM SALDOS 
			WHERE (
				comitente = %v AND 
				cuenta = %v AND 
				centro = %v AND 
				persona = %v AND
				sucursal = %v AND 
				caja = %v AND
				producto = %v AND 
				sku = %v AND 
				deposito = %v AND 
				moneda = %v AND 
				partida_aplicada = %v AND
				contrato = %v AND 
				empresa = %v
				);`,
				k.Comitente,
				k.Cuenta,
				k.Centro,
				by(k.Persona),
				k.Sucursal,
				k.Caja,
				by(k.Producto),
				by(k.SKU),
				k.Deposito,
				k.Moneda,
				by(k.PartidaAplicada),
				k.Contrato,
				k.Empresa,
			)
			deletes = append(deletes, value)
			continue
		}

		// Queda con saldo => Hago un upsert
		value := fmt.Sprintf("(%v,%v,%v,%v,%v,%v,%v,%v,%v,%v,%v,%v,%v,%v,%v,%v)",
			k.Comitente,
			k.Cuenta,
			k.Centro,
			by(k.Persona),
			k.Sucursal,
			k.Caja,
			by(k.Producto),
			by(k.SKU),
			k.Deposito,
			k.Moneda,
			by(k.PartidaAplicada),
			k.Contrato,
			k.Empresa,
			int(v.Monto),
			int(v.Cantidad),
			int(v.MontoOrig),
		)
		uu = append(uu, value)
	}

	if len(uu) > 0 {
		values := strings.Join(uu, " ,\n")
		upserts = fmt.Sprintf(`
			 UPSERT INTO saldos (comitente, cuenta, centro, persona, sucursal, caja, 
				producto, sku, deposito, moneda, partida_aplicada, 
				contrato, empresa, monto, cantidad, monto_orig) VALUES %v; `, values)
	}

	return
}

func by(in uuid.UUID) string {
	if in == uuid.Nil {
		return "x''"
	} else {
		return fmt.Sprintf("x'%x'", in)
	}
}

func byArgs(id uuid.UUID) []byte {
	if id == uuid.Nil {
		return []byte{}
	}
	return id.Bytes()
}
func arrayUUID(ids []uuid.UUID) string {
	idsStr := []string{}
	for _, v := range ids {
		idsStr = append(idsStr, fmt.Sprintf("'%v'", v.String()))
	}
	return strings.Join(idsStr, ", ")
}
