package dbtest

import (
	"context"
	"fmt"

	"bitbucket.org/marcos19/one/pkg/migrations/ddl"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

func Prepare(ctx context.Context, conn *pgxpool.Pool, dbName string) error {

	{ // Borro base de datos
		q := fmt.Sprintf(`DROP DATABASE IF EXISTS test_%v;`, dbName)
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "borro base de datos")
		}
	}
	{ // Creo base de datos
		q := fmt.Sprintf(`CREATE DATABASE test_%v; USE test_%v;`, dbName, dbName)
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando base de datos")
		}
	}

	{ // Comitentes
		q := ddl.Comitentes().SQL
		//`CREATE TABLE IF NOT EXISTS comitentes (ID int PRIMARY KEY);`
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla comitentes")
		}

		_, err = conn.Exec(ctx, "UPSERT INTO comitentes (id) VALUES (1);")
		if err != nil {
			return errors.Wrap(err, "insertando comitente")
		}
	}

	{ // Empresas
		q := ddl.Empresas().SQL
		//`CREATE TABLE IF NOT EXISTS empresas (ID int PRIMARY KEY);`
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla empresas")
		}

		_, err = conn.Exec(ctx, "UPSERT INTO empresas (id) VALUES (1);")
		if err != nil {
			return errors.Wrap(err, "insertando empresa")
		}
	}

	{ // Cuentas
		q := ddl.Cuentas().SQL
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla cuentas")
		}

		_, err = conn.Exec(ctx, "UPSERT INTO cuentas (id) VALUES (1),(2),(3);")
		if err != nil {
			return errors.Wrap(err, "insertando cuenta")
		}
	}

	{ // Cajas
		q := ddl.Cajas().SQL
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla cajas")
		}
	}
	{ // Comps
		q := ddl.Comps().SQL
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla comps")
		}
	}
	{ // Centros
		q := ddl.Centros().SQL
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla centros")
		}
	}
	{ // Depositos
		q := ddl.Centros().SQL
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla centros")
		}
	}
	{ // Personas
		q := ddl.Personas().SQL
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla personas")
		}
	}
	{ // Unicidades def
		q := ddl.UnicidadesDefiniciones().SQL
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla unicidades def")
		}
	}
	{ // Unicidades
		q := ddl.Unicidades().SQL
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla unicidades")
		}
	}
	{ // Ops
		q := ddl.Ops().SQL
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla ops")
		}
	}
	{ // Partidas
		q := ddl.Partidas().SQL
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla partidas")
		}
	}
	return nil
}
