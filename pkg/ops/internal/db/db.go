package db

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/ops"
	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Busca las operaciones que se ven en Historial
func BuscarOps(ctx context.Context, conn *pgxpool.Pool, req ops.HistorialReq) (out []ops.HistorialResp, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}

	ww := []string{"comitente = $1"}
	pp := []interface{}{req.Comitente}

	// Pongo filtros
	if req.Empresa != 0 {
		pp = append(pp, req.Empresa)
		ww = append(ww, fmt.Sprintf("empresa = $%v", len(pp)))
	}

	if req.Programa != "" {
		pp = append(pp, req.Programa)
		ww = append(ww, fmt.Sprintf("programa = $%v", len(pp)))
	}
	if req.Desde != 0 {
		pp = append(pp, req.Desde.JSONString())
		ww = append(ww, fmt.Sprintf("fecha >= $%v", len(pp)))
	}
	if req.Hasta != 0 {
		pp = append(pp, req.Hasta.JSONString())
		ww = append(ww, fmt.Sprintf("fecha <= $%v", len(pp)))
	}
	if req.TranID != uuid.Nil {
		pp = append(pp, req.TranID)
		ww = append(ww, fmt.Sprintf("tran_id = $%v", len(pp)))
	}
	if req.PuntoDeVenta != 0 {
		pp = append(pp, req.PuntoDeVenta)
		ww = append(ww, fmt.Sprintf("punto_de_venta = $%v", len(pp)))
	}
	if req.NComp != 0 {
		pp = append(pp, req.NComp)
		ww = append(ww, fmt.Sprintf("n_comp = $%v", len(pp)))
	}
	if req.Comp != 0 {
		pp = append(pp, req.Comp)
		ww = append(ww, fmt.Sprintf("comp_id = $%v", len(pp)))
	}
	if req.Persona != uuid.Nil {
		pp = append(pp, req.Persona)
		ww = append(ww, fmt.Sprintf(
			"(persona = $%v OR id IN (SELECT op_id FROM partidas WHERE persona = $%v AND comitente = $1))",
			len(pp), len(pp)))
	}
	if req.Sucursal != 0 {
		pp = append(pp, req.Sucursal)
		ww = append(ww, fmt.Sprintf(
			"(sucursal = $%v OR id IN (SELECT op_id FROM partidas WHERE sucursal = $%v AND comitente = $1))",
			len(pp), len(pp)))
	}

	limit := ""
	if req.Limit == 0 {
		req.Limit = 1000
	}
	limit = fmt.Sprintf("LIMIT %v", req.Limit)

	offset := ""
	offsetInt := 0
	if req.Pagina != 0 {
		offsetInt = req.Limit*req.Pagina - req.Limit
	}
	if offsetInt != 0 {
		offset = fmt.Sprintf("OFFSET %v", offset)
	}

	query := fmt.Sprintf(`
		SELECT id, fecha, comp_nombre, n_comp_str, persona_nombre, total_comp, detalle, created_at 
		FROM ops
		WHERE %v
		ORDER BY fecha, created_at
		%v
		%v;`, strings.Join(ww, " AND "), limit, offset)

	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()
	for rows.Next() {
		v := ops.HistorialResp{}
		err = rows.Scan(&v.ID, &v.Fecha, &v.CompNombre, &v.NCompStr, &v.PersonaNombre, &v.TotalComp, &v.Detalle, &v.CreatedAt)
		if err != nil {
			return out, deferror.DB(err, "escaneando")
		}
		out = append(out, v)
	}
	return
}

func ReadOne(ctx context.Context, conn *pgxpool.Pool, req ops.ReadOneReq) (out ops.Op, err error) {

	// Valido
	if req.ID == uuid.Nil {
		return out, deferror.Validation("no se ingresó ID de op")
	}
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}
	// if req.ID != uuid.Nil && req.TranID != uuid.Nil {
	// 	return out, deferror.Validation("el filtro debe ser por ID o TranID, no ambos")
	// }
	pp := []interface{}{req.Comitente}
	ww := []string{"comitente=$1"}

	if req.ID != uuid.Nil {
		pp = append(pp, req.ID)
		ww = append(ww, "id=$2")
	}
	// if req.TranID != uuid.Nil {
	// 	pp = append(pp, req.ID)
	// 	ww = append(ww, "tran_id=$2")
	// }
	where := "WHERE " + strings.Join(ww, " AND ")
	// Busco
	query := fmt.Sprintf(`
	SELECT 
id, 
comitente,
empresa,
fecha, 
fecha_contable,
programa,
tran_id,
tran_def,
comp_item,
comp_id,
comp_nombre, 
punto_de_venta, 
n_comp, 
n_comp_str, 
persona,
persona_nombre,
sucursal,
total_comp,
detalle,
tipo_moneda,
tipo_cambio,
tipo_cotizacion,
impresion,
fecha_original,
tipo_identificacion,
n_identificacion,
condicion_fiscal,
afip_comprobante_id,
afip_comprobante_nombre,
cae,
cae_vto,
bar_code,
afip_fields,
qr,
totales_conceptos,
created_at,
updated_at,
usuario
	FROM ops
	%v
`, where)

	err = conn.QueryRow(ctx, query, pp...).Scan(
		&out.ID,
		&out.Comitente,
		&out.Empresa,
		&out.Fecha,
		&out.FechaContable,
		&out.Programa,
		&out.TranID,
		&out.TranDef,
		&out.CompItem,
		&out.CompID,
		&out.CompNombre,
		&out.PuntoDeVenta,
		&out.NComp,
		&out.NCompStr,
		&out.Persona,
		&out.PersonaNombre,
		&out.Sucursal,
		&out.TotalComp,
		&out.Detalle,
		&out.TipoMoneda,
		&out.TipoCambio,
		&out.TipoCotizacion,
		&out.Impresion,
		&out.FechaOriginal,
		&out.TipoIdentificacion,
		&out.NIdentificacion,
		&out.CondicionFiscal,
		&out.AFIPComprobanteID,
		&out.AFIPComprobanteNombre,
		&out.CAE,
		&out.CAEVto,
		&out.BarCode,
		&out.AfipFields,
		&out.QR,
		&out.TotalesConceptos,
		&out.CreatedAt,
		&out.UpdatedAt,
		&out.Usuario,
	)
	if err != nil {
		return out, deferror.DB(err, "querying op")
	}

	return
}

func ProfundizarPartida(ctx context.Context, conn *pgxpool.Pool, req ops.ProfundizarReq) (out ops.ProfundizarResponse, err error) {

	// Valido
	if req.ID == uuid.Nil {
		return out, deferror.Validation("No se ingresó el ID de partida")
	}
	if req.Comitente == 0 {
		return out, deferror.Validation("No se ingresó el ID de comitente")
	}

	// Busco la partida
	opID := uuid.UUID{}
	{
		q := `SELECT op_id FROM partidas WHERE comitente=$1 AND id=$2`
		err = conn.QueryRow(ctx, q, req.Comitente, req.ID).Scan(&opID)
		if err != nil {
			return out, deferror.DB(err, "buscando en partidas")
		}
	}

	// Busco la op
	programa := ""
	tran := uuid.UUID{}
	{
		q := `SELECT programa, tran_id FROM ops WHERE comitente=$1 AND id=$2`
		err = conn.QueryRow(ctx, q, req.Comitente, opID).Scan(&programa, &tran)
		if err != nil {
			return out, deferror.DB(err, "buscando en ops")
		}
	}

	// Armo respuesta
	out = ops.ProfundizarResponse{
		Programa: programa,
		Key:      "id",
		Value:    tran,
	}

	return
}

// HandleProfundizarOp devuelve los datos para poder redireccionar el frontend
// al comprobante que dio origen a esta partida.
func ProfundizarOp(ctx context.Context, conn *pgxpool.Pool, req ops.ProfundizarReq) (out ops.ProfundizarResponse, err error) {

	// Valido
	if req.ID == uuid.Nil {
		return out, deferror.Validation("No se ingresó el ID de la op")
	}
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó el ID del comitente")
	}

	// Busco la op
	programa := ""
	tran := uuid.UUID{}
	{
		q := `SELECT programa, tran_id FROM ops WHERE comitente=$1 AND id=$2`
		err = conn.QueryRow(ctx, q, req.Comitente, req.ID).Scan(&programa, &tran)
		if err != nil {
			return out, deferror.DB(err, "buscando en ops")
		}
	}

	// Armo respuesta
	out = ops.ProfundizarResponse{
		Programa: programa,
		Key:      "id",
		Value:    tran,
	}
	return
}

func ReadMany(ctx context.Context, conn *pgxpool.Pool, req ops.ReadManyReq) (out []ops.Op, err error) {

	// Valido
	if req.TranID == uuid.Nil {
		return out, deferror.Validation("no se ingresó ID de op")
	}
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó ID de comitente")
	}

	// Busco
	query := `
	SELECT 
		id, 
		comitente, 
		empresa, 
		fecha, 
		fecha_contable, 
		programa, 
		tran_id, 
		tran_def,
		comp_item, 
		comp_id, 
		comp_nombre, 
		punto_de_venta, 
		n_comp, 
		n_comp_str, 
		persona,	
		persona_nombre, 
		sucursal,	
		total_comp,
		detalle,
		tipo_moneda,
		tipo_cambio,
		tipo_cotizacion,
		impresion,
		fecha_original,
		tipo_identificacion,
		n_identificacion,
		condicion_fiscal,
		afip_comprobante_id,
		afip_comprobante_nombre,
		cae, 
		cae_vto, 
		bar_code, 
		afip_fields, 
		qr, 
		totales_conceptos,
		created_at,
		updated_at,		
		usuario
	FROM ops
	WHERE comitente=$1 AND tran_id=$2;
`

	rows, err := conn.Query(ctx, query, req.Comitente, req.TranID)
	if err != nil {
		return out, errors.Wrap(err, "querying ops")
	}
	defer rows.Close()
	for rows.Next() {
		o := ops.Op{}
		err = rows.Scan(
			&o.ID,
			&o.Comitente,
			&o.Empresa,
			&o.Fecha,
			&o.FechaContable,
			&o.Programa,
			&o.TranID,
			&o.TranDef,
			&o.CompItem,
			&o.CompID,
			&o.CompNombre,
			&o.PuntoDeVenta,
			&o.NComp,
			&o.NCompStr,
			&o.Persona,
			&o.PersonaNombre,
			&o.Sucursal,
			&o.TotalComp,
			&o.Detalle,
			&o.TipoMoneda,
			&o.TipoCambio,
			&o.TipoCotizacion,
			&o.Impresion,
			&o.FechaOriginal,
			&o.TipoIdentificacion,
			&o.NIdentificacion,
			&o.CondicionFiscal,
			&o.AFIPComprobanteID,
			&o.AFIPComprobanteNombre,
			&o.CAE,
			&o.CAEVto,
			&o.BarCode,
			&o.AfipFields,
			&o.QR,
			&o.TotalesConceptos,
			&o.CreatedAt,
			&o.UpdatedAt,
			&o.Usuario,
		)
		if err != nil {
			return out, deferror.DB(err, "escaneando op")
		}
		out = append(out, o)
	}

	return
}

// Devuelve las partidas contables para una op.
func Partidas(ctx context.Context, conn *pgxpool.Pool, req ops.PartidasReq) (out []ops.Partida, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó el ID de comitente")
	}
	if conn == nil {
		return out, errors.Errorf("no se ingresó conn")
	}
	if req.ID == uuid.Nil && len(req.IDs) == 0 {
		return out, deferror.Validation("no se ingresó el ID de Op de partidas")
	}

	ww := []string{"comitente = $1"}
	pp := []interface{}{req.Comitente}
	if req.ID != uuid.Nil {
		pp = append(pp, req.ID)
		ww = append(ww, fmt.Sprintf("op_id=$%v", len(pp)))
	}
	ii := []string{}
	for _, v := range req.IDs {
		ii = append(ii, fmt.Sprintf("'%v'", v.String()))
	}
	if len(ii) > 0 {
		joined := strings.Join(ii, ",")
		ww = append(ww, fmt.Sprintf("id IN (%v)", joined))
	}
	where := fmt.Sprintf("WHERE %v", strings.Join(ww, " AND "))

	// Busco
	query := fmt.Sprintf(`
		SELECT
			id, comitente, fecha_contable, fecha_financiera, time_stamp_contable, op_id,
			empresa, tran_def, comp_def, created_at, updated_at, usuario, pata, comp_nombre,
			n_comp_str,	cuenta, cuenta_nombre, centro, persona, sucursal, caja, producto,
			sku, deposito, unicidad, unicidad_tipo, contrato, concepto_iva, alicuota_iva, 
			monto, monto_moneda_original, tc, um, cantidad, partida_aplicada, detalle, 
			tipo_asiento, moneda
		FROM partidas
		%v
	`, where)
	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, errors.Wrap(err, "querying partidas")
	}
	defer rows.Close()

	for rows.Next() {
		p := ops.Partida{}
		err = rows.Scan(&p.ID, &p.Comitente, &p.FechaContable, &p.FechaFinanciera,
			&p.TimeStampContable, &p.OpID, &p.Empresa, &p.TranDef, &p.CompDef, &p.CreatedAt,
			&p.UpdatedAt, &p.Usuario, &p.Pata, &p.CompNombre, &p.NCompStr, &p.Cuenta,
			&p.CuentaNombre, &p.Centro, &p.Persona, &p.Sucursal, &p.Caja, &p.Producto,
			&p.SKU, &p.Deposito, &p.Unicidad, &p.UnicidadTipo, &p.Contrato, &p.ConceptoIVA,
			&p.AlicuotaIVA, &p.Monto, &p.MontoMonedaOriginal, &p.TC, &p.UM, &p.Cantidad, &p.PartidaAplicada,
			&p.Detalle, &p.TipoAsiento, &p.Moneda)
		if err != nil {
			return out, errors.Wrap(err, "scaneando partida")
		}
		out = append(out, p)
	}
	return
}

// InsertOp persiste las operaciones tal como están.
// Se supone que ya están procesadas y validadas
func Create(ctx context.Context, tx pgx.Tx, op *ops.Op) (err error) {

	query := `
INSERT INTO ops (

id,
comitente,
empresa,
fecha,
fecha_contable,
programa,
tran_id,
tran_def,
comp_item,
comp_id,
comp_nombre,
punto_de_venta,
n_comp,
n_comp_str,
persona,
persona_nombre,
sucursal,
total_comp,
detalle,
tipo_moneda,
tipo_cambio,
tipo_cotizacion,
impresion,
fecha_original,
tipo_identificacion,
n_identificacion,
condicion_fiscal,
afip_comprobante_id,
afip_comprobante_nombre,
cae,
cae_vto,
bar_code,
qr,
afip_fields,
totales_conceptos,
usuario
) VALUES (
	$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,
	$25,$26,$27,$28,$29,$30,$31,$32,$33,$34,$35,$36
);
`
	_, err = tx.Exec(ctx, query,
		op.ID,
		op.Comitente,
		op.Empresa,
		op.Fecha,
		op.FechaContable,
		op.Programa,
		op.TranID,
		op.TranDef,
		op.CompItem,
		op.CompID,
		op.CompNombre,
		op.PuntoDeVenta,
		op.NComp,
		op.NCompStr,
		op.Persona,
		op.PersonaNombre,
		op.Sucursal,
		op.TotalComp,
		op.Detalle,
		op.TipoMoneda,
		op.TipoCambio,
		op.TipoCotizacion,
		op.Impresion,
		op.FechaOriginal,
		op.TipoIdentificacion,
		op.NIdentificacion,
		op.CondicionFiscal,
		op.AFIPComprobanteID,
		op.AFIPComprobanteNombre,
		op.CAE,
		op.CAEVto,
		op.BarCode,
		op.QR,
		op.AfipFields,
		op.TotalesConceptos,
		op.Usuario,
	)
	if err != nil {
		return deferror.DB(err, "insertando en tabla ops")
	}

	partQuery := `
INSERT INTO partidas (
comitente,
id,
fecha_contable,
time_stamp_contable,
op_id,
empresa,
tran_def,
comp_def,
usuario,
pata,
fecha_financiera,
comp_nombre,
n_comp_str,
cuenta,
cuenta_nombre,
centro,
persona, 
sucursal,
caja,
producto,
sku,
deposito,
unicidad,
unicidad_tipo,
contrato,
concepto_iva,
alicuota_iva,
monto,
moneda,
monto_moneda_original,
tc,
um,
cantidad,
partida_aplicada,
detalle,
tipo_asiento

) VALUES (
	$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,
	$25,$26,$27,$28,$29,$30,$31,$32,$33,$34,$35,$36
)
	`
	for _, v := range op.PartidasContables {
		_, err = tx.Exec(ctx, partQuery,
			v.Comitente,
			v.ID,
			v.FechaContable,
			v.TimeStampContable,
			v.OpID,
			v.Empresa,
			v.TranDef,
			v.CompDef,
			v.Usuario,
			v.Pata,
			v.FechaFinanciera,
			v.CompNombre,
			v.NCompStr,
			v.Cuenta,
			v.CuentaNombre,
			v.Centro,
			v.Persona,
			v.Sucursal,
			v.Caja,
			v.Producto,
			v.SKU,
			v.Deposito,
			v.Unicidad,
			v.UnicidadTipo,
			v.Contrato,
			v.ConceptoIVA,
			v.AlicuotaIVA,
			v.Monto,
			v.Moneda,
			v.MontoMonedaOriginal,
			v.TC,
			v.UM,
			v.Cantidad,
			v.PartidaAplicada,
			v.Detalle,
			v.TipoAsiento,
		)
		if err != nil {
			return deferror.DBf(err,
				"insertando en tabla partidas, (id=%v, cuenta=%v, monto=%v)",
				v.ID, v.Cuenta, v.Monto,
			)
		}
	}
	return
}

// Se ingresa el ID de una unicidad y devuelve el campo PartidaAplicada
// que dio origen a esa unicidad
func GetPartidaAplicada(ctx context.Context, conn *pgxpool.Pool, comitente int, id uuid.UUID) (out uuid.UUID, err error) {

	query := `SELECT partida_aplicada 
	FROM partidas
	WHERE comitente=$1 AND unicidad=$2
	ORDER BY fecha_contable
	LIMIT 1;`

	var o *uuid.UUID
	err = conn.QueryRow(ctx, query, comitente, id).Scan(&o)
	if err != nil {
		return out, errors.Wrap(err, "querying/scanning")
	}
	if o == nil {
		return out, errors.Errorf("no se encontró PartidaAplicada ID original")
	}
	return *o, nil
}

func ExisteCompPropio(ctx context.Context, tx pgx.Tx, comitente, compID, pv, n int) (out string, err error) {

	query := `SELECT created_at 
FROM OPS
WHERE comitente = $1 AND
	comp_id = $2 AND
	punto_de_venta = $2 AND
	n_comp = $3
`
	rows, err := tx.Query(ctx, query, comitente, pv, n)
	if err != nil {
		return out, errors.Wrap(err, "querying")
	}
	defer rows.Close()
	ff := []time.Time{}
	for rows.Next() {
		var f time.Time
		err = rows.Scan(&f)
		if err != nil {
			return out, errors.Wrap(err, "scanning")
		}
		ff = append(ff, f)
	}
	if len(ff) == 0 {
		return out, nil
	}
	if len(ff) == 1 {
		return fmt.Sprintf("el comprobante ya se cargó el %v", ff[0].String()), nil
	}

	return fmt.Sprintf("el comprobante se encuentras cargado %v veces", len(ff)), nil
}

// Determina si existe cargado este comp de tercero
func ExisteCompTercero(ctx context.Context, tx pgx.Tx, comitente, empresa, cuit, idAFIP, pv, n int) (out string, err error) {
	query := `SELECT created_at 
FROM OPS
WHERE comitente = $1 AND
	empresa = $2 AND
	afip_comprobante_id = $3 AND
	punto_de_venta = $4 AND
	n_comp = $5 AND
	n_identificacion = $6
`
	rows, err := tx.Query(ctx, query, comitente, empresa, idAFIP, pv, n, cuit)
	if err != nil {
		return out, errors.Wrap(err, "querying")
	}
	defer rows.Close()
	ff := []time.Time{}
	for rows.Next() {
		var f time.Time
		err = rows.Scan(&f)
		if err != nil {
			return out, errors.Wrap(err, "scanning")
		}
		ff = append(ff, f)
	}
	if len(ff) == 0 {
		return out, nil
	}
	if len(ff) == 1 {
		return fmt.Sprintf("el comprobante ya se cargó el %v", ff[0].String()), nil
	}

	return fmt.Sprintf("el comprobante se encuentras cargado %v veces", len(ff)), nil
}
