package exister

import (
	"context"
	"errors"

	"bitbucket.org/marcos19/one/pkg/ops/internal/db"
	"github.com/jackc/pgx/v4"
)

type Exister struct {
	tx pgx.Tx
}

func NewExister(tx pgx.Tx) *Exister {
	return &Exister{tx}
}

func (e *Exister) ExistePropio(ctx context.Context,
	comitente, compID, puntoVenta, nComp int,
) error {

	msg, err := db.ExisteCompPropio(ctx, e.tx, comitente, compID, puntoVenta, nComp)
	if err != nil {
		return err
	}

	if msg != "" {
		return errors.New(msg)
	}
	return nil
}

func (e *Exister) ExisteTercero(ctx context.Context,
	comitente, empresa, cuit, idAFIP, puntoVenta, nComp int,
) error {

	msg, err := db.ExisteCompTercero(ctx, e.tx, comitente, empresa, cuit, idAFIP, puntoVenta, nComp)
	if err != nil {
		return err
	}

	if msg != "" {
		return errors.New(msg)
	}
	return nil
}
