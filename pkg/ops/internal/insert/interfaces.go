package insert

import (
	"context"
)

// Determina si en la contabilidad hay una op con estos datos.
type Exister interface {
	ExistePropio(ctx context.Context,
		comitente, compID, puntoVenta, nComp int,
	) error
	ExisteTercero(ctx context.Context,
		comitente, empresa, cuit, idAFIP, puntoVenta, nComp int,
	) error
}
