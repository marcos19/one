package insert

import (
	"context"
	"testing"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/personas"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestValidarPartida(t *testing.T) {

	ctx := context.Background()

	t.Run("falta cuenta", func(t *testing.T) {
		refundicion := false

		cr := cuentaMock{}
		pr := personaMock{}
		p := &ops.Partida{}

		err := validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "cuenta")
	})
	t.Run("falla cuenta reader", func(t *testing.T) {
		refundicion := false
		per := uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e129")

		cr := cuentaMock{err: true}
		pr := personaMock{}

		p := partidaValida()
		p.Persona = &per
		err := validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "cuenta")
	})
	t.Run("falla persona reader", func(t *testing.T) {
		refundicion := false
		per := uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e129")

		cr := cuentaMock{}
		pr := personaMock{err: true}

		p := partidaValida()
		p.Persona = &per
		err := validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "persona")
	})
	t.Run("cuenta es agrupadora", func(t *testing.T) {
		refundicion := false

		cr := cuentaMock{TipoCuenta: cuentas.TipoCuentaAgrupadora}
		pr := personaMock{}
		p := partidaValida()

		err := validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "agrupadora")
	})
	t.Run("cuenta con persona sin apertura", func(t *testing.T) {
		refundicion := false
		per := uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e129")

		cr := cuentaMock{}
		pr := personaMock{}
		p := partidaValida()
		p.Persona = &per

		err := validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "persona")
	})
	t.Run("cuenta sin persona con apertura", func(t *testing.T) {
		refundicion := false

		cr := cuentaMock{AperturaPersona: true}
		pr := personaMock{}
		p := partidaValida()

		err := validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "persona")
	})
	t.Run("falla personas.ReaderOne", func(t *testing.T) {
		refundicion := false
		per := uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e129")

		cr := cuentaMock{}
		pr := personaMock{err: true}
		p := partidaValida()
		p.Persona = &per

		err := validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "persona")
	})
	t.Run("no define sucursal", func(t *testing.T) {
		refundicion := false
		per := uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e129")

		cr := cuentaMock{AperturaPersona: true}
		pr := personaMock{sucursales: true}
		p := partidaValida()
		p.Persona = &per

		err := validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "sucursal")
	})

	t.Run("cuenta con centro sin apertura", func(t *testing.T) {
		refundicion := false

		centro := 1
		cr := cuentaMock{}
		pr := personaMock{}
		p := partidaValida()
		p.Centro = &centro

		err := validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "centro")
	})
	t.Run("cuenta sin centro con apertura", func(t *testing.T) {
		refundicion := false

		cr := cuentaMock{AperturaCentro: true}
		pr := personaMock{}
		p := partidaValida()

		err := validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "centro")
	})

	t.Run("cuenta con caja sin apertura", func(t *testing.T) {
		refundicion := false

		caja := 1
		cr := cuentaMock{}
		pr := personaMock{}
		p := partidaValida()
		p.Caja = &caja

		err := validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "caja")
	})
	t.Run("cuenta sin caja con apertura", func(t *testing.T) {
		refundicion := false

		cr := cuentaMock{AperturaCaja: true}
		pr := personaMock{}
		p := partidaValida()

		err := validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "caja")
	})

	t.Run("cuenta con depósito sin apertura", func(t *testing.T) {
		refundicion := false

		deposito := 1
		cr := cuentaMock{}
		pr := personaMock{}
		p := partidaValida()
		p.Deposito = &deposito

		err := validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "depósito")
	})
	t.Run("cuenta sin depósito con apertura", func(t *testing.T) {
		refundicion := false

		cr := cuentaMock{AperturaDeposito: true}
		pr := personaMock{}
		p := partidaValida()

		err := validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "depósito")
	})

	t.Run("cuenta con producto sin apertura", func(t *testing.T) {
		refundicion := false

		prod := uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e129")
		cr := cuentaMock{}
		pr := personaMock{}
		p := partidaValida()
		p.Producto = &prod

		err := validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "producto")
	})
	t.Run("cuenta con sku sin apertura", func(t *testing.T) {
		refundicion := false

		prod := uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e129")
		cr := cuentaMock{}
		pr := personaMock{}
		p := partidaValida()
		p.SKU = &prod

		err := validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "producto")
	})
	t.Run("cuenta sin producto con apertura", func(t *testing.T) {
		refundicion := false

		cr := cuentaMock{AperturaProducto: true}
		pr := personaMock{}
		p := partidaValida()

		err := validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "producto")
	})

	t.Run("cuenta con aplicación sin apertura", func(t *testing.T) {
		refundicion := false

		apli := uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e129")
		cr := cuentaMock{}
		pr := personaMock{}
		p := partidaValida()
		p.PartidaAplicada = &apli

		err := validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "aplicaci")
	})
	t.Run("cuenta sin aplicación con apertura", func(t *testing.T) {
		refundicion := false

		cr := cuentaMock{UsaAplicaciones: true}
		pr := personaMock{}
		p := partidaValida()

		err := validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "aplicaci")
	})

	t.Run("cuenta con moneda sin apertura", func(t *testing.T) {
		refundicion := false

		moneda := 1
		cr := cuentaMock{}
		pr := personaMock{}
		p := partidaValida()
		p.Moneda = &moneda

		err := validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "moneda")
	})
	t.Run("cuenta con TC sin apertura", func(t *testing.T) {
		refundicion := false

		tc := dec.D4(34324)
		cr := cuentaMock{}
		pr := personaMock{}
		p := partidaValida()
		p.TC = &tc

		err := validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "moneda")
	})
	t.Run("cuenta con moneda original sin apertura", func(t *testing.T) {
		refundicion := false

		tc := dec.D4(34324)
		cr := cuentaMock{}
		pr := personaMock{}
		p := partidaValida()
		p.MontoMonedaOriginal = &tc

		err := validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "moneda")
	})
	t.Run("cuenta sin moneda con apertura", func(t *testing.T) {
		refundicion := false

		cr := cuentaMock{MonedaExtranjera: true}
		pr := personaMock{}
		p := partidaValida()

		err := validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "moneda")

		moneda := 1
		p.Moneda = &moneda
		err = validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "tipo de cambio")

		tc := dec.D4(5431)
		p.TC = &tc
		err = validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.NotNil(t, err)
		assert.ErrorContains(t, err, "moneda original")

		mo := dec.D4(5431)
		p.MontoMonedaOriginal = &mo
		err = validarPartida(ctx, 1, cr, pr, p, refundicion)
		require.Nil(t, err, "%v", err)
	})
}

func TestValidarEmpresa(t *testing.T) {
	ctx := context.Background()

	t.Run("debería andar", func(t *testing.T) {
		op := opEmpresaValida()
		err := validarEmpresa(ctx, empresaMock{}, &op)
		assert.Nil(t, err)
	})
	t.Run("sin empresa", func(t *testing.T) {
		op := opEmpresaValida()
		op.Empresa = 0
		err := validarEmpresa(ctx, empresaMock{}, &op)
		assert.NotNil(t, err)
		assert.ErrorContains(t, err, "empresa")
	})
	t.Run("falla buscando empresa", func(t *testing.T) {
		op := opEmpresaValida()
		op.Empresa = 4
		err := validarEmpresa(ctx, empresaMock{err: true}, &op)
		assert.NotNil(t, err)
		assert.ErrorContains(t, err, "empresa")
	})
	t.Run("fecha anterior a la permitida", func(t *testing.T) {
		op := opEmpresaValida()
		op.Fecha = 20220101
		err := validarEmpresa(ctx, empresaMock{desde: 20220201}, &op)
		assert.NotNil(t, err)
		assert.ErrorContains(t, err, "fecha")
	})
	t.Run("fecha posterior a la permitida", func(t *testing.T) {
		op := opEmpresaValida()
		op.Fecha = 20220301
		err := validarEmpresa(ctx, empresaMock{hasta: 20220201}, &op)
		assert.NotNil(t, err)
		assert.ErrorContains(t, err, "fecha")
	})
	t.Run("fecha dentro de la permitida", func(t *testing.T) {
		op := opEmpresaValida()
		op.Fecha = 20220105
		err := validarEmpresa(ctx, empresaMock{
			desde: 20220101,
			hasta: 20220201,
		}, &op)
		assert.Nil(t, err)
	})
}

func opEmpresaValida() (out ops.Op) {
	out.ID = uuid.FromStringOrNil("ba20cad4-208d-4d23-8f52-1d7111e0e129")
	out.Empresa = 2
	out.Fecha = fecha.Fecha(20220103)
	return
}

func TestValidarComp(t *testing.T) {
	ctx := context.Background()

	t.Run("falla busqueda comp", func(t *testing.T) {
		err := validarComp(ctx, nil,
			compMock{err: true},
			existerMock{},
			&ops.Op{Comitente: 1, CompID: 1},
		)
		assert.NotNil(t, err)
	})

	t.Run("comp sin limite fecha", func(t *testing.T) {
		op := &ops.Op{
			Comitente: 1,
			CompID:    1,
			Fecha:     20221218,
		}
		err := validarComp(ctx, nil, compMock{},
			existerMock{},
			op)
		assert.Nil(t, err)
	})
	t.Run("op dentro de fecha limite", func(t *testing.T) {
		op := &ops.Op{
			Comitente: 1,
			CompID:    1,
			Fecha:     20221218,
		}
		err := validarComp(ctx, nil, compMock{desde: 20220101, hasta: 20221231},
			existerMock{},
			op)
		assert.Nil(t, err)
	})
	t.Run("op anterior fecha limite", func(t *testing.T) {
		op := &ops.Op{
			Comitente: 1,
			CompID:    1,
			Fecha:     20211231,
		}
		err := validarComp(ctx, nil, compMock{desde: 20220101, hasta: 20221231},
			existerMock{},
			op)
		assert.NotNil(t, err)
		assert.ErrorContains(t, err, "fecha")
	})
	t.Run("op posterior fecha limite", func(t *testing.T) {
		op := &ops.Op{
			Comitente: 1,
			CompID:    1,
			Fecha:     20231231,
		}
		err := validarComp(ctx, nil, compMock{desde: 20220101, hasta: 20221231},
			existerMock{},
			op)
		assert.NotNil(t, err)
		assert.ErrorContains(t, err, "fecha")
	})
	t.Run("LibroIVA: sin id AFIP", func(t *testing.T) {
		op := &ops.Op{
			Comitente: 1,
			CompID:    1,
		}
		err := validarComp(ctx, nil, compMock{VaAlLibroDeIVA: true},
			existerMock{},
			op)
		assert.NotNil(t, err)
	})
	t.Run("LibroIVA: id AFIP cero", func(t *testing.T) {
		op := &ops.Op{
			Comitente: 1,
			CompID:    1,
		}
		op.AFIPComprobanteID = new(int)
		err := validarComp(ctx, nil, compMock{VaAlLibroDeIVA: true},
			existerMock{},
			op)
		assert.NotNil(t, err)

	})
	t.Run("LibroIVA: con id AFIP", func(t *testing.T) {
		op := &ops.Op{
			Comitente: 1,
			CompID:    1,
		}
		id := 1
		op.AFIPComprobanteID = &id
		op.NIdentificacion = &id
		err := validarComp(ctx, nil,
			compMock{VaAlLibroDeIVA: true},
			existerMock{},
			op)
		assert.Nil(t, err)
	})
	t.Run("LibroIVA: con id AFIP sin identificacion", func(t *testing.T) {
		op := &ops.Op{
			Comitente: 1,
			CompID:    1,
		}
		id := 1
		op.AFIPComprobanteID = &id
		err := validarComp(ctx, nil,
			compMock{VaAlLibroDeIVA: true},
			existerMock{},
			op)
		assert.NotNil(t, err)
	})
	t.Run("LibroIVA: comp propio ya registrado", func(t *testing.T) {
		op := &ops.Op{
			Comitente: 1,
			CompID:    1,
		}
		id := 1
		op.AFIPComprobanteID = &id
		err := validarComp(ctx, nil,
			compMock{
				VaAlLibroDeIVA: true,
				Emisor:         comps.EmisorPropiaEmpresa,
			},
			existerMock{errPropio: errors.Errorf("ya existía mock")},
			op)
		assert.NotNil(t, err)
	})
	t.Run("LibroIVA: comp tercero ya registrado", func(t *testing.T) {
		op := &ops.Op{
			Comitente: 1,
			CompID:    1,
		}
		id := 1
		op.AFIPComprobanteID = &id
		err := validarComp(ctx, nil,
			compMock{
				VaAlLibroDeIVA: true,
				Emisor:         comps.EmisorTercero,
			},
			existerMock{errTerc: errors.Errorf("ya existía mock")},
			op)
		assert.NotNil(t, err)
	})
}

type compMock struct {
	err                 bool
	desde               fecha.Fecha
	hasta               fecha.Fecha
	VaAlLibroDeIVA      bool
	CompraOVenta        string
	LibroIVA            string // Compras o Ventas
	ComprobanteFijo     bool   // Si es true, automáticamente le pega el código de original
	OriginalesAceptados tipos.GrupoInts
	Emisor              comps.Emisor
}

func (m compMock) ReadOneTx(context.Context, pgx.Tx, comps.ReadOneReq) (out comps.Comp, err error) {
	if m.err {
		return out, errors.Errorf("mock test")
	}
	out.ID = 1
	out.Nombre = "CompNombre"
	out.FechaDesde = &m.desde
	out.FechaHasta = &m.hasta
	out.VaAlLibroDeIVA = m.VaAlLibroDeIVA
	out.CompraOVenta = m.CompraOVenta
	out.LibroIVA = m.LibroIVA
	out.ComprobanteFijo = m.ComprobanteFijo
	out.OriginalesAceptados = m.OriginalesAceptados
	return out, nil
}

type existerMock struct {
	errPropio error
	errTerc   error
}

func (m existerMock) ExistePropio(context.Context, int, int, int, int) error {
	return m.errPropio
}
func (m existerMock) ExisteTercero(context.Context, int, int, int, int, int, int) error {
	return m.errTerc
}

type exTerceroMock struct {
	err error
}

func (m exTerceroMock) Existe(context.Context, int, int, int, int, int, int) error {
	return m.err
}

type empresaMock struct {
	err   bool
	desde fecha.Fecha
	hasta fecha.Fecha
}

func (e empresaMock) ReadOne(context.Context, empresas.ReadOneReq) (out empresas.Empresa, err error) {

	if e.err {
		return out, errors.Errorf("mock error")
	}
	out.ID = 1
	out.Nombre = "Empresa nombre"
	out.FechaDesde = &e.desde
	out.FechaHasta = &e.hasta
	return
}

func partidaValida() *ops.Partida {
	return &ops.Partida{
		Cuenta: 1,
		Monto:  1000,
	}
}

type cuentaMock struct {
	centro           bool
	AperturaPersona  bool
	AperturaProducto bool
	AperturaCaja     bool
	AperturaCentro   bool
	AperturaContrato bool
	AperturaDeposito bool
	AperturaUnicidad bool
	MonedaExtranjera bool
	UsaAplicaciones  bool

	TipoUnicidad *int `json:",string"`

	TipoCuenta cuentas.TipoCuentaContable
	err        bool
}

func (c cuentaMock) ReadOne(context.Context, cuentas.ReadOneReq) (cuentas.Cuenta, error) {
	if c.err {
		return cuentas.Cuenta{}, errors.Errorf("test error")
	}
	return cuentas.Cuenta{
		AperturaCaja:     c.AperturaCaja,
		AperturaPersona:  c.AperturaPersona,
		AperturaProducto: c.AperturaProducto,
		AperturaDeposito: c.AperturaDeposito,
		AperturaContrato: c.AperturaContrato,
		AperturaUnicidad: c.AperturaUnicidad,
		AperturaCentro:   c.AperturaCentro,
		TipoCuenta:       c.TipoCuenta,
		UsaAplicaciones:  c.UsaAplicaciones,
		MonedaExtranjera: c.MonedaExtranjera,
	}, nil
}

type personaMock struct {
	sucursales bool
	err        bool
}

func (p personaMock) ReadOne(context.Context, personas.ReadOneReq) (personas.Persona, error) {

	if p.err {
		return personas.Persona{}, errors.Errorf("test error")
	}
	return personas.Persona{UsaSucursales: p.sucursales}, nil
}
