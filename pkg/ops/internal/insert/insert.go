package insert

import (
	"context"
	"time"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/cuentas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/ops/internal/db"
	"bitbucket.org/marcos19/one/pkg/ops/internal/exister"
	"bitbucket.org/marcos19/one/pkg/ops/internal/saldos"
	"bitbucket.org/marcos19/one/pkg/personas"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/crosslogic/niler"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
)

type Inserter struct {
	conn *pgxpool.Pool

	compReader        comps.ReaderOneTx
	compNum           comps.Numerador
	cuentaReader      cuentas.ReaderOne
	empresaReader     empresas.ReaderOne
	personaReader     personas.ReaderOne
	bloqueadorCuentas ops.BloqueoCuenta
}

func NewInserter(
	conn *pgxpool.Pool,
	comp comps.ReaderOneTx,
	compNum comps.Numerador,
	cuenta cuentas.ReaderOne,
	empresa empresas.ReaderOne,
	persona personas.ReaderOne,
	bloq ops.BloqueoCuenta,
) (*Inserter, error) {

	if conn == nil {
		return nil, errors.Errorf("conn no puede ser nil")
	}
	if niler.IsNil(comp) {
		return nil, errors.Errorf("comps.ReaderOne no puede ser nil")
	}
	if niler.IsNil(compNum) {
		return nil, errors.Errorf("comps.Numerador no puede ser nil")
	}
	if niler.IsNil(cuenta) {
		return nil, errors.Errorf("cuentas.ReaderOne no puede ser nil")
	}
	if niler.IsNil(empresa) {
		return nil, errors.Errorf("empresas.ReaderOne no puede ser nil")
	}
	if niler.IsNil(persona) {
		return nil, errors.Errorf("persona.ReaderOne no puede ser nil")
	}
	if niler.IsNil(bloq) {
		return nil, errors.Errorf("ops.BloqueoCuenta puede ser nil")
	}

	return &Inserter{
		conn:              conn,
		compReader:        comp,
		compNum:           compNum,
		cuentaReader:      cuenta,
		empresaReader:     empresa,
		personaReader:     persona,
		bloqueadorCuentas: bloq,
	}, nil
}

// InsertOps valida, completa y persiste Op y Partidas.
func (h *Inserter) Insert(ctx context.Context, tx pgx.Tx, oo []*ops.Op, cfg *ops.InsertConfig) (err error) {

	// Valido
	err = h.Validate(ctx, tx, oo, cfg)
	if err != nil {
		return err
	}

	// Traigo saldos
	ss, err := saldos.TraerSaldos(ctx, tx, oo)
	if err != nil {
		return err
	}

	// Acá haría los controles de saldos necesarios
	//
	//
	//
	//

	// Inserto las operaciones
	for _, v := range oo {
		err = db.Create(ctx, tx, v)
		if err != nil {
			return deferror.DB(err, "insertando ops")
		}
	}

	// Actualizo saldos
	if len(ss) > 0 {
		err = saldos.FijarSaldos(ctx, tx, ss)
		if err != nil {
			return errors.Wrap(err, "actualizando saldos")
		}
	}

	return
}

// Valida y realiza cambios propios de la validación
func (h *Inserter) Validate(ctx context.Context, tx pgx.Tx, oo []*ops.Op, cfg *ops.InsertConfig) (err error) {

	if len(oo) == 0 {
		return errors.Errorf("no se ingresó ninguna op")
	}

	c := ops.InsertConfig{}
	if cfg != nil {
		c = *cfg
	}

	// Pongo últimos números
	err = numerarOps(ctx, tx, oo, h.compReader, h.compNum)
	if err != nil {
		return errors.Wrap(err, "numerando")
	}

	for _, v := range oo {
		err = h.validarOp(ctx, tx, v, c.AsientoEspecial)
		if err != nil {
			return errors.Wrapf(err, "validando %s %s", v.CompNombre, v.NCompStr)
		}
	}

	return nil
}

func numerarOps(ctx context.Context, tx pgx.Tx, oo []*ops.Op, cr comps.ReaderOneTx, num comps.Numerador) error {

	comitente := 0
	cuantos := map[int]int{}
	for _, v := range oo {
		if comitente == 0 {
			comitente = v.Comitente
		}
		cuantos[v.CompID]++
	}
	ids := tipos.GrupoInts{}
	for k := range cuantos {
		ids = append(ids, k)
	}

	log.Debug().Msg("trayendo ultimos numeros...")
	ultimos, err := num.UltimosNumeros(ctx, tx, comitente, ids)
	if err != nil {
		return errors.Wrap(err, "determinando últimos números usados")
	}
	for _, v := range oo {

		// Busco comp
		log.Debug().Msg("buscando comp...")
		comp, err := cr.ReadOneTx(ctx, tx, comps.ReadOneReq{Comitente: v.Comitente, ID: v.CompID})
		if err != nil {
			return errors.Wrap(err, "buscando comp")
		}

		// Pego NComp
		ultimo, estaba := ultimos[v.CompID]
		if estaba {
			v.NComp = ultimo + 1
			ultimos[v.CompID]++
		}

		// Nombre comp
		v.CompNombre = comp.Nombre

		// Punto de venta
		err = pegarPuntoVenta(ctx, v, &comp)
		if err != nil {
			return err
		}

		// NCompStr
		v.NCompStr, err = ops.FormatComp(v.PuntoDeVenta, v.NComp, comp.FormatoNumeracion)
		if err != nil {
			return errors.Wrap(err, "formateando numeración")
		}
		log.Debug().Msgf("Se formateó %v (%v-%v)", v.NCompStr, v.PuntoDeVenta, v.NComp)
	}

	// A los que usan numeración consecutiva la avanzo.
	err = num.AvanzarNumeradores(ctx, tx, comitente, ultimos)
	if err != nil {
		return errors.Wrap(err, "avanzando numeradores")
	}

	return nil
}

func pegarPuntoVenta(ctx context.Context, o *ops.Op, comp *comps.Comp) error {

	// Lo define comp: lo pego
	compDefinePV := comp.PuntoDeVenta != nil
	if compDefinePV {
		o.PuntoDeVenta = *comp.PuntoDeVenta
		return nil
	}

	switch comp.PuntoDeVentaObligatorio {

	case comps.ObligatorioNo:

	case comps.ObligatorioSi:
		if o.PuntoDeVenta == 0 {
			return errors.Errorf("el comprobante exige punto de venta, pero la op no tiene definido ninguno")
		}

	case comps.ObligatorioOriginal:
		if o.AFIPComprobanteID == nil {
			return errors.Errorf("el comprobante está configurado que obligue punto de venta de acuerdo al comprobante fiscal, pero el la operación no tiene definida comprobante fiscal")
		}
		if originalObliga(*o.AFIPComprobanteID) && o.PuntoDeVenta == 0 {
			return errors.Errorf("el comprobante fiscal exige punto de venta, pero la op no tiene definido ninguno")
		}
	default:
		return errors.Errorf("comprobante no define tipo de obligatoriedad")
	}

	return nil
}

func originalObliga(id int) bool {
	switch id {
	case 90, 99:
		return false
	}
	return true
}
func (h *Inserter) validarOp(ctx context.Context, tx pgx.Tx, op *ops.Op, refundicion bool) (err error) {

	if op.Fecha == 0 {
		return deferror.Validationf("no se determinó fecha contable")
	}
	if op.ID == uuid.Nil {
		return deferror.Validationf("no se determinó ID")
	}
	if op.Empresa == 0 {
		return deferror.Validationf("no se determinó empresa")
	}

	// Comp
	err = validarComp(ctx, tx, h.compReader, exister.NewExister(tx), op)
	if err != nil {
		return err
	}

	// Empresa
	err = validarEmpresa(ctx, h.empresaReader, op)
	if err != nil {
		return err
	}

	// Controlo partidas
	sum := dec.D2(0)
	cuentasUsadas := map[int]struct{}{}
	for i := range op.PartidasContables {
		sum += op.PartidasContables[i].Monto

		llenarPartida(op, &op.PartidasContables[i])

		err = validarPartida(ctx, op.Comitente, h.cuentaReader, h.personaReader, &op.PartidasContables[i], refundicion)
		if err != nil {
			return errors.Wrapf(err, "validando partida %v", i+1)
		}

		cuentasUsadas[op.PartidasContables[i].Cuenta] = struct{}{}
	}

	// Partida doble?
	if sum != 0 {
		return deferror.Validationf(
			"no cierra la partida doble del comprobante %v %v - %v. Diferencia: %v",
			op.CompNombre, op.NCompStr, op.Detalle, sum)
	}

	// Hay alguna cuenta bloqueada?
	ctas := tipos.GrupoInts{}
	for k := range cuentasUsadas {
		ctas = append(ctas, k)
	}
	if len(ctas) > 0 {
		log.Debug().Msg("Analizando bloqueo cuenta...")
		err = h.bloqueadorCuentas.Puede(ctx, op.Fecha, op.Comitente, op.Empresa, ctas)
		if err != nil {
			return errors.Wrap(err, "analizando bloqueo de cuentas")
		}
	}

	return
}

func validarComp(
	ctx context.Context,
	tx pgx.Tx,
	r comps.ReaderOneTx,
	exister Exister,
	op *ops.Op,
) error {

	// Busco comp
	compReq := comps.ReadOneReq{
		Comitente: op.Comitente,
		ID:        op.CompID,
	}
	comp, err := r.ReadOneTx(ctx, tx, compReq)
	if err != nil {
		return errors.Wrap(err, "buscando comp")
	}

	// Fecha comprobante
	if comp.FechaDesde != nil {
		if *comp.FechaDesde != 0 {
			if op.Fecha < *comp.FechaDesde {
				return deferror.Validationf("el comprobante %v no está habilitada para registrar operaciones con fecha anterior a %v", comp.Nombre, comp.FechaDesde)
			}
		}
	}
	if comp.FechaHasta != nil {
		if *comp.FechaHasta != 0 {
			if op.Fecha > *comp.FechaHasta {
				return deferror.Validationf("la empresa %v no está habilitada para registrar operaciones con fecha posterior a %v", comp.Nombre, comp.FechaHasta)
			}
		}
	}

	// Comprobante original
	if comp.ComprobanteFijo && len(comp.OriginalesAceptados) == 1 {
		op.AFIPComprobanteID = new(int)
		*op.AFIPComprobanteID = comp.OriginalesAceptados[0]
	}

	// // Consumidor final
	// if op.NIdentificacion == nil {
	// 	goto afipOK
	// }
	// if *op.NIdentificacion == 0 {
	// 	goto afipOK
	// }

	// Comprobante AFIP
	if comp.VaAlLibroDeIVA {
		if op.AFIPComprobanteID == nil {
			return deferror.Validation("no se definió el tipo de comprobante fiscal")
		} else {
			if *op.AFIPComprobanteID == 0 {
				return deferror.Validation("el tipo de comprobante fiscal no puede ser cero")
			}
		}

		if op.NIdentificacion == nil {
			return deferror.Validation("no se definió el tipo de comprobante fiscal")
		}

		switch comp.Emisor {

		case comps.EmisorPropiaEmpresa:
			err = exister.ExistePropio(ctx, op.Comitente, comp.ID, op.PuntoDeVenta, op.NComp)

		case comps.EmisorTercero:
			err = exister.ExisteTercero(ctx, op.Comitente, comp.Empresa, *op.NIdentificacion, *op.AFIPComprobanteID, op.PuntoDeVenta, op.NComp)
		}
		if err != nil {
			return err
		}

	}

	return nil
}

func validarEmpresa(ctx context.Context, r empresas.ReaderOne, op *ops.Op) error {

	if op.Empresa == 0 {
		return errors.Errorf("op no contiene ID de empresa")
	}
	// Empresa habilitada para la fecha?
	emp, err := r.ReadOne(ctx, empresas.ReadOneReq{
		Comitente: op.Comitente,
		ID:        op.Empresa,
	})
	if err != nil {
		return errors.Wrap(err, "buscando empresa para determinar rangos de fechas permitidos")
	}
	if emp.FechaDesde != nil {
		if *emp.FechaDesde != 0 && op.Fecha < *emp.FechaDesde {
			return deferror.Validationf("la empresa %v no está habilitada para registrar operaciones con fecha anterior a %v", emp.Nombre, emp.FechaDesde)
		}
	}
	if emp.FechaHasta != nil {
		if *emp.FechaHasta != 0 && op.Fecha > *emp.FechaHasta {
			return deferror.Validationf("la empresa %v no está habilitada para registrar operaciones con fecha posterior a %v", emp.Nombre, emp.FechaHasta)
		}
	}

	return nil
}

func validarPartida(
	ctx context.Context,
	comitente int,
	cr cuentas.ReaderOne,
	pr personas.ReaderOne,
	p *ops.Partida,
	esRefundicion bool,
) error {

	if p.Cuenta == 0 {
		return deferror.Validationf("partida sin cuenta contable")
	}

	// Busco cuenta
	c, err := cr.ReadOne(ctx, cuentas.ReadOneReq{
		Comitente: comitente,
		ID:        p.Cuenta,
	})
	if err != nil {
		return errors.Wrapf(err, "buscando cuenta %v", p.Cuenta)
	}

	// Es imputable?
	if c.TipoCuenta == cuentas.TipoCuentaAgrupadora {
		return deferror.Validationf("la cuenta %v es agrupadora. No puede usarse para contabilizar.", c.Nombre)
	}

	// Está activa?
	if c.Inactiva {
		return deferror.Validationf("la cuenta %v se encuentra inactivada", c.Nombre)
	}

	// En refundición y cierre esto no se controla
	if esRefundicion {
		return nil
	}

	// Centros
	if c.AperturaCentro && p.Centro == nil {
		return deferror.Validationf("no se definió centro de imputación para la cuenta %v", c.Nombre)
	}
	if !c.AperturaCentro && p.Centro != nil {
		return deferror.Validationf("la partida tiene centro, pero la cuenta %v no abre por centros", c.Nombre)
	}

	// Contratos
	if c.AperturaContrato && p.Contrato == nil {
		return deferror.Validationf("no se definió Contrato para la cuenta %v", c.Nombre)
	}
	if !c.AperturaContrato && p.Contrato != nil {
		return deferror.Validationf("la partida tiene definido contrato, pero la cuenta %v no abre por contratos", c.Nombre)
	}

	// Productos
	if c.AperturaProducto && p.Producto == nil {
		return deferror.Validationf("no se definió producto para la cuenta %v", c.Nombre)
	}
	if !c.AperturaProducto && p.Producto != nil {
		return deferror.Validationf("la partida define producto, pero la cuenta %v no usa apertura por producto", c.Nombre)
	}
	if c.AperturaProducto && p.SKU == nil {
		return deferror.Validationf("no se definió SKU para la cuenta: %v", c.Nombre)
	}
	if !c.AperturaProducto && p.SKU != nil {
		return deferror.Validationf("la partida define SKU, pero la cuenta %v no usa apertura por producto", c.Nombre)
	}

	// Caja
	if c.AperturaCaja && p.Caja == nil {
		return deferror.Validationf("no se definió la caja para la cuenta %v", c.Nombre)
	}
	if !c.AperturaCaja && p.Caja != nil {
		return deferror.Validationf("la partida define caja, pero la cuenta %v no abre por caja", c.Nombre)
	}

	// Deposito
	if c.AperturaDeposito && p.Deposito == nil {
		return deferror.Validationf("no se definió el depósito para la cuenta %v", c.Nombre)
	}
	if !c.AperturaDeposito && p.Deposito != nil {
		return deferror.Validationf("la partida define depósito, pero la cuenta %v no abre por depósitos", c.Nombre)
	}

	// Aplicaciones
	if c.UsaAplicaciones && p.PartidaAplicada == nil {
		return deferror.Validationf("la cuenta %v usa aplicaciones pero la partida no tenía definida aplicación", c.Nombre)
	}
	if !c.UsaAplicaciones && p.PartidaAplicada != nil {
		return deferror.Validationf("la partida tiene partida aplicación pero la cuenta %v no abre por aplicación", c.Nombre)
	}

	// Moneda extranjera
	if c.MonedaExtranjera {
		if p.Moneda == nil {
			return deferror.Validationf("no se definió moneda para la cuenta %v", c.Nombre)
		}
		if p.TC == nil {
			return deferror.Validationf("no se definió tipo de cambio para la cuenta %v", c.Nombre)
		}
		if p.MontoMonedaOriginal == nil {
			return deferror.Validationf("no se definió monto moneda original para cuenta %v", c.Nombre)
		}
	}
	if !c.MonedaExtranjera {
		if p.Moneda != nil {
			return deferror.Validationf("la partida define moneda, pero la cuenta '%v' no abre por moneda extranjera", c.Nombre)
		}
		if p.TC != nil {
			return deferror.Validationf("la partida define tipo de cambio, pero la cuenta '%v' no abre por moneda extranjera", c.Nombre)
		}
		if p.MontoMonedaOriginal != nil {
			return deferror.Validationf("no se definió monto moneda original para cuenta %v", c.Nombre)
		}
	}

	if c.AperturaPersona && p.Persona == nil {
		return deferror.Validationf("no se definió persona para la cuenta %v en op", c.Nombre)
	}
	if !c.AperturaPersona && p.Persona != nil {
		return deferror.Validationf("la partida define persona, pero la cuenta '%v' no tiene apertura por persona", c.Nombre)
	}
	// Tiene sucursal definida?
	if p.Persona != nil {
		per, err := pr.ReadOne(ctx, personas.ReadOneReq{
			Comitente: comitente,
			ID:        *p.Persona,
		})
		if err != nil {
			return deferror.Validationf("buscando persona id=%v", *p.Persona)
		}
		if per.UsaSucursales && p.Sucursal == nil {
			return deferror.Validationf("la persona ID=%v usa sucursales pero no se determinó ninguna", per.Nombre)
		}
	}

	return nil
}

func llenarPartida(op *ops.Op, p *ops.Partida) {

	// Copio datos de op a partida
	p.OpID = op.ID
	p.Comitente = op.Comitente
	p.Empresa = op.Empresa
	p.TranDef = op.TranDef
	p.CompDef = op.CompID
	p.FechaContable = op.Fecha
	p.TimeStampContable = op.FechaContable
	p.CompNombre = op.CompNombre
	p.NCompStr = op.NCompStr
	if p.FechaFinanciera == 0 {
		p.FechaFinanciera = p.FechaContable
	}

	// Corrección fechas
	hoy := fecha.NewFechaFromTime(time.Now())
	switch {
	case op.Fecha == hoy:
		// Si la operación es con la fecha del día, la fecha contable es time.Now()
		op.FechaContable = time.Now()
		// log.Debug().Str("hoy", hoy.String()).Str("fecha", w.Fecha.String()).Msg("nueva fecha contable" + w.FechaContable.String())
	case op.Fecha > hoy:
		// Si es una operación con fecha hacia adelante lo pongo a las 00:1
		op.FechaContable = op.Fecha.Time()
		// log.Debug().Str("hoy", hoy.String()).Str("fecha", w.Fecha.String()).Msg("nueva fecha contable" + w.FechaContable.String())
	case op.Fecha < hoy:
		// Si es una operación con fecha hacia atrás la pongo a las 23:59
		op.FechaContable = op.Fecha.Time().Add(-time.Second)
		// log.Debug().Str("hoy", hoy.String()).Str("fecha", w.Fecha.String()).Msg("nueva fecha contable" + w.FechaContable.String())
	}
}

//func ncompstr(mask string, pv int, ncomp int) string {

// // Formateo NCompStr
// if mask == "" {
// mask = "PPPP-NNNNNNNN"
// }
// f.Ops[i].NCompStr, err = formatComp(f.Ops[i].PuntoDeVenta, f.Ops[i].NComp, formato)
// if err != nil {
// return errors.Wrap(err, "uniendo punto de venta y número de comprobante")
// }
// }
