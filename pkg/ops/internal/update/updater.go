package update

import (
	"context"
	"fmt"
	"strings"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"bitbucket.org/marcos19/one/pkg/oplog"
	"bitbucket.org/marcos19/one/pkg/ops"
	"bitbucket.org/marcos19/one/pkg/ops/internal/db"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/crosslogic/niler"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

type Updater struct {
	conn          *pgxpool.Pool
	comp          comps.ReaderOneTx
	empresa       empresas.ReaderOne
	opReaderOne   ops.ReaderOne
	bloqueoCuenta ops.BloqueoCuenta
}

func New(
	conn *pgxpool.Pool,
	comp comps.ReaderOneTx,
	emp empresas.ReaderOne,
	opR ops.ReaderOne,
	bloq ops.BloqueoCuenta,
) (out *Updater, err error) {

	if niler.IsNil(conn) {
		return out, errors.Errorf("conn no puede ser nil")
	}
	if niler.IsNil(comp) {
		return out, errors.Errorf("comps.ReaderOne no puede ser nil")
	}
	if niler.IsNil(emp) {
		return out, errors.Errorf("empresas.ReaderOne no puede ser nil")
	}
	if niler.IsNil(opR) {
		return out, errors.Errorf("ops.ReaderOne no puede ser nil")
	}
	if niler.IsNil(bloq) {
		return out, errors.Errorf("ops.BloqueoCuenta no puede ser nil")
	}

	out = &Updater{
		conn:          conn,
		comp:          comp,
		empresa:       emp,
		opReaderOne:   opR,
		bloqueoCuenta: bloq,
	}
	return
}

// Trabaja por op, no por fact.
func (h *Updater) ModificarDatos(ctx context.Context, req ops.ModificarDatosReq) (err error) {
	if req.ID == uuid.Nil {
		return errors.Errorf("no se ingresó ID de fact")
	}
	if req.Comitente == 0 {
		return errors.Errorf("no se ingresó ID de comitente")
	}
	if req.Usuario == "" {
		return errors.Errorf("no se ingresó usuario")
	}

	// Busco op
	anterior, err := h.opReaderOne.ReadOne(ctx, ops.ReadOneReq{
		Comitente: req.Comitente,
		ID:        req.ID,
	})
	if err != nil {
		return errors.Wrap(err, "buscando op a modificar")
	}

	// Busco partidas
	partidas, err := db.Partidas(ctx, h.conn, ops.PartidasReq{
		Comitente: req.Comitente,
		ID:        req.ID,
	})
	if err != nil {
		return errors.Wrap(err, "buscando partidas")
	}

	cambios := []string{}
	{ // Determino cambios
		if req.PuntoDeVenta != nil {
			if *req.PuntoDeVenta != anterior.PuntoDeVenta {
				cambios = append(cambios, fmt.Sprintf("Punto de venta: %v => %v", anterior.PuntoDeVenta, *req.PuntoDeVenta))
			}
		}
		if req.NComp != anterior.NComp {
			cambios = append(cambios, fmt.Sprintf("NComp: %v => %v", anterior.NComp, req.NComp))
		}
		if req.FechaOriginal != anterior.FechaOriginal {
			cambios = append(cambios, fmt.Sprintf("Fecha original: %v => %v", anterior.FechaOriginal, req.FechaOriginal))
		}
		if req.FechaContable != anterior.Fecha {
			cambios = append(cambios, fmt.Sprintf("Fecha contable: %v => %v", anterior.Fecha, req.FechaContable))
		}
		if req.AFIPComprobanteID != nil {
			var antAFIP = 0
			if anterior.AFIPComprobanteID != nil {
				antAFIP = *anterior.AFIPComprobanteID
			}
			if *req.AFIPComprobanteID != antAFIP {
				cambios = append(cambios, fmt.Sprintf("AFIP comprobante: %v => %v", antAFIP, *req.AFIPComprobanteID))
			}
		}
	}

	// Puedo modificar fecha contabilidad
	// Empresa habilitada para la fecha?
	emp, err := h.empresa.ReadOne(ctx, empresas.ReadOneReq{
		Comitente: req.Comitente,
		ID:        anterior.Empresa,
	})
	if err != nil {
		return errors.Wrap(err, "buscando empresa para determinar rangos de fechas permitidos")
	}
	if emp.FechaDesde != nil {
		if anterior.Fecha < *emp.FechaDesde {
			return deferror.Validationf("la empresa %v no está habilitada para registrar operaciones con fecha anterior a %v", emp.Nombre, emp.FechaDesde)
		}
		if req.FechaContable < *emp.FechaDesde {
			return deferror.Validationf("la empresa %v no está habilitada para registrar operaciones con fecha anterior a %v", emp.Nombre, emp.FechaDesde)
		}
	}
	if emp.FechaHasta != nil {
		if anterior.Fecha > *emp.FechaHasta {
			return deferror.Validationf("la empresa %v no está habilitada para registrar operaciones con fecha posterior a %v", emp.Nombre, emp.FechaHasta)
		}
		if req.FechaContable > *emp.FechaHasta {
			return deferror.Validationf("la empresa %v no está habilitada para registrar operaciones con fecha posterior a %v", emp.Nombre, emp.FechaHasta)
		}
	}
	ctasUsadas := map[int]struct{}{}
	for _, v := range partidas {
		ctasUsadas[v.Cuenta] = struct{}{}
	}

	// Hay alguna cuenta bloqueada?
	ctas := tipos.GrupoInts{}
	for k := range ctasUsadas {
		ctas = append(ctas, k)
	}
	if len(ctas) > 0 {
		err := h.bloqueoCuenta.Puede(ctx, anterior.Fecha, anterior.Comitente, anterior.Empresa, ctas)
		if err != nil {
			return errors.Wrap(err, "bloqueo de cuentas")
		}
	}

	// Inicio transacción
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return errors.Wrap(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)

	// Comp
	comp, err := h.comp.ReadOneTx(ctx, tx, comps.ReadOneReq{
		Comitente: req.Comitente,
		ID:        anterior.CompID,
	})
	if err != nil {
		return errors.Wrap(err, "buscando comp")
	}
	if comp.FechaDesde != nil {
		if anterior.Fecha < *comp.FechaDesde {
			return deferror.Validationf("el comprobante no está habilitada para registrar operaciones con fecha anterior a %v", comp.FechaDesde)
		}
		if req.FechaContable < *comp.FechaDesde {
			return deferror.Validationf("el comprobante no está habilitada para registrar operaciones con fecha anterior a %v", comp.FechaDesde)
		}
	}
	if comp.FechaHasta != nil {
		if anterior.Fecha > *comp.FechaHasta {
			return deferror.Validationf("el comprobante no está habilitada para registrar operaciones con fecha posterior a %v", comp.FechaHasta)
		}
		if req.FechaContable > *comp.FechaHasta {
			return deferror.Validationf("el comprobante no está habilitada para registrar operaciones con fecha posterior a %v", comp.FechaHasta)
		}
	}
	formato := comp.FormatoNumeracion
	if formato == "" {
		formato = "PPPP-NNNNNNNN"
	}
	nCompStr, err := ops.FormatComp(*req.PuntoDeVenta, req.NComp, formato)
	if err != nil {
		return errors.Wrap(err, "uniendo punto de venta y número de comprobante")
	}

	{ // Modifico Ops

		query := `UPDATE ops SET 
			fecha = $3,
			afip_comprobante_id = $4,
			punto_de_venta = $5,
			n_comp = $6,
			n_comp_str = $7,
			fecha_original = $8
			WHERE comitente = $1 AND id = $2;
	`

		_, err = tx.Exec(ctx, query, req.Comitente, req.ID,
			req.FechaContable,
			req.AFIPComprobanteID,
			req.PuntoDeVenta,
			req.NComp,
			nCompStr,
			req.FechaOriginal,
		)
		if err != nil {
			return errors.Wrap(err, "modificando partidas")
		}
	}

	{ // Modifico partidas
		// Si todos los vencimientos de las partidas eran igual al
		// de la op, los cambio también

		fch := ""
		for _, v := range partidas {
			if v.FechaFinanciera != v.FechaContable {
				break
			}
			fch = ", fecha_financiera = $3"

		}
		query := fmt.Sprintf(`
		UPDATE partidas SET 
			fecha_contable = $3,
			n_comp_str = $4
			%v
			WHERE comitente = $1 AND op_id = $2;
	`, fch)

		_, err = tx.Exec(ctx, query, req.Comitente, req.ID,
			req.FechaContable,
			nCompStr,
		)
		if err != nil {
			return errors.Wrap(err, "modificando partidas")
		}
	}

	r := oplog.Registro{
		Comitente:    req.Comitente,
		CompID:       anterior.CompID,
		PuntoDeVenta: anterior.PuntoDeVenta,
		Persona:      anterior.Persona,
		OpID:         anterior.ID,
		NComp:        anterior.NComp,
		Usuario:      req.Usuario,
		Operacion:    oplog.OpDatosModificados,
		Detalle:      strings.Join(cambios, ", "),
	}
	err = oplog.Log(ctx, tx, r)
	if err != nil {
		return errors.Wrap(err, "logging")
	}

	err = tx.Commit(ctx)
	if err != nil {
		return errors.Wrap(err, "confirmando transacción")
	}

	return
}

func (u Updater) Update(ctx context.Context, tx pgx.Tx, oo []*ops.Op) error {

	return errors.Errorf("no implementada")
}
