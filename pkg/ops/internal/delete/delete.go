package delete

import (
	"context"
	"fmt"
	"sort"
	"strings"

	"bitbucket.org/marcos19/one/pkg/comps"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/ops/internal/saldos"
	"github.com/cockroachdb/errors"
	"github.com/crosslogic/niler"
	uuid "github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
)

type Deleter struct {
	numerador comps.Numerador
}

func New(num comps.Numerador) (out *Deleter, err error) {
	if niler.IsNil(num) {
		return out, errors.Errorf("comps.Numerador no puede ser nil")
	}
	return &Deleter{
		numerador: num,
	}, nil
}

// ActualizarSaldosDelete persiste 1
// 1) los saldos como quedarían luego de eliminadas las ops;
// 2) Los deletes de tabla partidas; y
// 3) Los deletes de tabla ops.
// 4) Vuelvo atrás el numerador del comprobante si corresponde
func (f *Deleter) Delete(ctx context.Context, tx pgx.Tx, comitente int, ops []uuid.UUID) (err error) {

	// Traigo los saldos de esas ops
	saldosOps, err := saldos.TraerSaldosDeOps(ctx, tx, comitente, ops)
	if err != nil {
		return errors.Wrap(err, "buscando saldos")
	}

	_, despues, err := saldos.DeterminarSaldos(ctx, tx, saldosOps)
	if err != nil {
		return errors.Wrap(err, "determinando saldos")
	}

	// Había alguna partida que aplique a alguna de las partidas que estoy
	// por borrar?
	err = corroborarAplicaciones(ctx, tx, comitente, ops)
	if err != nil {
		return errors.Wrap(err, "corroborando aplicaciones")
	}

	// err = partidasSobreAplicadas(antes, despues)
	// if err != nil {
	// 	return errors.E("partidas sobreal", operation, err)
	// }

	// // Controlo stock >= 0
	// err = saldosStockPositivos(antes, despues, v.Depositos)
	// if err != nil {
	// 	return err
	// }

	// // Controlo caja >= 0
	// err = saldosCajaPositivos(antes, v.saldosDespues, v.Cajas)
	// if err != nil {
	// 	return err
	// }

	// Determino como quedarían los saldos luego de eliminada estas ops
	err = saldos.FijarSaldos(ctx, tx, despues)
	if err != nil {
		return errors.Wrap(err, "persistiendo saldos")
	}

	{ // Borro partidas
		query := fmt.Sprintf(`DELETE FROM partidas WHERE op_id IN (%v) AND comitente = %v`, arrayUUID(ops), comitente)
		_, err := tx.Exec(ctx, query)
		if err != nil {
			return errors.Wrap(err, "borrando partidas")
		}
	}

	// Ajusto numeradores (si corresponde)
	err = ajustarNumeradores(ctx, tx, comitente, f.numerador, ops)
	if err != nil {
		return errors.Wrap(err, "ajustando numeradores")
	}

	{ // Borro op
		query := fmt.Sprintf(`DELETE FROM ops WHERE id IN (%v) AND comitente = %v`, arrayUUID(ops), comitente)
		_, err := tx.Exec(ctx, query)
		if err != nil {
			return errors.Wrap(err, "borrando op")
		}
	}

	return
}

func corroborarAplicaciones(ctx context.Context, tx pgx.Tx, comitente int, ids []uuid.UUID) error {

	q := fmt.Sprintf(`
		SELECT id, op_id, partida_aplicada
		FROM partidas
		WHERE partida_aplicada IN (SELECT id FROM partidas WHERE op_id IN (%v)) AND comitente = %v AND op_id NOT IN (%v)
		`, arrayUUID(ids), comitente, arrayUUID(ids))

	rows, err := tx.Query(ctx, q)
	if err != nil {
		return errors.Wrap(err, "querying")
	}
	defer rows.Close()
	type apli struct {
		ID              uuid.UUID
		OpID            uuid.UUID
		PartidaAplicada *uuid.UUID
	}
	found := []apli{}
	for rows.Next() {
		a := apli{}
		err = rows.Scan(&a.ID, &a.OpID, &a.PartidaAplicada)
		if err != nil {
			return errors.Wrap(err, "escaneando")
		}
		found = append(found, a)
	}
	if len(found) > 0 {
		return deferror.Validationf("no se puede borrar porque hay %v partidas aplicadas", len(found))
	}

	return nil
}

func ajustarNumeradores(
	ctx context.Context,
	tx pgx.Tx,
	comitente int,
	num comps.Numerador,
	ops []uuid.UUID,
) error {

	// Traigo datos tabla ops
	oo, err := traerDatosOps(ctx, tx, ops)
	if err != nil {
		return err
	}

	// Retrocedo comps
	err = retrocederNumeradores(ctx, tx, oo, comitente, num)
	if err != nil {
		return err
	}
	return nil
}

// Trae los datos que necesito de la tabla ops
func traerDatosOps(ctx context.Context, tx pgx.Tx, ids []uuid.UUID) (out []op, err error) {

	// Busco comps ID
	query := fmt.Sprintf(
		"SELECT n_comp, comp_id FROM ops WHERE id IN (%v) ORDER BY n_comp DESC",
		arrayUUID(ids),
	)
	rows, err := tx.Query(ctx, query)
	if err != nil {
		return out, errors.Wrap(err, "buscando n_comp de ops")
	}
	defer rows.Close()

	for rows.Next() {
		c := op{}
		err = rows.Scan(&c.NComp, &c.CompID)
		if err != nil {
			return out, errors.Wrap(err, "escaneando campos para numerador")
		}
		out = append(out, c)
	}

	return
}

type op struct {
	CompID int
	NComp  int
}

func retrocederNumeradores(
	ctx context.Context,
	tx pgx.Tx,
	oo []op,
	comitente int,
	num comps.Numerador) (err error) {

	eliminados := map[int][]int{}
	for _, v := range oo {
		prev := eliminados[v.CompID]
		prev = append(prev, v.NComp)
		eliminados[v.CompID] = prev
	}

	// Ordeno los ncomp
	for k := range eliminados {
		// El primero del slice es el más chico
		nn := eliminados[k]
		sort.Slice(nn, func(i, j int) bool {
			return nn[i] > nn[j]
		})
		eliminados[k] = nn
	}

	var ultimos map[int]int

	{ // Busco los últimos
		ids := []int{}
		for k := range eliminados {
			ids = append(ids, k)
		}
		ultimos, err = num.UltimosNumeros(ctx, tx, comitente, ids)
		if err != nil {
			return errors.Wrap(err, "buscando numeración de comprobantes")
		}
	}

	// Calculo los ajustes
	aRetroceder, err := determinarNumeradores(ultimos, eliminados)
	if err != nil {
		return errors.Wrap(err, "calculando nueva numeración")
	}

	// Vuelvo para atrás el numerador sólamente si son los últimos usados
	// Retrocedo
	err = num.AvanzarNumeradores(ctx, tx, comitente, aRetroceder)
	if err != nil {
		return errors.Wrap(err, "retrocendiendo numeradores")
	}

	return
}

func determinarNumeradores(ultimos map[int]int, eliminados map[int][]int) (out map[int]int, err error) {

	aRetroceder := map[int]int{}
	for id := range ultimos {
		delID, ok := eliminados[id]
		if !ok {
			return out, errors.Errorf("no se encontró último en eliminados")
		}
		ultimoQueBorre := delID[0]
		ultimoRegistrado := ultimos[id]
		if ultimoQueBorre != ultimoRegistrado {
			continue
		}
		aRetroceder[id] = ultimoQueBorre - len(eliminados[id])
	}
	return aRetroceder, nil
}

func arrayUUID(ids []uuid.UUID) string {
	idsStr := []string{}
	for _, v := range ids {
		idsStr = append(idsStr, fmt.Sprintf("'%v'", v.String()))
	}
	return strings.Join(idsStr, ", ")
}
