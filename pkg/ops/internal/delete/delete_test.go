package delete

import (
	"context"
	"testing"

	"bitbucket.org/marcos19/one/pkg/comps"
	"github.com/cockroachdb/errors"
	"github.com/davecgh/go-spew/spew"
	"github.com/jackc/pgx/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestRetrocederNumeradores(t *testing.T) {
	ctx := context.Background()
	comitente := 1
	t.Run("se borran los últimos registrados", func(t *testing.T) {
		oo := []op{
			{CompID: 1, NComp: 10},
			{CompID: 2, NComp: 33},
			{CompID: 2, NComp: 34},
		}
		mock := numMock{
			queryErr:   false,
			avanzarErr: false,
			ultimos:    map[int]int{1: 10, 2: 34},
		}

		err := retrocederNumeradores(ctx, nil, oo, comitente, &mock)
		require.Nil(t, err)
		assert.Equal(t, map[int]int{1: 9, 2: 32}, mock.ultimos, "%v", spew.Sdump(mock.ultimos))
	})
	t.Run("se borran anteriores", func(t *testing.T) {
		oo := []op{
			{CompID: 1, NComp: 10},
			{CompID: 2, NComp: 33},
			{CompID: 2, NComp: 34},
		}
		mock := numMock{
			queryErr:   false,
			avanzarErr: false,
			ultimos:    map[int]int{1: 100, 2: 340},
		}

		err := retrocederNumeradores(ctx, nil, oo, comitente, &mock)
		require.Nil(t, err)
		assert.Len(t, mock.ultimos, 0, "%v", spew.Sdump(mock.ultimos))
	})
}

func TestDeterminarNumeradores(t *testing.T) {

	t.Run("deben retroceder", func(t *testing.T) {
		eliminados := map[int][]int{
			1: {10},
			2: {34, 33},
		}
		ultimos := map[int]int{1: 10, 2: 34}
		out, err := determinarNumeradores(ultimos, eliminados)
		require.Nil(t, err)
		assert.Equal(t, 9, out[1])
		assert.Equal(t, 32, out[2])
	})
	t.Run("no debe retroceder", func(t *testing.T) {
		eliminados := map[int][]int{
			1: {9},
			2: {34, 33},
		}
		ultimos := map[int]int{1: 10, 2: 34}
		out, err := determinarNumeradores(ultimos, eliminados)
		require.Nil(t, err)
		require.Len(t, out, 1)
		assert.Equal(t, 32, out[2])
	})
}

type numMock struct {
	queryErr   bool
	avanzarErr bool
	ultimos    map[int]int
}

func (m *numMock) CambióNumero(comps.ReadOneReq) {}
func (m *numMock) UltimosNumeros(context.Context, pgx.Tx, int, []int) (map[int]int, error) {
	if m.queryErr {
		return nil, errors.Errorf("MockErr")
	}
	return m.ultimos, nil
}
func (m *numMock) AvanzarNumeradores(ctx context.Context, tx pgx.Tx, c int, uu map[int]int) error {
	if m.avanzarErr {
		return errors.Errorf("MockErr")
	}
	m.ultimos = uu
	return nil
}
