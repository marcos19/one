package ops

import (
	"time"

	"bitbucket.org/marcos19/one/pkg/iva"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	"github.com/gofrs/uuid"
)

// Partida representa el renglón de un asiento.
type Partida struct {
	Comitente int `json:"-"`
	ID        uuid.UUID

	FechaContable     fecha.Fecha
	FechaFinanciera   fecha.Fecha
	TimeStampContable time.Time

	OpID uuid.UUID

	// Datos duplicados para no tener que hacer un join con Ops
	// Son validados por ops
	Empresa int `json:",string"`

	// Le dejo pegada cuál fue la partida que la generó
	TranDef int `json:",string"`
	CompDef int `json:",string"`

	CreatedAt *time.Time
	UpdatedAt *time.Time `json:",omitempty"`
	Usuario   string     `json:",omitempty"`

	// Lo uso para identificar que cuando quiero analizar un asiento
	// que función cumple la partida. Tiene sentido dentro del contexto
	// del programa que la genera
	Pata string `json:",omitempty"`

	CompNombre string `json:",omitempty"` // Factura de Compra A. Es útil para tirar mayores
	NCompStr   string `json:",omitempty"` // 0001-00054987

	// Cuenta Contable
	Cuenta       int        `json:",string"`
	CuentaNombre string     `json:",omitempty"`
	Centro       *int       `json:",string,omitempty"` // Centro de imputación
	Persona      *uuid.UUID `json:",omitempty"`
	Sucursal     *int       `json:",string,omitempty"`
	Caja         *int       `json:",string,omitempty"`
	Producto     *uuid.UUID `json:",omitempty"`
	SKU          *uuid.UUID `json:",omitempty"`
	Deposito     *int       `json:",string,omitempty"`
	Unicidad     *uuid.UUID `json:",omitempty"`
	UnicidadTipo *int       `json:",string,omitempty"`
	Contrato     *int       `json:",string,omitempty"`

	ConceptoIVA *iva.Concepto `json:",omitempty"`
	AlicuotaIVA *iva.Alicuota `json:",omitempty"`
	Monto       dec.D2

	Moneda              *int    `json:",omitempty"`
	MontoMonedaOriginal *dec.D4 `json:",omitempty"`
	TC                  *dec.D4 `json:",omitempty"`

	// Este campo lo llevan todos los productos cuantificables.
	UM       *string `json:",omitempty"`
	Cantidad *dec.D4 `json:",omitempty"`

	// Puedo sumar Bienes De Cambio, agrupando por CompId y me va a dar
	// a que comprobantes pertenencen los pendientes.
	// También si quisiera costear a posteriori, podría agrupar por este id
	// y obtendría el resultado por Factura sin tener que modifcar la factura
	// original.
	// CompAplicado *uuid.UUID

	// Si la cuenta contable está definida como que aplica,
	// Deberá aplicar a otra partida contable, o bien sobre si misma.
	PartidaAplicada *uuid.UUID `json:",omitempty"`

	Detalle *string `json:",omitempty"`

	// Los asientos como refundición, cierre, apertura y ajuste por inflación
	// son especiales porque no están incluidos en el saldo
	// Cuando es normal este campo es null
	TipoAsiento *TipoAsiento

	// Los calces se generan con las APLICACIONES.
	// La fecha de contable en que se realice la aplicación es INDISTINTA.
	// Los calces son calculados automaticamente en base a las partidas
	// a las que elegi aplicar.
	//
	// La diferencia entre FchCont y la fecha contable de este comprobante de aplicación
	// DiasFinanciamiento *int

	// La diferencia entre FchVto y la fecha contable de este comprobante de aplicación
	// DiasMora *int

	// MontoAplicado * DíasFinanciamiento
	// PonderadoFinanciamiento *int

	// MontoAplicado * DíasMora
	// PonderadoMora *int

}

// TipoAsiento sirve para filtrar partidas contables especiales
// Los asientos como refundición, cierre, apertura y ajuste por inflación
// son especiales porque sus montos no se incluyen en los saldos.
type TipoAsiento int

const (
	// Refundicion es el código que llevan aquellas partidas contables que
	// pertenencen a un asiento de refundición de cuentas de resultado.
	Refundicion = 1
	// Cierre es el código que llevan aquellas partidas contables que
	// pertenencen a un asiento de cierre de cuentas patrimoniales.
	Cierre = 2
	// Apertura es el código que llevan aquellas partidas contables que
	// pertenencen a un asiento de apertura.
	Apertura = 3
	// Inflacion es el código que llevan aquellas partidas contables que
	// pertenencen a un asiento de ajuste por inflación.
	Inflacion = 4
)

// TableName devuelve el nombre de la tabla de partida
func (p Partida) TableName() string {
	return "partidas"
}
