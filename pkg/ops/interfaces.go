package ops

import (
	"context"
	"time"

	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
	uuid "github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
)

type ReaderOne interface {
	ReadOne(context.Context, ReadOneReq) (Op, error)
}
type ReadOneReq struct {
	ID        uuid.UUID
	Comitente int
}

type ReaderMany interface {
	ReadMany(context.Context, ReadManyReq) ([]Op, error)
}
type ReaderPartidas interface {
	Partidas(context.Context, PartidasReq) ([]Partida, error)
}

type Inserter interface {
	// Realiza las validaciones y rellena los campos que faltan.
	// Si InsertConfig es nil, el comportamiento es el estándar.
	Validate(context.Context, pgx.Tx, []*Op, *InsertConfig) error

	// Inserta las operaciones.
	// El tipo de numeración la obtiene de la configuración del comprobante.
	// Si InsertConfig es nil, el comportamiento es el estándar.
	Insert(ctx context.Context, tx pgx.Tx, oo []*Op, cfg *InsertConfig) error
}

type InsertConfig struct {
	// Si es un asiento de refundición, cierre o apertura, no
	// exige tener todos los campos de especificación.
	AsientoEspecial bool
}

type Deleter interface {
	Delete(ctx context.Context, tx pgx.Tx, comitente int, opsID []uuid.UUID) error
}

type Updater interface {
	Update(ctx context.Context, tx pgx.Tx, oo []*Op) error
	ModificarDatos(context.Context, ModificarDatosReq) error
}

// Devuelve el campo PartidaAplicada para la UnicidadID ingresada
type UnicidadIDGetter interface {
	GetPartidaAplicada(context.Context, int, uuid.UUID) (out uuid.UUID, err error)
}

// Determina si se pueden usar esas cuentas para la fecha ingresada
type BloqueoCuenta interface {
	Puede(ctx context.Context, fch fecha.Fecha, comitente int, empresa int, ctas tipos.GrupoInts) error
}
type HistorialReq struct {
	Empresa      int `json:",string"`
	Programa     string
	Desde        fecha.Fecha
	Hasta        fecha.Fecha
	PuntoDeVenta int `json:",string"`
	NComp        int `json:",string"`
	Comp         int `json:",string"`
	Persona      uuid.UUID
	Sucursal     int `json:",string"`

	Limit  int
	Pagina int

	TranID    uuid.UUID
	Comitente int
}

type HistorialResp struct {
	ID            uuid.UUID
	Fecha         fecha.Fecha
	CompNombre    string
	NCompStr      string
	PersonaNombre string
	TotalComp     dec.D2
	Detalle       string
	CreatedAt     time.Time
}

type PartidasReq struct {
	Comitente int
	ID        uuid.UUID   // op_id
	IDs       []uuid.UUID // id
}

type ProfundizarReq struct {
	// ID de la partida contable
	ID        uuid.UUID
	Comitente int
}
type ProfundizarResponse struct {
	Programa string
	Key      string
	Value    uuid.UUID
}
type ReadManyReq struct {
	TranID    uuid.UUID
	Comitente int
}
type ReconstruirReq struct {
	Comitente int
	Empresa   int `json:",string"`
	Cuenta    int `json:",string"`
}

type ModificarDatosReq struct {
	Comitente         int
	ID                uuid.UUID
	FechaContable     fecha.Fecha
	FechaOriginal     fecha.Fecha
	AFIPComprobanteID *int `json:",string"`
	PuntoDeVenta      *int
	NComp             int
	Usuario           string
}
