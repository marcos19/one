// Package ops define la Op, cada movimiento en la contabilidad tiene como
// cabecera una op.
// Todos los datos de la operacion quedan guardados dentro del mismo documento,
// tanto los "cargados" por el usuario como los calculados (asientos por ejemplo).
//
// - Definición de Op y Partida
// - Validación de op y sus partidas (para INSERT, UPDATE y DELETE)
//   - Cuestiones formales (que tenga número comprobante, empresa, de partida doble, etc).
//   - Directivas que vienen de otros packages
//   - Empresa habilitada (desde-hasta)
//   - Cuenta contable habilitada (desde-hasta)
//   - Comp habilitado (desde-hasta)
//   - Persona habilitada (desde-hasta)
//   - Persona usa sucursal? Tiene sucursal seleccionada?
//   - Caja habilitada (desde-hasta)
//   - Saldo caja positivo?
//   - Depósito habilitado (desde-hasta)
//   - Producto habilitado (desde-hasta)
//
// -
package ops

import (
	"bytes"
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"io"
	"reflect"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/afip/afipmodels"
	"bitbucket.org/marcos19/one/pkg/iva"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	uuid "github.com/gofrs/uuid"
	"github.com/olekukonko/tablewriter"

	"github.com/crosslogic/dec"
	"github.com/crosslogic/fecha"
)

// Op es la estructura que contiene toda la informacion sobre las operaciones
// que quedan registradas en la base de datos. Apunto a la inmutabilidad.
// Todos los datos de la operacion quedan guardados dentro del mismo documento,
// tanto los "cargados" por el usuario como los calculados (asientos por ejemplo).
type Op struct {
	Comitente int

	// Identificador único de operación
	ID uuid.UUID

	// Empresa es la separación legal que tienen los comprobantes.
	Empresa int `json:",string"`

	// Cada comprobante va a tener una fecha pegada, que es un dato independiente
	// de la hora.
	Fecha fecha.Fecha

	// Por ejemplo si en el mismo momento se hace un comprobante en
	// Argentina y otro en Japón, uno va a quedar con una fecha y el otro con la otra.
	// FechaContable será la misma.
	FechaContable time.Time

	// TRANSACCIÓN
	//
	// Cuál fue el que lo generó: fact, minuta, rendición, etc.
	Programa string
	// El programa que registra este comprobante, guarda los datos que necesita
	// en un registro en su tabla. Este ID es ese registro.
	// (este campo no referencia a ninguna tabla)
	TranID uuid.UUID
	// id que define el comportamiento de la transacción
	// Ej: dentro del programa fact => 2 - Factura Directa
	// Cada programa tiene su tabla con su configuración
	// (este campo no referencia a ninguna tabla)
	TranDef int `json:",string"`

	// Una transacción puede generar una Factura y un Recibo.
	// Todas esas ops, tendrán el mismo número de TranID, TranDef y CompDef.
	// Cuando abra el programa de facturación, tengo que tener una forma de
	// identificar cuál es la Factura y cual el Recibo.
	// Para ello el programa de facturación tiene definido que comprobante
	// en [types.Factura, types.Recibo, types.Anulacion...]
	CompItem *int `json:",string"`

	// COMPROBANTE
	//
	CompID int `json:",string"`
	// Comp nombre es el nombre que se va a mostrar cuando se muestre un mayor
	// por ejemplo. Diría "Factura A" por ejemplo.
	CompNombre   string
	PuntoDeVenta int // Puede ser determinado por el centro emisor o ingresado por el usuario
	NComp        int // Es el número correlativo (o ingresado por el usuario)
	// Corresponde al número de comprobante con su formato
	// El programa que lo hizo se encarga de formatearlo (Ej fact haría 0001-15612163).
	NCompStr string
	// Es una consulta muy común consultar los comprobantes de un cliente, o de un
	// proveedor.
	// Hay operaciones que no tienen personas involucradas (ej: rendición de caja)
	// Hay operaciones que tienen varios (ej: carta de porte, mandato)
	Persona  *uuid.UUID `json:",omitempty"`
	Sucursal *int       `json:",omitempty"`
	// Cuando tire el listado de comprobantes no quiero tener que hacer un
	// join para ver a quien corresponde el ID (incluye nombre sucursal)
	PersonaNombre string `json:",omitempty"`

	// Cuando tiro el listado de comprobantes, la mayoría tiene un número de total.
	// Minutas y remitos no van a tener.
	TotalComp *dec.D2 `json:",omitempty"`

	// Se encarga de generarlo el programa. Por ejemplo un recibo va a decir:
	// 'Cobro a JUAN PEREZ'
	Detalle string `json:",omitempty"`

	// A los efectos impositivos y de impresión. Un comprobante puede tener
	// productos en pesos y en dólares. Este campo indica la directiva a seguir.
	// Podría ser pesificar todas las partidas en dólares o bien dolarizar las
	// partidas en pesos.
	TipoMoneda     string `json:",omitempty"`
	TipoCambio     dec.D4 `json:",omitempty"`
	TipoCotizacion string `json:",omitempty"`

	// Toda la información necesaria para la impresión del comprobante que no
	// está en ningún campo ni en la struct del programa.
	Impresion tipos.JSON `json:",omitempty"`

	// Representa una aplicación que se hace a nivel comprobante.
	// El fin es servir de referencia cuando un comprobante anule totalmente otro.
	// ApliCompAplicado  int
	// ApliFechaContable time.Time
	// ApliTipoComp      string
	// ApliNComp         string
	// ApliMontoOriginal dec.D2

	// Sería el resumen de todas las partidas calzadas en el comprobante.
	// Yo podría estar calzando partidas del
	// DiasFinanciamiento dec.D2
	// DiasMora           dec.D2
	// Ponderado          dec.D2

	// MontoFinanciado    dec.D2

	FechaOriginal fecha.Fecha `json:",omitempty"`

	// Campos impositivos
	TipoIdentificacion    *int                     `json:",omitempty"`
	NIdentificacion       *int                     `json:",omitempty"`
	CondicionFiscal       *afipmodels.CondicionIVA `json:",omitempty"`
	AFIPComprobanteID     *int                     `json:",string,omitempty"`
	AFIPComprobanteNombre *string                  `json:",omitempty"`
	CAE                   *int                     `json:",string,omitempty"`
	CAEVto                *fecha.Fecha             `json:",omitempty"`
	BarCode               *string                  `json:",omitempty"`
	QR                    *string                  `json:",omitempty"`
	AfipFields            *AfipFields              `json:",omitempty"`
	TotalesConceptos      TotalesConceptos

	PartidasContables []Partida `json:",omitempty"`

	CreatedAt *time.Time
	UpdatedAt *time.Time `json:",omitempty"`
	Usuario   string
}

// Son datos extra que necesito para factura electrónica
type AfipFields struct {
	CbtesAsoc    []CbteAsoc  `json:",omitempty"`
	FchServDesde fecha.Fecha `json:",omitempty"`
	FchServHasta fecha.Fecha `json:",omitempty"`
	FchVtoPago   fecha.Fecha `json:",omitempty"`
}

type CbteAsoc struct {
	Tipo    int
	PtoVta  int
	Nro     int
	Cuit    string
	CbteFch string
}

// Value cumple con la interface SQL
func (j AfipFields) Value() (driver.Value, error) {
	return json.Marshal(j)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (j *AfipFields) Scan(src interface{}) error {
	if src == nil {
		*j = AfipFields{}
		return nil
	}

	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New(fmt.Sprint(
			"Type assertion error .([]byte). Era: ",
			reflect.TypeOf(src),
		))
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, j)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("Unmarshalling los siguientes bytes: '%s'", source))
	}

	return nil
}

type TotalesConceptos []TotalConcepto

func (t TotalesConceptos) Sum() (out dec.D2) {
	for _, v := range t {
		out += v.Monto
	}
	return
}

// Value cumple con la interface SQL
func (j TotalesConceptos) Value() (driver.Value, error) {
	return json.Marshal(j)
}

// Scan implementa la interface SQL. Es para pegar los datos de la base
// de datos en una struct.
func (j *TotalesConceptos) Scan(src interface{}) error {
	if src == nil {
		*j = TotalesConceptos{}
		return nil
	}

	// Chequeo que el campo sea []byte
	source, ok := src.([]byte)
	if !ok {
		return errors.New(fmt.Sprint(
			"Type assertion error .([]byte). Era: ",
			reflect.TypeOf(src),
		))
	}

	// Chequeo que se condiga con la struct
	err := json.Unmarshal(source, j)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("Unmarshalling los siguientes bytes: '%s'", source))
	}

	return nil
}

// Son los datos que uso para el libro de IVA
type TotalConcepto struct {
	Alic     iva.Alicuota
	Concepto iva.Concepto
	Nombre   string
	Monto    dec.D2
}

// TotalDebe devuelve la sumatoria de todas las partidas que están del lado del debe.
func (o *Op) TotalDebe() (debe dec.D2) {
	for _, v := range o.PartidasContables {
		if v.Monto > 0 {
			debe += v.Monto
		}
	}
	return
}

// TotalHaber devuelve la sumatoria de todas las partidas que están del lado del haber.
func (o *Op) TotalHaber() (haber dec.D2) {
	for _, v := range o.PartidasContables {
		if v.Monto < 0 {
			haber += v.Monto
		}
	}
	return
}

// PartidaDobleOk devuelve un error si no cierra la partida doble.
func (o *Op) PartidaDobleOk() error {
	var total dec.D2
	for _, v := range o.PartidasContables {
		total += v.Monto
	}
	if total == 0 {
		return nil
	}
	return errors.New(fmt.Sprint(
		"No cierra la partida doble.\n",
		"Diferencia:  ", total,
		"Total debe:  ", o.TotalDebe(),
		"Total haber: ", o.TotalHaber(),
	))
}

func FormatComp(puntoVenta, numero int, mask string) (out string, err error) {

	if mask == "" {
		mask = "PPPP-NNNNNNNN"
	}
	mask = strings.ToUpper(mask)
	partes := []string{}

	cantidadPV := strings.Count(mask, "P")
	pv, err := llenarCeros(puntoVenta, cantidadPV)
	if err != nil {
		return out, errors.Wrap(err, "llenando con ceros el punto de venta")
	}
	if pv != "" {
		partes = append(partes, pv)
	}

	cantidadN := strings.Count(mask, "N")
	n, err := llenarCeros(numero, cantidadN)
	if err != nil {
		return out, errors.Wrap(err, "llenando con ceros el comprobante")
	}
	if n != "" {
		partes = append(partes, n)
	}

	return strings.Join(partes, "-"), nil
}

func llenarCeros(n, digitos int) (out string, err error) {
	if digitos == 0 {
		return "", nil
	}
	nStr := fmt.Sprint(n)
	if len(nStr) > digitos {
		return out, errors.Errorf("había más dígitos (%v) que espacios (%v)", len(nStr), digitos)
	}
	cantidad := digitos - len(nStr)
	for i := 1; i <= cantidad; i++ {
		out += "0"
	}
	return out + nStr, nil
}
func (op Op) AsciiTableString() string {
	buf := bytes.Buffer{}
	op.AsciiTable(&buf)
	return buf.String()
}

func (op Op) AsciiTable(w io.Writer) {
	table := tablewriter.NewWriter(w)
	table.SetHeader([]string{"id", "pata", "cuenta", "concepto", "alicuota", "centro", "persona", "sucursal", "unicidad", "part apl", "monto", "fecha_vto", "detalle"})
	table.SetBorder(false)
	for _, v := range op.PartidasContables {
		det := ""
		if v.Detalle != nil {
			det = *v.Detalle
		}
		row := []string{
			v.ID.String(),
			v.Pata,
			fmt.Sprint(v.Cuenta),
			fmt.Sprint(v.ConceptoIVA),
			fmt.Sprint(v.AlicuotaIVA),
			fmt.Sprint(v.Centro),
			fmt.Sprint(v.Persona),
			fmt.Sprint(v.Sucursal),
			fmt.Sprint(v.Unicidad),
			fmt.Sprint(v.PartidaAplicada),
			v.Monto.String(),
			v.FechaFinanciera.String(),
			det,
		}
		table.Append(row)
	}
	// table.SetFooter([]string{"", "", "", "", "", "", "Total", op.TotalDebe())
	table.Render()
}
