package geo

// Provincia representa un registro de la base de datos
type Provincia struct {
	ID     int `json:",string"`
	Nombre string
	IDAfip int `json:",string"`
}

func (p Provincia) TableName() string {
	return "provincias"
}
