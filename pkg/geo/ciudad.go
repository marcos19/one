package geo

import (
	"fmt"
)

// Ciudad es el registro de la tabla Ciudades
type Ciudad struct {
	ID              int
	Nombre          string
	CP              int
	CPAlfa          string
	Provincia       int
	ProvinciaNombre string
}

func (c Ciudad) String() string {
	return fmt.Sprint(
		c.Nombre,
		" (", c.ID, ") ",
		c.ProvinciaNombre,
	)
}

// TableName devuelve el nombre de la tabla en la base de datos
func (c *Ciudad) TableName() string {
	return "ciudades"
}
