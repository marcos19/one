package geo

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/rs/zerolog/log"
)

type Handler struct {
	conn *pgxpool.Pool
}

func NewHandler(conn *pgxpool.Pool) *Handler {
	return &Handler{conn}
}

type ReadManyReq struct {
	Texto     string
	Provincia int `json:",string"`
	// ID        *int
}

func (h *Handler) CiudadReadMany(ctx context.Context, req ReadManyReq) (out []Ciudad, err error) {
	out = []Ciudad{}
	ww := []string{}
	pp := []interface{}{}

	cp, err := strconv.Atoi(req.Texto)
	if err == nil {
		// Si es numero es código postal
		log.Debug().Msgf("buscando por CP %v", cp)
		// Estoy buscando por cp
		pp = append(pp, cp)
		ww = append(ww, fmt.Sprintf("cp=$%v", len(pp)))
	}

	if req.Provincia != 0 {
		pp = append(pp, req.Provincia)
		ww = append(ww, fmt.Sprintf("provincia=$%v", len(pp)))
	}
	// Si pasé un texto en los parámetros busco por nombre
	if cp == 0 && req.Texto != "" {
		pp = append(pp, "%"+req.Texto+"%")
		ww = append(ww, fmt.Sprintf("nombre ILIKE $%v", len(pp)))
	}

	where := ""
	if len(ww) > 0 {
		where = "WHERE " + strings.Join(ww, " AND ")
	}
	query := fmt.Sprintf(
		`SELECT id, nombre, cp, cp_alfa, provincia, provincia_nombre FROM ciudades %v ORDER BY nombre`,
		where,
	)

	rows, err := h.conn.Query(ctx, query, pp...)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()

	for rows.Next() {
		v := Ciudad{}
		err = rows.Scan(&v.ID, &v.Nombre, &v.CP, &v.CPAlfa, &v.Provincia, &v.ProvinciaNombre)
		if err != nil {
			return out, deferror.DB(err, "escaneando")
		}
		out = append(out, v)
	}
	return
}

func (h *Handler) CiudadReadOne(ctx context.Context, id int) (out Ciudad, err error) {

	query := `SELECT id, nombre, cp, cp_alfa, provincia, provincia_nombre FROM ciudades WHERE id=$1`
	v := Ciudad{}
	err = h.conn.QueryRow(ctx, query, id).Scan(&v.ID, &v.Nombre, &v.CP, &v.CPAlfa, &v.Provincia, &v.ProvinciaNombre)
	if err != nil {
		return out, deferror.DB(err, "buscando ciudad por ID")
	}
	return
}

func (h *Handler) ProvinciasMany(ctx context.Context) (out []Provincia, err error) {

	// Busco
	rows, err := h.conn.Query(ctx, "SELECT id, nombre, id_afip FROM provincias ORDER BY nombre")
	if err != nil {
		return out, err
	}
	defer rows.Close()

	for rows.Next() {
		p := Provincia{}
		err = rows.Scan(&p.ID, &p.Nombre, &p.IDAfip)
		if err != nil {
			return out, deferror.DB(err, "escaneando")
		}
		out = append(out, p)
	}

	return
}

func ProvinciasIDAfip(ctx context.Context, conn *pgxpool.Pool, id int) (out Provincia, err error) {

	// Busco
	err = conn.QueryRow(ctx, "SELECT id, nombre, id_afip FROM provincias WHERE id_afip = $1 LIMIT 1", id).Scan(&out.ID, &out.Nombre, &out.IDAfip)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}

	return
}
