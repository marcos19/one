package centrosgrupos

import (
	"context"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/tipos"
	"github.com/cockroachdb/errors"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler es el encargado de recibir los http requests y emitir los responses.
type Handler struct {
	conn *pgxpool.Pool
}

type HandlerConfig struct {
	Conn *pgxpool.Pool
}

// NewHandler incializa un Handler.
func NewHandler(config HandlerConfig) (h *Handler, err error) {

	// Valido
	if config.Conn == nil {
		return h, errors.New("Conn no puede ser nil")
	}

	h = &Handler{
		conn: config.Conn,
	}

	return
}

func (h *Handler) Create(ctx context.Context, req *Grupo) (err error) {

	// Valido
	if req == nil {
		return deferror.Validation("centro era nil")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no ingresó el ID de comitente ")
	}
	if req.Nombre == "" {
		return deferror.Validation("Debe ingresar un nombre para el grupo")
	}
	if len(req.Centros) == 0 {
		return deferror.Validation("Debe ingresar alguno centro")
	}

	query := `INSERT INTO centros_grupos (comitente, nombre, detalle, centros, created_at) VALUES ($1,$2,$3,$4,$5)`

	// Persisto
	_, err = h.conn.Exec(ctx, query, req.Comitente, req.Nombre, req.Detalle, req.Centros, time.Now())
	if err != nil {
		return deferror.DB(err, "insertando grupo")
	}

	// h.eventbus.CambióCuentasGrupos("creó", *req)

	return
}

func (h *Handler) Update(ctx context.Context, req Grupo) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no ingresó el ID de comitente ")
	}
	if req.Nombre == "" {
		return deferror.Validation("Debe ingresar un nombre para el grupo")
	}
	if len(req.Centros) == 0 {
		return deferror.Validation("Debe ingresar algún")
	}

	query := `UPDATE centros_grupos SET nombre=$3, detalle=$4, centros=$5, updated_at=$6 WHERE comitente=$1 AND id=$2`
	// Persisto
	_, err = h.conn.Exec(ctx, query, req.Comitente, req.ID, req.Nombre, req.Detalle, req.Centros, time.Now())
	if err != nil {
		return deferror.DB(err, "modificando grupo")
	}

	return
}

type ReadOneReq struct {
	Comitente int
	ID        int `json:",string"`
}

func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (out Grupo, err error) {

	// Valido
	if req.ID == 0 {
		return out, deferror.Validation("no se ingresó ID a buscar")
	}
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó comitente a buscar")
	}

	query := `SELECT id, comitente, nombre, detalle, centros, created_at, updated_at FROM centros_grupos WHERE comitente=$1 AND id=$2`

	// Busco
	err = h.conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(
		&out.ID, &out.Comitente, &out.Nombre, &out.Detalle, &out.Centros, &out.CreatedAt, &out.UpdatedAt,
	)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning grupo")
	}

	return
}

type ReadManyReq struct {
	Comitente int
}

func (h *Handler) ReadMany(ctx context.Context, req ReadManyReq) (out []Grupo, err error) {
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó comitente a buscar")
	}

	query := `SELECT id, comitente, nombre, detalle, centros, created_at, updated_at FROM centros_grupos WHERE comitente=$1`

	rows, err := h.conn.Query(ctx, query, req.Comitente)
	if err != nil {
		return out, errors.Wrap(err, "querying centros")
	}
	defer rows.Close()

	for rows.Next() {
		v := Grupo{}
		err = rows.Scan(
			&v.ID, &v.Comitente, &v.Nombre, &v.Detalle, &v.Centros, &v.CreatedAt, &v.UpdatedAt,
		)
		if err != nil {
			return out, deferror.DB(err, "escaneando")
		}
		out = append(out, v)
	}
	return
}

func (h *Handler) Delete(ctx context.Context, req ReadOneReq) (err error) {

	// Valido
	if req.ID == 0 {
		return deferror.Validation("no se ingresó ID a buscar")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó comitente a buscar")
	}

	// Busco
	res, err := h.conn.Exec(ctx, "DELETE FROM centros_grupos WHERE comitente=$1 AND id=$2", req.Comitente, req.ID)
	if err != nil {
		return errors.Wrap(err, "deleting")
	}

	if res.RowsAffected() == 0 {
		return errors.Errorf("no se borró ningún registro")
	}
	return
}

// Cruza grupos y centros y devuelve un outter join de IDs de centros de costo.
func (h *Handler) CentrosID(ctx context.Context, comitente int, grupos, ids tipos.GrupoInts) (out tipos.GrupoInts, err error) {

	m := map[int]struct{}{}
	for _, v := range grupos {
		gr, err := h.ReadOne(ctx, ReadOneReq{Comitente: comitente, ID: v})
		if err != nil {
			return nil, errors.Wrap(err, "buscando grupo")
		}
		for _, id := range gr.Centros {
			m[id] = struct{}{}
		}
	}
	for _, v := range ids {
		m[v] = struct{}{}
	}

	for k := range m {
		out = append(out, k)
	}
	return
}

// // Devuelve las cuentas que están comprendidas dentro de los grupos ingresados
// func (h *Handler) Cuentas(comitente int, gg tipos.GrupoInts) (out tipos.GrupoInts, err error) {

// 	mm := map[int]struct{}{}
// 	for _, v := range gg {
// 		gr, err := h.ReadOne(ReadOneReq{
// 			Comitente: comitente,
// 			ID:        v,
// 		})
// 		if err != nil {
// 			return nil, errors.Wrap(err, "buscando grupo de cuentas")
// 		}

// 		for _, w := range gr.Cuentas {
// 			mm[w] = struct{}{}
// 		}
// 	}
// 	for k := range mm {
// 		out = append(out, k)
// 	}
// 	return
// }
