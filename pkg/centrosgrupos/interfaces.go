package centrosgrupos

import "context"

type ReaderOne interface {
	ReadOne(context.Context, ReadOneReq) (Grupo, error)
}
