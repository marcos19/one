package centrosgrupos

import (
	"time"

	"bitbucket.org/marcos19/one/pkg/tipos"
)

type Grupo struct {
	ID        int `json:",string"`
	Comitente int
	Nombre    string
	Detalle   string
	Centros   tipos.GrupoInts
	CreatedAt *time.Time
	UpdatedAt *time.Time
}

func (g Grupo) TableName() string {
	return "centros_grupos"
}

/*
	CREATE TABLE centros_grupos (
		id
			SERIAL PRIMARY KEY,
		comitente
			INT NOT NULL REFERENCES comitentes,
		nombre
			STRING,
		detalle
			STRING,
		centros
			INT[],
		created_at
			TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
		updated_at
			TIMESTAMP WITH TIME ZONE
	);
*/
