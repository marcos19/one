package allhandlers

import (
	"fmt"

	"bitbucket.org/marcos19/one/pkg/afip"
	"bitbucket.org/marcos19/one/pkg/afip/cert"
	"bitbucket.org/marcos19/one/pkg/afip/wsaa"
	wsaacore "bitbucket.org/marcos19/one/pkg/afip/wsaa/core"
	"bitbucket.org/marcos19/one/pkg/afip/wsfev1"
	"bitbucket.org/marcos19/one/pkg/apertura"
	"bitbucket.org/marcos19/one/pkg/aplicador"
	apliConfig "bitbucket.org/marcos19/one/pkg/aplicador/config"
	"bitbucket.org/marcos19/one/pkg/autoaplicador"
	"bitbucket.org/marcos19/one/pkg/bcra"
	"bitbucket.org/marcos19/one/pkg/bsuso"
	"bitbucket.org/marcos19/one/pkg/bus"
	cajashandler "bitbucket.org/marcos19/one/pkg/cajas/handler"
	centroshandler "bitbucket.org/marcos19/one/pkg/centros/handler"
	"bitbucket.org/marcos19/one/pkg/centrosgrupos"
	"bitbucket.org/marcos19/one/pkg/cierre"
	"bitbucket.org/marcos19/one/pkg/comitentes"
	compshandler "bitbucket.org/marcos19/one/pkg/comps/handler"
	"bitbucket.org/marcos19/one/pkg/condiciones"
	aplifinhandler "bitbucket.org/marcos19/one/pkg/contables/aplifin/handler"
	"bitbucket.org/marcos19/one/pkg/contables/apliprod"
	"bitbucket.org/marcos19/one/pkg/contables/balance"
	"bitbucket.org/marcos19/one/pkg/contables/librodiario"
	"bitbucket.org/marcos19/one/pkg/contables/mayor"
	"bitbucket.org/marcos19/one/pkg/contables/mayorproductos"
	"bitbucket.org/marcos19/one/pkg/contables/resumencaja"
	"bitbucket.org/marcos19/one/pkg/contables/saldospersonas"
	"bitbucket.org/marcos19/one/pkg/contables/saldosproductos"
	"bitbucket.org/marcos19/one/pkg/contables/vistapersona"
	cuentashandler "bitbucket.org/marcos19/one/pkg/cuentas/handler"
	"bitbucket.org/marcos19/one/pkg/cuentasgrupos"
	"bitbucket.org/marcos19/one/pkg/depositos"
	empresashandler "bitbucket.org/marcos19/one/pkg/empresas/handler"
	factConfig "bitbucket.org/marcos19/one/pkg/fact/config"
	factEsquema "bitbucket.org/marcos19/one/pkg/fact/esquemas"
	facthandler "bitbucket.org/marcos19/one/pkg/fact/handler"
	factPermisos "bitbucket.org/marcos19/one/pkg/fact/permisos"
	"bitbucket.org/marcos19/one/pkg/factinter"
	"bitbucket.org/marcos19/one/pkg/geo"
	"bitbucket.org/marcos19/one/pkg/grupos"
	"bitbucket.org/marcos19/one/pkg/inflacion"
	inflacionConfig "bitbucket.org/marcos19/one/pkg/inflacion/config"
	"bitbucket.org/marcos19/one/pkg/listasprecios"
	"bitbucket.org/marcos19/one/pkg/minuta"
	minConfig "bitbucket.org/marcos19/one/pkg/minuta/configs"
	minEsquema "bitbucket.org/marcos19/one/pkg/minuta/esquemas"
	"bitbucket.org/marcos19/one/pkg/monedas"
	opshandler "bitbucket.org/marcos19/one/pkg/ops/handler"
	"bitbucket.org/marcos19/one/pkg/opsbloqcta"
	"bitbucket.org/marcos19/one/pkg/permisos/lookups"
	personasHandler "bitbucket.org/marcos19/one/pkg/personas/handler"
	"bitbucket.org/marcos19/one/pkg/precios"
	"bitbucket.org/marcos19/one/pkg/productos"
	"bitbucket.org/marcos19/one/pkg/productos/categorias"
	"bitbucket.org/marcos19/one/pkg/programas"
	"bitbucket.org/marcos19/one/pkg/reclamos"
	reclamostipos "bitbucket.org/marcos19/one/pkg/reclamos/tipos"
	"bitbucket.org/marcos19/one/pkg/refundicion"
	rendConfig "bitbucket.org/marcos19/one/pkg/rendicion/configs"
	rendicionhandler "bitbucket.org/marcos19/one/pkg/rendicion/handler"
	"bitbucket.org/marcos19/one/pkg/resbancario"
	resConfig "bitbucket.org/marcos19/one/pkg/resbancario/config"
	"bitbucket.org/marcos19/one/pkg/sesiones"
	"bitbucket.org/marcos19/one/pkg/sucursales"
	"bitbucket.org/marcos19/one/pkg/unicidades"
	unicidadescont "bitbucket.org/marcos19/one/pkg/unicidadescont/handler"
	"bitbucket.org/marcos19/one/pkg/unicidadesdef"
	usuarioscache "bitbucket.org/marcos19/one/pkg/usuarios/cache"
	usuarioshandler "bitbucket.org/marcos19/one/pkg/usuarios/handler"
	usuariosgruposhandler "bitbucket.org/marcos19/one/pkg/usuariosgrupos/handler"

	"github.com/cockroachdb/errors"
	"github.com/dgraph-io/ristretto"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Handler struct {
	Conn      *pgxpool.Pool
	secretKey []byte

	AFIP            *afip.Handler
	AFIPwsaa        *wsaa.Store
	AFIPwsfe        *wsfev1.Handler
	AFIPcert        *cert.Handler
	Apertura        *apertura.Handler
	Aplicador       *aplicador.Handler
	Aplifin         *aplifinhandler.Handler
	AplicadorConfig *apliConfig.Handler
	Autoaplicador   *autoaplicador.Handler
	Cajas           *cajashandler.Handler
	BCRA            *bcra.Handler
	Bsuso           *bsuso.Handler
	Cache           *ristretto.Cache
	Centros         *centroshandler.Handler
	CentrosGrupos   *centrosgrupos.Handler
	Categorias      *categorias.Handler
	Cierre          *cierre.Handler
	Comitentes      *comitentes.Handler
	Condiciones     *condiciones.Handler
	Comps           *compshandler.Handler
	// Cruds           *cruds.Handler
	Cuentas         *cuentashandler.Handler
	CuentasGrupos   *cuentasgrupos.Handler
	Depositos       *depositos.Handler
	Empresas        *empresashandler.Handler
	Geo             *geo.Handler
	Grupos          *grupos.Handler
	Inflacion       *inflacion.Handler
	InflacionConfig *inflacionConfig.Handler
	Apliprod        *apliprod.Handler
	ListasPrecio    *listasprecios.Handler
	Lookups         *lookups.Handler

	Ops            *opshandler.Handler
	OpsBloqCta     *opsbloqcta.Handler
	Personas       *personasHandler.Handler
	Sucursales     *sucursales.Handler
	Precios        *precios.Handler
	Productos      *productos.Handler
	Programas      *programas.Handler
	Reclamos       *reclamos.Handler
	ReclamosTipos  *reclamostipos.Handler
	Refundicion    *refundicion.Handler
	Unicidades     *unicidades.Handler
	Unicidadesdef  *unicidadesdef.Handler
	Unicidadescont *unicidadescont.Handler

	Usuarios       *usuarioshandler.Handler
	UsuariosGrupos *usuariosgruposhandler.Handler
	Sesiones       *sesiones.Handler

	// Operacionales
	Rendicion       *rendicionhandler.Handler
	RendicionConfig *rendConfig.Handler

	Resbancario       *resbancario.Handler
	ResbancarioConfig *resConfig.Handler

	Fact         *facthandler.Handler
	FactInter    *factinter.Handler
	FactConfig   *factConfig.Handler
	FactEsquema  *factEsquema.Handler
	FactPermisos *factPermisos.Handler

	Minuta        *minuta.Handler
	MinutaConfig  *minConfig.Handler
	MinutaEsquema *minEsquema.Handler
	Monedas       *monedas.Handler

	WebSocket Socket

	// Informes contables
	Balance         *balance.Handler
	LibroDiario     *librodiario.Handler
	Mayor           *mayor.Handler
	ResumenCaja     *resumencaja.Handler
	SaldosPersonas  *saldospersonas.Handler
	SaldosProductos *saldosproductos.Handler
	MayorProductos  *mayorproductos.Handler
	VistaPersona    *vistapersona.Handler
}

type HandlerArgs struct {
	Conn *pgxpool.Pool
	// Si se deja nil utiliza una config por defecto
	CacheConfig *ristretto.Config
	EventBus    Socket

	Entorno Entorno
	// la clave que uso para codificar los JWT

	SecretKey []byte
	// Cuando se envían los mails, lo que aparece en el campo from.
	// Debería ser del tipo Sweet <no_responder@sweet.com.ar>
	MailFrom string

	//
	TamañoMaximoDeImagenes int
}

type Entorno string

const (
	EntornoTesting    = Entorno("Testing")
	EntornoProduccion = Entorno("Producción")
)

type Socket interface {
	BroadcastMsgAComitente(msg interface{}, comitente int)
	BroadcastMsgAUsuario(msg interface{}, usuario string)
}

// New devuelve una nueva instancia de un servidor
//
// EventBus: si no se ingresa => pone un mock
//
// CacheConfig: si no se ingresa => usa un default
func New(c HandlerArgs) (h *Handler, err error) {

	if c.Conn == nil {
		return h, errors.New("Conn no puede ser nil")
	}
	switch c.Entorno {
	case EntornoTesting, EntornoProduccion:
		//	ok
	default:
		return h, errors.New("entorno no definido")
	}

	h = &Handler{
		Conn:      c.Conn,
		secretKey: c.SecretKey,
	}
	h.WebSocket = c.EventBus

	// Cache
	defaultRistretto := &ristretto.Config{
		NumCounters: 1e7,     // number of keys to track frequency of (10M).
		MaxCost:     1 << 30, // maximum cost of cache (1GB).
		BufferItems: 64,      // number of keys per Get buffer.
		Metrics:     true,
	}
	var conf *ristretto.Config
	if c.CacheConfig == nil {
		conf = defaultRistretto
	} else {
		conf = c.CacheConfig
	}
	h.Cache, err = ristretto.NewCache(conf)
	if err != nil {
		return h, errors.Wrap(err, "creando Ristretto cache")
	}

	// Empresas
	empresasArgs := empresashandler.HandlerArgs{
		Conn:  c.Conn,
		Cache: h.Cache,
	}
	h.Empresas, err = empresashandler.NewHandler(empresasArgs)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando empresas Handler")
	}

	// Cuentas contables
	h.Cuentas, err = cuentashandler.NewHandler(cuentashandler.HandlerConfig{
		Conn:  c.Conn,
		Cache: h.Cache,
	})
	if err != nil {
		return nil, errors.Wrap(err, "iniciando Cuentas Handler")
	}

	// Cuentas grupos
	h.CuentasGrupos, err = cuentasgrupos.NewHandler(cuentasgrupos.HandlerConfig{
		Conn: c.Conn,
	})
	if err != nil {
		return nil, errors.Wrap(err, "iniciando CuentasGrupos handler ")
	}

	// AFIP
	h.AFIPwsaa, err = wsaa.NewStore(wsaacore.Entorno(c.Entorno), c.Conn, h.Empresas)
	if err != nil {
		return h, errors.Wrap(err, "iniciando AFIP WSAA Handler")
	}
	afipArgs := afip.AfipArgs{
		Conn:           c.Conn,
		EmpresasGetter: h.Empresas,
		Cuentas:        h.Cuentas,
		WSAAStore:      h.AFIPwsaa,
		Entorno:        wsaacore.Entorno(c.Entorno),
	}
	h.AFIP, err = afip.NewHandler(afipArgs)
	if err != nil {
		return h, errors.Wrap(err, "iniciando AFIP Handler")
	}
	h.AFIPcert, err = cert.NewHandler(c.Conn, h.Empresas, string(c.Entorno))
	if err != nil {
		return h, errors.Wrap(err, "iniciando AFIP cert Handler")
	}

	// BCRA
	h.BCRA = bcra.NewHandler(c.Conn)

	// Lookups
	h.Lookups = lookups.NewHandler(c.Conn)

	// Cajas
	h.Cajas, err = cajashandler.NewHandler(c.Conn, h.Cache, h.Lookups)
	if err != nil {
		return h, errors.Wrap(err, "iniciando Cajas Handler")
	}

	// Categorias productos
	h.Categorias, err = categorias.NewHandler(&categorias.HandlerArgs{
		Conn:  c.Conn,
		Cache: h.Cache,
	})
	if err != nil {
		return h, errors.Wrap(err, "iniciando Cajas Handler")
	}

	// Centros
	h.Centros = centroshandler.NewHandler(c.Conn)

	// Centros grupos
	h.CentrosGrupos, err = centrosgrupos.NewHandler(centrosgrupos.HandlerConfig{
		Conn: c.Conn,
	})
	if err != nil {
		return nil, errors.Wrap(err, "iniciando CentrosGrupos handler")
	}

	// Comitentes
	h.Comitentes, err = comitentes.NewHandler(c.Conn)
	if err != nil {
		return nil, errors.Wrap(err, "inciando comitentes Handler")
	}

	// Comps
	h.Comps, err = compshandler.NewHandler(c.Conn, h.Cache)
	if err != nil {
		return h, errors.Wrap(err, "iniciando Comps Handler")
	}

	// Condiciones
	condicionesConfig := condiciones.HandlerConfig{
		Conn:  c.Conn,
		Cache: h.Cache,
	}
	h.Condiciones, err = condiciones.NewHandler(condicionesConfig)
	if err != nil {
		return h, errors.Wrap(err, "iniciando Condiciones Handler")
	}

	// CRUDS
	// h.Cruds = cruds.NewHandler(c.Conn)

	// Depositos
	h.Depositos = depositos.NewHandler(c.Conn, h.Cache)

	// Geo
	h.Geo = geo.NewHandler(c.Conn)

	// Grupos
	h.Grupos = grupos.NewHandler(c.Conn)

	// Inflación
	h.InflacionConfig = inflacionConfig.NewHandler(c.Conn)

	h.Apliprod = apliprod.NewHandler(c.Conn)

	h.Aplifin, err = aplifinhandler.New(c.Conn, h.CuentasGrupos)
	if err != nil {
		return h, errors.Wrap(err, "iniciando aplifin")
	}

	// Listas precios
	h.ListasPrecio = listasprecios.NewHandler(c.Conn)

	// Monedas
	h.Monedas, err = monedas.NewHandler(c.Conn, h.Cache)
	if err != nil {
		return h, errors.Wrap(err, "iniciando monedas handler")
	}

	// Personas
	h.Personas, err = personasHandler.NewHandler(c.Conn, h.Cache, h.Cuentas, nil)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando Personas Handler")
	}

	// Sucursales
	sucArgs := sucursales.HandlerArgs{
		Conn:  c.Conn,
		Cache: h.Cache,
	}
	h.Sucursales, err = sucursales.NewHandler(sucArgs)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando sucursales handler")
	}

	// Precios
	h.Precios, err = precios.NewHandler(c.Conn)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando precios")
	}

	// Productos
	productosConfig := productos.HandlerConfig{
		Conn:                   c.Conn,
		Cache:                  h.Cache,
		TamañoMaximoDeImagenes: c.TamañoMaximoDeImagenes,
		CategoriasHandler:      h.Categorias,
	}
	h.Productos, err = productos.NewHandler(productosConfig)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando Productos Handler")
	}

	// Programas
	h.Programas = programas.NewHandler()
	h.Sesiones, err = sesiones.NewHandler(h.secretKey)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando Sesiones Handler")
	}

	// Reclamos
	h.Reclamos, err = reclamos.NewHandler(h.Conn)
	if err != nil {
		return nil, fmt.Errorf("iniciando reclamos Handler: %w", err)
	}

	// Reclamos tipos
	h.ReclamosTipos, err = reclamostipos.NewHandler(h.Conn)
	if err != nil {
		return nil, fmt.Errorf("iniciando reclamos tipos Handler: %w", err)
	}

	{ // Usuarios
		cac, err := usuarioscache.New(h.Cache)
		if err != nil {
			return nil, errors.Wrap(err, "iniciando usuarios")
		}
		h.Usuarios, err = usuarioshandler.NewHandler(c.Conn, cac)
		if err != nil {
			return nil, errors.Wrap(err, "iniciando Usuarios Handler")
		}
	}

	// Unicidades def
	unicidadesdefArgs := unicidadesdef.HandlerArgs{
		Conn:  c.Conn,
		Cache: h.Cache,
	}
	h.Unicidadesdef, err = unicidadesdef.NewHandler(unicidadesdefArgs)
	if err != nil {
		return nil, errors.Wrap(err, "inicidando UnicidadesDefHandler")
	}

	// Unicidades
	unicidadesArgs := unicidades.HandlerArgs{
		Conn:                 c.Conn,
		Cache:                h.Cache,
		UnicidadesdefHandler: h.Unicidadesdef,
	}
	h.Unicidades, err = unicidades.NewHandler(unicidadesArgs)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando unicidades handler")
	}

	// Usuarios grupos
	uu, err := usuariosgruposhandler.NewHandler(c.Conn)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando usuarios grupos")
	}
	h.UsuariosGrupos = uu

	// OpsBloqCta
	h.OpsBloqCta, err = opsbloqcta.NewHandler(c.Conn, h.Cuentas, h.Cache)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando opsbloqcta handler")
	}

	// Ops
	h.Ops, err = opshandler.NewHandler(
		h.Conn,
		h.Comps,
		h.Cuentas,
		h.Empresas,
		h.Personas,
		h.Comps,
		h.OpsBloqCta,
	)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando OpsHandler")
	}

	// Factura electronica handler
	h.AFIPwsfe, err = wsfev1.NewHandler(c.Conn, h.AFIPwsaa, h.Empresas, h.Comps, h.Ops)
	if err != nil {
		return h, errors.Wrap(err, "iniciando AFIP wsfev1 Handler")
	}

	// Autoaplicador
	h.Autoaplicador = autoaplicador.NewHandler(c.Conn, h.CuentasGrupos, h.Ops, h.Comps, h.Comps)

	// Inflación
	h.Inflacion, err = inflacion.NewHandler(c.Conn, h.Ops, h.Comps)
	if err != nil {
		return h, errors.Wrap(err, "iniciando inflacion handler")
	}

	// Minuta - Config
	h.MinutaConfig = minConfig.NewHandler(c.Conn)

	// Minuta esquemas
	h.MinutaEsquema = minEsquema.NewHandler(c.Conn)

	// Minuta
	h.Minuta, err = minuta.NewHandler(
		h.Conn,
		h.Ops,
		h.Comps,
		h.Comps,
		h.Cuentas,
		h.MinutaConfig,
		h.MinutaEsquema,
	)
	if err != nil {
		return h, errors.Wrap(err, "iniciando minutas Handler")
	}

	// Bienes de uso
	h.Bsuso = bsuso.NewHandler(c.Conn, h.Cuentas, h.Cuentas, h.Minuta)

	// Resumen bancario - Config
	resConfigArgs := resConfig.HandlerArgs{
		Conn:          c.Conn,
		CuentasGetter: h.Cuentas,
		PersonaGetter: h.Personas,
	}
	h.ResbancarioConfig, err = resConfig.NewHandler(resConfigArgs)
	if err != nil {
		return h, errors.Wrap(err, "iniciando ResbancarioConfig Handler")
	}

	// Resumen bancario
	h.Resbancario, err = resbancario.NewHandler(
		h.ResbancarioConfig,
		c.Conn,
		h.Personas,
		h.Cuentas,
		h.Cuentas,
		h.Ops,
		h.Comps,
		h.Comps,
	)
	if err != nil {
		return nil, errors.Wrap(err, "inciando resumen bancario")
	}

	// Rendiciones - Config
	h.RendicionConfig = rendConfig.NewHandler(c.Conn)

	// Rendiciones
	h.Rendicion, err = rendicionhandler.NewHandler(
		c.Conn,
		h.Cajas,
		h.RendicionConfig,
		h.Comps,
		h.Cuentas,
		h.Ops,
		h.Empresas,
		h.Empresas,
		h.Unicidades,
		h.Unicidadesdef,
	)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando rendiciones Handler")
	}

	// Fact - Permisos
	h.FactPermisos, err = factPermisos.NewHandler(c.Conn, h.Usuarios)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando fact/esquemas Handler")
	}

	// Fact - Config
	factConfigArgs := factConfig.HandlerArgs{
		Conn:              c.Conn,
		Cache:             h.Cache,
		CuentasHandler:    h.Cuentas,
		ProductosHandler:  h.Productos,
		CategoriasHandler: h.Categorias,
		PermisoDeleter:    h.FactPermisos,
	}
	h.FactConfig, err = factConfig.NewHandler(factConfigArgs)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando fact/config Handler")
	}

	// Fact - Esquemas
	factEsquemasArgs := factEsquema.HandlerArgs{
		Conn:           c.Conn,
		Cache:          h.Cache,
		PermisoDeleter: h.FactPermisos,
	}
	h.FactEsquema, err = factEsquema.NewHandler(factEsquemasArgs)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando fact/esquemas Handler")
	}

	// Fact
	h.Fact, err = facthandler.NewHandler(
		c.Conn,
		h.AFIP,
		h.Cajas,
		h.Categorias,
		h.Comps,
		h.Condiciones,
		h.FactConfig,
		h.Cuentas,
		h.CuentasGrupos,
		h.Depositos,
		h.Empresas,
		h.FactEsquema,
		h.Empresas,
		h.Monedas,
		h.Personas,
		h.Productos,
		h.Unicidades,
		h.Unicidades,
		h.Unicidadesdef,
		h.Ops,
		h.Usuarios,
		h.Ops,
		bus.NewBus(),
	)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando Fact Handler")
	}

	h.FactInter = factinter.NewHandler(factinter.Args{
		Fact:     h.Fact,
		Afip:     h.AFIPwsfe,
		Esquemas: h.FactEsquema,
		Comp:     h.Comps,
	})

	// Aplicador config
	h.AplicadorConfig, err = apliConfig.NewHandler(apliConfig.HandlerArgs{
		Conn: c.Conn,
	})
	if err != nil {
		return h, errors.Wrap(err, "iniciando aplicador config")
	}

	// Aplicador
	h.Aplicador, err = aplicador.NewHandler(
		c.Conn,
		h.AplicadorConfig,
		h.CuentasGrupos,
		h.Ops,
		h.Personas,
	)
	if err != nil {
		return h, errors.Wrap(err, "iniciando aplicador")
	}

	// Unicidades cont
	unicidadesArg := unicidadescont.HandlerArgs{
		Conn:          c.Conn,
		Unicidadesdef: h.Unicidadesdef,
		Unicidades:    h.Unicidades,
		Categorias:    h.Categorias,
		FactConfig:    h.FactConfig,
		Productos:     h.Productos,
	}
	h.Unicidadescont, err = unicidadescont.NewHandler(unicidadesArg)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando unicidadescont handler")
	}

	// Balances
	h.Balance = balance.NewHandler(c.Conn, h.Cuentas, h.CentrosGrupos, h.Centros, h.Empresas)

	// Libro diario
	h.LibroDiario, err = librodiario.NewHandler(c.Conn, h.Empresas, h.Cuentas)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando libro diario")
	}

	// Mayor
	h.Mayor, err = mayor.NewHandler(c.Conn, h.Monedas, h.CentrosGrupos)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando mayor")
	}

	// Refundicion
	h.Refundicion = refundicion.NewHandler(c.Conn, h.Minuta, h.MinutaConfig)
	h.Cierre = cierre.NewHandler(c.Conn, h.Minuta)
	h.Apertura = apertura.NewHandler(c.Conn, h.Minuta)

	// Resumen de caja
	h.ResumenCaja, err = resumencaja.NewHandler(c.Conn, h.Cajas)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando resumen caja")
	}

	// Saldos personas
	h.SaldosPersonas, err = saldospersonas.NewHandler(c.Conn, h.CuentasGrupos)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando saldospersonas")
	}

	// Saldos productos
	h.SaldosProductos = saldosproductos.NewHandler(c.Conn, h.CuentasGrupos)

	// Mayor productos
	h.MayorProductos = mayorproductos.NewHandler(c.Conn, h.CuentasGrupos)

	// Vista persona
	h.VistaPersona, err = vistapersona.NewHandler(
		c.Conn,
		h.Ops,
		h.Empresas,
		h.Empresas,
		h.Personas,
		h.Mayor,
		h.Aplifin,
	)
	if err != nil {
		return nil, errors.Wrap(err, "iniciando VistaPersona handler")
	}

	return
}
