package sucursales

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type ReadOneReq struct {
	ID        int `json:",string"` // Aquellas personas que tienen permitida esta cuenta
	Comitente int `json:",string"`
}

func ReadOne(ctx context.Context, conn *pgxpool.Pool, req ReadOneReq) (out Sucursal, err error) {
	if req.Comitente == 0 {
		return out, errors.Errorf("no se ingresó comitente")
	}
	if req.ID == 0 {
		return out, errors.Errorf("no se ingresó ID de sucursal")
	}

	query := `
		SELECT 
			id, persona_id, comitente, n_sucursal, nombre,
			calle, numero, piso, depto, ciudad_id, ciudad_cp, ciudad_nombre, provincia_id, provincia_nombre, pais,
			domicilios, emails, telefonos, cuentas_asociadas, activo, motivo_inactividad, 
			created_at, updated_at, empresa
		FROM sucursales
		WHERE comitente=$1 AND id=$2`

	err = conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(
		&out.ID, &out.PersonaID, &out.Comitente, &out.NSucursal, &out.Nombre,
		&out.Calle, &out.Numero, &out.Piso, &out.Depto, &out.CiudadID, &out.CiudadCP, &out.CiudadNombre, &out.ProvinciaID, &out.ProvinciaNombre, &out.Pais,
		&out.Domicilios, &out.Emails, &out.Telefonos, &out.CuentasAsociadas, &out.Activo, &out.MotivoInactividad, &out.CreatedAt, &out.UpdatedAt, &out.Empresa,
	)
	if err != nil {
		return out, deferror.DB(err, "escaneando sucursal")
	}
	return
}

type ReadManyReq struct {
	Comitente int
	Persona   uuid.UUID
	Cuenta    int `json:",string"` // Aquellas personas que tienen permitida esta cuenta
	Empresa   int `json:",string"`
}

// Devuelve los registro de la tabla personas
func ReadMany(ctx context.Context, conn *pgxpool.Pool, req ReadManyReq) (out []Sucursal, err error) {

	out = []Sucursal{}

	if req.Comitente == 0 {
		return out, errors.Errorf("no se ingresó comitente")
	}
	if req.Persona == uuid.Nil {
		return out, errors.Errorf("no se ingresó ID de persona")
	}

	ww := []string{
		"comitente = $1",
		"persona_id = $2",
	}
	pp := []interface{}{
		req.Comitente,
		req.Persona,
	}
	if req.Empresa != 0 {
		pp = append(pp, req.Empresa)
		ww = append(ww, fmt.Sprintf("(empresa = $%v OR empresa IS NULL)", len(pp)))
	}
	if req.Cuenta != 0 {
		pp = append(pp, req.Cuenta)
		ww = append(ww, fmt.Sprintf("id IN (SELECT sucursal FROM cuentas_asociadas WHERE comitente = $1 AND cuenta = $%v)", len(pp)))
	}
	where := fmt.Sprintf("WHERE %v", strings.Join(ww, " AND "))

	query := fmt.Sprintf(`
		SELECT 
			id, persona_id, comitente, n_sucursal, nombre,
			calle, numero, piso, depto, ciudad_id, ciudad_cp, ciudad_nombre, provincia_id, provincia_nombre, pais,
			domicilios, emails, telefonos, cuentas_asociadas, activo, motivo_inactividad, 
			created_at, updated_at, empresa
		FROM sucursales
		%v ORDER BY nombre`, where)

	rows, err := conn.Query(ctx, query, pp...)
	if err != nil {
		return out, errors.Wrap(err, "querying sucursales")
	}
	defer rows.Close()

	for rows.Next() {
		p := Sucursal{}
		err = rows.Scan(&p.ID, &p.PersonaID, &p.Comitente, &p.NSucursal, &p.Nombre,
			&p.Calle, &p.Numero, &p.Piso, &p.Depto, &p.CiudadID, &p.CiudadCP, &p.CiudadNombre, &p.ProvinciaID, &p.ProvinciaNombre, &p.Pais,
			&p.Domicilios, &p.Emails, &p.Telefonos, &p.CuentasAsociadas, &p.Activo, &p.MotivoInactividad,
			&p.CreatedAt, &p.UpdatedAt, &p.Empresa)
		if err != nil {
			return out, errors.Wrap(err, "escaneando sucursal")
		}
		out = append(out, p)
	}
	return
}

func Create(ctx context.Context, tx pgx.Tx, v *Sucursal) (err error) {

	query := `
INSERT INTO sucursales (
		persona_id,
		comitente,
		n_sucursal,
		nombre,
		calle,
		numero,
		piso,
		depto,
		ciudad_id,
		ciudad_cp,
		ciudad_nombre,
		pais,
		domicilios,
		emails,
		telefonos,
		cuentas_asociadas,
		activo,
		motivo_inactividad,
		created_at,
		updated_at,
		empresa ) VALUES (
	$1, $2, $3, $4, $5, $6, $7, $8, $9,	$10,
	$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,
	$21)`

	_, err = tx.Exec(ctx, query, v.PersonaID, v.Comitente, v.NSucursal, v.Nombre,
		v.Calle, v.Numero, v.Piso, v.Depto, v.CiudadID, v.CiudadCP, v.CiudadNombre,
		v.Pais, v.Domicilios, v.Emails, v.Telefonos, v.CuentasAsociadas, v.Activo,
		v.MotivoInactividad, time.Now(), nil, v.Empresa)
	if err != nil {
		return deferror.DB(err, "insertando sucursal")
	}
	return

}
