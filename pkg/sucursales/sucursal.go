package sucursales

import (
	"time"

	"bitbucket.org/marcos19/one/pkg/cuentasasociadas"
	"bitbucket.org/marcos19/one/pkg/domicilios"
	"bitbucket.org/marcos19/one/pkg/emails"
	"bitbucket.org/marcos19/one/pkg/telefonos"
	"github.com/gofrs/uuid"
)

// Sucursal permite dividir los actos económicos de una persona trabajando
// como si se tratase de dos personas independientes, pero que a los efectos legales
// estamos hablando de la misma persona.
//
// Así por ejemplo podríamos llevar como separadas las cuentas de AFA Las Rosas y
// AFA Los Cardos.
type Sucursal struct {
	ID        *int `json:",string"`
	PersonaID uuid.UUID
	Comitente int `json:",string"`
	// Un número más simple se lo doy correlativo automático
	NSucursal int
	// Descripción de la sucursal
	Nombre          string
	Calle           string
	Numero          string
	Piso            string
	Depto           string
	CiudadID        int
	CiudadCP        int
	CiudadNombre    string
	ProvinciaID     *int
	ProvinciaNombre *string
	Pais            *string

	Domicilios domicilios.Domicilios
	Emails     emails.Emails
	Telefonos  []telefonos.Telefono

	// Util para cuentas bancarias por ejemplo
	Empresa *int `json:",string"`

	Activo            bool
	MotivoInactividad string

	CuentasAsociadas cuentasasociadas.Cuentas

	CreatedAt *time.Time
	UpdatedAt *time.Time
}

func (Sucursal) TableName() string {
	return "sucursales"
}

func (p Sucursal) DomicilioString() string {
	ub := domicilios.Domicilio{
		DomCalle:     p.Calle,
		DomNumero:    p.Numero,
		DomPiso:      p.Piso,
		DomDpto:      p.Depto,
		CiudadNombre: p.CiudadNombre,
		CiudadCP:     p.CiudadCP,
	}
	return ub.String()
}
