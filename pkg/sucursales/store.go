package sucursales

// Store es un cache.
// Los packages que necesiten la Deposito, están programados hacia acá.
type Store interface {
	Get(ReadOneReq) (Sucursal, error)
	Del(id int)
}
