package sucursales

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/marcos19/one/pkg/cuentasasociadas"
	"bitbucket.org/marcos19/one/pkg/deferror"
	"github.com/cockroachdb/errors"
	"github.com/dgraph-io/ristretto"
	"github.com/gofrs/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler es el encargado de recibir los http requests y emitir los responses.
type Handler struct {
	conn  *pgxpool.Pool
	cache *ristretto.Cache
}

// NewHandler instancia el handler de fórmulas.
func NewHandler(args HandlerArgs) (h *Handler, err error) {

	if args.Conn == nil {
		return h, deferror.Validation("no se ingresó Conn")
	}
	if args.Cache == nil {
		return h, deferror.Validation("no se ingresó Cache")
	}

	h = &Handler{
		conn:  args.Conn,
		cache: args.Cache,
	}
	return
}

type HandlerArgs struct {
	Conn  *pgxpool.Pool
	Cache *ristretto.Cache
}

// HandleSucursalCreate recibe una Sucursal y la persiste.
func (h *Handler) Create(ctx context.Context, req *Sucursal) (err error) {

	// Valido
	if req == nil {
		return deferror.Validation("la sucursal ingresada era nil")
	}
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó comitente")
	}
	if req.Nombre == "" {
		return deferror.Validation("Debe ingresar un nombre para la sucursal")
	}
	if req.PersonaID == uuid.Nil {
		return deferror.Validation("no se ingresó el ID de la persona")
	}

	// Persona pertenece a este comitente?
	query := "SELECT id FROM personas WHERE id = $1 AND comitente = $2;"
	rows, err := h.conn.Query(context.Background(), query, req.PersonaID, req.Comitente)
	if err != nil {
		return deferror.DB(err, "buscando si la persona correspondia")
	}
	defer rows.Close()
	count := 0
	for rows.Next() {
		count++
	}
	if count == 0 {
		return deferror.Validationf("no se encontró la persona='%v'", req.PersonaID)
	}

	// Determino campo NSucursal
	// Para hacerlo más ameno, los llevo correlativo
	ultimo := 0
	err = h.conn.QueryRow(ctx, "SELECT n_sucursal FROM sucursales WHERE comitente=$1 AND persona_id=$2 ORDER BY n_sucursal DESC LIMIT 1", req.Comitente, req.ID).Scan(&ultimo)
	req.NSucursal = ultimo + 1

	// Inicio transacción
	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return errors.Wrap(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)

	// Creo sucursal
	err = Create(ctx, tx, req)
	if err != nil {
		return deferror.DB(err, "persistiendo sucursal")
	}

	// Inserto las cuentas asociadas
	for i := range req.CuentasAsociadas {
		req.CuentasAsociadas[i].Comitente = req.Comitente
		if req.ID != nil {
			req.CuentasAsociadas[i].Sucursal = *req.ID
		}
		req.CuentasAsociadas[i].Persona = req.PersonaID
		err = cuentasasociadas.Insert(ctx, tx, req.CuentasAsociadas[i])
		if err != nil {
			return deferror.DB(err, "insertando cuenta asociada")
		}
	}

	// Confirmo transacción
	err = tx.Commit(ctx)
	if err != nil {
		return deferror.DB(err, "confirmando transacción")
	}

	// Invalido cache
	key := hashUUID(req.Comitente, req.PersonaID)
	h.cache.Del(key)
	return
}

// HandleSucursales devuelve las sucursales para la persona y/o tipo de cuenta.
// Un banco por ejemplo puedo tener 3 sucursales para Cuenta corriente ARS,
// Cuenta Corriente USD y caja de ahorro, las cuales estan asociadas a las cuentas,
// contables correspondientes. Puedo tener también a Sucursal Las Rosas como cliente.
func (h *Handler) ReadMany(ctx context.Context, req ReadManyReq) (out []Sucursal, err error) {
	return ReadMany(ctx, h.conn, req)
}

func (h *Handler) ReadOne(ctx context.Context, req ReadOneReq) (out Sucursal, err error) {
	return ReadOne(ctx, h.conn, req)
}

func (h *Handler) Update(ctx context.Context, req *Sucursal) (err error) {

	// Valido
	if req == nil {
		return deferror.Validation("la sucursal era nil")
	}
	if req.Nombre == "" {
		return deferror.Validation("Debe ingresar un nombre para la sucursal")
	}
	if req.ID == nil {
		return deferror.Validation("no se ingresó el ID de la sucursal")
	}
	if *req.ID == 0 {
		return deferror.Validation("no se ingresó el ID de la sucursal")
	}

	// Busco anterior
	anterior, err := ReadOne(ctx, h.conn, ReadOneReq{Comitente: req.Comitente, ID: *req.ID})
	if err != nil {
		return deferror.DB(err, "buscando sucursales para modificar de la persona")
	}

	// Le pego el número que tenía la persona anterior
	req.PersonaID = anterior.PersonaID

	tx, err := h.conn.Begin(ctx)
	if err != nil {
		return deferror.DB(err, "iniciando transacción")
	}
	defer tx.Rollback(ctx)
	query := `
UPDATE sucursales
SET 
	nombre = $1,
	calle = $2,
	numero = $3,
	piso = $4,
	depto = $5,
	ciudad_id = $6,
	ciudad_cp = $7,
	ciudad_nombre = $8,
	provincia_id = $9,
	provincia_nombre = $10,
	pais = $11,
	domicilios = $12,
	emails = $13,
	telefonos = $14,
	cuentas_asociadas = $15,
	activo = $16,
	motivo_inactividad = $17,
	updated_at = $18,
	empresa = $19
WHERE comitente = $20 AND id = $21
;`
	_, err = tx.Exec(ctx, query,
		req.Nombre,
		req.Calle,
		req.Numero,
		req.Piso,
		req.Depto,
		req.CiudadID,
		req.CiudadCP,
		req.CiudadNombre,
		req.ProvinciaID,
		req.ProvinciaNombre,
		req.Pais,
		req.Domicilios,
		req.Emails,
		req.Telefonos,
		req.CuentasAsociadas,
		req.Activo,
		req.MotivoInactividad,
		time.Now(),
		req.Empresa,
		req.Comitente,
		*req.ID,
	)
	if err != nil {
		return deferror.DB(err, "modificando tabla sucursales")
	}

	{ // Cuentas asociadas

		// Borro anteriores
		query := `DELETE FROM cuentas_asociadas WHERE comitente = $1 AND persona = $2 AND sucursal = $3;`
		_, err = tx.Exec(ctx, query, req.Comitente, req.PersonaID, *req.ID)
		if err != nil {
			return errors.Wrap(err, "borrando cuentas asociadas")
		}

		// Inserto nuevas
		items := []string{}
		for _, v := range req.CuentasAsociadas {
			it := fmt.Sprintf("(%v,%v,'%v',%v)", req.Comitente, v.Cuenta, req.PersonaID, *req.ID)
			items = append(items, it)
		}
		if len(items) > 0 {
			values := strings.Join(items, ",")
			ins := fmt.Sprintf(
				"INSERT INTO cuentas_asociadas (comitente, cuenta, persona, sucursal) VALUES %v;",
				values,
			)
			_, err = tx.Exec(ctx, ins)
			if err != nil {
				return deferror.DB(err, "insertando cuentas asociadas")
			}
		}
	}

	// Confirmo
	err = tx.Commit(ctx)
	if err != nil {
		return deferror.DB(err, "confirmando transacción")
	}

	// // Inicio transacción
	// tx := h.db.Begin()
	// defer tx.Rollback()

	// // Grabo sucursal
	// err = tx.Save(&req).Error
	// if err != nil {
	// 	return deferror.DB(err, "persistiendo sucursal")

	// }

	// // Borro las cuentas asociadas
	// err = tx.Exec(`
	// 		DELETE FROM cuentas_asociadas
	// 		WHERE persona = ? AND sucursal = ?;`,
	// 	req.PersonaID, req.ID).Error
	// if err != nil {
	// 	deferror.DB(err, "borrando cuentas asociadas anteriores")
	// 	return
	// }

	// // Inserto las cuentas asociadas
	// for i := range req.CuentasAsociadas {
	// 	req.CuentasAsociadas[i].Comitente = req.Comitente
	// 	req.CuentasAsociadas[i].Sucursal = *req.ID
	// 	err = tx.Create(&req.CuentasAsociadas[i]).Error
	// 	if err != nil {
	// 		return deferror.DB(err, "insertando cuentas asociadas")
	// 	}
	// }

	// // Confirmo transacción
	// err = tx.Commit().Error
	// if err != nil {
	// 	return deferror.DB(err, "confirmando transacción")
	// }

	// Invalido cache
	key := hashUUID(req.Comitente, req.PersonaID)
	h.cache.Del(key)

	return
}

type DeleteReq struct {
	Comitente int `json:",string"`
	Sucursal  int `json:",string"`
	Persona   uuid.UUID
	Context   context.Context
}

func (h *Handler) Delete(req DeleteReq) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó comitente")
	}
	if req.Sucursal == 0 {
		return deferror.Validation("no se ingresó sucursal")
	}
	if req.Persona == uuid.Nil {
		return deferror.Validation("no se ingresó persona")
	}
	if req.Context == nil {
		req.Context = context.Background()
	}

	// Determino si la sucursal está usada en la contabilidad
	query := "SELECT id FROM partidas WHERE sucursal = $1 AND persona = $2 AND comitente = $3 LIMIT 1;"
	rows, err := h.conn.Query(req.Context, query, req.Sucursal, req.Persona, req.Comitente)
	if err != nil {
		deferror.DB(err, "determinando si la sucursal estaba usada en la contabilidad")
		return
	}
	defer rows.Close()
	for rows.Next() {
		deferror.Validation("No se puede borrar porque la sucursal estaba usada en la contabilidad")
		return
	}

	// Elimino sucursal
	delete := "DELETE FROM sucursales WHERE id = $1 AND persona_id = $2 AND comitente = $3;"
	_, err = h.conn.Exec(req.Context, delete, req.Sucursal, req.Persona, req.Comitente)
	if err != nil {
		deferror.DB(err, "borrando sucursal de base de datos")
		return
	}

	// Invalido cache
	key := hashUUID(req.Comitente, req.Persona)
	h.cache.Del(key)

	return
}

func hashUUID(comitente int, persona uuid.UUID) string {
	return fmt.Sprintf("%v/%v/%x", "Persona", comitente, persona)
}
