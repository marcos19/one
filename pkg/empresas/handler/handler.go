package handler

import (
	"context"
	"crypto/rsa"
	"encoding/base64"
	"fmt"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"bitbucket.org/marcos19/one/pkg/empresas/internal/db"
	"github.com/cockroachdb/errors"
	"github.com/dgraph-io/ristretto"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/rs/zerolog/log"
)

// Handler es el encargado de recibir los http requests y emitir los responses.
type Handler struct {
	conn  *pgxpool.Pool
	cache *ristretto.Cache
}

// NewHandler inicializa un cache y devuelve el puntero a este.
func NewHandler(args HandlerArgs) (h *Handler, err error) {

	if args.Conn == nil {
		return h, deferror.Validation("Conn no puede ser nil")
	}
	if args.Cache == nil {
		return h, deferror.Validation("Cache no puede ser nil")
	}
	h = &Handler{
		conn:  args.Conn,
		cache: args.Cache,
	}
	return
}

type HandlerArgs struct {
	Conn  *pgxpool.Pool
	Cache *ristretto.Cache
}

func (h *Handler) Create(ctx context.Context, req empresas.Empresa) (out int, err error) {

	if req.Nombre == "" {
		return out, deferror.Validation("no se ingresó nombre de empresa")
	}
	if req.CierreEjercicio == 0 {
		return out, deferror.Validation("no se ingresó fecha de cierre de ejercicio")
	}
	out, err = db.Create(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "creando empresa")
	}
	return
}

func (h *Handler) ReadOne(ctx context.Context, req empresas.ReadOneReq) (out empresas.Empresa, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó comitente")
	}
	if req.ID == 0 {
		return out, deferror.Validation("no se ingresó ID")
	}

	// Busco en cache
	key := hashInt(req)
	val, estaba := h.cache.Get(key)
	if estaba {
		out, ok := val.(empresas.Empresa)
		if ok && out.Comitente == req.Comitente {
			return out, nil
		}
	}

	// No estaba en cache, busco en DB
	out, err = db.ReadOne(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando empresa")
	}

	// Agrego a cache
	h.cache.Set(key, out, 0)

	return
}

func (h *Handler) ReadMany(ctx context.Context, req empresas.ReadManyReq) (out []empresas.ReadManyResp, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se ingresó comitente")
	}

	// Busco
	out, err = db.ReadMany(ctx, h.conn, req)
	if err != nil {
		return out, deferror.DB(err, "buscando empresas")
	}
	return
}

func (h *Handler) Update(ctx context.Context, req empresas.Empresa) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se ingresó ID de comitente")
	}
	if req.ID == 0 {
		return deferror.Validation("no se ingresó ID de empresa")
	}

	// Busco que exista
	_, err = h.ReadOne(ctx, empresas.ReadOneReq{Comitente: req.Comitente, ID: req.ID})
	if err != nil {
		return err
	}

	// Invalido cache
	key := hashInt(empresas.ReadOneReq{Comitente: req.Comitente, ID: req.ID})
	h.cache.Del(key)

	// Modifico
	err = db.Update(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "persistiendo empresa")
	}

	return
}

func (h *Handler) Delete(ctx context.Context, req empresas.ReadOneReq) (err error) {

	// Invalido cache
	key := hashInt(empresas.ReadOneReq(req))
	h.cache.Del(key)
	logo := hashLogo(empresas.ReadLogoReq{Comitente: req.Comitente, Empresa: req.ID})
	h.cache.Del(logo)

	// Borro
	err = db.Delete(ctx, h.conn, req)
	if err != nil {
		return deferror.DB(err, "borrando empresa")
	}

	return
}

func (h *Handler) UpdateLogo(ctx context.Context, req empresas.UpdateLogoReq) (err error) {

	// Valido
	if req.Comitente == 0 {
		return deferror.Validation("no se definió ID de comitente")
	}
	if req.Empresa == 0 {
		return deferror.Validation("no se definió ID de empresa")
	}

	by, err := base64.StdEncoding.DecodeString(req.Imagen)
	if err != nil {
		return errors.Wrap(err, "leyendo base64")
	}
	query := "UPDATE empresas SET logo = $1 WHERE id = $2 AND comitente = $3"

	_, err = h.conn.Exec(ctx, query, by, req.Empresa, req.Comitente)
	if err != nil {
		return deferror.DB(err, "insertando logo")
	}

	// Borro del cache
	h.cache.Del(hashLogo(empresas.ReadLogoReq{Comitente: req.Comitente, Empresa: req.Empresa}))

	return
}

func (h *Handler) ReadLogo(ctx context.Context, req empresas.ReadLogoReq) (out []byte, err error) {

	// Valido
	if req.Comitente == 0 {
		return out, deferror.Validation("no se definió ID de comitente")
	}
	if req.Empresa == 0 {
		return out, deferror.Validation("no se definió ID de empresa")
	}
	// Busco en cache
	key := hashLogo(req)
	val, estaba := h.cache.Get(key)
	if estaba {
		out, ok := val.([]byte)
		if ok {
			log.Debug().Str("tipo", "logo").Msg("Se leyó logo de cache")
			return out, nil
		}
	}

	// Busco en DB
	out, err = db.ReadLogo(ctx, h.conn, req)
	if err != nil {
		return out, err
	}

	// Agrego a cache
	h.cache.Set(key, out, 0)

	return
}

func (h *Handler) PrivateKey(ctx context.Context, req empresas.PrivateKeyReq) (key *rsa.PrivateKey, err error) {
	key, err = db.ReadOneKey(ctx, h.conn, req)
	if err != nil {
		return key, errors.Wrap(err, "buscando private key")
	}
	return
}

type PrivateKeyReg struct {
	Comitente int
	Empresa   int
	Key       []byte
	CreatedAt time.Time
}

func (p PrivateKeyReg) TableName() string {
	return "empresas_keys"
}

func hashInt(req empresas.ReadOneReq) string {
	return fmt.Sprintf("%v/%v/%v", "Empresa", req.Comitente, req.ID)
}
func hashLogo(req empresas.ReadLogoReq) string {
	return fmt.Sprintf("%v/%v/%v", "EmpresaLogo", req.Comitente, req.Empresa)
}
