package empresas

import (
	"context"
	"crypto/rsa"
)

type Creater interface {
	Create(context.Context, Empresa) (id int, err error)
}
type ReaderOne interface {
	ReadOne(context.Context, ReadOneReq) (Empresa, error)
}
type ReaderMany interface {
	ReadMany(context.Context, ReadManyReq) ([]ReadManyResp, error)
}
type Updater interface {
	Update(context.Context, Empresa) error
}
type Deleter interface {
	Delete(context.Context, ReadOneReq) error
}

type ReaderLogo interface {
	ReadLogo(context.Context, ReadLogoReq) ([]byte, error)
}
type UpdaterLogo interface {
	UpdateLogo(context.Context, UpdateLogoReq) error
}

type Getter interface {
	ReadOne(context.Context, ReadOneReq) (Empresa, error)
	PrivateKey(context.Context, PrivateKeyReq) (*rsa.PrivateKey, error)
}

type ReadManyReq struct {
	Comitente int
}
type ReadManyResp struct {
	ID     int `json:",string"`
	Nombre string
	IVA    CondicionIVA
}
type PrivateKeyReq struct {
	Comitente int
	Empresa   int `json:",string"`
}
type ReadLogoReq struct {
	Comitente int
	Empresa   int `json:",string"`
}
type ReadOneReq struct {
	ID        int `json:",string"`
	Comitente int `json:",string"`
}
type UpdateLogoReq struct {
	Comitente int
	Empresa   int `json:",string"`
	Imagen    string
}
