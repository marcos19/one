package dbtest

import (
	"context"
	"testing"

	"bitbucket.org/marcos19/one/pkg/database"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"bitbucket.org/marcos19/one/pkg/empresas/internal/db"
	"github.com/crosslogic/fecha"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDB(t *testing.T) {
	ctx := context.Background()

	// Conecto
	cn := database.TestDBConnectionString()
	cfg, err := pgxpool.ParseConfig(cn)
	require.Nil(t, err)
	conn, err := pgxpool.ConnectConfig(context.Background(), cfg)
	require.Nil(t, err)

	// Preparo base de datos
	err = Prepare(ctx, conn)
	require.Nil(t, err)

	id := 0
	t.Run("CREATE", func(t *testing.T) {
		id, err = db.Create(ctx, conn, empresaCompleta())
		require.Nil(t, err)
		assert.NotEqual(t, 0, id)
	})

	t.Run("READ", func(t *testing.T) {
		expected := empresaCompleta()
		expected.ID = id
		e, err := db.ReadOne(ctx, conn, empresas.ReadOneReq{
			Comitente: 1,
			ID:        id,
		})
		require.Nil(t, err)
		assert.NotEqual(t, 0, e.ID)
		assert.NotNil(t, 0, e.CreatedAt)
		assert.Nil(t, e.UpdatedAt)
		e.CreatedAt = nil
		e.ID = id
		assert.Equal(t, expected, e)
	})

	t.Run("UPDATE", func(t *testing.T) {
		expected := empresaCompletaModificada()
		expected.ID = id
		err = db.Update(ctx, conn, expected)
		require.Nil(t, err)

		leido, err := db.ReadOne(ctx, conn, empresas.ReadOneReq{
			Comitente: 1,
			ID:        id,
		})
		require.Nil(t, err)
		assert.NotNil(t, leido.UpdatedAt)
		assert.NotNil(t, leido.CreatedAt)
		leido.CreatedAt = nil
		leido.UpdatedAt = nil
		assert.Equal(t, expected, leido)
	})

	t.Run("DELETE sin partida", func(t *testing.T) {
		err := db.Delete(ctx, conn, empresas.ReadOneReq{
			Comitente: 1,
			ID:        id,
		})
		require.Nil(t, err)

		_, err = db.ReadOne(ctx, conn, empresas.ReadOneReq{
			Comitente: 1,
			ID:        id,
		})
		require.NotNil(t, err)
	})

	t.Run("DELETE con partida", func(t *testing.T) {

		// Creo nueva
		idConMov, err := db.Create(ctx, conn, empresaCompleta())
		require.Nil(t, err)
		assert.NotEqual(t, 0, id)

		// Borro partidas anteriores
		_, err = conn.Exec(ctx, "DELETE FROM partidas;")
		require.Nil(t, err)

		// Le agrego movimiento
		_, err = conn.Exec(ctx, "INSERT INTO partidas (id, comitente, empresa) VALUES (1,1,$1)", idConMov)
		require.Nil(t, err)

		// No debe dejar borrar
		err = db.Delete(ctx, conn, empresas.ReadOneReq{
			Comitente: 1,
			ID:        idConMov,
		})
		require.NotNil(t, err)

		_, err = db.ReadOne(ctx, conn, empresas.ReadOneReq{
			Comitente: 1,
			ID:        idConMov,
		})

		// Tiene que seguir estando
		require.Nil(t, err)

	})

}
func empresaCompleta() empresas.Empresa {
	desde := fecha.Fecha(20100101)
	hasta := fecha.Fecha(20300101)

	// Logo y logo formato se persisten con otro método

	return empresas.Empresa{
		Comitente:               1,
		Nombre:                  "Nombre testing",
		CUIT:                    5156151651,
		Domicilio:               "Domicilio",
		CierreEjercicio:         9,
		IVA:                     empresas.Inscripto,
		FechaDesde:              &desde,
		FechaHasta:              &hasta,
		ImpresionNombre:         "ImpresionNombre",
		ImpresionNombreFantasia: "ImpresionNombreFantasia",
		ImpresionRenglonesLeft:  "ImpresionRenglonesLeft",
		ImpresionRenglonesRight: "ImpresionRenglonesRight",
		PieMail:                 "PieMail",
		MailHost:                "MailHost",
		MailPort:                10,
		MailSenderAlias:         "MailSenderAlias",
		MailUserName:            "MailUserName",
		MailPassword:            "MailPassword",
		MailTemplate:            "MailTemplate",
	}
}

func empresaCompletaModificada() empresas.Empresa {
	desde := fecha.Fecha(20100102)
	hasta := fecha.Fecha(20300102)

	// Logo y logo formato se persisten con otro método

	return empresas.Empresa{
		Comitente:               1,
		Nombre:                  "Nombre testing2",
		CUIT:                    51561516512,
		Domicilio:               "Domicilio2",
		CierreEjercicio:         92,
		IVA:                     empresas.Monotributista,
		FechaDesde:              &desde,
		FechaHasta:              &hasta,
		ImpresionNombre:         "ImpresionNombre2",
		ImpresionNombreFantasia: "ImpresionNombreFantasia2",
		ImpresionRenglonesLeft:  "ImpresionRenglonesLeft2",
		ImpresionRenglonesRight: "ImpresionRenglonesRight2",
		PieMail:                 "PieMail2",
		MailHost:                "MailHost2",
		MailPort:                11,
		MailSenderAlias:         "MailSenderAlias2",
		MailUserName:            "MailUserName2",
		MailPassword:            "MailPassword2",
		MailTemplate:            "MailTemplate2",
	}
}
