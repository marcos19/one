package dbtest

import (
	"context"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/pkg/errors"
)

func Prepare(ctx context.Context, conn *pgxpool.Pool) error {

	{ // Borro base de datos
		q := `DROP DATABASE IF EXISTS test_empresas;`
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "borro base de datos")
		}
	}
	{ // Creo base de datos
		q := `CREATE DATABASE IF NOT EXISTS test_empresas; USE test_empresas;`
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando base de datos")
		}
	}

	{ // Tablas adicionales
		q := `CREATE TABLE IF NOT EXISTS comitentes (ID int PRIMARY KEY);`
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla comitentes")
		}

		_, err = conn.Exec(ctx, "UPSERT INTO comitentes (id) VALUES (1);")
		if err != nil {
			return errors.Wrap(err, "insertando comitente")
		}

	}

	{ // Tabla empresa
		q := ` CREATE TABLE IF NOT EXISTS empresas (
      id SERIAL PRIMARY KEY,
      comitente INT8 NOT NULL REFERENCES comitentes,
      nombre STRING NULL,
      cuit INT8 NULL,
      domicilio STRING NULL,
      cierre_ejercicio INT8 NULL,
      fecha_desde DATE NULL,
      fecha_hasta DATE NULL,
      impresion_nombre STRING NULL,
      impresion_nombre_fantasia STRING NULL,
      impresion_renglones_left STRING NULL,
      impresion_renglones_right STRING NULL,
      logo BYTES NULL,
      logo_formato STRING NULL,
      mail_host STRING NULL,
      mail_sender_alias STRING NULL,
      mail_user_name STRING NULL,
      mail_password STRING NULL,
      pie_mail STRING NULL,
      mail_port INT8 NULL,
      iva STRING NULL,
      created_at TIMESTAMPTZ NULL,
      updated_at TIMESTAMPTZ NULL,
      mail_template STRING NOT NULL DEFAULT '':::STRING
		  );`
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla empresa")
		}
	}

	{
		// Partidas (la necesito para borrar
		q := `CREATE TABLE IF NOT EXISTS partidas (ID int PRIMARY KEY, comitente int references comitentes, empresa int references empresas)`
		_, err := conn.Exec(ctx, q)
		if err != nil {
			return errors.Wrap(err, "creando tabla partidas")
		}
	}
	return nil
}
