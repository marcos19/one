package db

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"database/sql"
	"time"

	"bitbucket.org/marcos19/one/pkg/deferror"
	"bitbucket.org/marcos19/one/pkg/empresas"
	"github.com/cockroachdb/errors"
	"github.com/jackc/pgx/v4/pgxpool"
)

func Create(ctx context.Context, conn *pgxpool.Pool, req empresas.Empresa) (out int, err error) {

	if req.Comitente == 0 {
		return out, errors.New("no se ingresó ID de comitente")
	}
	query := `INSERT INTO empresas (
	comitente, 
	nombre,
	cuit,
	domicilio,
	cierre_ejercicio,
	fecha_desde,
	fecha_hasta,
	impresion_nombre,
	impresion_nombre_fantasia,
	impresion_renglones_left,
	impresion_renglones_right,
	mail_host,
	mail_port,
	mail_sender_alias,
	mail_user_name,
	mail_password,
	pie_mail,
	iva,
	mail_template,
	created_at) VALUES (
	 $1, $2, $3, $4, $5, $6, $7, $8, $9,$10,
	$11,$12,$13,$14,$15,$16,$17,$18,$19,$20) RETURNING id`

	err = conn.QueryRow(ctx, query,
		req.Comitente,
		req.Nombre,
		req.CUIT,
		req.Domicilio,
		req.CierreEjercicio,
		req.FechaDesde,
		req.FechaHasta,
		req.ImpresionNombre,
		req.ImpresionNombreFantasia,
		req.ImpresionRenglonesLeft,
		req.ImpresionRenglonesRight,
		req.MailHost,
		req.MailPort,
		req.MailSenderAlias,
		req.MailUserName,
		req.MailPassword,
		req.PieMail,
		req.IVA,
		req.MailTemplate,
		time.Now(),
	).Scan(&out)
	if err != nil {
		return out, deferror.DB(err, "inserting")
	}
	return
}

func ReadOne(ctx context.Context, conn *pgxpool.Pool, req empresas.ReadOneReq) (out empresas.Empresa, err error) {

	if req.Comitente == 0 {
		return out, errors.New("no se ingresó ID de comitente")
	}
	if req.ID == 0 {
		return out, errors.New("no se ingresó ID")
	}
	query := `SELECT 
	id, 
	comitente, 
	nombre,
	cuit,
	domicilio,
	cierre_ejercicio,
	fecha_desde,
	fecha_hasta,
	impresion_nombre,
	impresion_nombre_fantasia,
	impresion_renglones_left,
	impresion_renglones_right,
	mail_host,
	mail_port,
	mail_sender_alias,
	mail_user_name,
	mail_password,
	pie_mail,
	iva,
	mail_template,
	created_at,
	updated_at
FROM empresas
WHERE comitente=$1 AND id=$2;`

	err = conn.QueryRow(ctx, query, req.Comitente, req.ID).Scan(
		&out.ID,
		&out.Comitente,
		&out.Nombre,
		&out.CUIT,
		&out.Domicilio,
		&out.CierreEjercicio,
		&out.FechaDesde,
		&out.FechaHasta,
		&out.ImpresionNombre,
		&out.ImpresionNombreFantasia,
		&out.ImpresionRenglonesLeft,
		&out.ImpresionRenglonesRight,
		&out.MailHost,
		&out.MailPort,
		&out.MailSenderAlias,
		&out.MailUserName,
		&out.MailPassword,
		&out.PieMail,
		&out.IVA,
		&out.MailTemplate,
		&out.CreatedAt,
		&out.UpdatedAt,
	)
	if err != nil {
		return out, deferror.DB(err, "querying/scanning")
	}
	return
}

func ReadMany(ctx context.Context, conn *pgxpool.Pool, req empresas.ReadManyReq) (out []empresas.ReadManyResp, err error) {
	if req.Comitente == 0 {
		return out, deferror.ComitenteIndefinido()
	}

	out = []empresas.ReadManyResp{}

	query := `SELECT id, nombre, iva FROM empresas WHERE comitente=$1 ORDER BY nombre`

	rows, err := conn.Query(ctx, query, req.Comitente)
	if err != nil {
		return out, deferror.DB(err, "querying")
	}
	defer rows.Close()
	for rows.Next() {
		v := empresas.ReadManyResp{}
		err = rows.Scan(&v.ID, &v.Nombre, &v.IVA)
		if err != nil {
			return out, deferror.DB(err, "escaneando")
		}
		out = append(out, v)
	}

	return
}

func Update(ctx context.Context, conn *pgxpool.Pool, req empresas.Empresa) (err error) {

	if req.Comitente == 0 {
		return errors.New("no se ingresó ID de comitente")
	}
	if req.ID == 0 {
		return errors.New("no se ingresó ID")
	}
	query := `UPDATE empresas SET
	nombre=$3,
	cuit=$4,
	domicilio=$5,
	cierre_ejercicio=$6,
	fecha_desde=$7,
	fecha_hasta=$8,
	impresion_nombre=$9,
	impresion_nombre_fantasia=$10,
	impresion_renglones_left=$11,
	impresion_renglones_right=$12,
	mail_host=$13,
	mail_sender_alias=$14,
	mail_user_name=$15,
	mail_password=$16,
	pie_mail=$17,
	iva=$18,
	updated_at=$19,
	mail_port=$20,
	mail_template=$21
WHERE comitente=$1 AND id=$2;`

	res, err := conn.Exec(ctx, query,
		req.Comitente,
		req.ID,
		req.Nombre,
		req.CUIT,
		req.Domicilio,
		req.CierreEjercicio,
		req.FechaDesde,
		req.FechaHasta,
		req.ImpresionNombre,
		req.ImpresionNombreFantasia,
		req.ImpresionRenglonesLeft,
		req.ImpresionRenglonesRight,
		req.MailHost,
		req.MailSenderAlias,
		req.MailUserName,
		req.MailPassword,
		req.PieMail,
		req.IVA,
		time.Now(),
		req.MailPort,
		req.MailTemplate,
	)
	if err != nil {
		return deferror.DB(err, "updating")
	}
	if res.RowsAffected() == 0 {
		return deferror.DB(err, "no se modificó ningún registro")
	}

	return
}

func Delete(ctx context.Context, conn *pgxpool.Pool, req empresas.ReadOneReq) (err error) {

	if req.Comitente == 0 {
		return errors.New("no se ingresó ID de comitente")
	}
	if req.ID == 0 {
		return errors.New("no se ingresó ID de empresa")
	}
	{ // Está usada en la contabilidad?
		count := 0
		err = conn.QueryRow(ctx, "SELECT COUNT(id) FROM partidas WHERE comitente=$1 AND empresa=$2",
			req.Comitente, req.ID).Scan(&count)
		if err != nil {
			return deferror.DB(err, "corroborando si estaba usada en la contabilidad")
		}
		if count > 0 {
			return deferror.Validationf("No se puede borrar la empresa porque fue usada %v veces en la contabilidad", count)
		}
	}

	res, err := conn.Exec(ctx, "DELETE FROM empresas WHERE comitente=$1 AND id=$2", req.Comitente, req.ID)
	if err != nil {
		return deferror.DB(err, "deleting")
	}

	if res.RowsAffected() == 0 {
		return errors.New("no se borró ningún registro")
	}
	return
}

func ReadOneKey(ctx context.Context, conn *pgxpool.Pool, req empresas.PrivateKeyReq) (key *rsa.PrivateKey, err error) {

	if req.Comitente == 0 {
		return key, deferror.Validation("no se ingresó ID de comitente")
	}
	if req.Empresa == 0 {
		return key, deferror.Validation("no se ingresó ID de empresa")
	}

	query := "SELECT key FROM empresas_keys WHERE comitente=$1 AND empresa=$2"

	by := []byte{}
	err = conn.QueryRow(ctx, query, req.Comitente, req.Empresa).Scan(&by)

	if err != nil {

		// Agregué esta comparación por string porque errors.Is me daba false
		// No investigué motivo.
		if errors.Is(err, sql.ErrNoRows) || err.Error() == "no rows in result set" {
			key, err = CreateNewPrivateKey(ctx, conn, req)
			if err != nil {
				return key, errors.Wrap(err, "creando nueva private key")
			}
		} else {

			return key, errors.Wrap(err, "querying/scanning")
		}
	} else {
		key, err = x509.ParsePKCS1PrivateKey(by)
		if err != nil {
			return key, errors.Wrap(err, "parseando private key")
		}
	}
	return

}

func CreateNewPrivateKey(ctx context.Context, conn *pgxpool.Pool, req empresas.PrivateKeyReq) (key *rsa.PrivateKey, err error) {

	key, err = rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return key, errors.Wrap(err, "generando rsa key")
	}
	bytesCSR := x509.MarshalPKCS1PrivateKey(key)

	// la persisto
	query := `INSERT INTO empresas_keys (comitente, empresa, key, created_at) VALUES ($1,$2,$3,$4)`
	_, err = conn.Exec(ctx, query, req.Comitente, req.Empresa, bytesCSR, time.Now())
	if err != nil {
		return key, deferror.DB(err, "inserting")
	}
	return

}

func ReadLogo(ctx context.Context, conn *pgxpool.Pool, req empresas.ReadLogoReq) (out []byte, err error) {

	// No estaba en cache, busco en DB
	query := "SELECT logo FROM empresas WHERE id = $1 AND comitente = $2"

	err = conn.QueryRow(ctx, query, req.Empresa, req.Comitente).Scan(&out)
	if err != nil {
		return out, deferror.DB(err, "buscando logo")
	}
	return
}
