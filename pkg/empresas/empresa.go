package empresas

import (
	"time"

	"github.com/crosslogic/fecha"
)

// Empresa contiene los datos de todas las empresas habilitadas para trabajar
// Representa la estructura legal.
type Empresa struct {
	ID              int `json:",string"`
	Comitente       int `json:",string"`
	Nombre          string
	CUIT            int
	Domicilio       string
	CierreEjercicio int
	IVA             CondicionIVA

	FechaDesde *fecha.Fecha
	FechaHasta *fecha.Fecha

	// Impresion
	ImpresionNombre         string
	ImpresionNombreFantasia string
	ImpresionRenglonesLeft  string
	ImpresionRenglonesRight string

	Logo        []byte
	LogoFormato *string // png, jpg

	PieMail string // El texto que se imprime al final de los mails

	MailHost        string
	MailPort        int `json:",string"`
	MailSenderAlias string
	MailUserName    string
	MailPassword    string
	MailTemplate    string

	CreatedAt *time.Time
	UpdatedAt *time.Time
}

type CondicionIVA string

const (
	Inscripto      = "Responsable inscripto"
	NoInscripto    = "No inscripto"
	Monotributista = "Monotributista"
	Exento         = "Exento"
)
