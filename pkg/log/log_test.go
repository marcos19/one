package log

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/marcos19/one/pkg/usuarios"
)

func TestLog(t *testing.T) {

	//b := &bytes.Buffer{}
	raw := "/api/some_service?id=34&time=342543654"
	r := httptest.NewRequest(http.MethodGet, raw, nil)

	SetURL(r)
	newCtx := usuarios.SetUserInContext(r.Context(), "marcos")
	r = r.WithContext(newCtx)

	Warn(r.Context(), "some service was called")
}
