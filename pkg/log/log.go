package log

import (
	"context"
	"net/http"

	"bitbucket.org/marcos19/one/pkg/usuarios"
	"golang.org/x/exp/slog"
)

func Debug(ctx context.Context, msg string, args ...any) {
	args = append(getInfoFromContext(ctx), args...)
	slog.Debug(msg, args...)
}
func Info(ctx context.Context, msg string, args ...any) {
	args = append(getInfoFromContext(ctx), args...)
	slog.Info(msg, args...)
}
func Warn(ctx context.Context, msg string, args ...any) {
	args = append(getInfoFromContext(ctx), args...)
	slog.Warn(msg, args...)
}
func Error(ctx context.Context, err error, args ...any) {
	args = append(getInfoFromContext(ctx), args...)
	slog.Error(err.Error(), args...)
}

// Pega los datos del request al registro del log
func SetURL(r *http.Request) {
	ctx := r.Context()
	if len(r.URL.RawQuery) > 0 {
		ctx = context.WithValue(ctx, queryKey, r.URL.RawQuery)
	}
	ctx = context.WithValue(ctx, methodKey, r.Method)
	ctx = context.WithValue(ctx, pathKey, r.URL.Path)
	*r = *r.WithContext(ctx)
}

func getInfoFromContext(ctx context.Context) (out []any) {

	{ // URL Path
		val := getPath(ctx)
		if val != nil {
			out = append(out, slog.String("url", *val))
		}
	}

	{ // URL query
		val := getQuery(ctx)
		if val != nil {
			out = append(out, slog.String("query", *val))
		}
	}

	{ // Method
		val := getMethod(ctx)
		if val != nil {
			out = append(out, slog.String("method", *val))
		}
	}

	{ // User
		u := usuarios.GetUserFromContext(ctx)
		if u != nil {
			out = append(out, slog.String("user", *u))
		}
	}

	return out
}

type methodType string

var methodKey = methodType("method")

type pathType string

var pathKey = pathType("path")

type queryType string

var queryKey = queryType("query")

func getPath(ctx context.Context) *string {
	val, ok := ctx.Value(pathKey).(string)
	if !ok {
		return nil
	}
	return &val
}
func getMethod(ctx context.Context) *string {
	val, ok := ctx.Value(methodKey).(string)
	if !ok {
		return nil
	}
	return &val
}

func getQuery(ctx context.Context) *string {
	val, ok := ctx.Value(queryKey).(string)
	if !ok {
		return nil
	}
	return &val
}
