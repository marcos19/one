# Golang
export COCKROACH_PORT="26258"
go test ./...

# Vue.js
cd pocketbiz
npm run test:unit
