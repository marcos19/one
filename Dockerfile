# Frontend
FROM node:16-bullseye as frontend
WORKDIR /app/pocketbiz

COPY pocketbiz/src ./src
COPY pocketbiz/public ./public
COPY pocketbiz/*.json ./
COPY pocketbiz/*.js ./
RUN npm install
RUN npm run build

# Backend
FROM golang:alpine as backend
WORKDIR /app

COPY --from=frontend /app/pocketbiz/dist ./pocketbiz/dist

COPY go.mod ./
COPY go.sum ./
COPY main.go ./
COPY pkg ./pkg
COPY api ./api
COPY cmd ./cmd

RUN go mod download

RUN go install github.com/rakyll/statik 

RUN	statik --src=./pocketbiz/dist
COPY statik ./statik

RUN go build 

# Final build
FROM alpine
COPY --from=backend /app/one ./
